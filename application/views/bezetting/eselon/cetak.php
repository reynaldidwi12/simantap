<!--Import materialize.css-->
<link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>">
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<div class="box-body mdl-cell--12-col">

    <h3 class="mdl-cell mdl-cell--12-col"><div align="center"><b>BEZETTING PEGAWAI NEGERI SIPIL DAERAH<br/>BERDASARKAN ESELON</b></div></h3><br>
    
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th rowspan="3">No</th>
                <th rowspan="3">Unit Kerja</th>
                <th rowspan="3">JML</th>
                <th rowspan="2" colspan="2">Kelamin</th>
                <th colspan="14">ESELON</th>
                <th colspan="3" colspan="2">JML</th>
            </tr>
            <tr>
                <th colspan="2">I.b</th>
                <th colspan="2">II.a</th>
                <th colspan="2">II.b</th>
                <th colspan="2">III.a</th>
                <th colspan="2">III.b</th>
                <th colspan="2">IV.a</th>
                <th colspan="2">IV.b</th>
            </tr>
            <tr>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php for($c=1;$c<22;$c++) { ?>
                <td><small><i><?php echo $c; ?></i></small></td>
                <?php } ?>
            </tr>
            <?php
            $a=1;
            $b=1;

            if(!empty($datas)) {
                foreach($datas as $row) {
                    if (in_array($row->category, array('1','2','3','4'))) {
                        if(count($row->category) == '1') { ?>
                            <tr>
                                <td><?php echo str_romawi_format($row->category); ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->total_pegawai; ?></td>
                                <td><?php echo $row->L; ?></td>
                                <td><?php echo $row->P; ?></td>
                                <td><?php echo $row->L1B; ?></td>
                                <td><?php echo $row->P1B; ?></td>
                                <td><?php echo $row->L2A; ?></td>
                                <td><?php echo $row->P2A; ?></td>
                                <td><?php echo $row->L2B; ?></td>
                                <td><?php echo $row->P2B; ?></td>
                                <td><?php echo $row->L3A; ?></td>
                                <td><?php echo $row->P3A; ?></td>
                                <td><?php echo $row->L3B; ?></td>
                                <td><?php echo $row->P3B; ?></td>
                                <td><?php echo $row->L4A; ?></td>
                                <td><?php echo $row->P4A; ?></td>
                                <td><?php echo $row->L4B; ?></td>
                                <td><?php echo $row->P4B; ?></td>
                                <td><?php echo ($row->L1B+$row->L2A+$row->L2B+$row->L3A+$row->L3B+$row->L4A+$row->L4B); ?></td>
                                <td><?php echo ($row->P1B+$row->P2A+$row->P2B+$row->P3A+$row->P3B+$row->P4A+$row->P4B); ?></td>
                            </tr>
                        <?php }
                    } 
                }
                ?>
                <tr>
                    <td>V</td>
                    <td>DINAS</td>
                    <?php for($d=1;$d<22;$d++) { ?>
                    <td></td>
                    <?php } ?>
                </tr>
                <?php
                foreach($datas as $row) {
                    if (in_array($row->category, array('5'))) { ?>
                        <tr>
                            <td><?php echo $a; ?></td>
                            <td><?php echo $row->nama; ?></td>
                            <td><?php echo $row->total_pegawai; ?></td>
                            <td><?php echo $row->L; ?></td>
                            <td><?php echo $row->P; ?></td>
                            <td><?php echo $row->L1B; ?></td>
                            <td><?php echo $row->P1B; ?></td>
                            <td><?php echo $row->L2A; ?></td>
                            <td><?php echo $row->P2A; ?></td>
                            <td><?php echo $row->L2B; ?></td>
                            <td><?php echo $row->P2B; ?></td>
                            <td><?php echo $row->L3A; ?></td>
                            <td><?php echo $row->P3A; ?></td>
                            <td><?php echo $row->L3B; ?></td>
                            <td><?php echo $row->P3B; ?></td>
                            <td><?php echo $row->L4A; ?></td>
                            <td><?php echo $row->P4A; ?></td>
                            <td><?php echo $row->L4B; ?></td>
                            <td><?php echo $row->P4B; ?></td>
                            <td><?php echo ($row->L1B+$row->L2A+$row->L2B+$row->L3A+$row->L3B+$row->L4A+$row->L4B); ?></td>
                            <td><?php echo ($row->P1B+$row->P2A+$row->P2B+$row->P3A+$row->P3B+$row->P4A+$row->P4B); ?></td>
                        </tr>
                        <?php
                        $a++;
                    } 
                }
                ?>
                <tr>
                    <td>VI</td>
                    <td>BADAN</td>
                    <?php for($e=1;$e<22;$e++) { ?>
                    <td></td>
                    <?php } ?>
                </tr>
                <?php
                foreach($datas as $row) {
                    $tot1 += $row->total_pegawai;
                    $tot2 += $row->L;
                    $tot3 += $row->P;
                    $tot4 += $row->L1B;
                    $tot5 += $row->P1B;
                    $tot6 += $row->L2A;
                    $tot7 += $row->P2A;
                    $tot8 += $row->L2B;
                    $tot9 += $row->P2B;
                    $tot10 += $row->L3A;
                    $tot11 += $row->P3A;
                    $tot12 += $row->L3B;
                    $tot13 += $row->P3B;
                    $tot14 += $row->L4A;
                    $tot15 += $row->P4A;
                    $tot16 += $row->L4B;
                    $tot17 += $row->P4B;
                    $tot18 += ($row->L1B+$row->L2A+$row->L2B+$row->L3A+$row->L3B+$row->L4A+$row->L4B);
                    $tot19 += ($row->P1B+$row->P2A+$row->P2B+$row->P3A+$row->P3B+$row->P4A+$row->P4B);
                    if (in_array($row->category, array('6'))) { ?>
                        <tr>
                            <td><?php echo $b; ?></td>
                            <td><?php echo $row->nama; ?></td>
                            <td><?php echo $row->total_pegawai; ?></td>
                            <td><?php echo $row->L; ?></td>
                            <td><?php echo $row->P; ?></td>
                            <td><?php echo $row->L1B; ?></td>
                            <td><?php echo $row->P1B; ?></td>
                            <td><?php echo $row->L2A; ?></td>
                            <td><?php echo $row->P2A; ?></td>
                            <td><?php echo $row->L2B; ?></td>
                            <td><?php echo $row->P2B; ?></td>
                            <td><?php echo $row->L3A; ?></td>
                            <td><?php echo $row->P3A; ?></td>
                            <td><?php echo $row->L3B; ?></td>
                            <td><?php echo $row->P3B; ?></td>
                            <td><?php echo $row->L4A; ?></td>
                            <td><?php echo $row->P4A; ?></td>
                            <td><?php echo $row->L4B; ?></td>
                            <td><?php echo $row->P4B; ?></td>
                            <td><?php echo ($row->L1B+$row->L2A+$row->L2B+$row->L3A+$row->L3B+$row->L4A+$row->L4B); ?></td>
                            <td><?php echo ($row->P1B+$row->P2A+$row->P2B+$row->P3A+$row->P3B+$row->P4A+$row->P4B); ?></td>
                        </tr>
                        <?php
                        $b++;
                    } 
                    if (in_array($row->category, array('7','8','9','10','11','12','13','14','15','16','17','18','19','20','21'))) {
                        if(count($row->category) == '1') { ?>
                            <tr>
                                <td><?php echo str_romawi_format($row->category); ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->total_pegawai; ?></td>
                                <td><?php echo $row->L; ?></td>
                                <td><?php echo $row->P; ?></td>
                                <td><?php echo $row->L1B; ?></td>
                                <td><?php echo $row->P1B; ?></td>
                                <td><?php echo $row->L2A; ?></td>
                                <td><?php echo $row->P2A; ?></td>
                                <td><?php echo $row->L2B; ?></td>
                                <td><?php echo $row->P2B; ?></td>
                                <td><?php echo $row->L3A; ?></td>
                                <td><?php echo $row->P3A; ?></td>
                                <td><?php echo $row->L3B; ?></td>
                                <td><?php echo $row->P3B; ?></td>
                                <td><?php echo $row->L4A; ?></td>
                                <td><?php echo $row->P4A; ?></td>
                                <td><?php echo $row->L4B; ?></td>
                                <td><?php echo $row->P4B; ?></td>
                                <td><?php echo ($row->L1B+$row->L2A+$row->L2B+$row->L3A+$row->L3B+$row->L4A+$row->L4B); ?></td>
                                <td><?php echo ($row->P1B+$row->P2A+$row->P2B+$row->P3A+$row->P3B+$row->P4A+$row->P4B); ?></td>
                            </tr>
                        <?php }
                    }
                }
                ?>
                <tr>
                    <td colspan="2" align="center"><b>TOTAL</b></td>
                    <td><b><?php echo $tot1; ?></b></td>
                    <td><b><?php echo $tot2; ?></b></td>
                    <td><b><?php echo $tot3; ?></b></td>
                    <td><b><?php echo $tot4; ?></b></td>
                    <td><b><?php echo $tot5; ?></b></td>
                    <td><b><?php echo $tot6; ?></b></td>
                    <td><b><?php echo $tot7; ?></b></td>
                    <td><b><?php echo $tot8; ?></b></td>
                    <td><b><?php echo $tot9; ?></b></td>
                    <td><b><?php echo $tot10; ?></b></td>
                    <td><b><?php echo $tot11; ?></b></td>
                    <td><b><?php echo $tot12; ?></b></td>
                    <td><b><?php echo $tot13; ?></b></td>
                    <td><b><?php echo $tot14; ?></b></td>
                    <td><b><?php echo $tot15; ?></b></td>
                    <td><b><?php echo $tot16; ?></b></td>
                    <td><b><?php echo $tot17; ?></b></td>
                    <td><b><?php echo $tot18; ?></b></td>
                    <td><b><?php echo $tot19; ?></b></td>
                </tr>
                <?php
            } else { ?>
                <tr>
                    <td colspan="21">Data Tidak Tersedia</td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table><br>
</div>  

</div>    

</body>