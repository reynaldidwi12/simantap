<!--Import materialize.css-->
<link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>">
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<div class="box-body mdl-cell--12-col">

    <h3 class="mdl-cell mdl-cell--12-col"><div align="center"><b>BEZETTING PEGAWAI NEGERI SIPIL DAERAH<br/>BERDASARKAN PENDIDIKAN</b></div></h3><br>
    
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th rowspan="3">No</th>
                <th rowspan="3">Unit Kerja</th>
                <th rowspan="3">JML</th>
                <th rowspan="2" colspan="2">Kelamin</th>
                <th colspan="16">TINGKAT PENDIDIKAN</th>
                <th colspan="3" colspan="2">JML</th>
            </tr>
            <tr>
                <th colspan="2">S3</th>
                <th colspan="2">S2</th>
                <th colspan="2">S1</th>
                <th colspan="2">D3</th>
                <th colspan="2">D1</th>
                <th colspan="2">SMA</th>
                <th colspan="2">SMP</th>
                <th colspan="2">SD</th>
            </tr>
            <tr>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php for($c=1;$c<24;$c++) { ?>
                <td><small><i><?php echo $c; ?></i></small></td>
                <?php } ?>
            </tr>
            <?php
            $a=1;
            $b=1;

            if(!empty($datas)) {
                foreach($datas as $row) {
                    if (in_array($row->category, array('1','2','3','4'))) {
                        if(count($row->category) == '1') { ?>
                            <tr>
                                <td><?php echo str_romawi_format($row->category); ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->total_pegawai; ?></td>
                                <td><?php echo $row->L; ?></td>
                                <td><?php echo $row->P; ?></td>
                                <td><?php echo $row->LS3; ?></td>
                                <td><?php echo $row->PS3; ?></td>
                                <td><?php echo $row->LS2; ?></td>
                                <td><?php echo $row->PS2; ?></td>
                                <td><?php echo $row->LS1; ?></td>
                                <td><?php echo $row->PS1; ?></td>
                                <td><?php echo $row->LD3; ?></td>
                                <td><?php echo $row->PD3; ?></td>
                                <td><?php echo $row->LD1; ?></td>
                                <td><?php echo $row->PD1; ?></td>
                                <td><?php echo $row->LSMA; ?></td>
                                <td><?php echo $row->PSMA; ?></td>
                                <td><?php echo $row->LSMP; ?></td>
                                <td><?php echo $row->PSMP; ?></td>
                                <td><?php echo $row->LSD; ?></td>
                                <td><?php echo $row->PSD; ?></td>
                                <td><?php echo ($row->LS3+$row->LS2+$row->LS1+$row->LD3+$row->LD1+$row->LSMA+$row->LSMP+$row->LSD); ?></td>
                                <td><?php echo ($row->PS3+$row->PS2+$row->PS1+$row->PD3+$row->PD1+$row->PSMA+$row->PSMP+$row->PSD); ?></td>
                            </tr>
                        <?php }
                    } 
                }
                ?>
                <tr>
                    <td>V</td>
                    <td>DINAS</td>
                    <?php for($d=1;$d<22;$d++) { ?>
                    <td></td>
                    <?php } ?>
                </tr>
                <?php
                foreach($datas as $row) {
                    if (in_array($row->category, array('5'))) { ?>
                        <tr>
                            <td><?php echo $a; ?></td>
                            <td><?php echo $row->nama; ?></td>
                            <td><?php echo $row->total_pegawai; ?></td>
                            <td><?php echo $row->L; ?></td>
                            <td><?php echo $row->P; ?></td>
                            <td><?php echo $row->LS3; ?></td>
                            <td><?php echo $row->PS3; ?></td>
                            <td><?php echo $row->LS2; ?></td>
                            <td><?php echo $row->PS2; ?></td>
                            <td><?php echo $row->LS1; ?></td>
                            <td><?php echo $row->PS1; ?></td>
                            <td><?php echo $row->LD3; ?></td>
                            <td><?php echo $row->PD3; ?></td>
                            <td><?php echo $row->LD1; ?></td>
                            <td><?php echo $row->PD1; ?></td>
                            <td><?php echo $row->LSMA; ?></td>
                            <td><?php echo $row->PSMA; ?></td>
                            <td><?php echo $row->LSMP; ?></td>
                            <td><?php echo $row->PSMP; ?></td>
                            <td><?php echo $row->LSD; ?></td>
                            <td><?php echo $row->PSD; ?></td>
                            <td><?php echo ($row->LS3+$row->LS2+$row->LS1+$row->LD3+$row->LD1+$row->LSMA+$row->LSMP+$row->LSD); ?></td>
                            <td><?php echo ($row->PS3+$row->PS2+$row->PS1+$row->PD3+$row->PD1+$row->PSMA+$row->PSMP+$row->PSD); ?></td>
                        </tr>
                        <?php
                        $a++;
                    } 
                }
                ?>
                <tr>
                    <td>VI</td>
                    <td>BADAN</td>
                    <?php for($e=1;$e<22;$e++) { ?>
                    <td></td>
                    <?php } ?>
                </tr>
                <?php
                foreach($datas as $row) {
                    $tot1 += $row->total_pegawai;
                    $tot2 += $row->L;
                    $tot3 += $row->P;
                    $tot4 += $row->LS3;
                    $tot5 += $row->PS3;
                    $tot6 += $row->LS2;
                    $tot7 += $row->PS2;
                    $tot8 += $row->LS1;
                    $tot9 += $row->PS1;
                    $tot10 += $row->LD3;
                    $tot11 += $row->PD3;
                    $tot12 += $row->LD1;
                    $tot13 += $row->PD1;
                    $tot14 += $row->LSMA;
                    $tot15 += $row->PSMA;
                    $tot16 += $row->LSMP;
                    $tot17 += $row->PSMP;
                    $tot18 += $row->LSD;
                    $tot19 += $row->PSD;
                    $tot20 += ($row->LS3+$row->LS2+$row->LS1+$row->LD3+$row->LD1+$row->LSMA+$row->LSMP+$row->LSD);
                    $tot21 += ($row->PS3+$row->PS2+$row->PS1+$row->PD3+$row->PD1+$row->PSMA+$row->PSMP+$row->PSD);
                    if (in_array($row->category, array('6'))) { ?>
                        <tr>
                            <td><?php echo $b; ?></td>
                            <td><?php echo $row->nama; ?></td>
                            <td><?php echo $row->total_pegawai; ?></td>
                            <td><?php echo $row->L; ?></td>
                            <td><?php echo $row->P; ?></td>
                            <td><?php echo $row->LS3; ?></td>
                            <td><?php echo $row->PS3; ?></td>
                            <td><?php echo $row->LS2; ?></td>
                            <td><?php echo $row->PS2; ?></td>
                            <td><?php echo $row->LS1; ?></td>
                            <td><?php echo $row->PS1; ?></td>
                            <td><?php echo $row->LD3; ?></td>
                            <td><?php echo $row->PD3; ?></td>
                            <td><?php echo $row->LD1; ?></td>
                            <td><?php echo $row->PD1; ?></td>
                            <td><?php echo $row->LSMA; ?></td>
                            <td><?php echo $row->PSMA; ?></td>
                            <td><?php echo $row->LSMP; ?></td>
                            <td><?php echo $row->PSMP; ?></td>
                            <td><?php echo $row->LSD; ?></td>
                            <td><?php echo $row->PSD; ?></td>
                            <td><?php echo ($row->LS3+$row->LS2+$row->LS1+$row->LD3+$row->LD1+$row->LSMA+$row->LSMP+$row->LSD); ?></td>
                            <td><?php echo ($row->PS3+$row->PS2+$row->PS1+$row->PD3+$row->PD1+$row->PSMA+$row->PSMP+$row->PSD); ?></td>
                        </tr>
                        <?php
                        $b++;
                    } 
                    if (in_array($row->category, array('7','8','9','10','11','12','13','14','15','16','17','18','19','20','21'))) {
                        if(count($row->category) == '1') { ?>
                            <tr>
                                <td><?php echo str_romawi_format($row->category); ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->total_pegawai; ?></td>
                                <td><?php echo $row->L; ?></td>
                                <td><?php echo $row->P; ?></td>
                                <td><?php echo $row->LS3; ?></td>
                                <td><?php echo $row->PS3; ?></td>
                                <td><?php echo $row->LS2; ?></td>
                                <td><?php echo $row->PS2; ?></td>
                                <td><?php echo $row->LS1; ?></td>
                                <td><?php echo $row->PS1; ?></td>
                                <td><?php echo $row->LD3; ?></td>
                                <td><?php echo $row->PD3; ?></td>
                                <td><?php echo $row->LD1; ?></td>
                                <td><?php echo $row->PD1; ?></td>
                                <td><?php echo $row->LSMA; ?></td>
                                <td><?php echo $row->PSMA; ?></td>
                                <td><?php echo $row->LSMP; ?></td>
                                <td><?php echo $row->PSMP; ?></td>
                                <td><?php echo $row->LSD; ?></td>
                                <td><?php echo $row->PSD; ?></td>
                                <td><?php echo ($row->LS3+$row->LS2+$row->LS1+$row->LD3+$row->LD1+$row->LSMA+$row->LSMP+$row->LSD); ?></td>
                                <td><?php echo ($row->PS3+$row->PS2+$row->PS1+$row->PD3+$row->PD1+$row->PSMA+$row->PSMP+$row->PSD); ?></td>
                            </tr>
                        <?php }
                    }
                }
                ?>
                <tr>
                    <td colspan="2" align="center"><b>TOTAL</b></td>
                    <td><b><?php echo $tot1; ?></b></td>
                    <td><b><?php echo $tot2; ?></b></td>
                    <td><b><?php echo $tot3; ?></b></td>
                    <td><b><?php echo $tot4; ?></b></td>
                    <td><b><?php echo $tot5; ?></b></td>
                    <td><b><?php echo $tot6; ?></b></td>
                    <td><b><?php echo $tot7; ?></b></td>
                    <td><b><?php echo $tot8; ?></b></td>
                    <td><b><?php echo $tot9; ?></b></td>
                    <td><b><?php echo $tot10; ?></b></td>
                    <td><b><?php echo $tot11; ?></b></td>
                    <td><b><?php echo $tot12; ?></b></td>
                    <td><b><?php echo $tot13; ?></b></td>
                    <td><b><?php echo $tot14; ?></b></td>
                    <td><b><?php echo $tot15; ?></b></td>
                    <td><b><?php echo $tot16; ?></b></td>
                    <td><b><?php echo $tot17; ?></b></td>
                    <td><b><?php echo $tot18; ?></b></td>
                    <td><b><?php echo $tot19; ?></b></td>
                    <td><b><?php echo $tot20; ?></b></td>
                    <td><b><?php echo $tot21; ?></b></td>
                </tr>
                <?php
            } else { ?>
                <tr>
                    <td colspan="21">Data Tidak Tersedia</td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table><br>
</div>  

</div>    

</body>