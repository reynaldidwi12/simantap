<!--Import materialize.css-->
<link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>">
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<div class="box-body mdl-cell--12-col">

    <h3 class="mdl-cell mdl-cell--12-col"><div align="center"><b>BEZETTING PEGAWAI NEGERI SIPIL DAERAH<br/>BERDASARKAN DIKLAT</b></div></h3><br>
    
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th rowspan="3">No</th>
                <th rowspan="3">Unit Kerja</th>
                <th rowspan="3">JML</th>
                <th rowspan="2" colspan="2">Kelamin</th>
                <th colspan="8">DIKLAT</th>
                <th colspan="3" colspan="2">JML</th>
            </tr>
            <tr>
                <th colspan="2">I</th>
                <th colspan="2">II</th>
                <th colspan="2">III</th>
                <th colspan="2">IV</th>
            </tr>
            <tr>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php for($c=1;$c<16;$c++) { ?>
                <td><small><i><?php echo $c; ?></i></small></td>
                <?php } ?>
            </tr>
            <?php
            $a=1;
            $b=1;

            if(!empty($datas)) {
                foreach($datas as $row) {
                    if (in_array($row->category, array('1','2','3','4'))) {
                        if(count($row->category) == '1') { ?>
                            <tr>
                                <td><?php echo str_romawi_format($row->category); ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->total_pegawai; ?></td>
                                <td><?php echo $row->L; ?></td>
                                <td><?php echo $row->P; ?></td>
                                <td><?php echo $row->L1; ?></td>
                                <td><?php echo $row->P1; ?></td>
                                <td><?php echo $row->L2; ?></td>
                                <td><?php echo $row->P2; ?></td>
                                <td><?php echo $row->L3; ?></td>
                                <td><?php echo $row->P3; ?></td>
                                <td><?php echo $row->L4; ?></td>
                                <td><?php echo $row->P4; ?></td>
                                <td><?php echo ($row->L1+$row->L2+$row->L3+$row->L4); ?></td>
                                <td><?php echo ($row->P1+$row->P2+$row->P3+$row->P4); ?></td>
                            </tr>
                        <?php }
                    } 
                }
                ?>
                <tr>
                    <td>V</td>
                    <td>DINAS</td>
                    <?php for($d=1;$d<14;$d++) { ?>
                    <td></td>
                    <?php } ?>
                </tr>
                <?php
                foreach($datas as $row) {
                    if (in_array($row->category, array('5'))) { ?>
                        <tr>
                            <td><?php echo $a; ?></td>
                            <td><?php echo $row->nama; ?></td>
                            <td><?php echo $row->total_pegawai; ?></td>
                            <td><?php echo $row->L; ?></td>
                            <td><?php echo $row->P; ?></td>
                            <td><?php echo $row->L1; ?></td>
                            <td><?php echo $row->P1; ?></td>
                            <td><?php echo $row->L2; ?></td>
                            <td><?php echo $row->P2; ?></td>
                            <td><?php echo $row->L3; ?></td>
                            <td><?php echo $row->P3; ?></td>
                            <td><?php echo $row->L4; ?></td>
                            <td><?php echo $row->P4; ?></td>
                            <td><?php echo ($row->L1+$row->L2+$row->L3+$row->L4); ?></td>
                            <td><?php echo ($row->P1+$row->P2+$row->P3+$row->P4); ?></td>
                        </tr>
                        <?php
                        $a++;
                    } 
                }
                ?>
                <tr>
                    <td>VI</td>
                    <td>BADAN</td>
                    <?php for($e=1;$e<14;$e++) { ?>
                    <td></td>
                    <?php } ?>
                </tr>
                <?php
                foreach($datas as $row) {
                    $tot1 += $row->total_pegawai;
                    $tot2 += $row->L;
                    $tot3 += $row->P;
                    $tot4 += $row->L1;
                    $tot5 += $row->P1;
                    $tot6 += $row->L2;
                    $tot7 += $row->P2;
                    $tot8 += $row->L3;
                    $tot9 += $row->P3;
                    $tot10 += $row->L4;
                    $tot11 += $row->P4;
                    $tot12 += ($row->L1+$row->L2+$row->L3+$row->L4);
                    $tot13 += ($row->P1+$row->P2+$row->P3+$row->P4);
                    if (in_array($row->category, array('6'))) { ?>
                        <tr>
                            <td><?php echo $b; ?></td>
                            <td><?php echo $row->nama; ?></td>
                            <td><?php echo $row->total_pegawai; ?></td>
                            <td><?php echo $row->L; ?></td>
                            <td><?php echo $row->P; ?></td>
                            <td><?php echo $row->L1; ?></td>
                            <td><?php echo $row->P1; ?></td>
                            <td><?php echo $row->L2; ?></td>
                            <td><?php echo $row->P2; ?></td>
                            <td><?php echo $row->L3; ?></td>
                            <td><?php echo $row->P3; ?></td>
                            <td><?php echo $row->L4; ?></td>
                            <td><?php echo $row->P4; ?></td>
                            <td><?php echo ($row->L1+$row->L2+$row->L3+$row->L4); ?></td>
                            <td><?php echo ($row->P1+$row->P2+$row->P3+$row->P4); ?></td>
                        </tr>
                        <?php
                        $b++;
                    } 
                    if (in_array($row->category, array('7','8','9','10','11','12','13','14','15','16','17','18','19','20','21'))) {
                        if(count($row->category) == '1') { ?>
                            <tr>
                                <td><?php echo str_romawi_format($row->category); ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->total_pegawai; ?></td>
                                <td><?php echo $row->L; ?></td>
                                <td><?php echo $row->P; ?></td>
                                <td><?php echo $row->L1; ?></td>
                                <td><?php echo $row->P1; ?></td>
                                <td><?php echo $row->L2; ?></td>
                                <td><?php echo $row->P2; ?></td>
                                <td><?php echo $row->L3; ?></td>
                                <td><?php echo $row->P3; ?></td>
                                <td><?php echo $row->L4; ?></td>
                                <td><?php echo $row->P4; ?></td>
                                <td><?php echo ($row->L1+$row->L2+$row->L3+$row->L4); ?></td>
                                <td><?php echo ($row->P1+$row->P2+$row->P3+$row->P4); ?></td>
                            </tr>
                        <?php }
                    }
                }
                ?>
                <tr>
                    <td colspan="2" align="center"><b>TOTAL</b></td>
                    <td><b><?php echo $tot1; ?></b></td>
                    <td><b><?php echo $tot2; ?></b></td>
                    <td><b><?php echo $tot3; ?></b></td>
                    <td><b><?php echo $tot4; ?></b></td>
                    <td><b><?php echo $tot5; ?></b></td>
                    <td><b><?php echo $tot6; ?></b></td>
                    <td><b><?php echo $tot7; ?></b></td>
                    <td><b><?php echo $tot8; ?></b></td>
                    <td><b><?php echo $tot9; ?></b></td>
                    <td><b><?php echo $tot10; ?></b></td>
                    <td><b><?php echo $tot11; ?></b></td>
                    <td><b><?php echo $tot12; ?></b></td>
                    <td><b><?php echo $tot13; ?></b></td>
                </tr>
                <?php
            } else { ?>
                <tr>
                    <td colspan="14">Data Tidak Tersedia</td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table><br>
</div>  

</div>    

</body>