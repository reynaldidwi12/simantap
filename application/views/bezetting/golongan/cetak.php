<!--Import materialize.css-->
<link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>">
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<div class="box-body mdl-cell--12-col">

    <h3 class="mdl-cell mdl-cell--12-col"><div align="center"><b>BEZETTING PEGAWAI NEGERI SIPIL DAERAH<br/>BERDASARKAN GOLONGAN</b></div></h3><br>
    
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th rowspan="3">No</th>
                <th rowspan="3">Unit Kerja</th>
                <th rowspan="3">JML</th>
                <th rowspan="2" colspan="2">Kelamin</th>
                <th colspan="10">Golongan IV</th>
                <th colspan="10">Golongan III</th>
                <th colspan="10">Golongan II</th>
                <th colspan="10">Golongan I</th>
            </tr>
            <tr>
                <th colspan="2">d</th>
                <th colspan="2">c</th>
                <th colspan="2">b</th>
                <th colspan="2">a</th>
                <th colspan="2">JML</th>
                <th colspan="2">d</th>
                <th colspan="2">c</th>
                <th colspan="2">b</th>
                <th colspan="2">a</th>
                <th colspan="2">JML</th>
                <th colspan="2">d</th>
                <th colspan="2">c</th>
                <th colspan="2">b</th>
                <th colspan="2">a</th>
                <th colspan="2">JML</th>
                <th colspan="2">d</th>
                <th colspan="2">c</th>
                <th colspan="2">b</th>
                <th colspan="2">a</th>
                <th colspan="2">JML</th>
            </tr>
            <tr>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php for($c=1;$c<46;$c++) { ?>
                <td><small><i><?php echo $c; ?></i></small></td>
                <?php } ?>
            </tr>
            <?php
            $a=1;
            $b=1;

            if(!empty($datas)) {
                foreach($datas as $row) {
                    if (in_array($row->category, array('1','2','3','4'))) {
                        if(count($row->category) == '1') { ?>
                            <tr>
                                <td><?php echo str_romawi_format($row->category); ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->total_pegawai; ?></td>
                                <td><?php echo $row->L; ?></td>
                                <td><?php echo $row->P; ?></td>
                                <td><?php echo $row->L4D; ?></td>
                                <td><?php echo $row->P4D; ?></td>
                                <td><?php echo $row->L4C; ?></td>
                                <td><?php echo $row->P4C; ?></td>
                                <td><?php echo $row->L4B; ?></td>
                                <td><?php echo $row->P4B; ?></td>
                                <td><?php echo $row->L4A; ?></td>
                                <td><?php echo $row->P4A; ?></td>
                                <td><?php echo ($row->L4D+$row->L4C+$row->L4B+$row->L4A); ?></td>
                                <td><?php echo ($row->P4D+$row->P4C+$row->P4B+$row->P4A); ?></td>
                                <td><?php echo $row->L3D; ?></td>
                                <td><?php echo $row->P3D; ?></td>
                                <td><?php echo $row->L3C; ?></td>
                                <td><?php echo $row->P3C; ?></td>
                                <td><?php echo $row->L3B; ?></td>
                                <td><?php echo $row->P3B; ?></td>
                                <td><?php echo $row->L3A; ?></td>
                                <td><?php echo $row->P3A; ?></td>
                                <td><?php echo ($row->L3D+$row->L3C+$row->L3B+$row->L3A); ?></td>
                                <td><?php echo ($row->P3D+$row->P3C+$row->P3B+$row->P3A); ?></td>
                                <td><?php echo $row->L2D; ?></td>
                                <td><?php echo $row->P2D; ?></td>
                                <td><?php echo $row->L2C; ?></td>
                                <td><?php echo $row->P2C; ?></td>
                                <td><?php echo $row->L2B; ?></td>
                                <td><?php echo $row->P2B; ?></td>
                                <td><?php echo $row->L2A; ?></td>
                                <td><?php echo $row->P2A; ?></td>
                                <td><?php echo ($row->L2D+$row->L2C+$row->L2B+$row->L2A); ?></td>
                                <td><?php echo ($row->P2D+$row->P2C+$row->P2B+$row->P2A); ?></td>
                                <td><?php echo $row->L1D; ?></td>
                                <td><?php echo $row->P1D; ?></td>
                                <td><?php echo $row->L1C; ?></td>
                                <td><?php echo $row->P1C; ?></td>
                                <td><?php echo $row->L1B; ?></td>
                                <td><?php echo $row->P1B; ?></td>
                                <td><?php echo $row->L1A; ?></td>
                                <td><?php echo $row->P1A; ?></td>
                                <td><?php echo ($row->L1D+$row->L1C+$row->L1B+$row->L1A); ?></td>
                                <td><?php echo ($row->P1D+$row->P1C+$row->P1B+$row->P1A); ?></td>
                            </tr>
                        <?php }
                    } 
                }
                ?>
                <tr>
                    <td>V</td>
                    <td>DINAS</td>
                    <?php for($d=1;$d<44;$d++) { ?>
                    <td></td>
                    <?php } ?>
                </tr>
                <?php
                foreach($datas as $row) {
                    if (in_array($row->category, array('5'))) { ?>
                        <tr>
                            <td><?php echo $a; ?></td>
                            <td><?php echo $row->nama; ?></td>
                            <td><?php echo $row->total_pegawai; ?></td>
                            <td><?php echo $row->L; ?></td>
                            <td><?php echo $row->P; ?></td>
                            <td><?php echo $row->L4D; ?></td>
                            <td><?php echo $row->P4D; ?></td>
                            <td><?php echo $row->L4C; ?></td>
                            <td><?php echo $row->P4C; ?></td>
                            <td><?php echo $row->L4B; ?></td>
                            <td><?php echo $row->P4B; ?></td>
                            <td><?php echo $row->L4A; ?></td>
                            <td><?php echo $row->P4A; ?></td>
                            <td><?php echo ($row->L4D+$row->L4C+$row->L4B+$row->L4A); ?></td>
                            <td><?php echo ($row->P4D+$row->P4C+$row->P4B+$row->P4A); ?></td>
                            <td><?php echo $row->L3D; ?></td>
                            <td><?php echo $row->P3D; ?></td>
                            <td><?php echo $row->L3C; ?></td>
                            <td><?php echo $row->P3C; ?></td>
                            <td><?php echo $row->L3B; ?></td>
                            <td><?php echo $row->P3B; ?></td>
                            <td><?php echo $row->L3A; ?></td>
                            <td><?php echo $row->P3A; ?></td>
                            <td><?php echo ($row->L3D+$row->L3C+$row->L3B+$row->L3A); ?></td>
                            <td><?php echo ($row->P3D+$row->P3C+$row->P3B+$row->P3A); ?></td>
                            <td><?php echo $row->L2D; ?></td>
                            <td><?php echo $row->P2D; ?></td>
                            <td><?php echo $row->L2C; ?></td>
                            <td><?php echo $row->P2C; ?></td>
                            <td><?php echo $row->L2B; ?></td>
                            <td><?php echo $row->P2B; ?></td>
                            <td><?php echo $row->L2A; ?></td>
                            <td><?php echo $row->P2A; ?></td>
                            <td><?php echo ($row->L2D+$row->L2C+$row->L2B+$row->L2A); ?></td>
                            <td><?php echo ($row->P2D+$row->P2C+$row->P2B+$row->P2A); ?></td>
                            <td><?php echo $row->L1D; ?></td>
                            <td><?php echo $row->P1D; ?></td>
                            <td><?php echo $row->L1C; ?></td>
                            <td><?php echo $row->P1C; ?></td>
                            <td><?php echo $row->L1B; ?></td>
                            <td><?php echo $row->P1B; ?></td>
                            <td><?php echo $row->L1A; ?></td>
                            <td><?php echo $row->P1A; ?></td>
                            <td><?php echo ($row->L1D+$row->L1C+$row->L1B+$row->L1A); ?></td>
                            <td><?php echo ($row->P1D+$row->P1C+$row->P1B+$row->P1A); ?></td>
                        </tr>
                        <?php
                        $a++;
                    } 
                }
                ?>
                <tr>
                    <td>VI</td>
                    <td>BADAN</td>
                    <?php for($e=1;$e<44;$e++) { ?>
                    <td></td>
                    <?php } ?>
                </tr>
                <?php
                foreach($datas as $row) {
                    $tot1 += $row->total_pegawai;
                    $tot2 += $row->L;
                    $tot3 += $row->P;
                    $tot4 += $row->L4D;
                    $tot5 += $row->P4D;
                    $tot6 += $row->L4C;
                    $tot7 += $row->P4C;
                    $tot8 += $row->L4B;
                    $tot9 += $row->P4B;
                    $tot10 += $row->L4A;
                    $tot11 += $row->P4A;
                    $tot12 += ($row->L4D+$row->L4C+$row->L4B+$row->L4A);
                    $tot13 += ($row->P4D+$row->P4C+$row->P4B+$row->P4A);
                    $tot14 += $row->L3D;
                    $tot15 += $row->P3D;
                    $tot16 += $row->L3C;
                    $tot17 += $row->P3C;
                    $tot18 += $row->L3B;
                    $tot19 += $row->P3B;
                    $tot20 += $row->L3A;
                    $tot21 += $row->P3A;
                    $tot22 += ($row->L3D+$row->L3C+$row->L3B+$row->L3A);
                    $tot23 += ($row->P3D+$row->P3C+$row->P3B+$row->P3A);
                    $tot24 += $row->L2D;
                    $tot25 += $row->P2D;
                    $tot26 += $row->L2C;
                    $tot27 += $row->P2C;
                    $tot28 += $row->L2B;
                    $tot29 += $row->P2B;
                    $tot30 += $row->L2A;
                    $tot31 += $row->P2A;
                    $tot32 += ($row->L2D+$row->L2C+$row->L2B+$row->L2A);
                    $tot33 += ($row->P2D+$row->P2C+$row->P2B+$row->P2A);
                    $tot34 += $row->L1D;
                    $tot35 += $row->P1D;
                    $tot36 += $row->L1C;
                    $tot37 += $row->P1C;
                    $tot38 += $row->L1B;
                    $tot39 += $row->P1B;
                    $tot40 += $row->L1A;
                    $tot41 += $row->P1A;
                    $tot42 += ($row->L1D+$row->L1C+$row->L1B+$row->L1A);
                    $tot43 += ($row->P1D+$row->P1C+$row->P1B+$row->P1A);
                    if (in_array($row->category, array('6'))) { ?>
                        <tr>
                            <td><?php echo $b; ?></td>
                            <td><?php echo $row->nama; ?></td>
                            <td><?php echo $row->total_pegawai; ?></td>
                            <td><?php echo $row->L; ?></td>
                            <td><?php echo $row->P; ?></td>
                            <td><?php echo $row->L4D; ?></td>
                            <td><?php echo $row->P4D; ?></td>
                            <td><?php echo $row->L4C; ?></td>
                            <td><?php echo $row->P4C; ?></td>
                            <td><?php echo $row->L4B; ?></td>
                            <td><?php echo $row->P4B; ?></td>
                            <td><?php echo $row->L4A; ?></td>
                            <td><?php echo $row->P4A; ?></td>
                            <td><?php echo ($row->L4D+$row->L4C+$row->L4B+$row->L4A); ?></td>
                            <td><?php echo ($row->P4D+$row->P4C+$row->P4B+$row->P4A); ?></td>
                            <td><?php echo $row->L3D; ?></td>
                            <td><?php echo $row->P3D; ?></td>
                            <td><?php echo $row->L3C; ?></td>
                            <td><?php echo $row->P3C; ?></td>
                            <td><?php echo $row->L3B; ?></td>
                            <td><?php echo $row->P3B; ?></td>
                            <td><?php echo $row->L3A; ?></td>
                            <td><?php echo $row->P3A; ?></td>
                            <td><?php echo ($row->L3D+$row->L3C+$row->L3B+$row->L3A); ?></td>
                            <td><?php echo ($row->P3D+$row->P3C+$row->P3B+$row->P3A); ?></td>
                            <td><?php echo $row->L2D; ?></td>
                            <td><?php echo $row->P2D; ?></td>
                            <td><?php echo $row->L2C; ?></td>
                            <td><?php echo $row->P2C; ?></td>
                            <td><?php echo $row->L2B; ?></td>
                            <td><?php echo $row->P2B; ?></td>
                            <td><?php echo $row->L2A; ?></td>
                            <td><?php echo $row->P2A; ?></td>
                            <td><?php echo ($row->L2D+$row->L2C+$row->L2B+$row->L2A); ?></td>
                            <td><?php echo ($row->P2D+$row->P2C+$row->P2B+$row->P2A); ?></td>
                            <td><?php echo $row->L1D; ?></td>
                            <td><?php echo $row->P1D; ?></td>
                            <td><?php echo $row->L1C; ?></td>
                            <td><?php echo $row->P1C; ?></td>
                            <td><?php echo $row->L1B; ?></td>
                            <td><?php echo $row->P1B; ?></td>
                            <td><?php echo $row->L1A; ?></td>
                            <td><?php echo $row->P1A; ?></td>
                            <td><?php echo ($row->L1D+$row->L1C+$row->L1B+$row->L1A); ?></td>
                            <td><?php echo ($row->P1D+$row->P1C+$row->P1B+$row->P1A); ?></td>
                        </tr>
                        <?php
                        $b++;
                    } 
                    if (in_array($row->category, array('7','8','9','10','11','12','13','14','15','16','17','18','19','20','21'))) {
                        if(count($row->category) == '1') { ?>
                            <tr>
                                <td><?php echo str_romawi_format($row->category); ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->total_pegawai; ?></td>
                                <td><?php echo $row->L; ?></td>
                                <td><?php echo $row->P; ?></td>
                                <td><?php echo $row->L4D; ?></td>
                                <td><?php echo $row->P4D; ?></td>
                                <td><?php echo $row->L4C; ?></td>
                                <td><?php echo $row->P4C; ?></td>
                                <td><?php echo $row->L4B; ?></td>
                                <td><?php echo $row->P4B; ?></td>
                                <td><?php echo $row->L4A; ?></td>
                                <td><?php echo $row->P4A; ?></td>
                                <td><?php echo ($row->L4D+$row->L4C+$row->L4B+$row->L4A); ?></td>
                                <td><?php echo ($row->P4D+$row->P4C+$row->P4B+$row->P4A); ?></td>
                                <td><?php echo $row->L3D; ?></td>
                                <td><?php echo $row->P3D; ?></td>
                                <td><?php echo $row->L3C; ?></td>
                                <td><?php echo $row->P3C; ?></td>
                                <td><?php echo $row->L3B; ?></td>
                                <td><?php echo $row->P3B; ?></td>
                                <td><?php echo $row->L3A; ?></td>
                                <td><?php echo $row->P3A; ?></td>
                                <td><?php echo ($row->L3D+$row->L3C+$row->L3B+$row->L3A); ?></td>
                                <td><?php echo ($row->P3D+$row->P3C+$row->P3B+$row->P3A); ?></td>
                                <td><?php echo $row->L2D; ?></td>
                                <td><?php echo $row->P2D; ?></td>
                                <td><?php echo $row->L2C; ?></td>
                                <td><?php echo $row->P2C; ?></td>
                                <td><?php echo $row->L2B; ?></td>
                                <td><?php echo $row->P2B; ?></td>
                                <td><?php echo $row->L2A; ?></td>
                                <td><?php echo $row->P2A; ?></td>
                                <td><?php echo ($row->L2D+$row->L2C+$row->L2B+$row->L2A); ?></td>
                                <td><?php echo ($row->P2D+$row->P2C+$row->P2B+$row->P2A); ?></td>
                                <td><?php echo $row->L1D; ?></td>
                                <td><?php echo $row->P1D; ?></td>
                                <td><?php echo $row->L1C; ?></td>
                                <td><?php echo $row->P1C; ?></td>
                                <td><?php echo $row->L1B; ?></td>
                                <td><?php echo $row->P1B; ?></td>
                                <td><?php echo $row->L1A; ?></td>
                                <td><?php echo $row->P1A; ?></td>
                                <td><?php echo ($row->L1D+$row->L1C+$row->L1B+$row->L1A); ?></td>
                                <td><?php echo ($row->P1D+$row->P1C+$row->P1B+$row->P1A); ?></td>
                            </tr>
                        <?php }
                    }
                }
                ?>
                <tr>
                    <td colspan="2" align="center"><b>TOTAL</b></td>
                    <td><b><?php echo $tot1; ?></b></td>
                    <td><b><?php echo $tot2; ?></b></td>
                    <td><b><?php echo $tot3; ?></b></td>
                    <td><b><?php echo $tot4; ?></b></td>
                    <td><b><?php echo $tot5; ?></b></td>
                    <td><b><?php echo $tot6; ?></b></td>
                    <td><b><?php echo $tot7; ?></b></td>
                    <td><b><?php echo $tot8; ?></b></td>
                    <td><b><?php echo $tot9; ?></b></td>
                    <td><b><?php echo $tot10; ?></b></td>
                    <td><b><?php echo $tot11; ?></b></td>
                    <td><b><?php echo $tot12; ?></b></td>
                    <td><b><?php echo $tot13; ?></b></td>
                    <td><b><?php echo $tot14; ?></b></td>
                    <td><b><?php echo $tot15; ?></b></td>
                    <td><b><?php echo $tot16; ?></b></td>
                    <td><b><?php echo $tot17; ?></b></td>
                    <td><b><?php echo $tot18; ?></b></td>
                    <td><b><?php echo $tot19; ?></b></td>
                    <td><b><?php echo $tot20; ?></b></td>
                    <td><b><?php echo $tot21; ?></b></td>
                    <td><b><?php echo $tot22; ?></b></td>
                    <td><b><?php echo $tot23; ?></b></td>
                    <td><b><?php echo $tot24; ?></b></td>
                    <td><b><?php echo $tot25; ?></b></td>
                    <td><b><?php echo $tot26; ?></b></td>
                    <td><b><?php echo $tot27; ?></b></td>
                    <td><b><?php echo $tot28; ?></b></td>
                    <td><b><?php echo $tot29; ?></b></td>
                    <td><b><?php echo $tot30; ?></b></td>
                    <td><b><?php echo $tot31; ?></b></td>
                    <td><b><?php echo $tot32; ?></b></td>
                    <td><b><?php echo $tot33; ?></b></td>
                    <td><b><?php echo $tot34; ?></b></td>
                    <td><b><?php echo $tot35; ?></b></td>
                    <td><b><?php echo $tot36; ?></b></td>
                    <td><b><?php echo $tot37; ?></b></td>
                    <td><b><?php echo $tot38; ?></b></td>
                    <td><b><?php echo $tot39; ?></b></td>
                    <td><b><?php echo $tot40; ?></b></td>
                    <td><b><?php echo $tot41; ?></b></td>
                    <td><b><?php echo $tot42; ?></b></td>
                    <td><b><?php echo $tot43; ?></b></td>
                </tr>
                <?php
            } else { ?>
                <tr>
                    <td colspan="45">Data Tidak Tersedia</td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table><br>
</div>  

</div>    

</body>