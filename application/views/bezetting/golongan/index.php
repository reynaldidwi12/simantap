<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Golongan
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<?php if($this->session->userdata('group_id')<>'3') { ?>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('bezetting/golongan/exportPdf', 
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i> Print</button>' );?>
					&nbsp;
					<?php echo anchor('bezetting/golongan/exportExcel', 
					'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Download Excel"><i class="fa fa-file-excel-o"></i> Download Excel </button>' );?>
					</div>
				</div>
				<?php echo form_open("bezetting/golongan/category");?>
				<div class="col-md-5 col-sm-4 col-xs-4">
					<div class='col-md-12'>
					<?php 
					$option = array(
						''=>'Pilih Kategori',
						'1,2,3,4,9,10'=>'Sekretariat, Kecamatan dan Kelurahan',
						'5,11,12,13,14,15,16,17,18,19,20,21,22'=>'Dinas',
						'6'=>'Badan',
						'7,8'=>'Puskesmas dan Rumah Sakit',
						'23'=>'TK',
						'21'=>'SD',
						'22'=>'SLTP',
					); 
					echo form_dropdown('category',$option,@$category,'class="form-control selectpicker" data-live-search="true"');?>
					</div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:5%;vertical-align:middle;" rowspan="3"><center>No</center></th>
				<th style="width:25%;vertical-align:middle;" rowspan="3"><center>Instansi</center></th>
				<th style="width:10%;vertical-align:middle;" rowspan="3"><center>Jumlah Golongan</center></th>
				<th style="width:8%;vertical-align:middle;" rowspan="2" colspan="2"><center>Jenis Kelamin</center></th>
				<th style="width:10%;vertical-align:middle;" colspan="32"><center>Golongan</center></th>
            </tr>
            <tr>
				<th colspan="2"><center>I.a</center></th>
				<th colspan="2"><center>I.b</center></th>
				<th colspan="2"><center>I.c</center></th>
                <th colspan="2"><center>I.d</center></th>
                <th colspan="2"><center>II.a</center></th>
				<th colspan="2"><center>II.b</center></th>
				<th colspan="2"><center>II.c</center></th>
                <th colspan="2"><center>II.d</center></th>
                <th colspan="2"><center>III.a</center></th>
				<th colspan="2"><center>III.b</center></th>
				<th colspan="2"><center>III.c</center></th>
                <th colspan="2"><center>III.d</center></th>
                <th colspan="2"><center>IV.a</center></th>
				<th colspan="2"><center>IV.b</center></th>
				<th colspan="2"><center>IV.c</center></th>
                <th colspan="2"><center>IV.d</center></th>
            </tr>
            <tr>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
				<th><center>P</center></th>
				<th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
                <th><center>P</center></th>
                <th><center>L</center></th>
				<th><center>P</center></th>
				<th><center>L</center></th>
                <th><center>P</center></th>
            </tr>
			</thead>
			<tbody>
	    	<?php
	    	if($skpd){
	    		$i=$number+1;
				foreach ( $skpd as $row ) {
			?>
		        <tr class="gradeX">
		        <td ><center><?php echo $i?>.</center></td>
		        <td ><?php echo $row->nama?></td>
                <td><center><?php echo $row->total_pegawai?></center></td>
                <td><center><?php echo $row->L?></center></td>
                <td><center><?php echo $row->P?></center></td>
                <td><center><?php echo $row->L1A?></center></td>
				<td><center><?php echo $row->P1A?></center></td>
				<td><center><?php echo $row->L1B?></center></td>
				<td><center><?php echo $row->P1B?></center></td>
				<td><center><?php echo $row->L1C?></center></td>
				<td><center><?php echo $row->P1C?></center></td>
				<td><center><?php echo $row->L1D?></center></td>
				<td><center><?php echo $row->P1D?></center></td>
				<td><center><?php echo $row->L2A?></center></td>
				<td><center><?php echo $row->P2A?></center></td>
				<td><center><?php echo $row->L2B?></center></td>
				<td><center><?php echo $row->P2B?></center></td>
				<td><center><?php echo $row->L2C?></center></td>
				<td><center><?php echo $row->P2C?></center></td>
				<td><center><?php echo $row->L2D?></center></td>
				<td><center><?php echo $row->P2D?></center></td>
				<td><center><?php echo $row->L3A?></center></td>
				<td><center><?php echo $row->P3A?></center></td>
				<td><center><?php echo $row->L3B?></center></td>
				<td><center><?php echo $row->P3B?></center></td>
				<td><center><?php echo $row->L3C?></center></td>
				<td><center><?php echo $row->P3C?></center></td>
				<td><center><?php echo $row->L3D?></center></td>
				<td><center><?php echo $row->P3D?></center></td>
				<td><center><?php echo $row->L4A?></center></td>
				<td><center><?php echo $row->P4A?></center></td>
				<td><center><?php echo $row->L4B?></center></td>
				<td><center><?php echo $row->P4B?></center></td>
				<td><center><?php echo $row->L4C?></center></td>
				<td><center><?php echo $row->P4C?></center></td>
				<td><center><?php echo $row->L4D?></center></td>
                <td><center><?php echo $row->P4D?></center></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='37'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
