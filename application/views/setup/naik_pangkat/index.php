<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Data Pegawai Yang Akan Naik Pangkat
		</h3>
	</div>
	<!-- /.panel-heading -->
	<!--div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('report/report_naik_pangkat/add', 
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="fa fa-print"></i> Cetak</button>' );?>
					</div>
				</div>
			</div>
		</div>
	</div-->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					
				</div>
				<?php if($this->session->userdata('group_id')<>'3') { ?>
				<?php echo form_open("setup/naik_pangkat/find");?>
				<div class="col-md-5 col-sm-4 col-xs-4">
					<div class='col-md-2'>
					</div>
					<div class="col-md-10"><?php echo form_dropdown("nama_skpd", $nama_skpd, "",'id="kd_skpd" class="form-control selectpicker" onChange="tampilUnitkerja()" data-live-search="true" data-live-search-style="begins" title="Pilih SKPD" required');?></div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
			<?php } ?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:2%"><center>No</th>
				<th style="width:10%"><center>Foto</th>
				<th style="width:15%"><center>NIP Baru</th>
				<th style="width:30%"><center>Nama</th>
				<th style="width:25%"><center>TTL</th>
				<th style="width:20%"><center>Jenis Kelamin</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($pegawai){
	    		$i=$number+1;
				foreach ( $pegawai as $row ) {
					
					if($row->tgl_lahir){
						$TglLahir = date("d-m-Y",strtotime($row->tgl_lahir));							
					}else{}		
			?>
		        <tr class="gradeX">
		        <td ><?php echo $i?></td>
		        <td  ><center>
					<?php if($row->picture){?>
								<img src='<?php echo base_url();?>files/foto_pegawai/<?php echo $row->picture;?>' width="110" height="150"/>	
							<?php 
					}else{ ?>
								<img src='<?php echo base_url();?>files/foto_pegawai/image.png' width="110" height="150"/>	
					<?php } ?>
					</center>
				</td>
		        <td><?php echo $row->nip?></td>
		        <td><?php echo $row->gelar_depan.' '.$row->nama.' '.$row->gelar_belakang?></td>
		        <td><?php echo $row->tempat_lahir.", ".$TglLahir?></td>
		        <td><?php echo $row->kelamin?></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='6'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
