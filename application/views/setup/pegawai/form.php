
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

	    <script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/jquery.min.js'); ?>"></script>

	
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data Pegawai</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
			if($this->uri->segment(3)=="add"){
				echo form_open_multipart('setup/pegawai/add');
			}else if($this->uri->segment(3)=="modify"){
				echo form_open_multipart('setup/pegawai/modify/'.$this->uri->segment(4));
			}else{
				echo form_open('setup/pegawai/detail');
			}
			?>
			
				<div class="box-body">
					<div class="col-lg-6">
						<div class="form-group">
							<label >Foto Pegawai.</label>
							<?php echo form_input($picture)?>
							<?php if($gambar){?>
								<img src='<?php echo base_url();?>files/foto_pegawai/<?php echo $gambar?>' width="150" height="200"/>	
							<?php 
							}else{}?>
							
						</div>
						
						<div class="form-group">
							<label >NIP Baru</label>
							<?php echo form_input($nip)?>
						</div>
						<div class="form-group">
							<label >NIP Lama</label>
							<?php echo form_input($nip_lama)?>
						</div>
						
						<div class="form-group">
							<label >Tempat Lahir.</label>
							<?php echo form_input($tempat_lahir)?>
						</div>
						<div class="form-group">
							<label >Tanggal Lahir.</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tgl_lahir)?>	
							</div>
						</div>
						<div class="form-group">
							<label >No. KTP</label>
							<?php echo form_input($no_ktp)?>
						</div>
						<div class="form-group">
							<label >No. Karpeg</label>
							<?php echo form_input($no_kartu_pegawai)?>
						</div>
						<div class="form-group">
							<label >Jenis Kelamin.</label>
							<?php $option = array(
									''=>'',
									'1'=>'Pria',
									'2'=>'Wanita'
							); 
							echo form_dropdown($kelamin,$option,$get_kelamin);?>
						</div>
						<div class="form-group">
							<label >Alamat.</label>
							<?php echo form_textarea($alamat)?>
						</div>
						
						<div class="form-group">
							<label >Agama.</label>
							<?php $option = array(
									''=>'',
									'1'=>'Islam',
									'2'=>'Katolik',
									'3'=>'Hindu',
									'4'=>'Budha',
									'5'=>'Sinto',
									'6'=>'Konghucu',
									'7'=>'Protestan'
							); 
							echo form_dropdown($agama,$option,$get_agama);?>
						</div>
						<div class="form-group">
							<label >Golongan Darah.</label>
							<?php $option = array(
									''=>'',
									'1'=>'A',
									'2'=>'B',
									'3'=>'AB',
									'4'=>'O'
							); 
							echo form_dropdown($gol_darah,$option,$get_gol_darah);?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Nama Pegawai.</label>
							<?php echo form_input($nama)?>
						</div>
						<div class="form-group">
							<label >Gelar Depan.</label>
							<?php echo form_input($gelar_depan)?>
						</div>
						<div class="form-group">
							<label >Gelar Belakang.</label>
							<?php echo form_input($gelar_belakang)?>
						</div>
						<div class="form-group">
							<label >No. Telp.</label>
							<?php echo form_input($telp)?>
						</div>
						<div class="form-group">
							<label >No. BPJS / Askes</label>
							<?php echo form_input($no_askes)?>
						</div>
						<div class="form-group">
							<label >NPWP</label>
							<?php echo form_input($npwp)?>
						</div>
						<div class="form-group">
							<label >TMT CPNS</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tmt_cpns)?>	
							</div>
						</div>
						<div class="form-group">
							<label >TMT PNS</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tmt_pns)?>	
							</div>
						</div>
						<div class="form-group">
							<label >No Karsu/Karis</label>
							<?php echo form_input($no_kartu_keluarga)?>
						</div>
						<div class="form-group">
							<label >Status.</label>
							<?php $option = array(
									''=>'',
									'1'=>'Belum Kawin',
									'2'=>'Kawin',
									'3'=>'Cerai'
							); 
							echo form_dropdown($status,$option,$get_status);?>
						</div>
						<div class="form-group" id="lay_skpd">
							<label >OPD</label>
							<?php echo form_input($skpd)?>
						</div>
						<div class="form-group" id="lay_unitorganisasi">
							<label >Unit organisasi</label>
							<?php echo form_input($unitorganisasi)?>
						</div>	
						<div class="form-group" id="lay_unitkerja">
							<label >Unit kerja</label>
							<?php echo form_input($unitkerja)?>
						</div>	
						<div class="form-group" id="lay_subunitkerja">
							<label >Sub unit kerja</label>
							<?php echo form_input($subunitkerja)?>
						</div>	
					</div>

					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					
					<?php 
					if($this->uri->segment(4)){
							echo anchor('setup/pegawai/find/'.$this->uri->segment(4).'/nip', 
							'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );
					}else{
							echo anchor('setup/pegawai', 
							'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );
					}
					?>
					
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/jquery-2.1.1.js'); ?>"></script>
<script>
function tampilUnitkerja()
 {
	 kd_skpd = document.getElementById("kd_skpd").value;
	 $.ajax({
		 url:"<?php echo base_url();?>setup/pegawai/get/"+kd_skpd+"",
		 success: function(response){
		/*	 alert(response);*/
		 $("#kd_unitkerja").html(response);
		 /*console.log((response));
            $('.selectpicker').selectpicker({
                style: 'btn-primary',
                size: 2
            });*/
		 },
		/* error: function () {
            $('#kd_unitkerja').html('There was an error!');
        }*/
		
	 });
	 return false;
 }
</script>
<script>
  function maxLengthCheck(object) {
    if (object.value.length > object.maxLength)
      object.value = object.value.slice(0, object.maxLength)
  }
    
  function isNumeric (evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode (key);
    var regex = /[0-9]|\./;
    if ( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }



  $(document).ready(function($) {

  	var mykd_skpd = '<?php echo $this->data['skpd']['value']; ?>';
	var mykd_unitorganisasi = '<?php echo $this->data['unitorganisasi']['value']; ?>';
	var mykd_unitkerja = '<?php echo $this->data['unitkerja']['value']; ?>';
	var mykd_subunitkerja = '<?php echo $this->data['subunitkerja']['value']; ?>';


	if(mykd_skpd == ''){
		jQuery("#lay_skpd").css('display', 'none');
	}
	if(mykd_unitorganisasi == ''){ 
		jQuery("#lay_unitorganisasi").css('display', 'none');
	}
	if(mykd_unitkerja == ''){
		jQuery("#lay_unitkerja").css('display', 'none');
	}
	if(mykd_subunitkerja == ''){
		jQuery("#lay_subunitkerja").css('display', 'none');
	}


});
</script>