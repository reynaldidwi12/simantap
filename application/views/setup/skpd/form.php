<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data OPD</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
			if($this->uri->segment(3)=="add"){
				echo form_open('setup/skpd/add');
			}else if($this->uri->segment(3)=="modify"){
				echo form_open('setup/skpd/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
			}else{
				echo form_open('setup/skpd/detail');
			}
			?>
				<div class="box-body">
					<?php echo form_input($kd_skpd)?>
					
				</div>
				<div class="box-body">
					<div class="col-lg-6">
						<div class="form-group">
							<label >Nama OPD</label>
							<?php echo form_input($nama)?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Kepala OPD</label>
							<?php echo form_input($kepala_skpd)?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Kategori</label>
							<?php $option = array(
									''=>'Pilih Kategori',
									'1'=>'I - SEKRETARIAT DAERAH',
									'2'=>'II - SEKRETARIAT DPRD',
									'3'=>'III - SEKRETARIAT KPU',
									'4'=>'IV - SEKRETARIAT DEWAN PENGURUS KORPRI',
									'5'=>'V - DINAS',
									'6'=>'VI - BADAN',
									'7'=>'VII - RUMAH SAKIT UMUM DAERAH KOTA KENDARI',
									'8'=>'VIII - PUSKESMAS',
									'9'=>'IX - KECAMATAN',
									'10'=>'X - KELURAHAN',
									'11'=>'XI - PENGAWAS DINAS PENDIDIKAN',
									'12'=>'XII - SANGGAR KEGIATAN BELAJAR (SKB)',
									'13'=>'XIII - UPT DINAS PERTANIAN',
									'14'=>'XIV - UPT DINAS KELAUTAN & PERIKANAN',
									'15'=>'XV - UPT DINAS PENDIDIKAN, KEPEMUDAAN DAN OLAHRAGA',
									'16'=>'XVI - UPT DINAS PERHUBUNGAN',
									'17'=>'XVII - UPT DINAS DUKCAPIL',
									'18'=>'XVIII - UPT DINAS PERUMAHAN, KAWASAN PERMUKIMAN, DAN PERTANAHAN',
									'19'=>'XIX - UPT DINAS PP DAN KB',
									'20'=>'XX - UPT DINAS KESEHATAN',
									'21'=>'XXI - UPT DINAS LINGKUNGAN HIDUP',
									'22'=>'XXII - UPT BADAN PENGELOLA PAJAK DAN RETRIBUSI DAERAH',
									'23'=>'XXIII - TK',
									'21'=>'XXIV - SD',
									'22'=>'XXV - SLTP',
							); 
							echo form_dropdown($category,$option,$get_category,'class="form-control selectpicker" data-live-search="true" required');?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Kode</label>
							<?php echo form_input($kode)?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
								<label >Status</label>
								<?php $option = array(
										'N'=>'Tidak Tampil',
										'Y'=>'Tampil'
								); 
								echo form_dropdown($status,$option,$get_status);?>
						</div>
					</div>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('setup/skpd/index/'.$this->uri->segment(5), 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>