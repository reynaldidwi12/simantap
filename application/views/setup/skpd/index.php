<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> DATA OPD
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('setup/skpd/add', 
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data OPD"><i class="fa fa-plus"></i> Tambah OPD</button>' );?>
					&nbsp;
					<?php echo anchor('setup/skpd', 
					'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
					</div>
				</div>
				<?php echo form_open("setup/skpd/find");?>
				<div class="col-md-5 col-sm-4 col-xs-4">
					<div class='col-md-5'>
					<?php $option = array(
							'nama'=>'Nama OPD',
							'kepala_skpd'=>'Kepala OPD'
					); 
					echo form_dropdown('column',$option,'1','class="form-control"');?>
					</div>
					<div class="col-md-7"><input type="text" class="form-control" name="data" placeholder="Cari"></div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:5%"><center>No</center></th>
				<th  style="width:30%">Nama OPD</th>
				<th style="width:30%">Nama Kepala OPD</th>
				<th style="width:25%">Kategori</th>
				<th style="width:25%">Kode</th>
				<th>&nbsp;&nbsp;</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($skpd){
	    		$i=$number+1;
				foreach ( $skpd as $row ) {
			?>
		        <tr class="gradeX">
		        <td ><center><?php echo $i?>.</center></td>
		        <td ><?php echo $row->nama?></td>
				<td ><?php echo $row->kepala_skpd?></td>
				<td >
					<?php echo getNameCategory($row->category)?>
				</td>
				<td><?php echo $row->kode?></td>
		        <td ><center>
		        <a href="<?php echo base_url('setup/skpd/modify/'.$row->kd_skpd.'/'.$this->uri->segment(4)) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
		        &nbsp;
		        <a href="<?php echo base_url('setup/skpd/remove/'.$row->kd_skpd) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
				</center></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='6'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>

<?php
function getNameCategory($cat) {

	$unit = strtr($cat, array(
		'1'=>'I - SEKRETARIAT DAERAH',
		'2'=>'II - SEKRETARIAT DPRD',
		'3'=>'III - SEKRETARIAT KPU',
		'4'=>'IV - SEKRETARIAT DEWAN PENGURUS KORPRI',
		'5'=>'V - DINAS',
		'6'=>'VI - BADAN',
		'7'=>'VII - RUMAH SAKIT UMUM DAERAH KOTA KENDARI',
		'8'=>'VIII - PUSKESMAS',
		'9'=>'IX - KECAMATAN',
		'10'=>'X - KELURAHAN',
		'11'=>'XI - PENGAWAS DINAS PENDIDIKAN',
		'12'=>'XII - SANGGAR KEGIATAN BELAJAR (SKB)',
		'13'=>'XIII - UPT DINAS PERTANIAN',
		'14'=>'XIV - UPT DINAS KELAUTAN & PERIKANAN',
		'15'=>'XV - UPT DINAS PENDIDIKAN, KEPEMUDAAN DAN OLAHRAGA',
		'16'=>'XVI - UPT DINAS PERHUBUNGAN',
		'17'=>'XVII - UPT DINAS DUKCAPIL',
		'18'=>'XVIII - UPT DINAS PERUMAHAN, KAWASAN PERMUKIMAN, DAN PERTANAHAN',
		'19'=>'XIX - UPT DINAS PP DAN KB',
		'20'=>'XX - UPT DINAS KESEHATAN',
		'21'=>'XXI - UPT DINAS LINGKUNGAN HIDUP',
		'22'=>'XXII - UPT BADAN PENGELOLA PAJAK DAN RETRIBUSI DAERAH',
		'23'=>'XXIII - TK',
		'21'=>'XXIV - SD',
		'22'=>'XXV - SLTP',
	));

	return $unit;
}
?>
