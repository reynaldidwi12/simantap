<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data Jabatan</h3>
</div>
</section>

<?php //print_r($this->data['jabatan']);exit; ?>

<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
			if($this->uri->segment(3)=="add"){
				echo form_open('setup/riwayat_jabatan/add/'.$this->uri->segment(4),"name='jabatan'");
			}else if($this->uri->segment(3)=="modify"){
				echo form_open('setup/riwayat_jabatan/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5),"name='jabatan'");
			}else{
				echo form_open('setup/riwayat_jabatan/detail');
			}
			?>
				<div class="box-body">
					<div class="col-lg-7">
						<?php echo form_input($seq_no)?>
						<?php echo form_input($nama_jabatan_hiden)?>
						<div class="form-group">
							<label >NIP.</label>
							<?php echo form_input($nip)?>
						</div>
						<div class="form-group">
							<label >Jenis Jabatan</label>
							<?php $option = array(
									''=>'',
									'Struktural'=>'Struktural',
									'Fungsional Tertentu'=>'Fungsional Tertentu',
									'Fungsional Umum'=>'Fungsional Umum'
							); 
							echo form_dropdown($jenis_jabatan,$option,$get_jenis_jabatan,' onChange="filteringJenisJabatan()"');?>
						</div>
						
						<div class="form-group" id="lay_kd_skpd">
							<label>Nama OPD</label>
							<span id="unitorganisasi"></span>
							<?php echo form_dropdown("kd_skpd", $skpd, $get_skpd,'class="form-control selectpicker" data-live-search="true" id="kd_skpd" onChange="tampilUnitOrganisasi()" data-live-search-style="begins" title="Pilih OPD" required');?>
						</div>
						
						
						<div class="form-group" id="lay_kd_unitorganisasi">
							<label>Unit Organisasi</label>

							<?php echo form_dropdown("kd_unitorganisasi", $unitorganisasi,  $get_unitorganisasi,' class="form-control" id="kd_unitorganisasi" onChange="tampilUnitKerja()"  data-live-search="true" data-live-search-style="begins"');?>
						</div>
					
					
						<div class="form-group" id="lay_kd_unitkerja">
							<label>Unit Kerja</label>

							<?php echo form_dropdown("kd_unitkerja", $unitkerja,  $get_unitkerja,' class="form-control" id="kd_unitkerja" onChange="tampilSubUnitKerja()" data-live-search="true" data-live-search-style="begins"');?>
						</div>
					
					
						<div class="form-group" id="lay_kd_subunitkerja">
							<label >Sub Unit Kerja</label>
							<?php echo form_dropdown("kd_subunitkerja", $subunitkerja,  $get_subunitkerja,' class="form-control" id="kd_subunitkerja" onChange="tampilJabatan()" data-live-search="true" data-live-search-style="begins"');?>
						</div>
						
						<div class="form-group" id="lay_nama_jabatan">
							<label >Nama Jabatan</label>
							<?php echo form_dropdown("kd_jabatan", $jabatan, $get_jabatan,' id="kd_jabatan" class="form-control" data-live-search="true"  data-live-search-style="begins" required');?>
						</div>

						<div class="form-group" id="lay_auto_nama_jabatan">
							<label >Nama Jabatan</label>
							<?php echo form_input($nama_jabatan)?>
						</div>
						
						
					</div>
					<div class="col-lg-5">
						<div class="form-group">
							<label >Nama Pegawai.</label>
							<?php echo form_input($nama)?>
						</div>
						<div class="form-group">
							<label >No. SK</label>
							<?php echo form_input($no_sk)?>
						</div>
						<div class="form-group">
							<label >Esselon</label>
							<?php $option = array(
									''=>'',
									'11'=>'I a',
									'12'=>'I b',
									'21'=>'II a',
									'22'=>'II b',
									'31'=>'III a',
									'32'=>'III b',
									'41'=>'IV a',
									'42'=>'IV b',
									'51'=>'V a',
							); 
							if($get_jenis_jabatan=='Struktural'){
								echo form_dropdown($esselon,$option,$get_esselon);
							}else{
								echo form_dropdown($esselon,$option,$get_esselon,'disabled');
							}?>
						</div>
						<div class="form-group">
							<label >Tgl. SK</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tmt)?>
							</div>
						</div>
						<div class="form-group">
							<label >TMT Jabatan</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tmt_jabatan)?>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('setup/riwayat_jabatan/result_riwayat_jabatan/'.$this->uri->segment(4),
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>


<script>

var mykd_skpd = '<?php echo $this->data['kd_skpd']['value']; ?>';
var mykd_unitorganisasi = '<?php echo $this->data['kd_unitorganisasi']['value']; ?>';
var mykd_unitkerja = '<?php echo $this->data['kd_unitkerja']['value']; ?>';
var mykd_subunitkerja = '<?php echo $this->data['kd_subunitkerja']['value']; ?>';
var mykd_jabatan = '<?php echo $this->data['kd_jabatan']['value']; ?>';
var myjenis_jabatan = '<?php echo $this->data['jenis_jabatan']['value']; ?>';

if(mykd_skpd == ''){
	jQuery("#lay_kd_skpd").css('display', 'none');
}
if(mykd_unitorganisasi == ''){ 
	jQuery("#lay_kd_unitorganisasi").css('display', 'none');
}
if(mykd_unitkerja == ''){
	jQuery("#lay_kd_unitkerja").css('display', 'none');
}
if(mykd_subunitkerja == ''){
	jQuery("#lay_kd_subunitkerja").css('display', 'none');
}
if(mykd_jabatan == ''){
	jQuery("#lay_nama_jabatan").css('display', 'none');
}

if(myjenis_jabatan == ''){
	jQuery("#lay_auto_nama_jabatan").css('display', 'none');
}else if(myjenis_jabatan != 'Struktural'){
	jQuery("#lay_auto_nama_jabatan").css('display', '');
	jQuery("#lay_nama_jabatan").css('display', 'none');
	$('#kd_jabatan').removeAttr("required");
}else{
	jQuery("#lay_auto_nama_jabatan").css('display', 'none');
	$('#nama_jabatan').removeAttr("required");
}


function filteringJenisJabatan(){
	
	var jbselected = document.getElementById("jenis_jabatan").value;

	if (jbselected == "Struktural"){     
		document.getElementById("esselon").disabled=false;
		jQuery("#lay_auto_nama_jabatan").css('display', 'none');

		$('#kd_jabatan').attr("required", "true");
		$('#kd_jabatan_hiden').removeAttr("required");

    }else {
		document.getElementById("esselon").disabled=true;
		jQuery("#lay_auto_nama_jabatan").css('display', '');
		jQuery("#lay_nama_jabatan").css('display', 'none');

		$('#kd_jabatan').removeAttr("required");
    }

	jQuery("#lay_kd_skpd").css('display', '');
	
	$('.selectpicker').selectpicker('val', '');

	
	jQuery('#kd_unitorganisasi').html('');
	jQuery('#kd_unitkerja').html('');
	jQuery('#kd_subunitkerja').html('');

	if(jbselected == "Fungsional Umum"){

		
		jQuery("#lay_kd_skpd").css('display', '');

		jQuery("#lay_kd_unitorganisasi").css('display', 'none');
		jQuery("#kd_unitorganisasi").html('');

		jQuery("#lay_kd_unitkerja").css('display', 'none');
		jQuery("#kd_unitkerja").html('');

		jQuery("#lay_kd_subunitkerja").css('display', 'none');
		jQuery("#kd_subunitkerja").html('');

		
		jQuery("#lay_nama_jabatan").css('display', 'none');

		/*---------perubahan ---------*/
		jQuery("#lay_auto_nama_jabatan").css('display', 'none');
		$('#kd_jabatan').removeAttr("required");

	}else if(jbselected == "Fungsional Tertentu"){
 	
		jQuery("#lay_kd_skpd").css('display', '');
	 	jQuery("#lay_kd_unitorganisasi").css('display', 'none');
	 	jQuery("#lay_kd_unitkerja").css('display', 'none');
		jQuery("#lay_kd_subunitkerja").css('display', 'none');
		jQuery("#lay_nama_jabatan").css('display', '');

		/*---------perubahan ---------*/
		jQuery("#lay_auto_nama_jabatan").css('display', 'none');
		$('#kd_jabatan').removeAttr("required");


		jQuery("#kd_unitorganisasi").html('');
		jQuery("#kd_unitkerja").html('');
		jQuery("#kd_subunitkerja").html('');


	}else{

		jQuery("#lay_kd_skpd").css('display', '');

		jQuery("#lay_kd_unitorganisasi").css('display', 'none');
		jQuery("#kd_unitorganisasi").html('');

		jQuery("#lay_kd_unitkerja").css('display', 'none');
		jQuery("#kd_unitkerja").html('');

		jQuery("#lay_kd_subunitkerja").css('display', 'none');
		jQuery("#kd_subunitkerja").html('');

		
		jQuery("#lay_nama_jabatan").css('display', 'none');
		$('#kd_jabatan').attr("required", "true");
		$('#nama_jabatan').removeAttr("required");
	}


	if(mykd_jabatan == ''){
		jQuery("#lay_nama_jabatan").css('display', 'none');
	}else{
		jQuery("#lay_nama_jabatan").css('display', '');
	}



	return false;
}

   
function tampilUnitOrganisasi(){

	var jbselected = document.getElementById("jenis_jabatan").value;
	kd_skpd = document.getElementById("kd_skpd").value;
	
	if(kd_skpd !== ''){

		jQuery("#lay_nama_jabatan").css('display', '');

		$.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_unitorganisasi/"+kd_skpd+"",
			 success: function(response){

			 	if(jbselected == "Fungsional Umum"){
					
					$("#kd_unitorganisasi").html(response);
				 	$("#lay_kd_unitorganisasi").css('display', '');

				 	jQuery("#lay_kd_unitkerja").css('display', 'none');
					jQuery("#lay_kd_subunitkerja").css('display', 'none');
					jQuery("#lay_nama_jabatan").css('display', '');

					$("#kd_unitorganisasi").css('display', '');
					$("#kd_unitkerja").css('display', '');
					$("#kd_subunitkerja").css('display', '');

					jQuery("#lay_nama_jabatan").css('display', 'none');
					jQuery("#lay_auto_nama_jabatan").css('display', '');
					$('#kd_jabatan').removeAttr("required");

			 	}else if(jbselected == "Fungsional Tertentu"){
			 		
			 		$("#kd_unitorganisasi").html(response);
			 		$("#lay_kd_unitorganisasi").css('display', '');

				 	jQuery("#lay_kd_unitkerja").css('display', 'none');
					jQuery("#lay_kd_subunitkerja").css('display', 'none');
					jQuery("#lay_nama_jabatan").css('display', 'none');

					$("#lay_kd_skpd").css('display', '');

					$("#kd_unitorganisasi").css('display', '');
					$("#kd_unitkerja").css('display', '');
					$("#kd_subunitkerja").css('display', '');
					$('#kd_jabatan').removeAttr("required");

			 	}else{
			 		$("#kd_unitorganisasi").html(response);
				 	$("#lay_kd_unitorganisasi").css('display', '');

				 	jQuery("#lay_kd_unitkerja").css('display', 'none');
					jQuery("#lay_kd_subunitkerja").css('display', 'none');
					jQuery("#lay_nama_jabatan").css('display', '');

					$("#kd_unitorganisasi").css('display', '');
					$("#kd_unitkerja").css('display', '');
					$("#kd_subunitkerja").css('display', '');

					$('#kd_jabatan').attr("required", "true");
					$('#nama_jabatan').removeAttr("required");
			 	}
			 },
			 dataType:"html"
		 });


		$.ajax({
			 url:"<?php echo base_url();?>setup/riwayat_jabatan/get_jabatan_by_skpd/"+kd_skpd+"",
			 success: function(response){
			 	
			 	$("#kd_jabatan").html(response);
		 
			 },
			 dataType:"html"
		 });
	}
	 return false;
 }

 function tampilUnitKerja(){

 	var jbselected = document.getElementById("jenis_jabatan").value;

	kd_unitorganisasi = document.getElementById("kd_unitorganisasi").value;
	if(kd_unitorganisasi !== ''){
		 $.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_unitkerja/"+kd_unitorganisasi+"",
			 success: function(response){
			 	$("#kd_unitkerja").html(response);
			 	$("#lay_kd_unitkerja").css('display', '');

				jQuery("#lay_kd_subunitkerja").css('display', 'none');


				if(jbselected == "Fungsional Umum"){

					jQuery("#lay_nama_jabatan").css('display', 'none');
					jQuery("#lay_auto_nama_jabatan").css('display', '');
					$('#kd_jabatan').removeAttr("required");
			 	}else if(jbselected == "Fungsional Tertentu"){

			 		jQuery("#lay_nama_jabatan").css('display', 'none');
					jQuery("#lay_auto_nama_jabatan").css('display', '');
					$('#kd_jabatan').removeAttr("required");
			 	}else{

			 		jQuery("#lay_nama_jabatan").css('display', '');
					jQuery("#lay_auto_nama_jabatan").css('display', 'none');
					$('#kd_jabatan').attr("required", "true");
					$('#nama_jabatan').removeAttr("required");
			 	}

				

			 },
			 dataType:"html"
		 });

		 $.ajax({
			 url:"<?php echo base_url();?>setup/riwayat_jabatan/get_jabatan_by_unitorganisasi/"+kd_unitorganisasi+"",
			 success: function(response){
			 	
			 	$("#kd_jabatan").html(response);
		 
			 },
			 dataType:"html"
		 });
	}
	 return false;
 }

 function tampilSubUnitKerja(){

	var jbselected = document.getElementById("jenis_jabatan").value;
	kd_unitkerja = document.getElementById("kd_unitkerja").value;
	if(kd_unitkerja !== ''){
		 $.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_subunitkerja/"+kd_unitkerja+"",
			 success: function(response){
			 	$("#kd_subunitkerja").html(response);
			 	if(document.getElementById("kd_skpd").value == '18' || document.getElementById("kd_skpd").value == '1'){
					$("#lay_kd_subunitkerja").css('display', '');
			 	}else{
			 		$("#lay_kd_subunitkerja").css('display', 'none');
			 	}


				if(jbselected == "Fungsional Umum"){

					jQuery("#lay_nama_jabatan").css('display', 'none');
					jQuery("#lay_auto_nama_jabatan").css('display', '');
					$('#kd_jabatan').removeAttr("required");
			 	}else if(jbselected == "Fungsional Tertentu"){

			 		jQuery("#lay_nama_jabatan").css('display', 'none');
					jQuery("#lay_auto_nama_jabatan").css('display', '');
					$('#kd_jabatan').removeAttr("required");
			 	}else{

			 		jQuery("#lay_nama_jabatan").css('display', '');
					jQuery("#lay_auto_nama_jabatan").css('display', 'none');
					$('#kd_jabatan').attr("required", "true");
					$('#nama_jabatan').removeAttr("required");
			 	}
			 	
			 },
			 dataType:"html"
		 });

		 $.ajax({
			 url:"<?php echo base_url();?>setup/riwayat_jabatan/get_jabatan_by_unitkerja/"+kd_unitkerja+"",
			 success: function(response){
			 	
			 	$("#kd_jabatan").html(response);
		 
			 },
			 dataType:"html"
		 });
	}
	 return false;
 }

 function tampilJabatan(){

	var jbselected = document.getElementById("jenis_jabatan").value;
	kd_subunitkerja = document.getElementById("kd_subunitkerja").value;

	if(kd_subunitkerja !== ''){
		 
		 $.ajax({
			 url:"<?php echo base_url();?>setup/riwayat_jabatan/get_jabatan_by_subunitkerja/"+kd_subunitkerja+"",
			 success: function(response){

			 	$("#kd_jabatan").html(response);

			 	if(jbselected == "Fungsional Umum"){

					jQuery("#lay_nama_jabatan").css('display', 'none');
					jQuery("#lay_auto_nama_jabatan").css('display', '');
					$('#kd_jabatan').removeAttr("required");
			 	}else if(jbselected == "Fungsional Tertentu"){

			 		jQuery("#lay_nama_jabatan").css('display', 'none');
					jQuery("#lay_auto_nama_jabatan").css('display', '');
					$('#kd_jabatan').removeAttr("required");
			 	}else{

			 		jQuery("#lay_nama_jabatan").css('display', '');
					jQuery("#lay_auto_nama_jabatan").css('display', 'none');
					$('#kd_jabatan').attr("required", "true");
					$('#nama_jabatan').removeAttr("required");
			 	}
		 
			 },
			 dataType:"html"
		 });
	}
	 return false;
}


$(document).ready(function($) {

	var base_url_riwayat = "<?php echo base_url()?>"+"setup/riwayat_jabatan/get_auto_namajabatan?";

    $('#nama_jabatan').typeahead({
    	minLength: 3,
    	autoSelect: false,
		source:  function (query, result) {
        	var url_get_jabatan = base_url_riwayat+"jenis_jabatan="+$('#jenis_jabatan').val()+"&kd_skpd="+$('#kd_skpd').val()+"&kd_unitorganisasi="+$('#kd_unitorganisasi').val()+"&kd_unitkerja="+$('#kd_unitkerja').val()+"&kd_subunitkerja="+$('#kd_subunitkerja').val()+"&s="+query; 

        	if($('#jenis_jabatan').val() !== 'Struktural'){

        		$.ajax({
				    url: url_get_jabatan,
				    method:"get",
				    dataType:"json",
				    success:function(data)
				    {
	            		return result(data);
				    }
		   		});	
        	}
        	
	    },
	    displayText: function (item) {
			return typeof item !== 'undefined' && typeof item.nama_jabatan != 'undefined' ? item.nama_jabatan : item;
		},
		afterSelect: function(item){

			$('#nama_jabatan_hiden').val(item.kd_jabatan);
		}

	});




});





   
</script>