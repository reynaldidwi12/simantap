<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data Unit Organisasi</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
			if($this->uri->segment(3)=="add"){
				echo form_open('setup/unit_organisasi/add');
			}else if($this->uri->segment(3)=="modify"){
				echo form_open('setup/unit_organisasi/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
			}else{
				echo form_open('setup/unit_organisasi/detail');
			}
			?>
				<div class="box-body">
					<?php echo form_input($kd_unitorganisasi) ?>
					<div class="col-lg-6">
						<div class="form-group">
							<label >OPD</label>
							<?php echo form_dropdown("kd_skpd", $skpd, $get_skpd,'class="form-control selectpicker" data-live-search="true" id="kd_skpd" data-live-search-style="begins" title="Pilih OPD" required');?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Nama Unit Organisasi</label>
							<?php echo form_input($nama_unitorganisasi)?>
						</div>
					</div>
					<?php echo form_input($kd_hiden)?>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('setup/unit_organisasi/index/'.$this->uri->segment(5), 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<script>
	
	$(document).ready(function($) {

	var base_url_riwayat = "<?php echo base_url()?>"+"setup/unit_organisasi/get_auto_nama?";

    $('#nama_unitorganisasi').typeahead({
    	minLength: 3,
    	autoSelect: false,
		source:  function (query, result) {

			$('#kd_hiden').val('');

        	var url_get = base_url_riwayat+"kd_skpd="+$('#kd_skpd').val()+"&s="+query; 

    		$.ajax({
			    url: url_get,
			    method:"get",
			    dataType:"json",
			    success:function(data)
			    {
            		return result(data);
			    }
	   		});	
        	
	    },
	    displayText: function (item) {
			return typeof item !== 'undefined' && typeof item.nama != 'undefined' ? item.nama : item;
		},
		afterSelect: function(item){
			$('#kd_hiden').val(item.kd_unitorganisasi);
		}

	});




});
</script>