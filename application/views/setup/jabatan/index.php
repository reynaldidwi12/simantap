<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> DATA JABATAN
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('setup/jabatan/add', 
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data Jabatan"><i class="fa fa-plus"></i> Tambah Jabatan</button>' );?>
					&nbsp;
					<?php echo anchor('setup/jabatan', 
					'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
					</div>
				</div>
				<?php echo form_open("setup/jabatan/find");?>
				<div class="col-md-7 col-sm-4 col-xs-4">
					<div class='col-md-5'>
						 <?php echo form_dropdown("kd_skpd", $skpd, $kd_skpd,'id="kd_skpd" class="form-control selectpicker" data-live-search="true" data-live-search-style="begins" onChange="setOpd()" title="Pilih OPD"');?>

					</div>
					<div class="col-md-7"><input type="text" class="form-control" name="data" placeholder="Cari" required="required"></div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:5%"><center>No</center></th>
				<th  style="width:20%">Nama Jabatan</th>
				<th  style="width:15%">Sub Unit Kerja</th>
				<th  style="width:15%">Unit Kerja</th>
				<th  style="width:15%">Unit Organisasi</th>
				<th  style="width:15%">OPD</th>
				<th  style="width:15%">Jenis jabatan</th>
				<th>&nbsp;&nbsp;</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($jabatan){
	    		$i=$number+1;
				foreach ( $jabatan as $row ) {
			?>
		        <tr class="gradeX">
		        <td ><center><?php echo $i?>.</center></td>
		        <td ><?php echo $row->nama_jabatan?></td>
		        <td ><?php echo $row->sub_unit_kerja?></td>
		        <td ><?php echo $row->unit_kerja?></td>
		        <td ><?php echo $row->unit_organisasi?></td>
		        <td ><?php echo $row->skpd?></td>
		        <td ><?php echo $row->jenis_jabatan?></td>
		        
		        <td ><center>
		        <a href="<?php echo base_url('setup/jabatan/modify/'.$row->kd_jabatan.'/'.$this->uri->segment(4)) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
		        &nbsp;
		        <a href="<?php echo base_url('setup/jabatan/remove/'.$row->kd_jabatan) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
				</center></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='4'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
