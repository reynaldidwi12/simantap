<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data jabatan</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
			if($this->uri->segment(3)=="add"){
				echo form_open('setup/jabatan/add');
			}else if($this->uri->segment(3)=="modify"){
				echo form_open('setup/jabatan/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
			}else{
				echo form_open('setup/jabatan/detail');
			}
			?>
				<div class="box-body">
					<div class="col-lg-8">
						<div class="form-group">
							<label >Jenis Jabatan</label>
							<?php $option = array(
									''=>'',
									'Struktural'=>'Struktural',
									'Fungsional Tertentu'=>'Fungsional Tertentu',
									'Fungsional Umum'=>'Fungsional Umum'
							); 
							echo form_dropdown($jenis_jabatan,$option,$get_jenis_jabatan,' onChange="filteringJenisJabatan()"');?>
						</div>
						
						<div class="form-group" id="lay_kd_skpd">
							<label>Nama OPD</label>
							<span id="unitorganisasi"></span>
							<?php echo form_dropdown("kd_skpd", $skpd, $get_skpd,'class="form-control selectpicker" data-live-search="true" id="kd_skpd" onChange="tampilUnitOrganisasi()" data-live-search-style="begins" title="Pilih OPD" required');?>
						</div>
						
						
						<div class="form-group" id="lay_kd_unitorganisasi">
							<label>Unit Organisasi</label>

							<?php echo form_dropdown("kd_unitorganisasi", $unitorganisasi,  $get_unitorganisasi,' class="form-control" id="kd_unitorganisasi" onChange="tampilUnitKerja()"  data-live-search="true" data-live-search-style="begins"');?>
						</div>
					
					
						<div class="form-group" id="lay_kd_unitkerja">
							<label>Unit Kerja</label>

							<?php echo form_dropdown("kd_unitkerja", $unitkerja,  $get_unitkerja,' class="form-control" id="kd_unitkerja" onChange="tampilSubUnitKerja()" data-live-search="true" data-live-search-style="begins"');?>
						</div>
					
					
						<div class="form-group" id="lay_kd_subunitkerja">
							<label >Sub Unit Kerja</label>
							<?php echo form_dropdown("kd_subunitkerja", $subunitkerja,  $get_subunitkerja,' class="form-control" id="kd_subunitkerja" onChange"tampilRekomPosisiSubUnitKerja()" data-live-search="true" data-live-search-style="begins"');?>
						</div>
						

						<div class="form-group" id="lay_nama_jabatan">
							<?php echo form_input($kd_jabatan)?>
							<?php echo form_input($nama_jabatan_hiden)?>
							<div class="form-group">
								<label >Nama Jabatan</label>
								<?php echo form_input($nama_jabatan)?>
							</div>
						</div>

					</div>
					<div class="col-lg-4">
							<div class="form-group">
								<label >Posisi Peta Jabatan</label>
								<?php echo form_input($position,'','onkeypress="return isNumber(event)"')?>
								<span id="rekomendasi_posisi"><em><small>*Jika letak pada peta jabatan sudah sesuai, tidak perlu mengubah posisi ini.</small></em></span>
							</div>
					</div>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('setup/jabatan/index/'.$this->uri->segment(5), 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>


<script>

var mykd_skpd = '<?php echo $this->data['kd_skpd']['value']; ?>';
var mykd_unitorganisasi = '<?php echo $this->data['kd_unitorganisasi']['value']; ?>';
var mykd_unitkerja = '<?php echo $this->data['kd_unitkerja']['value']; ?>';
var mykd_subunitkerja = '<?php echo $this->data['kd_subunitkerja']['value']; ?>';
var mynama_jabatan = '<?php echo $this->data['nama_jabatan']['value']; ?>';

if(mykd_skpd == ''){
	jQuery("#lay_kd_skpd").css('display', 'none');
}
if(mykd_unitorganisasi == ''){ 
	jQuery("#lay_kd_unitorganisasi").css('display', 'none');
}
if(mykd_unitkerja == ''){
	jQuery("#lay_kd_unitkerja").css('display', 'none');
}
if(mykd_subunitkerja == ''){
	jQuery("#lay_kd_subunitkerja").css('display', 'none');
}
if(mynama_jabatan == ''){
	jQuery("#lay_nama_jabatan").css('display', 'none');
}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function filteringJenisJabatan(){
	
	var jbselected = document.getElementById("jenis_jabatan").value;
	jQuery("#lay_kd_skpd").css('display', '');
	
	$('.selectpicker').selectpicker('val', '');

	
	jQuery('#kd_unitorganisasi').html('');
	jQuery('#kd_unitkerja').html('');
	jQuery('#kd_subunitkerja').html('');

/*

	if(jbselected == "Fungsional Tertentu"){
 	
		jQuery("#lay_kd_skpd").css('display', '');
	 	jQuery("#lay_kd_unitorganisasi").css('display', 'none');
	 	jQuery("#lay_kd_unitkerja").css('display', 'none');
		jQuery("#lay_kd_subunitkerja").css('display', 'none');
		jQuery("#lay_nama_jabatan").css('display', '');


		jQuery("#kd_unitorganisasi").html('');
		jQuery("#kd_unitkerja").html('');
		jQuery("#kd_subunitkerja").html('');


	}else{

		jQuery("#lay_kd_skpd").css('display', '');

		jQuery("#lay_kd_unitorganisasi").css('display', 'none');
		jQuery("#kd_unitorganisasi").html('');

		jQuery("#lay_kd_unitkerja").css('display', 'none');
		jQuery("#kd_unitkerja").html('');

		jQuery("#lay_kd_subunitkerja").css('display', 'none');
		jQuery("#kd_subunitkerja").html('');

		
		jQuery("#lay_nama_jabatan").css('display', 'none');
	}

*/
	jQuery("#lay_kd_skpd").css('display', '');

		jQuery("#lay_kd_unitorganisasi").css('display', 'none');
		jQuery("#kd_unitorganisasi").html('');

		jQuery("#lay_kd_unitkerja").css('display', 'none');
		jQuery("#kd_unitkerja").html('');

		jQuery("#lay_kd_subunitkerja").css('display', 'none');
		jQuery("#kd_subunitkerja").html('');

		
		jQuery("#lay_nama_jabatan").css('display', 'none');


	if(mynama_jabatan == ''){
		jQuery("#lay_nama_jabatan").css('display', 'none');
	}else{
		jQuery("#lay_nama_jabatan").css('display', '');
	}



	return false;
}

   
function tampilUnitOrganisasi(){
	var jbselected = document.getElementById("jenis_jabatan").value;
	kd_skpd = document.getElementById("kd_skpd").value;
	
	if(kd_skpd !== ''){
		$.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_unitorganisasi/"+kd_skpd+"",
			 success: function(response){

/*
			 	if(jbselected == "Fungsional Tertentu"){
			 		
				 	jQuery("#lay_kd_unitorganisasi").css('display', 'none');
				 	jQuery("#lay_kd_unitkerja").css('display', 'none');
					jQuery("#lay_kd_subunitkerja").css('display', 'none');
					jQuery("#lay_nama_jabatan").css('display', '');

					$("#lay_kd_skpd").css('display', '');

					$("#kd_unitorganisasi").css('display', 'none');
					$("#kd_unitkerja").css('display', 'none');
					$("#kd_subunitkerja").css('display', 'none');

			 	}else{
			 		$("#kd_unitorganisasi").html(response);
				 	$("#lay_kd_unitorganisasi").css('display', '');

				 	jQuery("#lay_kd_unitkerja").css('display', 'none');
					jQuery("#lay_kd_subunitkerja").css('display', 'none');
					jQuery("#lay_nama_jabatan").css('display', '');

					$("#kd_unitorganisasi").css('display', '');
					$("#kd_unitkerja").css('display', '');
					$("#kd_subunitkerja").css('display', '');
			 	}
*/


			 	$("#kd_unitorganisasi").html(response);
				 	$("#lay_kd_unitorganisasi").css('display', '');

				 	jQuery("#lay_kd_unitkerja").css('display', 'none');
					jQuery("#lay_kd_subunitkerja").css('display', 'none');
					jQuery("#lay_nama_jabatan").css('display', '');

					$("#kd_unitorganisasi").css('display', '');
					$("#kd_unitkerja").css('display', '');
					$("#kd_subunitkerja").css('display', '');




			 	/*
		 		if(mynama_jabatan == ''){
					jQuery("#lay_nama_jabatan").css('display', 'none');
				}else{
					jQuery("#lay_nama_jabatan").css('display', '');
				}
				*/

			 },
			 dataType:"html"
		 });

		$.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_rekom_position?kd_skpd="+kd_skpd+"",
			 success: function(response){

			 	if(response.position != null && response.position != '-1'){
			 		msg = "<em><small>*rekomendasi posisi: <strong>"+response.position+"</strong>. Jika letak pada peta jabatan sudah sesuai, tidak perlu mengikuti rekomendasi ini.</small></em>";
			 	}else{
					msg = "<em><small>*sistem tidak memiliki rekomendasi posisi untuk jabatan ini. Silahkan menyesuaikan atau mengikuti posisi jabatan yang setara.</small></em>";
			 	}

			 	$("#rekomendasi_posisi").html(msg);

			 },
			 dataType:"json"
		 });
	}
	 return false;
 }

 function tampilUnitKerja(){

	kd_unitorganisasi = document.getElementById("kd_unitorganisasi").value;
	if(kd_unitorganisasi !== ''){
		 $.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_unitkerja/"+kd_unitorganisasi+"",
			 success: function(response){
			 	$("#kd_unitkerja").html(response);
			 	$("#lay_kd_unitkerja").css('display', '');

				jQuery("#lay_kd_subunitkerja").css('display', 'none');
				jQuery("#lay_nama_jabatan").css('display', '');

			 },
			 dataType:"html"
		 });

		 $.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_rekom_position?kd_unitorganisasi="+kd_unitorganisasi+"",
			 success: function(response){

			 	if(response.position != null && response.position != '-1'){
			 		msg = "<em><small>*rekomendasi posisi: <strong>"+response.position+"</strong>. Jika letak pada peta jabatan sudah sesuai, tidak perlu mengikuti rekomendasi ini.</small></em>";
			 	}else{
					msg = "<em><small>*sistem tidak memiliki rekomendasi posisi untuk jabatan ini. Silahkan menyesuaikan atau mengikuti posisi jabatan yang setara.</small></em>";
			 	}

			 	$("#rekomendasi_posisi").html(msg);

			 },
			 dataType:"json"
		 });

	}
	 return false;
 }

 function tampilSubUnitKerja(){

	kd_unitkerja = document.getElementById("kd_unitkerja").value;
	if(kd_unitkerja !== ''){
		 $.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_subunitkerja/"+kd_unitkerja+"",
			 success: function(response){
			 	$("#kd_subunitkerja").html(response);
			 	if(document.getElementById("kd_skpd").value == '18' || document.getElementById("kd_skpd").value == '1'){
					$("#lay_kd_subunitkerja").css('display', '');
			 	}else{
			 		$("#lay_kd_subunitkerja").css('display', 'none');
			 	}

			 	$("#lay_nama_jabatan").css('display', '');
			 	
			 },
			 dataType:"html"
		 });

		 $.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_rekom_position?kd_unitkerja="+kd_unitkerja+"",
			 success: function(response){

			 	if(response.position != null && response.position != '-1'){
			 		msg = "<em><small>*rekomendasi posisi: <strong>"+response.position+"</strong>. Jika letak pada peta jabatan sudah sesuai, tidak perlu mengikuti rekomendasi ini.</small></em>";
			 	}else{
					msg = "<em><small>*sistem tidak memiliki rekomendasi posisi untuk jabatan ini. Silahkan menyesuaikan atau mengikuti posisi jabatan yang setara.</small></em>";
			 	}

			 	$("#rekomendasi_posisi").html(msg);

			 },
			 dataType:"json"
		 });
	}
	 return false;
 }


function tampilRekomPosisiSubUnitKerja(){

	kd_subunitkerja = document.getElementById("kd_subunitkerja").value;
	if(kd_subunitkerja !== ''){

		 $.ajax({
			 url:"<?php echo base_url();?>setup/jabatan/get_rekom_position?kd_subunitkerja="+kd_subunitkerja+"",
			 success: function(response){

			 	if(response.position != null && response.position != '-1'){
			 		msg = "<em><small>*rekomendasi posisi: <strong>"+response.position+"</strong>. Jika letak pada peta jabatan sudah sesuai, tidak perlu mengikuti rekomendasi ini.</small></em>";
			 	}else{
					msg = "<em><small>*sistem tidak memiliki rekomendasi posisi untuk jabatan ini. Silahkan menyesuaikan atau mengikuti posisi jabatan yang setara.</small></em>";
			 	}

			 	$("#rekomendasi_posisi").html(msg);

			 },
			 dataType:"json"
		 });
	}
	 return false;
 }


 $(document).ready(function($) {

	var base_url_riwayat = "<?php echo base_url()?>"+"setup/jabatan/get_auto_namajabatan?";

    $('#nama_jabatan').typeahead({
    	minLength: 3,
    	autoSelect: false,
		source:  function (query, result) {

			$('#nama_jabatan_hiden').val('');

        	var url_get_jabatan = base_url_riwayat+"jenis_jabatan="+$('#jenis_jabatan').val()+"&kd_skpd="+$('#kd_skpd').val()+"&kd_unitorganisasi="+$('#kd_unitorganisasi').val()+"&kd_unitkerja="+$('#kd_unitkerja').val()+"&kd_subunitkerja="+$('#kd_subunitkerja').val()+"&s="+query; 

        	if($('#jenis_jabatan').val() !== 'Struktural'){

        		$.ajax({
				    url: url_get_jabatan,
				    method:"get",
				    dataType:"json",
				    success:function(data)
				    {
	            		return result(data);
				    }
		   		});	
        	}
        	
	    },
	    displayText: function (item) {
			return typeof item !== 'undefined' && typeof item.nama_jabatan != 'undefined' ? item.nama_jabatan : item;
		},
		afterSelect: function(item){

			$('#nama_jabatan_hiden').val(item.kd_jabatan);
		}

	});




});

</script>