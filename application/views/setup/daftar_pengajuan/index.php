<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> DAFTAR BERKAS PENGAJUAN
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('setup/daftar_pengajuan/add', 
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Daftar Berkas Pengajuan"><i class="fa fa-plus"></i> Tambah Daftar Berkas Pengajuan</button>' );?>
					&nbsp;
					<?php echo anchor('setup/daftar_pengajuan', 
					'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
					</div>
				</div>
				<?php echo form_open("setup/daftar_pengajuan/find");?>
				<div class="col-md-5 col-sm-4 col-xs-4">
					<div class='col-md-5'>
					<?php $option = array(
							'nama_berkas'=>'Nama Berkas',
					); 
					echo form_dropdown('column',$option,'1','class="form-control"');?>
					</div>
					<div class="col-md-7"><input type="text" class="form-control" name="data" placeholder="Cari"></div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:5%"><center>No</center></th>
				<th style="width:5%">No. Urut</th>
				<th style="width:30%">Nama Berkas</th>
				<th style="width:30%">Nama File</th>
				<th style="width:10%">Kategori</th>
				<th style="width:10%">Sub Kategori</th>
				<th style="width:10%">&nbsp;&nbsp;</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($daftar_pengajuan){
	    		$i=$number+1;
				foreach ( $daftar_pengajuan as $row ) {
			?>
		        <tr class="gradeX">
		        <td ><center><?php echo $i?>.</center></td>
		        <td ><?php echo $row->no_urut?></td>
				<td ><?php echo $row->nama_berkas?></td>
				<td ><?php echo $row->nama_file?></td>
				<td >
					<?php echo $row->kategori?>
				</td>
				<td><?php echo $row->sub_kategori?></td>
		        <td ><center>
		        <a href="<?php echo base_url('setup/daftar_pengajuan/modify/'.$row->id_daftar.'/'.$this->uri->segment(4)) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
		        &nbsp;
		        <a href="<?php echo base_url('setup/daftar_pengajuan/remove/'.$row->id_daftar) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
				</center></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='6'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
