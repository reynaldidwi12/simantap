<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Daftar Berkas Pengajuan</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
			if($this->uri->segment(3)=="add"){
				echo form_open('setup/daftar_pengajuan/add');
			}else if($this->uri->segment(3)=="modify"){
				echo form_open('setup/daftar_pengajuan/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
			}else{
				echo form_open('setup/daftar_pengajuan/detail');
			}
			?>
				<div class="box-body">
					<div class="col-lg-6">
						<div class="form-group">
							<label >No. Urut</label>
							<?php echo form_input($no_urut)?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Nama Berkas</label>
							<?php echo form_input($nama_berkas)?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Nama File</label>
							<?php echo form_input($nama_file)?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Kategori</label>
							<?php $option = array(
									''=>'Pilih Kategori',
									'kenaikan_pangkat'=>'Kenaikan Pangkat',
									'pensiun'=>'Pensiun',
									'kgb'=>'KGB'
							); 
							echo form_dropdown($kategori,$option,$get_category,'class="form-control selectpicker" data-live-search="true" required');?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Sub Kategori</label>
							<?php $option = array(
									''=>'Pilih Sub Kategori',
									'reguler'=>'Reguler',
									'pilihan'=>'Pilihan',
									'jft'=>'JFT',
									'bup'=>'BUP',
									'aps'=>'APS',
									'janda_duda'=>'Janda/Duda',
									'naik_pangkat'=>'Naik Pangkat',
									'lanjutan'=>'Lanjutan'
							); 
							echo form_dropdown($sub_kategori,$option,$get_sub_category,'class="form-control selectpicker" data-live-search="true" required');?>
						</div>
					</div>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('setup/daftar_pengajuan/index/'.$this->uri->segment(5), 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>