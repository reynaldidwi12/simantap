<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Data Pegawai Yang Pindah Tugas (Luar)
		</h3>
	</div>
	<!-- /.panel-heading -->
	<!--div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('report/report_pensiun/add', 
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="fa fa-print"></i> Cetak</button>' );?>
					</div>
				</div>
			</div>
		</div>
	</div-->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					
				</div>
				<?php if($this->session->userdata('group_id')<>'3') { ?>
				<?php echo form_open("setup/pensiun/find");?>
				<div class="col-md-5 col-sm-4 col-xs-4">
					<div class='col-md-2'>
					</div>
					<div class="col-md-10"><?php echo form_dropdown("nama_skpd", $nama_skpd, "",'id="kd_skpd" class="form-control selectpicker" onChange="tampilUnitkerja()" data-live-search="true" data-live-search-style="begins" title="Pilih SKPD" required');?></div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
			<?php } ?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:5%"><center>No</th>
				<th style="width:10%"><center>Foto</th>
				<th style="width:15%"><center>NIP Baru</th>
				<th style="width:20%"><center>Nama</th>
				<th style="width:25%"><center>TTL</th>
				<th style="width:8%"><center>Jenis Kelamin</th>
				
				<th style="width:25%"><center>Pilihan</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($pegawai){
	    		$i = $number + 1;
				foreach ( $pegawai as $row ) {
				
				if($row->status_pegawai=="Pindah Tugas"){
					$a = "#fbf8c9";
				}else if($row->status_pegawai=="Meninggal"){
					$a = "#ffd3d3";
				}else{
					$a = "";
				}

				if($row->tgl_lahir){
					$TglLahir = date("d-m-Y",strtotime($row->tgl_lahir));							
				}else{}				
			?>
		        <tr class="gradeX" >
		        <td bgcolor=<?php echo $a;?> ><?php echo $i?></td>
		        <td bgcolor=<?php echo $a;?> ><center>
					<?php if($row->picture){?>
								<img src='<?php echo base_url();?>files/foto_pegawai/<?php echo $row->picture;?>' width="110" height="150"/>	
							<?php 
					}else{ ?>
								<img src='<?php echo base_url();?>files/foto_pegawai/image.png' width="110" height="150"/>	
					<?php } ?>
					</center>
				</td>
		        <td bgcolor=<?php echo $a;?>><?php echo $row->nip?></td>
		        <td bgcolor=<?php echo $a;?>><?php echo $row->gelar_depan.' '.$row->nama.' '.$row->gelar_belakang?></td>
		        <td bgcolor=<?php echo $a;?>><?php echo $row->tempat_lahir.", ".$TglLahir?></td>
		        <td bgcolor=<?php echo $a;?>><?php echo $row->kelamin?></td>
		        <td  align="center" bgcolor=<?php echo $a;?>>
				<a href="<?php echo base_url('setup/pegawai/kembalikan_data/'.$row->nip) ?>"><button class="btn btn-success " title="Kembalikan Pegawai">Kembalikan Data<!--i class="fa fa-print"--></i></button></a><br>
				<a href="<?php echo base_url('setup/pegawai/report_rh/'.$row->nip) ?>"><button class="btn btn-primary " title="Cetak Riwayat Pegawai">Cetak Data<!--i class="fa fa-print"--></i></button></a><br>
				<a href="<?php echo base_url('setup/pegawai/remove/'.$row->nip) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger" title="Delete">Hapus Data<!--i class="fa fa-remove"--></i></button></a>
				</td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='7'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
