<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data Unit Kerja</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
			if($this->uri->segment(3)=="add"){
				echo form_open('setup/unit_kerja/add');
			}else if($this->uri->segment(3)=="modify"){
				echo form_open('setup/unit_kerja/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
			}else{
				echo form_open('setup/unit_kerja/detail');
			}
			?>
				<div class="box-body">
					<?php echo form_input($kd_unitkerja)?>
					<div class="col-lg-6">
						<div class="form-group">
							<label >OPD</label>
							<span id="unitorganisasi"></span>
							<?php echo form_dropdown("kd_skpd", $skpd, $get_skpd,'class="form-control selectpicker" data-live-search="true" id="kd_skpd" onChange="tampilUnitOrganisasi()" data-live-search-style="begins" title="Pilih OPD" required');?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Unit Organisasi</label>

							<?php echo form_dropdown("kd_unitorganisasi", $unitorganisasi,  $get_unitorganisasi,' class="form-control" id="kd_unitorganisasi" data-live-search="true" data-live-search-style="begins" required');?>
						</div>
					</div>
					<?php echo form_input($kd_hiden)?>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Nama Unit Kerja</label>
							<?php echo form_input($nama_unitkerja)?>
						</div>
					</div>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('setup/unit_kerja/index/'.$this->uri->segment(5), 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<script>
   
function tampilUnitOrganisasi(){

	 kd_skpd = document.getElementById("kd_skpd").value;
	 $.ajax({
		 url:"<?php echo base_url();?>setup/unit_kerja/get_unitorganisasi/"+kd_skpd+"",
		 success: function(response){
		 	$("#kd_unitorganisasi").html(response);
		 },
		 dataType:"html"
	 });
	 return false;
 }



 $(document).ready(function($) {

	var base_url_riwayat = "<?php echo base_url()?>"+"setup/unit_kerja/get_auto_nama?";

    $('#nama_unitkerja').typeahead({
    	minLength: 3,
    	autoSelect: false,
		source:  function (query, result) {

			$('#kd_hiden').val('');
        	var url_get = base_url_riwayat+"kd_skpd="+$('#kd_skpd').val()+"&kd_unitorganisasi="+$('#kd_unitorganisasi').val()+"&s="+query; 

        		$.ajax({
				    url: url_get,
				    method:"get",
				    dataType:"json",
				    success:function(data)
				    {
	            		return result(data);
				    }
		   		});	
        	
	    },
	    displayText: function (item) {
			return typeof item !== 'undefined' && typeof item.nama != 'undefined' ? item.nama : item;
		},
		afterSelect: function(item){

			$('#kd_hiden').val(item.kd_unitkerja);
		}

	});

});
</script>