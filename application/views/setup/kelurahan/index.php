<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> DATA KELURAHAN
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('setup/kelurahan/add', 
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data Kelurahan"><i class="fa fa-plus"></i> Tambah Kelurahan</button>' );?>
					&nbsp;
					<?php echo anchor('setup/kelurahan', 
					'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
					</div>
				</div>
				<?php echo form_open("setup/kelurahan/find");?>
				<div class="col-md-5 col-sm-4 col-xs-4">
					<div class='col-md-5'>
					<?php $option = array(
							'kel_nama'=>'Nama Kelurahan',
							'kec_nama'=>'Nama Kecamatan'
					); 
					echo form_dropdown('column',$option,'1','class="form-control"');?>
					</div>
					<div class="col-md-7"><input type="text" class="form-control" name="data" placeholder="Cari"></div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:5%"><center>No</center></th>
				<th style="width:42%">Nama Kelurahan</th>
				<th style="width:43%">Nama Kecamatan</th>
				<th>&nbsp;&nbsp;</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($kelurahan){
	    		$i=1;
				foreach ( $kelurahan as $row ) {
			?>
		        <tr class="gradeX">
		        <td ><center><?php echo $i?>.</center></td>
		        <td ><?php echo $row->kel_nama?></td>
		        <td ><?php echo $row->kec_nama?></td>
		        <td ><center>
		        <a href="<?php echo base_url('setup/kelurahan/modify/'.$row->kel_id) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
		        &nbsp;
		        <a href="<?php echo base_url('setup/kelurahan/remove/'.$row->kel_id) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
				</center></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='4'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
