<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> DATA SUB UNIT KERJA
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('setup/sub_unit_kerja/add', 
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data Sub Unit Kerja"><i class="fa fa-plus"></i> Tambah Sub Unit Kerja</button>' );?>
					&nbsp;
					<?php echo anchor('setup/sub_unit_kerja', 
					'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
					</div>
				</div>
				<?php echo form_open("setup/sub_unit_kerja/find");?>
				<div class="col-md-5 col-sm-4 col-xs-4">
					<div class='col-md-5'>
					<?php $option = array(
							'a.nama'=>'OPD',
							'c.nama'=>'Unit Organisasi',
							'b.nama'=>'Unit Kerja',
							'd.nama'=>'Sub Unit Kerja'
					); 
					echo form_dropdown('column',$option,'1','class="form-control"');?>
					</div>
					<div class="col-md-7"><input type="text" class="form-control" name="data" placeholder="Cari"></div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:5%"><center>No</center></th>
				<th  style="width:20%">Nama Sub Unit Kerja</th>
				<th  style="width:25%">Nama Unit Kerja</th>
				<th  style="width:25%">Nama Sub Unit Organisasi</th>
				<th  style="width:25%">Nama OPD</th>
				<th>&nbsp;&nbsp;</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($sub_unit_kerja){
	    		$i=$number+1;
				foreach ( $sub_unit_kerja as $row ) {
			?>
		        <tr class="gradeX">
		        <td ><center><?php echo $i?>.</center></td>
		        <td ><?php echo $row->nama_sub_unit_kerja?></td>
		        <td ><?php echo $row->nama_unit_kerja?></td>
		        <td ><?php echo $row->nama_unit_organisasi?></td>
		        <td ><?php echo $row->nama_skpd?></td>
		        <td ><center>
		        <a href="<?php echo base_url('setup/sub_unit_kerja/modify/'.$row->kd_subunitkerja.'/'.$this->uri->segment(4)) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
		        &nbsp;
		        <a href="<?php echo base_url('setup/sub_unit_kerja/remove/'.$row->kd_subunitkerja) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
				</center></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='4'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<?php if(count($sub_unit_kerja)>0):?>
				<div align="left">Menampilkan data <?php echo $number+1 ?> sampai <?php echo $number+count($sub_unit_kerja)?> dari <?php echo $total_rows?> records</div>
			<?php endif; ?>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
