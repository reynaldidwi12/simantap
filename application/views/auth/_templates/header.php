<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!DOCTYPE HTML>
<html lang="<?php echo $lang; ?>">
<head>
<title><?php echo $title; ?></title>
<!-- Custom Theme files -->
<link href="<?php echo base_url($frameworks_dir . '/simantap/css/style.css'); ?>" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="keywords" content="Static Login Form Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<!--script-->
<script src="<?php echo base_url($frameworks_dir . '/simantap/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/simantap/js/easyResponsiveTabs.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
          $(document).ready(function () {
              $('#horizontalTab').easyResponsiveTabs({
                  type: 'default', //Types: default, vertical, accordion           
                  width: 'auto', //auto or any width like 600px
                  fit: true   // 100% fit in a container
              });
          });
        
</script> 
<!--script-->

</head>
<body>
