<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

  <div class="head">
    <div class="logo">
      <div class="logo-top">
        <center><img src="<?php echo base_url($frameworks_dir . '/simantap/images/Kota_Kendari.png'); ?>" alt="Smiley face" height="180" width="200"></center>
        <h1>SIMANTAP <p>KOTA KENDARI</h1>
        <br>
        <h6>Sistem Informasi Manajemen Data Kepegawaian</h6>
      </div>
    </div>    
    <div class="login">
      <div class="sap_tabs">
        <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
          <ul class="resp-tabs-list">
            <li class="resp-tab-active" aria-controls="tab_item-0" role="tab"><span><?php echo lang('auth_sign_session'); ?></span></li>
            <?php echo $message;?>
            <div class="clearfix"></div>
            </ul> 
          <br>
          <br>           
          
                <?php echo form_open('auth/login');?>
          
              <div class="resp-tabs-container">
              <div class="login-top">
                 
                  <div class="name"><?php echo form_input($identity);?></div>
                  
                  <div class="password"><?php echo form_input($password);?></div>
                    <br>   

                  <label>
                   <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?><?php echo lang('auth_remember_me'); ?>
                   </label>
                  <div class="login-bottom login-bottom1">
                    <div class="submit">
                     <?php echo form_submit('submit', lang('auth_login'), array('class' => 'submit'));?>
                     </div>
                      <ul>
                        <li><p>Follow Us</p></li>
                        <li><a href="https://www.facebook.com/groups/1201121363237817/"><span class="fb"></span></a></li>
                        <li><a href="#"><span class="twit"></span></a></li>
                        <li><a href="#"><span class="google"></span></a></li>
                      </ul>
                        <div class="clear"></div>
                  </div>  
              </div>

                          
          </div> 
        </div>
      </div>  
    </div>  
    <div class="clear"></div>
  </div>  