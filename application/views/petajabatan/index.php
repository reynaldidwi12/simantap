<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>
            <i class="fa fa-university"></i> PETA JABATAN
        </h3>
    </div>

    <!-- /.panel-heading -->

            <div class="panel-body">
                    <div class="col-sm-12">
                        <div class=row>
                            <div class="col-md-6 col-sm-4 col-xs-4">
                            
                                <?php echo anchor('petajabatan', 
                                '<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
                                &nbsp;
                                 
                                <button type="button" class="btn btn-info" data-placement="top" title="Pencarian" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i> Pencarian</button>
                                </div>
                            
                            <?php if($this->session->userdata('group_id')<>'3') { ?>
                            <div class="col-md-5 col-sm-4 col-xs-4">
                                <div class='col-md-2'>
                                </div>
                                <div class="col-md-10">
                                    <?php echo form_dropdown("kd_skpd", $skpd, "",'id="kd_skpd" class="form-control selectpicker" data-live-search="true" data-live-search-style="begins" onChange="setOpd()" title="Pilih OPD" required');?>
                                        
                                </div>
                            </div>
                            <div class="btn-group">
                                <a href="#" name="submit" id="btn_lihat_bagan" class="btn btn-success" title="Lihat Bagan">Bagan</a>
                            </div>
                          <?php } else { ?>
                            <div class="col-md-5 col-sm-4 col-xs-4">
                            </div>
                            <div class="btn-group">
                                <a href="<?php echo base_url().'petajabatan/bagan/'.$this->session->userdata('ss_skpd'); ?>" name="submit" class="btn btn-success" title="Lihat Bagan">Bagan</a>
                            </div>
                          <?php } ?>
                        </div>
                    </div>
            </div>

            <hr>

                <div class="panel-body">
                    <div class="box-body">
                        <div id="datadb" class="demo"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Modal Search -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pecarian Pegawai</h4>
        </div>
        <div class="modal-body">
                <?php echo form_open("petajabatan/petajabatan/find_pegawai",'id="searchForm"');?>
                    <div class="col-md-10 col-sm-5 col-xs-5">
                        <div class='col-md-5'>
                        <?php $option = array(
                                'nip'=>'NIP Baru',
                                'nama'=>'Nama'
                        ); 
                        echo form_dropdown('column',$option,'1','class="form-control"');?>
                        </div>
                        <div class="col-md-7"><input type="text" class="form-control" name="data" placeholder="Cari" required=""></div>
                    </div>
                    <div class="btn-group">
                        <input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
                    </div>
                <?php echo form_close();?>

                <hr/>

                <div id="list_pegawai" class="list-group">
                 
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- End Modal -->



<!-- Modal Info -->
  <div class="modal fade" id="myModalInfo" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pecarian Pegawai</h4>
        </div>
        <div class="modal-body">
               <?php if($this->session->flashdata('msg_search') != NULL): ?>
                <?php echo $this->session->flashdata('msg_search'); ?>
               <?php endif; ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- End Modal -->


<!-- Modal Pegawai-->
  <div class="modal fade" id="myModalPegawai" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Info Singkat Pegawai</h4>
        </div>
        <div class="modal-body">
                
            <table class="table">
              <tbody>
                <tr>
                  <td class="col-3">Nama</td>
                  <td>:</td>
                  <td id="pgw_nama"></td>
                </tr>
                <tr>
                  <td class="col-3">NIP</td>
                  <td>:</td>
                  <td id="pgw_nip"></td>
                </tr>
                <tr>
                  <td class="col-3">Jabatan</td>
                  <td>:</td>
                  <td id="pgw_jabatan"></td>
                </tr>
                <tr>
                  <td class="col-3">Pangkat</td>
                  <td>:</td>
                  <td id="pgw_pangkat"></td>
                </tr>
              </tbody>
            </table>

        </div>
        <div class="modal-footer">
            <a id="btn_detail_pgw" class="btn btn-success">Info Detail</a>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

        </div>
      </div>
      
    </div>
  </div>

<!-- End Modal -->

</div>
			

<script>

$('#datadb').jstree({
    'core' : {
        'data' : {
            "url" : "<?php echo base_url()?>"+"/petajabatan/petajabatan/get_data_default?",
            "data" : function (node) {
                return { "kd_jabatan" : node.id };
            }
        }
    },
    'types' : {
      'default' : {
        'icon' : 'jstree-folder'
      },
      'user' : {
        'icon' : 'glyphicon glyphicon-user'
      }
    },
    'plugins' : [ 'types' ]
});


function setOpd(){
    var kd_skpd = $('#kd_skpd').val();

    if(kd_skpd != ''){
    
        var url = "<?php echo base_url()?>"+"petajabatan/bagan/"+kd_skpd;
        
        $('#btn_lihat_bagan').attr('target','_blank').attr('href',url); 
    }
}


$("#searchForm" ).submit(function( event ) {

      event.preventDefault();
      $(".list-group").html("");
     
      var $form = $( this ),
        column_ = $form.find( "select[name='column']" ).val(),
        data_ = $form.find( "input[name='data']" ).val(),
        submit_ = $form.find( "input[name='submit']" ).val(),
        url = $form.attr( "action" );
     
      var posting = $.post( url, { column: column_, data: data_ , submit: submit_} );
     
      posting.done(function( response ) {

        var content = '';
        for (var x = 0; x < response.length; x++) {
            var url = "<?php echo base_url()?>"+"petajabatan/find/"+response[x].nip;
            content += '<a href="'+url+'" id="item-pegawai" data-nip="'+response[x].nip+'" class="list-group-item list-group-item-action">'+response[x].gelar_depan+' '+response[x].nama +' '+response[x].gelar_belakang+' ('+response[x].nip+')'+'</a>';                
        }

        $(content).appendTo("#list_pegawai");

      });

});

/*
    Clear value when modal dissmised
*/

$('#myModal').on('hidden.bs.modal', function() {
    $("input[name='data']", this).val('');
    $(".list-group").html("");
});

    
$(document).ready(function($) {

    var msg_search = '<?php if($this->session->flashdata('msg_search') != NULL) echo $this->session->flashdata('msg_search'); ?>';

    if(msg_search != ''){    
        $('#myModalInfo').modal('show');
    }

    $('#datadb').on("changed.jstree", function (e, data) {
        
        if(data.node != null){

            if(data.node.original.nip != null){

                var nama = data.node.original.gelar_depan +' '+ data.node.original.nama+ ' '+data.node.original.gelar_belakang;
                $('#pgw_nama').text(nama);
                $('#pgw_nip').text(data.node.original.nip);
                $('#pgw_jabatan').text(data.node.original.nama_jabatan);
                $('#pgw_pangkat').text(data.node.original.golongan);

                var url = "<?php echo base_url()?>"+"setup/pegawai/find/"+data.node.original.nip+"/nip";
                $('#btn_detail_pgw').attr('href',url);
                
                $('#myModalPegawai').modal('show');
            }
        }
    });

});

</script>

			
