<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><i class="fa fa-university"></i> <?php if($skpd->nama != NULL) {echo ucwords(strtolower($skpd->nama));} ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="text-warning" href="<?php echo base_url().'petajabatan/bagan/'.$kd_skpd; ?>"><i class="fa fa-refresh"></i> Refresh</a></li>
            <li><a class="text-default" id="button_filter" data-toggle="modal" data-target="#myModal" href="#"><i class="fa fa-filter"></i> Filter</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

 <!-- End Nav -->

 <div class="container">
    
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-solid box-default">

                <div class="panel-body">
                    <div class="box-body">
                        <div id="chart-container"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 </div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Filter Bagan</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group" id="lay_tingkat_jabatan">
            <label for="tingkat_jabatan" class="col-form-label">Tingkatan jabatan yang akan ditampilkan :</label>
            <select id="tingkat_jabatan" name="tingkat_jabatan" class="form-control" onChange="tampilUnitOrganisasi()">
              <option value="">-PILIH-</option>
              <option value="unit_organisasi">Tingkat Unit Organisasi</option>
              <option value="unit_kerja">Tingkat Unit Kerja</option>
            </select>
          </div>
          <div class="form-group" id="lay_kd_unitorganisasi">
            <label for="kd_unitorganisasi" class="col-form-label">Unit organisasi :</label>
            <?php echo form_dropdown("kd_unitorganisasi", $get_unitorganisasi, '',' class="form-control" id="kd_unitorganisasi" onChange="tampilUnitKerja()"');?>
          </div>
          <div class="form-group" id="lay_kd_unitkerja">
            <label for="kd_unitkerja" class="col-form-label">Unit kerja :</label>
            <select id="kd_unitkerja" name="kd_unitkerja" class="form-control" onChange="tampilSubUnitKerja()">
            </select>
          </div>
          
          <div class="form-group" id="lay_blacklist">
            <label for="blacklist" class="col-form-label">Blacklist berdasarkan kata:</label>
            <input type="text" id="blacklist" name="blacklist" class="form-control" id="recipient-name">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="requestNewBagan()">Tampilkan</button>
      </div>
    </div>
  </div>
</div>



<!-- <script type="text/javascript" src="<?php // echo base_url($frameworks_dir .'/orgchart/js/jquery.min.js');?>"></script> -->

<script type="text/javascript" src="<?php echo base_url($frameworks_dir .'/orgchart/js/html2canvas.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url($frameworks_dir .'/orgchart/js/jspdf.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url($frameworks_dir .'/orgchart/js/jquery.orgchart.js');?>"></script>
<script>


var kd_skpd = "<?php echo $this->uri->segment(3);?>";

var nama_skpd = "<?php echo $this->lbr->getSingleSkpd($this->uri->segment(3))->nama;?>";

var mykd_unitorganisasi = '';
var mykd_unitkerja = '';

var mychart = $('#chart-container');



  function tampilUnitOrganisasi(){

    tingkat_jabatan = document.getElementById("tingkat_jabatan").value;

    if(tingkat_jabatan !== ''){
      $('#lay_kd_unitorganisasi').css('display','');
    }else{
      $('#lay_kd_unitorganisasi').css('display','none');
      $('#lay_kd_unitkerja').css('display','none');
    }

    $("#kd_unitorganisasi").val('');
    $("#kd_unitkerja").val('');

    return false;
   }


  function tampilUnitKerja(){

    tingkat_jabatan = document.getElementById("tingkat_jabatan").value;
    kd_unitorganisasi = document.getElementById("kd_unitorganisasi").value;
    mykd_unitorganisasi = kd_unitorganisasi;


    if(kd_unitorganisasi !== '' && tingkat_jabatan === 'unit_kerja'){

       $.ajax({
         url:"<?php echo base_url();?>setup/jabatan/get_unitkerja/"+kd_unitorganisasi+"",
         success: function(response){
          $("#kd_unitkerja").html(response);
          $("#lay_kd_unitkerja").css('display', '');

         },
         dataType:"html"
       });

    }

   return false;
 }

 function tampilSubUnitKerja(){

  kd_unitkerja = document.getElementById("kd_unitkerja").value;
  mykd_unitkerja = kd_unitkerja;
  if(kd_unitkerja !== ''){

  }
   return false;
 }


 function requestNewBagan(){
  
  myblacklist = document.getElementById("blacklist").value;
  console.log(myblacklist);

  myurl = "<?php echo base_url();?>petajabatan/petajabatan/get_filtering_bagan?kd_skpd="+kd_skpd+"&kd_unitorganisasi="+mykd_unitorganisasi+"&kd_unitkerja="+mykd_unitkerja+"&blacklist="+myblacklist;

  $.ajax({
      url:myurl,
      success: function(response){
        mychart.empty();
        
        mychart.orgchart({
            'data' : response,
            'visibleLevel': 6,
            /*'verticalLevel': 3,*/
            'nodeTitle': 'nama_jabatan',
            'toggleSiblingsResp': false,
            'nodeContent': 'nama',
            'exportButton': true,
            'exportFilename': nama_skpd,
            'exportFileextension': 'pdf',
            'createNode': function($node, data) {

              if(data.nip == ''){
                mycontent = 'Data belum dimasukan';  
              }else{
                mycontent = data.gelar_depan+" "+data.nama+ " "+data.gelar_belakang+" "+data.nip+", "+data.golongan+" ("+data.tmt+")";
              }

              $node.find('.content').text(mycontent);
            }

          });

       }

    });

 }



$(function() {


  $('#lay_kd_unitorganisasi').css('display','none');
  $('#lay_kd_unitkerja').css('display','none');
  $('#lay_kd_subunitkerja').css('display','none');

  $.ajax({

      url:"<?php echo base_url();?>petajabatan/petajabatan/get_bagan_default?kd_skpd="+kd_skpd,
      success: function(response){

         mychart.orgchart({
            'data' : response,
            'visibleLevel': 6,
            'verticalLevel': 3,
            'nodeTitle': 'nama_jabatan',
            'toggleSiblingsResp': false,
            'nodeContent': 'nama',
            'exportButton': true,
            'exportFilename': nama_skpd,
            'exportFileextension': 'pdf',
            'createNode': function($node, data) {

              if(data.nip == ''){
                mycontent = 'Data belum dimasukan';  
              }else{
                mycontent = data.gelar_depan+" "+data.nama+ " "+data.gelar_belakang+" "+data.nip+", "+data.golongan+" ("+data.tmt+")";
              }
              
              $node.find('.content').text(mycontent);
            }

          });

       }

    });





});



</script>


<!-- $node.find('.content').text(mycontent); -->


