<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> REPORT ALL</h3>
</div>
</section>
   <!-- Main content -->
    <section class="content">

      <div class="error-page">
        <h2 class="headline text-red">503</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> This Page is currently Under Maintenance.</h3>

          <p>
            We should be back shortly. Thank you for your patiente.
          </p>
		  <img src="<?php echo base_url();?>assets/image/maintenance.png">

        </div>
      </div>
      <!-- /.error-page -->

    </section>
    <!-- /.content -->
</div>