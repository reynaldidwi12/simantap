<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> REPORT DUK
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
			<?php 
		
				echo form_open_multipart('report/report_skpd/result');
			?>
				<div class="box-body">
				
				<div class="col-lg-6">
					
					<div class="form-group">
							<label >Nama SKPD</label>
							<?php echo form_dropdown("nama_skpd", $nama_skpd, $get_kd_skpd,'id="kd_skpd" class="form-control selectpicker" onChange="tampilUnitkerja()" data-live-search="true" data-live-search-style="begins" required');?>
					</div>
					<div class="form-group">
							<label >Unit Kerja</label>
							 <!--div id="kd_unitkerja" class="kd_unitkerja" data-container="body"></div> <!-- Jquery-->
							 <?php echo form_dropdown("nama_unit_kerja", $nama_unitkerja, $get_nama_unitkerja,'id="kd_unitkerja" class="form-control" data-live-search="true" data-live-search-style="begins"');?>
					</div>	
				</div>
				</div>
					
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Lihat"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('report/report_skpd', 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
	</div>
	
	<div class="panel-body">
	
		<?php 
		
				echo form_open_multipart('report/report_skpd/add');
			?>
				<?php echo form_input($kd_skpd)?>
				<?php echo form_input($kd_unitkerja)?>
					
				<div class="box-footer">
					<input class="btn btn-success" type="submit" name="submit" value="Cetak"/>
				</div>
		<?php echo form_close()?>
				
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th><center>No</center></th>
				<th >Nama/NIP/TTL</th>
				<th >Gol</th>
				<th >TMT Gol</th>
				<th >Jabatan</th>
				<th >TMT</th>
				
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($pegawai){
	    		$i=$number + 1;
				foreach ( $pegawai as $row ) {
			?>
		        <tr class="gradeX">
		        <td ><center><?php echo $i?>.</center></td>
		        <td ><?php echo $row->nama?><br><br>
					 NIP : <?php echo $row->nip?><br>
					 <?php echo $row->tempat_lahir?> / <?php echo $row->tgl_lahir?></td>
		        <td ><?php echo $row->golongan?></td>
		        <td ><?php 	
				
						if($row->tmt_pangkat){
							$tmt_pangkat = date("d-m-Y",strtotime($row->tmt_pangkat));
							echo $tmt_pangkat;							
						}else{}
				
				?></td>
		        <td ><?php echo $row->nama_jabatan?></td>
		        <td ><?php 
				
						if($row->tmt=='0000-00-00'){
														
						}else if($row->tmt){
							$tmt = date("d-m-Y",strtotime($row->tmt));
							echo $tmt;
						}else{
							
						}
				
				?></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='4'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/jquery-2.1.1.js'); ?>"></script>
<script>
function tampilUnitkerja()
 {
	 kd_skpd = document.getElementById("kd_skpd").value;
	 $.ajax({
		 url:"<?php echo base_url();?>report/report_skpd/get/"+kd_skpd+"",
		 success: function(response){
		 $("#kd_unitkerja").html(response);
		 }
	 });
	 return false;
 }
</script>