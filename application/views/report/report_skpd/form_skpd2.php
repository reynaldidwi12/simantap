<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<section class="content-header">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-user"></i> REPORT DUK</h3>
		</div>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-solid box-default">
					<?php if($this->session->flashdata('message')){ ?>
						<div class="box-body">
							<div class="alert alert-danger alert-dismissible">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
								<?php echo $this->session->flashdata('message'); ?>
							</div>
						</div>
					<?php }?>
					<div class="panel-body">
						<?php 
						echo form_open_multipart('report2/report_skpd/result');
						?>
						<div class="box-body">
							<div class="col-lg-6">
								<div class="form-group" id="lay_kd_skpd">
									<label>Nama OPD</label>
									<span id="unitorganisasi"></span>
									<?php echo form_dropdown("kd_skpd", $skpd, $get_skpd,'class="form-control" data-live-search="true" id="kd_skpd" data-live-search-style="begins" title="Pilih OPD" required');?>
								</div>


								<div class="form-group" id="lay_kd_unitorganisasi">
									<label>Unit Organisasi</label>

									<?php echo form_dropdown("kd_unitorganisasi", $unitorganisasi,  $get_unitorganisasi,' class="form-control" id="kd_unitorganisasi" onChange="tampilUnitKerja()"  data-live-search="true" data-live-search-style="begins"');?>
								</div>


								<div class="form-group" id="lay_kd_unitkerja">
									<label>Unit Kerja</label>

									<?php echo form_dropdown("kd_unitkerja", $unitkerja,  $get_unitkerja,' class="form-control" id="kd_unitkerja" onChange="tampilSubUnitKerja()" data-live-search="true" data-live-search-style="begins"');?>
								</div>
							</div>
						</div>

						<div class="box-footer">
							<input class="btn btn-primary" type="submit" name="submit" value="Lihat"/>&nbsp;
							<?php echo anchor('report2/report_skpd', 
							'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Reset"> Reset </button>' );?>&nbsp;
							<?php echo anchor('report2/report_skpd', 
							'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
						</div>
						<?php echo form_close()?>
					</div>
					<?php if ($pegawai != NULL || $pegawai != '') { ?>
						<div class="panel-body">

							<?php 

							echo form_open_multipart('report2/report_skpd/add');
							?>
							<?php echo form_input($input_kd_skpd)?>
							<?php echo form_input($input_kd_unitorganisasi)?>
							<?php echo form_input($input_kd_unitkerja)?>

							<div class="box-footer">
								<input class="btn btn-success" type="submit" name="submit" value="Cetak"/>
							</div>
							<?php echo form_close()?>

							<div class="dataTable_wrapper">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><center>No</center></th>
												<th >Nama/NIP/TTL</th>
												<th >Gol</th>
												<th >TMT Gol</th>
												<th >Jabatan</th>
												<th >TMT</th>

											</tr>
										</thead>
										<tbody>
											<?php
											if($pegawai){
												$i = $number + 1;
												foreach ( $pegawai as $row ) {

													?>
													<tr class="gradeX">
														<td ><center><?php echo $i?>.</center></td>
														<td ><?php echo $row->nama?><br><br>
															NIP : <?php echo $row->nip?><br>
															<?php echo $row->tempat_lahir?> / <?php echo $row->tgl_lahir?></td>
															<td ><?php echo $row->golongan?></td>
															<td ><?php 	

															if($row->tmt_pangkat){
																$tmt_pangkat = date("d-m-Y",strtotime($row->tmt_pangkat));
																echo $tmt_pangkat;							
															}else{}

															?></td>
															<td ><?php echo $row->nama_jabatan?></td>
															<td ><?php 

															if($row->tmt=='0000-00-00'){

															}else if($row->tmt){
																$tmt = date("d-m-Y",strtotime($row->tmt));
																echo $tmt;
															}else{

															}

															?></td>
														</tr>
														<?php 
														$i++;
													} 
												}else{
													echo "<tr class=\"gradeX\"><td colspan='4'>No Record</td></tr>";
												}?>
											</tbody>
										</table>
									</div>
									<div align="right"><?php echo $links?> </div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>
	</div>

	<script>										
		
		var kd_skpd;
		var kd_unitorganisasi;
		var kd_unitkerja;
		
		kd_skpd = document.getElementById("kd_skpd").value;
		kd_unitorganisasi = document.getElementById("kd_unitorganisasi").value;

		if (kd_skpd == null || kd_skpd == '') {
			jQuery("#lay_kd_unitorganisasi").css('display', 'none');
		} else {
			jQuery("#lay_kd_unitorganisasi").css('display', '');
		}

		if (kd_unitorganisasi == null || kd_unitorganisasi == '') {
			jQuery("#lay_kd_unitkerja").css('display', 'none');
		} else {
			jQuery("#lay_kd_unitkerja").css('display', '');
		}


		jQuery("#lay_kd_skpd").css('display', '');


		function tampilUnitOrganisasi(){
			kd_skpd = document.getElementById("kd_skpd").value;
			if(kd_skpd !== ''){
				$.ajax({
					url:"<?php echo base_url();?>report/report_skpd/get_unitorganisasi/"+kd_skpd+"",
					success: function(response){
						if (response !== '' || response !== null) {

							$("#kd_unitorganisasi").html(response);

							$("#kd_unitorganisasi").val('');
							$("#kd_unitkerja").val('');

							$("#lay_kd_unitorganisasi").css('display', '');

							jQuery("#lay_kd_unitkerja").css('display', 'none');
						} else {
							$("#kd_unitorganisasi").val('');
							$("#kd_unitkerja").val('');

							$("#lay_kd_unitorganisasi").css('display', 'none');

							jQuery("#lay_kd_unitkerja").css('display', 'none');
						}
					},
					dataType:"html"
				});

			}

			return false;
		}

		function tampilUnitKerja(){
			kd_unitorganisasi = document.getElementById("kd_unitorganisasi").value;
			if(kd_unitorganisasi !== ''){
				$.ajax({
					url:"<?php echo base_url();?>report/report_skpd/get_unitkerja/"+kd_skpd+"/"+kd_unitorganisasi+" ",
					success: function(response){
						if (response !== '' || response !== null) {
							$("#kd_unitkerja").html(response);
							$("#lay_kd_unitkerja").css('display', '');
							jQuery("#lay_kd_unitkerja").css('display', '');
						} else {
							$("#kd_unitkerja").val('');
							$("#lay_kd_unitkerja").css('display', 'none');
							jQuery("#lay_kd_unitkerja").css('display', 'none');
						}
						
					},
					dataType:"html"
				});

			} else {
				$("#kd_unitkerja").val('');
				$("#lay_kd_unitkerja").css('display', 'none');
				jQuery("#lay_kd_unitkerja").css('display', 'none');
			}
			return false;
		}

	</script>