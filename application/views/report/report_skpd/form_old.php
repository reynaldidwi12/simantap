<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> REPORT DUK</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
				echo form_open_multipart('report/report_skpd/result');
			?>
				<div class="box-body">
				<div class="col-lg-6">
					<div class="form-group">
							<label >Nama SKPD</label>
							<?php echo form_dropdown("nama_skpd", $nama_skpd, 0,'id="kd_skpd" class="form-control selectpicker" onChange="tampilUnitkerja()" data-live-search="true" data-live-search-style="begins" required');?>
					</div>
					<div class="form-group">
							<label >Unit Kerja</label>
							 <!--div id="kd_unitkerja" class="kd_unitkerja" data-container="body"></div> <!-- Jquery-->
							 <?php echo form_dropdown("nama_unit_kerja", $nama_unitkerja, $get_nama_unitkerja,'id="kd_unitkerja" class="form-control" data-live-search="true" data-live-search-style="begins"');?>
					</div>	
				</div>
				</div>
					
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Lihat"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('report/report_skpd', 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/jquery-2.1.1.js'); ?>"></script>
<script>
function tampilUnitkerja()
 {
	 kd_skpd = document.getElementById("kd_skpd").value;
	 $.ajax({
		 url:"<?php echo base_url();?>report/report_skpd/get/"+kd_skpd+"",
		 success: function(response){
		 $("#kd_unitkerja").html(response);
		 }
	 });
	 return false;
 }
</script>