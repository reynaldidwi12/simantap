<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> REPORT ALL</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
				<?php
					echo form_open_multipart('report/report_all/add');
				?>
					<div class="box-body">
					<div class="col-lg-6">
					<div class="form-group">
					<label >Cetak Rekapitulasi Pegawai</label>
					</div>
						</div>
						</div>

					<div class="box-footer">
						<input class="btn btn-primary" type="submit" name="submit" value="Cetak"/>&nbsp;

					</div>
					<?php echo form_close()?>

					<?php
						echo form_open_multipart('report/report_all/add_jenis');
					?>
						<div class="box-body">
						<div class="col-lg-6">
						<div class="form-group">
						<label >Cetak Rekapitulasi Pegawai Berdasarkan Jenis Kelamin</label>
						</div>
							</div>
							</div>

						<div class="box-footer">
							<input class="btn btn-primary" type="submit" name="submit" value="Cetak"/>&nbsp;

						</div>
						<?php echo form_close()?>

			<?php
				echo form_open_multipart('setup/maintenance');
			?>
				<div class="box-body">
				<div class="col-lg-6">
				<div class="form-group">
				<label >Cetak Rekapitulasi Pegawai per SKPD</label>
							<?php echo form_dropdown("nama_skpd", $nama_skpd, $nama_skpd2,'class="form-control selectpicker" data-live-search="true" required');?>
				</div>
					</div>
					</div>

				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Cetak"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('report/rekap_pegawai/',
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				<?php
				echo form_open_multipart('setup/maintenance');
			?>
				<div class="box-body">
				<div class="col-lg-6">
				<div class="form-group">
				<label >Rekap DUK Menurut Pendidikan</label>
							<?php echo form_dropdown("nama_skpd", $nama_skpd, $nama_skpd2,'class="form-control selectpicker" data-live-search="true" required');?>
				</div>
					</div>
					</div>

				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Cetak"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('report/rekap_pegawai/',
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				<?php
				echo form_open_multipart('setup/maintenance');
			?>
				<div class="box-body">
				<div class="col-lg-6">
				<div class="form-group">
				<label >Rekap DUK Menurut Golongan</label>
							<?php echo form_dropdown("nama_skpd", $nama_skpd, $nama_skpd2,'class="form-control selectpicker" data-live-search="true" required');?>
				</div>
					</div>
					</div>

				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Cetak"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('report/rekap_pegawai/',
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				<?php
				echo form_open_multipart('report/report_all/jurusan');
			?>
				<div class="box-body">

					<div class="col-lg-6">
						<div class="form-group">

							<label >Rekap DUK Menurut Jurusan</label>
							<?php echo form_dropdown("jurusan", $pendidikan, $get_pendidikan,'class="form-control selectpicker" data-live-search="true"  data-live-search-style="begins" title="Pilih Program Studi" required');?>
						</div>
						<div class="form-group">
							<?php echo form_dropdown("nama_skpd", $nama_skpd, $nama_skpd2,'class="form-control selectpicker" data-live-search="true" required');?>
						</div>
					</div>
				</div>

				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Cetak"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('report/rekap_pegawai/',
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
