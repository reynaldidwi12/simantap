<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Riwayat Hukuman
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			
		</div>
		<div class="col-sm-12">
			<div class="row">
				<div class="box-body">
					<div class="col-sm-3">
						<div class="form-group">
							<label >NIP.</label>
							<input type="text" name="nip" class="form-control" value="<?php echo $nip?>" readonly>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="form-group">
							<label >Nama.</label>
							<input type="text" name="nama" class="form-control" value="<?php echo $nama_pegawai?>" readonly>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:1%;">No</th>
				<th style="width:20%;">Hukuman</th>
				<th style="width:25%;">Uraian Hukuman</th>
				<th style="width:15%;">No. Surat</th>
				<th style="width:10%;">Tgl Surat</th>
				<th style="width:10%;">Pejabat</th>
				<th style="width:10%;"><center>Pilihan</center></th>			
			</tr>
			</thead>
			<tbody>
			<?php if($riwayat){ ?>
	    	<?php
	    		$i=1;
				foreach ( $riwayat as $row ) {
			?>
		        <tr class="gradeX">
		        <td class="col-md-1"><?php echo $i?></td>
		        <td class="col-md-1"><?php echo $row->hukuman?></td>
		        <td class="col-md-1"><?php echo $row->uraian_hukuman?></td>
		        <td class="col-md-1"><?php echo $row->no_surat_kerja?></td>
		        <td class="col-md-1"><?php 

		        	if($row->tgl_surat_kerja){
						$tgl_surat_kerja = date("d-m-Y",strtotime($row->tgl_surat_kerja));
						echo $tgl_surat_kerja;							
					}else{}

					?></td>
		        <td class="col-md-1"><?php echo $row->pejabat?></td>
		        <td ><?php if($status_edit=='1') { ?><center>
		        <a href="<?php echo base_url('member/riwayat_hukuman/modify/'.$row->seq_no) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
		        &nbsp;
		        <a href="<?php echo base_url('member/riwayat_hukuman/remove/'.$row->seq_no) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
				</center><?php } ?></td>
				</tr>
	        <?php $i++; 
				}
			}else{
	        	echo "<tr class=\"gradeX\"><td colspan='8'>No Record</td></tr>";
	        }?> 
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
		<div class=row>
			<div class="col-md-6 col-sm-4 col-xs-4">
				<div class="btn-group">
					<?php if($status_edit=='1') { ?>
				<?php echo anchor('member/riwayat_hukuman/add', 
				'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data Pegawai"><i class="fa fa-plus"></i> Tambah Riwayat</button>' );?>
				&nbsp;
			<?php } ?>
				<?php echo anchor('member/riwayat_hukuman', 
				'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
				&nbsp;
				<?php echo anchor('member', 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Kembali"> Kembali </button>' );?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</section>
</div>
