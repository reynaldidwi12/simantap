<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Riwayat Keluarga
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			
		</div>
		<div class="col-sm-12">
			<div class="row">
				<div class="box-body">
					<div class="col-sm-3">
						<div class="form-group">
							<label >NIP.</label>
							<input type="text" name="nip" class="form-control" value="<?php echo $nip?>" readonly>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="form-group">
							<label >Nama.</label>
							<input type="text" name="nama" class="form-control" value="<?php echo $nama_pegawai?>" readonly>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-body">
	<div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Suami / Istri</a></li>
              <li><a href="#tab_2" data-toggle="tab">Anak</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                
				<div class="dataTable_wrapper">
					<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
					<thead>
					<tr>
						<th style="width:1%;">No</th>
						<th style="width:15%;">Nama Suami/Istri</th>
						<th style="width:15%;">TTL</th>
						<th style="width:15%;">Tgl. Menikah</th>
						<th style="width:10%;"><center>Pilihan</center></th>			
					</tr>
					</thead>
					<tbody>
					<?php if($suami_istri){ ?>
					<?php
						$i=1;
						foreach ( $suami_istri as $row ) {
							
							if($row->tgl_lahir){
								$TglLahir = date("d-m-Y",strtotime($row->tgl_lahir));							
							}else{}	
							
							if($row->tgl_menikah){
								$tgl_menikah = date("d-m-Y",strtotime($row->tgl_menikah));							
							}else{}	
							
					?>
						<tr class="gradeX">
						<td ><?php echo $i?></td>
						<td ><?php echo $row->nama?></td>
						<td ><?php echo $row->tempat_lahir?> / <?php echo $TglLahir?></td>
						<td ><?php echo $tgl_menikah?></td>
						<td style="width:5%;">
							<?php if($status_edit=='1') { ?>
						<a href="<?php echo base_url('member/riwayat_suami_istri/modify/'.$row->seq_no) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
						&nbsp;	
						<a href="<?php echo base_url('member/riwayat_suami_istri/remove/'.$row->seq_no) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
					<?php } ?>
						</td>
						</tr>
					<?php $i++; 
						}
					}else{
						echo "<tr class=\"gradeX\"><td colspan='8'>No Record</td></tr>";
					}?>
					</tbody>
					</table>
					</div>
					<div align="right"><?php echo $links?> </div>
				</div>
				<div class=row>
					<div class="col-md-6 col-sm-4 col-xs-4">
						<div class="btn-group">
						<?php 
						//echo $jml;
						if($jml==0){
							if($status_edit=='1') {
						echo anchor('member/riwayat_suami_istri/add', 
						'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data Pegawai"><i class="fa fa-plus"></i> Tambah Riwayat</button>' );
					}
						}else{
							
						}
						?>
						&nbsp;	
						<?php echo anchor('member/riwayat_keluarga', 
						'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
						&nbsp;
						<?php echo anchor('member', 
							'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Kembali"> Kembali </button>' );?>
						</div>
					</div>
					
				</div>
				
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                
				<div class="dataTable_wrapper">
					<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
					<thead>
					<tr>
						<th style="width:1%;">No</th>
						<th style="width:15%;">Nama Anak</th>
						<th style="width:15%;">TTL</th>
						<th style="width:15%;">Jenis Kelamin</th>
						<th style="width:15%;">Status Anak</th>
						<th style="width:10%;"><center>Pilihan</center></th>			
					</tr>
					</thead>
					<tbody>
					<?php if($anak){ ?>
					<?php
						$i=1;
						foreach ( $anak as $row ) {
							
							if($row->tgl_lahir){
								$TglLahir = date("d-m-Y",strtotime($row->tgl_lahir));							
							}else{}			
				
					?>
						<tr class="gradeX">
						<td ><?php echo $i?></td>
						<td ><?php echo $row->nama?></td>
						<td ><?php echo $row->tempat_lahir?> / <?php echo $TglLahir?></td>
						<td ><?php echo $row->kelamin?></td>
						<td ><?php echo $row->status_anak?></td>
						<td style="width:5%;">
							<?php if($status_edit=='1') { ?>
						<a href="<?php echo base_url('member/riwayat_anak/modify/'.$row->nip.'/'.$row->seq_no) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
						&nbsp;
						<a href="<?php echo base_url('member/riwayat_anak/remove/'.$row->nip.'/'.$row->seq_no) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
					<?php } ?>
						</td>
						</tr>
					<?php $i++; 
						}
					}else{
						echo "<tr class=\"gradeX\"><td colspan='8'>No Record</td></tr>";
					}?>
					</tbody>
					</table>
					</div>
					<div align="right"><?php echo $links?> </div>
				</div>
				<div class=row>
					<div class="col-md-6 col-sm-4 col-xs-4">
						<div class="btn-group">
							<?php if($status_edit=='1') { ?>
						<?php echo anchor('member/riwayat_anak/add/'.$this->uri->segment(4), 
						'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data Pegawai"><i class="fa fa-plus"></i> Tambah Riwayat</button>' );?>
						&nbsp;
					<?php } ?>
						<?php echo anchor('member/riwayat_keluarga/result_riwayat_keluarga/'.$this->uri->segment(4), 
						'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
						&nbsp;
						<?php echo anchor('member/pegawai/find/'.$this->uri->segment(4).'/nip', 
							'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Kembali"> Kembali </button>' );?>
						</div>
					</div>
					
				</div>
				
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
		
		
	</div>
</div>
</div>
</section>
</div>
