<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data Anak</h3>
</div>
</section>
<?php if($status_edit=='1') { ?>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			
				<div class="box-body">
					<div class="col-lg-6">
						<?php echo form_open("member/riwayat_anak/cek/".$this->uri->segment(4));?>
						<div class="form-group">
							<label >NIP</label>
							<?php echo form_input($nip)?>
						</div>
						<div class="form-group">
							<label >PNS</label>
							<?php echo form_input($nip2)?>
						</div>
						<div class="btn-group">
								<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
							</div>
						<?php echo form_close();?>
					</div>
						
						<?php 
						if($this->uri->segment(3)=="add"){
							echo form_open('member/riwayat_anak/add');
						}else if($this->uri->segment(3)=="modify"){
							echo form_open('member/riwayat_anak/modify/'.$this->uri->segment(4));
						}else if($this->uri->segment(3)=="cek"){
							if($this->uri->segment(4)){
								echo form_open('member/riwayat_anak/modify/'.$this->uri->segment(4));
							}else{
								echo form_open('member/riwayat_anak/add');
							}
							
						}else{
							echo form_open('member/riwayat_anak/detail');
						}
						?>
						
					<div class="col-lg-6">
						<div class="form-group">
							<?php echo form_input($seq_no)?>
							<?php echo form_input($nip_)?>
							<label >Nama Pegawai</label>
							<?php echo form_input($nama)?>
						</div>
						<div class="form-group">
							<label >Nama Anak</label>
							<?php echo form_input($nama2)?>
						</div>
						<div class="form-group">
							<label >Tempat Lahir</label>
							<?php echo form_input($tempat_lahir)?>
						</div>
						<div class="form-group">
							<label >Tanggal Lahir</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tgl_lahir)?>	
							</div>
						</div>
						<div class="form-group">
							<label >Jenis Kelamin</label>
							<?php $option = array(
									''=>'',
									'1'=>'Pria',
									'2'=>'Wanita'
							); 
							echo form_dropdown($kelamin,$option,$get_kelamin);?>
						</div>
						<div class="form-group">
							<label >BPJS</label>
							<?php echo form_input($bpjs)?>
						</div>
						<div class="form-group">
							<label >Status Anak</label>
							<?php $option = array(
									''=>'',
									'1'=>'Kandung',
									'2'=>'Tiri',
									'3'=>'Angkat'
							); 
							echo form_dropdown($status_anak,$option,$get_status_anak);?>
						</div>
						<div class="form-group">
							<label >Status Pendidikan</label>
							<?php $option = array(
									''=>'',
									'1'=>'Pendidikan Anak Usia Dini / TK',
									'2'=>'Masih Sekolah',
									'3'=>'Belum sekolah / Belum masuk usia wajib belajar',
									'4'=>'Tidak Melanjutkan Sekolah',
									'5'=>'Sudah Bekerja',
							); 
							echo form_dropdown($status_pendidikan,$option,$get_status_pendidikan);?>
						</div>
						<div class="form-group">
							<label >Tingkat Pendidikan</label>
							<?php $option = array(
									''=>'',
									'1'=>'TK',
									'2'=>'SD',
									'3'=>'SMP',
									'4'=>'SMA',
									'5'=>'D1',
									'6'=>'D2',
									'7'=>'D3',
									'8'=>'S1',
									'9'=>'S2',
									'10'=>'S3'
							); 
							echo form_dropdown($tingkat_pendidikan,$option, $get_tingkat_pendidikan);?>
						</div>
					</div>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('member/riwayat_keluarga', 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<?php } else { ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="alert alert-warning alert-dismissible">
            Anda tidak bisa mengakses halaman ini. 
        </div>
    </div>
</div>
<?php } ?>