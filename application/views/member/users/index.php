<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
									<?php if($this->session->userdata('ss_skpd')=='Administrastor'){ ?>
											 <div class="box-header with-border">
												<h3 class="box-title"><?php echo anchor('admin/users/create', '<i class="fa fa-plus"></i> '. lang('users_create_user'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
											</div>
										<?php	} else {   
											}
											?>
                               
                                <div class="box-body">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>User Name</th>
                                                <th><?php echo lang('users_email');?></th>
                                                <th>SKPD</th>
                                                <th><?php echo lang('users_groups');?></th>
                                                <th><?php echo lang('users_status');?></th>
                                                <th><?php echo lang('users_action');?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php foreach ($users as $user):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($user->username, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php //echo htmlspecialchars($user->kd_skpd, ENT_QUOTES, 'UTF-8'); 
															$data = $this->skpd_model->fetchById2($user->kd_skpd);
															foreach ($data as $x){
																		echo $x->nama;
															}
												?>
													
												</td>
                                                <td>
<?php foreach ($user->groups as $group):?>
                                                    <?php echo anchor('admin/groups/edit/'.$group->id, '<span class="label" style="background:'.$group->bgcolor.';">'.htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8').'</span>'); ?>
<?php endforeach?>
                                                </td>
                                                <td><?php echo ($user->active) ? anchor('admin/users/deactivate/'.$user->id, '<span class="label label-success">'.lang('users_active').'</span>') : anchor('admin/users/activate/'. $user->id, '<span class="label label-default">'.lang('users_inactive').'</span>'); ?></td>
                                                <td>
                                                   <a href="<?php echo base_url('member/users/edit/'.$user->id) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
                                                   <a href="<?php echo base_url('member/users/profile/'.$user->id) ?>"><button class="btn btn-info btn-sm" title="Profile"><i class="fa fa-bars"></i></button></a>
                                                   
												   
												   <?php 
												   if($this->session->userdata('ss_skpd')=='Administrastor'){
													   if($user->kd_skpd=='Administrastor'){
													   } else {   ?>
													   <a href="<?php echo base_url('admin/users/remove/'.$user->id) ?>"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
												   <?php } 
												   } else {?>
												   
												   <?php } ?>
												   
												   
                                                </td>
                                            </tr>
<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
