<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo lang('users_edit_user'); ?></h3>
                                </div>
                                <div class="box-body">
                                    <?php echo $message;?>

                                    <?php echo form_open(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit_user')); ?>
									
										<div class="form-group">
                                            <?php echo lang('users_name', 'user_name', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($user_name);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('users_firstname', 'first_name', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($first_name);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('users_phone', 'phone', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($phone);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('users_password', 'password', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($password);?>
                                                <div class="progress" style="margin:0">
                                                    <div class="pwstrength_viewport_progress"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('users_password_confirm', 'password_confirm', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($password_confirm);?>
                                            </div>
                                        </div>
						<?php if($user->kd_skpd=='Administrastor'){ ?>
										 
										 <div class="form-group">
											<?php echo lang('', 'kd_skpd', array('class' => 'col-sm-2 control-label')); ?>
											<div class="col-sm-10">
											<?php 
											 $option = array(
													'Administrastor'=>'Administrastor'
											); 
											echo form_dropdown("nama_skpd", $option, '','class="form-control selectpicker" data-live-search="true"');?>
											</div>
										</div>
										 
						<?php } else { 
						
						if($this->session->userdata('ss_skpd')=='Administrastor'){
						?>
										 
										 
										<div class="form-group">
											<?php echo lang('users_kd_skpd', 'kd_skpd', array('class' => 'col-sm-2 control-label')); ?>
											<div class="col-sm-10">
											<?php echo form_dropdown("nama_skpd", $kd_skpd, $get_kd_skpd,'class="form-control selectpicker" data-live-search="true" title-begin="sss"');?>
											</div>
										</div>
										
										
										
										

<?php if ($this->ion_auth->is_admin()): ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?php echo lang('users_member_of_groups');?></label>
                                            <div class="col-sm-10">
<?php foreach ($groups as $group):?>
<?php
    $gID     = $group['id'];
    $checked = NULL;
    $item    = NULL;

    foreach($currentGroups as $grp) {
        if ($gID == $grp->id) {
            $checked = ' checked="checked"';
            break;
        }
    }
?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="radio" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked; ?>>
                                                        <?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8'); ?>
                                                    </label>
                                                </div>
<?php endforeach?>
                                            </div>
                                        </div>
<?php endif ?>
										
						<?php } else{ ?>
							
							<input type="hidden" name="nama_skpd" value=<?php echo $get_kd_skpd ?> class="form-control"  />

						<?php	
							} 
						
						} ?>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <?php echo form_hidden('id', $user->id);?>
                                                <?php echo form_hidden($csrf); ?>
                                                <div class="btn-group">
                                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                                    <?php echo anchor('member/users', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
