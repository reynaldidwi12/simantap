<style type="text/css">
.timeline-list {
 border-left: 4px solid #4298C3;
 border-bottom-right-radius: 4px;
 border-top-right-radius: 4px;
 background: rgba(255,255,255,0.03);
 margin: 30px auto;
 position: relative;
 line-height: 1.4em;
 font-size: 1.03em;
 padding: 0 0 50px 50px;
 list-style: none;
 text-align: left;
 font-weight: 100;
 max-width: 60%;
}
.timeline-list h1, 
.timeline-list h2, 
.timeline-list h3 {
 font-weight: bold;
 font-size: 16px;
}
.timeline-list .event {
 border-bottom: 1px dashed rgba(255,255,255,0.1);
 padding-bottom: 25px;
 margin-bottom: 50px;
 position: relative;
}
.timeline-list .event:last-of-type {
 padding-bottom: 0;
 margin-bottom: 0;
 border: none;
}
.timeline-list .event:before, 
.timeline-list .event:after {
 position: absolute;
 display: block;
 top: 0;
}
.timeline-list .event:before {
 left: -217.5px;
 content: attr(data-time) '\A' attr(data-date);
 white-space: pre;
 text-align: right;
 font-weight: 100;
 font-size: 0.9em;
 min-width: 120px;
}
.timeline-list .event:after {
 box-shadow: 0 0 0 4px #4298c3;
 left: -57.85px;
 background: #313534;
 border-radius: 50%;
 height: 11px;
 width: 11px;
 content: "";
 top: 5px;
}
</style>
<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Pengajuan Kenaikan Pangkat
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<h4>Timeline Pengajuan</h4>
			<div class="row">
				<div class="col-md-8">
					<?php
					$get_status = json_decode($data->status);
					?>
					<div id="timeline-content">
					    <ul class="timeline-list">
					        <?php
					        foreach($get_status->data as $key => $val) {
					            $date = tgl_indo(date("Y-m-d", $val->date));
					            $time = date("h:i A", $val->date);
					            echo '<li class="event" data-date="'.$date.'" data-time="'.$time.'">';
					                echo '<h3>'.convert_status_pegawai($val->status, $data->kategori, $data->sub_kategori).'</h3>';
					                echo '<p>'.$val->note.'</p>';
					            echo '</li>';
					        }
					        ?>
					    </ul>
					</div>
				</div>
				<div class="col-md-4">&nbsp;</div>
			</div>
		</div>
	</div>
</div>
</div>
</section>
</div>