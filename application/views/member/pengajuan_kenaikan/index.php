<style type="text/css">
	.parent-item {
		margin: 20px 0;
		padding-right: 50px;
	}
	.parent-item .sub-item {
		background-color: #dedede;
		padding: 8px 10px;
		margin-bottom: 1px;
	}
	.parent-item .sub-item.item-not-found {
		background-color: #e04a3a !important;
		color: #fff;
	}
	.parent-item .sub-item p {
		margin-bottom: 0;
		font-weight: bold;
	}
	.parent-item .sub-item:nth-child(2n) {
		background-color: #efefef;
	}
	.label-top {
		border-left: 3px solid #337ab7;
		padding: 10px;
		margin-top: 20px;
	}
	.btn-org {
		background-color: #ff7900;
		color: #fff;
	}
	.btn-org:hover,
	.btn-org:focus {
		background-color: #E26200;
		color: #fff;
	}
</style>

<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Pengajuan Kenaikan Pangkat
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<h4>Jenis Pengajuan</h4>
			<div class="row"><div class="col-sm-4">
			<select class="form-control" id="pilih-pengajuan">
				<option value="">--Pilih--</option>
				<option value="reguler">Reguler</option>
				<option value="pilihan">Pilihan</option>
				<option value="jft">JFT</option>	
			</select>
			</div></div>
		</div>
		<div class="col-sm-12">
			<div class="row">
				<div class="col-md-8">
					<label class="label-top">Berkas yang diperlukan:</label>
					<div id="result-pilih"></div>
				</div>
				<div class="col-md-4">
					<div class="alert alert-success" role="alert">
						<h4 class="alert-header">KETERANGAN:</h4>
						<p>
							Pastikan Line Merah tidak muncul. Jika muncul berupa kolom merah maka file yang di unggah di <b>Arsip Digital</b> belum sesuai penamaannya atau tidak tersedia dalam arsip digital.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
        $(document).ready(function(){

        	getNotFound();

        	function getNotFound() {
        		var count = $(".item-not-found").length;
        		var btn = $("#submit");

        		if(count == 0) {
        			btn.removeAttr("disabled");
        			btn.attr('data-toggle','modal');
        			btn.attr('data-target','#confirm_modal');
        		} else {
        			btn.prop('disable', true);
        			btn.removeAttr("data-toggle");
        			btn.removeAttr("data-target");
        		}
        	}
 
            $('#pilih-pengajuan').change(function(){ 
                var id=$(this).val();

                $.ajax({
		            url : "<?php echo site_url('member/pengajuan_kenaikan/get_daftar_berkas').'/'.$userNip;?>",
                    method : "POST",
                    data : {id: id}
		        }).done(function(data) {
		        	if(data && id != '') {
                        $('#result-pilih').html(data);
                        getNotFound();
                	} else {
                		$('#result-pilih').html('');
                	}
		        })
            }); 
             
        });
    </script>
</div>
</div>
</section>
</div>
