<style type="text/css">
	.parent-item {
		margin: 20px 0;
		padding-right: 50px;
	}
	.parent-item .sub-item {
		background-color: #dedede;
		padding: 8px 10px;
		margin-bottom: 1px;
	}
	.parent-item .sub-item.item-not-found {
		background-color: #e04a3a !important;
		color: #fff;
	}
	.parent-item .sub-item p {
		margin-bottom: 0;
		font-weight: bold;
	}
	.parent-item .sub-item:nth-child(2n) {
		background-color: #efefef;
	}
	.label-top {
		border-left: 3px solid #337ab7;
		padding: 10px;
		margin-top: 20px;
	}
	.btn-org {
		background-color: #ff7900;
		color: #fff;
	}
	.btn-org:hover,
	.btn-org:focus {
		background-color: #E26200;
		color: #fff;
	}
	.panel-heading h3 {
		text-transform: uppercase;
	}
</style>

<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Pengajuan Kenaikan Pangkat <?php echo $pengajuan->sub_kategori; ?>
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class="alert alert-warning" role="alert">
				<p>
					Pengajuan Anda dikembalikan, dimohon untuk memperbaiki berkas yang tidak valid, kemudian silahkan untuk melakukan pengiriman berkas kembali.
				</p>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="row">
				<div class="col-md-8">
					<label class="label-top">Berkas yang diperlukan:</label>
					<div id="result-pilih">
						<?php
						if(!empty($data)) { ?>
						<?php $year = date('Y'); ?>
						<div class="parent-item">
							<?php
							foreach ($data as $value) { ?>
								<?php

								$filePath = $topDirectory . '/' . $userNip . '/kenaikan/' . $year;
								$fileName = $value->sub_kategori . '_' . $value->no_urut . '_' . $value->nama_file . '.pdf';
								$fileExist = $filePath . '/' . $fileName;
								if (!file_exists($fileExist)) {
								    $status = 'item-not-found';
								} else {
									$status = '';
								}
								?>
								<div class="sub-item <?=$status;?>">
									<?php echo $value->nama_berkas; ?>
									<p class="sub-item-detail">
										Format File: <?php echo $value->sub_kategori; ?>_<?php echo $value->no_urut; ?>_<?php echo $value->nama_file; ?>.pdf
									</p>
								</div>
							<?php } ?>
						</div>
						<div class="btn-box">
							<a class="btn btn-org" id="submit" onclick="setConfirm('Pastikan data sudah sesuai, guna cepatnya proses pengajuan.','<?=base_url('member/pengajuan_kenaikan/kirim_ulang/'.$pengajuan->id.'/'.$this->uri->segment(4));?>','<?=$year;?>','<?=$this->uri->segment(4);?>','<?=$pengajuan->sub_kategori;?>')" disabled>Kirim Data</a>
						</div>
					<?php }
						?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="alert alert-success" role="alert">
						<h4 class="alert-header">KETERANGAN:</h4>
						<p>
							Pastikan Line Merah tidak muncul. Jika muncul berupa kolom merah maka file yang di unggah di <b>Arsip Digital</b> belum sesuai penamaannya atau tidak tersedia dalam arsip digital.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
        $(document).ready(function(){

        	getNotFound();

        	function getNotFound() {
        		var count = $(".item-not-found").length;
        		var btn = $("#submit");

        		if(count == 0) {
        			btn.removeAttr("disabled");
        			btn.attr('data-toggle','modal');
        			btn.attr('data-target','#confirm_modal');
        		} else {
        			btn.prop('disable', true);
        			btn.removeAttr("data-toggle");
        			btn.removeAttr("data-target");
        		}
        	}
        });
    </script>
</div>
</div>
</section>
</div>
