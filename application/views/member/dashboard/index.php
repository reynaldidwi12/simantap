<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style type="text/css">
    .profil-img {
        height: 120px;
        width: 120px;
        position: relative;
        margin: 0 auto 20px;
    }
    .profil-img img {
        height: 100%;
        width: 100%;
        object-position: 50% 50%;
        o-object-fit: cover;
        object-fit: cover;
    }
    .info-box-content {
        position: relative;
    }
    .info-box-content .info-box-number {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .list-group .list-group-item.active>a {
        color: #fff;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <?php echo $dashboard_alert_file_install; ?>

        <?php if($this->session->userdata('set_passwd') == 0) { ?>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Silahkan ganti password Anda sebelum menggunakan fitur ini.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-5">
                <div class="box box-primary">
                    <div class="box-body">
                        <h4>Ganti Password</h4>

                        <div id="infoMessage"><?php echo $message;?></div>

                        <?php echo form_open("member/dashboard/change_password","autocomplete='off'");?>
                            <div class="box-body">
                                <div class="form-group">
                                    <?php echo 'Password Lama'; ?> <br />
                                    <?php echo form_input($old_password);?>
                                </div>
                                <div class="form-group">
                                    <label for="new_password"><?php echo sprintf('Password Baru', $min_password_length);?></label> <br />
                                    <?php echo form_input($new_password);?>
                                    <input type="password" id="disable-pwd-mgr-1" style="display: none;" value="stop-pwd-mgr-1"/>
                                    <input type="password" id="disable-pwd-mgr-2" style="display: none;" value="stop-pwd-mgr-2"/>

                                    <div class="progress" style="margin:0">
                                        <div class="pwstrength_viewport_progress"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php echo 'Konfirmasi Password Baru';?> <br />
                                    <?php echo form_input($new_password_confirm);?>
                                </div>
                                <div class="box-footer">
                                    <input class="btn btn-primary" type="submit" name="submit" value="Submit"/>&nbsp;
                                </div>
                            </div>
                        <?php echo form_close();?>

                    </div>
                </div>
            </div>
        </div>
        <?php } else { ?>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="box box-primary">
                    <div class="box-body box-profile" style="height:240px;">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="profil-img">
                                    <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url();?>files/foto_pegawai/<?php echo $gambar;?>" alt="<?php echo $name; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                      <b>NIP Baru</b> <a class="pull-right"><?php echo $nip_txt; ?></a>
                                    </li>
                                    <li class="list-group-item">
                                      <b>Nama</b> <a class="pull-right"><?php echo $prev_nama.' '.$name.' '.$next_nama; ?></a>
                                    </li>
                                    <li class="list-group-item">
                                      <b>TTL</b> <a class="pull-right"><?php echo $place_birth.', '.date("d M Y", strtotime($date_birth)); ?></a>
                                    </li>
                                    <li class="list-group-item">
                                      <b>Jenis Kelamin</b> <a class="pull-right"><?php echo $jk; ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="info-box" style="border-radius:0;display:inline-block;">
                            <span class="info-box-icon bg-aqua" style="width:100%;border-radius:0;line-height:1.5;"><b style="display:block;font-size:16px;">SKPD</b><i class="fa fa-building"></i></span>

                            <div class="info-box-content text-center" style="width:100%;margin-left:0;float:left;height:150px;overflow:hidden;">
                              <span class="info-box-number" style="line-height:1;"><small><?php echo $skpd_name; ?></small></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info-box" style="border-radius:0;display:inline-block;">
                            <span class="info-box-icon bg-green" style="width:100%;border-radius:0;line-height:1.5;"><b style="display:block;font-size:16px;">Jabatan</b><i class="fa fa-user"></i></span>

                            <div class="info-box-content text-center" style="width:100%;margin-left:0;float:left;height:150px;overflow:hidden;">
                              <span class="info-box-number" style="line-height:1;"><small><?php echo $jabatan_txt; ?></small></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info-box" style="border-radius:0;display:inline-block;">
                            <span class="info-box-icon bg-yellow" style="width:100%;border-radius:0;line-height:1.5;"><b style="display:block;font-size:16px;">Gol</b><i class="fa fa-tag"></i></span>

                            <div class="info-box-content text-center" style="width:100%;margin-left:0;float:left;height:150px;overflow:hidden;">
                              <span class="info-box-number" style="line-height:1;"><big><?php echo $gol_txt; ?></big></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="box box-primary">
                    <div class="box-header">
                      <h3 class="box-title">Menu</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <ul class="list-group">
                        <li class="list-group-item active"><a href="<?php echo base_url('member'); ?>">Data Utama</a></li>
                        <li class="list-group-item">Data Riwayat
                            <ul class="sub-menu" style="list-style:none;padding:10px;">
                                <li><a href="<?php echo base_url('member/riwayat_pendidikan'); ?>">Riwayat Pendidikan</a></li>
                                <li><a href="<?php echo base_url('member/riwayat_kepangkatan'); ?>">Riwayat Kepangkatan</a></li>
                                <li><a href="<?php echo base_url('member/riwayat_jabatan'); ?>">Riwayat Jabatan</a></li>
                                <li><a href="<?php echo base_url('member/riwayat_diklat'); ?>">Riwayat Diklat</a></li>
                                <li><a href="<?php echo base_url('member/riwayat_penghargaan'); ?>">Riwayat Penghargaan</a></li>
                                <li><a href="<?php echo base_url('member/riwayat_hukuman'); ?>">Riwayat Hukum</a></li>
                                <li><a href="<?php echo base_url('member/riwayat_keluarga'); ?>">Riwayat Keluarga</a></li>
                            </ul>
                        </li>
                        <li class="list-group-item"><a href="<?php echo base_url('member/dashboard/report_rh'); ?>">Cetak Data</a></li>
                      </ul>
                    </div>
                  </div>
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="box box-primary">
                    <div class="box-header">
                      <h3 class="box-title">Form</h3>
                    </div>
                    <div class="panel-body">
                        <div class="box-body">
                            <?php if($status_edit == 1) { ?>
                            <?php echo form_open_multipart('member/dashboard/modify'); ?>
                            <?php } ?>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label >Foto Pegawai.</label>
                                    <?php echo form_input($picture)?>
                                    <?php if($gambar){?>
                                        <br/>
                                        <div class="profil-img">
                                            <img class="img-responsive img-circle" src='<?php echo base_url();?>files/foto_pegawai/<?php echo $gambar?>'/>
                                        </div>   
                                    <?php 
                                    }else{}?>
                                    
                                </div>
                                
                                <div class="form-group">
                                    <label >NIP Baru</label>
                                    <?php echo form_input($nip)?>
                                </div>
                                <div class="form-group">
                                    <label >NIP Lama</label>
                                    <?php echo form_input($nip_lama)?>
                                </div>
                                
                                <div class="form-group">
                                    <label >Tempat Lahir.</label>
                                    <?php echo form_input($tempat_lahir)?>
                                </div>
                                <div class="form-group">
                                    <label >Tanggal Lahir.</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <?php echo form_input($tgl_lahir)?> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >No. KTP</label>
                                    <?php echo form_input($no_ktp)?>
                                </div>
                                <div class="form-group">
                                    <label >No. Karpeg</label>
                                    <?php echo form_input($no_kartu_pegawai)?>
                                </div>
                                <div class="form-group">
                                    <label >Jenis Kelamin.</label>
                                    <?php $option = array(
                                            ''=>'',
                                            '1'=>'Pria',
                                            '2'=>'Wanita'
                                    ); 
                                    echo form_dropdown($kelamin,$option,$get_kelamin);?>
                                </div>
                                <div class="form-group">
                                    <label >Alamat.</label>
                                    <?php echo form_textarea($alamat)?>
                                </div>
                                
                                <div class="form-group">
                                    <label >Agama.</label>
                                    <?php $option = array(
                                            ''=>'',
                                            '1'=>'Islam',
                                            '2'=>'Katolik',
                                            '3'=>'Hindu',
                                            '4'=>'Budha',
                                            '5'=>'Sinto',
                                            '6'=>'Konghucu',
                                            '7'=>'Protestan'
                                    ); 
                                    echo form_dropdown($agama,$option,$get_agama);?>
                                </div>
                                <div class="form-group">
                                    <label >Golongan Darah.</label>
                                    <?php $option = array(
                                            ''=>'',
                                            '1'=>'A',
                                            '2'=>'B',
                                            '3'=>'AB',
                                            '4'=>'O'
                                    ); 
                                    echo form_dropdown($gol_darah,$option,$get_gol_darah);?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label >Nama Pegawai.</label>
                                    <?php echo form_input($nama)?>
                                </div>
                                <div class="form-group">
                                    <label >Gelar Depan.</label>
                                    <?php echo form_input($gelar_depan)?>
                                </div>
                                <div class="form-group">
                                    <label >Gelar Belakang.</label>
                                    <?php echo form_input($gelar_belakang)?>
                                </div>
                                <div class="form-group">
                                    <label >No. Telp.</label>
                                    <?php echo form_input($telp)?>
                                </div>
                                <div class="form-group">
                                    <label >No. BPJS / Askes</label>
                                    <?php echo form_input($no_askes)?>
                                </div>
                                <div class="form-group">
                                    <label >NPWP</label>
                                    <?php echo form_input($npwp)?>
                                </div>
                                <div class="form-group">
                                    <label >TMT CPNS</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <?php echo form_input($tmt_cpns)?>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >TMT PNS</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <?php echo form_input($tmt_pns)?>   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >No Karsu/Karis</label>
                                    <?php echo form_input($no_kartu_keluarga)?>
                                </div>
                                <div class="form-group">
                                    <label >Status.</label>
                                    <?php $option = array(
                                            ''=>'',
                                            '1'=>'Belum Kawin',
                                            '2'=>'Kawin',
                                            '3'=>'Cerai'
                                    ); 
                                    echo form_dropdown($status,$option,$get_status);?>
                                </div>
                            </div>

                            <div class="col-lg-12">
                            </div>
                            
                        </div>
                        <?php if($status_edit == 1) { ?>
                        <div class="box-footer">
                            <input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
                        </div>
                        <?php echo form_close(); ?>
                        <?php } ?>
                    </div>
                  </div>
            </div>
        </div>

        <?php } ?>
    </section>
</div>

<script src="<?php echo base_url($plugins_dir . '/pwstrength/pwstrength.min.js'); ?>"></script>
<script type="text/javascript">
if ($('#new').length) {
    var options = {};

    options.ui = {
        container: '#pwd-container',
        showVerdictsInsideProgressBar: true,
        viewports: {
            progress: '.pwstrength_viewport_progress'
        }
    };
    options.common = {
        debug: false,
        onLoad: function() {
            $('#messages').text('Start typing password');
        }
    };

    $('#new').pwstrength(options);
}
</script>

