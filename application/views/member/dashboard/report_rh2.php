
<!--Import materialize.css-->
<link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>">
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<div class="box-body mdl-cell--12-col">

    <h3 class="mdl-cell mdl-cell--12-col"><div align="center">IDENTITAS PEGAWAI</div></h3><br>
    
          <?php
                    foreach ($pegawai as $dt) {
                        ?>
						<div align="center">
						<?php if( $dt->picture){ ?>
								<img src='<?php echo base_url();?>files/foto_pegawai/<?php echo $dt->picture?>' width="150" height="200"/>
							<?php }else{ ?>
								<img src='<?php echo base_url();?>files/foto_pegawai/image.png' width="150" height="200"/>
							<?php } ?>
						</div><br><br>	
           <table class="table table-bordered table-hover">
                <tbody>

                   
                        <tr>
							<td height="30px" style="width:20%">NIP</td>
							<td style="width:70%">: <?php echo $dt->nip; ?></td>
						</tr>
                        <tr>
							<td height="30px">Nama Lengkap</td>
							<td>: <?php echo $dt->nama; ?></td>
						</tr>
                        <tr>
							<td height="30px">Gelar Kesarjanaan</td>
							<td>: <?php echo $dt->gelar_belakang; ?></td>
						</tr>
                        <tr>
							<td height="30px">Tempat/Tgl. Lahir</td>
							<td>: <?php if($dt->tempat_lahir){
											echo $dt->tempat_lahir." / ";
										}else{}?>
							      <?php if($dt->tgl_lahir){
											$TglLahir = date("d-m-Y",strtotime($dt->tgl_lahir));
											echo $TglLahir;
										}else{}?></td>
						</tr> 
                        <tr>
							<td height="30px">Jenis Kelamin</td>
							<td>: <?php if($dt->kelamin==1){
									echo "Laki-laki";
								}else{
									echo "Perempuan";
								}?></td>
						</tr>
                        <tr>
							<td height="30px">Agama</td>
							<td>: <?php if($dt->agama==1){
									echo "Islam";
								}else if($dt->agama==2){
									echo "Katolik";
								}else if($dt->agama==3){
									echo "Hindu";
								}else if($dt->agama==4){
									echo "Budha";
								}else if($dt->agama==5){
									echo "Sinto";
								}else if($dt->agama==6){
									echo "Konghucu";
								}else if($dt->agama==7){
									echo "Protestan";
								}
								?></td>
						</tr>   
                        <tr>
							<td height="30px">Alamat Tempat Tinggal</td>
							<td>: <?php echo $dt->alamat; ?></td>
						</tr>     
                        <tr>
							<td height="30px"> Golongan Darah</td>
							<td>:
								   <?php if($dt->gol_darah==1){
									echo "A";
								}else if($dt->gol_darah==2){
									echo "B";
								}else if($dt->gol_darah==3){
									echo "AB";
								}else if($dt->gol_darah==4){
									echo "0";
								}
								?></td>
						</tr>         
                        <tr>
							<td height="30px"> Nomor Karpeg</td>
							<td>: <?php echo $dt->no_kartu_pegawai; ?></td>
						</tr>          
                        <tr>
							<td height="30px"> Nomor BPJS</td>
							<td>: <?php echo $dt->no_askes; ?></td>
						</tr>             
                        <tr>
							<td height="30px">Nomor Karsu/Karis</td>
							<td>: <?php echo $dt->no_kartu_pegawai; ?></td>
						</tr>        
						<tr>
							<td height="30px">Nomor KTP</td>
							<td>: <?php echo $dt->no_ktp; ?></td>
						</tr> 
                        <tr>
							<td height="30px">NPWP</td>
							<td>: <?php echo $dt->npwp; ?></td>
						</tr>                          

                    <?php } ?> 
                </tbody>
            </table><br><br>
			
			<?php
                    foreach ($riwayat_suami_istri as $dt) {
                        ?>
						<h4 class="mdl-cell mdl-cell--12-col">RIWAYAT SUAMI/ISTRI</h4>
           <table class="table table-bordered table-hover">
                <tbody>

                   
                        <tr>
							<td height="30px">Nama Lengkap</td>
							<td>: <?php echo $dt->nama; ?></td>
						</tr>
                        <tr>
							<td height="30px">Tempat/Tgl. Lahir</td>
							<td>: <?php if($dt->tempat_lahir){
											echo $dt->tempat_lahir." / ";
										}else{}?>
							      <?php if($dt->tgl_lahir){
											$TglLahir = date("d-m-Y",strtotime($dt->tgl_lahir));
											echo $TglLahir;
										}else{}?></td>
						</tr> 
                        <tr>
							<td height="30px">Tgl. Menikah</td>
							<td>: <?php if($dt->tgl_menikah){
											$tgl_menikah = date("d-m-Y",strtotime($dt->tgl_menikah));
											echo $tgl_menikah;
										}else{}?></td>
						</tr>
                        <tr>
							<td height="30px">Tgl. Meninggal</td>
							<td>: <?php if($dt->tgl_meninggal){
											$tgl_meninggal = date("d-m-Y",strtotime(tgl_meninggal));
											echo $tgl_meninggal;
										}else{}?></td>
						</tr>
                        <tr>
							<td height="30px">Tgl. Cerai</td>
							<td>: <?php if($dt->tgl_cerai){
											$tgl_cerai = date("d-m-Y",strtotime(tgl_cerai));
											echo $tgl_cerai;
										}else{}?></td>
						</tr>         
                        <tr>
							<td height="30px"> Nomor Akta Nikah</td>
							<td>: <?php echo $dt->no_akta_nikah; ?></td>
						</tr>          
                        <tr>
							<td height="30px"> Nomor Akta Meninggal</td>
							<td>: <?php echo $dt->no_akta_meninggal; ?></td>
						</tr>             
                        <tr>
							<td height="30px">Nomor Akta Cerai</td>
							<td>: <?php echo $dt->no_akta_cerai; ?></td>
						</tr>        
						<tr>
							<td height="30px">Status</td>
							<td>: <?php if($dt->status==1){
									echo "Menikah";
								}else if($dt->status==2){
									echo "Cerai";
								}else if($dt->status==3){
									echo "Janda/Duda";
								}
								?></td>
						</tr> 
                        <tr>
							<td height="30px">BPJS</td>
							<td>: <?php echo $dt->bpjs; ?></td>
						</tr>                          

                    <?php } ?> 
                </tbody>
            </table><br><br>
			
			<h4 class="mdl-cell mdl-cell--12-col">RIWAYAT ANAK</h4>
			<table class="table table-bordered table-hover">
                <tbody>
					<thead>
						<tr>   
							<th height="30px" data-field="name">No</th>
							<th height="30px" data-field="name">Nama</th>
							<th height="30px" data-field="name">Tempat/Tgl. Lahir</th>
							<th height="30px" data-field="name">Jenis Kelamin</th>
							<th height="30px" data-field="name">Tingkat Pendidikan</th>
							<th height="30px" data-field="name">Status Anak</th>            
						</tr>    
					</thead>
                    <?php
					$no = 1;
                    foreach ($riwayat_anak as $riwayat_anak) {
                        ?>
                        <tr>
							<td height="30px"><?php echo $no; ?></td>
							<td height="30px"><?php echo $riwayat_anak->nama;?></td>
                            <td height="30px"><?php if($riwayat_anak->tempat_lahir){
											echo $riwayat_anak->tempat_lahir." / ";
										}else{}?>
							      <?php if($riwayat_anak->tgl_lahir){
											$TglLahir = date("d-m-Y",strtotime($riwayat_anak->tgl_lahir));
											echo $TglLahir;
										}else{}?></td>
                            <td height="30px"><?php if($riwayat_anak->kelamin==1){
												echo "Laki-laki";
											}else{
												echo "Perempuan";
											}?></td>
                            <td height="30px">
											<?php 
									
									switch($riwayat_anak->tingkat_pendidikan){
										case '1' 	: echo "TK"; break;
										case '2' 	: echo "SD"; break;
										case '3' 	: echo "SMP"; break;
										case '4' 	: echo "SMA"; break;
										case '5' 	: echo "D1"; break;
										case '6' 	: echo "D2"; break;
										case '7' 	: echo "D3"; break;
										case '8' 	: echo "S1"; break;
										case '9' 	: echo "S2"; break;
										case '10' 	: echo "S3"; break;
										default		: "";
									}
							
							?></td>
                            <td height="30px"><?php 
										
										switch($riwayat_anak->status_anak){
										case '1' 	: echo "Kandung"; break;
										case '2' 	: echo "Tiri"; break;
										case '3' 	: echo "Angkat"; break;
										default		: "";
									}
							 ?></td>                     
                        </tr> 
                    <?php $no++;} ?> 
                </tbody>
            </table><br><br>
			
			<h4 class="mdl-cell mdl-cell--12-col">RIWAYAT PENDIDIKAN PEGAWAI</h4>
			<table class="table table-bordered table-hover">
                <tbody>
					<thead>
						<tr>   
							<th height="30px" data-field="name">No</th>
							<th height="30px" data-field="name">Nama Pendidikan</th>
							<th height="30px" data-field="name">Jurusan</th>
							<th height="30px" data-field="name">pejabat</th>
							<th height="30px" data-field="name">No. Ijazah</th>
							<th height="30px" data-field="name">Tahun Ijazah</th>             
						</tr>    
					</thead>
                    <?php
					$no = 1;
                    foreach ($riwayat_pendidikan as $riwayat_pendidikan) {
                        ?>
                        <tr>
							<td height="30px"><?php echo $no; ?></td>
							<td height="30px"><?php
									
									switch($riwayat_pendidikan->jenis_pendidikan){
										case '1' 	: echo "Sekolah Dasar"; break;
										case '2' 	: echo "Sekolah Menengah Pertama"; break;
										case '3' 	: echo "Sekolah Menengah Atas"; break;
										case '4' 	: echo "Diploma 1"; break;
										case '5' 	: echo "Diploma 2"; break;
										case '6' 	: echo "Diploma 3"; break;
										case '7' 	: echo "Strata 1"; break;
										case '8' 	: echo "Strata 2"; break;
										case '9' 	: echo "Doktor"; break;
										case '10' 	: echo "Doktor"; break;
										default		: "";
									}
							
							?>
												
												
							</td>
                            <td height="30px"><?php echo $riwayat_pendidikan->nama_sekolah; ?></td>
                            <td height="30px"><?php echo $riwayat_pendidikan->kepala_sekolah; ?></td>
                            <td height="30px"><?php echo $riwayat_pendidikan->no_ijazah; ?></td>
                            <td height="30px"><?php echo $riwayat_pendidikan->tahun_ijazah; ?></td>                     
                        </tr> 
                    <?php $no++;} ?> 
                </tbody>
            </table><br><br>
			
			<h4 class="mdl-cell mdl-cell--12-col">RIWAYAT KEPANGKATAN PEGAWAI</h4>
			<table class="table table-bordered table-hover">
                <tbody>
					<thead>
						<tr>   
							<th height="30px" data-field="name">No</th>
							<th height="30px" data-field="name">Golongan</th>
							<th height="30px" data-field="name">TMT Pangkat</th>
							<th height="30px" data-field="name">Nomor SK</th>
							<th height="30px" data-field="name">Tgl SK</th>
							<th height="30px" data-field="name">Masa Kerja</th>             
						</tr>    
					</thead>
                    <?php
					$no = 1;
                    foreach ($riwayat_kepangkatan as $riwayat_kepangkatan) {
                        ?>
                        <tr>
							<td height="30px"><?php echo $no; ?></td>
							<td height="30px"><?php echo $riwayat_kepangkatan->golongan; ?></td>
                            <td height="30px"><?php if($riwayat_kepangkatan->tmt_pangkat){
											$tmt_pangkat = date("d-m-Y",strtotime($riwayat_kepangkatan->tmt_pangkat));
											echo $tmt_pangkat;
									  }else{}?>
									</td>
                            <td height="30px"><?php echo $riwayat_kepangkatan->no_surat_kerja; ?></td>
                            <td height="30px"><?php if($riwayat_kepangkatan->tgl_surat_kerja){
											$tgl_surat_kerja = date("d-m-Y",strtotime($riwayat_kepangkatan->tgl_surat_kerja));
											echo $tgl_surat_kerja;
									  }else{}?>
								</td>
                            <td height="30px"><?php echo $riwayat_kepangkatan->mk_bulan; ?> Bulan, <?php echo $riwayat_kepangkatan->mk_bulan; ?> Tahun</td>                     
                        </tr> 
                    <?php $no++;} ?> 
                </tbody>
            </table><br><br>
			
			<h4 class="mdl-cell mdl-cell--12-col">RIWAYAT JABATAN PEGAWAI</h4>
			<table class="table table-bordered table-hover">
                <tbody>
					<thead>
						<tr>   
							<th height="30px" data-field="name">No</th>
							<th height="30px" data-field="name">Unit Kerja</th>
							<th height="30px" data-field="name">Nama Jabatan</th>
							<th height="30px" data-field="name">Esselon</th>
							<th height="30px" data-field="name">TMT Jabatan</th>
							<th height="30px" data-field="name">Nomor SK</th>               
						</tr>    
					</thead>
                    <?php
					$no = 1;
                    foreach ($riwayat_jabatan as $riwayat_jabatan) {
                        ?>
                        <tr>
							<td height="30px"><?php echo $no; ?></td>                    
							<td height="30px"><?php echo $riwayat_jabatan->unit_kerja; ?></td>                    
							<td height="30px"><?php echo $riwayat_jabatan->nama_jabatan; ?></td>                    
							<td height="30px"><?php echo $riwayat_jabatan->esselon; ?></td>                    
							<td height="30px"><?php if($riwayat_jabatan->tmt==""){
														
												    }else if($riwayat_jabatan->tmt=="0000-00-00"){
														
													}
													else{
														$tmt = date("d-m-Y",strtotime($riwayat_jabatan->tmt));
														echo $tmt;
											   }?></td>                          
							<td height="30px"><?php echo $riwayat_jabatan->no_sk; ?></td>                    
							             
                        </tr> 
                    <?php $no++; } ?> 
                </tbody>
            </table><br><br>
			
			<h4 class="mdl-cell mdl-cell--12-col">RIWAYAT DIKLAT PEGAWAI</h4>
			<table class="table table-bordered table-hover">
                <tbody>
					<thead>
						<tr>   
							<th height="30px" data-field="name">No</th>
							<th height="30px" data-field="name">No. Diklat</th>
							<th height="30px" data-field="name">Nama Diklat</th>
							<th height="30px" data-field="name">tahun</th>               
						</tr>    
					</thead>
                    <?php
					$no = 1;
                    foreach ($riwayat_diklat as $riwayat_diklat) {
                        ?>
                        <tr>
							<td height="30px"><?php echo $no; ?></td>                    
							<td height="30px"><?php echo $riwayat_diklat->no_diklat; ?></td>                    
							<td height="50px"><?php echo $riwayat_diklat->nama_diklat; ?></td>                                   
							<td height="30px"><?php echo $riwayat_diklat->tahun; ?></td>                                   
                        </tr> 
                    <?php $no++; } ?> 
                </tbody>
            </table><br><br>
			
			<h4 class="mdl-cell mdl-cell--12-col">RIWAYAT PENGHARGAAN PEGAWAI</h4>
			<table class="table table-bordered table-hover">
                <tbody>
					<thead>
						<tr>   
							<th height="30px" data-field="name">No</th>
							<th height="30px" data-field="name">Penghargaan</th>
							<th height="30px" data-field="name">Nomor SK</th>
							<th height="30px" data-field="name">Tanggal SK</th>
							<th height="30px" data-field="name">Tahun</th>         
						</tr>    
					</thead>
                    <?php
					$no = 1;
                    foreach ($riwayat_penghargaan as $riwayat_penghargaan) {
                        ?>
                        <tr>
							<td height="30px"><?php echo $no; ?></td>                    
							<td height="30px"><?php echo $riwayat_penghargaan->penghargaan; ?></td>                   
							<td height="30px"><?php echo $riwayat_penghargaan->no_surat_kerja; ?></td>                   
							<td height="30px"><?php if($riwayat_penghargaan->tgl_surat_kerja==""){
														
												    }else if($riwayat_penghargaan->tgl_surat_kerja=="0000-00-00"){
														
													}
													else{
														$tgl_surat_kerja = date("d-m-Y",strtotime($riwayat_penghargaan->tgl_surat_kerja));
														echo $tgl_surat_kerja;
											   }?></td>   							
							<td height="30px"><?php echo $riwayat_penghargaan->tahun; ?></td>                   
							             
                        </tr> 
                    <?php $no++; } ?> 
                </tbody>
            </table><br><br>
			
			<h4 class="mdl-cell mdl-cell--12-col">RIWAYAT HUKUMAN PEGAWAI</h4>
			<table class="table table-bordered table-hover">
                <tbody>
					<thead>
						<tr>   
							<th height="30px" data-field="name">No</th>
							<th height="30px" data-field="name">Hukuman</th>
							<th height="30px" data-field="name">Uraian Hukuman</th>
							<th height="30px" data-field="name">Nomor SK</th>
							<th height="30px" data-field="name">Tanggal SK</th>         
							<th height="30px" data-field="name">Pejabat</th>         
						</tr>    
					</thead>
                    <?php
					$no = 1;
                    foreach ($riwayat_hukuman as $riwayat_hukuman) {
                        ?>
                        <tr>
							<td height="30px"><?php echo $no; ?></td>                    
							<td height="30px"><?php echo $riwayat_hukuman->hukuman; ?></td>                  
							<td height="30px"><?php echo $riwayat_hukuman->uraian_hukuman; ?></td>                  
							<td height="30px"><?php echo $riwayat_hukuman->no_surat_kerja; ?></td>                  
							<td height="30px"><?php if($riwayat_hukuman->tgl_surat_kerja==""){
															
														}else if($riwayat_hukuman->tgl_surat_kerja=="0000-00-00"){
															
														}
														else{
															$tgl_surat_kerja = date("d-m-Y",strtotime($riwayat_hukuman->tgl_surat_kerja));
															echo $tgl_surat_kerja;
														}
											   ?></td>                  
							<td height="30px"><?php echo $riwayat_hukuman->pejabat; ?></td>                  
							             
                        </tr> 
                    <?php $no++; } ?> 
                </tbody>
            </table><br>
</div>  

<div class="box-footer clearfix">
    <?php //echo $jum_penguji;  ?>
</div>
</div>    

</body>
