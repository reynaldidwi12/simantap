<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b><?php echo lang('footer_version'); ?></b> 1.0
                </div>
                <strong><?php echo lang('footer_copyright'); ?> &copy; 2016 Badan Kepegawaian Daerah Kota Kendari & <a href="http://technos-studio.com" target="_blank">Techno's Studio</a>.</strong> <?php echo lang('footer_all_rights_reserved'); ?>.
            </footer>
        </div>

		
        <script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url($plugins_dir . '/slimscroll/slimscroll.min.js'); ?>"></script>
		<!-- datepicker -->
		<script src="<?php echo base_url($plugins_dir . '/jQuery/jquery-2.2.3.min.js'); ?>"></script>
		<script type="text/javascript">
		   $(function() {
			 $('#datepicker').datepicker({
				 format:'dd-mm-yyyy',
				 autoclose: true
			});
		   });
		 </script>
		 <script type="text/javascript">
		   $(function() {
			 $('#datepicker1').datepicker({
				 format:'dd-mm-yyyy',
				 autoclose: true
			});
		   });
		 </script>
        <script src="<?php echo base_url($plugins_dir . '/datepicker/bootstrap-datepicker.js'); ?>"></script>
		
<?php if ($mobile == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/fastclick/fastclick.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($admin_prefs['transition_page'] == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/animsition/animsition.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'users' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/pwstrength/pwstrength.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'groups' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/tinycolor/tinycolor.min.js'); ?>"></script>
        <script src="<?php echo base_url($plugins_dir . '/colorpickersliders/colorpickersliders.min.js'); ?>"></script>
<?php endif; ?>
        <script src="<?php echo base_url($frameworks_dir . '/adminlte/js/adminlte.min.js'); ?>"></script>
        <script src="<?php echo base_url($frameworks_dir . '/domprojects/js/dp.min.js'); ?>"></script>
        <script type="text/javascript">
        	function setConfirm(message,url,year,nip,subCategory) {
        		var modal = $("#confirm_modal");
			    modal.find(".message").html(message);
				modal.find("#modal-form").prop('action',url);
			    modal.find("#year").val(year);
			    modal.find("#nip").val(nip);
			    modal.find("#sub-kategori").val(subCategory);
			}
        </script>
    </body>
</html>

<!-- modal -->
<div id="confirm_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
    	<form action="" method="post" id="modal-form">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <h4 class="modal-title">Apakah anda yakin?</h4>
	            </div>
	            <div class="modal-body">
	                <p class="message"></p>
	            </div>
	            <div class="modal-footer">
	            	<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
	                <button type="submit" class="btn btn-small btn-primary">Kirim</a>
	                <input type="hidden" name="submit" value="1" />
					<input type="hidden" id="year" name="year" value="" />
					<input type="hidden" id="nip" name="nip" value="" />
					<input type="hidden" id="sub-kategori" name="sub_kategori" value="" />
	            </div>
	        </div>
	    </form>
    </div>
</div>