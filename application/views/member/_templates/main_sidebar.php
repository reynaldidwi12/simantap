<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <aside class="main-sidebar">
                <section class="sidebar">
<?php if ($admin_prefs['user_panel'] == TRUE): ?>
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url($avatar_dir . '/m_001.png'); ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $user_login['firstname'].$user_login['lastname']; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo lang('menu_online'); ?></a>
                        </div>
                    </div>

<?php endif; ?>
<?php if ($admin_prefs['sidebar_form'] == TRUE): ?>
                    <!-- Search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="<?php echo lang('menu_search'); ?>...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
<?php endif; ?>
                    <!-- Sidebar menu -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?php echo site_url('/'); ?>">
                                <i class="fa fa-home text-primary"></i> <span><?php echo lang('menu_access_website'); ?></span>
                            </a>
                        </li>

                        <li class="header text-uppercase"><?php echo lang('menu_main_navigation'); ?></li>
                        <li class="<?=active_link_controller('dashboard')?>">
                            <a href="<?php echo site_url('admin/dashboard'); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo lang('menu_dashboard'); ?></span>
                            </a>
                        </li>

                        <!-- Menu Peta Jabatan -->
                        <?php if($this->session->userdata('set_passwd') <> 0) { ?>
                        <li class="<?=active_link_controller('petajabatan')?>">
                            <a href="<?php echo site_url('member/file_manager/user/'.$this->session->userdata('user_name')); ?>">
                                <i class="fa fa-file-archive-o"></i> <span><?php echo lang('menu_arsip_digital'); ?></span>
                            </a>
                        </li>
                        <?php } ?>
                        <!-- End Menu Peta Jabatan -->

                        <!-- Menu Pengajuan -->
                        <li class="treeview <?=active_link_controller('pengajuan')?>">
                            <a href="">
                                <i class="fa fa-list"></i>
                                <span><?php echo lang('menu_pengajuan'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('member/pengajuan_kenaikan/user/'.$this->session->userdata('user_name')); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_pengajuan_kenaikan'); ?></a></li>
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('member/pengajuan_pensiun/user/'.$this->session->userdata('user_name')); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_pengajuan_pensiun'); ?></a></li>
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('member/pengajuan_kgb/user/'.$this->session->userdata('user_name')); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_pengajuan_kgb'); ?></a></li>
                            </ul>
                        </li>
                        <!-- End Menu Pengajuan -->
                        
                    </ul>
                </section>
            </aside>
