<div class="content-wrapper">
	<section class="content-header">
	</section>
		<section class="content">
			<div class="row">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3>
						<i class="fa fa-user"></i> Arsip Digital
					</h3>
				</div>
				<div class="panel-body">
					<iframe src="<?php echo base_url('member/file_manager/manager/' . $userNip); ?>" style="width: 100%; height: 480px; border: none;"></iframe>
				</div>
					<div class="box-footer">
							<?php 
							if($this->uri->segment(4)){
									echo anchor('setup/pegawai/find/'.$this->uri->segment(4).'/nip', 
									'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Kembali </button>' );
							}else{
									echo anchor('setup/pegawai', 
									'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Kembali </button>' );
							}
							?>
					
				</div>
			</div>
			</div>
		</section>
</div>
