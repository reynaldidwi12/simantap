<?php
	if(!empty($data)) { ?>
		<?php $year = date('Y'); ?>
		<div class="parent-item">
			<?php
			foreach ($data as $value) { ?>
				<?php

				$filePath = $topDirectory . '/' . $userNip . '/pensiun/' . $year;
				$fileName = $value->sub_kategori . '_' . $value->no_urut . '_' . $value->nama_file . '.pdf';
				$fileExist = $filePath . '/' . $fileName;
				if (!file_exists($fileExist)) {
				    $status = 'item-not-found';
				} else {
					$status = '';
				}
				?>
				<div class="sub-item <?=$status;?>">
					<?php echo $value->nama_berkas; ?>
					<p class="sub-item-detail">
						Format File: <?php echo $value->sub_kategori; ?>_<?php echo $value->no_urut; ?>_<?php echo $value->nama_file; ?>.pdf
					</p>
				</div>
			<?php } ?>
		</div>
		<div class="btn-box">
			<a class="btn btn-org" id="submit" onclick="setConfirm('Pastikan data sudah sesuai, guna cepatnya proses pengajuan.','<?=base_url('member/pengajuan_pensiun/kirim/'.$this->uri->segment(4));?>','<?=$year;?>','<?=$this->uri->segment(4);?>','<?=$subKategori;?>')" disabled>Kirim Data</a>
		</div>
	<?php }
?>