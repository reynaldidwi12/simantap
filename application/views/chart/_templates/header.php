<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!doctype html>
<html lang="<?php echo $lang; ?>">
    <head>
        <meta charset="<?php echo $charset; ?>">
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="google" content="notranslate">
        <meta name="robots" content="noindex, nofollow">

        <link rel="icon" href="<?php echo base_url('assets/image/kendari.png'); ?>">
        
        <script src="<?php  echo base_url($frameworks_dir . '/bootstrap/js/jquery.min.js'); ?>"></script>

        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/font-awesome/css/font-awesome.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/orgchart/css/jquery.orgchart.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/orgchart/css/navbar-static-top.css'); ?>"> 

          <!-- <style type="text/css">
            #chart-container { text-align: left; }
            .orgchart .node .edge { display: none; }
          </style> -->

        <style type="text/css">

          #chart-container { 
            background-color: #fff;
            width: auto;
            min-width: 100%;
            min-height: 100%;
          }

          .orgchart { 
            width: auto; 
            min-width: 80%;
            min-height: 750px;
            background-color: #fff;
            background-image: unset;
            text-align: center;
          }

          .orgchart .node .edge { display: none; }

          .orgchart .node { 
            width: 150px; 
          } 
          .orgchart .node .title {
              height: auto;
              position: relative;
              
              padding-left: 5px;
              padding-right: 5px;
              padding-top: 5px;
              padding-bottom: 5px;
              text-align: center;
              font-size: 8px;
              font-weight: bold;
              line-height: 20px;
              text-overflow: ellipsis;
              white-space: unset;
              color: #fff;
              border-radius: 4px 4px 0 0;
          }

          .orgchart .node .content {
              height: auto;
              
              padding-left: 5px;
              padding-right: 5px;
              padding-top: 5px;
              padding-bottom: 5px;
              text-align: center;
              font-size: 9px;
              line-height: 20px;
              text-overflow: ellipsis;
              white-space: unset;
          }


        </style>

<?php if ($mobile === FALSE): ?>
        <!--[if lt IE 9]>
            <script src="<?php echo base_url($plugins_dir . '/html5shiv/html5shiv.min.js'); ?>"></script>
            <script src="<?php echo base_url($plugins_dir . '/respond/respond.min.js'); ?>"></script>
        <![endif]-->
<?php endif; ?>
    </head>

    <body>
