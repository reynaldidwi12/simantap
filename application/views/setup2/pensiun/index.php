<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Data Pegawai Yang Akan Pensiun
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('report2/report_pensiun/add', 
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="fa fa-print"></i> Cetak</button>' );?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th class="col-md-1"><center>No</center></th>
				<th class="col-md-2">NIP/NIP Lama</th>
				<th class="col-md-4">Nama</th>
				<th class="col-md-2">TTL</th>
				<th class="col-md-2">Jenis Kelamin</th>
				<th class="col-md-2">Umur</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($pegawai){
	    		$i=1;
				foreach ( $pegawai as $row ) {
			?>
		        <tr class="gradeX">
		        <td class="col-md-1"><?php echo $i?></td>
		        <td class="col-md-2"><?php echo $row->nip." / ".$row->nip_lama?></td>
		        <td class="col-md-3"><?php echo $row->nama?></td>
		        <td class="col-md-1"><?php echo $row->tempat_lahir.", ".$row->tgl_lahir?></td>
		        <td class="col-md-1"><?php echo $row->kelamin?></td>
		        <td class="col-md-1"><?php echo $row->umur?></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='6'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
