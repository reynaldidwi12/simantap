<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data Kepangkatan</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
			if($this->uri->segment(3)=="add"){
				echo form_open('setup2/riwayat_kepangkatan/add/'.$this->uri->segment(4));
			}else if($this->uri->segment(3)=="modify"){
				echo form_open('setup2/riwayat_kepangkatan/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
			}else{
				echo form_open('setup2/riwayat_kepangkatan/detail');
			}
			?>
				<div class="box-body">
					<div class="col-lg-6">
						<div class="form-group">
							<label >NIP.</label>
							<?php echo form_input($nip)?>
						</div>
						<div class="form-group">
							<label >No. SK</label>
							<?php echo form_input($no_surat_kerja)?>
						</div>
						<div class="form-group">
							<label >Masa Kerja Bulan</label>
							<?php echo form_input($mk_bulan)?>
						</div>
						<div class="form-group">
							<label >Golongan.</label>
							<?php $option = array(
									''=>'',
									'I/a'=>'I/a',
									'I/b'=>'I/b',
									'I/c'=>'I/c',
									'I/d'=>'I/d',
									'II/a'=>'II/a',
									'II/b'=>'II/b',
									'II/c'=>'II/c',
									'II/d'=>'II/d',
									'III/a'=>'III/a',
									'III/b'=>'III/b',
									'III/c'=>'III/c',
									'III/d'=>'III/d',
									'IV/a'=>'IV/a',
									'IV/b'=>'IV/b',
									'IV/c'=>'IV/c',
									'IV/d'=>'IV/d',
									'IV/e'=>'IV/e',
							); 
							echo form_dropdown($golongan,$option,$get_golongan);?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Nama Pegawai.</label>
							<?php echo form_input($nama)?>
						</div>
						<div class="form-group">
							<label >Tgl. SK</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tgl_surat_kerja)?>
							</div>
						</div>
						<div class="form-group">
							<label >Masa Kerja Tahun</label>
							<?php echo form_input($mk_tahun)?>
						</div>
						<div class="form-group">
							<label >TMT</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tmt_pangkat)?>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('setup2/riwayat_kepangkatan/result_riwayat_kepangkatan/'.$this->uri->segment(4),
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>