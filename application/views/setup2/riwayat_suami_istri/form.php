<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data Suami/Istri</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			
				<div class="box-body">
					<div class="col-lg-6">
						<?php echo form_open("setup2/riwayat_suami_istri/cek/".$this->uri->segment(4).'/'.$this->uri->segment(5));?>
						<div class="form-group">
							<label >NIP</label>
							<?php echo form_input($nip)?>
						</div>
						<div class="form-group">
							<label >PNS</label>
							<?php echo form_input($nip2)?>
						</div>
						<div class="btn-group">
								<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
							</div>
						<?php echo form_close();?>
					</div>
						
						<?php 
						if($this->uri->segment(3)=="add"){
							echo form_open('setup2/riwayat_suami_istri/add');
						}else if($this->uri->segment(3)=="modify"){
							echo form_open('setup2/riwayat_suami_istri/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
						}else if($this->uri->segment(3)=="cek"){
							if($this->uri->segment(5)){
								echo form_open('setup2/riwayat_suami_istri/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
							}else{
								echo form_open('setup2/riwayat_suami_istri/add');
							}
							
						}else{
							echo form_open('setup2/riwayat_suami_istri/detail');
						}
						?>
						
					<div class="col-lg-6">
						<div class="form-group">
							<?php echo form_input($seq_no)?>
							<?php echo form_input($nip_)?>
							<label >Nama Pegawai</label>
							<?php echo form_input($nama)?>
						</div>
						<div class="form-group">
							<label >Nama Suami/Istri</label>
							<?php echo form_input($nama2)?>
						</div>
						<div class="form-group">
							<label >Tempat Lahir</label>
							<?php echo form_input($tempat_lahir)?>
						</div>
						<div class="form-group">
							<label >Tanggal Lahir</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tgl_lahir)?>	
							</div>
						</div>
						<div class="form-group">
							<label >Tanggal Menikah</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tgl_menikah)?>	
							</div>
						</div>
						<div class="form-group">
							<label >Tanggal Meninggal</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tgl_meninggal)?>	
							</div>
						</div>
						<div class="form-group">
							<label >Tanggal Cerai</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tgl_cerai)?>	
							</div>
						</div>
						<div class="form-group">
							<label >No. Akta Nikah</label>
							<?php echo form_input($no_akta_nikah)?>
						</div>
						<div class="form-group">
							<label >No. Akta Meninggal</label>
							<?php echo form_input($no_akta_meninggal)?>
						</div>
						<div class="form-group">
							<label >No. Akta Cerai</label>
							<?php echo form_input($no_akta_cerai)?>
						</div>
						<div class="form-group">
							<label >Status</label>
							<?php $option = array(
									''=>'',
									'1'=>'Menikah',
									'2'=>'Cerai',
									'3'=>'Janda/Duda'
							); 
							echo form_dropdown($status,$option,$get_status);?>
						</div>
						<div class="form-group">
							<label >BPJS</label>
							<?php echo form_input($bpjs)?>
						</div>
					</div>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('setup2/riwayat_keluarga/result_riwayat_keluarga/'.$this->uri->segment(4), 
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>