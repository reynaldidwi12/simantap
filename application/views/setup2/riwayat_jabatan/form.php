<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
<section class="content-header">
<div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Data Jabatan</h3>
</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-solid box-default">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="box-body">
			<div class="alert alert-danger alert-dismissible">
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>
			<?php echo $this->session->flashdata('message'); ?>
			</div>
			</div>
			<?php }?>
			<div class="panel-body">
			<?php 
			if($this->uri->segment(3)=="add"){
				echo form_open('setup2/riwayat_jabatan/add/'.$this->uri->segment(4),"name='jabatan'");
			}else if($this->uri->segment(3)=="modify"){
				echo form_open('setup2/riwayat_jabatan/modify/'.$this->uri->segment(4).'/'.$this->uri->segment(5),"name='jabatan'");
			}else{
				echo form_open('setup2/riwayat_jabatan/detail');
			}
			?>
				<div class="box-body">
					<div class="col-lg-6">
						<?php echo form_input($seq_no)?>
						<div class="form-group">
							<label >NIP.</label>
							<?php echo form_input($nip)?>
						</div>
						<div class="form-group">
							<label >Jenis Jabatan</label>
							<?php $option = array(
									''=>'',
									'Struktural'=>'Struktural',
									'Fungsional Tertentu'=>'Fungsional Tertentu',
									'Fungsional Umum'=>'Fungsional Umum'
							); 
							echo form_dropdown($jenis_jabatan,$option,$get_jenis_jabatan,'onChange="tampilNamaJabatan()"');?>
						</div>
						<div class="form-group">
							<label >Nama Jabatan</label>
							<?php echo form_dropdown("nama_jabatan", $jabatan, $get_jabatan,' id="nama_jabatan" class="form-control" data-live-search="true"  data-live-search-style="begins" required');?>
						</div>
						<div class="form-group">
							<label >SKPD</label>
							<?php echo form_dropdown($kd_skpd, $nama_skpd, $get_nama_skpd,' class="form-control selectpicker" onChange="tampilUnitkerja()" data-live-search="true" data-live-search-style="begins" title="Pilih SKPD" required');?>
						</div>
						<div class="form-group">
							<label >Unit Kerja</label>
							<?php echo form_dropdown($kd_unitkerja, $nama_unitkerja,  $get_nama_unitkerja,' class="form-control"  data-live-search="true" data-live-search-style="begins"');?>
						</div>
						
						
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label >Nama Pegawai.</label>
							<?php echo form_input($nama)?>
						</div>
						<div class="form-group">
							<label >No. SK</label>
							<?php echo form_input($no_sk)?>
						</div>
						<div class="form-group">
							<label >Esselon</label>
							<?php $option = array(
									''=>'',
									'11'=>'I a',
									'12'=>'I b',
									'21'=>'II a',
									'22'=>'II b',
									'31'=>'III a',
									'32'=>'III b',
									'41'=>'IV a',
									'42'=>'IV b',
									'51'=>'V a',
							); 
							if($get_jenis_jabatan=='Struktural'){
								echo form_dropdown($esselon,$option,$get_esselon);
							}else{
								echo form_dropdown($esselon,$option,$get_esselon,'disabled');
							}?>
						</div>
						<div class="form-group">
							<label >Tgl. SK</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<?php echo form_input($tmt)?>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
					</div>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="submit" value="Simpan"/>&nbsp;
					<input class="btn btn-warning" type="reset" name="reset" value="Reset"/>&nbsp;
					<?php echo anchor('setup2/riwayat_jabatan/result_riwayat_jabatan/'.$this->uri->segment(4),
					'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Refresh"> Cancel </button>' );?>
				</div>
				<?php echo form_close()?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/jquery-2.1.1.js'); ?>"></script>
<script>

function tampilNamaJabatan(){
   if (document.jabatan.jenis_jabatan.value == "Struktural")
    {     
		document.getElementById("esselon").disabled=false;
		jenis_jabatan = document.getElementById("jenis_jabatan").value;
		 $.ajax({
			 url:"<?php echo base_url();?>setup/riwayat_jabatan/get_namajabatan/"+jenis_jabatan+"",
			 success: function(response){
			 $("#nama_jabatan").html(response);
			 },
			 dataType:"html"
		 });
		 return false;
    }
   else
    {
		document.getElementById("esselon").disabled=true;
		document.getElementById('esselon').value='' ; 
		jenis_jabatan = document.getElementById("jenis_jabatan").value;
		 $.ajax({
			 url:"<?php echo base_url();?>setup/riwayat_jabatan/get_namajabatan/"+jenis_jabatan+"",
			 success: function(response){
			 $("#nama_jabatan").html(response);
			 },
			 dataType:"html"
		 });
		 return false;
    }
   }
   
   
function tampilUnitkerja(){
	 kd_skpd = document.getElementById("kd_skpd").value;
	 $.ajax({
		 url:"<?php echo base_url();?>setup/riwayat_jabatan/get_unitkerja/"+kd_skpd+"",
		 success: function(response){
		 $("#kd_unitkerja").html(response);
		 },
		 dataType:"html"
	 });
	 return false;
 }
</script>