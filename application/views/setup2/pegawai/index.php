<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Data Pegawai
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<!--<?php echo anchor('setup2/pegawai/add',
					'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data Pegawai"><i class="fa fa-plus"></i> Tambah Pegawai</button>' );?>
					&nbsp;
					<?php echo anchor('setup2/pegawai',
					'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>-->
					</div>
				</div>
				<?php echo form_open("setup2/pegawai/find");?>
				<div class="col-md-5 col-sm-4 col-xs-4">
					<div class='col-md-5'>
					<?php $option = array(
							'nip'=>'NIP Baru',
							'nama'=>'Nama'
					);
					echo form_dropdown('column',$option,'1','class="form-control"');?>
					</div>
					<div class="col-md-7"><input type="text" class="form-control" name="data" placeholder="Cari"></div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:5%"><center>No</th>
				<th style="width:10%"><center>Foto</th>
				<th style="width:15%"><center>NIP/NIP Lama</th>
				<th style="width:20%"><center>Nama</th>
				<th style="width:25%"><center>TTL</th>
				<th style="width:8%"><center>Jenis Kelamin</th>

				<th style="width:25%"><center>Pilihan</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($pegawai){
	    		$i=1;
				foreach ( $pegawai as $row ) {

				if($row->status_pegawai=="Pindah Tugas"){
					$a = "#fbf8c9";
				}else if($row->status_pegawai=="Meninggal"){
					$a = "#ffd3d3";
				}else{
					$a = "";
				}

				if($row->tgl_lahir){
					$TglLahir = date("d-m-Y",strtotime($row->tgl_lahir));
				}else{}
			?>
		        <tr class="gradeX">
		        <td bgcolor=<?php echo $a;?> ><?php echo $i?></td>
		        <td bgcolor=<?php echo $a;?> ><center>
					<?php if($row->picture){?>
								<img src='<?php echo base_url();?>files/foto_pegawai/<?php echo $row->picture;?>' width="110" height="150"/>
							<?php
					}else{ ?>
								<img src='<?php echo base_url();?>files/foto_pegawai/image.png' width="110" height="150"/>
					<?php } ?>
					</center>
				</td>
		        <td bgcolor=<?php echo $a;?>><?php echo $row->nip." / ".$row->nip_lama?></td>
		        <td bgcolor=<?php echo $a;?>><?php echo $row->nama?></td>
		        <td bgcolor=<?php echo $a;?>><?php echo $row->tempat_lahir.", ".$TglLahir?></td>
		        <td bgcolor=<?php echo $a;?>><?php echo $row->kelamin?></td>
		        <td  align="center" bgcolor=<?php echo $a;?>>
		        <a href="<?php echo base_url('setup2/pegawai/modify/'.$row->nip) ?>"><button class="btn btn-warning" title="Edit">Data Utama<!--i class="fa fa-edit"--></i></button></a><br>
		        <!--a href="<?php echo base_url('setup2/pegawai/remove/'.$row->nip) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger" title="Delete"><i class="fa fa-remove"></i></button></a-->
				<div class="btn-group">
                              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">Daftar Riwayat<!--i class="fa fa-bars"--></i></button>
                              <ul class="dropdown-menu pull-right" role="menu">
								  <li><?php echo anchor("setup2/riwayat_pendidikan/result_riwayat_pendidikan/".$row->nip, "<i class='fa fa-bars'></i>Riwayat Pendidikan"); ?></li>
								  <li><?php echo anchor("setup2/riwayat_kepangkatan/result_riwayat_kepangkatan/".$row->nip, "<i class='fa fa-bars'></i>Riwayat Kepangkatan"); ?></li>
								  <li><?php echo anchor("setup2/riwayat_jabatan/result_riwayat_jabatan/".$row->nip, "<i class='fa fa-bars'></i>Riwayat Jabatan"); ?></li>
								  <li><?php echo anchor("setup2/riwayat_diklat/result_riwayat_diklat/".$row->nip, "<i class='fa fa-bars'></i>Riwayat Diklat"); ?></li>
								  <li><?php echo anchor("setup2/riwayat_penghargaan/result_riwayat_penghargaan/".$row->nip, "<i class='fa fa-bars'></i>Riwayat Penghargaan"); ?></li>
								  <li><?php echo anchor("setup2/riwayat_hukuman/result_riwayat_hukuman/".$row->nip, "<i class='fa fa-bars'></i>Riwayat Hukuman"); ?></li>
								  <li><?php echo anchor("setup2/riwayat_keluarga/result_riwayat_keluarga/".$row->nip, "<i class='fa fa-bars'></i>Riwayat Keluarga"); ?></li>
							  </ul>
                </div><br>
				<a href="<?php echo base_url('setup2/pegawai/report_rh/'.$row->nip) ?>"><button class="btn btn-primary" title="Cetak Riwayat Pegawai">Cetak Data<!--i class="fa fa-print"--></i></button></a>
				<div class="btn-group">
					<a href="<?php echo base_url('setup2/file_manager/user/'.$row->nip) ?>" class="btn btn-default">Arsip Digital</a>
				</div><br>
				</td>
				</tr>
	        <?php
	        	$i++;
				}
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='7'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
