<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> Riwayat Penghargaan
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			
		</div>
		<div class="col-sm-12">
			<div class="row">
			<?php echo form_open("setup2/riwayat_penghargaan/index",array('method'=>'POST'));?>
					<div class="box-body">
					<div class="col-sm-3">
						<div class="form-group">
							<label >NIP.</label>
							<input type="text" name="nip" class="form-control" value="<?php echo $nip?>" readonly>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="form-group">
							<label >Nama.</label>
							<input type="text" name="nama" class="form-control" value="<?php echo $nama_pegawai?>" readonly>
						</div>
					</div>
					</div>
			<?php echo form_close();?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:1%;">No</th>
				<th style="width:25%;">Penghargaan</th>
				<th style="width:25%;">No. Surat</th>
				<th style="width:20%;">Tgl Surat</th>
				<th style="width:15%;">Tahun</th>
				<th style="width:10%;"><center>Pilihan</center></th>			
			</tr>
			</thead>
			<tbody>
			<?php if($riwayat){ ?>
	    	<?php
	    		$i=1;
				foreach ( $riwayat as $row ) {
			?>
		        <tr class="gradeX">
		        <td ><?php echo $i?></td>
		        <td ><?php echo $row->penghargaan?></td>
		        <td ><?php echo $row->no_surat_kerja?></td>
		        <td ><?php 

		        	if($row->tgl_surat_kerja){
						$tgl_surat_kerja = date("d-m-Y",strtotime($row->tgl_surat_kerja));
						echo $tgl_surat_kerja;							
					}else{}

				?></td>
		        <td ><?php echo $row->tahun?></td>
		        <td ><center>
		        <a href="<?php echo base_url('setup2/riwayat_penghargaan/modify/'.$row->nip.'/'.$row->seq_no) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
		        &nbsp;
		        <a href="<?php echo base_url('setup2/riwayat_penghargaan/remove/'.$row->nip.'/'.$row->seq_no) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
				</center></td>
				</tr>
	        <?php $i++; 
				}
			}else{
	        	echo "<tr class=\"gradeX\"><td colspan='8'>No Record</td></tr>";
	        }?> 
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
		<div class=row>
			<div class="col-md-6 col-sm-4 col-xs-4">
				<div class="btn-group">
				<?php echo anchor('setup2/riwayat_penghargaan/add/'.$this->uri->segment(4), 
				'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data Pegawai"><i class="fa fa-plus"></i> Tambah Riwayat</button>' );?>
				&nbsp;
				<?php echo anchor('setup2/riwayat_penghargaan/result_riwayat_penghargaan/'.$this->uri->segment(4), 
				'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
				&nbsp;
				<?php echo anchor('setup2/pegawai/find/'.$this->uri->segment(4).'/nip', 
				'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Kembali"> Kembali </button>' );?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</section>
</div>
