<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>elFinder 2.1</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2" />

        <!-- Require JS (REQUIRED) -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>
        <script>
        define('elFinderConfig', {
            defaultOpts : {
                url : '<?php echo $connector ?>'
                ,rememberLastDir: false
                ,uiOptions: {
                  toolbar : [
                    ['back', 'forward'],
                    ['mkdir', 'mkfile', 'upload'],
                    ['open', 'download', 'getfile'],
                    ['info'],
                    ['quicklook'],
                    ['copy', 'cut', 'paste'],
                    ['rm'],
                    ['duplicate', 'rename', 'edit', 'resize'],
                    ['extract', 'archive'],
                    ['search'],
                    ['view'],
                    ['help']
                  ],
                }
                ,commandsOptions : {
                    edit : {
                        extraOptions : {
                            creativeCloudApiKey : '',
                            managerUrl : ''
                        }
                    }
                    ,quicklook : {
                        googleDocsMimes : ['application/pdf', 'image/tiff', 'application/vnd.ms-office', 'application/msword', 'application/vnd.ms-word', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
                    }
                }
                ,bootCallback : function(fm, extraObj) {
                    fm.bind('init', function() {
                    });
                    var title = document.title;
                    fm.bind('open', function() {
                        var path = '',
                            cwd  = fm.cwd();
                        if (cwd) {
                            path = fm.path(cwd.hash) || null;
                        }
                        document.title = path? path + ':' + title : title;
                    }).bind('destroy', function() {
                        document.title = title;
                    });
                }
               
            },
            managers : {
                'elfinder': {}
            }
        });
        define('returnVoid', void 0);
        (function(){
            var
                elver = '<?php echo elFinder::getApiFullVersion()?>',
                jqver = '3.2.1',
                uiver = '1.12.1',

                lang = (function() {
                    var locq = window.location.search,
                        fullLang, locm, lang;
                    if (locq && (locm = locq.match(/lang=([a-zA-Z_-]+)/))) {
                        fullLang = locm[1];
                    } else {
                        fullLang = (navigator.browserLanguage || navigator.language || navigator.userLanguage);
                    }
                    lang = fullLang.substr(0,2);
                    if (lang === 'ja') lang = 'jp';
                    else if (lang === 'pt') lang = 'pt_BR';
                    else if (lang === 'ug') lang = 'ug_CN';
                    else if (lang === 'zh') lang = (fullLang.substr(0,5).toLowerCase() === 'zh-tw')? 'zh_TW' : 'zh_CN';
                    return lang;
                })(),

                start = function(elFinder, editors, config) {
                    elFinder.prototype.loadCss('//cdnjs.cloudflare.com/ajax/libs/jqueryui/'+uiver+'/themes/smoothness/jquery-ui.css');

                    $(function() {
                        var optEditors = {
                                commandsOptions: {
                                    edit: {
                                        editors: Array.isArray(editors)? editors : []
                                    }
                                }
                            },
                            opts = {};

                        if (config && config.managers) {
                            $.each(config.managers, function(id, mOpts) {
                                opts = Object.assign(opts, config.defaultOpts || {});
                                try {
                                    mOpts.commandsOptions.edit.editors = mOpts.commandsOptions.edit.editors.concat(editors || []);
                                } catch(e) {
                                    Object.assign(mOpts, optEditors);
                                }
                                $('#' + id).elfinder(
                                    $.extend(true, { lang: lang }, opts, mOpts || {}),
                                    function(fm, extraObj) {
                                        fm.bind('init', function() {
                                            delete fm.options.rawStringDecoder;
                                            if (fm.lang === 'jp') {
                                                require(
                                                    [ 'encoding-japanese' ],
                                                    function(Encoding) {
                                                        if (Encoding.convert) {
                                                            fm.options.rawStringDecoder = function(s) {
                                                                return Encoding.convert(s,{to:'UNICODE',type:'string'});
                                                            };
                                                        }
                                                    }
                                                );
                                            }
                                        });
                                    }
                                );
                            });
                        } else {
                            alert('"elFinderConfig" object is wrong.');
                        }
                    });
                },

                load = function() {
                    require(
                        [
                            'elfinder'
                            , 'extras/editors.default'
                            , 'elFinderConfig'
                        ],
                        start,
                        function(error) {
                            alert(error.message);
                        }
                    );
                },

                ie8 = (typeof window.addEventListener === 'undefined' && typeof document.getElementsByClassName === 'undefined');

            require.config({
                baseUrl : '//cdnjs.cloudflare.com/ajax/libs/elfinder/'+elver+'/js',
                paths : {
                    'jquery'   : '//cdnjs.cloudflare.com/ajax/libs/jquery/'+(ie8? '1.12.4' : jqver)+'/jquery.min',
                    'jquery-ui': '//cdnjs.cloudflare.com/ajax/libs/jqueryui/'+uiver+'/jquery-ui.min',
                    'elfinder' : 'elfinder.min',
                    'encoding-japanese': '//cdn.rawgit.com/polygonplanet/encoding.js/master/encoding.min'
                },
                waitSeconds : 10
            });

            load();

        })();
        </script>
    </head>
    <body>

        <!-- Element where elFinder will be created (REQUIRED) -->
        <div id="elfinder" style="width:100%; height:100%; border: none;"></div>

    </body>
</html>
