<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <aside class="main-sidebar">
                <section class="sidebar">
<?php if ($admin_prefs['user_panel'] == TRUE): ?>
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url($avatar_dir . '/m_001.png'); ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $user_login['firstname'].$user_login['lastname']; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo lang('menu_online'); ?></a>
                        </div>
                    </div>

<?php endif; ?>
<?php if ($admin_prefs['sidebar_form'] == TRUE): ?>
                    <!-- Search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="<?php echo lang('menu_search'); ?>...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
<?php endif; ?>
                    <!-- Sidebar menu -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?php echo site_url('/'); ?>">
                                <i class="fa fa-home text-primary"></i> <span><?php echo lang('menu_access_website'); ?></span>
                            </a>
                        </li>

                        <li class="header text-uppercase"><?php echo lang('menu_main_navigation'); ?></li>
                        <li class="<?=active_link_controller('dashboard')?>">
                            <a href="<?php echo site_url('admin/dashboard'); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo lang('menu_dashboard'); ?></span>
                            </a>
                        </li>
                        
                        <li class="treeview <?=active_link_controller('prefs')?>">
                            <a href="">
                                <i class="fa fa-database"></i>
                                <span><?php echo lang('menu_kepegawaian'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/pensiun2'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_Pensiun'); ?></a></li>
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/meninggal'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_meninggal'); ?></a></li>
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/pindah_tugas_keluar'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_pindah_keluar'); ?></a></li>
                            </ul>
                        </li>

                        <!-- Menu Bezetting -->

                        <li class="treeview <?=active_link_controller('prefs')?>">
                            <a href="">
                                <i class="fa fa-file-text-o"></i>
                                <span><?php echo lang('menu_bezetting'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('bezetting/eselon'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_bezetting_eselon'); ?></a></li>
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('bezetting/golongan'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_bezetting_golongan'); ?></a></li>
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('bezetting/pendidikan'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_bezetting_pendidikan'); ?></a></li>
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('bezetting/diklat'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_bezetting_diklat'); ?></a></li>
                            </ul>
                        </li>

                        <!-- End Menu Bezetting -->

						<?php if($this->session->userdata('ss_skpd')=='Administrastor' || $this->session->userdata('group_id')=='1'||$this->session->userdata('group_id')=='4'){?>
							<li class="treeview <?=active_link_controller('files')?>">
								<a href="">
									<i class="fa fa-cogs"></i>
									<span><?php echo lang('menu_setup'); ?></span>
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
                                    <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/kepala_daerah'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_kepala_daerah'); ?></a></li>
									<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/skpd'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_skpd'); ?></a></li>
                                    <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/unit_organisasi'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_unit_organisasi'); ?></a></li>
									<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/unit_kerja'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_unit_kerja'); ?></a></li>
                                    <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/sub_unit_kerja'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_sub_unit_kerja'); ?></a></li>  
									<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/jabatan'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_jabatan'); ?></a></li>  
									<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/prodi'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_prodi'); ?></a></li>                                      	
									<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/kecamatan'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_kecamatan'); ?></a></li>                            	
									<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/kelurahan'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_kelurahan'); ?></a></li>
                                    <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('setup/daftar_pengajuan'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_daftar_pengajuan'); ?></a></li>                            	
									</ul>
							</li>
						<?php } else { ?>
						
						<?php } ?>
                        
                        <!-- Menu Peta Jabatan -->
                        
                        <li class="<?=active_link_controller('petajabatan')?>">
                            <a href="<?php echo site_url('petajabatan'); ?>">
                                <i class="fa fa-university"></i> <span><?php echo lang('menu_petajabatan'); ?></span>
                            </a>
                        </li>
						
                        <!-- End Menu Peta Jabatan -->

                        <?php if($this->session->userdata('ss_skpd')=='Administrastor' || $this->session->userdata('group_id')=='1'||$this->session->userdata('group_id')=='4'){?>

                            <!-- Menu Pengajuan -->
                        
                            <li class="<?=active_link_controller('pengajuan')?>">
                                <a href="<?php echo site_url('admin/pengajuan'); ?>">
                                    <i class="fa fa-check-square-o"></i> <span><?php echo lang('menu_pengajuan'); ?></span>
                                </a>
                            </li>
                            
                            <!-- End Menu Pengajuan -->

                            <!-- Menu Infografis -->
                        
                            <li class="<?=active_link_controller('infografis')?>">
                                <a href="<?php echo site_url('admin/infografis'); ?>">
                                    <i class="fa fa-pie-chart"></i> <span><?php echo lang('menu_infografis'); ?></span>
                                </a>
                            </li>
                            
                            <!-- End Menu Infografis -->

                        <?php } else { ?>
                        
                        <?php } ?>

                        <li class="treeview <?=active_link_controller('database')?>">
                        	<a href="">
                                <i class="fa fa-file"></i>
                                <span><?php echo lang('menu_report'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                            	<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('report/imports/'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_import'); ?></a></li>
                                <?php if($this->session->userdata('group_id')<>'3') { ?>
								<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('report/report_all/'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_report_all'); ?></a></li>
                            <?php } ?>
								<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('report/report_skpd/'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_report_skpd'); ?></a></li>
								<li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('report/report_pegawai/add'); ?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_report_pegawai'); ?></a></li>
                            </ul>
                        </li>

                        <!-- Menu API -->
                        <?php if($this->session->userdata('group_id')<>'3') { ?>
                        <li class="<?=active_link_controller('data')?>">
                            <a href="<?php echo site_url('api/data'); ?>">
                                <i class="fa fa-share"></i> <span><?php echo lang('menu_api'); ?></span>
                            </a>
                        </li>
                    <?php } ?>
                        <!-- End Menu API -->
	
						<?php if($this->session->userdata('ss_skpd')=='Administrastor'){?>
							<li class="header text-uppercase"><?php echo lang('menu_administration'); ?></li>
							<li class="<?=active_link_controller('users')?>">
								<a href="<?php echo site_url('admin/users'); ?>">
									<i class="fa fa-user"></i> <span><?php echo  lang('menu_users');  ?></span>
								</a>
							</li>
							<li class="<?=active_link_controller('groups')?>">
								<a href="<?php echo site_url('admin/groups'); ?>">
									<i class="fa fa-shield"></i> <span><?php echo lang('menu_security_groups'); ?></span>
								</a>
							</li>
						<?php } else { ?>
							<li class="header text-uppercase"><?php echo lang('menu_administration'); ?></li>
							<li class="<?=active_link_controller('users')?>">
								<a href="<?php echo site_url('admin/users'); ?>">
									<i class="fa fa-user"></i> <span><?php echo  lang('menu_users');  ?></span>
								</a>
							</li>
						<?php } ?>
                        
                    </ul>
                </section>
            </aside>
