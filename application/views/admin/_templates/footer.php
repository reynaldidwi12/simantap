<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b><?php echo lang('footer_version'); ?></b> 2.0
                </div>
                <strong><?php echo lang('footer_copyright'); ?> &copy; 2018 Badan Kepegawaian Daerah Kota Kendari.</strong> <?php echo lang('footer_all_rights_reserved'); ?>.
            </footer>
        </div>

		
        <script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <!-- <script src="<?php // echo base_url($frameworks_dir . '/jquery/jquery.min.js'); ?>"></script> -->
        <script src="<?php echo base_url($plugins_dir . '/slimscroll/slimscroll.min.js'); ?>"></script>
		<!-- datepicker -->
		<!-- <script src="<?php //echo base_url($plugins_dir . '/jQuery/jquery-2.2.3.min.js'); ?>"></script> -->
		<script src="<?php echo base_url($plugins_dir . '/datatables/jquery.dataTables.min.js'); ?>"></script>
		<script src="<?php echo base_url($plugins_dir . '/datatables/dataTables.bootstrap.min.js'); ?>"></script>

		<script type="text/javascript">
		   $(function() {
			 $('#datepicker').datepicker({
				 format:'dd-mm-yyyy',
				 autoclose: true
			});
		   });
		 </script>
		 <script type="text/javascript">
		   $(function() {
			 $('#datepicker1').datepicker({
				 format:'dd-mm-yyyy',
				 autoclose: true
			});
		   });
		 </script>
		 <script type="text/javascript">
		   $(function() {
			 $('#datepicker2').datepicker({
				 format:'dd-mm-yyyy',
				 autoclose: true
			});
		   });
		 </script>
		 <script type="text/javascript">
		   $(function() {
			 $('#datepicker3').datepicker({
				 format:'dd-mm-yyyy',
				 autoclose: true
			});
		   });
		 </script>
		 <script type="text/javascript">
		 	$(function(){
			    $("#table-datatables").dataTable({
			    	"order": [[ 5, "desc" ]]
			    });
			  })
		 </script>
        <script src="<?php echo base_url($plugins_dir . '/datepicker/bootstrap-datepicker.js'); ?>"></script>
		
<?php if ($mobile == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/fastclick/fastclick.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($admin_prefs['transition_page'] == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/animsition/animsition.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'users' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/pwstrength/pwstrength.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'groups' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/tinycolor/tinycolor.min.js'); ?>"></script>
        <script src="<?php echo base_url($plugins_dir . '/colorpickersliders/colorpickersliders.min.js'); ?>"></script>
<?php endif; ?>
        <script src="<?php echo base_url($frameworks_dir . '/adminlte/js/adminlte.min.js'); ?>"></script>
        <script src="<?php echo base_url($frameworks_dir . '/domprojects/js/dp.min.js'); ?>"></script>
        <script type="text/javascript">
        	function showPdf(url,title) {
        		var modal = $("#modal-pdf");
        		modal.find("#title").html(title);
				modal.find("#pdf-show").prop('src',url);
			}
        </script>
    </body>
</html>

<div id="modal-pdf" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title"></h4>
            </div>
            <div class="modal-body">
                <embed src="" id="pdf-show" frameborder="0" width="100%" height="400px">

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>