<style type="text/css">
	.btn-blue {
		background-color: #337ab7;
		border-color: #297093;
	}
	.btn-blue:hover,
	.btn-blue:focus {
		background-color: #297093;
		border-color: #297093;
	}
	td .btn {
		margin-right: 1px;
		margin-left: 1px;
	}
</style>
<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> DAFTAR PENGAJUAN
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<div class=row>
				<div class="col-md-6 col-sm-4 col-xs-4">
					<div class="btn-group">
					<?php echo anchor('admin/pengajuan', 
					'<button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
					</div>
				</div>
				<?php echo form_open("admin/pengajuan/find");?>
				<div class="col-md-5 col-sm-4 col-xs-4">
					<div class='col-md-5'>
					<?php $option = array(
						'nip'=>'NIP',
						'tahun'=>'Tahun',
						'kategori'=>'Kategori Pengajuan'
					); 
					echo form_dropdown('column',$option,'1','class="form-control"');?>
					</div>
					<div class="col-md-7"><input type="text" class="form-control" name="data" placeholder="Cari"></div>
				</div>
				<div class="btn-group">
					<input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th style="width:3%"><center>No</center></th>
				<th style="width:15%">NIP</th>
				<th style="width:5%">Tahun</th>
				<th style="width:6%">Kategori</th>
				<th style="width:6%">Sub Kategori</th>
				<th style="width:10%">Tanggal Masuk Berkas</th>
				<th style="width:10%">Tanggal Berkas Dikirim</th>
				<th style="width:12%">Status</th>
				<th style="width:10%">&nbsp;&nbsp;</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($pengajuan){
	    		$i=$number+1;
				foreach ( $pengajuan as $row ) {
					$tglmasuk = tgl_indo(date("Y-m-d", strtotime($row->tanggal_masuk)));

					if(!empty($row->tanggal_kirim)) {
						$tglkeluar = tgl_indo(date("Y-m-d", strtotime($row->tanggal_kirim)));
					} else {
						$tglkeluar = '-';
					}
			?>
		        <tr class="gradeX">
		        <td ><center><?php echo $i?>.</center></td>
		        <td ><?php echo $row->nip?></td>
				<td ><?php echo $row->tahun?></td>
				<td ><?php echo $row->kategori?></td>
				<td ><?php echo $row->sub_kategori?></td>
				<td ><?php echo $tglmasuk?></td>
				<td ><?php echo $tglkeluar?></td>
				<td >
					<?php
					$get_status = json_decode($row->status);
              		$keys = array_keys($get_status->data);
                 	$last_key = array_pop($keys);

                 	echo ucwords(convert_status_admin($get_status->data[$last_key]->status));
					?>
				</td>
		        <td ><center>
		        <?php if($get_status->data[$last_key]->status == 'berkas fisik diterima') { ?>
		        <a href="<?php echo base_url('admin/pengajuan/dibawa_ke_bkn/'.$row->id) ?>" onclick="return confirm('Apakah Anda Yakin ? Pastikan data sudah sesuai, dan notifikasi akan dikirim ke pegawai terkait.');"><button class="btn btn-success btn-blue btn-sm" title="Kirim"><i class="fa fa-send"></i></button></a>
				<?php } ?>
		        <a href="<?php echo base_url('admin/pengajuan/verify/'.$row->id.'/'.$this->uri->segment(4)) ?>"><button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
		        <a href="<?php echo base_url('admin/pengajuan/remove/'.$row->id) ?>" onclick="return confirm('Anda Yakin ?');"><button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-remove"></i></button></a>
				</center></td>
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='9'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<div align="right"><?php echo $links?> </div>
		</div>
	</div>
</div>
</div>
</section>
</div>
