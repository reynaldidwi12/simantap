<style type="text/css">
	.profil {
		padding: 30px 20px;
		border: 1px solid #ddd;
		border-radius: 10px;
		box-shadow: 0 0 10px rgba(0,0,0,0.2);
	}
	.profil-img {
		height: 120px;
		width: 120px;
		position: relative;
		margin: 0 auto 20px;
	}
	.profil-img img {
		height: 100%;
	    width: 100%;
	    object-position: 50% 50%;
	    o-object-fit: cover;
	    object-fit: cover;
	}
	.profil-desc p {
		margin-bottom: 0;
	}
	.table {
		border: 1px solid #ddd;
		border-radius: 10px;
		box-shadow: 0 0 10px rgba(0,0,0,0.2);
	}
	.table .btn-group .btn {
		border: 1px solid #ddd;
	}
	.table td {
		vertical-align: middle !important;
	}
	.btn-yes.active, .btn-yes:hover, .btn-yes:focus {
		background-color: #2cc66d;
		color: #fff; 
		box-shadow: none;
	}
	.btn-no.active, .btn-no:hover, .btn-no:focus {
		background-color: #f73e3e;
		color: #fff; 
		box-shadow: none;
	}
	.btn-blue {
		background-color: #337ab7;
		border-color: #297093;
	}
	.btn-blue:hover,
	.btn-blue:focus {
		background-color: #297093;
		border-color: #297093;
	}
</style>
<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>
			<i class="fa fa-user"></i> PENGAJUAN DETAIL
		</h3>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="col-sm-12">
			<h4 style="text-transform:uppercase;"><b><i class="fa fa-check-square-o"></i> Pengajuan <?=$pengajuan['kategori'];?> (<?=$pengajuan['sub_kategori'];?>) - <?=$pengajuan['tahun'];?></b></h4>
			<?php
			if(!empty($profil)) {
				foreach($profil as $row) {
			?>
			<section class="content">
				<div class="row">
					<div class="col-md-4">
						<div class="profil">
							<div class="profil-img">
				                <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url();?>files/foto_pegawai/<?php echo $row['picture'];?>" alt="<?php echo $row['nama']; ?>">
				            </div>
				            <div class="profil-desc text-center">
				            	<p><?php echo $row['gelar_depan'].' '.$row['nama'].' '.$row['gelar_belakang']; ?></p>
				            	<p><b><?php echo $row['nip']; ?></b></p>
				            </div>
				        </div>
				        <br/>
				        <?php
				        $tmpStatus = json_decode($pengajuan['status'], true);

				  		$keys = array_keys($tmpStatus['data']);
				     	$last_key = array_pop($keys);

        				if($tmpStatus['data'][$last_key]['status'] != 'berkas dikembalikan' && $tmpStatus['data'][$last_key]['status'] != 'validasi diterima' && $tmpStatus['data'][$last_key]['status'] != 'berkas fisik diterima' && $tmpStatus['data'][$last_key]['status'] != 'berkas dibawa bkn') {
				        ?>
				        <div class="text-center">
					        <div class="btn-group">
					        	<?php if($pengajuan['status_validasi'] == '2') { ?>
					        		<a href="javascript:;" id="diterima" class="btn btn-primary">Diterima</a>
						        	<a href="javascript:;" id="kembalikan" class="btn btn-danger">Kembalikan ke Pegawai</a>
						        	<input type="hidden" class="oncheck" value="1" />
					        	<?php } else { ?>
					        		<a href="javascript:;" class="btn btn-primary" disabled>Diterima</a>
						        	<a href="javascript:;" class="btn btn-danger" disabled>Kembalikan ke Pegawai</a>
						        	<input type="hidden" class="oncheck" value="0" />
					        	<?php } ?>
					        </div>
					    </div>
						<?php } else if($tmpStatus['data'][$last_key]['status'] == 'validasi diterima') { ?>
						<div class="text-center">
							<div class="btn-group">
				        		<a href="javascript:;" id="fisik-diterima" class="btn btn-success">Berkas Fisik Diterima</a>
					        </div>
					    </div>
						<?php } else if($tmpStatus['data'][$last_key]['status'] == 'berkas fisik diterima') { ?>
						<div class="text-center">
							<div class="btn-group">
				        		<a href="javascript:;" id="kirim-ke-bkn" class="btn btn-success btn-blue">Kirim</a>
					        </div>
					    </div>
						<?php } ?>
					</div>
					<div class="col-md-8">
						 <?php
						if(!empty($berkas)) {  if($tmpStatus['data'][$last_key]['status'] != 'berkas dikembalikan' && $tmpStatus['data'][$last_key]['status'] != 'validasi diterima' && $tmpStatus['data'][$last_key]['status'] != 'berkas fisik diterima' && $tmpStatus['data'][$last_key]['status'] != 'berkas dibawa bkn') { ?>
						<form role="form" action="" method="post">
							<?php 
							}
							echo '<table class="table" role="table">';
							echo '<thead><tr><th>Nama Berkas</th><th width="15%">Aksi</th></tr></thead>';
							echo '<tbody>';
							$year = date('Y'); 

							foreach ($berkas as $key => $value) { ?>
								<?php

								$filePath = $topDirectory . '/' . $userNip . '/'.$pengajuan['kategori'].'/' . $year;
								$fileName = $value->sub_kategori . '_' . $value->no_urut . '_' . $value->nama_file . '.pdf';
								$fileExist = $filePath . '/' . $fileName;
								$fileUrl = base_url('.ELFINDER'. '/' . $userNip . '/'.$pengajuan['kategori'].'/' . $year . '/' . $fileName);
								?>
								<tr>
									<td>
										<?php if(!file_exists($fileExist)) { ?>
										<p style="margin-bottom:0;"><?php echo $value->nama_berkas; ?></p>
										<?php } else { ?>
										<a href="javascript:;" data-toggle="modal" data-target="#modal-pdf" onclick="showPdf('<?=$fileUrl;?>','<?=$value->nama_berkas;?>')"><?php echo $value->nama_berkas; ?></a>
										<?php } ?>
									</td>
									<td>
										<?php
										$tmpValidasi = json_decode($pengajuan['validasi'], true);
								        $valCheck = array_column($tmpValidasi, 'answer');
								        if($tmpStatus['data'][$last_key]['status'] == 'berkas dikembalikan' || $tmpStatus['data'][$last_key]['status'] == 'validasi diterima' || $tmpStatus['data'][$last_key]['status'] == 'berkas fisik diterima' || $tmpStatus['data'][$last_key]['status'] == 'berkas dibawa bkn') { ?>
								        <div class="btn-group">
											<label class="btn btn-yes verify-check <?=$valCheck[$key]=='Y'?'active':'';?>" disabled>Y</label>
											<label class="btn btn-no verify-check <?=$valCheck[$key]=='N'?'active':'';?>" disabled>N</label>
										</div>
								        <?php } else { ?>
										<div class="btn-group" data-toggle="buttons">
											<label class="btn btn-yes verify-check <?=$valCheck[$key]=='Y'?'active':'';?>">
												<input type="radio" name="verify<?=$value->no_urut;?>" class="verify-radio" autocomplete="off" value="Y" <?=$valCheck[$key]=='Y'?'checked':'';?>> Y
											</label>
											<label class="btn btn-no verify-check <?=$valCheck[$key]=='N'?'active':'';?>">
												<input type="radio" name="verify<?=$value->no_urut;?>" class="verify-radio" autocomplete="off" value="N" <?=$valCheck[$key]=='N'?'checked':'';?>> N
											</label>
										</div>
										<input type="hidden" name="filename[]" value="<?=$fileName;?>" />
										<input type="hidden" name="no_urut[]" value="<?=$value->no_urut;?>" />
										<?php } ?>
									</td>
								</tr>
							<?php } echo '</tbody></table>'; if($tmpStatus['data'][$last_key]['status'] != 'berkas dikembalikan' && $tmpStatus['data'][$last_key]['status'] != 'validasi diterima' && $tmpStatus['data'][$last_key]['status'] != 'berkas fisik diterima' && $tmpStatus['data'][$last_key]['status'] != 'berkas dibawa bkn') { ?>
							<div class="text-right">
								<input type="hidden" name="submit" value="1" />
								<button type="submit" id="submit" name="submit" value="1" class="btn btn-success">Simpan Validasi</button>
							</div>
						</form>
						<?php } } ?>
					</div>
				</div>
			</section>
			<?php
			} }
			?>
			</div>
		</div>
	</div>
</div>
</section>
</div>

<script type="text/javascript">
    $(document).ready(function(){

    	var no = 0;
    	var id = "<?=$pengajuan['id']?>";
        $('.verify-check').click(function(){ 
        	no++;
        	$('.oncheck').val(no);

        	var oncheck = $('.oncheck').val();

        	if(oncheck==1) {
	        	$.ajax({
		            url : "<?php echo site_url('admin/pengajuan/berkas_on_check');?>",
	                method : "POST",
	                data : {id: id}
		        }).done(function(data) { console.log('success'); })
	        }
        }); 

        $('#diterima').click(function() {
	        $.ajax({
				url: "<?php echo site_url('admin/pengajuan/diterima');?>",
				type: "post",
				data : {id: id},
	            success: function(data){
	            	if(data=='success') {
	            		window.location.href = "<?php echo site_url('admin/pengajuan');?>";
	            	}	
				}
	        });
        });

        $('#fisik-diterima').click(function() {
	        $.ajax({
				url: "<?php echo site_url('admin/pengajuan/fisik_diterima');?>",
				type: "post",
				data : {id: id},
	            success: function(data){
	            	if(data=='success') {
	            		window.location.href = "<?php echo site_url('admin/pengajuan');?>";
	            	}	
				}
	        });
        });

        $('#kembalikan').click(function() {
        	if (window.confirm('Apakah Anda Yakin untuk mengembalikan berkas ke pegawai?'))
			{
			   $.ajax({
					url: "<?php echo site_url('admin/pengajuan/kembalikan_ke_pegawai');?>",
					type: "post",
					data : {id: id},
		            success: function(data){
		            	if(data=='success') {
		            		window.location.href = "<?php echo site_url('admin/pengajuan');?>";
		            	}	
					}
		        });
			}
        });

        $('#kirim-ke-bkn').click(function() {
        	if (window.confirm('Apakah Anda Yakin ? Pastikan data sudah sesuai, dan notifikasi akan dikirim ke pegawai terkait.'))
			{
			   $.ajax({
					url: "<?php echo site_url('admin/pengajuan/kirim_ke_bkn');?>",
					type: "post",
					data : {id: id},
		            success: function(data){
		            	if(data=='success') {
		            		window.location.href = "<?php echo site_url('admin/pengajuan');?>";
		            	}	
					}
		        });
			}
        });
         
    });
</script>