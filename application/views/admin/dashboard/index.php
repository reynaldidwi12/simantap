<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <?php echo $dashboard_alert_file_install; ?>
                    <div class="row">
				
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <a href="<?php echo base_url();?>setup/pegawai"><span class="info-box-icon bg-maroon"><i class="fa fa-user"></i></span></a>
                                <div class="info-box-content">
                                    <span class="info-box-text">Pegawai</span>
                                    <span class="info-box-number"><?php echo $jumlah_pegawai;?> Orang</span>
                                </div>
                            </div>
                        </div>
                        <?php if($this->session->userdata('group_id')<>'3') { ?>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <a href="<?php echo base_url();?>report/report_skpd"><span class="info-box-icon bg-green"><i class="fa fa-check"></i></span></a>
                                <div class="info-box-content">
                                    <span class="info-box-text">OPD</span>
                                    <span class="info-box-number"><?php echo 
									$jumlah_skpd+$jumlah_skpd2+$jumlah_skpd3+$jumlah_skpd4+$jumlah_skpd5+$jumlah_skpd6;?> </span>
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="clearfix visible-sm-block"></div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <a href="<?php echo base_url();?>setup/naik_pangkat"><span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span></a>
                                <div class="info-box-content">
                                    <!--span class="info-box-text">Pangkat Tahun ini</span-->
                                    <span class="info-box-text">Naik Pangkat</span>
                                    <span class="info-box-number"><?php echo $jumlah_naik_pangkat;?> Orang</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <a href="<?php echo base_url();?>setup/pensiun"><span class="info-box-icon bg-aqua"><i class="fa fa-shield"></i></span></a>
                                <div class="info-box-content">
                                    <!--span class="info-box-text">Pensiun Tahun ini</span-->
                                    <span class="info-box-text">Pensiun</span>
                                    <span class="info-box-number"><?php echo $jumlah_pensiun;?> Orang</span>
                                </div>
                            </div>
                        </div>					
										
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title">Golongan</h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
									 </div>
								</div>
								<div class="box-body chart-responsive">
									  <div id="golongan" style="min-width: 300px; height: 300px; margin: 0 auto"></div>
								</div>	
							</div>
						</div>
		
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <div class="box box-success">
							<div class="box-header with-border">
							  <h3 class="box-title">Pendidikan Formal</h3>

							  <div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>
								<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
							  </div>
							</div>
							<div class="box-body chart-responsive">
							   <div id="pendidikan" style="min-width: 300px; height: 300px; margin: 0 auto"></div>
							</div>
							
							</div>
						 </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title">Eselon Berdasarkan Golongan</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
								</div>

                                <div class="box-body chart-responsive">
                                    <div class="form-group">
                                        <form action="#eselon-per-golongan" method="post" name="submit-golongan">
                                        <select class="form-control" name="golongan-select" id="golongan-select">
                                            <option value="" <?php echo ($gol=='')?'selected':''; ?>>-- Pilih Golongan --</option>
                                            <option value="IV/c" <?php echo ($gol=='IV/c')?'selected':''; ?>>Golongan 4C</option>
                                            <option value="IV/b" <?php echo ($gol=='IV/b')?'selected':''; ?>>Golongan 4B</option>
                                            <option value="IV/a" <?php echo ($gol=='IV/a')?'selected':''; ?>>Golongan 4A</option>
                                            <option value="III/d" <?php echo ($gol=='III/d')?'selected':''; ?>>Golongan 3D</option>
                                            <option value="III/c" <?php echo ($gol=='III/c')?'selected':''; ?>>Golongan 3C</option>
                                            <option value="III/b" <?php echo ($gol=='III/b')?'selected':''; ?>>Golongan 3B</option>
                                            <option value="III/a" <?php echo ($gol=='III/a')?'selected':''; ?>>Golongan 3A</option>
                                        </select>
                                        </form>
                                    </div>
                                    <div id="eselon-per-golongan" style="min-width: 300px; height: 300px; margin: 0 auto"></div>
                                </div>
								
							</div>
						</div>
						 
                    </div>
					
						<!--div class="image" style="padding-top:280px;text-align:right;">
							<img src="<?php echo base_url();?>/assets/image/login-page.png" class="login-kendari" alt="" />
						</div-->
						
                    <div class="row">
                        <div class="col-md-12">

                        </div>
	
                </section>
				
				
            </div>
			
	
<?php
    foreach($jumlah_pegawai_eselon as $data) {
        @$eselon2 += $data->eselon2;
        @$eselon3 += $data->eselon3;
        @$eselon4 += $data->eselon4;
        @$staff += $data->staff;

    }
?>
<script>
$(function () {
    Highcharts.chart('golongan', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [
                'Golongan I',
                'Golongan II',
                'Golongan III',
                'Golongan IV',
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
			showInLegend: false, 
			name: 'A',
            data: [	<?php echo $jumlah_pegawai_golongan2->Ia;?>, 
					<?php echo $jumlah_pegawai_golongan2->IIa;?>, 
					<?php echo $jumlah_pegawai_golongan2->IIIa;?>, 
					<?php echo $jumlah_pegawai_golongan2->IVa;?>]

        }, {
			showInLegend: false, 
			name: 'B',
            data: [	<?php echo $jumlah_pegawai_golongan2->Ib;?>, 
					<?php echo $jumlah_pegawai_golongan2->IIb;?>, 
					<?php echo $jumlah_pegawai_golongan2->IIIb;?>, 
					<?php echo $jumlah_pegawai_golongan2->IVb;?>]

        }, {
			showInLegend: false, 
			name: 'C',
            data: [	<?php echo $jumlah_pegawai_golongan2->Ic;?>, 
					<?php echo $jumlah_pegawai_golongan2->IIc;?>, 
					<?php echo $jumlah_pegawai_golongan2->IIIc;?>, 
					<?php echo $jumlah_pegawai_golongan2->IVc;?>]

        }, {
			showInLegend: false, 
			name: 'D',
            data: [	<?php echo $jumlah_pegawai_golongan2->Id;?>, 
					<?php echo $jumlah_pegawai_golongan2->IId;?>, 
					<?php echo $jumlah_pegawai_golongan2->IIId;?>, 
					<?php echo $jumlah_pegawai_golongan2->IVd;?>]

        }]
    });
});
$(function () {
    Highcharts.chart('pendidikan', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [
                'SD',
                'SMP',
                'SMA',
                'D1/D2',
                'D3',
                'S1',
                'S2',
                'S3',
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
			showInLegend: false,  
            name: 'jml',
            colorByPoint: true,
            data: [{
                y: <?php echo $jumlah_pegawai_pendidikan2->SD;?>,
            }, {
                y: <?php echo $jumlah_pegawai_pendidikan2->SMP;?>,
            }, {
                y: <?php echo $jumlah_pegawai_pendidikan2->SMA;?>,
            }, {
                y: <?php echo $jumlah_pegawai_pendidikan2->D1;?>,
            }, {
                y: <?php echo $jumlah_pegawai_pendidikan2->D3;?>,
            },{
                y: <?php echo $jumlah_pegawai_pendidikan2->S1;?>,
            },{
                y: <?php echo $jumlah_pegawai_pendidikan2->S2;?>,
            },{
                y: <?php echo $jumlah_pegawai_pendidikan2->S3;?>,
            }]
        }],
    });
});
$(function () {
    Highcharts.chart('eselon-per-golongan', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [
                'Eselon 2',
                'Eselon 3',
                'Eselon 4',
                'Staff',
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
			showInLegend: false,  
            name: 'jml',
            colorByPoint: true,
            data: [{
                y: <?php echo (!empty($eselon2))?$eselon2:'0'; ?>,
            }, {
                y: <?php echo (!empty($eselon3))?$eselon3:'0'; ?>,
            }, {
                y: <?php echo (!empty($eselon4))?$eselon4:'0'; ?>,
            }, {
                y: <?php echo (!empty($staff))?$staff:'0'; ?>,
            }]
        }],
    });
});
$(document).ready(function() {
  $('#golongan-select').on('change', function() {
     document.forms['submit-golongan'].submit();
  });
});
</script>
			
