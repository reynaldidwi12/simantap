<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$jenis_data = $this->session->userdata('jenis_data');
$kategori_data = $this->session->userdata('kategori_data');
$sub_kategori_data = $this->session->userdata('sub_kategori_data');

?>

<style type="text/css">
    .content-infografis .info-box {
        position: relative;
        border-top: 50px solid #3c8dbc;
        background-color: #f0f1f3;
        border-radius: 20px 20px 0 0;
        box-shadow: 1px 1px 5px rgba(0,0,0,0.5);
        padding: 50px;
    }

    .widget-heading {
        background-color: #fff;
        border-radius: 10px;
        padding: 20px;
        box-shadow: 1px 1px 5px rgba(0,0,0,0.2);
        margin-bottom: 20px;
    }

    .widget-title {
        font-weight: bold;
        font-size: 18px;
        margin-top: 0;
    }

    .widget-bg {
        background-color: #fcf9fe;
        border-radius: 10px;
        border: 1px solid #edeef3;
        padding: 10px;
    }
</style>

<script type="text/javascript">
    var label = JSON.parse('<?= addslashes(json_encode($label)) ?>');
    var men = JSON.parse('<?= addslashes(json_encode($arr_men)) ?>');
    var women = JSON.parse('<?= addslashes(json_encode($arr_women)) ?>');
    var pend_label = JSON.parse('<?= addslashes(json_encode($label_pend)) ?>');
    var pend_men = JSON.parse('<?= addslashes(json_encode($arr_pend_men)) ?>');
    var pend_women = JSON.parse('<?= addslashes(json_encode($arr_pend_women)) ?>');

    var data1 = {
        "label": label,
        "total": {
            "men": men,
            "women": women
        }
    };

    var data2 = {
        "label": ['Laki-Laki', 'Perempuan'],
        "total": [<?php echo $total_men; ?>, <?php echo $total_women; ?>]
    };

    var data3 = {
        "label": pend_label,
        "total": {
            "men": pend_men,
            "women": pend_women
        }
    };
</script>

<script src="<?php echo base_url($plugins_dir . '/apex/apexcharts.min.js'); ?>"></script>

<div class="content-wrapper">

    <section class="content content-infografis">
        <div class="row">
            <div class="col-md-12">
                <div class="info-box">
                    <?php echo form_open("admin/infografis");?>
                    <div clas="text-center row">
                        <?php 
                            $arrJenis = array(
                                ''=>'--Jenis Data--',
                                'eselon'=>'Eselon', 
                                'golongan'=>'Golongan',
                                'pendidikan'=>'Pendidikan', 
                                'diklat'=>'Diklat',
                            ); 
                            $arrKat = array(
                                ''=>'--Pilih Kategori--',
                                '1,2,3,4,9,10'=>'Sekretariat, Kecamatan dan Kelurahan',
                                '5,11,12,13,14,15,16,17,18,19,20,21,22'=>'Dinas',
                                '6'=>'Badan',
                                '7,8'=>'Puskesmas dan Rumah Sakit',
                                '23'=>'TK',
                                '21'=>'SD',
                                '22'=>'SLTP',
                            ); 
                        ?>
                        <div class="col-md-3">
                            <?php
                            echo form_dropdown('jenis_data',$arrJenis,@$jenis_data,'class="form-control selectpicker" data-live-search="true"');
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?php
                            echo form_dropdown('kategori_data',$arrKat,@$kategori_data,'class="form-control selectpicker" data-live-search="true"');
                            ?>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control selectpicker" data-live-search="true" name="sub_kategori_data" id="sub_kategori_data">
                                <option value="">--Pilih Sub Kategori--</option>
                                <?php if(!empty($sub_kategori)) { ?>
                                    <?php
                                    foreach($sub_kategori as $sub) {
                                        if($sub_kategori_data == $sub['kode']) {
                                            $class = 'selected';
                                        } else {
                                            $class = '';
                                        }
                                        echo '<option value="'.$sub['kode'].'" '.$class.'>'.$sub['nama'].'</option>';
                                    }
                                    ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <div class="btn-group">
                                <input type="submit" name="submit" class="btn btn-info" title="Submit" value="Go">
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                    <br/><br/><br/>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="widget-heading">
                                <h3 class="widget-title">Jumlah <?php echo $name; ?></h3>

                                <div id="jumlahgolongan" class="widget-bg"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="widget-heading">
                                <h3 class="widget-title">Jenis Kelamin</h3>

                                <div id="jeniskelamin"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="widget-heading">
                                <h3 class="widget-title">Pendidikan</h3>

                                <div id="jumlahpendidikan" class="widget-bg"></div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </section>
    
</div>

<script type="text/javascript">
    var options1 = {
        series: [{
              name: 'Laki-Laki',
              data: data1.total.men
          }, {
              name: 'Perempuan',
              data: data1.total.women
          }],
      labels: data1.label,
      chart: {
        height: 365,
        type: 'area',
        zoom: {
            enabled: false
        },
        dropShadow: {
          enabled: true,
          opacity: 0.2,
          blur: 10,
          left: -7,
          top: 22
        },
        toolbar: {
          show: false
        },
        events: {
          mounted: function(ctx, config) {
            const highest1 = ctx.getHighestValueInSeries(0);
            const highest2 = ctx.getHighestValueInSeries(1);

            ctx.addPointAnnotation({
              x: new Date(ctx.w.globals.seriesX[0][ctx.w.globals.series[0].indexOf(highest1)]).getTime(),
              y: highest1,
              label: {
                style: {
                  cssClass: 'd-none'
                }
              },
              customSVG: {
                  SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#2196f3" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
                  cssClass: undefined,
                  offsetX: -8,
                  offsetY: 5
              }
            });

            ctx.addPointAnnotation({
              x: new Date(ctx.w.globals.seriesX[1][ctx.w.globals.series[1].indexOf(highest2)]).getTime(),
              y: highest2,
              label: {
                style: {
                  cssClass: 'd-none'
                }
              },
              customSVG: {
                  SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#6d17cb" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
                  cssClass: undefined,
                  offsetX: -8,
                  offsetY: 5
              }
            });
          },
        }
      },
      colors: ['#2196f3', '#6d17cb'],
      dataLabels: {
          enabled: false
      },
      markers: {
        discrete: [{
        seriesIndex: 0,
        dataPointIndex: 7,
        fillColor: '#000',
        strokeColor: '#000',
        size: 5
      }, {
        seriesIndex: 2,
        dataPointIndex: 11,
        fillColor: '#000',
        strokeColor: '#000',
        size: 4
      }]
      },
      subtitle: {
        text: '<?php echo $total; ?>',
        align: 'left',
        margin: 0,
        offsetX: 130,
        offsetY: 0,
        floating: false,
        style: {
          fontSize: '18px',
          color:  '#4361ee'
        }
      },
      title: {
        text: 'Total <?php echo $name; ?>',
        align: 'left',
        margin: 0,
        offsetX: -10,
        offsetY: 0,
        floating: false,
        style: {
          fontSize: '18px',
          color:  '#0e1726'
        },
      },
      stroke: {
          show: true,
          curve: 'smooth',
          width: 2,
          lineCap: 'square'
      },
      xaxis: {
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        },
        crosshairs: {
          show: true
        },
        labels: {
          offsetX: 0,
          offsetY: 5,
          style: {
              fontSize: '12px',
              cssClass: 'apexcharts-xaxis-title',
          },
        }
      },
      yaxis: {
        labels: {
          offsetX: -22,
          offsetY: 0,
          style: {
              fontSize: '12px',
              cssClass: 'apexcharts-yaxis-title',
          },
          formatter: function (val) {
              return val;
          }
        }
      },
      grid: {
        borderColor: '#e0e6ed',
        strokeDashArray: 5,
        xaxis: {
            lines: {
                show: true
            }
        },   
        yaxis: {
            lines: {
                show: false,
            }
        },
        padding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: -10
        }, 
      }, 
      legend: {
        position: 'top',
        horizontalAlign: 'right',
        offsetY: -50,
        fontSize: '16px',
        markers: {
          width: 10,
          height: 10,
          strokeWidth: 0,
          strokeColor: '#fff',
          fillColors: undefined,
          radius: 12,
          onClick: undefined,
          offsetX: 0,
          offsetY: 0
        },    
        itemMargin: {
          horizontal: 0,
          vertical: 20
        }
      },
      tooltip: {
        theme: 'dark',
        marker: {
          show: true,
        },
        x: {
          show: false,
        }
      },
      fill: {
          type:"gradient",
          gradient: {
              type: "vertical",
              shadeIntensity: 1,
              inverseColors: !1,
              opacityFrom: .28,
              opacityTo: .05,
              stops: [45, 100]
          }
      },
      responsive: [{
        breakpoint: 575,
        options: {
          legend: {
              offsetY: -30,
          },
        },
      }]
    };

    var options2 = {
        series: data2.total,
        labels: data2.label,
        chart: {
            type: 'donut',
            width: 380
        },
        colors: ['#2196f3', '#e2a03f', '#8738a7'],
        dataLabels: {
          enabled: false
        },
        legend: {
            position: 'bottom',
            horizontalAlign: 'center',
            fontSize: '14px',
            markers: {
              width: 10,
              height: 10,
            },
            itemMargin: {
              horizontal: 0,
              vertical: 8
            }
        },
        plotOptions: {
          pie: {
            donut: {
              size: '65%',
              background: 'transparent',
              labels: {
                show: true,
                name: {
                  show: true,
                  fontSize: '25px',
                  color: undefined,
                  offsetY: -10
                },
                value: {
                  show: true,
                  fontSize: '26px',
                  color: '20',
                  offsetY: 16,
                  formatter: function (val) {
                    return val
                  }
                },
                total: {
                  show: true,
                  showAlways: true,
                  label: 'Total',
                  color: '#888ea8',
                  formatter: function (w) {
                    return w.globals.seriesTotals.reduce( function(a, b) {
                      return a + b
                    }, 0)
                  }
                }
              }
            }
          }
        },
        stroke: {
          show: true,
          width: 25,
        },
        responsive: [{
            breakpoint: 1599,
            options: {
                chart: {
                    width: '350px',
                    height: '400px'
                },
                legend: {
                    position: 'bottom'
                }
            },

            breakpoint: 1439,
            options: {
                chart: {
                    width: '250px',
                    height: '390px'
                },
                legend: {
                    position: 'bottom'
                },
                plotOptions: {
                  pie: {
                    donut: {
                      size: '65%',
                    }
                  }
                }
            },
        }]
    };

    var options3 = {
        series: [{
            name: 'Laki-Laki',
            data: data3.total.men
        }, {
          name: 'Perempuan',
          data: data3.total.women
        }],
        chart: {
            height: 350,
            type: 'bar',
            toolbar: {
              show: false,
            }
        },
        colors: ['#517281', '#f67062'],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded'  
            },
        },
        dataLabels: {
            enabled: false
        },
        legend: {
              position: 'bottom',
              horizontalAlign: 'center',
              fontSize: '14px',
              markers: {
                width: 10,
                height: 10,
              },
              itemMargin: {
                horizontal: 0,
                vertical: 8
              }
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: data3.label,
        },
        yaxis: {
            labels: {
              offsetX: -22,
              offsetY: 0,
              style: {
                  fontSize: '12px',
                  cssClass: 'apexcharts-yaxis-title',
              },
              formatter: function (val) {
                return val;
              }
            }
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val
                }
            }
        }
    };

    var chart1 = new ApexCharts(document.querySelector("#jumlahgolongan"), options1);
    chart1.render();

    var chart2 = new ApexCharts(document.querySelector("#jeniskelamin"), options2);
    chart2.render();

    var chart3 = new ApexCharts(document.querySelector("#jumlahpendidikan"), options3);
    chart3.render();
</script>
            
