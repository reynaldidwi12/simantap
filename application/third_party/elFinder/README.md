# File Manager untuk simantap

Ini adalah file manager untuk simantap dengan bantuan elFinder.
Default base directorynya disetel ke `.ELFINDER`


Perubahan:
1. Penambahan Library elFinder berada di `application/third_party/elFinder`

2. Penambahan Controller file manager untuk admin ada di `application/controllers/setup/file_manager`
3. Penambahan Controller file manager untuk member ada di `application/controllers/setup2/file_manager`

4. Penambahan file manager views untuk admin ada di `application/views/setup/file_manager/index`
5. Penambahan file manager views untuk member ada di `application/views/setup2/file_manager/index`
6. Penambahan elfinder views ada di `application/views/elfinder.php`

7. Penambahan file manager button link untuk admin di `result_pegawai` view
8. Penambahan file manager button link untuk member di `pegawai/index.php` view

---
Pengaturan allowed file type:

allowed file type bisa diatur di masing-masing file manager controller.
```
array(
	'image',
	'text/plain',
	'application/pdf',
	'doc',
	'xls',
	'ppt',
	// Open office
	'odt',
	'odg',
	'odp',
	'ods',
	'odg',
	'odc',
	'odf',
	'odb',
	'odi',
	// MS office 2007
	'docx',
	'pptx',
	'xlsx',
	// Archives
	'zip',
	'rar',
),
```

---

Jika terdapat pesan gagal konek dengan status 500,
ubah pemilik folder pada basefolder `.ELFINDER` ke `www-data`
```
chown -R www-data .ELFINDER
```

atau ubah permission ke 777
```
chmod -R 777 .ELFINDER
```

Lebih disarankan solusi yang pertama karena lebih aman

---

support di Dwi Purnomo <dwipurnomo@antaraksi.com>
