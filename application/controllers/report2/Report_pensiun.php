<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class report_pensiun extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		
		ini_set('max_execution_time', 10000);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_pensiun'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('report/report_pensiun_model' );
		
	}
	
	public function add(){
		//if($this->input->post('submit')){
			
			$this->load->library('excel');
			
			//load PHPExcel library
			$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
                  $this->excel->getActiveSheet()->setTitle('Lap. Pegawai Yang Akan Pensiun');

  				//STYLING
  				$styleArray = array(
  					'borders' => array('allborders' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);
					
			$no = 7;		
					
			//SET DIMENSI TABEL
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(23);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(16);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(19);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(35);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			  
			//set KOP
			$this->excel->getActiveSheet()->mergeCells('A1:L1');
			$this->excel->getActiveSheet()->setCellValue('A1', 'Daftar Tenaga Pegawai Negeri Sipil Yang Akan Pensiun ');
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			
			//SET NO
			$this->excel->getActiveSheet()->mergeCells('A5:A6');
			$this->excel->getActiveSheet()->setCellValue('A5', 'NO');
			$this->excel->getActiveSheet()->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setName('Calibri');
			

			//SET NIP BARU
			$this->excel->getActiveSheet()->mergeCells('B5:B6');
			$this->excel->getActiveSheet()->setCellValue('B5', 'NIP BARU');
			$this->excel->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setName('Calibri');
			
			
			//SET NIP LAMA
			$this->excel->getActiveSheet()->mergeCells('C5:C6');
			$this->excel->getActiveSheet()->setCellValue('C5', 'NIP LAMA');
			$this->excel->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setName('Calibri');
			
			//SET NAMA
			$this->excel->getActiveSheet()->mergeCells('D5:D6');
			$this->excel->getActiveSheet()->setCellValue('D5', 'NAMA');
			$this->excel->getActiveSheet()->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setName('Calibri');
			
			//SET JENIS KELAMIN
			$this->excel->getActiveSheet()->mergeCells('E5:E6');
			$this->excel->getActiveSheet()->setCellValue('E5', 'L/P');
			$this->excel->getActiveSheet()->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setName('Calibri');
			
			//SET PANGKAT
			$this->excel->getActiveSheet()->mergeCells('F5:F6');
			$this->excel->getActiveSheet()->setCellValue('F5', 'PANGKAT / GOLONGAN');
			$this->excel->getActiveSheet()->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setName('Calibri');
			
			//SET TMT
			$this->excel->getActiveSheet()->mergeCells('G5:G6');
			$this->excel->getActiveSheet()->setCellValue('G5', 'TMT');
			$this->excel->getActiveSheet()->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('G5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('G5')->getFont()->setName('Calibri');
			
			//SET PENDIDIKAN
			$this->excel->getActiveSheet()->mergeCells('H5:H6');;
			$this->excel->getActiveSheet()->setCellValue('H5', 'PENDIDIKAN');
			$this->excel->getActiveSheet()->getStyle('H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('H5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('H5')->getFont()->setName('Calibri');
			
			//SET JURUSAN
			$this->excel->getActiveSheet()->mergeCells('I5:I6');;
			$this->excel->getActiveSheet()->setCellValue('I5', 'JURUSAN');
			$this->excel->getActiveSheet()->getStyle('I5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('I5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('I5')->getFont()->setName('Calibri');
			
			//SET TAHUN LULUS
			$this->excel->getActiveSheet()->mergeCells('J5:J6');
			$this->excel->getActiveSheet()->getStyle('J5:J6')->getAlignment()->setWrapText(true); 
			$this->excel->getActiveSheet()->setCellValue('J5', 'TAHUN LULUS');
			$this->excel->getActiveSheet()->getStyle('J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('J5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('J5')->getFont()->setName('Calibri');
			
			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->mergeCells('K5:K6');
			$this->excel->getActiveSheet()->getStyle('K5:K6')->getAlignment()->setWrapText(true); 
			$this->excel->getActiveSheet()->setCellValue('K5', 'TEMPAT TUGAS');
			$this->excel->getActiveSheet()->getStyle('K5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('K5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('K5')->getFont()->setName('Calibri');
			
			//SET UMUR
			$this->excel->getActiveSheet()->mergeCells('L5:L6');
			$this->excel->getActiveSheet()->getStyle('L5:L6')->getAlignment()->setWrapText(true); 
			$this->excel->getActiveSheet()->setCellValue('L5', 'UMUR TAHUN INI');
			$this->excel->getActiveSheet()->getStyle('L5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('L5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('L5')->getFont()->setName('Calibri');
			
			$no =8;
			$hit =1;
			$data = $this->report_pensiun_model->fetchAll_skpd($this->session->userdata('ss_skpd'));
			foreach ($data as $datax) {
				$this->excel->getActiveSheet()->setCellValue('A'.$no, $hit );
				$this->excel->getActiveSheet()->setCellValue('B'.$no, $datax->nip );
				$this->excel->getActiveSheet()->setCellValue('C'.$no, $datax->nip_lama );
				$this->excel->getActiveSheet()->setCellValue('D'.$no, $datax->gelar_depan.''.$datax->nama.','.$datax->gelar_belakang );
				if($datax->kelamin=='1'){
					$this->excel->getActiveSheet()->setCellValue('E'.$no, 'L' );
				}else if($datax->kelamin=='2'){
					$this->excel->getActiveSheet()->setCellValue('E'.$no, 'P' );
				}
				$this->excel->getActiveSheet()->setCellValue('F'.$no, $datax->golongan );
				
				if($datax->tmt_pangkat=='0000-00-00'){
					$this->excel->getActiveSheet()->setCellValue('G'.$no, '-' );
				}else{
					$tmt_pangkat = date("d-m-Y",strtotime($datax->tmt_pangkat));
					$this->excel->getActiveSheet()->setCellValue('G'.$no, $tmt_pangkat );
				}
				
				$this->excel->getActiveSheet()->setCellValue('H'.$no, $datax->jenis_pendidikan );
				$this->excel->getActiveSheet()->setCellValue('I'.$no, $datax->nama_sekolah );
				$this->excel->getActiveSheet()->setCellValue('J'.$no, $datax->tahun_ijazah );
				$this->excel->getActiveSheet()->setCellValue('K'.$no, $datax->unit_kerja );
				$this->excel->getActiveSheet()->setCellValue('L'.$no, $datax->umur." Tahun" );
				
				/*if($datax->KGBTerakhir=='0000-00-00'){
					$this->excel->getActiveSheet()->setCellValue('P'.$no, '-' );
				}else{
					$KGBTerakhir = date("d-m-Y",strtotime($datax->KGBTerakhir));
					$this->excel->getActiveSheet()->setCellValue('P'.$no, $KGBTerakhir );
				}*/
				
				
				
				$this->excel->getActiveSheet()->getStyle('A'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
				$this->excel->getActiveSheet()->getStyle('B'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('C'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('D'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('E'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('F'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('G'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('H'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('I'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('J'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('K'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('L'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('M'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('N'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('O'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('P'.$no)->getFont()->setSize(9);
				
				
			   
				$no++;
				$hit++;
				

			}
			  $this->excel->getActiveSheet()->getStyle('A5:L'.($no-1))->applyFromArray($styleArray);
			  
					
					
			ob_end_clean();
                  $filename='Lap. Pegawai Yang Akan Pensiun.xls'; //save our workbook as this file name
                  header('Content-Type: application/vnd.ms-excel'); //mime type
                  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                  header('Cache-Control: max-age=0'); //no cache

                  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                  //if you want to save it as .XLSX Excel 2007 format
                  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');
          
           //redirect('report/report_all','refresh');	
			
		//} else {
			
		//}
		
	}
}
	
	