<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infografis extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/infografis_model');
    }

	public function index()
	{	
        if ( !$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
        	if($this->ion_auth->is_admin()){
	            /* Title Page */
	            $this->page_title->push(lang('menu_infografis'));
	            $this->data['pagetitle'] = $this->page_title->show();

	            $this->data['label'] = '';
	            $this->data['label_pend'] = '';
	            $this->data['name'] = '';
	            $this->data['total'] = 0;
	            $this->data['arr_men'] = '';
	            $this->data['arr_women'] = '';
	            $this->data['total_men'] = 0;
	            $this->data['total_women'] = 0;
	            $this->data['sub_kategori'] = '';
	            $this->data['arr_pend_men'] = '';
	            $this->data['arr_pend_women'] = '';

	            if($this->input->post('submit')) {

		            $jenis_data = $this->input->post("jenis_data");
					$kategori_data = $this->input->post("kategori_data");
					$sub_kategori_data = $this->input->post("sub_kategori_data");

		            $infografis = array(
				        'jenis_data'  => $jenis_data,
				        'kategori_data' => $kategori_data,
				        'sub_kategori_data' => $sub_kategori_data
					);

					$this->session->set_userdata($infografis);

					if(!empty($jenis_data) && $jenis_data == 'eselon') {

						if(!empty($kategori_data)) {
							if(!empty($sub_kategori_data)) {
								$this->session->unset_userdata('sub_kategori_data');
								$eselon = $this->infografis_model->getDataEselonByCat($kategori_data, $sub_kategori_data);
							} else {
								$this->session->unset_userdata('sub_kategori_data');
								$eselon = $this->infografis_model->getDataEselonByCat($kategori_data, '');
							}
						} else {
							if(!empty($sub_kategori_data)) {
								$eselon = $this->infografis_model->getDataEselonByCat('', $sub_kategori_data);
							} else {
								$eselon = $this->infografis_model->getDataEselon();
							}
						}

						$total = 0;
						$total_men = 0;
						$total_women = 0;
						$total_l1b = 0;
						$total_l2a = 0;
						$total_l2b = 0;
						$total_l3a = 0;
						$total_l3b = 0;
						$total_l4a = 0;
						$total_l4b = 0;
						$total_p1b = 0;
						$total_p2a = 0;
						$total_p2b = 0;
						$total_p3a = 0;
						$total_p3b = 0;
						$total_p4a = 0;
						$total_p4b = 0;
						$arr_men = '';
						$arr_women = '';
						$sub_kategori = array();

						$total_lsd = 0;
						$total_psd = 0;
						$total_lsmp = 0;
						$total_psmp = 0;
						$total_lsma = 0;
						$total_psma = 0;
						$total_ld1 = 0;
						$total_pd1 = 0;
						$total_ld3 = 0;
						$total_pd3 = 0;
						$total_ls1 = 0;
						$total_ps1 = 0;
						$total_ls2 = 0;
						$total_ps2 = 0;
						$total_ls3 = 0;
						$total_ps3 = 0;
 
 						if(!empty($eselon)) {
							foreach($eselon as $row) {
								$total += $row->total_pegawai;
								$total_men += $row->L;
								$total_women += $row->P;
								$total_l1b += $row->L1B;
								$total_l2a += $row->L2A;
								$total_l2b += $row->L2B;
								$total_l3a += $row->L3A;
								$total_l3b += $row->L3B;
								$total_l4a += $row->L4A;
								$total_l4b += $row->L4B;
								$total_p1b += $row->P1B;
								$total_p2a += $row->P2A;
								$total_p2b += $row->P2B;
								$total_p3a += $row->P3A;
								$total_p3b += $row->P3B;
								$total_p4a += $row->P4A;
								$total_p4b += $row->P4B;
								$sub_kategori[] = array( 
									'kode' => $row->kd_skpd,
									'nama' => $row->nama
								);
								$total_lsd += $row->LSD;
								$total_psd += $row->PSD;
								$total_lsmp += $row->LSMP;
								$total_psmp += $row->PSMP;
								$total_lsma += $row->LSMA;
								$total_psma += $row->PSMA;
								$total_ld1 += $row->LD1;
								$total_pd1 += $row->PD1;
								$total_ld3 += $row->LD3;
								$total_pd3 += $row->PD3;
								$total_ls1 += $row->LS1;
								$total_ps1 += $row->PS1;
								$total_ls2 += $row->LS2;
								$total_ps2 += $row->PS2;
								$total_ls3 += $row->LS3;
								$total_ps3 += $row->PS3;
							}
						}

						$this->data['name'] = 'Eselon';

						$arr_label = array('I.b','II.a','II.b','III.a','III.b','IV.a','IV.b');
						$arr_men = array($total_l1b,$total_l2a,$total_l2b,$total_l3a,$total_l3b,$total_l4a,$total_l4b);
						$arr_women = array($total_p1b,$total_p2a,$total_p2b,$total_p3a,$total_p3b,$total_p4a,$total_p4b);
						$arr_pend_label = array('SD','SMP','SMA','D1','D3','S1','S2','S3');
						$arr_pend_men = array($total_lsd,$total_lsmp,$total_lsma,$total_ld1,$total_ld3,$total_ls1,$total_ls2,$total_ls3);
						$arr_pend_women = array($total_psd,$total_psmp,$total_psma,$total_pd1,$total_pd3,$total_ps1,$total_ps2,$total_ps3);

						$this->data['label'] = $arr_label;

						$this->data['label_pend'] = $arr_pend_label;

						$this->data['total'] = $total;

						$this->data['total_men'] = $total_men;
	            		$this->data['total_women'] = $total_women;

	            		$this->data['arr_men'] = $arr_men;
	            		$this->data['arr_women'] = $arr_women;

	            		$this->data['arr_pend_men'] = $arr_pend_men;
	            		$this->data['arr_pend_women'] = $arr_pend_women;

	            		$this->data['sub_kategori'] = $sub_kategori;

					} else if(!empty($jenis_data) && $jenis_data == 'golongan') {

						if(!empty($kategori_data)) {
							if(!empty($sub_kategori_data)) {
								$this->session->unset_userdata('sub_kategori_data');
								$golongan = $this->infografis_model->getDataGolonganByCat($kategori_data, $sub_kategori_data);
							} else {
								$this->session->unset_userdata('sub_kategori_data');
								$golongan = $this->infografis_model->getDataGolonganByCat($kategori_data, '');
							}
						} else {
							if(!empty($sub_kategori_data)) {
								$golongan = $this->infografis_model->getDataGolonganByCat('', $sub_kategori_data);
							} else {
								$golongan = $this->infografis_model->getDataGolongan();
							}
						}

						$total = 0;
						$total_men = 0;
						$total_women = 0;
						$total_l1a = 0;
						$total_l1b = 0;
						$total_l1c = 0;
						$total_l1d = 0;
						$total_l2a = 0;
						$total_l2b = 0;
						$total_l2c = 0;
						$total_l2d = 0;
						$total_l3a = 0;
						$total_l3b = 0;
						$total_l3c = 0;
						$total_l3d = 0;
						$total_l4a = 0;
						$total_l4b = 0;
						$total_l4c = 0;
						$total_l4d = 0;
						$total_p1a = 0;
						$total_p1b = 0;
						$total_p1c = 0;
						$total_p1d = 0;
						$total_p2a = 0;
						$total_p2b = 0;
						$total_p2c = 0;
						$total_p2d = 0;
						$total_p3a = 0;
						$total_p3b = 0;
						$total_p3c = 0;
						$total_p3d = 0;
						$total_p4a = 0;
						$total_p4b = 0;
						$total_p4c = 0;
						$total_p4d = 0;
						$arr_men = '';
						$arr_women = '';
						$sub_kategori = array();

						$total_lsd = 0;
						$total_psd = 0;
						$total_lsmp = 0;
						$total_psmp = 0;
						$total_lsma = 0;
						$total_psma = 0;
						$total_ld1 = 0;
						$total_pd1 = 0;
						$total_ld3 = 0;
						$total_pd3 = 0;
						$total_ls1 = 0;
						$total_ps1 = 0;
						$total_ls2 = 0;
						$total_ps2 = 0;
						$total_ls3 = 0;
						$total_ps3 = 0;
 
 						if(!empty($golongan)) {
							foreach($golongan as $row) {
								$total += $row->total_pegawai;
								$total_men += $row->L;
								$total_women += $row->P;
								$total_l1a += $row->L1A;
								$total_l1b += $row->L1B;
								$total_l1c += $row->L1C;
								$total_l1d += $row->L1D;
								$total_l2a += $row->L2A;
								$total_l2b += $row->L2B;
								$total_l2c += $row->L2C;
								$total_l2d += $row->L2D;
								$total_l3a += $row->L3A;
								$total_l3b += $row->L3B;
								$total_l3c += $row->L3C;
								$total_l3d += $row->L3D;
								$total_l4a += $row->L4A;
								$total_l4b += $row->L4B;
								$total_l4c += $row->L4C;
								$total_l4d += $row->L4D;
								$total_p1a += $row->P1A;
								$total_p1b += $row->P1B;
								$total_p1c += $row->P1C;
								$total_p1d += $row->P1D;
								$total_p2a += $row->P2A;
								$total_p2b += $row->P2B;
								$total_p2c += $row->P2C;
								$total_p2d += $row->P2D;
								$total_p3a += $row->P3A;
								$total_p3b += $row->P3B;
								$total_p3c += $row->P3C;
								$total_p3d += $row->P3D;
								$total_p4a += $row->P4A;
								$total_p4b += $row->P4B;
								$total_p4c += $row->P4C;
								$total_p4d += $row->P4D;
								$sub_kategori[] = array( 
									'kode' => $row->kd_skpd,
									'nama' => $row->nama
								);
								$total_lsd += $row->LSD;
								$total_psd += $row->PSD;
								$total_lsmp += $row->LSMP;
								$total_psmp += $row->PSMP;
								$total_lsma += $row->LSMA;
								$total_psma += $row->PSMA;
								$total_ld1 += $row->LD1;
								$total_pd1 += $row->PD1;
								$total_ld3 += $row->LD3;
								$total_pd3 += $row->PD3;
								$total_ls1 += $row->LS1;
								$total_ps1 += $row->PS1;
								$total_ls2 += $row->LS2;
								$total_ps2 += $row->PS2;
								$total_ls3 += $row->LS3;
								$total_ps3 += $row->PS3;
							}
						}

						$this->data['name'] = 'Golongan';

						$arr_label = array('I.a','I.b','I.c','I.d','II.a','II.b','II.c','II.d','III.a','III.b','III.c','III.d','IV.a','IV.b','IV.c','IV.d');
						$arr_men = array($total_l1a,$total_l1b,$total_l1c,$total_l1d,$total_l2a,$total_l2b,$total_l2c,$total_l2d,$total_l3a,$total_l3b,$total_l3c,$total_l3d,$total_l4a,$total_l4b,$total_l4c,$total_l4d);
						$arr_women = array($total_p1a,$total_p1b,$total_p1c,$total_p1d,$total_p2a,$total_p2b,$total_p2c,$total_p2d,$total_p3a,$total_p3b,$total_p3c,$total_p3d,$total_p4a,$total_p4b,$total_p4c,$total_p4d);
						$arr_pend_label = array('SD','SMP','SMA','D1','D3','S1','S2','S3');
						$arr_pend_men = array($total_lsd,$total_lsmp,$total_lsma,$total_ld1,$total_ld3,$total_ls1,$total_ls2,$total_ls3);
						$arr_pend_women = array($total_psd,$total_psmp,$total_psma,$total_pd1,$total_pd3,$total_ps1,$total_ps2,$total_ps3);

						$this->data['label'] = $arr_label;

						$this->data['label_pend'] = $arr_pend_label;

						$this->data['total'] = $total;

						$this->data['total_men'] = $total_men;
	            		$this->data['total_women'] = $total_women;

	            		$this->data['arr_men'] = $arr_men;
	            		$this->data['arr_women'] = $arr_women;

	            		$this->data['arr_pend_men'] = $arr_pend_men;
	            		$this->data['arr_pend_women'] = $arr_pend_women;

	            		$this->data['sub_kategori'] = $sub_kategori;

					} else if(!empty($jenis_data) && $jenis_data == 'pendidikan') {

						if(!empty($kategori_data)) {
							if(!empty($sub_kategori_data)) {
								$this->session->unset_userdata('sub_kategori_data');
								$pendidikan = $this->infografis_model->getDataPendidikanByCat($kategori_data, $sub_kategori_data);
							} else {
								$this->session->unset_userdata('sub_kategori_data');
								$pendidikan = $this->infografis_model->getDataPendidikanByCat($kategori_data, '');
							}
						} else {
							if(!empty($sub_kategori_data)) {
								$pendidikan = $this->infografis_model->getDataPendidikanByCat('', $sub_kategori_data);
							} else {
								$pendidikan = $this->infografis_model->getDataPendidikan();
							}
						}

						$total = 0;
						$total_men = 0;
						$total_women = 0;
						$arr_men = '';
						$arr_women = '';
						$sub_kategori = array();

						$total_lsd = 0;
						$total_psd = 0;
						$total_lsmp = 0;
						$total_psmp = 0;
						$total_lsma = 0;
						$total_psma = 0;
						$total_ld1 = 0;
						$total_pd1 = 0;
						$total_ld3 = 0;
						$total_pd3 = 0;
						$total_ls1 = 0;
						$total_ps1 = 0;
						$total_ls2 = 0;
						$total_ps2 = 0;
						$total_ls3 = 0;
						$total_ps3 = 0;
 
 						if(!empty($pendidikan)) {
							foreach($pendidikan as $row) {
								$total += $row->total_pegawai;
								$total_men += $row->L;
								$total_women += $row->P;
								$sub_kategori[] = array( 
									'kode' => $row->kd_skpd,
									'nama' => $row->nama
								);
								$total_lsd += $row->LSD;
								$total_psd += $row->PSD;
								$total_lsmp += $row->LSMP;
								$total_psmp += $row->PSMP;
								$total_lsma += $row->LSMA;
								$total_psma += $row->PSMA;
								$total_ld1 += $row->LD1;
								$total_pd1 += $row->PD1;
								$total_ld3 += $row->LD3;
								$total_pd3 += $row->PD3;
								$total_ls1 += $row->LS1;
								$total_ps1 += $row->PS1;
								$total_ls2 += $row->LS2;
								$total_ps2 += $row->PS2;
								$total_ls3 += $row->LS3;
								$total_ps3 += $row->PS3;
							}
						}

						$this->data['name'] = 'Pendidikan';

						$arr_label = array('SD','SMP','SMA','D1','D3','S1','S2','S3');
						$arr_men = array($total_lsd,$total_lsmp,$total_lsma,$total_ld1,$total_ld3,$total_ls1,$total_ls2,$total_ls3);
						$arr_women = array($total_psd,$total_psmp,$total_psma,$total_pd1,$total_pd3,$total_ps1,$total_ps2,$total_ps3);
						$arr_pend_label = array('SD','SMP','SMA','D1','D3','S1','S2','S3');
						$arr_pend_men = array($total_lsd,$total_lsmp,$total_lsma,$total_ld1,$total_ld3,$total_ls1,$total_ls2,$total_ls3);
						$arr_pend_women = array($total_psd,$total_psmp,$total_psma,$total_pd1,$total_pd3,$total_ps1,$total_ps2,$total_ps3);

						$this->data['label'] = $arr_label;

						$this->data['label_pend'] = $arr_pend_label;

						$this->data['total'] = $total;

						$this->data['total_men'] = $total_men;
	            		$this->data['total_women'] = $total_women;

	            		$this->data['arr_men'] = $arr_men;
	            		$this->data['arr_women'] = $arr_women;

	            		$this->data['arr_pend_men'] = $arr_pend_men;
	            		$this->data['arr_pend_women'] = $arr_pend_women;

	            		$this->data['sub_kategori'] = $sub_kategori;

					} else if(!empty($jenis_data) && $jenis_data == 'diklat') {

						if(!empty($kategori_data)) {
							if(!empty($sub_kategori_data)) {
								$this->session->unset_userdata('sub_kategori_data');
								$diklat = $this->infografis_model->getDataDiklatByCat($kategori_data, $sub_kategori_data);
							} else {
								$this->session->unset_userdata('sub_kategori_data');
								$diklat = $this->infografis_model->getDataDiklatByCat($kategori_data, '');
							}
						} else {
							if(!empty($sub_kategori_data)) {
								$diklat = $this->infografis_model->getDataDiklatByCat('', $sub_kategori_data);
							} else {
								$diklat = $this->infografis_model->getDataDiklat();
							}
						}

						$total = 0;
						$total_men = 0;
						$total_women = 0;
						$total_l1 = 0;
						$total_l2 = 0;
						$total_l3 = 0;
						$total_l4 = 0;
						$total_p1 = 0;
						$total_p2 = 0;
						$total_p3 = 0;
						$total_p4 = 0;
						$arr_men = '';
						$arr_women = '';
						$sub_kategori = array();

						$total_lsd = 0;
						$total_psd = 0;
						$total_lsmp = 0;
						$total_psmp = 0;
						$total_lsma = 0;
						$total_psma = 0;
						$total_ld1 = 0;
						$total_pd1 = 0;
						$total_ld3 = 0;
						$total_pd3 = 0;
						$total_ls1 = 0;
						$total_ps1 = 0;
						$total_ls2 = 0;
						$total_ps2 = 0;
						$total_ls3 = 0;
						$total_ps3 = 0;
 
 						if(!empty($diklat)) {
							foreach($diklat as $row) {
								$total += $row->total_pegawai;
								$total_men += $row->L;
								$total_women += $row->P;
								$total_l1 += $row->L1;
								$total_l2 += $row->L2;
								$total_l3 += $row->L3;
								$total_l4 += $row->L4;
								$total_p1 += $row->P1;
								$total_p2 += $row->P2;
								$total_p3 += $row->P3;
								$total_p4 += $row->P4;
								$sub_kategori[] = array( 
									'kode' => $row->kd_skpd,
									'nama' => $row->nama
								);
								$total_lsd += $row->LSD;
								$total_psd += $row->PSD;
								$total_lsmp += $row->LSMP;
								$total_psmp += $row->PSMP;
								$total_lsma += $row->LSMA;
								$total_psma += $row->PSMA;
								$total_ld1 += $row->LD1;
								$total_pd1 += $row->PD1;
								$total_ld3 += $row->LD3;
								$total_pd3 += $row->PD3;
								$total_ls1 += $row->LS1;
								$total_ps1 += $row->PS1;
								$total_ls2 += $row->LS2;
								$total_ps2 += $row->PS2;
								$total_ls3 += $row->LS3;
								$total_ps3 += $row->PS3;
							}
						}

						$this->data['name'] = 'Diklat';

						$arr_label = array('Diklat I','Diklat II','Diklat III','Diklat IV');
						$arr_men = array($total_l1,$total_l2,$total_l3,$total_l4);
						$arr_women = array($total_p1,$total_p2,$total_p3,$total_p4);
						$arr_pend_label = array('SD','SMP','SMA','D1','D3','S1','S2','S3');
						$arr_pend_men = array($total_lsd,$total_lsmp,$total_lsma,$total_ld1,$total_ld3,$total_ls1,$total_ls2,$total_ls3);
						$arr_pend_women = array($total_psd,$total_psmp,$total_psma,$total_pd1,$total_pd3,$total_ps1,$total_ps2,$total_ps3);

						$this->data['label'] = $arr_label;

						$this->data['label_pend'] = $arr_pend_label;

						$this->data['total'] = $total;

						$this->data['total_men'] = $total_men;
	            		$this->data['total_women'] = $total_women;

	            		$this->data['arr_men'] = $arr_men;
	            		$this->data['arr_women'] = $arr_women;

	            		$this->data['arr_pend_men'] = $arr_pend_men;
	            		$this->data['arr_pend_women'] = $arr_pend_women;

	            		$this->data['sub_kategori'] = $sub_kategori;

					}

				}
	
	            /* Load Template */
	            $this->template->admin_render('admin/infografis/index', $this->data);
        	}else{
        		redirect('member','refresh');
        	}
        }
	}
}
