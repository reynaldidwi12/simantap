<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Pengajuan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();

		/* directory that will be used to store files */
		$this->config->set_item('files_directory', FCPATH . '.ELFINDER');
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_pengajuan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('admin/pengajuan_model' );
		
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "admin/pengajuan/index";
			$config ["total_rows"] = $this->pengajuan_model->record_count();
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$this->data  ['number'] = 0;
			} else {
				$this->data  ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['pengajuan'] = $this->pengajuan_model->fetchAll($config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('admin/pengajuan/index', $this->data);
		}
	}

	public function verify($id=null) {
	
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {

			if(empty($id)) {
				redirect ('admin/pengajuan/index','refresh');
			}

			if($this->input->post('submit')){

				$no_urut = $this->input->post('no_urut');

				$tmpArr = array();
				foreach($no_urut as $key => $value) {
					$name = 'verify'.$value;
					$arr = array(
						'no_urut' => $value,
						'filename' => $this->input->post('filename')[$key],
						'answer' => $this->input->post($name),
					);

					array_push($tmpArr, $arr);
				}

				$data = array(
		            'validasi' => json_encode($tmpArr),
		            'status_validasi' => 2,
		        );

		        $this->pengajuan_model->updateRow('pengajuan_tbl', 'id', $id, $data);
			} 

			$pengajuan = $this->pengajuan_model->getDataById($id);
			$this->data['topDirectory'] = $this->config->item('files_directory');
			$this->data['userNip'] = $pengajuan['nip'];
			$this->data['pengajuan'] = $pengajuan;
			$this->data['profil'] = $this->pengajuan_model->get_data_pegawai($pengajuan['nip']);
			$this->data['berkas'] = $this->pengajuan_model->get_data_by('daftar_pengajuan',$pengajuan['kategori'],'kategori',$pengajuan['sub_kategori'],'sub_kategori','no_urut');
	    	
			$this->template->admin_render('admin/pengajuan/form',$this->data);
		}
	}
	
	public function remove($id=null) {
		$this->pengajuan_model->delete($id);
		redirect ('admin/pengajuan/index','refresh');
	}

	public function dibawa_ke_bkn($id=null){
		
		$pemeriksa = $this->session->userdata('user_name');

		if(empty($id)) {
			redirect ('admin/pengajuan/index','refresh');
		}

        $pengajuan = $this->pengajuan_model->getDataById($id);

        $tmpStatus = json_decode($pengajuan['status'], true);

		$arr = array( 
            'status' => 'berkas dibawa bkn',
            'note' => '',
            'date' => strtotime(date('Y-m-d H:i:s'))
        );
        array_push($tmpStatus['data'], $arr);

        $data = array(
            'status' => json_encode($tmpStatus),
            'tanggal_kirim' => date('Y-m-d H:i:s'),
            'user_kirim' => $pemeriksa,
        );

        $this->pengajuan_model->updateRow('pengajuan_tbl', 'id', $id, $data);
        redirect ('admin/pengajuan/index','refresh');
    }
	
	public function find(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$column = $this->input->post('column');
				$query = trim($this->input->post('data'));
				
				$option = array(
					'user_column'=>$column,
					'user_data'=>$query
				);
				$this->session->set_userdata($option);
			}else{
			   $query = trim($this->uri->segment ( 4 ));
			   $column = $this->uri->segment ( 5 );
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "admin/pengajuan/find/".$query."/".$column;
			$config ["total_rows"] = $this->pengajuan_model->search_count($column,$query);
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 6;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 6 ) == "") {
				$this->data  ['number'] = 0;
			} else {
				$this->data  ['number'] = $this->uri->segment ( 6 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
				
			$this->data ['pengajuan'] = $this->pengajuan_model->search($column,$query,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('admin/pengajuan/index', $this->data);
		}
	}

	function berkas_on_check(){
		$id = $this->input->post('id');

		if(empty($id)) {
			redirect ('admin/pengajuan/index','refresh');
		}

        $pengajuan = $this->pengajuan_model->getDataById($id);
        $peg = $this->pengajuan_model->get_data_pegawai($pengajuan['nip']);

        $tmpStatus = json_decode($pengajuan['status'], true);

  		$keys = array_keys($tmpStatus['data']);
     	$last_key = array_pop($keys);

		if($tmpStatus['data'][$last_key]['status'] !== 'dalam pengecekan') {
			$arr = array( 
	            'status' => 'dalam pengecekan',
	            'note' => 'Sedang pengecekan pengesahan data yang diajukan oleh '.$peg[0]['nama'].' NIP '.$peg[0]['nip'],
	            'date' => strtotime(date('Y-m-d H:i:s'))
	        );
	        array_push($tmpStatus['data'], $arr);

	        $data = array(
	            'status' => json_encode($tmpStatus)
	        );

	        if(empty($pengajuan['validasi']) || $tmpStatus['data'][$last_key]['status'] == 'berkas dikirim ulang' ) {
	        	$this->pengajuan_model->updateRow('pengajuan_tbl', 'id', $id, $data);
	        }
	    }
    }

    function diterima(){
		$id = $this->input->post('id');
		$pemeriksa = $this->session->userdata('user_name');

		if(empty($id)) {
			redirect ('admin/pengajuan/index','refresh');
		}

        $pengajuan = $this->pengajuan_model->getDataById($id);
        @$pegVal = $this->pengajuan_model->get_data_pegawai(@$pemeriksa);

        $tmpStatus = json_decode($pengajuan['status'], true);

		$arr = array( 
            'status' => 'validasi diterima',
            'note' => 'Pengajuan anda telah di ACC oleh '.@$pegVal[0]['nama'].' NIP '.@$pegVal[0]['nip'].' dan Anda diharuskan membawa berkas fisik ke BKPSDM',
            'date' => strtotime(date('Y-m-d H:i:s'))
        );
        array_push($tmpStatus['data'], $arr);

        $data = array(
            'status' => json_encode($tmpStatus)
        );

        $result = $this->pengajuan_model->updateRow('pengajuan_tbl', 'id', $id, $data);

        if($result) {
        	echo 'success';
        }
    }

    function fisik_diterima(){
		$id = $this->input->post('id');
		$pemeriksa = $this->session->userdata('user_name');

		if(empty($id)) {
			redirect ('admin/pengajuan/index','refresh');
		}

        $pengajuan = $this->pengajuan_model->getDataById($id);
        @$pegVal = $this->pengajuan_model->get_data_pegawai(@$pemeriksa);

        $tmpStatus = json_decode($pengajuan['status'], true);

		$arr = array( 
            'status' => 'berkas fisik diterima',
            'note' => 'Berkas fisik diterima oleh '.@$pegVal[0]['nama'].' NIP '.@$pegVal[0]['nip'].'.',
            'date' => strtotime(date('Y-m-d H:i:s'))
        );
        array_push($tmpStatus['data'], $arr);

        $data = array(
            'status' => json_encode($tmpStatus)
        );

        $result = $this->pengajuan_model->updateRow('pengajuan_tbl', 'id', $id, $data);

        if($result) {
        	echo 'success';
        }
    }

    function kirim_ke_bkn(){
		$id = $this->input->post('id');
		$pemeriksa = $this->session->userdata('user_name');

		if(empty($id)) {
			redirect ('admin/pengajuan/index','refresh');
		}

        $pengajuan = $this->pengajuan_model->getDataById($id);

        $tmpStatus = json_decode($pengajuan['status'], true);

		$arr = array( 
            'status' => 'berkas dibawa bkn',
            'note' => '',
            'date' => strtotime(date('Y-m-d H:i:s'))
        );
        array_push($tmpStatus['data'], $arr);

        $data = array(
            'status' => json_encode($tmpStatus),
            'tanggal_kirim' => date('Y-m-d H:i:s'),
            'user_kirim' => $pemeriksa,
        );

        $result = $this->pengajuan_model->updateRow('pengajuan_tbl', 'id', $id, $data);

        if($result) {
        	echo 'success';
        }
    }

    function kembalikan_ke_pegawai(){
		$id = $this->input->post('id');

		if(empty($id)) {
			redirect ('admin/pengajuan/index','refresh');
		}

        $pengajuan = $this->pengajuan_model->getDataById($id);

        $tmpStatus = json_decode($pengajuan['status'], true);

        $validasi = json_decode($pengajuan['validasi'], true);

        $year = date('Y');

        $topDirectory = $this->config->item('files_directory');
		$pathToOpen = $topDirectory . '/' . $pengajuan['nip'];
		$pathPensiun = $pathToOpen . '/' . $pengajuan['kategori'];
		$pathYear = $pathPensiun . '/' . $year;

		foreach($validasi as $key => $val) {
			if($val['answer']=='Y') continue;

			$filePath = $pathYear . '/' . $val['filename'];
			$fileName = pathinfo($val['filename'], PATHINFO_FILENAME);
			$ext = pathinfo($val['filename'], PATHINFO_EXTENSION);
			$newFilePath = $pathYear . '/' . $fileName . '-tidakvalid' . '.' . $ext;
			
			if (file_exists($filePath)) {
				rename($filePath, $newFilePath);
			}
		}

		$arr = array( 
            'status' => 'berkas dikembalikan',
            'note' => 'Pengajuan Anda dikembalikan, dimohon untuk memperbaiki berkas yang tidak valid, kemudian silahkan untuk melakukan pengiriman berkas kembali.',
            'date' => strtotime(date('Y-m-d H:i:s'))
        );
        array_push($tmpStatus['data'], $arr);

        $data = array(
            'status' => json_encode($tmpStatus),
            'status_validasi' => 1,
        );

        $result = $this->pengajuan_model->updateRow('pengajuan_tbl', 'id', $id, $data);

        if($result) {
        	echo 'success';
        }
    }
	
}