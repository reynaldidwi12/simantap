<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/dashboard_model');
    }

	public function index()
	{	
        if ( !$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
        	if($this->ion_auth->is_admin()){
	            /* Title Page */
	            $this->page_title->push(lang('menu_dashboard'));
	            $this->data['pagetitle'] = $this->page_title->show();
	
	            /* Breadcrumbs */
	            $this->data['breadcrumb'] = $this->breadcrumbs->show();
	
	            /* Data */
	            $this->data['count_users']       = $this->dashboard_model->get_count_record('users');
	            $this->data['count_groups']      = $this->dashboard_model->get_count_record('groups');
	            $this->data['disk_totalspace']   = $this->dashboard_model->disk_totalspace(DIRECTORY_SEPARATOR);
	            $this->data['disk_freespace']    = $this->dashboard_model->disk_freespace(DIRECTORY_SEPARATOR);
	            $this->data['disk_usespace']     = $this->data['disk_totalspace'] - $this->data['disk_freespace'];
	            $this->data['disk_usepercent']   = $this->dashboard_model->disk_usepercent(DIRECTORY_SEPARATOR, FALSE);
	            $this->data['memory_usage']      = $this->dashboard_model->memory_usage();
	            $this->data['memory_peak_usage'] = $this->dashboard_model->memory_peak_usage(TRUE);
	            $this->data['memory_usepercent'] = $this->dashboard_model->memory_usepercent(TRUE, FALSE);
	            $this->data['jumlah_skpd'] 		 = $this->dashboard_model->jml_skpd();
	            $this->data['jumlah_skpd2'] 	= $this->dashboard_model->jml_skpd2();
	            $this->data['jumlah_skpd3'] 	= $this->dashboard_model->jml_skpd3();
	            $this->data['jumlah_skpd4'] 	= $this->dashboard_model->jml_skpd4();
	            $this->data['jumlah_skpd5'] 	= $this->dashboard_model->jml_skpd5();
	            $this->data['jumlah_skpd6'] 	= $this->dashboard_model->jml_skpd6();

	            if($this->session->userdata('group_id')=='3') {
	            	$this->data['jumlah_pegawai'] 	 = $this->dashboard_model->jml_pegawai_per_skpd($this->session->userdata("ss_skpd"));
	            	$this->data['jumlah_naik_pangkat'] = $this->dashboard_model->jml_naik_pangkat_per_skpd($this->session->userdata("ss_skpd"));
	            	$this->data['jumlah_pensiun'] = $this->dashboard_model->jml_pensiun_per_skpd($this->session->userdata("ss_skpd"));
	            } else {
	            	$this->data['jumlah_pegawai'] 	 = $this->dashboard_model->jml_pegawai();
	            	$this->data['jumlah_naik_pangkat'] = $this->dashboard_model->jml_naik_pangkat();
	            	$this->data['jumlah_pensiun'] = $this->dashboard_model->jml_pensiun();
	            }

	            //-----EDITED-----//
	   //          $this->data['jumlah_pegawai_golongan'] = NULL;
				// $this->data['jumlah_pegawai_golongan2'] = NULL;

				if($this->input->post('golongan-select')) {
					$gol = $this->input->post('golongan-select');
				} else {
					$gol = '';
				}

				$this->data['gol'] = $gol;


				if($this->session->userdata('group_id')=='3') {
	            	$this->data['jumlah_pegawai_golongan'] = $this->dashboard_model->jumlah_pegawai_golongan_per_skpd($this->session->userdata("ss_skpd"));
	            	$this->data['jumlah_pegawai_pendidikan'] = $this->dashboard_model->jumlah_pegawai_pendidikan_per_skpd($this->session->userdata("ss_skpd"));
	            	$this->data['jumlah_pegawai_eselon'] = $this->dashboard_model->jumlah_pegawai_eselon_per_skpd($gol, $this->session->userdata("ss_skpd"));
	            } else {
	            	$this->data['jumlah_pegawai_golongan'] = $this->dashboard_model->jumlah_pegawai_golongan();
	            	$this->data['jumlah_pegawai_pendidikan'] = $this->dashboard_model->jumlah_pegawai_pendidikan();
	            	$this->data['jumlah_pegawai_eselon'] = $this->dashboard_model->jumlah_pegawai_eselon($gol);
	            }
				$this->data['jumlah_pegawai_golongan2'] = $this->data['jumlah_pegawai_golongan'][0];
				$this->data['jumlah_pegawai_pendidikan2'] = $this->data['jumlah_pegawai_pendidikan'][0];
	
	            /* Load Template */
	            $this->template->admin_render('admin/dashboard/index', $this->data);
        	}else{
        		redirect('member','refresh');
        	}
        }
	}
}
