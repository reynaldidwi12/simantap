<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class pendidikan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		
		/* Load Library */
		$this->load->library('session');
		$this->load->library('m_pdf');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_bezetting_pendidikan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('url'));
		$this->load->model ('bezetting/pendidikan_model' );
		
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "bezetting/pendidikan/index";
			$config ["total_rows"] = 1;
			$config ["per_page"] = 50;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$this->data  ['number'] = 0;
			} else {
				$this->data  ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['model_obj'] = $this;

			if($this->session->userdata('group_id')=='3') {
				$this->data ['skpd'] = $this->pendidikan_model->fetchAll_per_skpd($this->session->userdata("ss_skpd"), $config ["per_page"], $page);
			} else {
				$this->data ['skpd'] = $this->pendidikan_model->fetchAll($config ["per_page"], $page);
			}

			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('bezetting/pendidikan/index', $this->data);
		}
	}
	
	public function category(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$category = $this->input->post('category');
				
				$option = array(
					'category'=>$category
				);
				$this->session->set_userdata($option);
			}else{
			   $category = $this->uri->segment ( 4 );
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "bezetting/pendidikan/category/".$category;
			$config ["total_rows"] = $this->pendidikan_model->search_count($category);
			$config ["per_page"] = 50;
			$config ["uri_segment"] = 5;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 5 ) == "") {
				$this->data  ['number'] = 0;
			} else {
				$this->data  ['number'] = $this->uri->segment ( 5 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 5 )) ? $this->uri->segment ( 5 ) : 0;
				
			$this->data ['skpd'] = $this->pendidikan_model->search($category,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('bezetting/pendidikan/index', $this->data);
		}
	}
	

	public function get_record_count_per_skpd() {
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			$result = $this->pendidikan_model->record_count_per_skpd();

			header('Content-type: application/json');
			$json = json_encode($result);
			echo $json;
			
			exit;
		}
	}

	public function exportExcel() {

        $this->load->library('excel');

		$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
                  $this->excel->getActiveSheet()->setTitle('Bezetting Pegawai Negeri');

  				//STYLING
  				$styleArray = array(
								'borders' => array('allborders' =>
												array('style' => PHPExcel_Style_Border::BORDER_THIN,
												'color' =>	array('argb' => '0000'),
  							),
  						),

  					);
				$styleCenter = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);

			$no = 7;

			//data
			$this->excel->getActiveSheet()->mergeCells('A1:W1');
              $this->excel->getActiveSheet()->setCellValue('A1', 'BEZETTING PEGAWAI NEGERI SIPIL DAERAH');
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('A2:W2');
              $this->excel->getActiveSheet()->setCellValue('A2', 'BERDASARKAN PENDIDIKAN');
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);

			  //set font
			  $this->excel->getActiveSheet()->getStyle('A5:W100')->getFont()->setSize(10);

			  //INIT WIDTH
			  $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			  $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
			  $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(5);

			  //set column name
			  $this->excel->getActiveSheet()->mergeCells('A5:A7');
              $this->excel->getActiveSheet()->setCellValue('A5', 'No');
			  $this->excel->getActiveSheet()->mergeCells('B5:B7');
			  $this->excel->getActiveSheet()->setCellValue('B5', 'Unit Kerja');
			  $this->excel->getActiveSheet()->mergeCells('C5:C7');
			  $this->excel->getActiveSheet()->setCellValue('C5', 'JML');
			  $this->excel->getActiveSheet()->mergeCells('D5:E6');
			  $this->excel->getActiveSheet()->setCellValue('D5', 'Kelamin');
			  $this->excel->getActiveSheet()->setCellValue('D7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('E7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('F5:U5');
			  $this->excel->getActiveSheet()->setCellValue('F5', 'TINGKAT PENDIDIKAN');
			  $this->excel->getActiveSheet()->mergeCells('F6:G6');
			  $this->excel->getActiveSheet()->setCellValue('F6', 'S3');
			  $this->excel->getActiveSheet()->setCellValue('F7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('G7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('H6:I6');
			  $this->excel->getActiveSheet()->setCellValue('H6', 'S2');
			  $this->excel->getActiveSheet()->setCellValue('H7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('I7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('J6:K6');
			  $this->excel->getActiveSheet()->setCellValue('J6', 'S1');
			  $this->excel->getActiveSheet()->setCellValue('J7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('K7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('L6:M6');
			  $this->excel->getActiveSheet()->setCellValue('L6', 'D3');
			  $this->excel->getActiveSheet()->setCellValue('L7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('M7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('N6:O6');
			  $this->excel->getActiveSheet()->setCellValue('N6', 'D1');
			  $this->excel->getActiveSheet()->setCellValue('N7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('O7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('P6:Q6');
			  $this->excel->getActiveSheet()->setCellValue('P6', 'SMA');
			  $this->excel->getActiveSheet()->setCellValue('P7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('Q7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('R6:S6');
			  $this->excel->getActiveSheet()->setCellValue('R6', 'SMP');
			  $this->excel->getActiveSheet()->setCellValue('R7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('S7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('T6:U6');
			  $this->excel->getActiveSheet()->setCellValue('R6', 'SD');
			  $this->excel->getActiveSheet()->setCellValue('R7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('S7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('V5:W6');
			  $this->excel->getActiveSheet()->setCellValue('V5', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('V7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('W7', 'P');

			  $this->excel->getActiveSheet()->setCellValue('A8', '1');
			  $this->excel->getActiveSheet()->setCellValue('B8', '2');
			  $this->excel->getActiveSheet()->setCellValue('C8', '3');
			  $this->excel->getActiveSheet()->setCellValue('D8', '4');
			  $this->excel->getActiveSheet()->setCellValue('E8', '5');
			  $this->excel->getActiveSheet()->setCellValue('F8', '6');
			  $this->excel->getActiveSheet()->setCellValue('G8', '7');
			  $this->excel->getActiveSheet()->setCellValue('H8', '8');
			  $this->excel->getActiveSheet()->setCellValue('I8', '9');
			  $this->excel->getActiveSheet()->setCellValue('J8', '10');
			  $this->excel->getActiveSheet()->setCellValue('K8', '11');
			  $this->excel->getActiveSheet()->setCellValue('L8', '12');
			  $this->excel->getActiveSheet()->setCellValue('M8', '13');
			  $this->excel->getActiveSheet()->setCellValue('N8', '14');
			  $this->excel->getActiveSheet()->setCellValue('O8', '15');
			  $this->excel->getActiveSheet()->setCellValue('P8', '16');
			  $this->excel->getActiveSheet()->setCellValue('Q8', '17');
			  $this->excel->getActiveSheet()->setCellValue('R8', '18');
			  $this->excel->getActiveSheet()->setCellValue('S8', '19');
			  $this->excel->getActiveSheet()->setCellValue('T8', '20');
			  $this->excel->getActiveSheet()->setCellValue('U8', '21');
			  $this->excel->getActiveSheet()->setCellValue('V8', '22');
			  $this->excel->getActiveSheet()->setCellValue('W8', '23');
            
        $datas = $this->pendidikan_model->record_count_per_skpd();

		$i=9;
		$j=14;
		$k=35;
		$l=43;
		$a=1;
		$b=1;
        foreach($datas as $row) {
			if (in_array($row->category, array('1','2','3','4'))) {
				if(count($row->category) == '1') {
					$this->excel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, str_romawi_format($row->category))
					->setCellValue('B'.$i, $row->nama)
					->setCellValue('C'.$i, $row->total_pegawai)
					->setCellValue('D'.$i, $row->L)
					->setCellValue('E'.$i, $row->P)
					->setCellValue('F'.$i, $row->LS3)
					->setCellValue('G'.$i, $row->PS3)
					->setCellValue('H'.$i, $row->LS2)
					->setCellValue('I'.$i, $row->PS2)
					->setCellValue('J'.$i, $row->LS1)
					->setCellValue('K'.$i, $row->PS1)
					->setCellValue('L'.$i, $row->LD3)
					->setCellValue('M'.$i, $row->PD3)
					->setCellValue('N'.$i, $row->LD1)
					->setCellValue('O'.$i, $row->PD1)
					->setCellValue('P'.$i, $row->LSMA)
					->setCellValue('Q'.$i, $row->PSMA)
					->setCellValue('R'.$i, $row->LSMP)
					->setCellValue('S'.$i, $row->PSMP)
					->setCellValue('T'.$i, $row->LSD)
					->setCellValue('U'.$i, $row->PSD)
					->setCellValue('V'.$i, ($row->LS3+$row->LS2+$row->LS1+$row->LD3+$row->LD1+$row->LSMA+$row->LSMP+$row->LSD))
					->setCellValue('W'.$i, ($row->PS3+$row->PS2+$row->PS1+$row->PD3+$row->PD1+$row->PSMA+$row->PSMP+$row->PSD));
				}
				$i++;
			}
			$this->excel->getActiveSheet()->setCellValue('A13', 'V');
			$this->excel->getActiveSheet()->setCellValue('B13', 'DINAS');
			if (in_array($row->category, array('5'))) {
				$this->excel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, $a)
				->setCellValue('B'.$j, $row->nama)
				->setCellValue('C'.$j, $row->total_pegawai)
				->setCellValue('D'.$j, $row->L)
				->setCellValue('E'.$j, $row->P)
				->setCellValue('F'.$j, $row->LS3)
				->setCellValue('G'.$j, $row->PS3)
				->setCellValue('H'.$j, $row->LS2)
				->setCellValue('I'.$j, $row->PS2)
				->setCellValue('J'.$j, $row->LS1)
				->setCellValue('K'.$j, $row->PS1)
				->setCellValue('L'.$j, $row->LD3)
				->setCellValue('M'.$j, $row->PD3)
				->setCellValue('N'.$j, $row->LD1)
				->setCellValue('O'.$j, $row->PD1)
				->setCellValue('P'.$j, $row->LSMA)
				->setCellValue('Q'.$j, $row->PSMA)
				->setCellValue('R'.$j, $row->LSMP)
				->setCellValue('S'.$j, $row->PSMP)
				->setCellValue('T'.$j, $row->LSD)
				->setCellValue('U'.$j, $row->PSD)
				->setCellValue('V'.$j, ($row->LS3+$row->LS2+$row->LS1+$row->LD3+$row->LD1+$row->LSMA+$row->LSMP+$row->LSD))
				->setCellValue('W'.$j, ($row->PS3+$row->PS2+$row->PS1+$row->PD3+$row->PD1+$row->PSMA+$row->PSMP+$row->PSD));
				$a++;
				$j++;
			}
			$this->excel->getActiveSheet()->setCellValue('A34', 'VI');
			$this->excel->getActiveSheet()->setCellValue('B34', 'BADAN');
			if (in_array($row->category, array('6'))) {
				$this->excel->setActiveSheetIndex(0)
				->setCellValue('A'.$k, $b)
				->setCellValue('B'.$k, $row->nama)
				->setCellValue('C'.$k, $row->total_pegawai)
				->setCellValue('D'.$k, $row->L)
				->setCellValue('E'.$k, $row->P)
				->setCellValue('F'.$k, $row->LS3)
				->setCellValue('G'.$k, $row->PS3)
				->setCellValue('H'.$k, $row->LS2)
				->setCellValue('I'.$k, $row->PS2)
				->setCellValue('J'.$k, $row->LS1)
				->setCellValue('K'.$k, $row->PS1)
				->setCellValue('L'.$k, $row->LD3)
				->setCellValue('M'.$k, $row->PD3)
				->setCellValue('N'.$k, $row->LD1)
				->setCellValue('O'.$k, $row->PD1)
				->setCellValue('P'.$k, $row->LSMA)
				->setCellValue('Q'.$k, $row->PSMA)
				->setCellValue('R'.$k, $row->LSMP)
				->setCellValue('S'.$k, $row->PSMP)
				->setCellValue('T'.$k, $row->LSD)
				->setCellValue('U'.$k, $row->PSD)
				->setCellValue('V'.$k, ($row->LS3+$row->LS2+$row->LS1+$row->LD3+$row->LD1+$row->LSMA+$row->LSMP+$row->LSD))
				->setCellValue('W'.$k, ($row->PS3+$row->PS2+$row->PS1+$row->PD3+$row->PD1+$row->PSMA+$row->PSMP+$row->PSD));
				$b++;
				$k++;
			}
			if (in_array($row->category, array('7','8','9','10','11','12','13','14','15','16','17','18','19','20','21'))) {
				if(count($row->category) == '1') {
					$this->excel->setActiveSheetIndex(0)
					->setCellValue('A'.$l, str_romawi_format($row->category))
					->setCellValue('B'.$l, $row->nama)
					->setCellValue('C'.$l, $row->total_pegawai)
					->setCellValue('D'.$l, $row->L)
					->setCellValue('E'.$l, $row->P)
					->setCellValue('F'.$l, $row->LS3)
					->setCellValue('G'.$l, $row->PS3)
					->setCellValue('H'.$l, $row->LS2)
					->setCellValue('I'.$l, $row->PS2)
					->setCellValue('J'.$l, $row->LS1)
					->setCellValue('K'.$l, $row->PS1)
					->setCellValue('L'.$l, $row->LD3)
					->setCellValue('M'.$l, $row->PD3)
					->setCellValue('N'.$l, $row->LD1)
					->setCellValue('O'.$l, $row->PD1)
					->setCellValue('P'.$l, $row->LSMA)
					->setCellValue('Q'.$l, $row->PSMA)
					->setCellValue('R'.$l, $row->LSMP)
					->setCellValue('S'.$l, $row->PSMP)
					->setCellValue('T'.$l, $row->LSD)
					->setCellValue('U'.$l, $row->PSD)
					->setCellValue('V'.$l, ($row->LS3+$row->LS2+$row->LS1+$row->LD3+$row->LD1+$row->LSMA+$row->LSMP+$row->LSD))
					->setCellValue('W'.$l, ($row->PS3+$row->PS2+$row->PS1+$row->PD3+$row->PD1+$row->PSMA+$row->PSMP+$row->PSD));
				}
				$l++;
			}
		}
		$m = ($l-1);
		$this->excel->getActiveSheet()->getStyle('A'.$l)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A'.$l)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A'.$l.':AS'.$l)->getFont()->setBold(true);
		$this->excel->getActiveSheet()->mergeCells('A'.$l.':B'.$l);
		$this->excel->getActiveSheet()->setCellValue('A'.$l, 'JUMLAH');
		$this->excel->setActiveSheetIndex(0)
			->setCellValue('C'.$l, '=SUM(C9:C'.$m.')')
			->setCellValue('D'.$l, '=SUM(D9:D'.$m.')')
			->setCellValue('E'.$l, '=SUM(E9:E'.$m.')')
			->setCellValue('F'.$l, '=SUM(F9:F'.$m.')')
			->setCellValue('G'.$l, '=SUM(G9:G'.$m.')')
			->setCellValue('H'.$l, '=SUM(H9:H'.$m.')')
			->setCellValue('I'.$l, '=SUM(I9:I'.$m.')')
			->setCellValue('J'.$l, '=SUM(J9:J'.$m.')')
			->setCellValue('K'.$l, '=SUM(K9:K'.$m.')')
			->setCellValue('L'.$l, '=SUM(L9:L'.$m.')')
			->setCellValue('M'.$l, '=SUM(M9:M'.$m.')')
			->setCellValue('N'.$l, '=SUM(N9:N'.$m.')')
			->setCellValue('O'.$l, '=SUM(O9:O'.$m.')')
			->setCellValue('P'.$l, '=SUM(P9:P'.$m.')')
			->setCellValue('Q'.$l, '=SUM(Q9:Q'.$m.')')
			->setCellValue('R'.$l, '=SUM(R9:R'.$m.')')
			->setCellValue('S'.$l, '=SUM(S9:S'.$m.')')
			->setCellValue('T'.$l, '=SUM(T9:T'.$m.')')
			->setCellValue('U'.$l, '=SUM(U9:U'.$m.')')
			->setCellValue('V'.$l, '=SUM(V9:V'.$m.')')
			->setCellValue('W'.$l, '=SUM(W9:W'.$m.')');

		ob_end_clean();
		$filename='Rekapitulasi.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');
        exit;
	}

	public function exportPdf()
	{
		$this->data['datas'] = $this->pendidikan_model->record_count_per_skpd();
		$this->load->view('bezetting/pendidikan/cetak', $this->data);
		
		$sumber = $this->load->view('bezetting/pendidikan/cetak', $this->data, TRUE);
        $html = $sumber;


        $pdfFilePath = "Cetak_Pendidikan.pdf";

        $pdf = $this->m_pdf->load();

        $pdf->AddPage('L');
		$pdf=new mPDF('','A4-L');
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);
        
        $pdf->Output($pdfFilePath, "D");
        exit();
		
	}
}