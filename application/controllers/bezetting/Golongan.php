<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class golongan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		
		/* Load Library */
		$this->load->library('session');
		$this->load->library('m_pdf');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_bezetting_golongan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('url'));
		$this->load->model ('bezetting/golongan_model' );
		
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "bezetting/golongan/index";
			$config ["total_rows"] = $this->golongan_model->record_count();
			$config ["per_page"] = 50;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$this->data  ['number'] = 0;
			} else {
				$this->data  ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['model_obj'] = $this;

			if($this->session->userdata('group_id')=='3') {
				$this->data ['skpd'] = $this->golongan_model->fetchAll_per_skpd($this->session->userdata("ss_skpd"), $config ["per_page"], $page);
			} else {
				$this->data ['skpd'] = $this->golongan_model->fetchAll($config ["per_page"], $page);
			}

			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('bezetting/golongan/index', $this->data);
		}
	}
	
	public function category(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$category = $this->input->post('category');
				
				$option = array(
					'category'=>$category
				);
				$this->session->set_userdata($option);
			}else{
			   $category = $this->uri->segment ( 4 );
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "bezetting/golongan/category/".$category;
			$config ["total_rows"] = $this->golongan_model->search_count($category);
			$config ["per_page"] = 50;
			$config ["uri_segment"] = 5;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 5 ) == "") {
				$this->data  ['number'] = 0;
			} else {
				$this->data  ['number'] = $this->uri->segment ( 5 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 5 )) ? $this->uri->segment ( 5 ) : 0;
				
			$this->data ['skpd'] = $this->golongan_model->search($category,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('bezetting/golongan/index', $this->data);
		}
	}
	

	public function get_record_count_per_skpd() {
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			$result = $this->golongan_model->record_count_per_skpd();

			header('Content-type: application/json');
			$json = json_encode($result);
			echo $json;
			
			exit;
		}
	}

	public function exportExcel() {

        $this->load->library('excel');

		$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
                  $this->excel->getActiveSheet()->setTitle('Bezetting Pegawai Negeri');

  				//STYLING
  				$styleArray = array(
								'borders' => array('allborders' =>
												array('style' => PHPExcel_Style_Border::BORDER_THIN,
												'color' =>	array('argb' => '0000'),
  							),
  						),

  					);
				$styleCenter = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);

			$no = 7;

			//data
			$this->excel->getActiveSheet()->mergeCells('A1:AS1');
              $this->excel->getActiveSheet()->setCellValue('A1', 'BEZETTING PEGAWAI NEGERI SIPIL DAERAH');
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('A2:AS2');
              $this->excel->getActiveSheet()->setCellValue('A2', 'BERDASARKAN GOLONGAN');
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);

			  //set font
			  $this->excel->getActiveSheet()->getStyle('A5:AS100')->getFont()->setSize(10);

			  //INIT WIDTH
			  $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			  $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
			  $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AF')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AG')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AH')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AI')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AJ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AK')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AL')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AM')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AN')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AO')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AP')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AQ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AR')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AS')->setWidth(5);

			  //set column name
			  $this->excel->getActiveSheet()->mergeCells('A5:A7');
              $this->excel->getActiveSheet()->setCellValue('A5', 'No');
			  $this->excel->getActiveSheet()->mergeCells('B5:B7');
			  $this->excel->getActiveSheet()->setCellValue('B5', 'Unit Kerja');
			  $this->excel->getActiveSheet()->mergeCells('C5:C7');
			  $this->excel->getActiveSheet()->setCellValue('C5', 'JML');
			  $this->excel->getActiveSheet()->mergeCells('D5:E6');
			  $this->excel->getActiveSheet()->setCellValue('D5', 'Kelamin');
			  $this->excel->getActiveSheet()->setCellValue('D7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('E7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('F5:O5');
			  $this->excel->getActiveSheet()->setCellValue('F5', 'Golongan IV');
			  $this->excel->getActiveSheet()->mergeCells('F6:G6');
			  $this->excel->getActiveSheet()->setCellValue('F6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('F7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('G7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('H6:I6');
			  $this->excel->getActiveSheet()->setCellValue('H6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('H7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('I7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('J6:K6');
			  $this->excel->getActiveSheet()->setCellValue('J6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('J7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('K7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('L6:M6');
			  $this->excel->getActiveSheet()->setCellValue('L6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('L7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('M7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('N6:O6');
			  $this->excel->getActiveSheet()->setCellValue('N6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('N7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('O7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('P5:Y5');
			  $this->excel->getActiveSheet()->setCellValue('P5', 'Golongan III');
			  $this->excel->getActiveSheet()->mergeCells('P6:Q6');
			  $this->excel->getActiveSheet()->setCellValue('P6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('P7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('Q7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('R6:S6');
			  $this->excel->getActiveSheet()->setCellValue('R6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('R7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('S7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('T6:U6');
			  $this->excel->getActiveSheet()->setCellValue('T6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('T7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('U7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('V6:W6');
			  $this->excel->getActiveSheet()->setCellValue('V6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('V7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('W7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('X6:Y6');
			  $this->excel->getActiveSheet()->setCellValue('X6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('X7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('Y7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('Z5:AI5');
			  $this->excel->getActiveSheet()->setCellValue('Z5', 'Golongan II');
			  $this->excel->getActiveSheet()->mergeCells('Z6:AA6');
			  $this->excel->getActiveSheet()->setCellValue('Z6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('Z7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AA7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AB6:AC6');
			  $this->excel->getActiveSheet()->setCellValue('AB6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('AB7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AC7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AD6:AE6');
			  $this->excel->getActiveSheet()->setCellValue('AD6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('AD7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AE7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AF6:AG6');
			  $this->excel->getActiveSheet()->setCellValue('AF6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('AF7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AG7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AH6:AI6');
			  $this->excel->getActiveSheet()->setCellValue('AH6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('AH7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AI7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('AJ5:AS5');
			  $this->excel->getActiveSheet()->setCellValue('AJ5', 'Golongan I');
			  $this->excel->getActiveSheet()->mergeCells('AJ6:AK6');
			  $this->excel->getActiveSheet()->setCellValue('AJ6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('AJ7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AK7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AL6:AM6');
			  $this->excel->getActiveSheet()->setCellValue('AL6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('AL7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AM7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AN6:AO6');
			  $this->excel->getActiveSheet()->setCellValue('AN6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('AN7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AO7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AP6:AQ6');
			  $this->excel->getActiveSheet()->setCellValue('AP6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('AP7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AQ7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AR6:AS6');
			  $this->excel->getActiveSheet()->setCellValue('AR6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('AR7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AS7', 'P');

			  $this->excel->getActiveSheet()->setCellValue('A8', '1');
			  $this->excel->getActiveSheet()->setCellValue('B8', '2');
			  $this->excel->getActiveSheet()->setCellValue('C8', '3');
			  $this->excel->getActiveSheet()->setCellValue('D8', '4');
			  $this->excel->getActiveSheet()->setCellValue('E8', '5');
			  $this->excel->getActiveSheet()->setCellValue('F8', '6');
			  $this->excel->getActiveSheet()->setCellValue('G8', '7');
			  $this->excel->getActiveSheet()->setCellValue('H8', '8');
			  $this->excel->getActiveSheet()->setCellValue('I8', '9');
			  $this->excel->getActiveSheet()->setCellValue('J8', '10');
			  $this->excel->getActiveSheet()->setCellValue('K8', '11');
			  $this->excel->getActiveSheet()->setCellValue('L8', '12');
			  $this->excel->getActiveSheet()->setCellValue('M8', '13');
			  $this->excel->getActiveSheet()->setCellValue('N8', '14');
			  $this->excel->getActiveSheet()->setCellValue('O8', '15');
			  $this->excel->getActiveSheet()->setCellValue('P8', '16');
			  $this->excel->getActiveSheet()->setCellValue('Q8', '17');
			  $this->excel->getActiveSheet()->setCellValue('R8', '18');
			  $this->excel->getActiveSheet()->setCellValue('S8', '19');
			  $this->excel->getActiveSheet()->setCellValue('T8', '20');
			  $this->excel->getActiveSheet()->setCellValue('U8', '21');
			  $this->excel->getActiveSheet()->setCellValue('V8', '22');
			  $this->excel->getActiveSheet()->setCellValue('W8', '23');
			  $this->excel->getActiveSheet()->setCellValue('X8', '24');
			  $this->excel->getActiveSheet()->setCellValue('Y8', '25');
			  $this->excel->getActiveSheet()->setCellValue('Z8', '26');
			  $this->excel->getActiveSheet()->setCellValue('AA8', '27');
			  $this->excel->getActiveSheet()->setCellValue('AB8', '28');
			  $this->excel->getActiveSheet()->setCellValue('AC8', '29');
			  $this->excel->getActiveSheet()->setCellValue('AD8', '30');
			  $this->excel->getActiveSheet()->setCellValue('AE8', '31');
			  $this->excel->getActiveSheet()->setCellValue('AF8', '32');
			  $this->excel->getActiveSheet()->setCellValue('AG8', '33');
			  $this->excel->getActiveSheet()->setCellValue('AH8', '34');
			  $this->excel->getActiveSheet()->setCellValue('AI8', '35');
			  $this->excel->getActiveSheet()->setCellValue('AJ8', '36');
			  $this->excel->getActiveSheet()->setCellValue('AK8', '37');
			  $this->excel->getActiveSheet()->setCellValue('AL8', '38');
			  $this->excel->getActiveSheet()->setCellValue('AM8', '39');
			  $this->excel->getActiveSheet()->setCellValue('AN8', '40');
			  $this->excel->getActiveSheet()->setCellValue('AO8', '41');
			  $this->excel->getActiveSheet()->setCellValue('AP8', '42');
			  $this->excel->getActiveSheet()->setCellValue('AQ8', '43');
			  $this->excel->getActiveSheet()->setCellValue('AR8', '44');
			  $this->excel->getActiveSheet()->setCellValue('AS8', '45');
            
        $datas = $this->golongan_model->record_count_per_skpd();

		$i=9;
		$j=14;
		$k=35;
		$l=43;
		$a=1;
		$b=1;
        foreach($datas as $row) {
			if (in_array($row->category, array('1','2','3','4'))) {
				if(count($row->category) == '1') {
					$this->excel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, str_romawi_format($row->category))
					->setCellValue('B'.$i, $row->nama)
					->setCellValue('C'.$i, $row->total_pegawai)
					->setCellValue('D'.$i, $row->L)
					->setCellValue('E'.$i, $row->P)
					->setCellValue('F'.$i, $row->L4D)
					->setCellValue('G'.$i, $row->P4D)
					->setCellValue('H'.$i, $row->L4C)
					->setCellValue('I'.$i, $row->P4C)
					->setCellValue('J'.$i, $row->L4B)
					->setCellValue('K'.$i, $row->P4B)
					->setCellValue('L'.$i, $row->L4A)
					->setCellValue('M'.$i, $row->P4A)
					->setCellValue('N'.$i, ($row->L4D+$row->L4C+$row->L4B+$row->L4A))
					->setCellValue('O'.$i, ($row->P4D+$row->P4C+$row->P4B+$row->P4A))
					->setCellValue('P'.$i, $row->L3D)
					->setCellValue('Q'.$i, $row->P3D)
					->setCellValue('R'.$i, $row->L3C)
					->setCellValue('S'.$i, $row->P3C)
					->setCellValue('T'.$i, $row->L3B)
					->setCellValue('U'.$i, $row->P3B)
					->setCellValue('V'.$i, $row->L3A)
					->setCellValue('W'.$i, $row->P3A)
					->setCellValue('X'.$i, ($row->L3D+$row->L3C+$row->L3B+$row->L3A))
					->setCellValue('Y'.$i, ($row->P3D+$row->P3C+$row->P3B+$row->P3A))
					->setCellValue('Z'.$i, $row->L2D)
					->setCellValue('AA'.$i, $row->P2D)
					->setCellValue('AB'.$i, $row->L2C)
					->setCellValue('AC'.$i, $row->P2C)
					->setCellValue('AD'.$i, $row->L2B)
					->setCellValue('AE'.$i, $row->P2B)
					->setCellValue('AF'.$i, $row->L2A)
					->setCellValue('AG'.$i, $row->P2A)
					->setCellValue('AH'.$i, ($row->L2D+$row->L2C+$row->L2B+$row->L2A))
					->setCellValue('AI'.$i, ($row->P2D+$row->P2C+$row->P2B+$row->P2A))
					->setCellValue('AJ'.$i, $row->L1D)
					->setCellValue('AK'.$i, $row->P1D)
					->setCellValue('AL'.$i, $row->L1C)
					->setCellValue('AM'.$i, $row->P1C)
					->setCellValue('AN'.$i, $row->L1B)
					->setCellValue('AO'.$i, $row->P1B)
					->setCellValue('AP'.$i, $row->L1A)
					->setCellValue('AQ'.$i, $row->P1A)
					->setCellValue('AR'.$i, ($row->L1D+$row->L1C+$row->L1B+$row->L1A))
					->setCellValue('AS'.$i, ($row->P1D+$row->P1C+$row->P1B+$row->P1A));
				}
				$i++;
			}
			$this->excel->getActiveSheet()->setCellValue('A13', 'V');
			$this->excel->getActiveSheet()->setCellValue('B13', 'DINAS');
			if (in_array($row->category, array('5'))) {
				$this->excel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, $a)
				->setCellValue('B'.$j, $row->nama)
				->setCellValue('C'.$j, $row->total_pegawai)
				->setCellValue('D'.$j, $row->L)
				->setCellValue('E'.$j, $row->P)
				->setCellValue('F'.$j, $row->L4D)
				->setCellValue('G'.$j, $row->P4D)
				->setCellValue('H'.$j, $row->L4C)
				->setCellValue('I'.$j, $row->P4C)
				->setCellValue('J'.$j, $row->L4B)
				->setCellValue('K'.$j, $row->P4B)
				->setCellValue('L'.$j, $row->L4A)
				->setCellValue('M'.$j, $row->P4A)
				->setCellValue('N'.$j, ($row->L4D+$row->L4C+$row->L4B+$row->L4A))
				->setCellValue('O'.$j, ($row->P4D+$row->P4C+$row->P4B+$row->P4A))
				->setCellValue('P'.$j, $row->L3D)
				->setCellValue('Q'.$j, $row->P3D)
				->setCellValue('R'.$j, $row->L3C)
				->setCellValue('S'.$j, $row->P3C)
				->setCellValue('T'.$j, $row->L3B)
				->setCellValue('U'.$j, $row->P3B)
				->setCellValue('V'.$j, $row->L3A)
				->setCellValue('W'.$j, $row->P3A)
				->setCellValue('X'.$j, ($row->L3D+$row->L3C+$row->L3B+$row->L3A))
				->setCellValue('Y'.$j, ($row->P3D+$row->P3C+$row->P3B+$row->P3A))
				->setCellValue('Z'.$j, $row->L2D)
				->setCellValue('AA'.$j, $row->P2D)
				->setCellValue('AB'.$j, $row->L2C)
				->setCellValue('AC'.$j, $row->P2C)
				->setCellValue('AD'.$j, $row->L2B)
				->setCellValue('AE'.$j, $row->P2B)
				->setCellValue('AF'.$j, $row->L2A)
				->setCellValue('AG'.$j, $row->P2A)
				->setCellValue('AH'.$j, ($row->L2D+$row->L2C+$row->L2B+$row->L2A))
				->setCellValue('AI'.$j, ($row->P2D+$row->P2C+$row->P2B+$row->P2A))
				->setCellValue('AJ'.$j, $row->L1D)
				->setCellValue('AK'.$j, $row->P1D)
				->setCellValue('AL'.$j, $row->L1C)
				->setCellValue('AM'.$j, $row->P1C)
				->setCellValue('AN'.$j, $row->L1B)
				->setCellValue('AO'.$j, $row->P1B)
				->setCellValue('AP'.$j, $row->L1A)
				->setCellValue('AQ'.$j, $row->P1A)
				->setCellValue('AR'.$j, ($row->L1D+$row->L1C+$row->L1B+$row->L1A))
				->setCellValue('AS'.$j, ($row->P1D+$row->P1C+$row->P1B+$row->P1A));
				$a++;
				$j++;
			}
			$this->excel->getActiveSheet()->setCellValue('A34', 'VI');
			$this->excel->getActiveSheet()->setCellValue('B34', 'BADAN');
			if (in_array($row->category, array('6'))) {
				$this->excel->setActiveSheetIndex(0)
				->setCellValue('A'.$k, $b)
				->setCellValue('B'.$k, $row->nama)
				->setCellValue('C'.$k, $row->total_pegawai)
				->setCellValue('D'.$k, $row->L)
				->setCellValue('E'.$k, $row->P)
				->setCellValue('F'.$k, $row->L4D)
				->setCellValue('G'.$k, $row->P4D)
				->setCellValue('H'.$k, $row->L4C)
				->setCellValue('I'.$k, $row->P4C)
				->setCellValue('J'.$k, $row->L4B)
				->setCellValue('K'.$k, $row->P4B)
				->setCellValue('L'.$k, $row->L4A)
				->setCellValue('M'.$k, $row->P4A)
				->setCellValue('N'.$k, ($row->L4D+$row->L4C+$row->L4B+$row->L4A))
				->setCellValue('O'.$k, ($row->P4D+$row->P4C+$row->P4B+$row->P4A))
				->setCellValue('P'.$k, $row->L3D)
				->setCellValue('Q'.$k, $row->P3D)
				->setCellValue('R'.$k, $row->L3C)
				->setCellValue('S'.$k, $row->P3C)
				->setCellValue('T'.$k, $row->L3B)
				->setCellValue('U'.$k, $row->P3B)
				->setCellValue('V'.$k, $row->L3A)
				->setCellValue('W'.$k, $row->P3A)
				->setCellValue('X'.$k, ($row->L3D+$row->L3C+$row->L3B+$row->L3A))
				->setCellValue('Y'.$k, ($row->P3D+$row->P3C+$row->P3B+$row->P3A))
				->setCellValue('Z'.$k, $row->L2D)
				->setCellValue('AA'.$k, $row->P2D)
				->setCellValue('AB'.$k, $row->L2C)
				->setCellValue('AC'.$k, $row->P2C)
				->setCellValue('AD'.$k, $row->L2B)
				->setCellValue('AE'.$k, $row->P2B)
				->setCellValue('AF'.$k, $row->L2A)
				->setCellValue('AG'.$k, $row->P2A)
				->setCellValue('AH'.$k, ($row->L2D+$row->L2C+$row->L2B+$row->L2A))
				->setCellValue('AI'.$k, ($row->P2D+$row->P2C+$row->P2B+$row->P2A))
				->setCellValue('AJ'.$k, $row->L1D)
				->setCellValue('AK'.$k, $row->P1D)
				->setCellValue('AL'.$k, $row->L1C)
				->setCellValue('AM'.$k, $row->P1C)
				->setCellValue('AN'.$k, $row->L1B)
				->setCellValue('AO'.$k, $row->P1B)
				->setCellValue('AP'.$k, $row->L1A)
				->setCellValue('AQ'.$k, $row->P1A)
				->setCellValue('AR'.$k, ($row->L1D+$row->L1C+$row->L1B+$row->L1A))
				->setCellValue('AS'.$k, ($row->P1D+$row->P1C+$row->P1B+$row->P1A));
				$b++;
				$k++;
			}
			if (in_array($row->category, array('7','8','9','10','11','12','13','14','15','16','17','18','19','20','21'))) {
				if(count($row->category) == '1') {
					$this->excel->setActiveSheetIndex(0)
					->setCellValue('A'.$l, str_romawi_format($row->category))
					->setCellValue('B'.$l, $row->nama)
					->setCellValue('C'.$l, $row->total_pegawai)
					->setCellValue('D'.$l, $row->L)
					->setCellValue('E'.$l, $row->P)
					->setCellValue('F'.$l, $row->L4D)
					->setCellValue('G'.$l, $row->P4D)
					->setCellValue('H'.$l, $row->L4C)
					->setCellValue('I'.$l, $row->P4C)
					->setCellValue('J'.$l, $row->L4B)
					->setCellValue('K'.$l, $row->P4B)
					->setCellValue('L'.$l, $row->L4A)
					->setCellValue('M'.$l, $row->P4A)
					->setCellValue('N'.$l, ($row->L4D+$row->L4C+$row->L4B+$row->L4A))
					->setCellValue('O'.$l, ($row->P4D+$row->P4C+$row->P4B+$row->P4A))
					->setCellValue('P'.$l, $row->L3D)
					->setCellValue('Q'.$l, $row->P3D)
					->setCellValue('R'.$l, $row->L3C)
					->setCellValue('S'.$l, $row->P3C)
					->setCellValue('T'.$l, $row->L3B)
					->setCellValue('U'.$l, $row->P3B)
					->setCellValue('V'.$l, $row->L3A)
					->setCellValue('W'.$l, $row->P3A)
					->setCellValue('X'.$l, ($row->L3D+$row->L3C+$row->L3B+$row->L3A))
					->setCellValue('Y'.$l, ($row->P3D+$row->P3C+$row->P3B+$row->P3A))
					->setCellValue('Z'.$l, $row->L2D)
					->setCellValue('AA'.$l, $row->P2D)
					->setCellValue('AB'.$l, $row->L2C)
					->setCellValue('AC'.$l, $row->P2C)
					->setCellValue('AD'.$l, $row->L2B)
					->setCellValue('AE'.$l, $row->P2B)
					->setCellValue('AF'.$l, $row->L2A)
					->setCellValue('AG'.$l, $row->P2A)
					->setCellValue('AH'.$l, ($row->L2D+$row->L2C+$row->L2B+$row->L2A))
					->setCellValue('AI'.$l, ($row->P2D+$row->P2C+$row->P2B+$row->P2A))
					->setCellValue('AJ'.$l, $row->L1D)
					->setCellValue('AK'.$l, $row->P1D)
					->setCellValue('AL'.$l, $row->L1C)
					->setCellValue('AM'.$l, $row->P1C)
					->setCellValue('AN'.$l, $row->L1B)
					->setCellValue('AO'.$l, $row->P1B)
					->setCellValue('AP'.$l, $row->L1A)
					->setCellValue('AQ'.$l, $row->P1A)
					->setCellValue('AR'.$l, ($row->L1D+$row->L1C+$row->L1B+$row->L1A))
					->setCellValue('AS'.$l, ($row->P1D+$row->P1C+$row->P1B+$row->P1A));
				}
				$l++;
			}
		}
		$m = ($l-1);
		$this->excel->getActiveSheet()->getStyle('A'.$l)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A'.$l)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A'.$l.':AS'.$l)->getFont()->setBold(true);
		$this->excel->getActiveSheet()->mergeCells('A'.$l.':B'.$l);
		$this->excel->getActiveSheet()->setCellValue('A'.$l, 'JUMLAH');
		$this->excel->setActiveSheetIndex(0)
			->setCellValue('C'.$l, '=SUM(C9:C'.$m.')')
			->setCellValue('D'.$l, '=SUM(D9:D'.$m.')')
			->setCellValue('E'.$l, '=SUM(E9:E'.$m.')')
			->setCellValue('F'.$l, '=SUM(F9:F'.$m.')')
			->setCellValue('G'.$l, '=SUM(G9:G'.$m.')')
			->setCellValue('H'.$l, '=SUM(H9:H'.$m.')')
			->setCellValue('I'.$l, '=SUM(I9:I'.$m.')')
			->setCellValue('J'.$l, '=SUM(J9:J'.$m.')')
			->setCellValue('K'.$l, '=SUM(K9:K'.$m.')')
			->setCellValue('L'.$l, '=SUM(L9:L'.$m.')')
			->setCellValue('M'.$l, '=SUM(M9:M'.$m.')')
			->setCellValue('N'.$l, '=SUM(N9:N'.$m.')')
			->setCellValue('O'.$l, '=SUM(O9:O'.$m.')')
			->setCellValue('P'.$l, '=SUM(P9:P'.$m.')')
			->setCellValue('Q'.$l, '=SUM(Q9:Q'.$m.')')
			->setCellValue('R'.$l, '=SUM(R9:R'.$m.')')
			->setCellValue('S'.$l, '=SUM(S9:S'.$m.')')
			->setCellValue('T'.$l, '=SUM(T9:T'.$m.')')
			->setCellValue('U'.$l, '=SUM(U9:U'.$m.')')
			->setCellValue('V'.$l, '=SUM(V9:V'.$m.')')
			->setCellValue('W'.$l, '=SUM(W9:W'.$m.')')
			->setCellValue('X'.$l, '=SUM(X9:X'.$m.')')
			->setCellValue('Y'.$l, '=SUM(Y9:Y'.$m.')')
			->setCellValue('Z'.$l, '=SUM(Z9:Z'.$m.')')
			->setCellValue('AA'.$l, '=SUM(AA9:AA'.$m.')')
			->setCellValue('AB'.$l, '=SUM(AB9:AB'.$m.')')
			->setCellValue('AC'.$l, '=SUM(AC9:AC'.$m.')')
			->setCellValue('AD'.$l, '=SUM(AD9:AD'.$m.')')
			->setCellValue('AE'.$l, '=SUM(AE9:AE'.$m.')')
			->setCellValue('AF'.$l, '=SUM(AF9:AF'.$m.')')
			->setCellValue('AG'.$l, '=SUM(AG9:AG'.$m.')')
			->setCellValue('AH'.$l, '=SUM(AH9:AH'.$m.')')
			->setCellValue('AI'.$l, '=SUM(AI9:AI'.$m.')')
			->setCellValue('AJ'.$l, '=SUM(AJ9:AJ'.$m.')')
			->setCellValue('AK'.$l, '=SUM(AK9:AK'.$m.')')
			->setCellValue('AL'.$l, '=SUM(AL9:AL'.$m.')')
			->setCellValue('AM'.$l, '=SUM(AM9:AM'.$m.')')
			->setCellValue('AN'.$l, '=SUM(AN9:AN'.$m.')')
			->setCellValue('AO'.$l, '=SUM(AO9:AO'.$m.')')
			->setCellValue('AP'.$l, '=SUM(AP9:AP'.$m.')')
			->setCellValue('AQ'.$l, '=SUM(AQ9:AQ'.$m.')')
			->setCellValue('AR'.$l, '=SUM(AR9:AR'.$m.')')
			->setCellValue('AS'.$l, '=SUM(AS9:AS'.$m.')');

		ob_end_clean();
		$filename='Rekapitulasi.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');
        exit;
	}

	public function exportPdf()
	{
		$this->data['datas'] = $this->golongan_model->record_count_per_skpd();
		$this->load->view('bezetting/golongan/cetak', $this->data);
		
		$sumber = $this->load->view('bezetting/golongan/cetak', $this->data, TRUE);
        $html = $sumber;


        $pdfFilePath = "Cetak_golongan.pdf";

        $pdf = $this->m_pdf->load();

        $pdf->AddPage('L');
		$pdf=new mPDF('','A4-L');
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);
        
        $pdf->Output($pdfFilePath, "D");
        exit();
		
	}
}