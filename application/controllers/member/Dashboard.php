<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ion_auth','form_validation','m_pdf'));
        $this->load->helper(array('form', 'url'));

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('member/dashboard_model');
        $this->load->model ('setup/pegawai_model' );
        $this->load->model ('setup/riwayat_pendidikan_model' );
        $this->load->model ('setup/riwayat_kepangkatan_model' );
        $this->load->model ('setup/riwayat_jabatan_model' );
        $this->load->model ('setup/riwayat_diklat_model' );
        $this->load->model ('setup/riwayat_penghargaan_model' );
        $this->load->model ('setup/riwayat_hukuman_model' );
        $this->load->model ('setup/Riwayat_suami_istri_model' );
        $this->load->model ('setup/Riwayat_anak_model' );
        $this->load->model ('report/report_skpd_model' );
    }


	public function index()
	{

        if ( !$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
    		$this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();
        
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            
            /* Data */
            $query = $this->dashboard_model->get_data_pegawai($this->session->userdata('user_name'));
        	
    		/* Load Template */
            foreach ($query as $row){
                $this->template->member_render('member/dashboard/index',$this->inputSetting($row));
            }
        }
	}

    public function validationData(){
        
        $this->form_validation->set_rules('picture','lang:pegawai_picture','max_length[50]');
        $this->form_validation->set_rules('nip','lang:pegawai_nip','max_length[50]');
        $this->form_validation->set_rules('nip_lama', 'lang:pegawai_nip_lama','max_length[50]');
        $this->form_validation->set_rules('nama', 'lang:pegawai_nama','max_length[100]');
        $this->form_validation->set_rules('gelar_depan', 'lang:gelar_depan', 'max_length[25]');
        $this->form_validation->set_rules('gelar_belakang', 'lang:gelar_belakang', 'max_length[25]');
        $this->form_validation->set_rules('tempat_lahir', 'lang:pegawai_tempat_lahir', 'max_length[100]');
        $this->form_validation->set_rules('telp', 'lang:pegawai_telp', 'max_length[50]');
        $this->form_validation->set_rules('kelamin', 'lang:pegawai_kelamin', 'max_length[50]');
        $this->form_validation->set_rules('agama', 'lang:pegawai_agama', 'max_length[50]');
        $this->form_validation->set_rules('alamat', 'lang:pegawai_alamat', 'max_length[2000]');
        $this->form_validation->set_rules('kode_pos', 'lang:pegawai_kode_pos', 'max_length[11]');
        $this->form_validation->set_rules('gol_darah', 'lang:pegawai_gol_darah', 'max_length[50]');
        $this->form_validation->set_rules('status', 'lang:pegawai_status', 'max_length[50]');
        $this->form_validation->set_rules('status_pegawai', 'lang:pegawai_status_pegawai', 'max_length[100]');
        $this->form_validation->set_rules('no_kartu_pegawai', 'lang:pegawai_no_kartu_pegawai', 'max_length[50]');
        $this->form_validation->set_rules('no_askes', 'lang:pegawai_no_askes', 'max_length[50]');
        $this->form_validation->set_rules('no_ktp', 'lang:no_ktp', 'max_length[50]');
        $this->form_validation->set_rules('npwp', 'lang:pegawai_npwp', 'max_length[50]');
        $this->form_validation->set_rules('nama_skpd', 'lang:nama_skpd', 'max_length[50]');
        $this->form_validation->set_rules('old', 'lang:change_password_validation_old_password_label', 'required');
        $this->form_validation->set_rules('new', 'lang:change_password_validation_new_password_label', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', 'change_password_validation_new_password_confirm_label', 'required');
        
        return $this->form_validation->run();
    }

    public function inputSetting($data){
        $id = $this->session->userdata('user_id');
        
        $this->data['status_edit'] = $this->ion_auth->get_status_edit($id);

        $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        $this->data['min_password_length'] = 5;
        $this->data['old_password'] = array(
                'name' => 'old',
                'id'   => 'old',
                'type' => 'password',
                'class' => 'form-control',
                'autocomplete' => 'off',
        );
        $this->data['new_password'] = array(
                'name'    => 'new',
                'id'      => 'new',
                'type'    => 'password',
                'class' => 'form-control',
                'autocomplete' => 'new-password',
        );
        $this->data['new_password_confirm'] = array(
                'name'    => 'new_confirm',
                'id'      => 'new_confirm',
                'type'    => 'password',
                'class' => 'form-control',
                'autocomplete' => 'new-password',
        );

        if($this->data['status_edit'] == 1) {
            $this->data['picture'] = array(
                    'name'  => 'userfile',
                    'id'    => 'userfile',
                    'type'  => 'file',
                    'class' => 'form-control',
                    'placeholder'=>'userfile',
                    'value' => $data['picture'],
            );
            $this->data['nip'] = array(
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly' => 'readonly',
                    'placeholder'=>'nomor induk pegawai',
                    'value' => $data['nip'],
            );
            $this->data['nip_lama'] = array(
                    'name'  => 'nip_lama',
                    'id'    => 'nip_lama',
                    'type'  => 'number',
                    'onkeypress'=>'return isNumeric(event)',
                    'oninput'=>'maxLengthCheck(this)',
                    'minlength'  => '9',
                    'maxlength'  => '9',
                    'min'=>'000000001',
                    'class' => 'form-control',
                    'placeholder'=>'nomor induk pegawai lama ',
                    'value' => $data['nip_lama'],
            );
            $this->data['nama'] = array(
                    'name'  => 'nama',
                    'id'    => 'nama',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'required'=> 'required',
                    'placeholder'=>'nama pegawai',
                    'value' => $data['nama'],
            );
            $this->data['gelar_depan'] = array(
                    'name'  => 'gelar_depan',
                    'id'    => 'gelar_depan',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'gelar di depan',
                    'value' => $data['gelar_depan'],
            );
            $this->data['gelar_belakang'] = array(
                    'name'  => 'gelar_belakang',
                    'id'    => 'gelar_belakang',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'gelar di belakang',
                    'value' => $data['gelar_belakang'],
            );
            $this->data['tempat_lahir'] = array(
                    'name'  => 'tempat_lahir',
                    'id'    => 'tempat_lahir',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'required'=> 'required',
                    'placeholder'=>'Tempat kelahiran sesuai ktp',
                    'value' => $data['tempat_lahir'],
            );
            if($data['tgl_lahir']){
                $data['tgl_lahir2'] = date("d-m-Y",strtotime($data['tgl_lahir']));
            }else{
                
            }
            $this->data['tgl_lahir'] = array(
                    'name'  => 'tgl_lahir',
                    'id'    => 'datepicker',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'required'=> 'required',
                    'placeholder'=>'tgl-bln-thn',
                    'value' => $data['tgl_lahir2'],
            );
            $this->data['telp'] = array(
                    'name'  => 'telp',
                    'id'    => 'telp',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'nomor yang bisa di hubungi',
                    'value' => $data['telp'],
            );
            $this->data['kelamin'] = array(
                    'name'  => 'kelamin',
                    'id'    => 'kelamin',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'required'=> 'required',
                    'value' => $data['kelamin'],
            );
            $this->data['agama'] = array(
                    'name'  => 'agama',
                    'id'    => 'agama',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'required'=> 'required',
                    'value' => $data['agama'],
            );
            $this->data['alamat'] = array(
                    'name'  => 'alamat',
                    'id'    => 'alamat',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'required'=> 'required',
                    'placeholder'=>'Alamat sesuai dengan ktp',
                    'value' => $data['alamat'],
            );
            $this->data['kode_pos'] = array(
                    'name'  => 'kode_pos',
                    'id'    => 'kode_pos',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'required'=> 'required',
                    'placeholder'=>'kode pos sesuai dengan ktp',
                    'value' => $data['kode_pos'],
            );
            $this->data['gol_darah'] = array(
                    'name'  => 'gol_darah',
                    'id'    => 'gol_darah',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'value' => $data['gol_darah'],
            );
            $this->data['status'] = array(
                    'name'  => 'status',
                    'id'    => 'status',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'value' => $data['status'],
            );
            $this->data['status_pegawai'] = array(
                    'name'  => 'status_pegawai',
                    'id'    => 'status_pegawai',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'value' => $data['status_pegawai'],
            );
            $this->data['no_kartu_pegawai'] = array(
                    'name'  => 'no_kartu_pegawai',
                    'id'    => 'no_kartu_pegawai',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'nomor kartu pegawai',
                    'value' => $data['no_kartu_pegawai'],
            );
            $this->data['no_askes'] = array(
                    'name'  => 'no_askes',
                    'id'    => 'no_askes',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'Nomor BPJS / Akses',
                    'value' => $data['no_askes'],
            );
            $this->data['no_kartu_keluarga'] = array(
                    'name'  => 'no_kartu_keluarga',
                    'id'    => 'no_kartu_keluarga',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'No Karsu/Karis',
                    'value' => $data['no_kartu_keluarga'],
            );
            $this->data['no_ktp'] = array(
                    'name'  => 'no_ktp',
                    'id'    => 'no_ktp',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'Nomor KTP',
                    'value' => $data['no_ktp'],
            );
            $this->data['npwp'] = array(
                    'name'  => 'npwp',
                    'id'    => 'npwp',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'Nomor pokok wajib pajak',
                    'value' => $data['npwp'],
            );
            if($data['tmt_cpns']=='0000-00-00'){
                $data['tmt_cpns2'] = '';
            }else if ($data['tmt_cpns']){
                $data['tmt_cpns2'] = date("d-m-Y",strtotime($data['tmt_cpns']));    
            }else{
                $data['tmt_cpns2']='';
            }
            $this->data['tmt_cpns'] = array(
                    'name'  => 'tmt_cpns',
                    'id'    => 'datepicker1',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'tgl-bln-thn',
                    'value' => $data['tmt_cpns2'],
            );
            if($data['tmt_pns']=='0000-00-00'){
                $data['tmt_pns2'] = '';
            }else if ($data['tmt_pns']){
                $data['tmt_pns2'] = date("d-m-Y",strtotime($data['tmt_pns']));
            }else{
                $data['tmt_pns2'] = '';
            }
            $this->data['tmt_pns'] = array(
                    'name'  => 'tmt_pns',
                    'id'    => 'datepicker2',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'placeholder'=>'tgl-bln-thn',
                    'value' => $data['tmt_pns2'],
            );

            $this->data['skpd'] = array(
                    'name'  => 'skpd',
                    'id'    => 'skpd',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly'=> 'readonly',
                    'placeholder'=>'OPD',
                    'value' => $data['skpd'],
            );

            $this->data['unitorganisasi'] = array(
                    'name'  => 'unitorganisasi',
                    'id'    => 'unitorganisasi',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly'=> 'readonly',
                    'placeholder'=>'Unit Organisasi',
                    'value' => $data['unitorganisasi'],
            );

            $this->data['unitkerja'] = array(
                    'name'  => 'unitkerja',
                    'id'    => 'unitkerja',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly'=> 'readonly',
                    'placeholder'=>'Unit Kerja',
                    'value' => @$data['unitkerja'],
            );

            $this->data['subunitkerja'] = array(
                    'name'  => 'subunitkerja',
                    'id'    => 'subunitkerja',
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly'=> 'readonly',
                    'placeholder'=>'Sub Unit Kerja',
                    'value' => @$data['subunitkerja'],
            );
        } else {
            $this->data['picture'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'userfile',
                    'value' => $data['picture'],
            );
            $this->data['nip'] = array(
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly' => 'readonly',
                    'placeholder'=>'nomor induk pegawai',
                    'value' => $data['nip'],
            );
            $this->data['nip_lama'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'nomor induk pegawai lama ',
                    'value' => $data['nip_lama'],
            );
            $this->data['nama'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'nama pegawai',
                    'value' => $data['nama'],
            );
            $this->data['gelar_depan'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'gelar di depan',
                    'value' => $data['gelar_depan'],
            );
            $this->data['gelar_belakang'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'gelar di belakang',
                    'value' => $data['gelar_belakang'],
            );
            $this->data['tempat_lahir'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'Tempat kelahiran sesuai ktp',
                    'value' => $data['tempat_lahir'],
            );
            if($data['tgl_lahir']){
                $data['tgl_lahir2'] = date("d-m-Y",strtotime($data['tgl_lahir']));
            }else{
                
            }
            $this->data['tgl_lahir'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'tgl-bln-thn',
                    'value' => $data['tgl_lahir2'],
            );
            $this->data['telp'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'nomor yang bisa di hubungi',
                    'value' => $data['telp'],
            );
            $this->data['kelamin'] = array(
                    'type'  => 'text',
                    'disabled' => 'disabled',
                    'class' => 'form-control',
                    'required'=> 'required',
                    'value' => $data['kelamin'],
            );
            $this->data['agama'] = array(
                    'type'  => 'text',
                    'disabled' => 'disabled',
                    'class' => 'form-control',
                    'required'=> 'required',
                    'value' => $data['agama'],
            );
            $this->data['alamat'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'Alamat sesuai dengan ktp',
                    'value' => $data['alamat'],
            );
            $this->data['kode_pos'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'kode pos sesuai dengan ktp',
                    'value' => $data['kode_pos'],
            );
            $this->data['gol_darah'] = array(
                    'type'  => 'text',
                    'disabled' => 'disabled',
                    'class' => 'form-control',
                    'value' => $data['gol_darah'],
            );
            $this->data['status'] = array(
                    'type'  => 'text',
                    'disabled' => 'disabled',
                    'class' => 'form-control',
                    'value' => $data['status'],
            );
            $this->data['status_pegawai'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'value' => $data['status_pegawai'],
            );
            $this->data['no_kartu_pegawai'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'nomor kartu pegawai',
                    'value' => $data['no_kartu_pegawai'],
            );
            $this->data['no_askes'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'Nomor BPJS / Akses',
                    'value' => $data['no_askes'],
            );
            $this->data['no_kartu_keluarga'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'No Karsu/Karis',
                    'value' => $data['no_kartu_keluarga'],
            );
            $this->data['no_ktp'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'Nomor KTP',
                    'value' => $data['no_ktp'],
            );
            $this->data['npwp'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'Nomor pokok wajib pajak',
                    'value' => $data['npwp'],
            );
            if($data['tmt_cpns']=='0000-00-00'){
                $data['tmt_cpns2'] = '';
            }else if ($data['tmt_cpns']){
                $data['tmt_cpns2'] = date("d-m-Y",strtotime($data['tmt_cpns']));    
            }else{
                $data['tmt_cpns2']='';
            }
            $this->data['tmt_cpns'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'tgl-bln-thn',
                    'value' => $data['tmt_cpns2'],
            );
            if($data['tmt_pns']=='0000-00-00'){
                $data['tmt_pns2'] = '';
            }else if ($data['tmt_pns']){
                $data['tmt_pns2'] = date("d-m-Y",strtotime($data['tmt_pns']));
            }else{
                $data['tmt_pns2'] = '';
            }
            $this->data['tmt_pns'] = array(
                    'type'  => 'text',
                    'readonly' => 'readonly',
                    'class' => 'form-control',
                    'placeholder'=>'tgl-bln-thn',
                    'value' => $data['tmt_pns2'],
            );

            $this->data['skpd'] = array(
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly'=> 'readonly',
                    'placeholder'=>'OPD',
                    'value' => $data['skpd'],
            );

            $this->data['unitorganisasi'] = array(
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly'=> 'readonly',
                    'placeholder'=>'Unit Organisasi',
                    'value' => $data['unitorganisasi'],
            );

            $this->data['unitkerja'] = array(
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly'=> 'readonly',
                    'placeholder'=>'Unit Kerja',
                    'value' => $data['unitkerja'],
            );

            $this->data['subunitkerja'] = array(
                    'type'  => 'text',
                    'class' => 'form-control',
                    'readonly'=> 'readonly',
                    'placeholder'=>'Sub Unit Kerja',
                    'value' => $data['subunitkerja'],
            );
        }
        
        $this->data['get_kelamin'] = $data['kelamin'];
        $this->data['get_agama'] = $data['agama'];
        $this->data['get_gol_darah'] = $data['gol_darah'];
        $this->data['get_status'] = $data['status'];
        $this->data['gambar'] = $data['picture'];
        $this->data['jk'] = $data['jk'];
        $this->data['name'] = $data['nama'];
        $this->data['nip_txt'] = $data['nip'];
        $this->data['prev_nama'] = $data['gelar_depan'];
        $this->data['next_nama'] = $data['gelar_belakang'];
        $this->data['place_birth'] = $data['tempat_lahir'];
        $this->data['date_birth'] = $data['tgl_lahir'];
        $this->data['skpd_name'] = $data['skpd'];
        $this->data['jabatan_txt'] = $data['nama_jabatan'];
        $this->data['gol_txt'] = $data['golongan']; 

        return $this->data;
    }

    public function modify() {
        if($this->input->post('submit')){
            // if($this->validationData()==TRUE){
                $filename = $this->input->post('userfile');
                $config['upload_path'] = "./files/foto_pegawai/";
                $config['allowed_types'] = "png|jpg|pdf";
                $config['overwrite']="true";
                $config['max_size']="20000000";
                $config['file_name'] = $this->input->post('nip');
                $this->load->library('upload', $config);
                

                if(!$this->upload->do_upload())
                {
                        $data = array(
                            'nip'=>$this->input->post('nip'),
                            'nip_lama'=>$this->input->post ('nip_lama'),
                            'nama'=>$this->input->post ('nama'),
                            'gelar_depan'=>$this->input->post ('gelar_depan'),
                            'gelar_belakang'=>$this->input->post ('gelar_belakang'),
                            'tempat_lahir'=>$this->input->post ('tempat_lahir'),
                            'tgl_lahir'=>$this->input->post ('tgl_lahir'),
                            'telp'=>$this->input->post ('telp'),
                            'kelamin'=>$this->input->post ('kelamin'),
                            'agama'=>$this->input->post ('agama'),
                            'alamat'=>$this->input->post ('alamat'),
                            'kode_pos'=>$this->input->post ('kode_pos'),
                            'gol_darah'=>$this->input->post ('gol_darah'),
                            'status'=>$this->input->post ('status'),
                            'status_pegawai'=>'Aktif',
                            'no_kartu_pegawai'=>$this->input->post ('no_kartu_pegawai'),
                            'no_askes'=>$this->input->post ('no_askes'),
                            'no_kartu_keluarga'=>$this->input->post ('no_kartu_keluarga'),
                            'no_ktp'=>$this->input->post ('no_ktp'),
                            'npwp'=>$this->input->post ('npwp'),
                            'tmt_cpns'=>$this->input->post ('tmt_cpns'),
                            'tmt_pns'=>$this->input->post ('tmt_pns')
                        );
                    $this->dashboard_model->updatenoimage($data);
                    redirect('member','refresh');
                
                }else{
                        $dat = $this->upload->data();
                        $data = array(
                            'picture'=>$dat['file_name'],
                            'nip'=>$this->input->post('nip'),
                            'nip_lama'=>$this->input->post ('nip_lama'),
                            'nama'=>$this->input->post ('nama'),
                            'gelar_depan'=>$this->input->post ('gelar_depan'),
                            'gelar_belakang'=>$this->input->post ('gelar_belakang'),
                            'tempat_lahir'=>$this->input->post ('tempat_lahir'),
                            'tgl_lahir'=>$this->input->post ('tgl_lahir'),
                            'telp'=>$this->input->post ('telp'),
                            'kelamin'=>$this->input->post ('kelamin'),
                            'agama'=>$this->input->post ('agama'),
                            'alamat'=>$this->input->post ('alamat'),
                            'kode_pos'=>$this->input->post ('kode_pos'),
                            'gol_darah'=>$this->input->post ('gol_darah'),
                            'status'=>$this->input->post ('status'),
                            'status_pegawai'=>'Aktif',
                            'no_kartu_pegawai'=>$this->input->post ('no_kartu_pegawai'),
                            'no_askes'=>$this->input->post ('no_askes'),
                            'no_kartu_keluarga'=>$this->input->post ('no_kartu_keluarga'),
                            'no_ktp'=>$this->input->post ('no_ktp'),
                            'npwp'=>$this->input->post ('npwp'),                            
                            'tmt_cpns'=>$this->input->post ('tmt_cpns'),
                            'tmt_pns'=>$this->input->post ('tmt_pns')
                        );
                    $this->dashboard_model->update($data);
                    redirect('member','refresh');
                
                }
            // } else {
            //     redirect('member','refresh');
            // } 
        } else {

            $query = $this->dashboard_model->get_data_pegawai($this->session->userdata('user_name'));
            
            /* Load Template */
            foreach ($query as $row){
                $this->template->member_render('member/dashboard/index',$this->inputSetting($row));
            }
        }
    }

    public function report_rh()
    {
        $nip = $this->session->userdata('user_name');
        $this->data['pegawai'] =  $this->pegawai_model->get($nip);
        $this->data['riwayat_pendidikan'] =  $this->riwayat_pendidikan_model->get($nip);
        $this->data['riwayat_kepangkatan'] =  $this->riwayat_kepangkatan_model->get($nip);
        $this->data['riwayat_jabatan'] =  $this->riwayat_jabatan_model->get($nip);
        $this->data['riwayat_diklat'] =  $this->riwayat_diklat_model->get($nip);
        $this->data['riwayat_penghargaan'] =  $this->riwayat_penghargaan_model->get($nip);
        $this->data['riwayat_hukuman'] =  $this->riwayat_hukuman_model->get($nip);
        $this->data['riwayat_suami_istri'] =  $this->Riwayat_suami_istri_model->get($nip);
        $this->data['riwayat_anak'] =  $this->Riwayat_anak_model->get($nip);
        $this->load->view('member/dashboard/report_rh2', $this->data);
        
        $sumber = $this->load->view('member/dashboard/report_rh', $this->data, TRUE);
        $html = $sumber;


        $pdfFilePath = "".$nip.".pdf";

        $pdf = $this->m_pdf->load();

        $pdf->AddPage('L');
        $pdf=new mPDF('','A4');
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);
        
        $pdf->Output($pdfFilePath, "D");
        exit(); 
        
    }

    function change_password() {
        $id = $this->session->userdata('user_id');

        if($this->input->post('submit'))
        {

            $old_password_matches = $this->ion_auth->hash_password_db($id, $this->input->post('old'));

            if ($old_password_matches === TRUE) {

                if($this->input->post('new_confirm') <> $this->input->post('new')) {
                    $this->session->set_flashdata('message', 'Konfirmasi Password Gagal');
                    redirect('member/dashboard/index', 'refresh');
                }

                if($this->validationData()==TRUE)
                {
                    $identity = $this->session->userdata('identity');

                    $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

                    if ($change)
                    {
                        //if the password was successfully changed
                        $this->session->set_flashdata('message', 'Password berhasil diubah');
                        $this->ion_auth->logout();
                        redirect('auth/login', 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        redirect('member/dashboard/index', 'refresh');
                    }
                }
            } else {
                $this->session->set_flashdata('message', 'Password Lama Anda salah.');
                redirect('member/dashboard/index', 'refresh');
            }
        }
    }
}
