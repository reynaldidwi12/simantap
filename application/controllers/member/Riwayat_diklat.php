<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_diklat extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_diklat'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/riwayat_diklat_model' );
		$this->load->model ('setup/pegawai_model');
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('nip','lang:riwayat_nip','max_length[50]');
		$this->form_validation->set_rules('no_diklat', 'lang:riwayat_no_diklat','max_length[50]');
		$this->form_validation->set_rules('nama_diklat', 'lang:riwayat_nama_diklat','max_length[100]');
		$this->form_validation->set_rules('tahun', 'lang:riwayat_tahun','max_length[50]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['nip'] = array(
				'type'  => 'text',
				'class' => 'form-control',
				'readonly'=> 'readonly',
				'placeholder'=>'nomor induk pegawai',
				'value' => $this->session->userdata('user_name'),
		);
		$this->data['nama'] = array(
				'name'  => 'nama',
				'id'    => 'nama',
				'type'  => 'text',
				'readonly'=>'readonly',
				'class' => 'form-control',
				'placeholder'=>'Nama Pegawai',
				'value' => $data['nama'],
		);
		$this->data['seq_no'] = array(
				'name'  => 'seq_no',
				'id'    => 'seq_no',
				'type'  => 'hidden',
				'class' => 'form-control',
				'placeholder'=>'sequence no',
				'value' => $data['seq_no'],
		);
		$this->data['no_diklat'] = array(
				'name'  => 'no_diklat',
				'id'    => 'no_diklat',
				'type'  => 'text',
				'required'  => 'required',
				'class' => 'form-control',
				'placeholder'=>'no diklat',
				'value' => $data['no_diklat'],
		);
		$this->data['nama_diklat'] = array(
				'name'  => 'nama_diklat',
				'id'    => 'nama_diklat',
				'type'  => 'text',
				'required'  => 'required',
				'class' => 'form-control',
				'placeholder'=>'nama diklat',
				'value' => $data['nama_diklat'],
		);
		$this->data['tahun'] = array(
				'name'  => 'tahun',
				'id'    => 'tahun',
				'type'  => 'number',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'tahun',
				'value' => $data['tahun'],
		);
		$this->data['get_nama_diklat'] = $data['nama_diklat'];
		return $this->data;
	}
	
	public function index() {
		$id = $this->session->userdata('user_id');
		
        $this->data['status_edit'] = $this->ion_auth->get_status_edit($id);
        
		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
		/* Get all users */
		
		$config = array ();
		$config ["base_url"] = base_url () . "member/riwayat_diklat/index";
		$config ["total_rows"] = $this->riwayat_diklat_model->record_count($this->session->userdata('user_name'));
		$config ["per_page"] = 50;
		$config ["uri_segment"] = 5;
		$choice = $config ["total_rows"] / $config ["per_page"];
		$config ["num_links"] = 5;
		
		// config css for pagination
		$config ['full_tag_open'] = '<ul class="pagination">';
		$config ['full_tag_close'] = '</ul>';
		$config ['first_link'] = 'First';
		$config ['last_link'] = 'Last';
		$config ['first_tag_open'] = '<li>';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="prev">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li>';
		$config ['next_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li>';
		$config ['last_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		
		if ($this->uri->segment ( 5 ) == "") {
			$data ['number'] = 0;
		} else {
			$data ['number'] = $this->uri->segment ( 5 );
		}
		
		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 5 )) ? $this->uri->segment ( 5 ) : 0;
		
		$this->data ['riwayat'] = $this->riwayat_diklat_model->fetchAll($config ["per_page"], $page,$this->session->userdata('user_name'));
		$this->data ['nip'] = $this->session->userdata('user_name');
		$this->data ['nama_pegawai'] = $this->pegawai_model->getName($this->session->userdata('user_name'));
		$this->data ['links'] = $this->pagination->create_links ();
		$this->template->member_render('member/riwayat_diklat/index', $this->data);
		
	}
	
	public function add(){
		$id = $this->session->userdata('user_id');
		
        $this->data['status_edit'] = $this->ion_auth->get_status_edit($id);
        
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'nip'=>$this->session->userdata('user_name'),
						'seq_no'=>$this->riwayat_diklat_model->generateSeqNo($this->session->userdata('user_name')),
						'no_diklat'=>$this->input->post ('no_diklat'),
						'nama_diklat'=>$this->input->post ('nama_diklat'),
						'tahun'=>$this->input->post ('tahun')
				);
				$this->riwayat_diklat_model->create($data);
				redirect('member/riwayat_diklat');
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('member/riwayat_diklat/add', 'refresh');
			}
			
		} else {
			$data = array(
					'nip'=>$this->session->userdata('user_name'),
					'nama'=>$this->pegawai_model->getName($this->session->userdata('user_name')),
					'seq_no'=>null,
					'no_diklat'=>null,
					'nama_diklat'=>null,
					'tahun'=>null
			);
			$this->template->member_render('member/riwayat_diklat/form',$this->inputSetting($data));
		}
	}
	
	public function modify($seq_no) {
		$id = $this->session->userdata('user_id');
		
        $this->data['status_edit'] = $this->ion_auth->get_status_edit($id);
        
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'nip'=>$this->session->userdata('user_name'),
					'seq_no'=>$this->input->post ('seq_no'),
					'no_diklat'=>$this->input->post ('no_diklat'),
					'nama_diklat'=>$this->input->post ('nama_diklat'),
					'get_nama_diklat'=>$this->input->post ('nama_diklat'),
					'tahun'=>$this->input->post ('tahun')
				);
			}
			$this->riwayat_diklat_model->update($data);
			redirect('member/riwayat_diklat','refresh');
		} else {
			$query = $this->riwayat_diklat_model->fetchById($this->session->userdata('user_name'),$seq_no);
			foreach ($query as $row){
				$this->template->member_render('member/riwayat_diklat/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($seq_no=null) {
		$this->riwayat_diklat_model->delete($this->session->userdata('user_name'),$seq_no);
		redirect ('member/riwayat_diklat','refresh');
	}
	

	
}