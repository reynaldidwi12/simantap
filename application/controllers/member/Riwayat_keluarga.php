<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_keluarga extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_jabatan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/riwayat_anak_model' );
		$this->load->model ('setup/riwayat_suami_istri_model' );
		$this->load->model ('setup/pegawai_model');
		
	}
	
	public function index() {
		$id = $this->session->userdata('user_id');
		
        $this->data['status_edit'] = $this->ion_auth->get_status_edit($id);
        
		
		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
		$this->pagination->initialize ( );
		$page = ($this->uri->segment ( 5 )) ? $this->uri->segment ( 5 ) : 0;

		$user = $this->ion_auth->user($id)->row();

        if($user->status_edit == '0') {
            $this->data['status_edit'] = 0;
        } elseif($user->status_edit == '1') {
            $this->data['status_edit'] = 1;
        } else {
            $this->data['status_edit'] = '';
        }
		
		$this->data ['anak'] = $this->riwayat_anak_model->fetchAll($this->session->userdata('user_name'));
		$this->data ['suami_istri'] = $this->riwayat_suami_istri_model->jml($this->session->userdata('user_name'));
		$this->data['jml'] = count($this->data ['suami_istri']);
		$this->data ['nip'] = $this->session->userdata('user_name');
		$this->data ['nama_pegawai'] = $this->pegawai_model->getName($this->session->userdata('user_name'));
		$this->template->member_render('member/riwayat_keluarga/index', $this->data);
		
	}
	
}