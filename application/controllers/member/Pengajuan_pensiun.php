<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * File manager
 *
 * Ini adalah fitur file manager dengan bantuan elFinder.
 * elFinder lib-nya berada di application/third_party/elFinder.
 *
 * @author greentech.ID <me@system112.org>
 */

class Pengajuan_pensiun extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		/* directory that will be used to store files */
		$this->config->set_item('files_directory', FCPATH . '.ELFINDER');

		/* Load Library */
		$this->load->library('session', 'ion_auth');

		/* Title Page :: Common */
		$this->page_title->push(lang('menu_pengajuan_pensiun'));
		$this->data['pagetitle'] = $this->page_title->show();

		$this->load->model ('member/pengajuan_pensiun_model' );
	}

	public function user($userNip = NULL)
	{
		$this->checkPermission($userNip);

		$this->data['userNip'] = $userNip;

		$year = date('Y');

		$pengajuan = $this->pengajuan_pensiun_model->get_data_by_row('pengajuan_tbl',$userNip,'nip');

		if(!empty($pengajuan)) {

			$tmpStatus = json_decode($pengajuan->status, true);

	  		$keys = array_keys($tmpStatus['data']);
	     	$last_key = array_pop($keys);

			if($pengajuan && $tmpStatus['data'][$last_key]['status'] != 'berkas dikembalikan') {
				$this->data['data'] = $pengajuan;
				$this->template->member_render('member/pengajuan_pensiun/status', $this->data);
			} else if($tmpStatus['data'][$last_key]['status'] == 'berkas dikembalikan') {
				$topDirectory = $this->config->item('files_directory');
				$pathToOpen = $topDirectory . '/' . $userNip;
				$pathPensiun = $pathToOpen . '/' . 'pensiun';
				$pathYear = $pathPensiun . '/' . $year;

				$this->data['userNip'] = $this->session->userdata('user_name');
		        $this->data['topDirectory'] = $this->config->item('files_directory');
				$this->data['pengajuan'] = $pengajuan;
				$this->data['data'] = $this->pengajuan_pensiun_model->get_data_by('daftar_pengajuan',$pengajuan->sub_kategori,'sub_kategori','no_urut');
				$this->template->member_render('member/pengajuan_pensiun/revision', $this->data);
			} 
		} else {
			$topDirectory = $this->config->item('files_directory');
			$pathToOpen = $topDirectory . '/' . $userNip;
			$pathPensiun = $pathToOpen . '/' . 'pensiun';
			$pathYear = $pathPensiun . '/' . $year;

			// if dir not exist, create it.
			if (!is_dir($pathToOpen)) {
				if ( ! mkdir($pathToOpen, 0777)) {
					return show_error('Internal server error. code: 32e', 500);
				}
				// create .htaccess that prevent direct directory access (security).
				copy($topDirectory . '/.htaccess', $pathToOpen . '/.htaccess');
			}

			if (!file_exists($pathPensiun)) {
			    mkdir($pathPensiun, 0777, true);
			    // create .htaccess that prevent direct directory access (security).
				copy($topDirectory . '/.htaccess', $pathPensiun . '/.htaccess');
			}

			if (!file_exists($pathYear)) {
			    mkdir($pathYear, 0777, true);
			    // create .htaccess that prevent direct directory access (security).
				copy($topDirectory . '/.htaccess', $pathYear . '/.htaccess');
			}

			$this->template->member_render('member/pengajuan_pensiun/index', $this->data);
		}

	}

	public function get_daftar_berkas($userNip){
		$this->checkPermission($userNip);

		$id = $this->input->post('id');
        $category_id = $this->input->post('id',TRUE);

        $this->data['data'] = $this->pengajuan_pensiun_model->get_data_by('daftar_pengajuan',$category_id,'sub_kategori','no_urut');
        $this->data['userNip'] = $userNip;
        $this->data['subKategori'] = $id;
        $this->data['topDirectory'] = $this->config->item('files_directory');

        echo $this->load->view('member/pengajuan_pensiun/daftar_berkas', $this->data, true);
    }

    function kirim($userNip){
		$this->checkPermission($userNip);

		$id = $this->session->userdata('user_id');
		$skpd = $this->session->userdata('ss_skpd');
		$username = $this->session->userdata('user_name');

		$arr = array( 
            '0' => array(
                'status' => 'belum divalidasi',
                'note' => 'Data telah dikirim dalam sistem dan siap untuk diperiksa kebenaran datanya oleh BKPSDM di bidang pensiun.',
                'date' => strtotime(date('Y-m-d H:i:s'))
            )
        );

        $result = array("data" => array_values($arr));
        
		if($this->input->post('submit')){
			$data = array(
				'nip'=>$this->input->post('nip'),
				'year'=>$this->input->post('year'),
				'kategori'=>'pensiun',
				'sub_kategori'=>$this->input->post('sub_kategori'),
				'kd_skpd'=>$skpd,
				'user_masuk'=>$username,
				'status'=>json_encode($result)
			);
			$this->pengajuan_pensiun_model->create($data);
			redirect('member/pengajuan_pensiun/user/'.$userNip);
		} else {
			redirect('member/pengajuan_pensiun/user/'.$userNip);
		}
    }

    function kirim_ulang($id, $userNip){
		$this->checkPermission($userNip);

        $pengajuan = $this->pengajuan_pensiun_model->getDataById($id);

        $tmpStatus = json_decode($pengajuan['status'], true);

		$arr = array( 
            'status' => 'berkas dikirim ulang',
            'note' => 'Berkas Anda telah dikirim ulang dan siap untuk diperiksa kebenaran datanya oleh BKPSDM di bidang pensiun',
            'date' => strtotime(date('Y-m-d H:i:s'))
        );
        array_push($tmpStatus['data'], $arr);
        
		if($this->input->post('submit')){
			$data = array(
	            'status' => json_encode($tmpStatus)
	        );

	        $result = $this->pengajuan_pensiun_model->updateRow('pengajuan_tbl', 'id', $id, $data);
			redirect('member/pengajuan_pensiun/user/'.$userNip);
		} else {
			redirect('member/pengajuan_pensiun/user/'.$userNip);
		}
    }

	/**
	 * Permission check
	 *
	 * Checking current logged in user who access file manager.
	 *
	 * Rules:
	 *   1. Must be logged in
	 *   2. Must be An Admin
	 *   3. Must pass user target NIP
	 */
	private function checkPermission($userNip = NULL)
	{
		$id = $this->session->userdata('user_id');
		
        $status_edit = $this->ion_auth->get_status_edit($id);

		if ( ! $this->ion_auth->logged_in() OR $this->session->userdata('user_name') <> $userNip OR $status_edit == 0 )
		{
			return show_error('Anda tidak diizinkan mengakses halaman ini.');
		}
	}
}
