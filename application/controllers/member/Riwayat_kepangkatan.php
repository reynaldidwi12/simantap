<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_kepangkatan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_pendidikan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/riwayat_kepangkatan_model' );
		$this->load->model ('setup/pegawai_model');
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('nip','lang:riwayat_nip','max_length[50]');
		$this->form_validation->set_rules('golongan', 'lang:riwayat_golongan','max_length[10]');
		$this->form_validation->set_rules('tmt_pangkat', 'lang:riwayat_tmt_pangkat','max_length[50]');
		$this->form_validation->set_rules('no_surat_kerja', 'lang:riwayat_no_surat_kerja','max_length[50]');
		$this->form_validation->set_rules('tgl_surat_kerja', 'lang:riwayat_tgl_surat_kerja','max_length[50]');
		$this->form_validation->set_rules('mk_bulan', 'lang:riwayat_mk_bulan','max_length[50]');
		$this->form_validation->set_rules('mk_tahun', 'lang:riwayat_mk_tahun','max_length[50]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
        
		$this->data['nip'] = array(
				'type'  => 'text',
				'class' => 'form-control',
				'readonly'=> 'readonly',
				'value' => $this->session->userdata('user_name'),
		);
		$this->data['nama'] = array(
				'name'  => 'nama',
				'id'    => 'nama',
				'type'  => 'text',
				'readonly'=>'readonly',
				'class' => 'form-control',
				'placeholder'=>'Nama Pegawai',
				'value' => $data['nama'],
		);
		$this->data['seq_no'] = array(
				'name'  => 'seq_no',
				'id'    => 'seq_no',
				'type'  => 'number',
				'class' => 'form-control',
				'readonly' => 'readonly',
				'placeholder'=>'sequence no',
				'value' => $data['seq_no'],
		);
		$this->data['golongan'] = array(
				'name'  => 'golongan',
				'id'    => 'golongan',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'golongan',
				'value' => $data['golongan'],
		);
		if($data['tmt_pangkat']){
			$data['tmt_pangkat2'] = date("d-m-Y",strtotime($data['tmt_pangkat']));
		}else{
			
		}
		$this->data['tmt_pangkat'] = array(
				'name'  => 'tmt_pangkat',
				'id'    => 'datepicker',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tmt_pangkat2'],
		);
		$this->data['no_surat_kerja'] = array(
				'name'  => 'no_surat_kerja',
				'id'    => 'no_surat_kerja',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'no surat kerja',
				'value' => $data['no_surat_kerja'],
		);
		if($data['tgl_surat_kerja']){
			$data['tgl_surat_kerja2'] = date("d-m-Y",strtotime($data['tgl_surat_kerja']));
		}else{
			
		}
		$this->data['tgl_surat_kerja'] = array(
				'name'  => 'tgl_surat_kerja',
				'id'    => 'datepicker1',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tgl_surat_kerja2'],
		);
		$this->data['mk_bulan'] = array(
				'name'  => 'mk_bulan',
				'id'    => 'mk_bulan',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'mk bulan',
				'value' => $data['mk_bulan'],
		);
		$this->data['mk_tahun'] = array(
				'name'  => 'mk_tahun',
				'id'    => 'mk_tahun',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'mk tahun',
				'value' => $data['mk_tahun'],
		);
		$this->data['get_golongan'] = $data['golongan'];
		return $this->data;
	}
	
	public function index() {
		$id = $this->session->userdata('user_id');
		
        $this->data['status_edit'] = $this->ion_auth->get_status_edit($id);
        
		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
		/* Get all users */
		
		$config = array ();
		$config ["base_url"] = base_url () . "member/riwayat_kepangkatan/index";
		$config ["total_rows"] = $this->riwayat_kepangkatan_model->record_count($this->session->userdata('user_name'));
		$config ["per_page"] = 50;
		$config ["uri_segment"] = 5;
		$choice = $config ["total_rows"] / $config ["per_page"];
		$config ["num_links"] = 5;
		
		// config css for pagination
		$config ['full_tag_open'] = '<ul class="pagination">';
		$config ['full_tag_close'] = '</ul>';
		$config ['first_link'] = 'First';
		$config ['last_link'] = 'Last';
		$config ['first_tag_open'] = '<li>';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="prev">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li>';
		$config ['next_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li>';
		$config ['last_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		
		if ($this->uri->segment ( 5 ) == "") {
			$data ['number'] = 0;
		} else {
			$data ['number'] = $this->uri->segment ( 5 );
		}
		
		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 5 )) ? $this->uri->segment ( 5 ) : 0;
		
		$this->data ['riwayat'] = $this->riwayat_kepangkatan_model->fetchAll($config ["per_page"], $page,$this->session->userdata('user_name'));
		$this->data ['nip'] = $this->session->userdata('user_name');
		$this->data ['nama_pegawai'] = $this->pegawai_model->getName($this->session->userdata('user_name'));
		$this->data ['links'] = $this->pagination->create_links ();
		$this->template->member_render('member/riwayat_kepangkatan/index', $this->data);
		
	}
	
	public function add(){
		$id = $this->session->userdata('user_id');
		
        $this->data['status_edit'] = $this->ion_auth->get_status_edit($id);
        
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'nip'=>$this->session->userdata('user_name'),
						'seq_no'=>$this->riwayat_kepangkatan_model->generateSeqNo($this->session->userdata('user_name')),
						'golongan'=>$this->input->post ('golongan'),
						'tmt_pangkat'=>$this->input->post ('tmt_pangkat'),
						'no_surat_kerja'=>$this->input->post ('no_surat_kerja'),
						'tgl_surat_kerja'=>$this->input->post ('tgl_surat_kerja'),
						'mk_bulan'=>$this->input->post ('mk_bulan'),
						'mk_tahun'=>$this->input->post ('mk_tahun')
				);
				$this->riwayat_kepangkatan_model->create($data);
				redirect('member/riwayat_kepangkatan','refresh');
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('member/riwayat_kepangkatan/add', 'refresh');
			}
			
		} else {
			$data = array(
					'nip'=>$this->session->userdata('user_name'),
					'nama'=>$this->pegawai_model->getName($this->session->userdata('user_name')),
					'seq_no'=>null,
					'golongan'=>null,
					'tmt_pangkat'=>null,
					'no_surat_kerja'=>null,
					'tgl_surat_kerja'=>null,
					'mk_bulan'=>null,
					'mk_tahun'=>null
			);
			$this->template->member_render('member/riwayat_kepangkatan/form',$this->inputSetting($data));
		}
	}
	
	public function modify($seq_no) {
		$id = $this->session->userdata('user_id');
		
        $this->data['status_edit'] = $this->ion_auth->get_status_edit($id);
        
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'nip'=>$this->session->userdata('user_name'),
					'seq_no'=>$seq_no,
					'golongan'=>$this->input->post ('golongan'),
					'tmt_pangkat'=>$this->input->post ('tmt_pangkat'),
					'no_surat_kerja'=>$this->input->post ('no_surat_kerja'),
					'tgl_surat_kerja'=>$this->input->post ('tgl_surat_kerja'),
					'mk_bulan'=>$this->input->post ('mk_bulan'),
					'mk_tahun'=>$this->input->post ('mk_tahun')
				);
			}
			$this->riwayat_kepangkatan_model->update($data);
			redirect('member/riwayat_kepangkatan','refresh');
		} else {
			$query = $this->riwayat_kepangkatan_model->fetchById($this->session->userdata('user_name'),$seq_no);
			foreach ($query as $row){
				$this->template->member_render('member/riwayat_kepangkatan/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($seq_no=null) {
		$this->riwayat_kepangkatan_model->delete($this->session->userdata('user_name'),$seq_no);
		redirect('member/riwayat_kepangkatan','refresh');
	}
	
}