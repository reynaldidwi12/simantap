<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class pegawai extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('m_pdf');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_pegawai'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/pegawai_model' );
		$this->load->model ('setup/riwayat_pendidikan_model' );
		$this->load->model ('setup/riwayat_kepangkatan_model' );
		$this->load->model ('setup/riwayat_jabatan_model' );
		$this->load->model ('setup/riwayat_diklat_model' );
		$this->load->model ('setup/riwayat_penghargaan_model' );
		$this->load->model ('setup/riwayat_hukuman_model' );
		$this->load->model ('report/report_skpd_model' );
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('picture','lang:pegawai_picture','max_length[50]');
		$this->form_validation->set_rules('nip','lang:pegawai_nip','max_length[50]');
		$this->form_validation->set_rules('nip_lama', 'lang:pegawai_nip_lama','max_length[50]');
		$this->form_validation->set_rules('nama', 'lang:pegawai_nama','max_length[100]');
		$this->form_validation->set_rules('gelar_depan', 'lang:gelar_depan', 'max_length[25]');
		$this->form_validation->set_rules('gelar_belakang', 'lang:gelar_belakang', 'max_length[25]');
		$this->form_validation->set_rules('tempat_lahir', 'lang:pegawai_tempat_lahir', 'max_length[100]');
		$this->form_validation->set_rules('telp', 'lang:pegawai_telp', 'max_length[50]');
		$this->form_validation->set_rules('kelamin', 'lang:pegawai_kelamin', 'max_length[50]');
		$this->form_validation->set_rules('agama', 'lang:pegawai_agama', 'max_length[50]');
		$this->form_validation->set_rules('alamat', 'lang:pegawai_alamat', 'max_length[2000]');
		$this->form_validation->set_rules('kode_pos', 'lang:pegawai_kode_pos', 'max_length[11]');
		$this->form_validation->set_rules('gol_darah', 'lang:pegawai_gol_darah', 'max_length[50]');
		$this->form_validation->set_rules('status', 'lang:pegawai_status', 'max_length[50]');
		$this->form_validation->set_rules('status_pegawai', 'lang:pegawai_status_pegawai', 'max_length[100]');
		$this->form_validation->set_rules('no_kartu_pegawai', 'lang:pegawai_no_kartu_pegawai', 'max_length[50]');
		$this->form_validation->set_rules('no_askes', 'lang:pegawai_no_askes', 'max_length[50]');
 		$this->form_validation->set_rules('no_ktp', 'lang:no_ktp', 'max_length[50]');
		$this->form_validation->set_rules('npwp', 'lang:pegawai_npwp', 'max_length[50]');
		$this->form_validation->set_rules('nama_skpd', 'lang:nama_skpd', 'max_length[50]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['picture'] = array(
				'name'  => 'userfile',
				'id'    => 'userfile',
				'type'  => 'file',
				'class' => 'form-control',
				'placeholder'=>'userfile',
				'value' => $data['picture'],
		);
		$this->data['nip'] = array(
				'name'  => 'nip',
				'id'    => 'nip',
				'type'  => 'text',
				'minlength'=>'18',
				'maxlength'=>'18',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'nomor induk pegawai',
				'value' => $data['nip'],
		);
		$this->data['nip_lama'] = array(
				'name'  => 'nip_lama',
				'id'    => 'nip_lama',
				'type'  => 'text',
				'minlength'  => '9',
				'maxlength'  => '9',
				'class' => 'form-control',
				'placeholder'=>'nomor induk pegawai lama ',
				'value' => $data['nip_lama'],
		);
		$this->data['nama'] = array(
				'name'  => 'nama',
				'id'    => 'nama',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama pegawai',
				'value' => $data['nama'],
		);
		$this->data['gelar_depan'] = array(
				'name'  => 'gelar_depan',
				'id'    => 'gelar_depan',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'gelar di depan',
				'value' => $data['gelar_depan'],
		);
		$this->data['gelar_belakang'] = array(
				'name'  => 'gelar_belakang',
				'id'    => 'gelar_belakang',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'gelar di belakang',
				'value' => $data['gelar_belakang'],
		);
		$this->data['tempat_lahir'] = array(
				'name'  => 'tempat_lahir',
				'id'    => 'tempat_lahir',
				'type'  => 'text',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'Tempat kelahiran sesuai ktp',
				'value' => $data['tempat_lahir'],
		);
		
		if($data['tgl_lahir']){
			$data['tgl_lahir2'] = date("d-m-Y",strtotime($data['tgl_lahir']));
		}else{
			
		}
		$this->data['tgl_lahir'] = array(
				'name'  => 'tgl_lahir',
				'id'    => 'datepicker',
				'type'  => 'text',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tgl_lahir2'],
		);
		$this->data['telp'] = array(
				'name'  => 'telp',
				'id'    => 'telp',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nomor yang bisa di hubungi',
				'value' => $data['telp'],
		);
		$this->data['kelamin'] = array(
				'name'  => 'kelamin',
				'id'    => 'kelamin',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $data['kelamin'],
		);
		$this->data['agama'] = array(
				'name'  => 'agama',
				'id'    => 'agama',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $data['agama'],
		);
		$this->data['alamat'] = array(
				'name'  => 'alamat',
				'id'    => 'alamat',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Alamat sesuai dengan ktp',
				'value' => $data['alamat'],
		);
		$this->data['kode_pos'] = array(
				'name'  => 'kode_pos',
				'id'    => 'kode_pos',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'kode pos sesuai dengan ktp',
				'value' => $data['kode_pos'],
		);
		$this->data['gol_darah'] = array(
				'name'  => 'gol_darah',
				'id'    => 'gol_darah',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $data['gol_darah'],
		);
		$this->data['status'] = array(
				'name'  => 'status',
				'id'    => 'status',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $data['status'],
		);
		$this->data['status_pegawai'] = array(
				'name'  => 'status_pegawai',
				'id'    => 'status_pegawai',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $data['status_pegawai'],
		);
		$this->data['no_kartu_pegawai'] = array(
				'name'  => 'no_kartu_pegawai',
				'id'    => 'no_kartu_pegawai',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nomor kartu pegawai',
				'value' => $data['no_kartu_pegawai'],
		);
		$this->data['no_askes'] = array(
				'name'  => 'no_askes',
				'id'    => 'no_askes',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Nomor BPJS / Akses',
				'value' => $data['no_askes'],
		);
		$this->data['no_ktp'] = array(
				'name'  => 'no_ktp',
				'id'    => 'no_ktp',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Nomor KTP',
				'value' => $data['no_ktp'],
		);
		$this->data['npwp'] = array(
				'name'  => 'npwp',
				'id'    => 'npwp',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Nomor pokok wajib pajak',
				'value' => $data['npwp'],
		);
		$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'type'  => 'text',
				'class' => 'form-control selectpicker',
				'placeholder'=>'Nama SKPD',
				'value' => $data['kd_skpd'],
		);
		$this->data['kd_unitkerja'] = array(
				'name'  => 'kd_unitkerja',
				'id'    => 'kd_unitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Nama Unit Kerja',
				'value' => $data['kd_unitkerja'],
		);
		
		$this->data['get_kelamin'] = $data['kelamin'];
		$this->data['get_agama'] = $data['agama'];
		$this->data['get_gol_darah'] = $data['gol_darah'];
		$this->data['get_status'] = $data['status'];
		$this->data['nama_skpd'] = $this->pegawai_model->get_skpd_all();
		$this->data['get_nama_skpd'] = $data['kd_skpd'];
		$this->data['nama_unitkerja'] = $this->pegawai_model->get_unitkerja_all();
		$this->data['get_nama_unitkerja'] = $data['kd_unitkerja'];
		$this->data['gambar'] = $data['picture'];

		return $this->data;
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "setup/pegawai/index";
			$config ["total_rows"] = $this->pegawai_model->record_count ();
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$data ['number'] = 0;
			} else {
				$data ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['pegawai'] = $this->pegawai_model->fetchAll($config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/pegawai/index', $this->data);
		}
	}
	
	public function add(){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				
				$filename = $this->input->post('picture');
				$config['upload_path'] = "./files/foto_pegawai/";
				$config['allowed_types'] = "png|jpg|pdf";
				$config['overwrite']="true";
				$config['max_size']="20000000";
				$config['file_name'] = $this->input->post('nip');
				$this->load->library('upload', $config);
				

				if(!$this->upload->do_upload())
				{
						$data = array(
							'nip'=>$this->input->post('nip'),
							'nip_lama'=>$this->input->post ('nip_lama'),
							'nama'=>$this->input->post ('nama'),
							'gelar_depan'=>$this->input->post ('gelar_depan'),
							'gelar_belakang'=>$this->input->post ('gelar_belakang'),
							'tempat_lahir'=>$this->input->post ('tempat_lahir'),
							'tgl_lahir'=>$this->input->post ('tgl_lahir'),
							'telp'=>$this->input->post ('telp'),
							'kelamin'=>$this->input->post ('kelamin'),
							'agama'=>$this->input->post ('agama'),
							'alamat'=>$this->input->post ('alamat'),
							'kode_pos'=>$this->input->post ('kode_pos'),
							'gol_darah'=>$this->input->post ('gol_darah'),
							'status'=>$this->input->post ('status'),
							'status_pegawai'=>$this->input->post ('status_pegawai'),
							'no_kartu_pegawai'=>$this->input->post ('no_kartu_pegawai'),
							'no_askes'=>$this->input->post ('no_askes'),
							'no_ktp'=>$this->input->post ('no_ktp'),
							'npwp'=>$this->input->post ('npwp'),
							'nama_skpd'=>$this->input->post ('nama_skpd'),
							'kd_skpd'=>$this->input->post ('kd_skpd'),
							'nama_unit_kerja'=>$this->input->post ('nama_unit_kerja')
							
						);
					$this->pegawai_model->createnoimage($data);
					redirect('setup/pegawai/find/'.$this->input->post('nip').'/nip','refresh');
				
				}else{
								 //get data
						$dat = $this->upload->data();
					
						$data = array(
							'picture'=>$dat['file_name'],
							'nip'=>$this->input->post('nip'),
							'nip_lama'=>$this->input->post ('nip_lama'),
							'nama'=>$this->input->post ('nama'),
							'gelar_depan'=>$this->input->post ('gelar_depan'),
							'gelar_belakang'=>$this->input->post ('gelar_belakang'),
							'tempat_lahir'=>$this->input->post ('tempat_lahir'),
							'tgl_lahir'=>$this->input->post ('tgl_lahir'),
							'telp'=>$this->input->post ('telp'),
							'kelamin'=>$this->input->post ('kelamin'),
							'agama'=>$this->input->post ('agama'),
							'alamat'=>$this->input->post ('alamat'),
							'kode_pos'=>$this->input->post ('kode_pos'),
							'gol_darah'=>$this->input->post ('gol_darah'),
							'status'=>$this->input->post ('status'),
							'status_pegawai'=>$this->input->post ('status_pegawai'),
							'no_kartu_pegawai'=>$this->input->post ('no_kartu_pegawai'),
							'no_askes'=>$this->input->post ('no_askes'),
							'no_ktp'=>$this->input->post ('no_ktp'),
							'npwp'=>$this->input->post ('npwp'),
							'nama_skpd'=>$this->input->post ('nama_skpd'),
							'kd_skpd'=>$this->input->post ('kd_skpd'),
							'nama_unit_kerja'=>$this->input->post ('nama_unit_kerja')
						);
					$this->pegawai_model->create($data);
					redirect('setup/pegawai/find/'.$this->input->post('nip').'/nip','refresh');
				
				
				}
				
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup/pegawai/add', 'refresh');
			}
			
		} else {
			$data = array(
					'picture'=>null,
					'nip'=>null,
					'nip_lama'=>null,
					'nama'=>null,
					'gelar_depan'=>null,
					'gelar_belakang'=>null,
					'tempat_lahir'=>null,
					'tgl_lahir'=>null,
					'telp'=>null,
					'kelamin'=>null,
					'agama'=>null,
					'alamat'=>null,
					'kode_pos'=>null,
					'gol_darah'=>null,
					'status'=>null,
					'status_pegawai'=>null,
					'no_kartu_pegawai'=>null,
					'no_askes'=>null,
					'no_ktp'=>null,
					'npwp'=>null,
					'kd_skpd'=>null
			);
			$this->template->admin_render('setup/pegawai/form',$this->inputSetting($data));
		}
	}
	
	public function modify($id=null) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				
				$filename = $this->input->post('picture');
				$config['upload_path'] = "./files/foto_pegawai/";
				$config['allowed_types'] = "png|jpg|pdf";
				$config['overwrite']="true";
				$config['max_size']="20000000";
				$config['file_name'] = $this->input->post('nip');
				$this->load->library('upload', $config);
				

				if(!$this->upload->do_upload())
				{
						$data = array(
							'nip'=>$this->input->post('nip'),
							'nip_lama'=>$this->input->post ('nip_lama'),
							'nama'=>$this->input->post ('nama'),
							'gelar_depan'=>$this->input->post ('gelar_depan'),
							'gelar_belakang'=>$this->input->post ('gelar_belakang'),
							'tempat_lahir'=>$this->input->post ('tempat_lahir'),
							'tgl_lahir'=>$this->input->post ('tgl_lahir'),
							'telp'=>$this->input->post ('telp'),
							'kelamin'=>$this->input->post ('kelamin'),
							'agama'=>$this->input->post ('agama'),
							'alamat'=>$this->input->post ('alamat'),
							'kode_pos'=>$this->input->post ('kode_pos'),
							'gol_darah'=>$this->input->post ('gol_darah'),
							'status'=>$this->input->post ('status'),
							'status_pegawai'=>$this->input->post ('status_pegawai'),
							'no_kartu_pegawai'=>$this->input->post ('no_kartu_pegawai'),
							'no_askes'=>$this->input->post ('no_askes'),
							'no_ktp'=>$this->input->post ('no_ktp'),
							'npwp'=>$this->input->post ('npwp'),
							'nama_skpd'=>$this->input->post ('nama_skpd'),
							'kd_skpd'=>$this->input->post ('kd_skpd'),
							'nama_unit_kerja'=>$this->input->post ('nama_unit_kerja')
						);
					$this->pegawai_model->updatenoimage($data);
					redirect('setup/pegawai/find/'.$this->input->post('nip').'/nip','refresh');
				
				}else{
								 //get data
						$dat = $this->upload->data();
					
						$data = array(
							'picture'=>$dat['file_name'],
							'nip'=>$this->input->post('nip'),
							'nip_lama'=>$this->input->post ('nip_lama'),
							'nama'=>$this->input->post ('nama'),
							'gelar_depan'=>$this->input->post ('gelar_depan'),
							'gelar_belakang'=>$this->input->post ('gelar_belakang'),
							'tempat_lahir'=>$this->input->post ('tempat_lahir'),
							'tgl_lahir'=>$this->input->post ('tgl_lahir'),
							'telp'=>$this->input->post ('telp'),
							'kelamin'=>$this->input->post ('kelamin'),
							'agama'=>$this->input->post ('agama'),
							'alamat'=>$this->input->post ('alamat'),
							'kode_pos'=>$this->input->post ('kode_pos'),
							'gol_darah'=>$this->input->post ('gol_darah'),
							'status'=>$this->input->post ('status'),
							'status_pegawai'=>$this->input->post ('status_pegawai'),
							'no_kartu_pegawai'=>$this->input->post ('no_kartu_pegawai'),
							'no_askes'=>$this->input->post ('no_askes'),
							'no_ktp'=>$this->input->post ('no_ktp'),
							'npwp'=>$this->input->post ('npwp'),
							'nama_skpd'=>$this->input->post ('nama_skpd'),
							'kd_skpd'=>$this->input->post ('kd_skpd'),
							'nama_unit_kerja'=>$this->input->post ('nama_unit_kerja')
						);
					$this->pegawai_model->update($data);
					redirect('setup/pegawai/find/'.$this->input->post('nip').'/nip','refresh');
				
				}
			}
		} else {
			$query = $this->pegawai_model->fetchById($id);
			foreach ($query as $row){
				$this->template->admin_render('setup/pegawai/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($id=null) {
		
		
		$gambar = $this->pegawai_model->link_gambar($id);
			if ($gambar->num_rows() > 0)
			{
				$row = $gambar->row();			
				$file_gambar = $row->picture;
				$path_file = './files/foto_pegawai/';
				unlink($path_file.$file_gambar);
			}
			
        $this->pegawai_model->delete($id);
		redirect ('setup/pegawai/index','refresh');
			
		
	}
	
	public function find(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$column = $this->input->post('column');
				
				if($this->input->post('column')=="nip"){				
					$query = str_replace(" ","",$this->input->post('data'));
				}else{
					$query = $this->input->post('data');
				}
				
				$option = array(
					'user_column'=>$column,
					'user_data'=>$query
				);
				$this->session->set_userdata($option);
			}else{
			   $query = $this->uri->segment ( 4 );
			   $column = $this->uri->segment ( 5 );
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "setup/pegawai/find/".$query."/".$column;
			$config ["total_rows"] = $this->pegawai_model->search_count($column,$query);
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 6;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 6 ) == "") {
				$data ['number'] = 0;
			} else {
				$data ['number'] = $this->uri->segment ( 6 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
				
			$this->data ['pegawai'] = $this->pegawai_model->search($column,$query,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/pegawai/result_pegawai', $this->data);
		}
	}
	
	function get() {
        
    }
	
	public function report_rh($nip)
	{
		$this->data['pegawai'] =  $this->pegawai_model->get($nip);
		$this->data['riwayat_pendidikan'] =  $this->riwayat_pendidikan_model->get($nip);
		$this->data['riwayat_kepangkatan'] =  $this->riwayat_kepangkatan_model->get($nip);
		$this->data['riwayat_jabatan'] =  $this->riwayat_jabatan_model->get($nip);
		$this->data['riwayat_diklat'] =  $this->riwayat_diklat_model->get($nip);
		$this->data['riwayat_penghargaan'] =  $this->riwayat_penghargaan_model->get($nip);
		$this->data['riwayat_hukuman'] =  $this->riwayat_hukuman_model->get($nip);
		$this->load->view('report/report_rh/report_rh2', $this->data);
		
		$sumber = $this->load->view('report/report_rh/report_rh', $this->data, TRUE);
        $html = $sumber;


        $pdfFilePath = "".$nip.".pdf";

        $pdf = $this->m_pdf->load();

        $pdf->AddPage('L');
		$pdf=new mPDF('','A4');
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);
        
        $pdf->Output($pdfFilePath, "D");
        exit();
		
	}  
	
	public function uploadImgae(){
		
	}
	
}