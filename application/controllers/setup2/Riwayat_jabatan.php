<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_jabatan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_jabatan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/riwayat_jabatan_model' );
		$this->load->model ('setup/pegawai_model');
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('nip','lang:riwayat_nip','max_length[50]');
		$this->form_validation->set_rules('kd_skpd', 'lang:riwayat_kd_skpd','max_length[50]');
		$this->form_validation->set_rules('unit_kerja', 'lang:unit_kerja','max_length[50]');
		$this->form_validation->set_rules('jenis_jabatan', 'lang:jenis_jabatan','max_length[50]');
		$this->form_validation->set_rules('nama_jabatan', 'lang:nama_jabatan','max_length[50]');
		$this->form_validation->set_rules('esselon', 'lang:riwayat_esselon','max_length[50]');
		$this->form_validation->set_rules('no_sk', 'lang:riwayat_no_sk','max_length[50]');
		$this->form_validation->set_rules('tmt', 'lang:tmt','max_length[50]');
		$this->form_validation->set_rules('valid_jabatan', 'lang:riwayat_valid_jabatan','max_length[1]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['nip'] = array(
				'name'  => 'nip',
				'id'    => 'nip',
				'type'  => 'text',
				'class' => 'form-control',
				'readonly'=> 'readonly',
				'placeholder'=>'nomor induk pegawai',
				'value' => $data['nip'],
		);
		$this->data['nama'] = array(
				'name'  => 'nama',
				'id'    => 'nama',
				'type'  => 'text',
				'readonly'=>'readonly',
				'class' => 'form-control',
				'placeholder'=>'Nama Pegawai',
				'value' => $data['nama'],
		);
		$this->data['seq_no'] = array(
				'name'  => 'seq_no',
				'id'    => 'seq_no',
				'type'  => 'hidden',
				'class' => 'form-control',
				'readonly' => 'readonly',
				'placeholder'=>'sequence no',
				'value' => $data['seq_no'],
		);
		$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'SKPD',
				'value' => $data['kd_skpd'],
		);
		$this->data['jenis_jabatan'] = array(
				'name'  => 'jenis_jabatan',
				'id'    => 'jenis_jabatan',
				'type'  => 'text',
				'required'  => 'required',
				'class' => 'form-control',
				'placeholder'=>'jenis jabatan',
				'value' => $data['jenis_jabatan'],
		);
		$this->data['nama_jabatan'] = array(
				'name'  => 'nama_jabatan',
				'id'    => 'nama_jabatan',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'nama jabatan',
				'value' => $data['nama_jabatan'],
		);
		$this->data['unit_kerja'] = array(
				'name'  => 'unit_kerja',
				'id'    => 'unit_kerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Unit Kerja',
				'value' => $data['unit_kerja'],
		);
		$this->data['esselon'] = array(
				'name'  => 'esselon',
				'id'    => 'esselon',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'esselon',
				'value' => $data['esselon'],
		);
		$this->data['no_sk'] = array(
				'name'  => 'no_sk',
				'id'    => 'no_sk',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'no surat keputusan',
				'value' => $data['no_sk'],
		);
		if($data['tmt']=="0000-00-00"){
		} else if($data['tmt']==""){
		} else {
			$data['tmt2'] = date("d-m-Y",strtotime($data['tmt']));
		}
		$this->data['tmt'] = array(
				'name'  => 'tmt',
				'id'    => 'datepicker',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tmt2'],
		);
		$this->data['valid_jabatan'] = array(
				'name'  => 'valid_jabatan',
				'id'    => 'valid_jabatan',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'valid',
				'value' => $data['valid_jabatan'],
		);
		$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'type'  => 'text',
				'required' => 'required',
				'class' => 'form-control selectpicker',
				'placeholder'=>'Nama SKPD',
				'value' => $data['kd_skpd'],
		);
		$this->data['kd_unitkerja'] = array(
				'name'  => 'kd_unitkerja',
				'id'    => 'kd_unitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Nama Unit Kerja',
				'value' => $data['kd_unitkerja'],
		);
		$this->data['get_jenis_jabatan'] = $data['jenis_jabatan'];
		$this->data['get_esselon'] = $data['esselon'];
		$this->data['jabatan'] = $this->riwayat_jabatan_model->ambil_nama_jabatan($data['jenis_jabatan']);
		$this->data['get_jabatan'] = $data['nama_jabatan'];
		$this->data['nama_skpd'] = $this->riwayat_jabatan_model->get_skpd_all();
		$this->data['get_nama_skpd'] = $data['kd_skpd'];
		$this->data['nama_unitkerja'] = $this->riwayat_jabatan_model->ambil_unit_kerja($data['kd_skpd']);
		$this->data['get_nama_unitkerja'] = $data['kd_unitkerja'];
		return $this->data;
	}
	
	public function result_riwayat_jabatan($id) {
		
		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
		/* Get all users */
		
		$config = array ();
		$config ["base_url"] = base_url () . "setup2/riwayatjabatan/index";
		$config ["total_rows"] = $this->riwayat_jabatan_model->record_count($id);
		$config ["per_page"] = 25;
		$config ["uri_segment"] = 5;
		$choice = $config ["total_rows"] / $config ["per_page"];
		$config ["num_links"] = 5;
		
		// config css for pagination
		$config ['full_tag_open'] = '<ul class="pagination">';
		$config ['full_tag_close'] = '</ul>';
		$config ['first_link'] = 'First';
		$config ['last_link'] = 'Last';
		$config ['first_tag_open'] = '<li>';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="prev">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li>';
		$config ['next_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li>';
		$config ['last_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		
		if ($this->uri->segment ( 5 ) == "") {
			$data ['number'] = 0;
		} else {
			$data ['number'] = $this->uri->segment ( 5 );
		}
		
		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 5 )) ? $this->uri->segment ( 5 ) : 0;
		
		$this->data ['riwayat'] = $this->riwayat_jabatan_model->fetchAll($config ["per_page"], $page,$id);
		$this->data ['nip'] = $id;
		$this->data ['nama_pegawai'] = $this->pegawai_model->getName($id);
		$this->data ['links'] = $this->pagination->create_links ();
		$this->template->member_render('setup2/riwayat_jabatan/result_riwayat_jabatan', $this->data);
		
	}
	
	public function add($nip){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'nip'=>$this->input->post('nip'),
						'seq_no'=>$this->riwayat_jabatan_model->generateSeqNo($this->input->post('nip')),
						'kd_skpd'=>$this->input->post ('kd_skpd'),
						'kd_unitkerja'=>$this->input->post ('kd_unitkerja'),
						'jenis_jabatan'=>$this->input->post ('jenis_jabatan'),
						'nama_jabatan'=>$this->input->post ('nama_jabatan'),
						'esselon'=>$this->input->post ('esselon'),
						'no_sk'=>$this->input->post ('no_sk'),
						'tmt'=>$this->input->post ('tmt'),
						'valid_jabatan'=>$this->input->post ('valid_jabatan')
				);
				$this->riwayat_jabatan_model->create($data);
				redirect('setup2/riwayat_jabatan/result_riwayat_jabatan/'.$this->input->post('nip'),'refresh');
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup2/riwayat_jabatan/add', 'refresh');
			}
			
		} else {
			$data = array(
						'nip'=>$nip,
						'nama'=>$this->pegawai_model->getName($nip),
						'seq_no'=>null,
						'kd_skpd'=>null,
						'kd_unitkerja'=>null,
						'jenis_jabatan'=>null,
						'nama_jabatan'=>null,
						'esselon'=>null,
						'no_sk'=>null,
						'tmt'=>null,
						'valid_jabatan'=>null
			);
			$this->template->member_render('setup2/riwayat_jabatan/form',$this->inputSetting($data));
		}
	}
	
	public function modify($nip,$seq_no) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'nip'=>$this->input->post('nip'),
						'seq_no'=>$this->input->post('seq_no'),
						'kd_skpd'=>$this->input->post ('kd_skpd'),
						'kd_unitkerja'=>$this->input->post ('kd_unitkerja'),
						'jenis_jabatan'=>$this->input->post ('jenis_jabatan'),
						'nama_jabatan'=>$this->input->post ('nama_jabatan'),
						'esselon'=>$this->input->post ('esselon'),
						'no_sk'=>$this->input->post ('no_sk'),
						'tmt'=>$this->input->post ('tmt'),
						'valid_jabatan'=>$this->input->post ('valid_jabatan')
				);
			}
			$this->riwayat_jabatan_model->update($data);
			redirect('setup2/riwayat_jabatan/result_riwayat_jabatan/'.$nip,'refresh');
		} else {
			$query = $this->riwayat_jabatan_model->fetchById($nip,$seq_no);
			foreach ($query as $row){
				$this->template->member_render('setup2/riwayat_jabatan/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($nip=null,$seq_no=null) {
		$this->riwayat_jabatan_model->delete($nip,$seq_no);
		redirect('setup2/riwayat_jabatan/result_riwayat_jabatan/'.$nip,'refresh');
	}
	
	public function find(){
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "setup/riwayat_jabatan/find";
			$config ["total_rows"] = $this->riwayat_jabatan_model->search_count($column,$query);
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 4 ) == "") {
				$data ['number'] = 0;
			} else {
				$data ['number'] = $this->uri->segment ( 4 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
				
			$this->data ['riwayat'] = $this->riwayat_jabatan_model->search($column,$query,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->member_render('setup/riwayat_jabatan/index', $this->data);
		}
	}
	
	public function getImage(){
		
	}
	
}