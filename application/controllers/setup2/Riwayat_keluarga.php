<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_keluarga extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_jabatan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/riwayat_anak_model' );
		$this->load->model ('setup/riwayat_suami_istri_model' );
		$this->load->model ('setup/pegawai_model');
		
	}
	
	public function result_riwayat_keluarga($id) {
		
		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
		$this->pagination->initialize ();
		$page = ($this->uri->segment ( 5 )) ? $this->uri->segment ( 5 ) : 0;
		
		$this->data ['anak'] = $this->riwayat_anak_model->fetchAll($id);
		$this->data ['suami_istri'] = $this->riwayat_suami_istri_model->jml($id);
		$this->data['jml'] = count($this->data ['suami_istri']);
		$this->data ['nip'] = $id;
		$this->data ['nama_pegawai'] = $this->pegawai_model->getName($id);
		$this->template->member_render('setup2/riwayat_keluarga/result_riwayat_keluarga', $this->data);
		
	}
	
}