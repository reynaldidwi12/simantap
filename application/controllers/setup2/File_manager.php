<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * File manager
 *
 * Ini adalah fitur file manager dengan bantuan elFinder.
 * elFinder lib-nya berada di application/third_party/elFinder.
 *
 * @author greentech.ID <me@system112.org>
 */
require APPPATH . '/third_party/elFinder/vendor/autoload.php';

class File_Manager extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		/* directory that will be used to store files */
		$this->config->set_item('files_directory', FCPATH . '.ELFINDER');

		/* Load Library */
		$this->load->library('session', 'ion_auth');

		/* Title Page :: Common */
		$this->page_title->push(lang('menu_pegawai'));
		$this->data['pagetitle'] = $this->page_title->show();
	}

	public function user($userNip = NULL)
	{
		$this->checkPermission($userNip);

		$this->data['userNip'] = $userNip;
		$this->template->member_render('setup2/file_manager/index', $this->data);
	}

	public function manager($userNip = NULL)
	{
		$this->checkPermission($userNip);

		$this->load->helper('url');
		$this->data['connector'] = site_url() . 'setup2/file_manager/connector/' . $userNip;
		$this->load->view('elfinder', $this->data);
	}

	public function connector($userNip = NULL)
	{
		$topDirectory = $this->config->item('files_directory');
		$pathToOpen = $topDirectory . '/' . $userNip;

		// if dir not exist, create it.
		if (!is_dir($pathToOpen)) {
				if ( ! mkdir($pathToOpen, 0777)) {
					return show_error('Internal server error. code: 32e', 500);
				}
				// create .htaccess that prevent direct directory access (security).
				copy($topDirectory . '/.htaccess', $pathToOpen . '/.htaccess');
		}
		$opts = array(
			'roots' => array(
				array(
					'driver'        => 'LocalFileSystem',
					'path'          => $pathToOpen,
					'uploadDeny'    => array('all'),                  // All Mimetypes not allowed to upload
					'uploadMaxSize' => '200M',
					'uploadAllow'   => array(
						'image',
						'text/plain',
						'application/pdf',
						'application/msword',
						'application/vnd.ms-excel',
						'application/vnd.ms-powerpoint',
						// Open office
						'application/vnd.oasis.opendocument.text',
						'application/vnd.oasis.opendocument.graphics',
						'application/vnd.oasis.opendocument.presentation',
						'application/vnd.oasis.opendocument.spreadsheet',
						'application/vnd.oasis.opendocument.chart',
						'application/vnd.oasis.opendocument.formula',
						'application/vnd.oasis.opendocument.database',
						'application/vnd.oasis.opendocument.image',
						// MS office 2007
						'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
						'application/vnd.openxmlformats-officedocument.presentationml.presentation',
						'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
						'application/vnd.openxmlformats-officedocument.presentationml.slideshow',

						// Archives
						'application/zip',
						'application/x-rar',
					),// Mimetype
					'uploadOrder'   => array('deny', 'allow'),        // allowed Mimetype
					'accessControl' => array($this, 'elfinderAccess'),// disable and hide dot starting files (OPTIONAL)
				)
			),
		);
		$connector = new elFinderConnector(new elFinder($opts));
		$connector->run();
	}

	public function elfinderAccess($attr, $path, $data, $volume, $isDir, $relpath)
	{
		$basename = basename($path);
		return $basename[0] === '.'                  // if file/folder begins with '.' (dot)
						 && strlen($relpath) !== 1           // but with out volume root
				? !($attr == 'read' || $attr == 'write') // set read+write to false, other (locked+hidden) set to true
				:  null;                                 // else elFinder decide it itself
	}

	/**
	 * Permission check
	 *
	 * Checking current logged in user who access file manager.
	 *
	 * Rules:
	 *   1. Must be logged in
	 *   2. Must pass user target NIP
	 *   3. Must be in the same SKPD
	 */
	private function checkPermission($userNip = NULL)
	{
		if ( ! $this->ion_auth->logged_in() OR ! $userNip )
		{
			return show_error('Anda tidak diizinkan mengakses halaman ini.');
		}

		// Compare kd_skpd, do not allow access if different skpd
		$this->load->model('setup/pegawai_model', 'pegawai_model');

		$loggedInUser = $this->ion_auth->user()->row();
		$userTarget = $this->pegawai_model->get($userNip);

		if ($loggedInUser->kd_skpd !== $userTarget[0]->kd_skpd) {
			return show_error('Anda tidak diizinkan mengakses halaman ini.');
		}
	}
}
