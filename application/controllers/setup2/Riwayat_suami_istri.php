<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_suami_istri extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_diklat'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/riwayat_suami_istri_model' );
		$this->load->model ('setup/pegawai_model');
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('nip','lang:riwayat_nip','max_length[50]');
		$this->form_validation->set_rules('nama', 'lang:nama','max_length[100]');
		$this->form_validation->set_rules('tempat_lahir', 'lang:tempat_lahir','max_length[100]');
		$this->form_validation->set_rules('tgl_lahir', 'lang:tgl_lahir','max_length[50]');
		$this->form_validation->set_rules('kelamin', 'lang:kelamin','max_length[50]');
		$this->form_validation->set_rules('bpjs', 'lang:bpjs','max_length[50]');
		$this->form_validation->set_rules('status_anak', 'lang:status_anak','max_length[50]');
		$this->form_validation->set_rules('status_pendidikan', 'lang:status_pendidikan','max_length[50]');
		$this->form_validation->set_rules('tingkat_pendidikan', 'lang:tingkat_pendidikan','max_length[50]');
		
		return $this->form_validation->run();
	}
	
	/* setup2 Property column */
	public function inputSetting($data){
		$this->data['nip'] = array(
				'name'  => 'nip',
				'id'    => 'nip',
				'type'  => 'text',
				'class' => 'form-control',
				'readonly'=> 'readonly',
				'placeholder'=>'nomor induk pegawai',
				'value' => $data['nip'],
		);
		$this->data['nip_'] = array(
				'name'  => 'nip_',
				'id'    => 'nip_',
				'type'  => 'hidden',
				'class' => 'form-control',
				'readonly'=> 'readonly',
				'placeholder'=>'nomor induk pegawai',
				'value' => $data['nip'],
		);
		$this->data['nama'] = array(
				'name'  => 'nama',
				'id'    => 'nama',
				'type'  => 'text',
				'readonly'=>'readonly',
				'class' => 'form-control',
				'placeholder'=>'Nama Pegawai',
				'value' => $data['nama'],
		);
		$this->data['seq_no'] = array(
				'name'  => 'seq_no',
				'id'    => 'seq_no',
				'type'  => 'hidden',
				'class' => 'form-control',
				'placeholder'=>'sequence no',
				'value' => $data['seq_no'],
		);
		$this->data['no_diklat'] = array(
				'name'  => 'no_diklat',
				'id'    => 'no_diklat',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'no diklat',
				'value' => $data['no_diklat'],
		);
		$this->data['nip2'] = array(
				'name'  => 'nip2',
				'id'    => 'nip2',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Masukkan NIP',
				'value' => $data['nip2'],
		);
		$this->data['nama2'] = array(
				'name'  => 'nama2',
				'id'    => 'nama2',
				'type'  => 'text',
				'required'  => 'required',
				'class' => 'form-control',
				'placeholder'=>'Nama Suami/Istri',
				'value' => $data['nama2'],
		);
		$this->data['tempat_lahir'] = array(
				'name'  => 'tempat_lahir',
				'id'    => 'tempat_lahir',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Tempat Lahir',
				'value' => $data['tempat_lahir'],
		);
		if($data['tgl_lahir']){
			$data['tgl_lahir2'] = date("d-m-Y",strtotime($data['tgl_lahir']));
		}else{
			
		}
		$this->data['tgl_lahir'] = array(
				'name'  => 'tgl_lahir',
				'id'    => 'datepicker',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tgl_lahir2'],
		);
		
		if($data['tgl_menikah']){
			$data['tgl_menikah2'] = date("d-m-Y",strtotime($data['tgl_menikah']));
		}else{
			
		}
		$this->data['tgl_menikah'] = array(
				'name'  => 'tgl_menikah',
				'id'    => 'datepicker1',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tgl_menikah2'],
		);
		
		if($data['tgl_meninggal']){
			$data['tgl_meninggal2'] = date("d-m-Y",strtotime($data['tgl_meninggal']));
		}else{
			
		}
		$this->data['tgl_meninggal'] = array(
				'name'  => 'tgl_meninggal',
				'id'    => 'datepicker2',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tgl_meninggal2'],
		);
		
		if($data['tgl_cerai']){
			$data['tgl_cerai2'] = date("d-m-Y",strtotime($data['tgl_cerai']));
		}else{
			
		}
		$this->data['tgl_cerai'] = array(
				'name'  => 'tgl_cerai',
				'id'    => 'datepicker3',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tgl_cerai2'],
		);
		
		$this->data['no_akta_nikah'] = array(
				'name'  => 'no_akta_nikah',
				'id'    => 'no_akta_nikah',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'No. Akta Nikah',
				'value' => $data['no_akta_nikah'],
		);
		$this->data['no_akta_meninggal'] = array(
				'name'  => 'no_akta_meninggal',
				'id'    => 'no_akta_meninggal',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'No. Akta Meninggal',
				'value' => $data['no_akta_meninggal'],
		);
		$this->data['no_akta_cerai'] = array(
				'name'  => 'no_akta_cerai',
				'id'    => 'no_akta_cerai',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'No. Akta Cerai',
				'value' => $data['no_akta_cerai'],
		);
		$this->data['status'] = array(
				'name'  => 'status',
				'id'    => 'status',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Status',
				'value' => $data['status'],
		);
		$this->data['bpjs'] = array(
				'name'  => 'bpjs',
				'id'    => 'bpjs',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'No. BPJS',
				'value' => $data['bpjs'],
		);
		$this->data['get_nip'] = $data['nip2'];
		$this->data['get_status'] = $data['status'];
		return $this->data;
	}
	
	
	public function add($nip){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'nip'=>$this->input->post('nip_'),
						'seq_no'=>$this->riwayat_suami_istri_model->generateSeqNo($this->input->post('nip_')),
						'nama'=>$this->input->post ('nama2'),
						'tempat_lahir'=>$this->input->post ('tempat_lahir'),
						'tgl_lahir'=>$this->input->post ('tgl_lahir'),
						'tgl_menikah'=>$this->input->post ('tgl_menikah'),
						'tgl_meninggal'=>$this->input->post ('tgl_meninggal'),
						'tgl_cerai'=>$this->input->post ('tgl_cerai'),
						'no_akta_nikah'=>$this->input->post ('no_akta_nikah'),
						'no_akta_meninggal'=>$this->input->post ('no_akta_meninggal'),
						'no_akta_cerai'=>$this->input->post ('no_akta_cerai'),
						'status'=>$this->input->post ('status'),
						'bpjs'=>$this->input->post ('bpjs')
		
				);
				$this->riwayat_suami_istri_model->create($data);
				redirect('setup2/riwayat_keluarga/result_riwayat_keluarga/'.$this->input->post('nip_'));
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup2/riwayat_suami_istri/add', 'refresh');
			}
			
		} else {
			$data = array(
					'nip'=>$nip,
					'nama'=>$this->pegawai_model->getName($nip),
					'seq_no'=>null,
					'nama2'=>null,
					'tempat_lahir'=>null,
					'tgl_lahir'=>null,
					'tgl_menikah'=>null,
					'tgl_meninggal'=>null,
					'tgl_cerai'=>null,
					'no_akta_nikah'=>null,
					'no_akta_meninggal'=>null,
					'no_akta_cerai'=>null,
					'status'=>null,
					'bpjs'=>null
			);
			$this->template->member_render('setup2/riwayat_suami_istri/form',$this->inputSetting($data));
		}
	}
	
	public function modify($nip,$seq_no) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'nip'=>$this->input->post('nip_'),
					'seq_no'=>$this->input->post ('seq_no'),
					'nama'=>$this->input->post ('nama2'),
					'tempat_lahir'=>$this->input->post ('tempat_lahir'),
					'tgl_lahir'=>$this->input->post ('tgl_lahir'),
					'tgl_menikah'=>$this->input->post ('tgl_menikah'),
					'tgl_meninggal'=>$this->input->post ('tgl_meninggal'),
					'tgl_cerai'=>$this->input->post ('tgl_cerai'),
					'no_akta_nikah'=>$this->input->post ('no_akta_nikah'),
					'no_akta_meninggal'=>$this->input->post ('no_akta_meninggal'),
					'no_akta_cerai'=>$this->input->post ('no_akta_cerai'),
					'status'=>$this->input->post ('status'),
					'bpjs'=>$this->input->post ('bpjs')
				);
			}
			$this->riwayat_suami_istri_model->update($data);
			redirect('setup2/riwayat_keluarga/result_riwayat_keluarga/'.$nip,'refresh');
		} else {
			$query = $this->riwayat_suami_istri_model->fetchById($nip,$seq_no);
			foreach ($query as $row){
				$this->template->member_render('setup2/riwayat_suami_istri/form',$this->inputSetting($row));
			}
		}
	}
	
	public function cek() {
			
			$nip2 = str_replace(" ","",$this->input->post('nip2'));
			
			if($this->uri->segment(5)){
				$query = $this->riwayat_suami_istri_model->fetchById_pegawai($nip2,$this->input->post('nip'), $this->uri->segment(5));
				$jml = count($query);
				if($jml==0){
						redirect('/setup2/riwayat_suami_istri/modify/'.$this->input->post('nip').'/'.$this->uri->segment(5));
				}else{
					foreach ($query as $row){
						$this->template->member_render('setup2/riwayat_suami_istri/form',$this->inputSetting($row));
					}
				}
			}else{
				$query = $this->riwayat_suami_istri_model->fetchById_pegawai($nip2,$this->input->post('nip'));
				$jml = count($query);
				if($jml==0){
						redirect('/setup2/riwayat_suami_istri/add/'.$this->input->post('nip'));
				}else{
					foreach ($query as $row){
						$this->template->member_render('setup2/riwayat_suami_istri/form',$this->inputSetting($row));
					}
				}
			}
			
	}
	
	public function remove($nip=null,$seq_no=null) {
		$this->riwayat_suami_istri_model->delete($nip,$seq_no);
		redirect ('setup2/riwayat_keluarga/result_riwayat_keluarga/'.$nip,'refresh');
	}
	

	
}