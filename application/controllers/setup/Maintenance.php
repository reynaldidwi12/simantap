<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Maintenance extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_kecamatan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
		
			$this->data ['Maintenance'] = "Maintenance";
			$this->template->admin_render('maintenance/index', $this->data);
		}
	}
	
}