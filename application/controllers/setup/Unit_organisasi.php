<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class unit_organisasi extends Admin_Controller {

	public function __construct() {
		parent::__construct ();
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_kecamatan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/unit_organisasi_model' );
		$this->load->model ('setup/skpd_model' );
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('kd_unitorganisasi','lang:kd_unitorganisasi','max_length[300]');
		$this->form_validation->set_rules('nama_unitorganisasi', 'lang:nama_unitorganisasi','max_length[2000]');
		$this->form_validation->set_rules('kd_skpd', 'lang:kd_skpd','max_length[2000]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['kd_unitorganisasi'] = array(
				'name'  => 'kd_unitorganisasi',
				'id'    => 'kd_unitorganisasi',
				'type'  => 'hidden',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'kode unit organisasi',
				'value' => $data['kd_unitorganisasi'],
		);
		$this->data['nama_unitorganisasi'] = array(
				'name'  => 'nama_unitorganisasi',
				'id'    => 'nama_unitorganisasi',
				'type'  => 'text',
				'class' => 'form-control typeahead',
				'data-provide' => 'typeahead',
				'autocomplete' => 'off',
				'required'=> 'required',
				'placeholder'=>'nama unit organisasi',
				'value' => $data['nama_unitorganisasi'],
		); 
		$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama prodi',
				'value' => $data['kd_skpd'],
		);
		$this->data['kd_hiden'] = array(
				'name'  => 'kd_hiden',
				'id'    => 'kd_hiden',
				'type'  => 'hidden',
				'placeholder'=>'kd_hiden',
				'value' => $data['kd_unitorganisasi'],
		);

		$this->data['skpd'] = $this->skpd_model->get_skpd_by_status('Y');
		$this->data['get_skpd'] = $data['kd_skpd'];
		return $this->data;
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "setup/unit_organisasi/index";
			$config ["total_rows"] = $this->unit_organisasi_model->record_count ();
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['unit_organisasi'] = $this->unit_organisasi_model->fetchAll($config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->data ['total_rows'] = $config["total_rows"];
			
			$this->template->admin_render('setup/unit_organisasi/index', $this->data);
		}
	}
	
	public function add(){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$nama_unor = trim($this->input->post ('nama_unitorganisasi'));
				$data = array(
						'kd_unitorganisasi'=> md5($nama_unor.time()),
						'nama_unitorganisasi'=> trim($nama_unor),
						'kd_skpd'=> trim($this->input->post ('kd_skpd'))
				);

				if($this->input->post ('kd_hiden') != NULL){
					$mymsg_errors = 'Unit organisasi telah ada dalam sistem.';
					$this->session->set_flashdata('message',$mymsg_errors);
					redirect('setup/unit_organisasi/add', 'refresh');

				}else{
					$this->unit_organisasi_model->create($data);
					redirect('setup/unit_organisasi','refresh');
				}
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup/unit_organisasi/add', 'refresh');
			}
			
		} else {
			$data = array(
					'kd_unitorganisasi'=>null,
					'nama_unitorganisasi'=>null,
					'kd_skpd'=>null
			);
			// print_r($this->inputSetting($data));exit;
			$this->template->admin_render('setup/unit_organisasi/form',$this->inputSetting($data));
		}
	}
	
	public function modify($id=null) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'kd_unitorganisasi'=> trim($this->input->post('kd_unitorganisasi')),
					'nama_unitorganisasi'=> trim($this->input->post('nama_unitorganisasi')),
					'kd_skpd'=> trim($this->input->post ('kd_skpd'))
				);
			}
			$this->unit_organisasi_model->update($data);
			
			if($this->uri->segment(5)){
				redirect('setup/unit_organisasi/index/'.$this->uri->segment(5),'refresh');
			} else {
				redirect('setup/unit_organisasi','refresh');
			}
			
		} else {
			$query = $this->unit_organisasi_model->fetchById($id);
			foreach ($query as $row){
				$this->template->admin_render('setup/unit_organisasi/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($id=null) {
		$this->unit_organisasi_model->delete($id);
		redirect ('setup/unit_organisasi/index','refresh');
	}
	
	public function find(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$column = $this->input->post('column');
				$query = $this->input->post('data');
				
				$option = array(
					'user_column'=>$column,
					'user_data'=>$query
				);
				$this->session->set_userdata($option);
			}else{
				$query = $this->uri->segment ( 4 );
				$column = $this->uri->segment ( 5 );

				if($query == NULL || $column == NULL){
					redirect ('setup/unit_organisasi/index','refresh');
				}
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "setup/unit_organisasi/find/".$query."/".$column;
			$config ["total_rows"] = $this->unit_organisasi_model->search_count($column,$query);
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 6;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 6 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 6 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
		
			$this->data ['unit_organisasi'] = $this->unit_organisasi_model->search($column,$query,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->data ['total_rows'] = $config["total_rows"];
			$this->template->admin_render('setup/unit_organisasi/index', $this->data);
		}
	}
	
	public function uploadImage(){
		
	}

	public function get_auto_nama(){

		$s = $this->input->get('s',TRUE);
		$kd_skpd = $this->input->get('kd_skpd',TRUE);

			
		$datas = $this->unit_organisasi_model->get_auto_nama(array('kd_skpd'=>$kd_skpd,'keyword'=>$s));

		header('Content-type: application/json');
        $json = json_encode($datas);
        echo $json;
        exit;
	}
	
}