<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_jabatan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_jabatan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/riwayat_jabatan_model' );
		$this->load->model ('setup/pegawai_model');

		$this->load->model ('setup/jabatan_model' );
		$this->load->model ('setup/sub_unit_kerja_model' );
		$this->load->model ('setup/unit_kerja_model' );
		$this->load->model ('setup/unit_organisasi_model' );
		$this->load->model ('setup/skpd_model' );
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('nip','lang:riwayat_nip','trim|max_length[50]');
		
		
		$this->form_validation->set_rules('jenis_jabatan', 'lang:jenis_jabatan','trim|max_length[50]');
		
		$this->form_validation->set_rules('esselon', 'lang:riwayat_esselon','trim|max_length[50]');
		
		$this->form_validation->set_rules('tmt', 'lang:tmt','trim|max_length[50]');
		$this->form_validation->set_rules('tmt_jabatan', 'lang:tmt','trim|max_length[50]');
		$this->form_validation->set_rules('valid_jabatan', 'lang:riwayat_valid_jabatan','trim|max_length[1]');

		$this->form_validation->set_rules('kd_unitorganisasi','lang:kd_unitorganisasi','trim|max_length[255]');
		$this->form_validation->set_rules('kd_unitkerja','lang:kd_unitkerja','trim|max_length[255]');
		$this->form_validation->set_rules('kd_subunitkerja','lang:kd_subunitkerja','trim|max_length[255]');

		$required_if = $this->input->post('kd_jabatan') ? '|required' : '' ;
		$required_if_nama_jabatan = $this->input->post('nama_jabatan') ? '|required' : '' ;
		$required_if_nosk = $this->input->post('no_sk') ? '|required' : '' ;

		$this->form_validation->set_rules('kd_jabatan','lang:kd_jabatan','trim|max_length[50]'.$required_if);
		$this->form_validation->set_rules('no_sk', 'lang:riwayat_no_sk','trim|max_length[50]'.$required_if_nosk);
		
		$this->form_validation->set_rules('nama_jabatan', 'lang:nama_jabatan','trim|max_length[255]'.$required_if_nama_jabatan);

		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['nip'] = array(
				'name'  => 'nip',
				'id'    => 'nip',
				'type'  => 'text',
				'class' => 'form-control',
				'readonly'=> 'readonly',
				'placeholder'=>'nomor induk pegawai',
				'value' => $data['nip'],
		);
		$this->data['nama'] = array(
				'name'  => 'nama',
				'id'    => 'nama',
				'type'  => 'text',
				'readonly'=>'readonly',
				'class' => 'form-control',
				'placeholder'=>'Nama Pegawai',
				'value' => $data['nama'],
		);
		$this->data['seq_no'] = array(
				'name'  => 'seq_no',
				'id'    => 'seq_no',
				'type'  => 'hidden',
				'class' => 'form-control',
				'readonly' => 'readonly',
				'placeholder'=>'sequence no',
				'value' => $data['seq_no'],
		);
		
		$this->data['jenis_jabatan'] = array(
				'name'  => 'jenis_jabatan',
				'id'    => 'jenis_jabatan',
				'type'  => 'text',
				'required'  => 'required',
				'class' => 'form-control',
				'placeholder'=>'jenis jabatan',
				'value' => $data['jenis_jabatan'],
		);
		$this->data['kd_jabatan'] = array(
				'name'  => 'kd_jabatan',
				'id'    => 'kd_jabatan',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'Nama jabatan',
				'value' => $data['kd_jabatan'],
		);
		
		$this->data['esselon'] = array(
				'name'  => 'esselon',
				'id'    => 'esselon',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'esselon',
				'value' => $data['esselon'],
		);
		$this->data['no_sk'] = array(
				'name'  => 'no_sk',
				'id'    => 'no_sk',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'no surat keputusan',
				'value' => $data['no_sk'],
		);
		$this->data['nama_jabatan'] = array(
				'name'  => 'nama_jabatan',
				'id'    => 'nama_jabatan',
				'type'  => 'text',
				'class' => 'form-control typeahead',
				'data-provide' => 'typeahead',
				'autocomplete' => 'off',
				'required' => 'required',
				'placeholder'=>'nama jabatan',
				'value' => $data['nama_jabatan'],
		);

		$this->data['nama_jabatan_hiden'] = array(
				'name'  => 'nama_jabatan_hiden',
				'id'    => 'nama_jabatan_hiden',
				'type'  => 'hidden',
				'placeholder'=>'nama jabatan',
				'value' => $data['kd_jabatan'],
		);

		if($data['tmt']=="0000-00-00"){
		} else if ($data['tmt']==""){
		} else{
			$data['tmt2'] = date("d-m-Y",strtotime($data['tmt']));
		}
		$this->data['tmt'] = array(
				'name'  => 'tmt',
				'id'    => 'datepicker',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tmt2'],
		);

		if($data['tmt_jabatan']=="0000-00-00"){
		} else if ($data['tmt_jabatan']==""){
		} else{
			$data['tmt_jabatan2'] = date("d-m-Y",strtotime($data['tmt_jabatan']));
		}
		$this->data['tmt_jabatan'] = array(
				'name'  => 'tmt_jabatan',
				'id'    => 'datepicker2',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'tgl-bln-thn',
				'value' => $data['tmt_jabatan2'],
		);

		$this->data['valid_jabatan'] = array(
				'name'  => 'valid_jabatan',
				'id'    => 'valid_jabatan',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'valid',
				'value' => $data['valid_jabatan'],
		);

		$this->data['kd_subunitkerja'] = array(
				'name'  => 'kd_subunitkerja',
				'id'    => 'kd_subunitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'kode sub unit kerja',
				'value' => $data['kd_subunitkerja'],
		);

		$this->data['kd_unitkerja'] = array(
				'name'  => 'kd_unitkerja',
				'id'    => 'kd_unitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'kode unit kerja',
				'value' => $data['kd_unitkerja'],
		);

		$this->data['kd_unitorganisasi'] = array(
				'name'  => 'kd_unitorganisasi',
				'id'    => 'kd_unitorganisasi',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama unit organisasi',
				'value' => $data['kd_unitorganisasi'],
		); 

		$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'required' => 'required',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama opd',
				'value' => $data['kd_skpd'],
		); 
		
		
		
		
		$this->data['get_esselon'] = $data['esselon'];
		$this->data['skpd'] = $this->skpd_model->get_skpd();
		$this->data['get_skpd'] = $data['kd_skpd'];
		$this->data['unitorganisasi'] = $this->unit_organisasi_model->ambil_unit_organisasi($data['kd_skpd']);
		$this->data['get_unitorganisasi'] = $data['kd_unitorganisasi'];
		$this->data['unitkerja'] = $this->unit_kerja_model->ambil_unit_kerja($data['kd_unitorganisasi']);
		$this->data['get_unitkerja'] = $data['kd_unitkerja'];
		$this->data['subunitkerja'] = $this->sub_unit_kerja_model->ambil_sub_unit_kerja($data['kd_unitkerja']);
		$this->data['get_subunitkerja'] = $data['kd_subunitkerja'];
		$this->data['get_jenis_jabatan'] = $data['jenis_jabatan'];

		if($data['kd_subunitkerja'] != NULL){
			$this->data['jabatan'] = $this->jabatan_model->ambil_jabatan_by_subunitkerja($data['kd_subunitkerja']);
		}else if($data['kd_unitkerja'] != NULL){
			$this->data['jabatan']=$this->jabatan_model->ambil_jabatan_by_unitkerja($data['kd_unitkerja']);
		}else if($data['kd_unitorganisasi'] != NULL){
			$this->data['jabatan']=$this->jabatan_model->ambil_jabatan_by_unitorganisasi($data['kd_unitorganisasi']);
		}else{
			$this->data['jabatan'] = $this->jabatan_model->ambil_jabatan_by_skpd($data['kd_skpd']);	
		}
		
		$this->data['get_jabatan'] = $data['kd_jabatan'];

		return $this->data;
	}
	
	public function index() {
		$nip = null;
		if($this->input->post('submit') || $this->session->userdata('nip')) {
			if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
			{
				redirect('auth/login', 'refresh');
			}
			else
			{	$nip_session = $this->session->userdata('nip');
				$nip = $this->input->post('nip');
				if($nip == $nip_session){
					$nip = $this->input->post('nip');
				}else{
					$option = array(
						'nip'=> $this->input->post('nip')
					);
					$this->session->set_userdata($option);
					$nip = $this->input->post('nip');
				}
			}
		}else{
			$this->data['riwayat']=null;
			$this->data['links']=null;
			$this->template->admin_render('setup/riwayat_jabatan/index', $this->data);
		}
		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
		/* Get all users */
		
		$config = array ();
		$config ["base_url"] = base_url () . "setup/riwayat_jabatan/index";
		$config ["total_rows"] = $this->riwayat_jabatan_model->record_count($nip);
		$config ["per_page"] = 25;
		$config ["uri_segment"] = 4;
		$choice = $config ["total_rows"] / $config ["per_page"];
		$config ["num_links"] = 5;
		
		// config css for pagination
		$config ['full_tag_open'] = '<ul class="pagination">';
		$config ['full_tag_close'] = '</ul>';
		$config ['first_link'] = 'First';
		$config ['last_link'] = 'Last';
		$config ['first_tag_open'] = '<li>';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="prev">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li>';
		$config ['next_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li>';
		$config ['last_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		
		if ($this->uri->segment ( 4 ) == "") {
			$data ['number'] = 0;
		} else {
			$data ['number'] = $this->uri->segment ( 4 );
		}
		
		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
		
		$this->data ['riwayat'] = $this->riwayat_jabatan_model->fetchAll($config ["per_page"], $page,$nip);
		$this->data ['links'] = $this->pagination->create_links ();
		$this->template->admin_render('setup/riwayat_jabatan/index', $this->data);
		
	}
	
	public function result_riwayat_jabatan($id) {
		
		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
		/* Get all users */
		
		$config = array ();
		$config ["base_url"] = base_url () . "setup/riwayatjabatan/index";
		$config ["total_rows"] = $this->riwayat_jabatan_model->record_count($id);
		$config ["per_page"] = 50;
		$config ["uri_segment"] = 5;
		$choice = $config ["total_rows"] / $config ["per_page"];
		$config ["num_links"] = 5;
		
		// config css for pagination
		$config ['full_tag_open'] = '<ul class="pagination">';
		$config ['full_tag_close'] = '</ul>';
		$config ['first_link'] = 'First';
		$config ['last_link'] = 'Last';
		$config ['first_tag_open'] = '<li>';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="prev">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li>';
		$config ['next_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li>';
		$config ['last_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		
		if ($this->uri->segment ( 5 ) == "") {
			$data ['number'] = 0;
		} else {
			$data ['number'] = $this->uri->segment ( 5 );
		}
		
		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 5 )) ? $this->uri->segment ( 5 ) : 0;
		
		$this->data ['riwayat'] = $this->riwayat_jabatan_model->fetchAll($config ["per_page"], $page,$id);
		$this->data ['nip'] = $id;
		$this->data ['nama_pegawai'] = $this->pegawai_model->getName($id);
		$this->data ['links'] = $this->pagination->create_links ();
		$this->template->admin_render('setup/riwayat_jabatan/result_riwayat_jabatan', $this->data);
		
	}
	
	public function add($nip){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'nip'=>$this->input->post('nip'),
						'seq_no'=>$this->riwayat_jabatan_model->generateSeqNo($this->input->post('nip')),
						'jenis_jabatan'=>$this->input->post ('jenis_jabatan'),
						'kd_jabatan'=>$this->lbrinput->setNullIfEmpty($this->input->post('kd_jabatan')),
						'esselon'=>$this->input->post ('esselon'),
						'no_sk'=>trim($this->input->post ('no_sk')),
						'tmt'=>$this->input->post ('tmt'),
						'tmt_jabatan'=>$this->input->post ('tmt_jabatan'),
						'valid_jabatan'=>$this->input->post ('valid_jabatan'),

						'kd_subunitkerja'=>$this->lbrinput->setNullIfEmpty($this->input->post('kd_subunitkerja')),
						'kd_skpd'=>$this->lbrinput->setNullIfEmpty($this->input->post('kd_skpd')),
						'kd_unitorganisasi'=>$this->lbrinput->setNullIfEmpty($this->input->post('kd_unitorganisasi')),
						'kd_unitkerja'=>$this->lbrinput->setNullIfEmpty($this->input->post ('kd_unitkerja')),

						'nama_jabatan'=>$this->lbrinput->setNullIfEmpty($this->input->post('nama_jabatan')),
						'nama_jabatan_hiden'=>$this->lbrinput->setNullIfEmpty($this->input->post ('nama_jabatan_hiden'))
				);

				if($data['jenis_jabatan'] != 'Struktural' && $data['nama_jabatan_hiden'] == NULL){
					unset($data['kd_jabatan']);
					$data['kd_jabatan'] = $this->jabatan_model->generatekd_jabatan();
					// print_r($data);exit;
					$this->jabatan_model->create($data);
				}

				if($data['kd_jabatan'] == NULL && $data['nama_jabatan_hiden'] != NULL){
					unset($data['kd_jabatan']);
					$data['kd_jabatan'] = $data['nama_jabatan_hiden'];
				}else if($data['kd_jabatan'] != NULL && $data['nama_jabatan_hiden'] != NULL){
					if($data['kd_jabatan'] != $data['nama_jabatan_hiden']){
						unset($data['kd_jabatan']);
						$data['kd_jabatan'] = $data['nama_jabatan_hiden'];
					}
				}

				// print_r($data); exit;

				$this->riwayat_jabatan_model->create($data);
				redirect('setup/riwayat_jabatan/result_riwayat_jabatan/'.$this->input->post('nip'),'refresh');
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup/riwayat_jabatan/add/'.$nip, 'refresh');
			}
			
		} else {
			$data = array(
						'nip'=>$nip,
						'nama'=>$this->pegawai_model->getName($nip),
						'seq_no'=>null,
						'jenis_jabatan'=>null,
						'kd_jabatan'=>null,
						'esselon'=>null,
						'no_sk'=>null,
						'tmt'=>null,
						'tmt_jabatan'=>null,
						'valid_jabatan'=>null,
						'kd_subunitkerja'=>null,
						'kd_unitkerja'=>null,
						'kd_skpd'=>null,
						'kd_unitorganisasi'=>null,

						'nama_jabatan'=>null,
						'nama_jabatan_hiden'=>null
			);
			$this->template->admin_render('setup/riwayat_jabatan/form',$this->inputSetting($data));
		}
	}
	
	public function modify($nip,$seq_no) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'nip'=>$this->input->post('nip'),
						'seq_no'=>$this->input->post('seq_no'),
						'jenis_jabatan'=>$this->input->post ('jenis_jabatan'),
						'esselon'=>$this->input->post ('esselon'),
						'no_sk'=>trim($this->input->post ('no_sk')),
						'tmt'=>$this->input->post ('tmt'),
						'tmt_jabatan'=>$this->input->post ('tmt_jabatan'),
						'valid_jabatan'=>$this->input->post ('valid_jabatan'),

						'nama_jabatan'=>$this->lbrinput->setNullIfEmpty($this->input->post ('nama_jabatan')),
						'nama_jabatan_hiden'=>$this->lbrinput->setNullIfEmpty($this->input->post ('nama_jabatan_hiden')),

						'kd_subunitkerja'=>$this->lbrinput->setNullIfEmpty($this->input->post('kd_subunitkerja')),
						'kd_skpd'=>$this->lbrinput->setNullIfEmpty($this->input->post('kd_skpd')),
						'kd_unitorganisasi'=>$this->lbrinput->setNullIfEmpty($this->input->post('kd_unitorganisasi')),
						'kd_unitkerja'=>$this->lbrinput->setNullIfEmpty($this->input->post ('kd_unitkerja')),
						'kd_jabatan'=>$this->lbrinput->setNullIfEmpty($this->input->post ('kd_jabatan')),
				);

				// print_r($data);exit;

				if($data['jenis_jabatan'] != 'Struktural' && $data['nama_jabatan_hiden'] == NULL){
					unset($data['kd_jabatan']);
					$data['kd_jabatan'] = $this->jabatan_model->generatekd_jabatan();
					// print_r($data);exit;
					$this->jabatan_model->create($data);
				}

				if($data['kd_jabatan'] == NULL && $data['nama_jabatan_hiden'] != NULL){
					unset($data['kd_jabatan']);
					$data['kd_jabatan'] = $data['nama_jabatan_hiden'];
				}else if($data['kd_jabatan'] != NULL && $data['nama_jabatan_hiden'] != NULL){
					// if($data['jenis_jabatan'] != 'Struktural' )
					if($data['kd_jabatan'] != $data['nama_jabatan_hiden'] && $data['jenis_jabatan'] != 'Struktural'){
						unset($data['kd_jabatan']);
						$data['kd_jabatan'] = $data['nama_jabatan_hiden'];
					}
				}

				// print_r($data);exit;

				$this->riwayat_jabatan_model->update($data);
				redirect('setup/riwayat_jabatan/result_riwayat_jabatan/'.$nip,'refresh');

			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup/riwayat_jabatan/modify/'.$nip.'/'.$seq_no, 'refresh');
			
			}

		} else {
			$query = $this->riwayat_jabatan_model->fetchById($nip,$seq_no);
			foreach ($query as $row){
				$this->template->admin_render('setup/riwayat_jabatan/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($nip=null,$seq_no=null) {
		$this->riwayat_jabatan_model->delete($nip,$seq_no);
		redirect('setup/riwayat_jabatan/result_riwayat_jabatan/'.$nip,'refresh');
	}
	
	public function find(){
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "setup/riwayat_jabatan/find";
			$config ["total_rows"] = $this->riwayat_jabatan_model->search_count($column,$query);
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 4 ) == "") {
				$data ['number'] = 0;
			} else {
				$data ['number'] = $this->uri->segment ( 4 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
				
			$this->data ['riwayat'] = $this->riwayat_jabatan_model->search($column,$query,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/riwayat_jabatan/index', $this->data);
		}
	}
	
	public function get_namajabatan(){
		if($this->uri->segment(4)=='Struktural'){
			$a = "Struktural";
		} else if($this->uri->segment(4)=='Fungsional%20Tertentu'){
			$a = "Fungsional Tertentu";
		} else if($this->uri->segment(4)=='Fungsional%20Umum'){
			$a = "Fungsional Umum";
		} else{
			
		}
			
		$data ['get_namajabatan']=$this->riwayat_jabatan_model->ambil_nama_jabatan($a);
		$this->load->view('setup/riwayat_jabatan/drop_down_nama_jabatan',$data);
	}

	public function get_auto_namajabatan(){

		$s = $this->input->get('s');
		$jenis_jabatan = $this->input->get('jenis_jabatan');
		$kd_skpd = $this->lbrinput->setNullIfInputLikeNull($this->input->get('kd_skpd'));
		$kd_unitorganisasi = $this->lbrinput->setNullIfInputLikeNull($this->input->get('kd_unitorganisasi'));
		$kd_unitkerja = $this->lbrinput->setNullIfInputLikeNull($this->input->get('kd_unitkerja'));
		$kd_subunitkerja = $this->lbrinput->setNullIfInputLikeNull($this->input->get('kd_subunitkerja'));

		
			
		$datas = $this->riwayat_jabatan_model->get_auto_nama_jabatan($jenis_jabatan,$kd_skpd,$kd_unitorganisasi,$kd_unitkerja,$kd_subunitkerja,$s);

		header('Content-type: application/json');
        $json = json_encode($datas);
        echo $json;
        exit;
	}

	public function get_jabatan_by_skpd(){
		$data ['get_jabatan']=$this->jabatan_model->ambil_jabatan_by_skpd($this->uri->segment(4));
		$this->load->view('setup/riwayat_jabatan/drop_down_nama_jabatan',$data);
	}

	public function get_jabatan_by_unitorganisasi(){
		$data ['get_jabatan']=$this->jabatan_model->ambil_jabatan_by_unitorganisasi($this->uri->segment(4));
		$this->load->view('setup/riwayat_jabatan/drop_down_nama_jabatan',$data);
	}

	public function get_jabatan_by_unitkerja(){
		$data ['get_jabatan']=$this->jabatan_model->ambil_jabatan_by_unitkerja($this->uri->segment(4));
		$this->load->view('setup/riwayat_jabatan/drop_down_nama_jabatan',$data);
	}

	public function get_jabatan_by_subunitkerja(){
		$data ['get_jabatan']=$this->jabatan_model->ambil_jabatan_by_subunitkerja($this->uri->segment(4));
		$this->load->view('setup/riwayat_jabatan/drop_down_nama_jabatan',$data);
	}

	public function get_unitorganisasi(){
		$data ['get_unitorganisasi']=$this->unit_organisasi_model->ambil_unit_organisasi($this->uri->segment(4));
		$this->load->view('setup/jabatan/drop_down_unit_organisasi',$data);
	}

	public function get_unitkerja(){
		$data ['get_unitkerja']=$this->unit_kerja_model->ambil_unit_kerja($this->uri->segment(4));
		$this->load->view('setup/jabatan/drop_down_unit_kerja',$data);
	}

	public function get_subunitkerja(){
		$data ['get_subunitkerja']=$this->sub_unit_kerja_model->ambil_sub_unit_kerja($this->uri->segment(4));
		$this->load->view('setup/jabatan/drop_down_sub_unit_kerja',$data);
	}
	
}
