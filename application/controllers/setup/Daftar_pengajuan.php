<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class daftar_pengajuan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_daftar_pengajuan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/daftar_pengajuan_model' );
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('no_urut','lang:no_urut','max_length[50]');
		$this->form_validation->set_rules('nama_berkas', 'lang:nama_berkas','max_length[2000]');
		$this->form_validation->set_rules('nama_file', 'lang:nama_file','max_length[2000]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['no_urut'] = array(
				'name'  => 'no_urut',
				'id'    => 'no_urut',
				'type'  => 'number',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'No Urut',
				'value' => $data['no_urut'],
		);
		$this->data['nama_berkas'] = array(
				'name'  => 'nama_berkas',
				'id'    => 'nama_berkas',
				'type'  => 'text',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'nama berkas',
				'value' => $data['nama_berkas'],
		);
		$this->data['nama_file'] = array(
				'name'  => 'nama_file',
				'id'    => 'nama_file',
				'type'  => 'text',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'nama file',
				'value' => $data['nama_file'],
		);
		$this->data['kategori'] = array(
			'name'  => 'kategori',
			'id'    => 'kategori',
			'value' => $data['kategori'],
		);
		$this->data['sub_kategori'] = array(
			'name'  => 'sub_kategori',
			'id'    => 'sub_kategori',
			'value' => $data['sub_kategori'],
		);

		$this->data['get_category'] = $data['kategori'];
		$this->data['get_sub_category'] = $data['sub_kategori'];

		return $this->data;
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "setup/daftar_pengajuan/index";
			$config ["total_rows"] = $this->daftar_pengajuan_model->record_count();
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$this->data  ['number'] = 0;
			} else {
				$this->data  ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['daftar_pengajuan'] = $this->daftar_pengajuan_model->fetchAll($config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/daftar_pengajuan/index', $this->data);
		}
	}
	
	public function add(){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'no_urut'=>trim($this->input->post ('no_urut')),
						'nama_berkas'=>trim($this->input->post ('nama_berkas')),
						'nama_file'=>trim($this->input->post ('nama_file')),
						'kategori'=>trim($this->input->post ('kategori')),
						'sub_kategori'=>trim($this->input->post ('sub_kategori')),
				);
				$this->daftar_pengajuan_model->create($data);
				redirect('setup/daftar_pengajuan','refresh');
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup/daftar_pengajuan/add', 'refresh');
			}
			
		} else {
			$data = array(
					'no_urut'=>null,
					'nama_berkas'=>null,
					'nama_file'=>null,
					'kategori'=>null,
					'sub_kategori'=>null,
			);
			$this->template->admin_render('setup/daftar_pengajuan/form',$this->inputSetting($data));
		}
	}
	
	public function modify($id=null) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'id_daftar'=>$id,
					'no_urut'=>trim($this->input->post('no_urut')),
					'nama_berkas'=>trim($this->input->post ('nama_berkas')),
					'nama_file'=>trim($this->input->post ('nama_file')),
					'kategori'=>trim($this->input->post ('kategori')),
					'sub_kategori'=>trim($this->input->post ('sub_kategori')),
				);
			}
			$this->daftar_pengajuan_model->update($data);
			
			if($this->uri->segment(5)){
				redirect('setup/daftar_pengajuan/index/'.$this->uri->segment(5),'refresh');
			} else {
				redirect('setup/daftar_pengajuan','refresh');
			}
			
		} else {
			$query = $this->daftar_pengajuan_model->fetchById($id);
			foreach ($query as $row){
				$this->template->admin_render('setup/daftar_pengajuan/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($id=null) {
		$this->daftar_pengajuan_model->delete($id);
		redirect ('setup/daftar_pengajuan/index','refresh');
	}
	
	public function find(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$column = $this->input->post('column');
				$query = trim($this->input->post('data'));
				
				$option = array(
					'user_column'=>$column,
					'user_data'=>$query
				);
				$this->session->set_userdata($option);
			}else{
			   $query = trim($this->uri->segment ( 4 ));
			   $column = $this->uri->segment ( 5 );
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "setup/daftar_pengajuan/find/".$query."/".$column;
			$config ["total_rows"] = $this->daftar_pengajuan_model->search_count($column,$query);
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 6;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 6 ) == "") {
				$this->data  ['number'] = 0;
			} else {
				$this->data  ['number'] = $this->uri->segment ( 6 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
				
			$this->data ['daftar_pengajuan'] = $this->daftar_pengajuan_model->search($column,$query,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/daftar_pengajuan/index', $this->data);
		}
	}
	
	public function uploadImage(){
		
	}
	
}