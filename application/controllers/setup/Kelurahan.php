<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class kelurahan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_kelurahan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/kelurahan_model' );
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('kel_id','lang:kelurahan_id','max_length[50]');
		$this->form_validation->set_rules('kec_id','lang:kecamatan_id','max_length[50]');
		$this->form_validation->set_rules('kel_nama', 'lang:kelurahan_nama','max_length[2000]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['kel_id'] = array(
				'name'  => 'kel_id',
				'id'    => 'kel_id',
				'type'  => 'hidden',
				'class' => 'form-control',
				'placeholder'=>'kode kelurahan',
				'value' => $data['kel_id'],
		);
		$this->data['kel_nama'] = array(
				'name'  => 'kel_nama',
				'id'    => 'kel_nama',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama kelurahan',
				'value' => $data['kel_nama'],
		); 
		$this->data['kec_nama'] = $this->kelurahan_model->get_kecamatan();
		$this->data['kec_id2'] = $this->kelurahan_model->get_kecamatan_id( $data['kec_id']);
		return $this->data;
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "setup/kelurahan/index";
			$config ["total_rows"] = $this->kelurahan_model->record_count ();
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$data ['number'] = 0;
			} else {
				$data ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['kelurahan'] = $this->kelurahan_model->fetchAll($config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/kelurahan/index', $this->data);
		}
	}
	
	public function add(){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'kel_id'=>$this->kelurahan_model->generatekd_kel(),
						'kec_id'=>$this->input->post('kec_nama'),
						'kel_nama'=>$this->input->post ('kel_nama')
				);
				$this->kelurahan_model->create($data);
				redirect('setup/kelurahan','refresh');
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup/kelurahan/add', 'refresh');
			}
			
		} else {
			$data = array(
					'kel_id'=>null,
					'kec_id'=>null,
					'kel_nama'=>null
			);
			$this->template->admin_render('setup/kelurahan/form',$this->inputSetting($data));
		}
	}
	
	public function modify($id=null) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'kel_id'=>$this->input->post('kel_id'),
					'kec_id'=>$this->input->post('kec_nama'),
					'kec_id2'=>$this->input->post('kec_id'),
					'kel_nama'=>$this->input->post ('kel_nama')
				);
			}
			$this->kelurahan_model->update($data);
			redirect('setup/kelurahan','refresh');
		} else {
			$query = $this->kelurahan_model->fetchById($id);
			foreach ($query as $row){
				$this->template->admin_render('setup/kelurahan/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($id=null) {
		$this->kelurahan_model->delete($id);
		redirect ('setup/kelurahan/index','refresh');
	}
	
	public function find(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$column = $this->input->post('column');
				$query = $this->input->post('data');
				
				$option = array(
					'user_column'=>$column,
					'user_data'=>$query
				);
				$this->session->set_userdata($option);
			}else{
				$query = $this->uri->segment ( 4 );
				$column = $this->uri->segment ( 5 );
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "setup/kelurahan/find/".$query."/".$column;
			$config ["total_rows"] = $this->kelurahan_model->search_count($column,$query);
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 6;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 6 ) == "") {
				$data ['number'] = 0;
			} else {
				$data ['number'] = $this->uri->segment ( 6 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
		
			$this->data ['kelurahan'] = $this->kelurahan_model->search($column,$query,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/kelurahan/index', $this->data);
		}
	}
	
	public function uploadImage(){
		
	}
	
}