<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_count_record($table)
    {
        $query = $this->db->count_all($table);

        return $query;
    }


    public function disk_totalspace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_total_space($dir);
    }


    public function disk_freespace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_free_space($dir);
    }


    public function disk_usespace($dir = DIRECTORY_SEPARATOR)
    {
        return $this->disk_totalspace($dir) - $this->disk_freespace($dir);
    }


    public function disk_freepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_freespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function disk_usepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_usespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function memory_usage()
    {
        return memory_get_usage();
    }


    public function memory_peak_usage($real = TRUE)
    {
        if ($real)
        {
            return memory_get_peak_usage(TRUE);
        }
        else
        {
            return memory_get_peak_usage(FALSE);
        }
    }


    public function memory_usepercent($real = TRUE, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->memory_usage() * 100) / $this->memory_peak_usage($real), 0).$unit;
    }
	
	public function jml_pegawai() {
		return $this->db->count_all("pegawai_tbl");
	}
	
	public function jml_skpd() {
		return $this->db->count_all("skpd_tbl");
	}
	
	public function jml_naik_pangkat() {
		return $this->db->count_all("pegawai_tbl p, riwayat_kepangkatan_tbl b 
										WHERE b.tmt_pangkat IN(
										SELECT MAX(tes.tmt_pangkat)
										FROM riwayat_kepangkatan_tbl tes
										where tes.nip=b.nip)
										and p.nip = b.nip
										and YEAR(curdate())-YEAR(tmt_pangkat) > 4 ");
	}
	
	public function jml_pensiun() {
		return $this->db->count_all("pegawai_tbl WHERE YEAR(curdate())-YEAR(tgl_lahir) > 55");
	}
	
	
	public function jml_pegawai_skpd($kd_skpd) {
		return $this->db->count_all("pegawai_tbl where kd_skpd='$kd_skpd'");
	}
	
	public function jml_naik_pangkat_skpd($kd_skpd) {
		return $this->db->count_all("pegawai_tbl p, riwayat_kepangkatan_tbl b 
										WHERE b.tmt_pangkat IN(
										SELECT MAX(tes.tmt_pangkat)
										FROM riwayat_kepangkatan_tbl tes
										where tes.nip=b.nip)
										and p.nip = b.nip
										AND p.kd_skpd = '$kd_skpd'
										and YEAR(curdate())-YEAR(tmt_pangkat) > 4 ");
	}
	
	public function jml_pensiun_skpd($kd_skpd) {
		return $this->db->count_all("pegawai_tbl WHERE kd_skpd='$kd_skpd' AND YEAR(curdate())-YEAR(tgl_lahir) > 55");
	}
	
	public function jumlah_pegawai_golongan() {
			$query = $this->db->query("
   
			select
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'I/a' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `Ia`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'I/b' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `Ib`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'I/c' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `Ic`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'I/d' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `Id`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'II/a' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IIa`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'II/b' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IIb`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'II/c' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IIc`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'II/d' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IId`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'III/a' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IIIa`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'III/b' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IIIb`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'III/c' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IIIc`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'III/d' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IIId`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'IV/a' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IVa`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'IV/b' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IVb`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'IV/c' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IVc`,
			ifnull((select count(*) from riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'IV/d' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					order by a.golongan desc),0) AS `IVd`

			  ");
			   
			  return $query->result();
    }
	
	public function jumlah_pegawai_pendidikan() {
			$query = $this->db->query("
   
			select
			ifnull((select count(*) from riwayat_pendidikan_tbl where jenis_pendidikan = '1'),0) AS `SD`,
			ifnull((select count(*) from riwayat_pendidikan_tbl where jenis_pendidikan = '2'),0) AS `SMP`,
			ifnull((select count(*) from riwayat_pendidikan_tbl where jenis_pendidikan = '3'),0) AS `SMA`,
			ifnull((select count(*) from riwayat_pendidikan_tbl where jenis_pendidikan >= '4' and jenis_pendidikan <= '5'),0) AS `D1`,
			ifnull((select count(*) from riwayat_pendidikan_tbl where jenis_pendidikan = '6'),0) AS `D3`,
			ifnull((select count(*) from riwayat_pendidikan_tbl where jenis_pendidikan = '7'),0) AS `S1`,
			ifnull((select count(*) from riwayat_pendidikan_tbl where jenis_pendidikan = '8'),0) AS `S2`,
			ifnull((select count(*) from riwayat_pendidikan_tbl where jenis_pendidikan = '9'),0) AS `S3`

			  ");
			   
			  return $query->result();
    }
	
}
