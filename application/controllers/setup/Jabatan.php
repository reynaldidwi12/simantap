<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class jabatan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_jabatan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/jabatan_model' );
		$this->load->model ('setup/sub_unit_kerja_model' );
		$this->load->model ('setup/unit_kerja_model' );
		$this->load->model ('setup/unit_organisasi_model' );
		$this->load->model ('setup/skpd_model' );
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('kd_skdp','lang:kd_skdp','max_length[255]');
		$this->form_validation->set_rules('kd_unitorganisasi','lang:kd_unitorganisasi','max_length[255]');
		$this->form_validation->set_rules('kd_unitkerja','lang:kd_unitkerja','max_length[255]');
		$this->form_validation->set_rules('kd_subunitkerja','lang:kd_subunitkerja','max_length[255]');

		$this->form_validation->set_rules('kd_jabatan','lang:kd_jabatan','max_length[50]');
		$this->form_validation->set_rules('nama_jabatan', 'lang:nama_jabatan','max_length[2000]');
		$this->form_validation->set_rules('jenis_jabatan', 'lang:jenis_jabatan','required|max_length[2000]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['kd_jabatan'] = array(
				'name'  => 'kd_jabatan',
				'id'    => 'kd_jabatan',
				'type'  => 'hidden',
				'class' => 'form-control',
				'placeholder'=>'kode jabatan',
				'value' => $data['kd_jabatan'],
		);
		$this->data['nama_jabatan'] = array(
				'name'  => 'nama_jabatan',
				'id'    => 'nama_jabatan',
				'type'  => 'text',
				'class' => 'form-control typeahead',
				'data-provide' => 'typeahead',
				'autocomplete' => 'off',
				'required' => 'required',
				'placeholder'=>'nama jabatan',
				'value' => $data['nama_jabatan'],
		); 

		$this->data['position'] = array(
				'name'  => 'position',
				'id'    => 'position',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'Posisi peta jabatan',
				'value' => $data['position'],
		);

		$this->data['nama_jabatan_hiden'] = array(
				'name'  => 'nama_jabatan_hiden',
				'id'    => 'nama_jabatan_hiden',
				'type'  => 'hidden',
				'placeholder'=>'nama jabatan',
				'value' => $data['kd_jabatan'],
		);

		$this->data['jenis_jabatan'] = array(
				'name'  => 'jenis_jabatan',
				'id'    => 'jenis_jabatan',
				'type'  => 'text',
				'class' => 'form-control',
				'required' => 'required',
				'placeholder'=>'nama jabatan',
				'value' => $data['jenis_jabatan'],
		);
		$this->data['kd_subunitkerja'] = array(
				'name'  => 'kd_subunitkerja',
				'id'    => 'kd_subunitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'kode sub unit kerja',
				'value' => $data['kd_subunitkerja'],
		);

		$this->data['kd_unitkerja'] = array(
				'name'  => 'kd_unitkerja',
				'id'    => 'kd_unitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'kode unit kerja',
				'value' => $data['kd_unitkerja'],
		);

		$this->data['kd_unitorganisasi'] = array(
				'name'  => 'kd_unitorganisasi',
				'id'    => 'kd_unitorganisasi',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama unit organisasi',
				'value' => $data['kd_unitorganisasi'],
		); 

		$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'required' => 'required',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama opd',
				'value' => $data['kd_skpd'],
		); 
		
		$this->data['skpd'] = $this->skpd_model->get_skpd();
		$this->data['get_skpd'] = $data['kd_skpd'];
		$this->data['unitorganisasi'] = $this->unit_organisasi_model->ambil_unit_organisasi($data['kd_skpd']);
		$this->data['get_unitorganisasi'] = $data['kd_unitorganisasi'];
		$this->data['unitkerja'] = $this->unit_kerja_model->ambil_unit_kerja($data['kd_unitorganisasi']);
		$this->data['get_unitkerja'] = $data['kd_unitkerja'];
		$this->data['subunitkerja'] = $this->sub_unit_kerja_model->ambil_sub_unit_kerja($data['kd_unitkerja']);
		$this->data['get_subunitkerja'] = $data['kd_subunitkerja'];

		$this->data['get_jenis_jabatan'] = $data['jenis_jabatan'];
		return $this->data;
	}

	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "setup/jabatan/index";
			$config ["total_rows"] = $this->jabatan_model->record_count ();
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 4 );
			}
			
			

			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['jabatan'] = $this->jabatan_model->fetchAllNew($config ["per_page"], $page);
			$this->data['skpd'] = $this->skpd_model->get_skpd();
			$this->data['kd_skpd'] = NULL;

			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/jabatan/index', $this->data);
		}
	}
	
	public function add(){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'kd_jabatan'=>$this->jabatan_model->generatekd_jabatan(),
						'nama_jabatan'=>trim($this->input->post ('nama_jabatan')),
						'jenis_jabatan'=>trim($this->input->post ('jenis_jabatan')),

						'kd_subunitkerja'=>trim($this->input->post('kd_subunitkerja')),
						'kd_skpd'=>trim($this->input->post('kd_skpd')),
						'kd_unitorganisasi'=>trim($this->input->post('kd_unitorganisasi')),
						'kd_unitkerja'=>trim($this->input->post ('kd_unitkerja')),
						'position'=>trim($this->input->post ('position')),
				);


				if($this->input->post ('nama_jabatan_hiden') != NULL){
					$mymsg_errors = 'Jabatan telah ada dalam sistem.';
					$this->session->set_flashdata('message',$mymsg_errors);
					redirect('setup/jabatan/add', 'refresh');
				}else{
					// print_r($data);exit;
					$this->jabatan_model->create($data);
					redirect('setup/jabatan','refresh');	
				}
				
			}else{
				// print_r($this->input->post());exit;
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup/jabatan/add', 'refresh');
			}
			
		} else {
			$data = array(
					'kd_jabatan'=>null,
					'nama_jabatan'=>null,
					'jenis_jabatan'=>null,
					'kd_subunitkerja'=>null,
					'kd_unitkerja'=>null,
					'kd_skpd'=>null,
					'kd_unitorganisasi'=>null,
					'position'=> null,
			);
			$this->template->admin_render('setup/jabatan/form',$this->inputSetting($data));
		}
	}
	
	public function modify($id=null) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'kd_jabatan'=>$this->input->post('kd_jabatan'),
					'nama_jabatan'=>trim($this->input->post ('nama_jabatan')),
					'jenis_jabatan'=>trim($this->input->post ('jenis_jabatan')),

					'kd_subunitkerja'=>trim($this->input->post('kd_subunitkerja')),
					'kd_skpd'=>trim($this->input->post ('kd_skpd')),
					'kd_unitorganisasi'=>trim($this->input->post('kd_unitorganisasi')),
					'kd_unitkerja'=>trim($this->input->post ('kd_unitkerja')),
					'position'=>trim($this->input->post ('position')),
				);
			}

			// print_r($data);exit;
			$this->jabatan_model->update($data);
			
			if($this->uri->segment(5)){
				redirect('setup/jabatan/index/'.$this->uri->segment(5),'refresh');
			} else {
				redirect('setup/jabatan','refresh');
			}
			
		} else {
			$query = $this->jabatan_model->fetchById($id);
			// print_r($query);exit;
			foreach ($query as $row){
				$this->template->admin_render('setup/jabatan/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($id=null) {
		$this->jabatan_model->delete($id);
		redirect ('setup/jabatan/index','refresh');
	}
	
	public function find(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {

			$kd_skpd = NULL;

			if($this->input->post('submit')){
				$column = 'nama_jabatan';
				$query = trim($this->input->post('data'));

				if(trim($this->input->post('kd_skpd')) != NULL){
					// $column = array('nama_jabatan','kd_skpd');
					$kd_skpd = trim($this->input->post('kd_skpd'));
					$column = array('kd_skpd','nama_jabatan');
					$query = array($kd_skpd,$query);
				}

				$option = array(
					'user_column'=>$column,
					'user_data'=>$query
				);
				$this->session->set_userdata($option);
			}else{
				if(count($this->uri->segment_array()) > 6){
					$kd_skpd = trim($this->uri->segment(4));
					$query = trim($this->uri->segment(6));
					$column = array('kd_skpd','nama_jabatan');
					$query = array($kd_skpd,$query);
				}else{
					if(is_numeric($this->uri->segment(4)) && is_string($this->uri->segment(6))){
						$kd_skpd = trim($this->uri->segment(4));
						$query = trim($this->uri->segment(6));
						$column = array('kd_skpd','nama_jabatan');
						$query = array($kd_skpd,$query);
					}else{
						$column = 'nama_jabatan';
				   		$query = trim($this->uri->segment ( 5 ));

				   		// print_r($query);exit;
			   		}
				}
			   
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			
			if($kd_skpd != NULL){
				$mybase_url = base_url () . "setup/jabatan/find/".$query[0]."/".$column[1]."/".urldecode($query[1]);
			}else{
				$mybase_url = base_url () . "setup/jabatan/find/".$column."/".urldecode($query);
			}
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = $mybase_url;
			$config ["total_rows"] = $this->jabatan_model->search_count_new($column,$query);
			$config ["per_page"] = 25;

			if(count($this->uri->segment_array()) > 6){
				$config ["uri_segment"] = 7;
			}else{
				$config ["uri_segment"] = 6;
			}
			
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 6 ) == "") {
				$this->data ['number'] = 0;
			} else {

				if(count($this->uri->segment_array()) > 6){
					$this->data ['number'] = $this->uri->segment ( 7 );
					// print_r($this->data);exit;
				}else{

					if(is_numeric($this->uri->segment(4)) && is_string($this->uri->segment(6))){
						$this->data ['number'] = 0;
					}else{

				   		$this->data ['number'] = $this->uri->segment ( 6 );	
			   		}
				}
				
			}

			// print_r($config);exit;
				
			$this->pagination->initialize ( $config );

			if(count($this->uri->segment_array()) > 6){
				$page = ($this->uri->segment ( 7 )) ? $this->uri->segment ( 7 ) : 0;
			}else{
				// $page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
				$page = $this->uri->segment ( 6 );
				if(!is_numeric($page)){
					$page =  0;
				}
				// print_r($page);exit;	
			}

			// print_r(array('column'=> $column,'query'=>$query,'page'=>$page));exit;
			
		
			$this->data['skpd'] = $this->skpd_model->get_skpd();

			$this->data['kd_skpd'] = $kd_skpd;

			$this->data ['jabatan'] = $this->jabatan_model->search_new($column,$query,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			// print_r($this->data['links']);exit;
			$this->template->admin_render('setup/jabatan/index', $this->data);
		}
	}

	public function get_unitorganisasi(){
		$data ['get_unitorganisasi']=$this->unit_organisasi_model->ambil_unit_organisasi($this->uri->segment(4));
		$this->load->view('setup/jabatan/drop_down_unit_organisasi',$data);
	}

	public function get_unitkerja(){
		$data ['get_unitkerja']=$this->unit_kerja_model->ambil_unit_kerja($this->uri->segment(4));
		$this->load->view('setup/jabatan/drop_down_unit_kerja',$data);
	}

	public function get_subunitkerja(){
		$data ['get_subunitkerja']=$this->sub_unit_kerja_model->ambil_sub_unit_kerja($this->uri->segment(4));
		$this->load->view('setup/jabatan/drop_down_sub_unit_kerja',$data);
	}
	
	public function uploadImage(){
		
	}

	public function get_auto_namajabatan(){

		$s = $this->input->get('s');
		$jenis_jabatan = $this->input->get('jenis_jabatan');
		$kd_skpd = $this->input->get('kd_skpd');
		$kd_unitorganisasi = $this->input->get('kd_unitorganisasi');
		$kd_unitkerja = $this->input->get('kd_unitkerja');
		$kd_subunitkerja = $this->input->get('kd_subunitkerja');

		
			
		$datas = $this->jabatan_model->get_auto_nama_jabatan($jenis_jabatan,$kd_skpd,$kd_unitorganisasi,$kd_unitkerja,$kd_subunitkerja,$s);

		header('Content-type: application/json');
        $json = json_encode($datas);
        echo $json;
        exit;
	}


	public function get_rekom_position(){

		$kd_skpd = $kd_unitorganisasi = $kd_unitkerja = $kd_subunitkerja = NULL;
		$data = array();

		if($this->input->get('kd_skpd') != NULL){
			$kd_skpd = $this->input->get('kd_skpd');	
			$data['kd_skpd'] = $kd_skpd;
		}
		if($this->input->get('kd_unitorganisasi') != NULL){
			$kd_unitorganisasi = $this->input->get('kd_unitorganisasi');
			$data['kd_unitorganisasi'] = $kd_unitorganisasi;	
		}
		if($this->input->get('kd_unitkerja') != NULL){
			$kd_unitkerja = $this->input->get('kd_unitkerja');	
			$data['kd_unitkerja'] = $kd_unitkerja;	
		}
		if($this->input->get('kd_subunitkerja') != NULL){
			$kd_subunitkerja = $this->input->get('kd_subunitkerja');
			$data['kd_subunitkerja'] = $kd_subunitkerja;		
		}
		// print_r($data);exit;
			
		$position = $this->jabatan_model->get_rekom_position($data);
		$res = array('position'=> $position);

		header('Content-type: application/json');
        $json = json_encode($position);
        echo $json;
        exit;
	}
	
}
