<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class kecamatan extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_kecamatan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/kecamatan_model' );
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('kec_id','lang:kecamatan_id','max_length[50]');
		$this->form_validation->set_rules('kec_nama', 'lang:kecamatan_nama','max_length[2000]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['kec_id'] = array(
				'name'  => 'kec_id',
				'id'    => 'kec_id',
				'type'  => 'hidden',
				'class' => 'form-control',
				'placeholder'=>'kode kecamatan',
				'value' => $data['kec_id'],
		);
		$this->data['kec_nama'] = array(
				'name'  => 'kec_nama',
				'id'    => 'kec_nama',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama kecamatan',
				'value' => $data['kec_nama'],
		); 
		return $this->data;
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "setup/kecamatan/index";
			$config ["total_rows"] = $this->kecamatan_model->record_count ();
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$data ['number'] = 0;
			} else {
				$data ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['kecamatan'] = $this->kecamatan_model->fetchAll($config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/kecamatan/index', $this->data);
		}
	}
	
	public function add(){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
						'kec_id'=>$this->kecamatan_model->generatekd_kec(),
						'kec_nama'=>$this->input->post ('kec_nama')
				);
				$this->kecamatan_model->create($data);
				redirect('setup/kecamatan','refresh');
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup/kecamatan/add', 'refresh');
			}
			
		} else {
			$data = array(
					'kec_id'=>null,
					'kec_nama'=>null
			);
			$this->template->admin_render('setup/kecamatan/form',$this->inputSetting($data));
		}
	}
	
	public function modify($id=null) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'kec_id'=>$this->input->post('kec_id'),
					'kec_nama'=>$this->input->post ('kec_nama')
				);
			}
			$this->kecamatan_model->update($data);
			redirect('setup/kecamatan','refresh');
		} else {
			$query = $this->kecamatan_model->fetchById($id);
			foreach ($query as $row){
				$this->template->admin_render('setup/kecamatan/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($id=null) {
		$this->kecamatan_model->delete($id);
		redirect ('setup/kecamatan/index','refresh');
	}
	
	public function find(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$column = 'kec_nama';
				$query = $this->input->post('data');
				
				$option = array(
					'user_column'=>$column,
					'user_data'=>$query
				);
				$this->session->set_userdata($option);
			}else{
			   $column = 'kec_nama';
			   $query = $this->uri->segment ( 5 );
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "setup/kecamatan/find/".$column.$query;
			$config ["total_rows"] = $this->kecamatan_model->search_count($column,$query);
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 6;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 6 ) == "") {
				$data ['number'] = 0;
			} else {
				$data ['number'] = $this->uri->segment ( 6 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
		
			$this->data ['kecamatan'] = $this->kecamatan_model->search($column,$query,$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/kecamatan/index', $this->data);
		}
	}
	
	public function uploadImage(){
		
	}
	
}