<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class sub_unit_kerja extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_kecamatan'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/sub_unit_kerja_model' );
		$this->load->model ('setup/unit_kerja_model' );
		$this->load->model ('setup/unit_organisasi_model' );
		$this->load->model ('setup/skpd_model' );
		
	}
	
	public function validationData(){

		$this->form_validation->set_rules('kd_subunitkerja','lang:kd_subunitkerja','max_length[300]');
		$this->form_validation->set_rules('kd_unitkerja','lang:kd_unitkerja','required|max_length[50]');
		$this->form_validation->set_rules('kd_unitorganisasi','lang:kd_unitorganisasi','required|max_length[300]');
		$this->form_validation->set_rules('nama_subunitkerja', 'lang:nama_subunitkerja','required|max_length[2000]');
		$this->form_validation->set_rules('kd_skpd', 'lang:kd_skpd','required|max_length[2000]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['kd_subunitkerja'] = array(
				'name'  => 'kd_subunitkerja',
				'id'    => 'kd_subunitkerja',
				'type'  => 'hidden',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'kode sub unit kerja',
				'value' => $data['kd_subunitkerja'],
		);
		$this->data['kd_unitkerja'] = array(
				'name'  => 'kd_unitkerja',
				'id'    => 'kd_unitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'kode unit kerja',
				'value' => $data['kd_unitkerja'],
		);
		$this->data['nama_subunitkerja'] = array(
				'name'  => 'nama_subunitkerja',
				'id'    => 'nama_subunitkerja',
				'type'  => 'text',
				'class' => 'form-control typeahead',
				'data-provide' => 'typeahead',
				'autocomplete' => 'off',
				'required' => 'required',
				'placeholder'=>'nama sub unit kerja',
				'value' => $data['nama_subunitkerja'],
		); 

		$this->data['kd_unitorganisasi'] = array(
				'name'  => 'kd_unitorganisasi',
				'id'    => 'kd_unitorganisasi',
				'type'  => 'text',
				'required' => 'required',
				'class' => 'form-control',
				'placeholder'=>'nama unit organisasi',
				'value' => $data['kd_unitorganisasi'],
		);

		$this->data['kd_hiden'] = array(
				'name'  => 'kd_hiden',
				'id'    => 'kd_hiden',
				'type'  => 'hidden',
				'placeholder'=>'kd_hiden',
				'value' => $data['kd_subunitkerja'],
		); 

		$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'required' => 'required',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama opd',
				'value' => $data['kd_skpd'],
		); 

		$this->data['skpd'] = $this->skpd_model->get_skpd();
		$this->data['get_skpd'] = $data['kd_skpd'];
		$this->data['unitorganisasi'] = $this->unit_organisasi_model->ambil_unit_organisasi($data['kd_skpd']);
		$this->data['get_unitorganisasi'] = $data['kd_unitorganisasi'];
		$this->data['unitkerja'] = $this->unit_kerja_model->ambil_unit_kerja($data['kd_unitorganisasi']);
		$this->data['get_unitkerja'] = $data['kd_unitkerja'];
		return $this->data;
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			$config = array ();
			$config ["base_url"] = base_url () . "setup/sub_unit_kerja/index";
			$config ["total_rows"] = $this->sub_unit_kerja_model->record_count ();
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			$this->data ['sub_unit_kerja'] = $this->sub_unit_kerja_model->fetchAll($config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->data ['total_rows'] = $config['total_rows'];
			$this->template->admin_render('setup/sub_unit_kerja/index', $this->data);
		}
	}
	
	public function add(){
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$nama_subunitkerja = trim($this->input->post ('nama_subunitkerja'));
				$data = array(
						'kd_subunitkerja'=>md5(trim($nama_subunitkerja.time())),
						'nama_subunitkerja'=>trim($nama_subunitkerja),
						'kd_skpd'=>trim($this->input->post ('kd_skpd')),
						'kd_unitorganisasi'=>trim($this->input->post ('kd_unitorganisasi')),
						'kd_unitkerja'=>trim($this->input->post ('kd_unitkerja')),
				);
				
				if($this->input->post ('kd_hiden') != NULL){
					$mymsg_errors = 'Sub unit kerja telah ada dalam sistem.';
					$this->session->set_flashdata('message',$mymsg_errors);
					redirect('setup/sub_unit_kerja/add', 'refresh');
				}else{
					$this->sub_unit_kerja_model->create($data);
					redirect('setup/sub_unit_kerja','refresh');	
				}
			}else{
				$this->session->set_flashdata('message', validation_errors());
				redirect('setup/sub_unit_kerja/add', 'refresh');
			}
			
		} else {
			$data = array(
					'kd_subunitkerja'=>null,
					'kd_unitkerja'=>null,
					'nama_subunitkerja'=>null,
					'kd_skpd'=>null,
					'kd_unitorganisasi'=>null
			);
			$this->template->admin_render('setup/sub_unit_kerja/form',$this->inputSetting($data));
		}
	}
	
	public function modify($id=null) {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'kd_subunitkerja'=>$this->input->post ('kd_subunitkerja'),
					'kd_unitkerja'=>$this->input->post ('kd_unitkerja'),
					'kd_unitorganisasi'=>$this->input->post ('kd_unitorganisasi'),
					'nama_subunitkerja'=>$this->input->post ('nama_subunitkerja'),
					'kd_skpd'=>$this->input->post ('kd_skpd')
				);
			}
			$this->sub_unit_kerja_model->update($data);
			
			if($this->uri->segment(5)){
				redirect('setup/sub_unit_kerja/index/'.$this->uri->segment(5),'refresh');
			} else {
				redirect('setup/sub_unit_kerja','refresh');
			}
			
		} else {
			$query = $this->sub_unit_kerja_model->fetchById($id);
			foreach ($query as $row){
				$this->template->admin_render('setup/sub_unit_kerja/form',$this->inputSetting($row));
			}
		}
	}
	
	public function remove($id=null) {
		$this->sub_unit_kerja_model->delete($id);
		redirect ('setup/sub_unit_kerja/index','refresh');
	}
	
	public function find(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$column = $this->input->post('column');
				$query = $this->input->post('data');
				
				$option = array(
					'user_column'=>$column,
					'user_data'=>$query
				);
				$this->session->set_userdata($option);
			}else{
				$query = $this->uri->segment (4);
				$column = $this->uri->segment (5);
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
			$config = array ();
			$config ["base_url"] = base_url () . "setup/sub_unit_kerja/find/".$query."/".$column;
			$config ["total_rows"] = $this->sub_unit_kerja_model->search_count($column,urldecode($query));
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 6;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 6 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 6 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
		
			$this->data ['sub_unit_kerja'] = $this->sub_unit_kerja_model->search($column,urldecode($query),$config ["per_page"], $page);
			$this->data ['links'] = $this->pagination->create_links ();
			$this->data ['total_rows'] = $config['total_rows'];
			$this->template->admin_render('setup/sub_unit_kerja/index', $this->data);
		}
	}

	public function get_unitorganisasi(){
		$data ['get_unitorganisasi']=$this->unit_organisasi_model->ambil_unit_organisasi($this->uri->segment(4));
		$this->load->view('setup/sub_unit_kerja/drop_down_unit_organisasi',$data);
	}

	public function get_unitkerja(){
		$data ['get_unitkerja']=$this->unit_kerja_model->ambil_unit_kerja($this->uri->segment(4));
		$this->load->view('setup/sub_unit_kerja/drop_down_unit_kerja',$data);
	}
	
	public function uploadImage(){
		
	}


	public function get_auto_nama(){

		$s = $this->input->get('s');
		$kd_skpd = $this->input->get('kd_skpd');
		$kd_unitorganisasi = $this->input->get('kd_unitorganisasi');
		$kd_unitkerja = $this->input->get('kd_unitkerja');
			
		$datas = $this->sub_unit_kerja_model->get_auto_nama(array('kd_skpd'=>$kd_skpd,'kd_unitorganisasi'=>$kd_unitorganisasi,'kd_unitkerja'=>$kd_unitkerja,'keyword'=>$s));

		header('Content-type: application/json');
        $json = json_encode($datas);
        echo $json;
        exit;
	}
	
}