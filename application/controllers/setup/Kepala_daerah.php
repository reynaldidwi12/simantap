<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class kepala_daerah extends Admin_Controller {

	public function __construct() {
		parent::__construct ();
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_kepala_daerah'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/kepala_daerah_model' );
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('qid','lang:qid','max_length[50]');
		$this->form_validation->set_rules('kd_kada','lang:kd_kada','max_length[50]');
		$this->form_validation->set_rules('kosong_satu', 'lang:kosong_satu','max_length[255]');
		$this->form_validation->set_rules('kosong_dua', 'lang:kosong_dua','max_length[255]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['kd_kada'] = array(
				'name'  => 'kd_kada',
				'id'    => 'kd_kada',
				'type'  => 'hidden',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'kode kada',
				'value' => $data['kd_kada'],
		);
		$this->data['kosong_satu'] = array(
				'name'  => 'kosong_satu',
				'id'    => 'kosong_satu',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Walikota',
				'value' => $data['kosong_satu'],
		); 
		$this->data['kosong_dua'] = array(
				'name'  => 'kosong_dua',
				'id'    => 'kosong_dua',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Wakil Walikota',
				'value' => $data['kosong_dua'],
		);
		$this->data['qid'] = array(
				'name'  => 'qid',
				'id'    => 'qid',
				'type'  => 'hidden',
				'class' => 'form-control',
				'required'=> 'required',
				'placeholder'=>'qid',
				'value' => $data['qid'],
		);

		return $this->data;
	}
	
	
	public function index() {
		if($this->input->post('submit')){
			if($this->validationData()==TRUE){
				$data = array(
					'qid' => trim($this->input->post('qid')),
					'kd_kada'=>trim($this->input->post('kd_kada')),
					'kosong_satu'=>trim($this->input->post ('kosong_satu')),
					'kosong_dua'=>trim($this->input->post ('kosong_dua'))
				);
			}
			$this->kepala_daerah_model->update($data);
			
			redirect('setup/kepala_daerah','refresh');
			
		} else {
			$query = $this->kepala_daerah_model->getKepalaDaerah();

			
			foreach ($query as $row){
				$this->template->admin_render('setup/kepala_daerah/form',$this->inputSetting($row));
			}
		}
	}
	
	
}