<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class naik_pangkat extends Admin_Controller {
	
	public function __construct() {
		parent::__construct ();
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		// $this->load->library('m_pdf');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_naik_pangkat'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/naik_pangkat_model' );
		
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'type'  => 'text',
				'class' => 'form-control selectpicker',
				'placeholder'=>'Nama SKPD',
				'value' => $data['kd_skpd'],
		);
		$this->data['kd_unitkerja'] = array(
				'name'  => 'kd_unitkerja',
				'id'    => 'kd_unitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Nama Unit Kerja',
				'value' => $data['kd_unitkerja'],
		);
		
		$this->data['nama_unitkerja'] = $this->pegawai_model->ambil_unit_kerja($data['kd_skpd']);
		$this->data['get_nama_unitkerja'] = $data['kd_unitkerja'];
		$this->data['gambar'] = $data['picture'];

		return $this->data;
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */

			if($this->session->userdata('group_id')=='3') {
				$record_count = $this->naik_pangkat_model->record_count_per_skpd($this->session->userdata("ss_skpd"));
			} else {
				$record_count = $this->naik_pangkat_model->record_count();
			}

			$config = array ();
			$config ["base_url"] = base_url () . "setup/naik_pangkat/index";
			$config ["total_rows"] = $record_count;
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 4;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
			
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
			
			if ($this->uri->segment ( 4 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 4 );
			}
			
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
			
			if($this->session->userdata('group_id')=='3') {
				$this->data ['pegawai'] = $this->naik_pangkat_model->fetchAll_per_skpd($this->session->userdata("ss_skpd"), $config ["per_page"], $page);
			} else {
				$this->data ['pegawai'] = $this->naik_pangkat_model->fetchAll($config ["per_page"], $page);
			}

			$this->data	['nama_skpd'] = $this->naik_pangkat_model->get_skpd_all();
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/naik_pangkat/index', $this->data);
		}
	}
	
	public function find(){
		
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit')){
				$column = 'kd_skpd';
				$query = $this->input->post('nama_skpd');
				
				
				$option = array(
					'user_column'=>$column,
					'user_data'=>$query
				);
				$this->session->set_userdata($option);
			}else{
			   $query = $this->uri->segment ( 4 );
			   $column = $this->uri->segment ( 5 );
			}
			
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
			/* Get all users */
		
			$config = array ();
			$config ["base_url"] = base_url () . "setup/naik_pangkat/find/".$query."/".$column;
			$config ["total_rows"] = $this->naik_pangkat_model->search_count($column,$query);
			$config ["per_page"] = 25;
			$config ["uri_segment"] = 6;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;
				
			// config css for pagination
			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';
				
			if ($this->uri->segment ( 6 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 6 );
			}
				
			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
				
			$this->data ['pegawai'] = $this->naik_pangkat_model->search($column,$query,$config ["per_page"], $page);
			$this->data	['nama_skpd'] = $this->naik_pangkat_model->get_skpd_all();
			$this->data ['links'] = $this->pagination->create_links ();
			$this->template->admin_render('setup/naik_pangkat/index', $this->data);
		}
	}
	
}