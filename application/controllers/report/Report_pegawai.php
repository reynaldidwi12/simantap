<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class report_pegawai extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		
		ini_set('max_execution_time', 10000);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_skpd'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('report/report_pegawai_model' );
		
	}
	
	
	public function add(){
			
			$this->load->library('excel');
			
			//load PHPExcel library
			$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
                  $this->excel->getActiveSheet()->setTitle('Laporan Data Pegawai');

  				//STYLING
  				$styleArray = array(
  					'borders' => array('vertical' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);
					
					//STYLING
  				$styleArray2 = array(
  					'borders' => array('allborders' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);
					
				//STYLING
  				$styleArray3 = array(
  					'borders' => array('bottom' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);
					
				$styleArray4 = array(
  					'borders' => array('right' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);
				
				$styleArray5 = array(
  					'borders' => array('left' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);
				
					
			$no = 7;		
					
			//SET DIMENSI TABEL
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(60);
			  
			//set KOP
			$this->excel->getActiveSheet()->mergeCells('A1:E1');
			$this->excel->getActiveSheet()->setCellValue('A1', 'DATA PEGAWAI NEGERI SIPIL LINGKUP PEMERINTAH KOTA KENDARI ');
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			
			
			
			//SET NO
			$this->excel->getActiveSheet()->setCellValue('A5', 'NO');
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setName('Calibri');
			

			$this->excel->getActiveSheet()->setCellValue('B5', 'NIP');
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setName('Calibri');
			
			$this->excel->getActiveSheet()->setCellValue('C5', 'NAMA');
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setName('Calibri');
			
			$this->excel->getActiveSheet()->setCellValue('D5', 'UNIT KERJA');
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setName('Calibri');
			
			$this->excel->getActiveSheet()->setCellValue('E5', 'NAMA SKPD');
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setName('Calibri');
			
			$no =7;
			$hit =1;
			
			if($this->session->userdata('group_id')<>'3') { 
				$data = $this->report_pegawai_model->cetak();
			} else {
				$data = $this->report_pegawai_model->cetak_per_skpd($this->session->userdata("ss_skpd"));
			}
			
			foreach ($data as $datax) {
				$this->excel->getActiveSheet()->setCellValue('A'.$no, $hit );
				$this->excel->getActiveSheet()->setCellValue('B'.$no, " ".$datax->nip );
				$this->excel->getActiveSheet()->setCellValue('C'.$no, $datax->nama);
				$this->excel->getActiveSheet()->setCellValue('D'.$no, $datax->nama_unitkerja);
				$this->excel->getActiveSheet()->setCellValue('E'.$no, $datax->nama_skpd);
				
				$this->excel->getActiveSheet()->getStyle('A'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('B'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('C'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('D'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('E'.$no)->getFont()->setSize(9);
				
				$no++;
				$hit++;
				
			}
				
			
				$this->excel->getActiveSheet()->getStyle('A5:E'.($no-1))->applyFromArray($styleArray2);
						
			  
					
					
			ob_end_clean();
                  $filename='Data Pegawai.xls'; //save our workbook as this file name
                  header('Content-Type: application/vnd.ms-excel'); //mime type
                  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                  header('Cache-Control: max-age=0'); //no cache

                  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                  //if you want to save it as .XLSX Excel 2007 format
                  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');
          
           //redirect('report/report_all','refresh');	
			
		
		
	}
}
	
	
