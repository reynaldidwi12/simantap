<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Report_all extends Admin_Controller {
	public function __construct() {
		parent::__construct ();

		ini_set('max_execution_time', 10000);

		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');

		/* Title Page :: Common */
		$this->page_title->push(lang('menu_skpd'));
		$this->data['pagetitle'] = $this->page_title->show();

		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model('setup/skpd_model');
		$this->load->model ('setup/prodi_model');
		$this->load->model ('report/report_skpd_model');
		$this->load->model('report/report_all_model');

	}

	public function validationData(){

		$this->form_validation->set_rules('kd_skpd','lang:kd_skpd','max_length[50]');
		$this->form_validation->set_rules('nama', 'lang:pegawai_nama','max_length[2000]');

		return $this->form_validation->run();
	}

	/* Setup Property column */
	public function inputSetting($data){
		$this->data['bulan'] = array(
				'name'  => 'bulan',
				'id'    => 'bulan',
				'type'  => 'file',
				'class' => 'form-control',
		);
		$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Nama SKPD',
				'value' => $data['kd_skpd'],
		);
		$this->data['program_studi'] = array(
				'name'  => 'program_studi',
				'id'    => 'program_studi',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'Program Studi',
				'value' => $data['program_studi'],
		);
		$this->data['nama_skpd'] = $this->report_skpd_model->get_skpd_all();
		$this->data['nama_skpd2'] = $this->report_skpd_model->get_skpd_all_2($data['kd_skpd']);
		$this->data['pendidikan'] = $this->prodi_model->get_prodi();
		$this->data['get_pendidikan'] = $data['program_studi'];
		return $this->data;
	}

	public function index() {

		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{

				$data = array(
						'filename'=>null,
						'nama'=>null,
						'kd_skpd'=>null,
						'program_studi'=>null,
				);
				$this->template->admin_render('report/report_all/form',$this->inputSetting($data));

		}
	}

	public function add(){
		if($this->input->post('submit')){

		$this->load->library('excel');

		$bulan = date('m');

		if($bulan==1) {
			$bulan = "Januari";
		} else if($bulan==2) {
			$bulan = "Februari";
		} else if($bulan==3) {
			$bulan = "Maret";
		} else if($bulan==4) {
			$bulan = "April";
		} else if($bulan==5) {
			$bulan = "Mei";
		} else if($bulan==6) {
			$bulan = "Juni";
		} else if($bulan==7) {
			$bulan = "Juli";
		} else if($bulan==8) {
			$bulan = "Agustus";
		} else if($bulan==9) {
			$bulan = "September";
		} else if($bulan==10) {
			$bulan = "Oktober";
		} else if($bulan==11) {
			$bulan = "November";
		} else if($bulan==12) {
			$bulan = "Desember";
		}

			//load PHPExcel library
  		$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
                  $this->excel->getActiveSheet()->setTitle('Laporan Rekapitulasi Pegawai');

  				//STYLING
  				$styleArray = array(
								'borders' => array('allborders' =>
												array('style' => PHPExcel_Style_Border::BORDER_THIN,
												'color' =>	array('argb' => '0000'),
  							),
  						),

  					);
				$styleCenter = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);

			$no = 7;

			//data
			$this->excel->getActiveSheet()->mergeCells('A1:AQ1');
              $this->excel->getActiveSheet()->setCellValue('A1', 'REKAPITULASI JUMLAH PEGAWAI NEGERI SIPIL');
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('A2:AQ2');
              $this->excel->getActiveSheet()->setCellValue('A2', 'LINGKUP PEMERINTAH KOTA KENDARI');
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('A3:AQ3');
              $this->excel->getActiveSheet()->setCellValue('A3', 'PERIODE '.strtoupper($bulan).' 2016');
              $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(12);

			  //set font
			  $this->excel->getActiveSheet()->getStyle('A5:AW68')->getFont()->setSize(10);

			  //INIT WIDTH
			  $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			  $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
			  $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AF')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AG')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AH')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AI')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AJ')->setWidth(10);
			  $this->excel->getActiveSheet()->getColumnDimension('AK')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AL')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AM')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AN')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AO')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AP')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AQ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AR')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AS')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AT')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AU')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AV')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AW')->setWidth(5);

			  //set column name
			  $this->excel->getActiveSheet()->mergeCells('A5:A6');
              $this->excel->getActiveSheet()->setCellValue('A5', 'No');
			  $this->excel->getActiveSheet()->mergeCells('B5:B6');
			  $this->excel->getActiveSheet()->setCellValue('B5', 'Unit Kerja');
			  $this->excel->getActiveSheet()->mergeCells('C5:C6');
			  $this->excel->getActiveSheet()->setCellValue('C5', 'JML');
			  $this->excel->getActiveSheet()->mergeCells('D5:E5');
			  $this->excel->getActiveSheet()->setCellValue('D5', 'Kelamin');
			  $this->excel->getActiveSheet()->setCellValue('D6', 'L');
			  $this->excel->getActiveSheet()->setCellValue('E6', 'P');
			  $this->excel->getActiveSheet()->mergeCells('F5:M5');
			  $this->excel->getActiveSheet()->setCellValue('F5', 'ESSELON');
			  $this->excel->getActiveSheet()->setCellValue('F6', 'I.a');
			  $this->excel->getActiveSheet()->setCellValue('G6', 'I.b');
			  $this->excel->getActiveSheet()->setCellValue('H6', 'II.a');
			  $this->excel->getActiveSheet()->setCellValue('I6', 'II.b');
			  $this->excel->getActiveSheet()->setCellValue('J6', 'III.a');
			  $this->excel->getActiveSheet()->setCellValue('K6', 'III.b');
			  $this->excel->getActiveSheet()->setCellValue('L6', 'IV.a');
			  $this->excel->getActiveSheet()->setCellValue('M6', 'IV.b');

			  $this->excel->getActiveSheet()->mergeCells('N5:N6');
			  $this->excel->getActiveSheet()->setCellValue('N5', 'JML');

			  $this->excel->getActiveSheet()->mergeCells('O5:T5');
			  $this->excel->getActiveSheet()->setCellValue('O5', 'Golongan IV');
			  $this->excel->getActiveSheet()->setCellValue('O6', 'e');
			  $this->excel->getActiveSheet()->setCellValue('P6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('Q6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('R6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('S6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('T6', 'JML');

			  $this->excel->getActiveSheet()->mergeCells('U5:Y5');
			  $this->excel->getActiveSheet()->setCellValue('U5', 'Golongan III');
			  $this->excel->getActiveSheet()->setCellValue('U6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('V6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('W6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('X6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('Y6', 'JML');

			  $this->excel->getActiveSheet()->mergeCells('Z5:AD5');
			  $this->excel->getActiveSheet()->setCellValue('Z5', 'Golongan II');
			  $this->excel->getActiveSheet()->setCellValue('Z6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('AA6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('AB6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('AC6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('AD6', 'JML');

			  $this->excel->getActiveSheet()->mergeCells('AE5:AI5');
			  $this->excel->getActiveSheet()->setCellValue('AE5', 'Golongan I');
			  $this->excel->getActiveSheet()->setCellValue('AE6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('AF6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('AG6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('AH6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('AI6', 'JML');

			  $this->excel->getActiveSheet()->mergeCells('AJ5:AJ6');
			  $this->excel->getActiveSheet()->setCellValue('AJ5', 'JUMLAH');

			  $this->excel->getActiveSheet()->mergeCells('AK5:AR5');
			  $this->excel->getActiveSheet()->setCellValue('AK5', 'Tingkat Pendidikan');
			  $this->excel->getActiveSheet()->setCellValue('AK6', 'S3');
			  $this->excel->getActiveSheet()->setCellValue('AL6', 'S2');
			  $this->excel->getActiveSheet()->setCellValue('AM6', 'S1');
			  $this->excel->getActiveSheet()->setCellValue('AN6', 'SM');
			  $this->excel->getActiveSheet()->setCellValue('AO6', 'SLTA');
			  $this->excel->getActiveSheet()->setCellValue('AP6', 'SLTP');
			  $this->excel->getActiveSheet()->setCellValue('AQ6', 'SD');
			  $this->excel->getActiveSheet()->setCellValue('AR6', 'JML');

			  $this->excel->getActiveSheet()->mergeCells('AS5:AW5');
			  $this->excel->getActiveSheet()->setCellValue('AS5', 'DIKLAT');
			  $this->excel->getActiveSheet()->setCellValue('AS6', 'I');
			  $this->excel->getActiveSheet()->setCellValue('AT6', 'II');
			  $this->excel->getActiveSheet()->setCellValue('AU6', 'III');
			  $this->excel->getActiveSheet()->setCellValue('AV6', 'IV');
			  $this->excel->getActiveSheet()->setCellValue('AW6', 'JML');

			  $this->excel->getActiveSheet()->setCellValue('A7', '1');
			  $this->excel->getActiveSheet()->setCellValue('B7', '2');
			  $this->excel->getActiveSheet()->setCellValue('C7', '3');
			  $this->excel->getActiveSheet()->setCellValue('D7', '4');
			  $this->excel->getActiveSheet()->setCellValue('E7', '5');
			  $this->excel->getActiveSheet()->setCellValue('F7', '6');
			  $this->excel->getActiveSheet()->setCellValue('G7', '7');
			  $this->excel->getActiveSheet()->setCellValue('H7', '8');
			  $this->excel->getActiveSheet()->setCellValue('I7', '9');
			  $this->excel->getActiveSheet()->setCellValue('J7', '10');
			  $this->excel->getActiveSheet()->setCellValue('K7', '11');
			  $this->excel->getActiveSheet()->setCellValue('L7', '12');
			  $this->excel->getActiveSheet()->setCellValue('M7', '13');
			  $this->excel->getActiveSheet()->setCellValue('N7', '14');
			  $this->excel->getActiveSheet()->setCellValue('O7', '15');
			  $this->excel->getActiveSheet()->setCellValue('P7', '16');
			  $this->excel->getActiveSheet()->setCellValue('Q7', '17');
			  $this->excel->getActiveSheet()->setCellValue('R7', '18');
			  $this->excel->getActiveSheet()->setCellValue('S7', '19');
			  $this->excel->getActiveSheet()->setCellValue('T7', '20');
			  $this->excel->getActiveSheet()->setCellValue('U7', '21');
			  $this->excel->getActiveSheet()->setCellValue('V7', '22');
			  $this->excel->getActiveSheet()->setCellValue('W7', '23');
			  $this->excel->getActiveSheet()->setCellValue('X7', '24');
			  $this->excel->getActiveSheet()->setCellValue('Y7', '25');
			  $this->excel->getActiveSheet()->setCellValue('Z7', '26');
			  $this->excel->getActiveSheet()->setCellValue('AA7', '27');
			  $this->excel->getActiveSheet()->setCellValue('AB7', '28');
			  $this->excel->getActiveSheet()->setCellValue('AC7', '29');
			  $this->excel->getActiveSheet()->setCellValue('AD7', '30');
			  $this->excel->getActiveSheet()->setCellValue('AE7', '31');
			  $this->excel->getActiveSheet()->setCellValue('AF7', '32');
			  $this->excel->getActiveSheet()->setCellValue('AG7', '33');
			  $this->excel->getActiveSheet()->setCellValue('AH7', '34');
			  $this->excel->getActiveSheet()->setCellValue('AI7', '35');
			  $this->excel->getActiveSheet()->setCellValue('AJ7', '36');
			  $this->excel->getActiveSheet()->setCellValue('AK7', '37');
			  $this->excel->getActiveSheet()->setCellValue('AL7', '38');
			  $this->excel->getActiveSheet()->setCellValue('AM7', '39');
			  $this->excel->getActiveSheet()->setCellValue('AN7', '40');
			  $this->excel->getActiveSheet()->setCellValue('AO7', '41');
			  $this->excel->getActiveSheet()->setCellValue('AP7', '42');
			  $this->excel->getActiveSheet()->setCellValue('AQ7', '43');
			  $this->excel->getActiveSheet()->setCellValue('AR7', '44');
			  $this->excel->getActiveSheet()->setCellValue('AS7', '45');
			  $this->excel->getActiveSheet()->setCellValue('AT7', '46');
			  $this->excel->getActiveSheet()->setCellValue('AU7', '47');
			  $this->excel->getActiveSheet()->setCellValue('AV7', '48');
			  $this->excel->getActiveSheet()->setCellValue('AW7', '49');

			  //INIT NUMBERING
			  $this->excel->getActiveSheet()->setCellValue('A8', 'I');
			  $this->excel->getActiveSheet()->setCellValue('A9', 'II');
			  $this->excel->getActiveSheet()->setCellValue('A10', 'III');
			  $this->excel->getActiveSheet()->setCellValue('A11', 'IV');
			  $this->excel->getActiveSheet()->setCellValue('A12', 'V');
			  $this->excel->getActiveSheet()->setCellValue('A13', '1');
			  $this->excel->getActiveSheet()->setCellValue('A14', '2');
			  $this->excel->getActiveSheet()->setCellValue('A15', '3');
			  $this->excel->getActiveSheet()->setCellValue('A16', '4');
			  $this->excel->getActiveSheet()->setCellValue('A17', '5');
			  $this->excel->getActiveSheet()->setCellValue('A18', '6');
			  $this->excel->getActiveSheet()->setCellValue('A19', '7');
			  $this->excel->getActiveSheet()->setCellValue('A20', '8');
			  $this->excel->getActiveSheet()->setCellValue('A21', '9');
			  $this->excel->getActiveSheet()->setCellValue('A22', '10');
			  $this->excel->getActiveSheet()->setCellValue('A23', '11');
			  $this->excel->getActiveSheet()->setCellValue('A24', '12');
			  $this->excel->getActiveSheet()->setCellValue('A25', '13');
			  $this->excel->getActiveSheet()->setCellValue('A26', '14');
			  $this->excel->getActiveSheet()->setCellValue('A27', '15');
			  $this->excel->getActiveSheet()->setCellValue('A28', '16');
			  $this->excel->getActiveSheet()->setCellValue('A29', '17');
			  $this->excel->getActiveSheet()->setCellValue('A30', '18');
			  $this->excel->getActiveSheet()->setCellValue('A31', '19');
			  $this->excel->getActiveSheet()->setCellValue('A32', '20');
			  $this->excel->getActiveSheet()->setCellValue('A33', 'VI');
			  $this->excel->getActiveSheet()->setCellValue('A34', '1');
			  $this->excel->getActiveSheet()->setCellValue('A35', '2');
			  $this->excel->getActiveSheet()->setCellValue('A36', '3');
			  $this->excel->getActiveSheet()->setCellValue('A37', '4');
			  $this->excel->getActiveSheet()->setCellValue('A38', '5');
			  $this->excel->getActiveSheet()->setCellValue('A39', '6');
			  $this->excel->getActiveSheet()->setCellValue('A40', '7');
			  $this->excel->getActiveSheet()->setCellValue('A41', '8');
			  $this->excel->getActiveSheet()->setCellValue('A42', 'VII');
			  $this->excel->getActiveSheet()->setCellValue('A43', 'VIII');
			  $this->excel->getActiveSheet()->setCellValue('A44', 'IX');
			  $this->excel->getActiveSheet()->setCellValue('A45', 'X');
			  $this->excel->getActiveSheet()->setCellValue('A46', 'XI');
			  $this->excel->getActiveSheet()->setCellValue('A47', 'XII');
			  $this->excel->getActiveSheet()->setCellValue('A48', 'XIII');
			  $this->excel->getActiveSheet()->setCellValue('A49', 'XIV');
			  $this->excel->getActiveSheet()->setCellValue('A50', 'XV');
			  $this->excel->getActiveSheet()->setCellValue('A51', 'XVI');
			  $this->excel->getActiveSheet()->setCellValue('A52', 'XVII');
			  $this->excel->getActiveSheet()->setCellValue('A53', 'XVIII');
			  $this->excel->getActiveSheet()->setCellValue('A54', 'XIX');
			  $this->excel->getActiveSheet()->setCellValue('A55', 'XX');
			  $this->excel->getActiveSheet()->setCellValue('A56', 'XXI');
			  $this->excel->getActiveSheet()->setCellValue('A57', 'XXII');
			  $this->excel->getActiveSheet()->setCellValue('A58', 'XXIII');

			  //INIT UNIT KERJA
			  $this->excel->getActiveSheet()->setCellValue('B8', 'SEKRETARIAT DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B9', 'SEKRETARIAT DPRD');
			  $this->excel->getActiveSheet()->setCellValue('B10', 'SEKRETARIAT KPU');
			  $this->excel->getActiveSheet()->setCellValue('B11', 'SEKRETARIAT DEWAN PENGURUS KORPRI');
			  $this->excel->getActiveSheet()->setCellValue('B12', 'DINAS');
			  $this->excel->getActiveSheet()->setCellValue('B13', 'DINAS PENDIDIKAN, KEPEMUDAAN DAN OLAHRAGA');
			  $this->excel->getActiveSheet()->setCellValue('B14', 'DINAS SOSIAL');
			  $this->excel->getActiveSheet()->setCellValue('B15', 'DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL');
			  $this->excel->getActiveSheet()->setCellValue('B16', 'DINAS PEKERJAAN UMUM DAN PENATAAN RUANG');
			  $this->excel->getActiveSheet()->setCellValue('B17', 'DINAS PERHUBUNGAN');
			  $this->excel->getActiveSheet()->setCellValue('B18', 'DINAS KESEHATAN');
			  $this->excel->getActiveSheet()->setCellValue('B19', 'DINAS PERUMAHAN, KAWASAN PERMUKIMAN DAN PERTANAHAN');
			  $this->excel->getActiveSheet()->setCellValue('B20', 'DINAS PERTANIAN');
			  $this->excel->getActiveSheet()->setCellValue('B21', 'DINAS KELAUTAN DAN PERIKANAN');
			  $this->excel->getActiveSheet()->setCellValue('B22', 'DINAS PERDAGANGAN, KOPERASI DAN UMKM');
			  $this->excel->getActiveSheet()->setCellValue('B23', 'DINAS KEBUDAYAAN DAN PARIWISATA');
			  $this->excel->getActiveSheet()->setCellValue('B24', 'DINAS KOMUNIKASI DAN INFORMATIKA');
			  $this->excel->getActiveSheet()->setCellValue('B25', 'DINAS PENGENDALIAN PENDUDUK DAN KELUARGA BERENCANA');
			  $this->excel->getActiveSheet()->setCellValue('B26', 'DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU');
			  $this->excel->getActiveSheet()->setCellValue('B27', 'DINAS LINGKUNGAN HIDUP DAN KEHUTANAN');
			  $this->excel->getActiveSheet()->setCellValue('B28', 'DINAS KEBAKARAN');
			  $this->excel->getActiveSheet()->setCellValue('B29', 'DINAS PERPUSTAKAAN DAN KEARSIPAN');
			  $this->excel->getActiveSheet()->setCellValue('B30', 'DINAS PANGAN');
			  $this->excel->getActiveSheet()->setCellValue('B31', 'DINAS TENAGA KERJA DAN PERINDUSTRIAN');
			  $this->excel->getActiveSheet()->setCellValue('B32', 'DINAS PEMBERDAYAAN PEREMPUAN DAN PERLINDUNGAN ANAK');
			  $this->excel->getActiveSheet()->setCellValue('B33', 'BADAN');
			  $this->excel->getActiveSheet()->setCellValue('B34', 'INSPEKTORAT DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B35', 'BADAN PERENCANAAN PEMBANGUNAN DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B36', 'BADAN KESATUAN BANGSA DAN POLITIK');
			  $this->excel->getActiveSheet()->setCellValue('B37', 'BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA');
			  $this->excel->getActiveSheet()->setCellValue('B38', 'BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B39', 'BADAN PENANGGULANGAN BENCANA DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B40', 'SATUAN POLISI PAMONG PRAJA');
			  $this->excel->getActiveSheet()->setCellValue('B41', 'BADAN PENGELOLA PAJAK DAN RETRIBUSI DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B42', 'RUMAH SAKIT UMUM DAERAH KOTA KENDARI');
			  $this->excel->getActiveSheet()->setCellValue('B43', 'PUSKESMAS');
			  $this->excel->getActiveSheet()->setCellValue('B44', 'KECAMATAN & KELURAHAN');
			  $this->excel->getActiveSheet()->setCellValue('B45', 'PENGAWAS DIKNAS');
			  $this->excel->getActiveSheet()->setCellValue('B46', 'UPTD DINAS PERTANIAN');
			  $this->excel->getActiveSheet()->setCellValue('B47', 'UPTD DINAS KELAUTAN & PERIKANAN');
			  $this->excel->getActiveSheet()->setCellValue('B48', 'UPTD DIKNAS');
			  $this->excel->getActiveSheet()->setCellValue('B49', 'UPTD PERHUBUNGAN');
			  $this->excel->getActiveSheet()->setCellValue('B50', 'UPTD CAPIL');
			  $this->excel->getActiveSheet()->setCellValue('B51', 'UPTD RUSUNAWA');
			  $this->excel->getActiveSheet()->setCellValue('B52', 'UPTD BADAN KB DAN PP');
			  $this->excel->getActiveSheet()->setCellValue('B53', 'UPTD KESEHATAN');
			  $this->excel->getActiveSheet()->setCellValue('B54', 'UPTD LINGKUNGAN HIDUP');
			  $this->excel->getActiveSheet()->setCellValue('B55', 'UPTD DINAS PENDAPATAN');
			  $this->excel->getActiveSheet()->setCellValue('B56', 'TK');
			  $this->excel->getActiveSheet()->setCellValue('B57', 'SD');
			  $this->excel->getActiveSheet()->setCellValue('B58', 'SLTP');

			  $urutan = array('18', '16', '109', '21', '0', '1', '13', '11', '4', '8', '2', '6', '104', '107', '108', '106',
				'112', '111', '115', '9', '5', '103', '101', '113', '12', '0', '22', '7', '14', '24', '99', '100', '15', '20', '3',
				'1111', '11111', '170', '104a', '107a', '1112', '8a', '11a', '6a', '111a', '2a', '9a', '20a',
				'11112', '11113', '1113');
			  $length = count($urutan);
			  $total_jml = 0;$total_pria = 0;$total_wanita = 0;$total_e1a = 0;$total_e1b = 0;
			  $total_e2a = 0;$total_e2b = 0;$total_e3a = 0;$total_e3b = 0;$total_e4a = 0;
			  $total_e4b = 0;$total_eselon = 0;$total_g4e = 0;$total_g4d = 0;$total_g4c = 0;
			  $total_g4b = 0;$total_g4a = 0;$total_g4 = 0;$total_g3d = 0;$total_g3c = 0;
			  $total_g3b = 0;$total_g3a = 0;$total_g3 = 0;$total_g2d = 0;$total_g2c = 0;
			  $total_g2b = 0;$total_g2a = 0;$total_g2 = 0;$total_g1d = 0;$total_g1c = 0;
			  $total_g1b = 0;$total_g1a = 0;$total_g1 = 0;$total_g = 0;$total_s3 = 0;
			  $total_s2 = 0;$total_s1 = 0;$total_sma = 0;$total_smp = 0;$total_sd = 0;
			  $total_sekolah = 0;$total_diklat1 = 0;$total_diklat2 = 0;
			  $total_diklat3 = 0;$total_diklat4 = 0;$total_diklat = 0;

				//1 diknas sendiri
				$diknas = array('');

			  //104 Pertanian
			  $dinaspertanian = array(343,344,345);

			  // 107 dinas perikanan
			  $dinasperikanan = array(324,325,326);

			  //11 capil
			  $capil = array(191);

			  //111 kb
			  $kb = array(327,328,329,330,331,332,333,334,335,336,337);

			  //12 dinaspemberdayaan perempuan
			  $dpppa = array(192);

			  //18 sekda
			  // $sekda = array(295,296,297,298,299,300,301,302,303,304,308,309,310,312,313,314,315,316,317,318,
			  // 319,320,321,322,323);

				$sekda = array('', 298, 322, 321, 320, 319, 316, 315, 314, 313, 312, 310, 309, 308, 299, 323);

			  //2 dinkes
			  $dinkes = array(171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,357,358);
			  //puskesmas
			  //$puskesmas = array(171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186);

			  $uptdkesehatan = array(186,358);

			  //20 pajak
			  $pajak = array(194,195,196,197,198,199,200,201,202,203,346,347,348,349,350,351,352,
			  353,354,355,356,427);

			  //6 perumahan
			  $perumahan = array(190);

			  //8 perhubungan
			  $perhubungan = array(189,339,340,341,342);

			  //9 blh
			  $blh = array(187, 338);

				//skpd gabung
				$puskesmas = array(126,127,128,129,130,131,132,133, 134, 135,136,137,138,139,140);
				$uptddiknas = array(116,117,118,119,120,121,122,123,124,125);
				$kec = array(25,26,27,28,29,30,31,32,33,34,114);
				$kel = array(359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,
				377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,
				398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,418,419,420,422,423,424);
				$smp = array(141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160);
				$tk = array(121,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,
			237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,
			266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,
			305,307,425,430,431,433,434,435,436,437,440,441,442,446,448);

			//pengawas diknas
			$pengawasdiknas = array(170);
				$sd_skpd=array(1, 166, 117, 118, 119, 120, 121, 122, 123, 124, 125);
				$sd_unitkerja=array(1,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,
														65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,102,103,104,
														105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,
														129,130,131,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,
														155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,270,311,426,429);

			  for($i=0;$i<$length;$i++) {
						//echo $urutan[$i];
					 	$id = $urutan[$i];
					 	if($id == 0) {
							$no = $no + 1;
					 	} else {
							//get unit kerja
							$temp_jml = 0;$temp_pria = 0;$temp_wanita = 0;$temp_e1a = 0;$temp_e1b = 0;$temp_e2a = 0;
							$temp_e2b = 0;$temp_e3a = 0;$temp_e3b = 0;$temp_e4a = 0;$temp_e4b = 0;$temp_eselon = 0;
							$temp_g4e = 0;$temp_g4d = 0;$temp_g4c = 0;$temp_g4b = 0;$temp_g4a = 0;$temp_g4 = 0;
							$temp_g3d = 0;$temp_g3c = 0;$temp_g3b = 0;$temp_g3a = 0;$temp_g3 = 0;$temp_g2d = 0;
							$temp_g2c = 0;$temp_g2b = 0;$temp_g2a = 0;$temp_g2 = 0;$temp_g1d = 0;$temp_g1c = 0;
							$temp_g1b = 0;$temp_g1a = 0;$temp_g1 = 0;$temp_g = 0;$temp_s3 = 0;$temp_s2 = 0;
							$temp_s1 = 0;$temp_sma = 0;$temp_smp = 0;$temp_sd = 0;$temp_sekolah = 0;$temp_diklat1 = 0;
							$temp_diklat2 = 0;$temp_diklat3 = 0;$temp_diklat4 = 0;$temp_diklat = 0;

							if($id == 18) {
								 //sekertariat
								  $datas = $sekda;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($id, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "104a") {
								  $ids=104;
								  $datas = $dinaspertanian;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "107a") {
								  $ids=107;
								  $datas = $dinasperikanan;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "8a") {
								  $ids=8;
								  $datas = $perhubungan;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "11a"){
								  $ids=11;
								  $datas = $capil;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "6a"){
								  $ids=6;
								  $datas = $perumahan;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "111a"){
								  $ids=111;
								  $datas = $kb;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "2a"){
								  $ids=2;
								  $datas = $uptdkesehatan;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "9a"){
								  $ids=9;
								  $datas = $blh;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "20a"){
								  $ids=20;
								  $datas = $pajak;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "1111"){
								  // $ids=20;
								  $datas = $puskesmas;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues($datas[$j], '');
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
							   //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "1112"){
									// $ids=20;
									$datas = $uptddiknas;
									for($j=0;$j<count($datas);$j++) {
									$data = $this->report_all_model->getValues($datas[$j], '');
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
									}
								}

							 //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
								 //show it
								 //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "11111"){
									// $ids=20;
									$datas = $kec;
									for($j=0;$j<count($datas);$j++) {
									$data = $this->report_all_model->getValues($datas[$j], '');
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
									}
									//KELURAHAN
									$ids = $datas[$j];
									$datam = $kel;
									for($k=0;$k<count($datam);$k++) {
									$data = $this->report_all_model->getValues($ids ,$datam[$k]);
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
									}
								}


								}

							 //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
								 //show it
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "1113"){
									// $ids=20;
									$datas = $smp;
									for($j=0;$j<count($datas);$j++) {
									$data = $this->report_all_model->getValues($datas[$j], '');
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
										$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
										$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
										$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
										$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
										$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
										$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
										$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
										$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
										$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
										$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
										$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
										$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
										$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
										$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
										$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
										$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
										$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
										$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
										$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
										$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
										$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
										$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
										$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
										$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
										$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
										$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
										$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
										$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
										$temp_g = $temp_g + $datax->sekertariat_g;
										$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
										$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
										$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
										$temp_sma = $temp_sma + $datax->sekertariat_sma;
										$temp_smp = $temp_smp + $datax->sekertariat_smp;
										$temp_sd = $temp_sd + $datax->sekertariat_sd;
										$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
										$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
										$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
										$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
									}
								}

							 //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
								 //show it
								 //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "11112"){

										$datam = $tk;
										for($k=0;$k<count($datam);$k++) {
										$data = $this->report_all_model->getValuesU($datam[$k]);
										foreach($data as $datax) {
											$temp_jml = $temp_jml + $datax->sekertariat_all;
											$temp_pria = $temp_pria + $datax->sekertariat_pria;
											$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
											$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
											$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
											$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
											$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
											$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
											$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
											$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
											$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
											$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
											$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
											$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
											$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
											$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
											$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
											$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
											$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
											$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
											$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
											$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
											$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
											$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
											$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
											$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
											$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
											$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
											$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
											$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
											$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
											$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
											$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
											$temp_g = $temp_g + $datax->sekertariat_g;
											$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
											$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
											$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
											$temp_sma = $temp_sma + $datax->sekertariat_sma;
											$temp_smp = $temp_smp + $datax->sekertariat_smp;
											$temp_sd = $temp_sd + $datax->sekertariat_sd;
											$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
											$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
											$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
											$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
											$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
											$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
										}
								}


							 //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
								 //show it
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "11112"){

										$datam = $pengawasdiknas;
										for($k=0;$k<count($datam);$k++) {
										$data = $this->report_all_model->getValuesU($datam[$k]);
										foreach($data as $datax) {
											$temp_jml = $temp_jml + $datax->sekertariat_all;
											$temp_pria = $temp_pria + $datax->sekertariat_pria;
											$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
											$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
											$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
											$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
											$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
											$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
											$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
											$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
											$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
											$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
											$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
											$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
											$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
											$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
											$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
											$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
											$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
											$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
											$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
											$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
											$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
											$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
											$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
											$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
											$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
											$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
											$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
											$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
											$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
											$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
											$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
											$temp_g = $temp_g + $datax->sekertariat_g;
											$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
											$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
											$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
											$temp_sma = $temp_sma + $datax->sekertariat_sma;
											$temp_smp = $temp_smp + $datax->sekertariat_smp;
											$temp_sd = $temp_sd + $datax->sekertariat_sd;
											$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
											$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
											$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
											$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
											$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
											$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
										}
								}


							 //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
								 //show it
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);

							}
							else if($id == "11113"){

								$datam = $sd_unitkerja;
								for($k=0;$k<count($datam);$k++) {
									$data = $this->report_all_model->getValuesU($datam[$k]);
									foreach($data as $datax) {
									$temp_jml = $temp_jml + $datax->sekertariat_all;
									$temp_pria = $temp_pria + $datax->sekertariat_pria;
									$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;
									$temp_e1a = $temp_e1a + $datax->sekertariat_e1a;
									$temp_e1b = $temp_e1a + $datax->sekertariat_e1b;
									$temp_e2a = $temp_e2a + $datax->sekertariat_e2a;
									$temp_e2b = $temp_e2a + $datax->sekertariat_e2b;
									$temp_e3a = $temp_e3a + $datax->sekertariat_e3a;
									$temp_e3b = $temp_e3a + $datax->sekertariat_e3b;
									$temp_e4a = $temp_e4a + $datax->sekertariat_e4a;
									$temp_e4b = $temp_e4a + $datax->sekertariat_e4b;
									$temp_eselon = $temp_eselon + $datax->sekertariat_eselon;
									$temp_g4e = $temp_g4e + $datax->sekertariat_g4e;
									$temp_g4d = $temp_g4d + $datax->sekertariat_g4d;
									$temp_g4c = $temp_g4c + $datax->sekertariat_g4c;
									$temp_g4b = $temp_g4b + $datax->sekertariat_g4b;
									$temp_g4a = $temp_g4a + $datax->sekertariat_g4a;
									$temp_g4 = $temp_g4 + $datax->sekertariat_g4;
									$temp_g3d = $temp_g3d + $datax->sekertariat_g3d;
									$temp_g3c = $temp_g3c + $datax->sekertariat_g3c;
									$temp_g3b = $temp_g3b + $datax->sekertariat_g3b;
									$temp_g3a = $temp_g3a + $datax->sekertariat_g3a;
									$temp_g3 = $temp_g3 + $datax->sekertariat_g3;
									$temp_g2d = $temp_g2d + $datax->sekertariat_g2d;
									$temp_g2c = $temp_g2c + $datax->sekertariat_g2c;
									$temp_g2b = $temp_g2b + $datax->sekertariat_g2b;
									$temp_g2a = $temp_g2a + $datax->sekertariat_g2a;
									$temp_g2 = $temp_g2 + $datax->sekertariat_g2;
									$temp_g1d = $temp_g1d + $datax->sekertariat_g1d;
									$temp_g1c = $temp_g1c + $datax->sekertariat_g1c;
									$temp_g1b = $temp_g1b + $datax->sekertariat_g1b;
									$temp_g1a = $temp_g1a + $datax->sekertariat_g1a;
									$temp_g1 = $temp_g1 + $datax->sekertariat_g1;
									$temp_g = $temp_g + $datax->sekertariat_g;
									$temp_s3 = $temp_s3 + $datax->sekertariat_s3;
									$temp_s2 = $temp_s2 + $datax->sekertariat_s2;
									$temp_s1 = $temp_s1 + $datax->sekertariat_s1;
									$temp_sma = $temp_sma + $datax->sekertariat_sma;
									$temp_smp = $temp_smp + $datax->sekertariat_smp;
									$temp_sd = $temp_sd + $datax->sekertariat_sd;
									$temp_sekolah = $temp_sekolah + $datax->sekertariat_sekolah;
									$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1;
									$temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
									$temp_diklat3 = $temp_diklat3 + $datax->sekretariat_diklat3;
									$temp_diklat4 = $temp_diklat4 + $datax->sekretariat_diklat4;
									$temp_diklat = $temp_diklat + $datax->sekretariat_diklat;
								}
								}

							 //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;
								$total_e1a = $total_e1a + $temp_e1a;
								$total_e1b = $total_e1b + $temp_e1b;
								$total_e2a = $total_e2a + $temp_e2a;
								$total_e2b = $total_e2b + $temp_e2b;
								$total_e3a = $total_e3a + $temp_e3a;
								$total_e3b = $total_e3b + $temp_e3b;
								$total_e4a = $total_e4a + $temp_e4a;
								$total_e4b = $total_e4b + $$temp_e4a;
								$total_eselon = $total_eselon + $temp_eselon;
								$total_g4e = $total_g4e + $temp_g4e;
								$total_g4d = $total_g4d + $temp_g4d;
								$total_g4c = $total_g4c + $temp_g4c;
								$total_g4b = $total_g4b + $temp_g4b;
								$total_g4a = $total_g4a + $temp_g4a;
								$total_g4 = $total_g4 + $temp_g4;
								$total_g3d = $total_g3d + $temp_g3d;
								$total_g3c = $total_g3c + $temp_g3c;
								$total_g3b = $total_g3b + $temp_g3b;
								$total_g3a = $total_g3a + $temp_g3a;
								$total_g3 = $total_g3 + $temp_g3;
								$total_g2d = $total_g2d + $temp_g2d;
								$total_g2c = $total_g2c + $temp_g2c;
								$total_g2b = $total_g2b + $temp_g2b;
								$total_g2a = $total_g2a + $temp_g2a;
								$total_g2 = $total_g2 + $temp_g2;
								$total_g1d = $total_g1d + $temp_g1d;
								$total_g1c = $total_g1c + $temp_g1c;
								$total_g1b = $total_g1b + $temp_g1b;
								$total_g1a = $total_g1a + $temp_g1a;
								$total_g1 = $total_g1 + $temp_g1;
								$total_g = $total_g + $temp_g;
								$total_s3 = $total_s3 + $temp_s3;
								$total_s2 = $total_s2 + $temp_s2 ;
								$total_s1 = $total_s1 + $temp_s1;
								$total_sma = $total_sma + $temp_sma;
								$total_smp = $total_smp + $temp_smp;
								$total_sd = $total_sd + $temp_sd;
								$total_sekolah = $total_sekolah + $temp_sekolah;
								$total_diklat1 = $total_diklat1 + $temp_diklat1;
								$total_diklat2 = $total_diklat2 + $temp_diklat2;
								$total_diklat3 = $total_diklat3 + $temp_diklat3;
								$total_diklat4 = $total_diklat4 + $temp_diklat4;
								$total_diklat = $total_diklat + $temp_diklat;
								 //show it
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_diklat);
							}

							else {
								$data = $this->report_all_model->getValues($id, '');
								foreach ($data as $datax) {
									$no++;
									$total_jml = $total_jml + $datax->sekertariat_all;
									$total_pria = $total_pria + $datax->sekertariat_pria;
									$total_wanita = $total_wanita + $datax->sekertariat_wanita;
									$total_e1a = $total_e1a + $datax->sekertariat_e1a;
									$total_e1b = $total_e1a + $datax->sekertariat_e1b;
									$total_e2a = $total_e2a + $datax->sekertariat_e2a;
									$total_e2b = $total_e2b + $datax->sekertariat_e2b;
									$total_e3a = $total_e3a + $datax->sekertariat_e3a;
									$total_e3b = $total_e3a + $datax->sekertariat_e3b;
									$total_e4a = $total_e4a + $datax->sekertariat_e4a;
									$total_e4b = $total_e4b + $datax->sekertariat_e4b;
									$total_eselon = $total_eselon + $datax->sekertariat_eselon;
									$total_g4e = $total_g4e + $datax->sekertariat_g4e;
									$total_g4d = $total_g4d + $datax->sekertariat_g4d;
									$total_g4c = $total_g4c + $datax->sekertariat_g4c;
									$total_g4b = $total_g4b + $datax->sekertariat_g4b;
									$total_g4a = $total_g4a + $datax->sekertariat_g4a;
									$total_g4 = $total_g4 + $datax->sekertariat_g4;
									$total_g3d = $total_g3d + $datax->sekertariat_g3d;
									$total_g3c = $total_g3c + $datax->sekertariat_g3c;
									$total_g3b = $total_g3b + $datax->sekertariat_g3b;
									$total_g3a = $total_g3a + $datax->sekertariat_g3a;
									$total_g3 = $total_g3 + $datax->sekertariat_g3;
									$total_g2d = $total_g2d + $datax->sekertariat_g2d;
									$total_g2c = $total_g2c + $datax->sekertariat_g2c;
									$total_g2b = $total_g2b + $datax->sekertariat_g2b;
									$total_g2a = $total_g2a + $datax->sekertariat_g2a;
									$total_g2 = $total_g2 + $datax->sekertariat_g2;
									$total_g1d = $total_g1d + $datax->sekertariat_g1d;
									$total_g1c = $total_g1c + $datax->sekertariat_g1c;
									$total_g1b = $total_g1b + $datax->sekertariat_g1b;
									$total_g1a = $total_g1a + $datax->sekertariat_g1a;
									$total_g1 = $total_g1 + $datax->sekertariat_g1;
									$total_g = $total_g + $datax->sekertariat_g;
									$total_s3 = $total_s3 + $datax->sekertariat_s3;
									$total_s2 = $total_s2 + $datax->sekertariat_s2;
									$total_s1 = $total_s1 + $datax->sekertariat_s1;
									$total_sma = $total_sma + $datax->sekertariat_sma;
									$total_smp = $total_smp + $datax->sekertariat_smp;
									$total_sd = $total_sd + $datax->sekertariat_sd;
									$total_sekolah = $total_sekolah + $datax->sekertariat_sekolah;
									$total_diklat1 = $total_diklat1 + $datax->sekretariat_diklat1;
									$total_diklat2 = $total_diklat2 + $datax->sekretariat_diklat2;
									$total_diklat3 = $total_diklat3 + $datax->sekretariat_diklat3;
									$total_diklat4 = $total_diklat4 + $datax->sekretariat_diklat4;
									$total_diklat = $total_diklat + $datax->sekretariat_diklat;

								//sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $datax->sekertariat_all);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $datax->sekertariat_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $datax->sekertariat_wanita);
								$this->excel->getActiveSheet()->setCellValue('F'.$no, $datax->sekertariat_e1a);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $datax->sekertariat_e1b);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $datax->sekertariat_e2a);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $datax->sekertariat_e2b);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $datax->sekertariat_e3a);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $datax->sekertariat_e3b);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $datax->sekertariat_e4a);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $datax->sekertariat_e4b);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $datax->sekertariat_eselon);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $datax->sekertariat_g4e);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $datax->sekertariat_g4d);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $datax->sekertariat_g4c);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $datax->sekertariat_g4b);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $datax->sekertariat_g4a);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $datax->sekertariat_g4);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $datax->sekertariat_g3d);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $datax->sekertariat_g3c);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $datax->sekertariat_g3b);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $datax->sekertariat_g3a);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $datax->sekertariat_g3);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $datax->sekertariat_g2d);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $datax->sekertariat_g2c);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $datax->sekertariat_g2b);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $datax->sekertariat_g2a);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $datax->sekertariat_g2);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $datax->sekertariat_g1d);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $datax->sekertariat_g1c);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $datax->sekertariat_g1b);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $datax->sekertariat_g1a);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $datax->sekertariat_g1);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $datax->sekertariat_g);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $datax->sekertariat_s3);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $datax->sekertariat_s2);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $datax->sekertariat_s1);
								//$this->excel->getActiveSheet()->setCellValue('AM'.$no, $datax->sekertariat_s1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $datax->sekertariat_sma);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $datax->sekertariat_smp);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $datax->sekertariat_sd);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $datax->sekertariat_sekolah);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $datax->sekretariat_diklat1);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $datax->sekretariat_diklat2);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $datax->sekretariat_diklat3);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $datax->sekretariat_diklat4);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $datax->sekretariat_diklat);


							}
						}
					}
				}
			  //set row jumlah
			  $this->excel->getActiveSheet()->getStyle('A59:AW59')->getFont()->setBold(true);
			  $this->excel->getActiveSheet()->mergeCells('A59:B59');
        $this->excel->getActiveSheet()->setCellValue('A59', 'JUMLAH');
			  $this->excel->getActiveSheet()->setCellValue('C59', $total_jml);
			  $this->excel->getActiveSheet()->setCellValue('D59', $total_pria);
			  $this->excel->getActiveSheet()->setCellValue('E59', $total_wanita);
			  $this->excel->getActiveSheet()->setCellValue('F59', $total_e1a);
			  $this->excel->getActiveSheet()->setCellValue('G59', $total_e1a);
			  $this->excel->getActiveSheet()->setCellValue('H59', $total_e2a);
			  $this->excel->getActiveSheet()->setCellValue('I59', $total_e2b);
			  $this->excel->getActiveSheet()->setCellValue('J59', $total_e3a);
			  $this->excel->getActiveSheet()->setCellValue('K59', $total_e3b);
			  $this->excel->getActiveSheet()->setCellValue('L59', $total_e4a);
			  $this->excel->getActiveSheet()->setCellValue('M59', $total_e4b);
			  $this->excel->getActiveSheet()->setCellValue('N59', $total_eselon);
			  $this->excel->getActiveSheet()->setCellValue('O59', $total_g4e);
			  $this->excel->getActiveSheet()->setCellValue('P59', $total_g4d);
			  $this->excel->getActiveSheet()->setCellValue('Q59', $total_g4c);
			  $this->excel->getActiveSheet()->setCellValue('R59', $total_g4b);
			  $this->excel->getActiveSheet()->setCellValue('S59', $total_g4a);
			  $this->excel->getActiveSheet()->setCellValue('T59', $total_g4);
			  $this->excel->getActiveSheet()->setCellValue('U59', $total_g3d);
			  $this->excel->getActiveSheet()->setCellValue('V59', $total_g3c);
			  $this->excel->getActiveSheet()->setCellValue('W59', $total_g3b);
			  $this->excel->getActiveSheet()->setCellValue('X59', $total_g3a);
			  $this->excel->getActiveSheet()->setCellValue('Y59', $total_g3);
			  $this->excel->getActiveSheet()->setCellValue('Z59', $total_g2d);
			  $this->excel->getActiveSheet()->setCellValue('AA59', $total_g2c);
			  $this->excel->getActiveSheet()->setCellValue('AB59', $total_g2b);
			  $this->excel->getActiveSheet()->setCellValue('AC59', $total_g2a);
			  $this->excel->getActiveSheet()->setCellValue('AD59', $total_g2);
			  $this->excel->getActiveSheet()->setCellValue('AE59', $total_g1d);
			  $this->excel->getActiveSheet()->setCellValue('AF59', $total_g1c);
			  $this->excel->getActiveSheet()->setCellValue('AG59', $total_g1b);
			  $this->excel->getActiveSheet()->setCellValue('AH59', $total_g1a);
			  $this->excel->getActiveSheet()->setCellValue('AI59', $total_g1);
			  $this->excel->getActiveSheet()->setCellValue('AJ59', $total_g);
			  $this->excel->getActiveSheet()->setCellValue('AK59', $total_s3);
			  $this->excel->getActiveSheet()->setCellValue('AL59', $total_s2);
			  $this->excel->getActiveSheet()->setCellValue('AM59', $total_s1);
			  //$this->excel->getActiveSheet()->setCellValue('AN59', $total_sma);
			  $this->excel->getActiveSheet()->setCellValue('AO59', $total_sma);
			  $this->excel->getActiveSheet()->setCellValue('AP59', $total_smp);
			  $this->excel->getActiveSheet()->setCellValue('AQ59', $total_sd);
			  $this->excel->getActiveSheet()->setCellValue('AR59', $total_sekolah);
			  $this->excel->getActiveSheet()->setCellValue('AS59', $total_diklat1);
			  $this->excel->getActiveSheet()->setCellValue('AT59', $total_diklat2);
			  $this->excel->getActiveSheet()->setCellValue('AU59', $total_diklat3);
			  $this->excel->getActiveSheet()->setCellValue('AV59', $total_diklat4);
			  $this->excel->getActiveSheet()->setCellValue('AW59', $total_diklat);

			  $this->excel->getActiveSheet()->getStyle('A5:AW59')->applyFromArray($styleArray);
			  //$this->excel->getActiveSheet()->getStyle('A5:AW'.($no-1))->applyFromArray($styleArray);
			  $this->excel->getActiveSheet()->getStyle("A5:AW7")->applyFromArray($styleCenter);
			  $this->excel->getActiveSheet()->getStyle("A8:A59")->applyFromArray($styleCenter);
			  $this->excel->getActiveSheet()->getStyle("C8:AW59")->applyFromArray($styleCenter);

			$this->excel->getActiveSheet()->getStyle('AD61')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD61', 'Kendari,          '.$bulan.' '.date('Y'));
			$this->excel->getActiveSheet()->getStyle('AA63')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AA63', 'KEPALA BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA');
			$this->excel->getActiveSheet()->getStyle('AD64')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD64', 'KOTA KENDARI');
			$this->excel->getActiveSheet()->getStyle('AD69')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD69', 'ZAINAL ARIFIN S.Sos, M.Si');
			$this->excel->getActiveSheet()->getStyle('AD70')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD70', 'NIP. 195904061981031016');


			ob_end_clean();
                  $filename='Rekapitulasi.xls'; //save our workbook as this file name
                  header('Content-Type: application/vnd.ms-excel'); //mime type
                  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                  header('Cache-Control: max-age=0'); //no cache

                  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                  //if you want to save it as .XLSX Excel 2007 format
                  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');

           //redirect('report/report_all','refresh');

		} else {
			$data = array(
						'bulan'=>null,
						'nama'=>null
				);
				$this->template->admin_render('report/report_all/form',$this->inputSetting($data));
		}

	}

	public function add_jenis() {

		if($this->input->post('submit')){

		$this->load->library('excel');

		$bulan = date('m');

		if($bulan==1) {
			$bulan = "Januari";
		} else if($bulan==2) {
			$bulan = "Februari";
		} else if($bulan==3) {
			$bulan = "Maret";
		} else if($bulan==4) {
			$bulan = "April";
		} else if($bulan==5) {
			$bulan = "Mei";
		} else if($bulan==6) {
			$bulan = "Juni";
		} else if($bulan==7) {
			$bulan = "Juli";
		} else if($bulan==8) {
			$bulan = "Agustus";
		} else if($bulan==9) {
			$bulan = "September";
		} else if($bulan==10) {
			$bulan = "Oktober";
		} else if($bulan==11) {
			$bulan = "November";
		} else if($bulan==12) {
			$bulan = "Desember";
		}

			//load PHPExcel library
  		$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
                  $this->excel->getActiveSheet()->setTitle('Laporan Rekapitulasi Pegawai');

  				//STYLING
  				$styleArray = array(
								'borders' => array('allborders' =>
												array('style' => PHPExcel_Style_Border::BORDER_THIN,
												'color' =>	array('argb' => '0000'),
  							),
  						),

  					);
				$styleCenter = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);

			$no = 8;

			//data
			$this->excel->getActiveSheet()->mergeCells('A1:AQ1');
              $this->excel->getActiveSheet()->setCellValue('A1', 'REKAPITULASI JUMLAH PEGAWAI NEGERI SIPIL BERDASARKAN JENIS KELAMIN');
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('A2:AQ2');
              $this->excel->getActiveSheet()->setCellValue('A2', 'LINGKUP PEMERINTAH KOTA KENDARI');
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('A3:AQ3');
              $this->excel->getActiveSheet()->setCellValue('A3', 'PERIODE '.strtoupper($bulan).' 2016');
              $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(12);

			  //set font
			  $this->excel->getActiveSheet()->getStyle('A5:CO68')->getFont()->setSize(10);

			  //INIT WIDTH
			  $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			  $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
			  $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AF')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AG')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AH')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AI')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AJ')->setWidth(10);
			  $this->excel->getActiveSheet()->getColumnDimension('AK')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AL')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AM')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AN')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AO')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AP')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AQ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AR')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AS')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AT')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AU')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AV')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AW')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('AX')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('AY')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('AZ')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BA')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BB')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BC')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BD')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BE')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BF')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BG')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BH')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BI')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BJ')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BK')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BL')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BM')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BN')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BO')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BP')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BQ')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BR')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BS')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BT')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BU')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BV')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BW')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BX')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BY')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('BZ')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CA')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CB')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CC')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CD')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CE')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CF')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CG')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CH')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CI')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CJ')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CK')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CL')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CM')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CN')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('CO')->setWidth(5);


			  //set column name
			  $this->excel->getActiveSheet()->mergeCells('A5:A7');
        $this->excel->getActiveSheet()->setCellValue('A5', 'No');
			  $this->excel->getActiveSheet()->mergeCells('B5:B7');
			  $this->excel->getActiveSheet()->setCellValue('B5', 'Unit Kerja');
			  $this->excel->getActiveSheet()->mergeCells('C5:C7');
			  $this->excel->getActiveSheet()->setCellValue('C5', 'JML');
			  $this->excel->getActiveSheet()->mergeCells('D5:E6');
			  $this->excel->getActiveSheet()->setCellValue('D5', 'Kelamin');
			  $this->excel->getActiveSheet()->setCellValue('D7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('E7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('F5:U5');
			  $this->excel->getActiveSheet()->setCellValue('F5', 'ESSELON');
				$this->excel->getActiveSheet()->mergeCells('F6:G6');
			  $this->excel->getActiveSheet()->setCellValue('F6', 'I.a');
				$this->excel->getActiveSheet()->setCellValue('F7', 'L');
				$this->excel->getActiveSheet()->setCellValue('G7', 'P');
				$this->excel->getActiveSheet()->mergeCells('H6:I6');
			  $this->excel->getActiveSheet()->setCellValue('H6', 'I.b');
				$this->excel->getActiveSheet()->setCellValue('H7', 'L');
				$this->excel->getActiveSheet()->setCellValue('I7', 'P');
				$this->excel->getActiveSheet()->mergeCells('J6:K6');
			  $this->excel->getActiveSheet()->setCellValue('J6', 'II.a');
				$this->excel->getActiveSheet()->setCellValue('J7', 'L');
				$this->excel->getActiveSheet()->setCellValue('K7', 'P');
				$this->excel->getActiveSheet()->mergeCells('L6:M6');
			  $this->excel->getActiveSheet()->setCellValue('L6', 'II.b');
				$this->excel->getActiveSheet()->setCellValue('L7', 'L');
				$this->excel->getActiveSheet()->setCellValue('M7', 'P');
				$this->excel->getActiveSheet()->mergeCells('N6:O6');
			  $this->excel->getActiveSheet()->setCellValue('N6', 'III.a');
				$this->excel->getActiveSheet()->setCellValue('N7', 'L');
				$this->excel->getActiveSheet()->setCellValue('O7', 'P');
				$this->excel->getActiveSheet()->mergeCells('P6:Q6');
			  $this->excel->getActiveSheet()->setCellValue('P6', 'III.b');
				$this->excel->getActiveSheet()->setCellValue('P7', 'L');
				$this->excel->getActiveSheet()->setCellValue('Q7', 'P');
				$this->excel->getActiveSheet()->mergeCells('R6:S6');
			  $this->excel->getActiveSheet()->setCellValue('R6', 'IV.a');
				$this->excel->getActiveSheet()->setCellValue('R7', 'L');
				$this->excel->getActiveSheet()->setCellValue('S7', 'P');
				$this->excel->getActiveSheet()->mergeCells('T6:U6');
			  $this->excel->getActiveSheet()->setCellValue('T6', 'IV.b');
				$this->excel->getActiveSheet()->setCellValue('T7', 'L');
				$this->excel->getActiveSheet()->setCellValue('U7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('V5:W6');
			  $this->excel->getActiveSheet()->setCellValue('V5', 'JML');
				$this->excel->getActiveSheet()->setCellValue('V7', 'L');
				$this->excel->getActiveSheet()->setCellValue('W7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('X5:AI5');
			  $this->excel->getActiveSheet()->setCellValue('X5', 'Golongan IV');
				$this->excel->getActiveSheet()->mergeCells('X6:Y6');
			  $this->excel->getActiveSheet()->setCellValue('X6', 'e');
				$this->excel->getActiveSheet()->setCellValue('X7', 'L');
				$this->excel->getActiveSheet()->setCellValue('Y7', 'P');
				$this->excel->getActiveSheet()->mergeCells('Z6:AA6');
			  $this->excel->getActiveSheet()->setCellValue('Z6', 'd');
				$this->excel->getActiveSheet()->setCellValue('Z7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AA7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AB6:AC6');
			  $this->excel->getActiveSheet()->setCellValue('AB6', 'c');
				$this->excel->getActiveSheet()->setCellValue('AB7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AC7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AD6:AE6');
			  $this->excel->getActiveSheet()->setCellValue('AD6', 'b');
				$this->excel->getActiveSheet()->setCellValue('AD7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AE7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AF6:AG6');
			  $this->excel->getActiveSheet()->setCellValue('AF6', 'a');
				$this->excel->getActiveSheet()->setCellValue('AF7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AG7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AH6:AI6');
			  $this->excel->getActiveSheet()->setCellValue('AH6', 'JML');
				$this->excel->getActiveSheet()->setCellValue('AH7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AI7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('AJ5:AS5');
			  $this->excel->getActiveSheet()->setCellValue('AJ5', 'Golongan III');
				$this->excel->getActiveSheet()->mergeCells('AJ6:AK6');
			  $this->excel->getActiveSheet()->setCellValue('AJ6', 'd');
				$this->excel->getActiveSheet()->setCellValue('AJ7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AK7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AL6:AM6');
			  $this->excel->getActiveSheet()->setCellValue('AL6', 'c');
				$this->excel->getActiveSheet()->setCellValue('AL7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AM7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AN6:AO6');
			  $this->excel->getActiveSheet()->setCellValue('AN6', 'b');
				$this->excel->getActiveSheet()->setCellValue('AN7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AO7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AP6:AQ6');
				$this->excel->getActiveSheet()->setCellValue('AP6', 'a');
				$this->excel->getActiveSheet()->setCellValue('AP7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AQ7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AR6:AS6');
			  $this->excel->getActiveSheet()->setCellValue('AR6', 'JML');
				$this->excel->getActiveSheet()->setCellValue('AR7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AS7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('AT5:BC5');
			  $this->excel->getActiveSheet()->setCellValue('AT5', 'Golongan II');
				$this->excel->getActiveSheet()->mergeCells('AT6:AU6');
			  $this->excel->getActiveSheet()->setCellValue('AT6', 'd');
				$this->excel->getActiveSheet()->setCellValue('AT7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AU7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AV6:AW6');
			  $this->excel->getActiveSheet()->setCellValue('AV6', 'c');
				$this->excel->getActiveSheet()->setCellValue('AV7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AW7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AX6:AY6');
			  $this->excel->getActiveSheet()->setCellValue('AX6', 'b');
				$this->excel->getActiveSheet()->setCellValue('AX7', 'L');
				$this->excel->getActiveSheet()->setCellValue('AY7', 'P');
				$this->excel->getActiveSheet()->mergeCells('AZ6:BA6');
			  $this->excel->getActiveSheet()->setCellValue('AZ6', 'a');
				$this->excel->getActiveSheet()->setCellValue('AZ7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BA7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BB6:BC6');
			  $this->excel->getActiveSheet()->setCellValue('BB6', 'JML');
				$this->excel->getActiveSheet()->setCellValue('BB7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BC7', 'P');


			  $this->excel->getActiveSheet()->mergeCells('BD5:BM5');
			  $this->excel->getActiveSheet()->setCellValue('BD5', 'Golongan I');
				$this->excel->getActiveSheet()->mergeCells('BD6:BE6');
			  $this->excel->getActiveSheet()->setCellValue('BD6', 'd');
				$this->excel->getActiveSheet()->setCellValue('BD7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BE7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BF6:BG6');
			  $this->excel->getActiveSheet()->setCellValue('BF6', 'c');
				$this->excel->getActiveSheet()->setCellValue('BF7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BG7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BH6:BI6');
			  $this->excel->getActiveSheet()->setCellValue('BH6', 'b');
				$this->excel->getActiveSheet()->setCellValue('BH7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BI7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BJ6:BK6');
			  $this->excel->getActiveSheet()->setCellValue('BJ6', 'a');
				$this->excel->getActiveSheet()->setCellValue('BJ7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BK7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BL6:BM6');
			  $this->excel->getActiveSheet()->setCellValue('BL6', 'JML');
				$this->excel->getActiveSheet()->setCellValue('BL7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BM7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('BN5:BO6');
			  $this->excel->getActiveSheet()->setCellValue('BN5', 'JUMLAH');
				$this->excel->getActiveSheet()->setCellValue('BN7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BO7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('BP5:CE5');
			  $this->excel->getActiveSheet()->setCellValue('BP5', 'Tingkat Pendidikan');
				$this->excel->getActiveSheet()->mergeCells('BP6:BQ6');
			  $this->excel->getActiveSheet()->setCellValue('BP6', 'S3');
				$this->excel->getActiveSheet()->setCellValue('BP7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BQ7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BR6:BS6');
			  $this->excel->getActiveSheet()->setCellValue('BR6', 'S2');
				$this->excel->getActiveSheet()->setCellValue('BR7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BS7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BT6:BU6');
			  $this->excel->getActiveSheet()->setCellValue('BT6', 'S1');
				$this->excel->getActiveSheet()->setCellValue('BT7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BU7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BV6:BW6');
			  $this->excel->getActiveSheet()->setCellValue('BV6', 'SM');
				$this->excel->getActiveSheet()->setCellValue('BV7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BW7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BX6:BY6');
			  $this->excel->getActiveSheet()->setCellValue('BX6', 'SLTA');
				$this->excel->getActiveSheet()->setCellValue('BX7', 'L');
				$this->excel->getActiveSheet()->setCellValue('BY7', 'P');
				$this->excel->getActiveSheet()->mergeCells('BZ6:CA6');
			  $this->excel->getActiveSheet()->setCellValue('BZ6', 'SLTP');
				$this->excel->getActiveSheet()->setCellValue('BZ7', 'L');
				$this->excel->getActiveSheet()->setCellValue('CA7', 'P');
				$this->excel->getActiveSheet()->mergeCells('CB6:CC6');
			  $this->excel->getActiveSheet()->setCellValue('CB6', 'SD');
				$this->excel->getActiveSheet()->setCellValue('CB7', 'L');
				$this->excel->getActiveSheet()->setCellValue('CC7', 'P');
				$this->excel->getActiveSheet()->mergeCells('CD6:CE6');
			  $this->excel->getActiveSheet()->setCellValue('CD6', 'JML');
				$this->excel->getActiveSheet()->setCellValue('CD7', 'L');
				$this->excel->getActiveSheet()->setCellValue('CE7', 'P');


			  $this->excel->getActiveSheet()->mergeCells('CF5:CO5');
			  $this->excel->getActiveSheet()->setCellValue('CF5', 'DIKLAT');
				$this->excel->getActiveSheet()->mergeCells('CF6:CG6');
			  $this->excel->getActiveSheet()->setCellValue('CF6', 'I');
				$this->excel->getActiveSheet()->setCellValue('CF7', 'L');
				$this->excel->getActiveSheet()->setCellValue('CG7', 'P');
				$this->excel->getActiveSheet()->mergeCells('CH6:CI6');
			  $this->excel->getActiveSheet()->setCellValue('CH6', 'II');
				$this->excel->getActiveSheet()->setCellValue('CH7', 'L');
				$this->excel->getActiveSheet()->setCellValue('CI7', 'P');
				$this->excel->getActiveSheet()->mergeCells('CJ6:CK6');
			  $this->excel->getActiveSheet()->setCellValue('CJ6', 'III');
				$this->excel->getActiveSheet()->setCellValue('CJ7', 'L');
				$this->excel->getActiveSheet()->setCellValue('CK7', 'P');
				$this->excel->getActiveSheet()->mergeCells('CL6:CM6');
			  $this->excel->getActiveSheet()->setCellValue('CL6', 'IV');
				$this->excel->getActiveSheet()->setCellValue('CL7', 'L');
				$this->excel->getActiveSheet()->setCellValue('CM7', 'P');
				$this->excel->getActiveSheet()->mergeCells('CN6:CO6');
			  $this->excel->getActiveSheet()->setCellValue('CN6', 'JML');
				$this->excel->getActiveSheet()->setCellValue('CN7', 'L');
				$this->excel->getActiveSheet()->setCellValue('CO7', 'P');
				//NO
			  $this->excel->getActiveSheet()->setCellValue('A8', '1');
			  $this->excel->getActiveSheet()->setCellValue('B8', '2');
			  $this->excel->getActiveSheet()->setCellValue('C8', '3');
			  $this->excel->getActiveSheet()->setCellValue('D8', '4');
			  $this->excel->getActiveSheet()->setCellValue('E8', '5');
			  $this->excel->getActiveSheet()->setCellValue('F8', '6');
			  $this->excel->getActiveSheet()->setCellValue('G8', '7');
			  $this->excel->getActiveSheet()->setCellValue('H8', '8');
			  $this->excel->getActiveSheet()->setCellValue('I8', '9');
			  $this->excel->getActiveSheet()->setCellValue('J8', '10');
			  $this->excel->getActiveSheet()->setCellValue('K8', '11');
			  $this->excel->getActiveSheet()->setCellValue('L8', '12');
			  $this->excel->getActiveSheet()->setCellValue('M8', '13');
			  $this->excel->getActiveSheet()->setCellValue('N8', '14');
			  $this->excel->getActiveSheet()->setCellValue('O8', '15');
			  $this->excel->getActiveSheet()->setCellValue('P8', '16');
			  $this->excel->getActiveSheet()->setCellValue('Q8', '17');
			  $this->excel->getActiveSheet()->setCellValue('R8', '18');
			  $this->excel->getActiveSheet()->setCellValue('S8', '19');
			  $this->excel->getActiveSheet()->setCellValue('T8', '20');
			  $this->excel->getActiveSheet()->setCellValue('U8', '21');
			  $this->excel->getActiveSheet()->setCellValue('V8', '22');
			  $this->excel->getActiveSheet()->setCellValue('W8', '23');
			  $this->excel->getActiveSheet()->setCellValue('X8', '24');
			  $this->excel->getActiveSheet()->setCellValue('Y8', '25');
			  $this->excel->getActiveSheet()->setCellValue('Z8', '26');
			  $this->excel->getActiveSheet()->setCellValue('AA8', '27');
			  $this->excel->getActiveSheet()->setCellValue('AB8', '28');
			  $this->excel->getActiveSheet()->setCellValue('AC8', '29');
			  $this->excel->getActiveSheet()->setCellValue('AD8', '30');
			  $this->excel->getActiveSheet()->setCellValue('AE8', '31');
			  $this->excel->getActiveSheet()->setCellValue('AF8', '32');
			  $this->excel->getActiveSheet()->setCellValue('AG8', '33');
			  $this->excel->getActiveSheet()->setCellValue('AH8', '34');
			  $this->excel->getActiveSheet()->setCellValue('AI8', '35');
			  $this->excel->getActiveSheet()->setCellValue('AJ8', '36');
			  $this->excel->getActiveSheet()->setCellValue('AK8', '37');
			  $this->excel->getActiveSheet()->setCellValue('AL8', '38');
			  $this->excel->getActiveSheet()->setCellValue('AM8', '39');
			  $this->excel->getActiveSheet()->setCellValue('AN8', '40');
			  $this->excel->getActiveSheet()->setCellValue('AO8', '41');
			  $this->excel->getActiveSheet()->setCellValue('AP8', '42');
			  $this->excel->getActiveSheet()->setCellValue('AQ8', '43');
			  $this->excel->getActiveSheet()->setCellValue('AR8', '44');
			  $this->excel->getActiveSheet()->setCellValue('AS8', '45');
			  $this->excel->getActiveSheet()->setCellValue('AT8', '46');
			  $this->excel->getActiveSheet()->setCellValue('AU8', '47');
			  $this->excel->getActiveSheet()->setCellValue('AV8', '48');
			  $this->excel->getActiveSheet()->setCellValue('AW8', '49');
				$this->excel->getActiveSheet()->setCellValue('AX8', '50');
				$this->excel->getActiveSheet()->setCellValue('AY8', '51');
				$this->excel->getActiveSheet()->setCellValue('AZ8', '52');
				$this->excel->getActiveSheet()->setCellValue('BA8', '53');
				$this->excel->getActiveSheet()->setCellValue('BB8', '54');
				$this->excel->getActiveSheet()->setCellValue('BC8', '55');
				$this->excel->getActiveSheet()->setCellValue('BD8', '56');
				$this->excel->getActiveSheet()->setCellValue('BE8', '57');
				$this->excel->getActiveSheet()->setCellValue('BF8', '58');
				$this->excel->getActiveSheet()->setCellValue('BG8', '59');
				$this->excel->getActiveSheet()->setCellValue('BH8', '60');
				$this->excel->getActiveSheet()->setCellValue('BI8', '61');
				$this->excel->getActiveSheet()->setCellValue('BJ8', '62');
				$this->excel->getActiveSheet()->setCellValue('BK8', '63');
				$this->excel->getActiveSheet()->setCellValue('BL8', '64');
				$this->excel->getActiveSheet()->setCellValue('BM8', '65');
				$this->excel->getActiveSheet()->setCellValue('BN8', '66');
				$this->excel->getActiveSheet()->setCellValue('BO8', '67');
				$this->excel->getActiveSheet()->setCellValue('BP8', '68');
				$this->excel->getActiveSheet()->setCellValue('BQ8', '69');
				$this->excel->getActiveSheet()->setCellValue('BR8', '70');
				$this->excel->getActiveSheet()->setCellValue('BS8', '71');
				$this->excel->getActiveSheet()->setCellValue('BT8', '72');
				$this->excel->getActiveSheet()->setCellValue('BU8', '73');
				$this->excel->getActiveSheet()->setCellValue('BV8', '74');
				$this->excel->getActiveSheet()->setCellValue('BW8', '75');
				$this->excel->getActiveSheet()->setCellValue('BX8', '76');
				$this->excel->getActiveSheet()->setCellValue('BY8', '77');
				$this->excel->getActiveSheet()->setCellValue('BZ8', '78');
				$this->excel->getActiveSheet()->setCellValue('CA8', '79');
				$this->excel->getActiveSheet()->setCellValue('CB8', '80');
				$this->excel->getActiveSheet()->setCellValue('CC8', '81');
				$this->excel->getActiveSheet()->setCellValue('CD8', '82');
				$this->excel->getActiveSheet()->setCellValue('CE8', '83');
				$this->excel->getActiveSheet()->setCellValue('CF8', '84');
				$this->excel->getActiveSheet()->setCellValue('CG8', '85');
				$this->excel->getActiveSheet()->setCellValue('CH8', '86');
				$this->excel->getActiveSheet()->setCellValue('CI8', '87');
				$this->excel->getActiveSheet()->setCellValue('CJ8', '88');
				$this->excel->getActiveSheet()->setCellValue('CK8', '89');
				$this->excel->getActiveSheet()->setCellValue('CL8', '90');
				$this->excel->getActiveSheet()->setCellValue('CM8', '91');
				$this->excel->getActiveSheet()->setCellValue('CN8', '92');
				$this->excel->getActiveSheet()->setCellValue('CO8', '93');

			  //INIT NUMBERING
			  $this->excel->getActiveSheet()->setCellValue('A9', 'I');
			  $this->excel->getActiveSheet()->setCellValue('A10', 'II');
			  $this->excel->getActiveSheet()->setCellValue('A11', 'III');
			  $this->excel->getActiveSheet()->setCellValue('A12', 'IV');
			  $this->excel->getActiveSheet()->setCellValue('A13', 'V');
			  $this->excel->getActiveSheet()->setCellValue('A14', '1');
			  $this->excel->getActiveSheet()->setCellValue('A15', '2');
			  $this->excel->getActiveSheet()->setCellValue('A16', '3');
			  $this->excel->getActiveSheet()->setCellValue('A17', '4');
			  $this->excel->getActiveSheet()->setCellValue('A18', '5');
			  $this->excel->getActiveSheet()->setCellValue('A19', '6');
			  $this->excel->getActiveSheet()->setCellValue('A20', '7');
			  $this->excel->getActiveSheet()->setCellValue('A21', '8');
			  $this->excel->getActiveSheet()->setCellValue('A22', '9');
			  $this->excel->getActiveSheet()->setCellValue('A23', '10');
			  $this->excel->getActiveSheet()->setCellValue('A24', '11');
			  $this->excel->getActiveSheet()->setCellValue('A25', '12');
			  $this->excel->getActiveSheet()->setCellValue('A26', '13');
			  $this->excel->getActiveSheet()->setCellValue('A27', '14');
			  $this->excel->getActiveSheet()->setCellValue('A28', '15');
			  $this->excel->getActiveSheet()->setCellValue('A29', '16');
			  $this->excel->getActiveSheet()->setCellValue('A30', '17');
			  $this->excel->getActiveSheet()->setCellValue('A31', '18');
			  $this->excel->getActiveSheet()->setCellValue('A32', '19');
			  $this->excel->getActiveSheet()->setCellValue('A33', '20');
			  $this->excel->getActiveSheet()->setCellValue('A34', 'VI');
			  $this->excel->getActiveSheet()->setCellValue('A35', '1');
			  $this->excel->getActiveSheet()->setCellValue('A36', '2');
			  $this->excel->getActiveSheet()->setCellValue('A37', '3');
			  $this->excel->getActiveSheet()->setCellValue('A38', '4');
			  $this->excel->getActiveSheet()->setCellValue('A39', '5');
			  $this->excel->getActiveSheet()->setCellValue('A40', '6');
			  $this->excel->getActiveSheet()->setCellValue('A41', '7');
			  $this->excel->getActiveSheet()->setCellValue('A42', '8');
			  $this->excel->getActiveSheet()->setCellValue('A43', 'VII');
			  $this->excel->getActiveSheet()->setCellValue('A44', 'VIII');
			  $this->excel->getActiveSheet()->setCellValue('A45', 'IX');
			  $this->excel->getActiveSheet()->setCellValue('A46', 'X');
			  $this->excel->getActiveSheet()->setCellValue('A47', 'XI');
			  $this->excel->getActiveSheet()->setCellValue('A48', 'XII');
			  $this->excel->getActiveSheet()->setCellValue('A49', 'XIII');
			  $this->excel->getActiveSheet()->setCellValue('A50', 'XIV');
			  $this->excel->getActiveSheet()->setCellValue('A51', 'XV');
			  $this->excel->getActiveSheet()->setCellValue('A52', 'XVI');
			  $this->excel->getActiveSheet()->setCellValue('A53', 'XVII');
			  $this->excel->getActiveSheet()->setCellValue('A54', 'XVIII');
			  $this->excel->getActiveSheet()->setCellValue('A55', 'XIX');
			  $this->excel->getActiveSheet()->setCellValue('A56', 'XX');
			  $this->excel->getActiveSheet()->setCellValue('A57', 'XXI');
			  $this->excel->getActiveSheet()->setCellValue('A58', 'XXII');
			  $this->excel->getActiveSheet()->setCellValue('A59', 'XXIII');

			  //INIT UNIT KERJA
			  $this->excel->getActiveSheet()->setCellValue('B9', 'SEKRETARIAT DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B10', 'SEKRETARIAT DPRD');
			  $this->excel->getActiveSheet()->setCellValue('B11', 'SEKRETARIAT KPU');
			  $this->excel->getActiveSheet()->setCellValue('B12', 'SEKRETARIAT DEWAN PENGURUS KORPRI');
			  $this->excel->getActiveSheet()->setCellValue('B13', 'DINAS');
			  $this->excel->getActiveSheet()->setCellValue('B14', 'DINAS PENDIDIKAN, KEPEMUDAAN DAN OLAHRAGA');
			  $this->excel->getActiveSheet()->setCellValue('B15', 'DINAS SOSIAL');
			  $this->excel->getActiveSheet()->setCellValue('B16', 'DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL');
			  $this->excel->getActiveSheet()->setCellValue('B17', 'DINAS PEKERJAAN UMUM DAN PENATAAN RUANG');
			  $this->excel->getActiveSheet()->setCellValue('B18', 'DINAS PERHUBUNGAN');
			  $this->excel->getActiveSheet()->setCellValue('B19', 'DINAS KESEHATAN');
			  $this->excel->getActiveSheet()->setCellValue('B20', 'DINAS PERUMAHAN, KAWASAN PERMUKIMAN DAN PERTANAHAN');
			  $this->excel->getActiveSheet()->setCellValue('B21', 'DINAS PERTANIAN');
			  $this->excel->getActiveSheet()->setCellValue('B22', 'DINAS KELAUTAN DAN PERIKANAN');
			  $this->excel->getActiveSheet()->setCellValue('B23', 'DINAS PERDAGANGAN, KOPERASI DAN UMKM');
			  $this->excel->getActiveSheet()->setCellValue('B24', 'DINAS KEBUDAYAAN DAN PARIWISATA');
			  $this->excel->getActiveSheet()->setCellValue('B25', 'DINAS KOMUNIKASI DAN INFORMATIKA');
			  $this->excel->getActiveSheet()->setCellValue('B26', 'DINAS PENGENDALIAN PENDUDUK DAN KELUARGA BERENCANA');
			  $this->excel->getActiveSheet()->setCellValue('B27', 'DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU');
			  $this->excel->getActiveSheet()->setCellValue('B28', 'DINAS LINGKUNGAN HIDUP DAN KEHUTANAN');
			  $this->excel->getActiveSheet()->setCellValue('B29', 'DINAS KEBAKARAN');
			  $this->excel->getActiveSheet()->setCellValue('B30', 'DINAS PERPUSTAKAAN DAN KEARSIPAN');
			  $this->excel->getActiveSheet()->setCellValue('B31', 'DINAS PANGAN');
			  $this->excel->getActiveSheet()->setCellValue('B32', 'DINAS TENAGA KERJA DAN PERINDUSTRIAN');
			  $this->excel->getActiveSheet()->setCellValue('B33', 'DINAS PEMBERDAYAAN PEREMPUAN DAN PERLINDUNGAN ANAK');
			  $this->excel->getActiveSheet()->setCellValue('B34', 'BADAN');
			  $this->excel->getActiveSheet()->setCellValue('B35', 'INSPEKTORAT DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B36', 'BADAN PERENCANAAN PEMBANGUNAN DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B37', 'BADAN KESATUAN BANGSA DAN POLITIK');
			  $this->excel->getActiveSheet()->setCellValue('B38', 'BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA');
			  $this->excel->getActiveSheet()->setCellValue('B39', 'BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B40', 'BADAN PENANGGULANGAN BENCANA DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B41', 'SATUAN POLISI PAMONG PRAJA');
			  $this->excel->getActiveSheet()->setCellValue('B42', 'BADAN PENGELOLA PAJAK DAN RETRIBUSI DAERAH');
			  $this->excel->getActiveSheet()->setCellValue('B43', 'RUMAH SAKIT UMUM DAERAH KOTA KENDARI');
			  $this->excel->getActiveSheet()->setCellValue('B44', 'PUSKESMAS');
			  $this->excel->getActiveSheet()->setCellValue('B45', 'KECAMATAN & KELURAHAN');
			  $this->excel->getActiveSheet()->setCellValue('B46', 'PENGAWAS DIKNAS');
			  $this->excel->getActiveSheet()->setCellValue('B47', 'UPTD DINAS PERTANIAN');
			  $this->excel->getActiveSheet()->setCellValue('B48', 'UPTD DINAS KELAUTAN & PERIKANAN');
			  $this->excel->getActiveSheet()->setCellValue('B49', 'UPTD DIKNAS');
			  $this->excel->getActiveSheet()->setCellValue('B50', 'UPTD PERHUBUNGAN');
			  $this->excel->getActiveSheet()->setCellValue('B51', 'UPTD CAPIL');
			  $this->excel->getActiveSheet()->setCellValue('B52', 'UPTD RUSUNAWA');
			  $this->excel->getActiveSheet()->setCellValue('B53', 'UPTD BADAN KB DAN PP');
			  $this->excel->getActiveSheet()->setCellValue('B54', 'UPTD KESEHATAN');
			  $this->excel->getActiveSheet()->setCellValue('B55', 'UPTD LINGKUNGAN HIDUP');
			  $this->excel->getActiveSheet()->setCellValue('B56', 'UPTD DINAS PENDAPATAN');
			  $this->excel->getActiveSheet()->setCellValue('B57', 'TK');
			  $this->excel->getActiveSheet()->setCellValue('B58', 'SD');
			  $this->excel->getActiveSheet()->setCellValue('B59', 'SLTP');

				//ada isinya disni

				$urutan = array('18', '16', '109', '21', '0', '1', '13', '11', '4', '8', '2', '6', '104', '107', '108', '106',
				'112', '111', '115', '9', '5', '103', '101', '113', '12', '0', '22', '7', '14', '24', '99', '100', '15', '20', '3',
				'1111', '11111', '170', '104a', '107a', '1112', '8a', '11a', '6a', '111a', '2a', '9a', '20a',
				'11112', '11113', '1113');
			  $length = count($urutan);
			  $total_jml = 0;$total_pria = 0;$total_wanita = 0;
				$total_e1a1 = 0;$total_e1b1 = 0;$total_e2a1 = 0;$total_e2b1 = 0;$total_e3a1 = 0;$total_e3b1 = 0;$total_e4a1 = 0;$total_e4b1 = 0;
				$total_e1a2 = 0;$total_e1b2 = 0;$total_e2a2 = 0;$total_e2b2 = 0;$total_e3a2 = 0;$total_e3b2 = 0;$total_e4a2 = 0;$total_e4b2 = 0;
				$total_eselon1 = 0;$total_eselon2 = 0;
				$total_g4e1 = 0;$total_g4d1 = 0;$total_g4c1 = 0;$total_g4b1 = 0;$total_g4a1 = 0;$total_g41 = 0;
				$total_g4e2 = 0;$total_g4d2 = 0;$total_g4c2 = 0;$total_g4b2= 0;$total_g4a2 = 0;$total_g42 = 0;
				$total_g3d1 = 0;$total_g3c1 = 0;$total_g3b1 = 0;$total_g3a1 = 0;$total_g31 = 0;
				$total_g3d2 = 0;$total_g3c2 = 0;$total_g3b2 = 0;$total_g3a2 = 0;$total_g32 = 0;
				$total_g2d1 = 0;$total_g2c1 = 0;$total_g2b1 = 0;$total_g2a1 = 0;$total_g21 = 0;
				$total_g2d2 = 0;$total_g2c2 = 0;$total_g2b2 = 0;$total_g2a2 = 0;$total_g22 = 0;
				$total_g1d1 = 0;$total_g1c1 = 0;$total_g1b1 = 0;$total_g1a1 = 0;$total_g11 = 0;
				$total_g1d2 = 0;$total_g1c2 = 0;$total_g1b2 = 0;$total_g1a2 = 0;$total_g12 = 0;
				$total_g1 = 0;$total_g2 = 0;
				$total_s31 = 0;$total_s21 = 0;$total_s11 = 0;$total_sma1 = 0;$total_smp1 = 0;$total_sd1 = 0;$total_sekolah1 = 0;
				$total_s32 = 0;$total_s22 = 0;$total_s12 = 0;$total_sma2 = 0;$total_smp2 = 0;$total_sd2 = 0;$total_sekolah2 = 0;
				$total_diklat11 = 0;$total_diklat21 = 0;$total_diklat31 = 0;$total_diklat41 = 0;$total_diklat1 = 0;
				$total_diklat12 = 0;$total_diklat22 = 0;$total_diklat32 = 0;$total_diklat42 = 0;$total_diklat2 = 0;

				//1 diknas sendiri
				$diknas = array('');

			  //104 Pertanian
			  $dinaspertanian = array(343,344,345);

			  // 107 dinas perikanan
			  $dinasperikanan = array(324,325,326);

			  //11 capil
			  $capil = array(191);

			  //111 kb
			  $kb = array(327,328,329,330,331,332,333,334,335,336,337);

			  //12 dinaspemberdayaan perempuan
			  $dpppa = array(192);

			  //18 sekda
			  // $sekda = array(295,296,297,298,299,300,301,302,303,304,308,309,310,312,313,314,315,316,317,318,
			  // 319,320,321,322,323);

				$sekda = array('', 298, 322, 321, 320, 319, 316, 315, 314, 313, 312, 310, 309, 308, 299, 323);

			  //2 dinkes
			  $dinkes = array(171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,357,358);
			  //puskesmas
			  //$puskesmas = array(171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186);

			  $uptdkesehatan = array(186,358);

			  //20 pajak
			  $pajak = array(194,195,196,197,198,199,200,201,202,203,346,347,348,349,350,351,352,
			  353,354,355,356,427);



			  //6 perumahan
			  $perumahan = array(190);

			  //8 perhubungan
			  $perhubungan = array(189,339,340,341,342);

			  //9 blh
			  $blh = array(187, 338);

				//skpd gabung
				$puskesmas = array(126,127,128,129,130,131,132,133, 134, 135,136,137,138,139,140);
				$uptddiknas = array(116,117,118,119,120,121,122,123,124,125);
				$kec = array(25,26,27,28,29,30,31,32,33,34,114);
				$kel = array(359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,
				377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,
				398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,418,419,420,422,423,424);
				$smp = array(141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160);
				$tk = array(121,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,
			237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,
			266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,
			305,307,425,430,431,433,434,435,436,437,440,441,442,446,448);

			//pengawas diknas
			$pengawasdiknas = array(170);

				$sd_skpd=array(1, 166, 117, 118, 119, 120, 121, 122, 123, 124, 125);
				$sd_unitkerja=array(1,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,
														65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,102,103,104,
														105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,
														129,130,131,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,
														155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,270,311,426,429);

			  for($i=0;$i<$length;$i++) {
						//echo $urutan[$i];
					 	$id = $urutan[$i];
					 	if($id == 0) {
							$no = $no + 1;
					 	} else {
							//get unit kerja
							$temp_jml = 0;$temp_pria = 0;$temp_wanita = 0;
							$temp_e1a1 = 0;$temp_e1b1= 0;$temp_e2a1 = 0;$temp_e2b1 = 0;$temp_e3a1 = 0;$temp_e3b1 = 0;$temp_e4a1 = 0;$temp_e4b1 = 0;
							$temp_e1a2 = 0;$temp_e1b2 = 0;$temp_e2a2 = 0;$temp_e2b2 = 0;$temp_e3a2 = 0;$temp_e3b2 = 0;$temp_e4a2 = 0;$temp_e4b2 = 0;
							$temp_eselon1 = 0;$temp_eselon2 = 0;
							$temp_g4e1 = 0;$temp_g4d1 = 0;$temp_g4c1 = 0;$temp_g4b1 = 0;$temp_g4a1 = 0;$temp_g41 = 0;
							$temp_g4e2 = 0;$temp_g4d2 = 0;$temp_g4c2 = 0;$temp_g4b2 = 0;$temp_g4a2 = 0;$temp_g42 = 0;
							$temp_g3d1 = 0;$temp_g3c1 = 0;$temp_g3b1 = 0;$temp_g3a1 = 0;$temp_g31 = 0;$temp_g2d1 = 0;
							$temp_g3d2 = 0;$temp_g3c2 = 0;$temp_g3b2 = 0;$temp_g3a2 = 0;$temp_g32 = 0;$temp_g2d2 = 0;
							$temp_g2c1 = 0;$temp_g2b1 = 0;$temp_g2a1 = 0;$temp_g21 = 0;$temp_g1d1 = 0;$temp_g1c1 = 0;
							$temp_g2c2 = 0;$temp_g2b2 = 0;$temp_g2a2 = 0;$temp_g22 = 0;$temp_g1d2 = 0;$temp_g1c2 = 0;
							$temp_g1b1 = 0;$temp_g1a1 = 0;$temp_g11 = 0;$temp_g1 = 0;$temp_s31 = 0;$temp_s21 = 0;
							$temp_g1b2 = 0;$temp_g1a2 = 0;$temp_g12 = 0;$temp_g2 = 0;$temp_s32 = 0;$temp_s22 = 0;
							$temp_s11 = 0;$temp_sma1 = 0;$temp_smp1 = 0;$temp_sd1 = 0;$temp_sekolah1 = 0;$temp_diklat11 = 0;
							$temp_s12 = 0;$temp_sma2 = 0;$temp_smp2 = 0;$temp_sd2 = 0;$temp_sekolah2 = 0;$temp_diklat12 = 0;
							$temp_diklat21 = 0;$temp_diklat31 = 0;$temp_diklat41 = 0;$temp_diklat1 = 0;
							$temp_diklat22 = 0;$temp_diklat32 = 0;$temp_diklat42 = 0;$temp_diklat2 = 0;

							if($id == 18) {
								  //sekertariat
								  $datas = $sekda;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($id, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "104a") {
								  $ids=104;
								  $datas = $dinaspertanian;
									for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "107a") {
								  $ids=107;
								  $datas = $dinasperikanan;
									for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "8a") {
								  $ids=8;
								  $datas = $perhubungan;
									for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);
							}
							else if($id == "11a"){
								  $ids=11;
								  $datas = $capil;
									for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "6a"){
								  $ids=6;
								  $datas = $perumahan;
									for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "111a"){
								  $ids=111;
								  $datas = $kb;
									for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "2a"){
								  $ids=2;
								  $datas = $uptdkesehatan;
										for($j=0;$j<count($datas);$j++) {
									  $data = $this->report_all_model->getValues_jen($ids, $datas[$j]);
									  foreach($data as $datax) {
											$temp_jml = $temp_jml + $datax->sekertariat_all;
											$temp_pria = $temp_pria + $datax->sekertariat_pria;
											$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

											$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
											$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
											$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
											$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
											$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
											$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
											$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
											$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
											$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
											$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
											$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
											$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
											$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
											$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
											$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
											$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
											$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
											$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
											$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
											$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
											$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
											$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
											$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
											$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
											$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
											$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
											$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
											$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
											$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
											$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
											$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
											$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
											$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
											$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
											$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
											$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
											$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
											$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
											$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
											$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
											$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
											$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
											$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


								  	}
					   			}

							   //isi total
									$no++;
									$total_jml = $total_jml + $temp_jml;
									$total_pria = $total_pria + $temp_pria;
									$total_wanita = $total_wanita + $temp_wanita;

									$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
									$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
									$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
									$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
									$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
									$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
									$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
									$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
									$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
									$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
									$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
									$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
									$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
									$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
									$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
									$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
									$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
									$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
									$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
									$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
									$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
									$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
									$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
									$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
									$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
									$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
									$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
									$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
									$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
									$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
									$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
									$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
									$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
									$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
									$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
									$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
									$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
									$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
									$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
									$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
									$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
									$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
									$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

									 //show it
								   //sekertariat
									$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
									$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
									$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

									$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
									$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
									$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
									$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
									$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
									$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
									$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
									$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
									$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
									$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
									$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
									$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
									$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
									$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
									$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
									$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
									$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
									$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
									$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
									$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
									$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
									$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
									$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
									$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
									$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
									$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
									$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
									$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
									$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
									$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
									$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
									$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
									$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
									$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
									$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
									$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
									$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
									$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
									$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
									$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
									$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
									$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
									$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
									$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
									$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
									$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
									$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
									$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
									$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
									$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
									$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
									$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
									$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
									$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
									$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
									$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
									$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
									$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
									$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
									$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
									$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
									$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
									$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
									$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
									$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
									$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
									$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
									$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
									$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
									$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
									$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
									$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
									$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
									$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
									$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
									$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
									$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
									$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
									$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
									$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
									$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
									$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
									$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
									$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
									$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
									$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
									$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
									$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);
								}
							else if($id == "9a"){
								  $ids=9;
								  $datas = $blh;
									for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "20a"){
								  $ids=20;
								  $datas = $pajak;
									for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($ids, $datas[$j]);
								  foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "1111"){
								  // $ids=20;
								  $datas = $puskesmas;
							    for($j=0;$j<count($datas);$j++) {
								  $data = $this->report_all_model->getValues_jen($datas[$j], '');
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "1112"){
									// $ids=20;
									$datas = $uptddiknas;
									for($j=0;$j<count($datas);$j++) {
									$data = $this->report_all_model->getValues_jen($datas[$j], '');
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;


							  	}
				   			}

						   //isi total
								$no++;
								$total_jml = $total_jml + $temp_jml;
								$total_pria = $total_pria + $temp_pria;
								$total_wanita = $total_wanita + $temp_wanita;

								$total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
								$total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
								$total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
								$total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
								$total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
								$total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
								$total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
								$total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
								$total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
								$total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
								$total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
								$total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
								$total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
								$total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
								$total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
								$total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
								$total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
								$total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
								$total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
								$total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
								$total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
								$total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
								$total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
								$total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
								$total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
								$total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
								$total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
								$total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
								$total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
								$total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
								$total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
								$total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
								$total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
								$total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
								$total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
								$total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
								$total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
								$total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
								$total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
								$total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
								$total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
								$total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
								$total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								 //show it
							   //sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "11111"){
									// $ids=20;
									$datas = $kec;
									for($j=0;$j<count($datas);$j++) {
									$data = $this->report_all_model->getValues_jen($datas[$j], '');
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
									}
									//KELURAHAN
									$ids = $datas[$j];
									$datam = $kel;
									for($k=0;$k<count($datam);$k++) {
									$data = $this->report_all_model->getValues_jen($ids ,$datam[$k]);
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
									}
								}


								}

							 //isi total
							 $no++;
							 $total_jml = $total_jml + $temp_jml;
							 $total_pria = $total_pria + $temp_pria;
							 $total_wanita = $total_wanita + $temp_wanita;

							 $total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
							 $total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
							 $total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
							 $total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
							 $total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
							 $total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
							 $total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
							 $total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
							 $total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
							 $total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
							 $total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
							 $total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
							 $total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
							 $total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
							 $total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
							 $total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
							 $total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
							 $total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
							 $total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
							 $total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
							 $total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
							 $total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
							 $total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
							 $total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
							 $total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
							 $total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
							 $total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
							 $total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
							 $total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
							 $total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
							 $total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
							 $total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
							 $total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
							 $total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
							 $total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
							 $total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
							 $total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
							 $total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
							 $total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
							 $total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
							 $total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
							 $total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
							 $total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

								//show it
								//sekertariat
							 $this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
							 $this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
							 $this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

							 $this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
							 $this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
							 $this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
							 $this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
							 $this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
							 $this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
							 $this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
							 $this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
							 $this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
							 $this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
							 $this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
							 $this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
							 $this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
							 $this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
							 $this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
							 $this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
							 $this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
							 $this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
							 $this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
							 $this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
							 $this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
							 $this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
							 $this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
							 $this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
							 $this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
							 $this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
							 $this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
							 $this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
							 $this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
							 $this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
							 $this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
							 $this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
							 $this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
							 $this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
							 $this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
							 $this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
							 $this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
							 $this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
							 $this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
							 $this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
							 $this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
							 $this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
							 $this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
							 $this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
							 $this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
							 $this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
							 $this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
							 $this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
							 $this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
							 $this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
							 $this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
							 $this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
							 $this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
							 $this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
							 $this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
							 $this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
							 $this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
							 $this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
							 $this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
							 $this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
							 $this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
							 $this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
							 $this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
							 $this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
							 $this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
							 $this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
							 $this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
							 $this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
							 $this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
							 $this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
							 $this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
							 $this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
							 $this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
							 $this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
							 $this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
							 $this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
							 $this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
							 $this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
							 $this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
							 $this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
							 $this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
							 $this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
							 $this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
							 $this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
							 $this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
							 $this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
							 $this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
							 $this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "1113"){
									// $ids=20;
									$datas = $smp;
									for($j=0;$j<count($datas);$j++) {
									$data = $this->report_all_model->getValues_jen($datas[$j], '');
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
									}
								}

								//isi total
 							 $no++;
 							 $total_jml = $total_jml + $temp_jml;
 							 $total_pria = $total_pria + $temp_pria;
 							 $total_wanita = $total_wanita + $temp_wanita;

 							 $total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
 							 $total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
 							 $total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
 							 $total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
 							 $total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
 							 $total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
 							 $total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
 							 $total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
 							 $total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
 							 $total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
 							 $total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
 							 $total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
 							 $total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
 							 $total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
 							 $total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
 							 $total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
 							 $total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
 							 $total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
 							 $total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
 							 $total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
 							 $total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
 							 $total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
 							 $total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
 							 $total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
 							 $total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
 							 $total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
 							 $total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
 							 $total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
 							 $total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
 							 $total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
 							 $total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
 							 $total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
 							 $total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
 							 $total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
 							 $total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
 							 $total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
 							 $total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
 							 $total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
 							 $total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
 							 $total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
 							 $total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
 							 $total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
 							 $total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

 								//show it
 								//sekertariat
 							 $this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
 							 $this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
 							 $this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

 							 $this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
 							 $this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
 							 $this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
 							 $this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
 							 $this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
 							 $this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
 							 $this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
 							 $this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
 							 $this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
 							 $this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
 							 $this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
 							 $this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
 							 $this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
 							 $this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
 							 $this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
 							 $this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
 							 $this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
 							 $this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
 							 $this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
 							 $this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
 							 $this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
 							 $this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
 							 $this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
 							 $this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
 							 $this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
 							 $this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
 							 $this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
 							 $this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
 							 $this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
 							 $this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
 							 $this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
 							 $this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
 							 $this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
 							 $this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
 							 $this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
 							 $this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
 							 $this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
 							 $this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
 							 $this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
 							 $this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
 							 $this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
 							 $this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
 							 $this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
 							 $this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
 							 $this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
 							 $this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
 							 $this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
 							 $this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
 							 $this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
 							 $this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
 							 $this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
 							 $this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
 							 $this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
 							 $this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
 							 $this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
 							 $this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
 							 $this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
 							 $this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
 							 $this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
 							 $this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
 							 $this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
 							 $this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
 							 $this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
 							 $this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
 							 $this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
 							 $this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
 							 $this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
 							 $this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
 							 $this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
 							 $this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
 							 $this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
 							 $this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
 							 $this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
 							 $this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
 							 $this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
 							 $this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
 							 $this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
 							 $this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
 							 $this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
 							 $this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
 							 $this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
 							 $this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
 							 $this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
 							 $this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
 							 $this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
 							 $this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
 							 $this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
 							 $this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "11112"){

										$datam = $tk;
										for($k=0;$k<count($datam);$k++) {
										$data = $this->report_all_model->getValuesU_jen($datam[$k]);
										foreach($data as $datax) {
											$temp_jml = $temp_jml + $datax->sekertariat_all;
											$temp_pria = $temp_pria + $datax->sekertariat_pria;
											$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

											$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
											$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
											$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
											$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
											$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
											$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
											$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
											$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
											$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
											$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
											$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
											$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
											$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
											$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
											$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
											$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
											$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
											$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
											$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
											$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
											$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
											$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
											$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
											$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
											$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
											$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
											$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
											$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
											$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
											$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
											$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
											$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
											$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
											$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
											$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
											$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
											$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
											$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
											$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
											$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
											$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
											$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
											$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										}
								}


								//isi total
 							 $no++;
 							 $total_jml = $total_jml + $temp_jml;
 							 $total_pria = $total_pria + $temp_pria;
 							 $total_wanita = $total_wanita + $temp_wanita;

 							 $total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
 							 $total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
 							 $total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
 							 $total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
 							 $total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
 							 $total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
 							 $total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
 							 $total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
 							 $total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
 							 $total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
 							 $total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
 							 $total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
 							 $total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
 							 $total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
 							 $total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
 							 $total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
 							 $total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
 							 $total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
 							 $total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
 							 $total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
 							 $total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
 							 $total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
 							 $total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
 							 $total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
 							 $total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
 							 $total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
 							 $total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
 							 $total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
 							 $total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
 							 $total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
 							 $total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
 							 $total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
 							 $total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
 							 $total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
 							 $total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
 							 $total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
 							 $total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
 							 $total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
 							 $total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
 							 $total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
 							 $total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
 							 $total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
 							 $total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

 								//show it
 								//sekertariat
 							 $this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
 							 $this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
 							 $this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

 							 $this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
 							 $this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
 							 $this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
 							 $this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
 							 $this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
 							 $this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
 							 $this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
 							 $this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
 							 $this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
 							 $this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
 							 $this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
 							 $this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
 							 $this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
 							 $this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
 							 $this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
 							 $this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
 							 $this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
 							 $this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
 							 $this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
 							 $this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
 							 $this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
 							 $this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
 							 $this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
 							 $this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
 							 $this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
 							 $this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
 							 $this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
 							 $this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
 							 $this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
 							 $this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
 							 $this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
 							 $this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
 							 $this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
 							 $this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
 							 $this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
 							 $this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
 							 $this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
 							 $this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
 							 $this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
 							 $this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
 							 $this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
 							 $this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
 							 $this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
 							 $this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
 							 $this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
 							 $this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
 							 $this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
 							 $this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
 							 $this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
 							 $this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
 							 $this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
 							 $this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
 							 $this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
 							 $this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
 							 $this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
 							 $this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
 							 $this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
 							 $this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
 							 $this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
 							 $this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
 							 $this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
 							 $this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
 							 $this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
 							 $this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
 							 $this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
 							 $this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
 							 $this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
 							 $this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
 							 $this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
 							 $this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
 							 $this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
 							 $this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
 							 $this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
 							 $this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
 							 $this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
 							 $this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
 							 $this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
 							 $this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
 							 $this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
 							 $this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
 							 $this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
 							 $this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
 							 $this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
 							 $this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
 							 $this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
 							 $this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
 							 $this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
 							 $this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "170"){

										$datam = $pengawasdiknas;
										for($k=0;$k<count($datam);$k++) {
										$data = $this->report_all_model->getValuesU_jen($datam[$k]);
										foreach($data as $datax) {
											$temp_jml = $temp_jml + $datax->sekertariat_all;
											$temp_pria = $temp_pria + $datax->sekertariat_pria;
											$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

											$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
											$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
											$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
											$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
											$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
											$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
											$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
											$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
											$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
											$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
											$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
											$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
											$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
											$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
											$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
											$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
											$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
											$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
											$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
											$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
											$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
											$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
											$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
											$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
											$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
											$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
											$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
											$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
											$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
											$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
											$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
											$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
											$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
											$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
											$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
											$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
											$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
											$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
											$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
											$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
											$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
											$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
											$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
										}
								}


								//isi total
 							 $no++;
 							 $total_jml = $total_jml + $temp_jml;
 							 $total_pria = $total_pria + $temp_pria;
 							 $total_wanita = $total_wanita + $temp_wanita;

 							 $total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
 							 $total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
 							 $total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
 							 $total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
 							 $total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
 							 $total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
 							 $total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
 							 $total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
 							 $total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
 							 $total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
 							 $total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
 							 $total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
 							 $total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
 							 $total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
 							 $total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
 							 $total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
 							 $total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
 							 $total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
 							 $total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
 							 $total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
 							 $total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
 							 $total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
 							 $total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
 							 $total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
 							 $total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
 							 $total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
 							 $total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
 							 $total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
 							 $total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
 							 $total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
 							 $total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
 							 $total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
 							 $total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
 							 $total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
 							 $total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
 							 $total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
 							 $total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
 							 $total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
 							 $total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
 							 $total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
 							 $total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
 							 $total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
 							 $total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;

 								//show it
 								//sekertariat
 							 $this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
 							 $this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
 							 $this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

 							 $this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
 							 $this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
 							 $this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
 							 $this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
 							 $this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
 							 $this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
 							 $this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
 							 $this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
 							 $this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
 							 $this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
 							 $this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
 							 $this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
 							 $this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
 							 $this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
 							 $this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
 							 $this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
 							 $this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
 							 $this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
 							 $this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
 							 $this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
 							 $this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
 							 $this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
 							 $this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
 							 $this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
 							 $this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
 							 $this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
 							 $this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
 							 $this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
 							 $this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
 							 $this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
 							 $this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
 							 $this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
 							 $this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
 							 $this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
 							 $this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
 							 $this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
 							 $this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
 							 $this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
 							 $this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
 							 $this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
 							 $this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
 							 $this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
 							 $this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
 							 $this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
 							 $this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
 							 $this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
 							 $this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
 							 $this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
 							 $this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
 							 $this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
 							 $this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
 							 $this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
 							 $this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
 							 $this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
 							 $this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
 							 $this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
 							 $this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
 							 $this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
 							 $this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
 							 $this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
 							 $this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
 							 $this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
 							 $this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
 							 $this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
 							 $this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
 							 $this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
 							 $this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
 							 $this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
 							 $this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
 							 $this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
 							 $this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
 							 $this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
 							 $this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
 							 $this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
 							 $this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
 							 $this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
 							 $this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
 							 $this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
 							 $this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
 							 $this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
 							 $this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
 							 $this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
 							 $this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
 							 $this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
 							 $this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
 							 $this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
 							 $this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
 							 $this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);

							}
							else if($id == "11113"){

								$datam = $sd_unitkerja;
								for($k=0;$k<count($datam);$k++) {
									$data = $this->report_all_model->getValuesU_jen($datam[$k]);
									foreach($data as $datax) {
										$temp_jml = $temp_jml + $datax->sekertariat_all;
										$temp_pria = $temp_pria + $datax->sekertariat_pria;
										$temp_wanita = $temp_wanita + $datax->sekertariat_wanita;

										$temp_e1a1 = $temp_e1a1 + $datax->sekertariat_e1a1; $temp_e1a2 = $temp_e1a2 + $datax->sekertariat_e1a2;
										$temp_e1b1 = $temp_e1a1 + $datax->sekertariat_e1b1; $temp_e1b2 = $temp_e1a2 + $datax->sekertariat_e1b2;
										$temp_e2a1 = $temp_e2a1 + $datax->sekertariat_e2a1; $temp_e2a2 = $temp_e2a2 + $datax->sekertariat_e2a2;
										$temp_e2b1 = $temp_e2a1 + $datax->sekertariat_e2b1; $temp_e2b2 = $temp_e2a2 + $datax->sekertariat_e2b2;
										$temp_e3a1 = $temp_e3a1 + $datax->sekertariat_e3a1; $temp_e3a2 = $temp_e3a2 + $datax->sekertariat_e3a2;
										$temp_e3b1 = $temp_e3a1 + $datax->sekertariat_e3b1; $temp_e3b2 = $temp_e3a2 + $datax->sekertariat_e3b2;
										$temp_e4a1 = $temp_e4a1 + $datax->sekertariat_e4a1; $temp_e4a2 = $temp_e4a2 + $datax->sekertariat_e4a2;
										$temp_e4b1 = $temp_e4a1 + $datax->sekertariat_e4b1; $temp_e4b2 = $temp_e4a2 + $datax->sekertariat_e4b2;
										$temp_eselon1 = $temp_eselon1 + $datax->sekertariat_eselon1;$temp_eselon2 = $temp_eselon2 + $datax->sekertariat_eselon2;
										$temp_g4e1 = $temp_g4e1 + $datax->sekertariat_g4e1; $temp_g4e2 = $temp_g4e2 + $datax->sekertariat_g4e2;
										$temp_g4d1 = $temp_g4d1 + $datax->sekertariat_g4d1; $temp_g4d2 = $temp_g4d2 + $datax->sekertariat_g4d2;
										$temp_g4c1 = $temp_g4c1 + $datax->sekertariat_g4c1; $temp_g4c2 = $temp_g4c2 + $datax->sekertariat_g4c2;
										$temp_g4b1 = $temp_g4b1 + $datax->sekertariat_g4b1; $temp_g4b2 = $temp_g4b2 + $datax->sekertariat_g4b2;
										$temp_g4a1 = $temp_g4a1 + $datax->sekertariat_g4a1; $temp_g4a2 = $temp_g4a2 + $datax->sekertariat_g4a2;
										$temp_g41 = $temp_g41 + $datax->sekertariat_g41; $temp_g42 = $temp_g42 + $datax->sekertariat_g42;
										$temp_g3d1 = $temp_g3d1 + $datax->sekertariat_g3d1; $temp_g3d2 = $temp_g3d2 + $datax->sekertariat_g3d2;
										$temp_g3c1 = $temp_g3c1 + $datax->sekertariat_g3c1; $temp_g3c2 = $temp_g3c2 + $datax->sekertariat_g3c2;
										$temp_g3b1 = $temp_g3b1 + $datax->sekertariat_g3b1; $temp_g3b2 = $temp_g3b2 + $datax->sekertariat_g3b2;
										$temp_g3a1 = $temp_g3a1 + $datax->sekertariat_g3a1; $temp_g3a2 = $temp_g3a2 + $datax->sekertariat_g3a2;
										$temp_g31 = $temp_g31 + $datax->sekertariat_g31; $temp_g32 = $temp_g32 + $datax->sekertariat_g32;
										$temp_g2d1 = $temp_g2d1 + $datax->sekertariat_g2d1; $temp_g2d2 = $temp_g2d2 + $datax->sekertariat_g2d2;
										$temp_g2c1 = $temp_g2c1 + $datax->sekertariat_g2c1; $temp_g2c2 = $temp_g2c2 + $datax->sekertariat_g2c2;
										$temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1; $temp_g2b1 = $temp_g2b1 + $datax->sekertariat_g2b1;
										$temp_g2a1 = $temp_g2a1 + $datax->sekertariat_g2a1; $temp_g2a2 = $temp_g2a2 + $datax->sekertariat_g2a2;
										$temp_g21 = $temp_g21 + $datax->sekertariat_g21; $temp_g21 = $temp_g21 + $datax->sekertariat_g21;
										$temp_g1d1 = $temp_g1d1 + $datax->sekertariat_g1d1; $temp_g1d2 = $temp_g1d2 + $datax->sekertariat_g1d2;
										$temp_g1c1 = $temp_g1c1 + $datax->sekertariat_g1c1; $temp_g1c2 = $temp_g1c2 + $datax->sekertariat_g1c2;
										$temp_g1b1 = $temp_g1b1 + $datax->sekertariat_g1b1; $temp_g1b2 = $temp_g1b2 + $datax->sekertariat_g1b2;
										$temp_g1a1 = $temp_g1a1 + $datax->sekertariat_g1a1; $temp_g1a2 = $temp_g1a2 + $datax->sekertariat_g1a2;
										$temp_g11 = $temp_g11 + $datax->sekertariat_g11; $temp_g12 = $temp_g12 + $datax->sekertariat_g12;
										$temp_g1 = $temp_g1 + $datax->sekertariat_g1; $temp_g2 = $temp_g2 + $datax->sekertariat_g2;
										$temp_s31 = $temp_s31 + $datax->sekertariat_s31; $temp_s32 = $temp_s32 + $datax->sekertariat_s32;
										$temp_s21 = $temp_s21 + $datax->sekertariat_s21; $temp_s22 = $temp_s22 + $datax->sekertariat_s22;
										$temp_s11 = $temp_s11 + $datax->sekertariat_s11; $temp_s12 = $temp_s12 + $datax->sekertariat_s12;
										$temp_sma1 = $temp_sma1 + $datax->sekertariat_sma1; $temp_sma2 = $temp_sma2 + $datax->sekertariat_sma2;
										$temp_smp1 = $temp_smp1 + $datax->sekertariat_smp1; $temp_smp2 = $temp_smp2 + $datax->sekertariat_smp2;
										$temp_sd1 = $temp_sd1 + $datax->sekertariat_sd1; $temp_sd2 = $temp_sd2 + $datax->sekertariat_sd2;
										$temp_sekolah1 = $temp_sekolah1 + $datax->sekertariat_sekolah1; $temp_sekolah2 = $temp_sekolah2 + $datax->sekertariat_sekolah2;
										$temp_diklat11 = $temp_diklat11 + $datax->sekretariat_diklat11; $temp_diklat12 = $temp_diklat12 + $datax->sekretariat_diklat12;
										$temp_diklat21 = $temp_diklat21+ $datax->sekretariat_diklat21; $temp_diklat22 = $temp_diklat22 + $datax->sekretariat_diklat22;
										$temp_diklat31 = $temp_diklat31 + $datax->sekretariat_diklat31; $temp_diklat32 = $temp_diklat32 + $datax->sekretariat_diklat32;
										$temp_diklat41 = $temp_diklat41 + $datax->sekretariat_diklat41; $temp_diklat42 = $temp_diklat42 + $datax->sekretariat_diklat42;
										$temp_diklat1 = $temp_diklat1 + $datax->sekretariat_diklat1; $temp_diklat2 = $temp_diklat2 + $datax->sekretariat_diklat2;
								}
								}

								//show it
 								//sekertariat
								$no++;
								$total_jml = $total_jml + $temp_jml;
  							 $total_pria = $total_pria + $temp_pria;
  							 $total_wanita = $total_wanita + $temp_wanita;

  							 $total_e1a1 = $total_e1a1 + $temp_e1a1; $total_e1a2 = $total_e1a2 + $temp_e1a2;
  							 $total_e1b1 = $total_e1b1 + $temp_e1b1; $total_e1b2 = $total_e1b2 + $temp_e1b2;
  							 $total_e2a1 = $total_e2a1 + $temp_e2a1; $total_e2a2 = $total_e2a2 + $temp_e2a2;
  							 $total_e2b1 = $total_e2b1 + $temp_e2b1; $total_e2b2 = $total_e2b2 + $temp_e2b2;
  							 $total_e3a1 = $total_e3a1 + $temp_e3a1; $total_e3a2 = $total_e3a2 + $temp_e3a2;
  							 $total_e3b1 = $total_e3b1 + $temp_e3b1; $total_e3b2 = $total_e3b2 + $temp_e3b2;
  							 $total_e4a1 = $total_e4a1 + $temp_e4a1; $total_e4a2 = $total_e4a2 + $temp_e4a2;
  							 $total_e4b1 = $total_e4b1 + $temp_e4a1; $total_e4b2 = $total_e4b2 + $temp_e4a2;
  							 $total_eselon1 = $total_eselon1 + $temp_eselon1; $total_eselon2 = $total_eselon2 + $temp_eselon2;
  							 $total_g4e1 = $total_g4e1 + $temp_g4e1; $total_g4e2 = $total_g4e2 + $temp_g4e2;
  							 $total_g4d1 = $total_g4d1 + $temp_g4d1; $total_g4d2 = $total_g4d2 + $temp_g4d2;
  							 $total_g4c1 = $total_g4c1 + $temp_g4c1; $total_g4c2 = $total_g4c2 + $temp_g4c2;
  							 $total_g4b1 = $total_g4b1 + $temp_g4b1; $total_g4b2 = $total_g4b2 + $temp_g4b2;
  							 $total_g4a1 = $total_g4a1 + $temp_g4a1; $total_g4a2 = $total_g4a2 + $temp_g4a2;
  							 $total_g41 = $total_g41 + $temp_g41; $total_g42 = $total_g42 + $temp_g42;
  							 $total_g3d1 = $total_g3d1 + $temp_g3d1; $total_g3d2 = $total_g3d2 + $temp_g3d2;
  							 $total_g3c1 = $total_g3c1 + $temp_g3c1; $total_g3c2 = $total_g3c2 + $temp_g3c2;
  							 $total_g3b1 = $total_g3b1 + $temp_g3b1; $total_g3b2 = $total_g3b2 + $temp_g3b2;
  							 $total_g3a1 = $total_g3a1 + $temp_g3a1; $total_g3a2 = $total_g3a2 + $temp_g3a2;
  							 $total_g31 = $total_g31 + $temp_g31; $total_g32 = $total_g32 + $temp_g32;
  							 $total_g2d1 = $total_g2d1 + $temp_g2d1; $total_g2d2 = $total_g2d2 + $temp_g2d2;
  							 $total_g2c1 = $total_g2c1 + $temp_g2c1; $total_g2c2 = $total_g2c2 + $temp_g2c2;
  							 $total_g2b1 = $total_g2b1 + $temp_g2b1; $total_g2b2 = $total_g2b2 + $temp_g2b2;
  							 $total_g2a1 = $total_g2a1 + $temp_g2a1; $total_g2a2 = $total_g2a2 + $temp_g2a2;
  							 $total_g21 = $total_g21 + $temp_g21; $total_g22 = $total_g22 + $temp_g22;
  							 $total_g1d1 = $total_g1d1 + $temp_g1d1; $total_g1d2 = $total_g1d2 + $temp_g1d2;
  							 $total_g1c1 = $total_g1c1 + $temp_g1c1; $total_g1c2 = $total_g1c2 + $temp_g1c2;
  							 $total_g1b1 = $total_g1b1 + $temp_g1b1; $total_g1b2 = $total_g1b2 + $temp_g1b2;
  							 $total_g1a1 = $total_g1a1 + $temp_g1a1; $total_g1a2 = $total_g1a2 + $temp_g1a2;
  							 $total_g11 = $total_g11 + $temp_g11; $total_g12 = $total_g12 + $temp_g12;
  							 $total_g1 = $total_g1 + $temp_g1; $total_g2 = $total_g2 + $temp_g2;
  							 $total_s31 = $total_s31 + $temp_s31; $total_s32 = $total_s32 + $temp_s32;
  							 $total_s21 = $total_s21 + $temp_s21; $total_s22 = $total_s22 + $temp_s22;
  							 $total_s11 = $total_s11 + $temp_s11; $total_s12 = $total_s12 + $temp_s12;
  							 $total_sma1 = $total_sma1 + $temp_sma1; $total_sma2 = $total_sma2 + $temp_sma2;
  							 $total_smp1 = $total_smp1 + $temp_smp1; $total_smp2 = $total_smp2 + $temp_smp2;
  							 $total_sd1 = $total_sd1 + $temp_sd1; $total_sd2 = $total_sd2 + $temp_sd2;
  							 $total_sekolah1 = $total_sekolah1 + $temp_sekolah1; $total_sekolah2 = $total_sekolah2 + $temp_sekolah2;
  							 $total_diklat11 = $total_diklat11 + $temp_diklat11; $total_diklat12 = $total_diklat12 + $temp_diklat12;
  							 $total_diklat21 = $total_diklat21 + $temp_diklat21; $total_diklat22 = $total_diklat22 + $temp_diklat22;
  							 $total_diklat31 = $total_diklat31 + $temp_diklat31; $total_diklat32 = $total_diklat32 + $temp_diklat32;
  							 $total_diklat41 = $total_diklat41 + $temp_diklat41; $total_diklat42 = $total_diklat42 + $temp_diklat42;
  							 $total_diklat1 = $total_diklat1 + $temp_diklat1; $total_diklat2 = $total_diklat2 + $temp_diklat2;
 							 $this->excel->getActiveSheet()->setCellValue('C'.$no, $temp_jml);
 							 $this->excel->getActiveSheet()->setCellValue('D'.$no, $temp_pria);
 							 $this->excel->getActiveSheet()->setCellValue('E'.$no, $temp_wanita);

 							 $this->excel->getActiveSheet()->setCellValue('F'.$no, $temp_e1a1);
 							 $this->excel->getActiveSheet()->setCellValue('G'.$no, $temp_e1a2);
 							 $this->excel->getActiveSheet()->setCellValue('H'.$no, $temp_e1b1);
 							 $this->excel->getActiveSheet()->setCellValue('I'.$no, $temp_e1b2);
 							 $this->excel->getActiveSheet()->setCellValue('J'.$no, $temp_e2a1);
 							 $this->excel->getActiveSheet()->setCellValue('K'.$no, $temp_e2a2);
 							 $this->excel->getActiveSheet()->setCellValue('L'.$no, $temp_e2b1);
 							 $this->excel->getActiveSheet()->setCellValue('M'.$no, $temp_e2b2);
 							 $this->excel->getActiveSheet()->setCellValue('N'.$no, $temp_e3a1);
 							 $this->excel->getActiveSheet()->setCellValue('O'.$no, $temp_e3a2);
 							 $this->excel->getActiveSheet()->setCellValue('P'.$no, $temp_e3b1);
 							 $this->excel->getActiveSheet()->setCellValue('Q'.$no, $temp_e3b2);
 							 $this->excel->getActiveSheet()->setCellValue('R'.$no, $temp_e4a1);
 							 $this->excel->getActiveSheet()->setCellValue('S'.$no, $temp_e4a2);
 							 $this->excel->getActiveSheet()->setCellValue('T'.$no, $temp_e4b1);
 							 $this->excel->getActiveSheet()->setCellValue('U'.$no, $temp_e4b2);
 							 $this->excel->getActiveSheet()->setCellValue('V'.$no, $temp_eselon1);
 							 $this->excel->getActiveSheet()->setCellValue('W'.$no, $temp_eselon2);
 							 $this->excel->getActiveSheet()->setCellValue('X'.$no, $temp_g4e1);
 							 $this->excel->getActiveSheet()->setCellValue('Y'.$no, $temp_g4e2);
 							 $this->excel->getActiveSheet()->setCellValue('Z'.$no, $temp_g4d1);
 							 $this->excel->getActiveSheet()->setCellValue('AA'.$no, $temp_g4d2);
 							 $this->excel->getActiveSheet()->setCellValue('AB'.$no, $temp_g4c1);
 							 $this->excel->getActiveSheet()->setCellValue('AC'.$no, $temp_g4c2);
 							 $this->excel->getActiveSheet()->setCellValue('AD'.$no, $temp_g4b1);
 							 $this->excel->getActiveSheet()->setCellValue('AE'.$no, $temp_g4b2);
 							 $this->excel->getActiveSheet()->setCellValue('AF'.$no, $temp_g4a1);
 							 $this->excel->getActiveSheet()->setCellValue('AG'.$no, $temp_g4a2);
 							 $this->excel->getActiveSheet()->setCellValue('AH'.$no, $temp_g41);
 							 $this->excel->getActiveSheet()->setCellValue('AI'.$no, $temp_g42);
 							 $this->excel->getActiveSheet()->setCellValue('AJ'.$no, $temp_g3d1);
 							 $this->excel->getActiveSheet()->setCellValue('AK'.$no, $temp_g3d2);
 							 $this->excel->getActiveSheet()->setCellValue('AL'.$no, $temp_g3c1);
 							 $this->excel->getActiveSheet()->setCellValue('AM'.$no, $temp_g3c2);
 							 $this->excel->getActiveSheet()->setCellValue('AN'.$no, $temp_g3b1);
 							 $this->excel->getActiveSheet()->setCellValue('AO'.$no, $temp_g3b2);
 							 $this->excel->getActiveSheet()->setCellValue('AP'.$no, $temp_g3a1);
 							 $this->excel->getActiveSheet()->setCellValue('AQ'.$no, $temp_g3a2);
 							 $this->excel->getActiveSheet()->setCellValue('AR'.$no, $temp_g31);
 							 $this->excel->getActiveSheet()->setCellValue('AS'.$no, $temp_g32);
 							 $this->excel->getActiveSheet()->setCellValue('AT'.$no, $temp_g2d1);
 							 $this->excel->getActiveSheet()->setCellValue('AU'.$no, $temp_g2d2);
 							 $this->excel->getActiveSheet()->setCellValue('AV'.$no, $temp_g2c1);
 							 $this->excel->getActiveSheet()->setCellValue('AW'.$no, $temp_g2c2);
 							 $this->excel->getActiveSheet()->setCellValue('AX'.$no, $temp_g2b1);
 							 $this->excel->getActiveSheet()->setCellValue('AY'.$no, $temp_g2b2);
 							 $this->excel->getActiveSheet()->setCellValue('AZ'.$no, $temp_g2a1);
 							 $this->excel->getActiveSheet()->setCellValue('BA'.$no, $temp_g2a2);
 							 $this->excel->getActiveSheet()->setCellValue('BB'.$no, $temp_g21);
 							 $this->excel->getActiveSheet()->setCellValue('BC'.$no, $temp_g22);
 							 $this->excel->getActiveSheet()->setCellValue('BD'.$no, $temp_g1d1);
 							 $this->excel->getActiveSheet()->setCellValue('BE'.$no, $temp_g1d2);
 							 $this->excel->getActiveSheet()->setCellValue('BF'.$no, $temp_g1c1);
 							 $this->excel->getActiveSheet()->setCellValue('BG'.$no, $temp_g1c2);
 							 $this->excel->getActiveSheet()->setCellValue('BH'.$no, $temp_g1b1);
 							 $this->excel->getActiveSheet()->setCellValue('BI'.$no, $temp_g1b2);
 							 $this->excel->getActiveSheet()->setCellValue('BJ'.$no, $temp_g1a1);
 							 $this->excel->getActiveSheet()->setCellValue('BK'.$no, $temp_g1a2);
 							 $this->excel->getActiveSheet()->setCellValue('BL'.$no, $temp_g11);
 							 $this->excel->getActiveSheet()->setCellValue('BM'.$no, $temp_g12);
 							 $this->excel->getActiveSheet()->setCellValue('BN'.$no, $temp_g1);
 							 $this->excel->getActiveSheet()->setCellValue('BO'.$no, $temp_g2);
 							 $this->excel->getActiveSheet()->setCellValue('BP'.$no, $temp_s31);
 							 $this->excel->getActiveSheet()->setCellValue('BQ'.$no, $temp_s32);
 							 $this->excel->getActiveSheet()->setCellValue('BR'.$no, $temp_s21);
 							 $this->excel->getActiveSheet()->setCellValue('BS'.$no, $temp_s22);
 							 $this->excel->getActiveSheet()->setCellValue('BT'.$no, $temp_s11);
 							 $this->excel->getActiveSheet()->setCellValue('BU'.$no, $temp_s12);
 							 $this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
 							 $this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
 							 $this->excel->getActiveSheet()->setCellValue('BX'.$no, $temp_sma1);
 							 $this->excel->getActiveSheet()->setCellValue('BY'.$no, $temp_sma2);
 							 $this->excel->getActiveSheet()->setCellValue('BZ'.$no, $temp_smp1);
 							 $this->excel->getActiveSheet()->setCellValue('CA'.$no, $temp_smp2);
 							 $this->excel->getActiveSheet()->setCellValue('CB'.$no, $temp_sd1);
 							 $this->excel->getActiveSheet()->setCellValue('CC'.$no, $temp_sd2);
 							 $this->excel->getActiveSheet()->setCellValue('CD'.$no, $temp_sekolah1);
 							 $this->excel->getActiveSheet()->setCellValue('CE'.$no, $temp_sekolah2);
 							 $this->excel->getActiveSheet()->setCellValue('CF'.$no, $temp_diklat11);
 							 $this->excel->getActiveSheet()->setCellValue('CG'.$no, $temp_diklat12);
 							 $this->excel->getActiveSheet()->setCellValue('CH'.$no, $temp_diklat21);
 							 $this->excel->getActiveSheet()->setCellValue('CI'.$no, $temp_diklat22);
 							 $this->excel->getActiveSheet()->setCellValue('CJ'.$no, $temp_diklat31);
 							 $this->excel->getActiveSheet()->setCellValue('CK'.$no, $temp_diklat32);
 							 $this->excel->getActiveSheet()->setCellValue('CL'.$no, $temp_diklat41);
 							 $this->excel->getActiveSheet()->setCellValue('CM'.$no, $temp_diklat42);
 							 $this->excel->getActiveSheet()->setCellValue('CN'.$no, $temp_diklat1);
 							 $this->excel->getActiveSheet()->setCellValue('CO'.$no, $temp_diklat2);
							}

							else {
								$data = $this->report_all_model->getValues_jen($id, '');
								foreach ($data as $datax) {
									$no++;
									$total_jml = $total_jml + $datax->sekertariat_all;
									$total_pria = $total_pria + $datax->sekertariat_pria;
									$total_wanita = $total_wanita + $datax->sekertariat_wanita;

									$total_e1a1 = $total_e1a1 + $datax->sekertariat_e1a1;
									$total_e1a2 = $total_e1a2 + $datax->sekertariat_e1a2;
									$total_e1b1 = $total_e1a1 + $datax->sekertariat_e1b1;
									$total_e1b2 = $total_e1a2 + $datax->sekertariat_e1b2;
									$total_e2a1 = $total_e2a1 + $datax->sekertariat_e2a1;
									$total_e2a2 = $total_e2a2 + $datax->sekertariat_e2a2;
									$total_e3a1 = $total_e3a1 + $datax->sekertariat_e3a1;
									$total_e3a2 = $total_e3a2 + $datax->sekertariat_e3a2;
									$total_e3b1 = $total_e3b1 + $datax->sekertariat_e3b1;
									$total_e3b2 = $total_e3b2 + $datax->sekertariat_e3b2;
									$total_e4a1 = $total_e4a1 + $datax->sekertariat_e4a1;
									$total_e4a2 = $total_e4a2 + $datax->sekertariat_e4a2;
									$total_e4b1 = $total_e4b1 + $datax->sekertariat_e4b1;
									$total_e4b2 = $total_e4b2 + $datax->sekertariat_e4b2;
									$total_eselon1 = $total_eselon1 + $datax->sekertariat_eselon1;
									$total_eselon2 = $total_eselon2 + $datax->sekertariat_eselon2;
									$total_g4e1 = $total_g4e1 + $datax->sekertariat_g4e1;
									$total_g4e2 = $total_g4e2 + $datax->sekertariat_g4e2;
									$total_g4d1 = $total_g4d1 + $datax->sekertariat_g4d1;
									$total_g4d2 = $total_g4d2 + $datax->sekertariat_g4d2;
									$total_g4c1 = $total_g4c1 + $datax->sekertariat_g4c1;
									$total_g4c2 = $total_g4c2 + $datax->sekertariat_g4c2;
									$total_g4b1 = $total_g4b1 + $datax->sekertariat_g4b1;
									$total_g4b2 = $total_g4b2 + $datax->sekertariat_g4b2;
									$total_g4a1 = $total_g4a1 + $datax->sekertariat_g4a1;
									$total_g4a2 = $total_g4a2 + $datax->sekertariat_g4a2;
									$total_g41 = $total_g41 + $datax->sekertariat_g41;
									$total_g42 = $total_g42 + $datax->sekertariat_g42;
									$total_g3d1 = $total_g3d1 + $datax->sekertariat_g3d1;
									$total_g3d2 = $total_g3d2 + $datax->sekertariat_g3d2;
									$total_g3c1 = $total_g3c1 + $datax->sekertariat_g3c1;
									$total_g3c2 = $total_g3c2 + $datax->sekertariat_g3c2;
									$total_g3b1 = $total_g3b1 + $datax->sekertariat_g3b1;
									$total_g3b2 = $total_g3b2 + $datax->sekertariat_g3b2;
									$total_g3a1 = $total_g3a1 + $datax->sekertariat_g3a1;
									$total_g3a2 = $total_g3a2 + $datax->sekertariat_g3a2;
									$total_g31 = $total_g31 + $datax->sekertariat_g31;
									$total_g32 = $total_g32 + $datax->sekertariat_g32;
									$total_g2d1 = $total_g2d1 + $datax->sekertariat_g2d1;
									$total_g2d2 = $total_g2d2 + $datax->sekertariat_g2d2;
									$total_g2c1 = $total_g2c1 + $datax->sekertariat_g2c1;
									$total_g2c2 = $total_g2c2 + $datax->sekertariat_g2c2;
									$total_g2b1 = $total_g2b1 + $datax->sekertariat_g2b1;
									$total_g2b2 = $total_g2b2 + $datax->sekertariat_g2b2;
									$total_g2a1 = $total_g2a1 + $datax->sekertariat_g2a1;
									$total_g2a2 = $total_g2a2 + $datax->sekertariat_g2a2;
									$total_g21 = $total_g21 + $datax->sekertariat_g21;
									$total_g22 = $total_g22 + $datax->sekertariat_g22;
									$total_g1d1 = $total_g1d1 + $datax->sekertariat_g1d1;
									$total_g1d2 = $total_g1d2 + $datax->sekertariat_g1d2;
									$total_g1c1 = $total_g1c1 + $datax->sekertariat_g1c1;
									$total_g1c2 = $total_g1c2 + $datax->sekertariat_g1c2;
									$total_g1b1 = $total_g1b1 + $datax->sekertariat_g1b1;
									$total_g1b2 = $total_g1b2 + $datax->sekertariat_g1b2;
									$total_g1a1 = $total_g1a1 + $datax->sekertariat_g1a1;
									$total_g1a2 = $total_g1a2 + $datax->sekertariat_g1a2;
									$total_g11 = $total_g11 + $datax->sekertariat_g11;
									$total_g12 = $total_g12 + $datax->sekertariat_g12;
									$total_g1 = $total_g1 + $datax->sekertariat_g1;
									$total_g2 = $total_g2 + $datax->sekertariat_g2;
									$total_s31 = $total_s31 + $datax->sekertariat_s31;
									$total_s32 = $total_s32 + $datax->sekertariat_s32;
									$total_s21 = $total_s21 + $datax->sekertariat_s21;
									$total_s22 = $total_s22 + $datax->sekertariat_s22;
									$total_s11 = $total_s11 + $datax->sekertariat_s11;
									$total_s12 = $total_s12 + $datax->sekertariat_s12;
									$total_sma1 = $total_sma1 + $datax->sekertariat_sma1;
									$total_sma2 = $total_sma2 + $datax->sekertariat_sma2;
									$total_smp1 = $total_smp1 + $datax->sekertariat_smp1;
									$total_smp2 = $total_smp2 + $datax->sekertariat_smp2;
									$total_sd1 = $total_sd1 + $datax->sekertariat_sd1;
									$total_sd2 = $total_sd2 + $datax->sekertariat_sd2;
									$total_sekolah1 = $total_sekolah1 + $datax->sekertariat_sekolah1;
									$total_sekolah2 = $total_sekolah2 + $datax->sekertariat_sekolah2;
									$total_diklat11 = $total_diklat11 + $datax->sekretariat_diklat12;
									$total_diklat12 = $total_diklat12 + $datax->sekretariat_diklat12;
									$total_diklat21 = $total_diklat21 + $datax->sekretariat_diklat21;
									$total_diklat22 = $total_diklat22 + $datax->sekretariat_diklat22;
									$total_diklat31 = $total_diklat31 + $datax->sekretariat_diklat31;
									$total_diklat32 = $total_diklat32 + $datax->sekretariat_diklat32;
									$total_diklat41 = $total_diklat41 + $datax->sekretariat_diklat41;
									$total_diklat42 = $total_diklat42 + $datax->sekretariat_diklat42;
									$total_diklat1 = $total_diklat1 + $datax->sekretariat_diklat1;
									$total_diklat2 = $total_diklat2 + $datax->sekretariat_diklat2;

								//sekertariat
								$this->excel->getActiveSheet()->setCellValue('C'.$no, $datax->sekertariat_all);
								$this->excel->getActiveSheet()->setCellValue('D'.$no, $datax->sekertariat_pria);
								$this->excel->getActiveSheet()->setCellValue('E'.$no, $datax->sekertariat_wanita);

								$this->excel->getActiveSheet()->setCellValue('F'.$no, $datax->sekertariat_e1a1);
								$this->excel->getActiveSheet()->setCellValue('G'.$no, $datax->sekertariat_e1a2);
								$this->excel->getActiveSheet()->setCellValue('H'.$no, $datax->sekertariat_e1b1);
								$this->excel->getActiveSheet()->setCellValue('I'.$no, $datax->sekertariat_e1b2);
								$this->excel->getActiveSheet()->setCellValue('J'.$no, $datax->sekertariat_e2a1);
								$this->excel->getActiveSheet()->setCellValue('K'.$no, $datax->sekertariat_e2a2);
								$this->excel->getActiveSheet()->setCellValue('L'.$no, $datax->sekertariat_e2b1);
								$this->excel->getActiveSheet()->setCellValue('M'.$no, $datax->sekertariat_e2b2);
								$this->excel->getActiveSheet()->setCellValue('N'.$no, $datax->sekertariat_e3a1);
								$this->excel->getActiveSheet()->setCellValue('O'.$no, $datax->sekertariat_e3a2);
								$this->excel->getActiveSheet()->setCellValue('P'.$no, $datax->sekertariat_e3b1);
								$this->excel->getActiveSheet()->setCellValue('Q'.$no, $datax->sekertariat_e3b2);
								$this->excel->getActiveSheet()->setCellValue('R'.$no, $datax->sekertariat_e4a1);
								$this->excel->getActiveSheet()->setCellValue('S'.$no, $datax->sekertariat_e4a2);
								$this->excel->getActiveSheet()->setCellValue('T'.$no, $datax->sekertariat_e4b1);
								$this->excel->getActiveSheet()->setCellValue('U'.$no, $datax->sekertariat_e4b2);
								$this->excel->getActiveSheet()->setCellValue('V'.$no, $datax->sekertariat_eselon1);
								$this->excel->getActiveSheet()->setCellValue('W'.$no, $datax->sekertariat_eselon2);
								$this->excel->getActiveSheet()->setCellValue('X'.$no, $datax->sekertariat_g4e1);
								$this->excel->getActiveSheet()->setCellValue('Y'.$no, $datax->sekertariat_g4e2);
								$this->excel->getActiveSheet()->setCellValue('Z'.$no, $datax->sekertariat_g4d1);
								$this->excel->getActiveSheet()->setCellValue('AA'.$no, $datax->sekertariat_g4d2);
								$this->excel->getActiveSheet()->setCellValue('AB'.$no, $datax->sekertariat_g4c1);
								$this->excel->getActiveSheet()->setCellValue('AC'.$no, $datax->sekertariat_g4c2);
								$this->excel->getActiveSheet()->setCellValue('AD'.$no, $datax->sekertariat_g4b1);
								$this->excel->getActiveSheet()->setCellValue('AE'.$no, $datax->sekertariat_g4b2);
								$this->excel->getActiveSheet()->setCellValue('AF'.$no, $datax->sekertariat_g4a1);
								$this->excel->getActiveSheet()->setCellValue('AG'.$no, $datax->sekertariat_g4a2);
								$this->excel->getActiveSheet()->setCellValue('AH'.$no, $datax->sekertariat_g41);
								$this->excel->getActiveSheet()->setCellValue('AI'.$no, $datax->sekertariat_g42);
								$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $datax->sekertariat_g3d1);
								$this->excel->getActiveSheet()->setCellValue('AK'.$no, $datax->sekertariat_g3d2);
								$this->excel->getActiveSheet()->setCellValue('AL'.$no, $datax->sekertariat_g3c1);
								$this->excel->getActiveSheet()->setCellValue('AM'.$no, $datax->sekertariat_g3c2);
								$this->excel->getActiveSheet()->setCellValue('AN'.$no, $datax->sekertariat_g3b1);
								$this->excel->getActiveSheet()->setCellValue('AO'.$no, $datax->sekertariat_g3b2);
								$this->excel->getActiveSheet()->setCellValue('AP'.$no, $datax->sekertariat_g3a1);
								$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $datax->sekertariat_g3a2);
								$this->excel->getActiveSheet()->setCellValue('AR'.$no, $datax->sekertariat_g31);
								$this->excel->getActiveSheet()->setCellValue('AS'.$no, $datax->sekertariat_g32);
								$this->excel->getActiveSheet()->setCellValue('AT'.$no, $datax->sekertariat_g2d1);
								$this->excel->getActiveSheet()->setCellValue('AU'.$no, $datax->sekertariat_g2d2);
								$this->excel->getActiveSheet()->setCellValue('AV'.$no, $datax->sekertariat_g2c1);
								$this->excel->getActiveSheet()->setCellValue('AW'.$no, $datax->sekertariat_g2c2);
								$this->excel->getActiveSheet()->setCellValue('AX'.$no, $datax->sekertariat_g2b1);
								$this->excel->getActiveSheet()->setCellValue('AY'.$no, $datax->sekertariat_g2b2);
								$this->excel->getActiveSheet()->setCellValue('AZ'.$no, $datax->sekertariat_g2a1);
								$this->excel->getActiveSheet()->setCellValue('BA'.$no, $datax->sekertariat_g2a2);
								$this->excel->getActiveSheet()->setCellValue('BB'.$no, $datax->sekertariat_g21);
								$this->excel->getActiveSheet()->setCellValue('BC'.$no, $datax->sekertariat_g22);
								$this->excel->getActiveSheet()->setCellValue('BD'.$no, $datax->sekertariat_g1d1);
								$this->excel->getActiveSheet()->setCellValue('BE'.$no, $datax->sekertariat_g1d2);
								$this->excel->getActiveSheet()->setCellValue('BF'.$no, $datax->sekertariat_g1c1);
								$this->excel->getActiveSheet()->setCellValue('BG'.$no, $datax->sekertariat_g1c2);
								$this->excel->getActiveSheet()->setCellValue('BH'.$no, $datax->sekertariat_g1b1);
								$this->excel->getActiveSheet()->setCellValue('BI'.$no, $datax->sekertariat_g1b2);
								$this->excel->getActiveSheet()->setCellValue('BJ'.$no, $datax->sekertariat_g1a1);
								$this->excel->getActiveSheet()->setCellValue('BK'.$no, $datax->sekertariat_g1a2);
								$this->excel->getActiveSheet()->setCellValue('BL'.$no, $datax->sekertariat_g11);
								$this->excel->getActiveSheet()->setCellValue('BM'.$no, $datax->sekertariat_g12);
								$this->excel->getActiveSheet()->setCellValue('BN'.$no, $datax->sekertariat_g1);
								$this->excel->getActiveSheet()->setCellValue('BO'.$no, $datax->sekertariat_g2);
								$this->excel->getActiveSheet()->setCellValue('BP'.$no, $datax->sekertariat_s31);
								$this->excel->getActiveSheet()->setCellValue('BQ'.$no, $datax->sekertariat_s32);
								$this->excel->getActiveSheet()->setCellValue('BR'.$no, $datax->sekertariat_s21);
								$this->excel->getActiveSheet()->setCellValue('BS'.$no, $datax->sekertariat_s22);
								$this->excel->getActiveSheet()->setCellValue('BT'.$no, $datax->sekertariat_s11);
								$this->excel->getActiveSheet()->setCellValue('BU'.$no, $datax->sekertariat_s12);
								$this->excel->getActiveSheet()->setCellValue('BV'.$no, 0);
								$this->excel->getActiveSheet()->setCellValue('BW'.$no, 0);
								//$this->excel->getActiveSheet()->setCellValue('AM'.$no, $datax->sekertariat_s1);
								$this->excel->getActiveSheet()->setCellValue('BX'.$no, $datax->sekertariat_sma1);
								$this->excel->getActiveSheet()->setCellValue('BY'.$no, $datax->sekertariat_sma2);
								$this->excel->getActiveSheet()->setCellValue('BZ'.$no, $datax->sekertariat_smp1);
								$this->excel->getActiveSheet()->setCellValue('CA'.$no, $datax->sekertariat_smp2);
								$this->excel->getActiveSheet()->setCellValue('CB'.$no, $datax->sekertariat_sd1);
								$this->excel->getActiveSheet()->setCellValue('CC'.$no, $datax->sekertariat_sd2);
								$this->excel->getActiveSheet()->setCellValue('CD'.$no, $datax->sekertariat_sekolah1);
								$this->excel->getActiveSheet()->setCellValue('CE'.$no, $datax->sekertariat_sekolah2);
								$this->excel->getActiveSheet()->setCellValue('CF'.$no, $datax->sekretariat_diklat11);
								$this->excel->getActiveSheet()->setCellValue('CG'.$no, $datax->sekretariat_diklat12);
								$this->excel->getActiveSheet()->setCellValue('CH'.$no, $datax->sekretariat_diklat21);
								$this->excel->getActiveSheet()->setCellValue('CI'.$no, $datax->sekretariat_diklat22);
								$this->excel->getActiveSheet()->setCellValue('CJ'.$no, $datax->sekretariat_diklat31);
								$this->excel->getActiveSheet()->setCellValue('CK'.$no, $datax->sekretariat_diklat32);
								$this->excel->getActiveSheet()->setCellValue('CL'.$no, $datax->sekretariat_diklat41);
								$this->excel->getActiveSheet()->setCellValue('CM'.$no, $datax->sekretariat_diklat42);
								$this->excel->getActiveSheet()->setCellValue('CN'.$no, $datax->sekretariat_diklat1);
								$this->excel->getActiveSheet()->setCellValue('CO'.$no, $datax->sekretariat_diklat2);


							}
						}
					}
				}
			  //set row jumlah
				$this->excel->getActiveSheet()->getStyle('A60:CO60')->getFont()->setBold(true);
			  $this->excel->getActiveSheet()->mergeCells('A60:B60');
        $this->excel->getActiveSheet()->setCellValue('A60', 'JUMLAH');
			  $this->excel->getActiveSheet()->setCellValue('C60', $total_jml);
			  $this->excel->getActiveSheet()->setCellValue('D60', $total_pria);
			  $this->excel->getActiveSheet()->setCellValue('E60', $total_wanita);

			  $this->excel->getActiveSheet()->setCellValue('F60', $total_e1a1);
				$this->excel->getActiveSheet()->setCellValue('G60', $total_e1a2);
			  $this->excel->getActiveSheet()->setCellValue('H60', $total_e1b1);
				$this->excel->getActiveSheet()->setCellValue('I60', $total_e1b2);
			  $this->excel->getActiveSheet()->setCellValue('J60', $total_e2a1);
				$this->excel->getActiveSheet()->setCellValue('K60', $total_e2a2);
			  $this->excel->getActiveSheet()->setCellValue('L60', $total_e2b1);
				$this->excel->getActiveSheet()->setCellValue('M60', $total_e2b2);
			  $this->excel->getActiveSheet()->setCellValue('N60', $total_e3a1);
				$this->excel->getActiveSheet()->setCellValue('O60', $total_e3a2);
			  $this->excel->getActiveSheet()->setCellValue('P60', $total_e3b1);
				$this->excel->getActiveSheet()->setCellValue('Q60', $total_e3b2);
			  $this->excel->getActiveSheet()->setCellValue('R60', $total_e4a1);
				$this->excel->getActiveSheet()->setCellValue('S60', $total_e4a2);
			  $this->excel->getActiveSheet()->setCellValue('T60', $total_e4b1);
				$this->excel->getActiveSheet()->setCellValue('U60', $total_e4b2);
			  $this->excel->getActiveSheet()->setCellValue('V60', $total_eselon1);
				$this->excel->getActiveSheet()->setCellValue('W60', $total_eselon2);
			  $this->excel->getActiveSheet()->setCellValue('X60', $total_g4e1);
				$this->excel->getActiveSheet()->setCellValue('Y60', $total_g4e2);
			  $this->excel->getActiveSheet()->setCellValue('Z60', $total_g4d1);
				$this->excel->getActiveSheet()->setCellValue('AA60', $total_g4d2);
			  $this->excel->getActiveSheet()->setCellValue('AB60', $total_g4c1);
				$this->excel->getActiveSheet()->setCellValue('AC60', $total_g4c2);
			  $this->excel->getActiveSheet()->setCellValue('AD60', $total_g4b1);
				$this->excel->getActiveSheet()->setCellValue('AE60', $total_g4b2);
			  $this->excel->getActiveSheet()->setCellValue('AF60', $total_g4a1);
				$this->excel->getActiveSheet()->setCellValue('AG60', $total_g4a2);
			  $this->excel->getActiveSheet()->setCellValue('AH60', $total_g41);
				$this->excel->getActiveSheet()->setCellValue('AI60', $total_g42);
			  $this->excel->getActiveSheet()->setCellValue('AJ60', $total_g3d1);
				$this->excel->getActiveSheet()->setCellValue('AK60', $total_g3d2);
			  $this->excel->getActiveSheet()->setCellValue('AL60', $total_g3c1);
				$this->excel->getActiveSheet()->setCellValue('AM60', $total_g3c2);
			  $this->excel->getActiveSheet()->setCellValue('AN60', $total_g3b1);
				$this->excel->getActiveSheet()->setCellValue('AO60', $total_g3b2);
			  $this->excel->getActiveSheet()->setCellValue('AP60', $total_g3a1);
				$this->excel->getActiveSheet()->setCellValue('AQ60', $total_g3a2);
			  $this->excel->getActiveSheet()->setCellValue('AR60', $total_g31);
				$this->excel->getActiveSheet()->setCellValue('AS60', $total_g32);
			  $this->excel->getActiveSheet()->setCellValue('AT60', $total_g2d1);
				$this->excel->getActiveSheet()->setCellValue('AU60', $total_g2d2);
			  $this->excel->getActiveSheet()->setCellValue('AV60', $total_g2c1);
				$this->excel->getActiveSheet()->setCellValue('AW60', $total_g2c2);
			  $this->excel->getActiveSheet()->setCellValue('AX60', $total_g2b1);
				$this->excel->getActiveSheet()->setCellValue('AY60', $total_g2b2);
			  $this->excel->getActiveSheet()->setCellValue('AZ60', $total_g2a1);
				$this->excel->getActiveSheet()->setCellValue('BA60', $total_g2a2);
			  $this->excel->getActiveSheet()->setCellValue('BB60', $total_g21);
				$this->excel->getActiveSheet()->setCellValue('BC60', $total_g22);
			  $this->excel->getActiveSheet()->setCellValue('BD60', $total_g1d1);
				$this->excel->getActiveSheet()->setCellValue('BE60', $total_g1d2);
			  $this->excel->getActiveSheet()->setCellValue('BF60', $total_g1c1);
				$this->excel->getActiveSheet()->setCellValue('BG60', $total_g1c2);
			  $this->excel->getActiveSheet()->setCellValue('BH60', $total_g1b1);
				$this->excel->getActiveSheet()->setCellValue('BI60', $total_g1b2);
			  $this->excel->getActiveSheet()->setCellValue('BJ60', $total_g1a1);
				$this->excel->getActiveSheet()->setCellValue('BK60', $total_g1a2);
			  $this->excel->getActiveSheet()->setCellValue('BL60', $total_g11);
				$this->excel->getActiveSheet()->setCellValue('BM60', $total_g12);
			  $this->excel->getActiveSheet()->setCellValue('BN60', $total_g1);
				$this->excel->getActiveSheet()->setCellValue('BO60', $total_g2);
			  $this->excel->getActiveSheet()->setCellValue('BP60', $total_s31);
				$this->excel->getActiveSheet()->setCellValue('BQ60', $total_s32);
			  $this->excel->getActiveSheet()->setCellValue('BR60', $total_s21);
				$this->excel->getActiveSheet()->setCellValue('BS60', $total_s22);
			  $this->excel->getActiveSheet()->setCellValue('BT60', $total_s11);
				$this->excel->getActiveSheet()->setCellValue('BU60', $total_s12);
				$this->excel->getActiveSheet()->setCellValue('BV60', 0);
				$this->excel->getActiveSheet()->setCellValue('BW60', 0);
			  //$this->excel->getActiveSheet()->setCellValue('AN60', $total_sma);
			  $this->excel->getActiveSheet()->setCellValue('BX60', $total_sma1);
				$this->excel->getActiveSheet()->setCellValue('BY60', $total_sma2);
			  $this->excel->getActiveSheet()->setCellValue('BZ60', $total_smp1);
				$this->excel->getActiveSheet()->setCellValue('CA60', $total_smp2);
			  $this->excel->getActiveSheet()->setCellValue('CB60', $total_sd1);
				$this->excel->getActiveSheet()->setCellValue('CC60', $total_sd2);
			  $this->excel->getActiveSheet()->setCellValue('CD60', $total_sekolah1);
				$this->excel->getActiveSheet()->setCellValue('CE60', $total_sekolah2);
			  $this->excel->getActiveSheet()->setCellValue('CF60', $total_diklat11);
				$this->excel->getActiveSheet()->setCellValue('CG60', $total_diklat12);
			  $this->excel->getActiveSheet()->setCellValue('CH60', $total_diklat21);
				$this->excel->getActiveSheet()->setCellValue('CI60', $total_diklat22);
			  $this->excel->getActiveSheet()->setCellValue('CJ60', $total_diklat31);
				$this->excel->getActiveSheet()->setCellValue('CK60', $total_diklat32);
			  $this->excel->getActiveSheet()->setCellValue('CL60', $total_diklat41);
				$this->excel->getActiveSheet()->setCellValue('CM60', $total_diklat42);
			  $this->excel->getActiveSheet()->setCellValue('CN60', $total_diklat1);
				$this->excel->getActiveSheet()->setCellValue('CO60', $total_diklat2);




			  // $this->excel->getActiveSheet()->setCellValue('C59', $total_jml);
			  // $this->excel->getActiveSheet()->setCellValue('D59', $total_pria);
			  // $this->excel->getActiveSheet()->setCellValue('E59', $total_wanita);
			  // $this->excel->getActiveSheet()->setCellValue('F59', $total_e1a);
			  // $this->excel->getActiveSheet()->setCellValue('G59', $total_e1a);
			  // $this->excel->getActiveSheet()->setCellValue('H59', $total_e2a);
			  // $this->excel->getActiveSheet()->setCellValue('I59', $total_e2b);
			  // $this->excel->getActiveSheet()->setCellValue('J59', $total_e3a);
			  // $this->excel->getActiveSheet()->setCellValue('K59', $total_e3b);
			  // $this->excel->getActiveSheet()->setCellValue('L59', $total_e4a);
			  // $this->excel->getActiveSheet()->setCellValue('M59', $total_e4b);
			  // $this->excel->getActiveSheet()->setCellValue('N59', $total_eselon);
			  // $this->excel->getActiveSheet()->setCellValue('O59', $total_g4e);
			  // $this->excel->getActiveSheet()->setCellValue('P59', $total_g4d);
			  // $this->excel->getActiveSheet()->setCellValue('Q59', $total_g4c);
			  // $this->excel->getActiveSheet()->setCellValue('R59', $total_g4b);
			  // $this->excel->getActiveSheet()->setCellValue('S59', $total_g4a);
			  // $this->excel->getActiveSheet()->setCellValue('T59', $total_g4);
			  // $this->excel->getActiveSheet()->setCellValue('U59', $total_g3d);
			  // $this->excel->getActiveSheet()->setCellValue('V59', $total_g3c);
			  // $this->excel->getActiveSheet()->setCellValue('W59', $total_g3b);
			  // $this->excel->getActiveSheet()->setCellValue('X59', $total_g3a);
			  // $this->excel->getActiveSheet()->setCellValue('Y59', $total_g3);
			  // $this->excel->getActiveSheet()->setCellValue('Z59', $total_g2d);
			  // $this->excel->getActiveSheet()->setCellValue('AA59', $total_g2c);
			  // $this->excel->getActiveSheet()->setCellValue('AB59', $total_g2b);
			  // $this->excel->getActiveSheet()->setCellValue('AC59', $total_g2a);
			  // $this->excel->getActiveSheet()->setCellValue('AD59', $total_g2);
			  // $this->excel->getActiveSheet()->setCellValue('AE59', $total_g1d);
			  // $this->excel->getActiveSheet()->setCellValue('AF59', $total_g1c);
			  // $this->excel->getActiveSheet()->setCellValue('AG59', $total_g1b);
			  // $this->excel->getActiveSheet()->setCellValue('AH59', $total_g1a);
			  // $this->excel->getActiveSheet()->setCellValue('AI59', $total_g1);
			  // $this->excel->getActiveSheet()->setCellValue('AJ59', $total_g);
			  // $this->excel->getActiveSheet()->setCellValue('AK59', $total_s3);
			  // $this->excel->getActiveSheet()->setCellValue('AL59', $total_s2);
			  // $this->excel->getActiveSheet()->setCellValue('AM59', $total_s1);
			  // //$this->excel->getActiveSheet()->setCellValue('AN59', $total_sma);
			  // $this->excel->getActiveSheet()->setCellValue('AO59', $total_sma);
			  // $this->excel->getActiveSheet()->setCellValue('AP59', $total_smp);
			  // $this->excel->getActiveSheet()->setCellValue('AQ59', $total_sd);
			  // $this->excel->getActiveSheet()->setCellValue('AR59', $total_sekolah);
			  // $this->excel->getActiveSheet()->setCellValue('AS59', $total_diklat1);
			  // $this->excel->getActiveSheet()->setCellValue('AT59', $total_diklat2);
			  // $this->excel->getActiveSheet()->setCellValue('AU59', $total_diklat3);
			  // $this->excel->getActiveSheet()->setCellValue('AV59', $total_diklat4);
			  // $this->excel->getActiveSheet()->setCellValue('AW59', $total_diklat);

			  $this->excel->getActiveSheet()->getStyle('A5:CO60')->applyFromArray($styleArray);
			  //$this->excel->getActiveSheet()->getStyle('A5:AW'.($no-1))->applyFromArray($styleArray);
			  $this->excel->getActiveSheet()->getStyle("A5:CO8")->applyFromArray($styleCenter);
			  $this->excel->getActiveSheet()->getStyle("A9:A60")->applyFromArray($styleCenter);
			  $this->excel->getActiveSheet()->getStyle("C9:CO60")->applyFromArray($styleCenter);

			$this->excel->getActiveSheet()->getStyle('AD62')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD62', 'Kendari,          '.$bulan.' '.date('Y'));
			$this->excel->getActiveSheet()->getStyle('AA64')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AA64', 'KEPALA BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA');
			$this->excel->getActiveSheet()->getStyle('AD65')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD65', 'KOTA KENDARI');
			$this->excel->getActiveSheet()->getStyle('AD70')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD70', 'ZAINAL ARIFIN S.Sos, M.Si');
			$this->excel->getActiveSheet()->getStyle('AD71')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD71', 'NIP. 195904061981031016');


			ob_end_clean();
                  $filename='Rekapitulasi.xls'; //save our workbook as this file name
                  header('Content-Type: application/vnd.ms-excel'); //mime type
                  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                  header('Cache-Control: max-age=0'); //no cache

                  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                  //if you want to save it as .XLSX Excel 2007 format
                  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');

           //redirect('report/report_all','refresh');

		} else {
			$data = array(
						'bulan'=>null,
						'nama'=>null
				);
				$this->template->admin_render('report/report_all/form',$this->inputSetting($data));
		}


	}

	public function add_skpd(){
		if($this->input->post('submit')){

		$kd_skpd = $this->input->post('nama_skpd');

		$nama_skpd = $this->report_all_model->getName($kd_skpd);

		$this->load->library('excel');

		$bulan = date('m');

		if($bulan==1) {
			$bulan = "Januari";
		} else if($bulan==2) {
			$bulan = "Februari";
		} else if($bulan==3) {
			$bulan = "Maret";
		} else if($bulan==4) {
			$bulan = "April";
		} else if($bulan==5) {
			$bulan = "Mei";
		} else if($bulan==6) {
			$bulan = "Juni";
		} else if($bulan==7) {
			$bulan = "Juli";
		} else if($bulan==8) {
			$bulan = "Agustus";
		} else if($bulan==9) {
			$bulan = "September";
		} else if($bulan==10) {
			$bulan = "Oktober";
		} else if($bulan==11) {
			$bulan = "November";
		} else if($bulan==12) {
			$bulan = "Desember";
		}

			//load PHPExcel library
  		$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
                  $this->excel->getActiveSheet()->setTitle('Rekapitulasi Pegawai Dinas');

  				//STYLING
  				$styleArray = array(
								'borders' => array('allborders' =>
												array('style' => PHPExcel_Style_Border::BORDER_THIN,
												'color' =>	array('argb' => '0000'),
  							),
  						),

  					);
				$styleCenter = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);

			$no = 8;

			//data
			$this->excel->getActiveSheet()->mergeCells('A1:AQ1');
              $this->excel->getActiveSheet()->setCellValue('A1', 'REKAPITULASI JUMLAH PEGAWAI NEGERI SIPIL');
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('A2:AQ2');
              $this->excel->getActiveSheet()->setCellValue('A2', ''.strtoupper($nama_skpd));
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('A3:AQ3');
              $this->excel->getActiveSheet()->setCellValue('A3', 'PERIODE '.strtoupper($bulan).' 2016');
              $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(12);

			  //set font
			  $this->excel->getActiveSheet()->getStyle('A5:AW68')->getFont()->setSize(10);

			  //INIT WIDTH
			  $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			  $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			  $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AF')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AG')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AH')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AI')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AJ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AK')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AL')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AM')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AN')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AO')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AP')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AQ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AR')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AS')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AT')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AU')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AV')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AW')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AX')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AY')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('AZ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BA')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BB')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BC')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BD')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BE')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BF')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BG')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BH')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BI')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BJ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BK')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BL')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BM')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BN')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BO')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BP')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BQ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BR')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BS')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BT')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BU')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BV')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BW')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BX')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BY')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('BZ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CA')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CB')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CC')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CD')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CE')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CF')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CG')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CH')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CI')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CJ')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('CK')->setWidth(5);

			  //set column name
			  $this->excel->getActiveSheet()->mergeCells('A5:A7');
              $this->excel->getActiveSheet()->setCellValue('A5', 'No');
			  $this->excel->getActiveSheet()->mergeCells('B5:B7');
			  $this->excel->getActiveSheet()->setCellValue('B5', 'Unit Kerja');
			  $this->excel->getActiveSheet()->mergeCells('C5:C7');
			  $this->excel->getActiveSheet()->setCellValue('C5', 'JML');
			  $this->excel->getActiveSheet()->mergeCells('D5:E6');
			  $this->excel->getActiveSheet()->setCellValue('D5', 'Kelamin');
			  $this->excel->getActiveSheet()->setCellValue('D7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('E7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('F5:U5');
			  $this->excel->getActiveSheet()->setCellValue('F5', 'ESSELON');
			  $this->excel->getActiveSheet()->mergeCells('F6:G6');
			  $this->excel->getActiveSheet()->setCellValue('F6', 'I.a');
			  $this->excel->getActiveSheet()->setCellValue('F7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('G7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('H6:I6');
			  $this->excel->getActiveSheet()->setCellValue('H6', 'I.b');
			  $this->excel->getActiveSheet()->setCellValue('H7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('I7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('J6:K6');
			  $this->excel->getActiveSheet()->setCellValue('J6', 'II.a');
			  $this->excel->getActiveSheet()->setCellValue('J7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('K7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('L6:M6');
			  $this->excel->getActiveSheet()->setCellValue('L6', 'II.b');
			  $this->excel->getActiveSheet()->setCellValue('L7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('M7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('N6:O6');
			  $this->excel->getActiveSheet()->setCellValue('N6', 'III.a');
			  $this->excel->getActiveSheet()->setCellValue('N7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('O7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('P6:Q6');
			  $this->excel->getActiveSheet()->setCellValue('P6', 'III.b');
			  $this->excel->getActiveSheet()->setCellValue('P7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('Q7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('R6:S6');
			  $this->excel->getActiveSheet()->setCellValue('R6', 'IV.a');
			  $this->excel->getActiveSheet()->setCellValue('R7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('S7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('T6:U6');
			  $this->excel->getActiveSheet()->setCellValue('T6', 'IV.b');
			  $this->excel->getActiveSheet()->setCellValue('T7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('U7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('V5:AG5');
			  $this->excel->getActiveSheet()->setCellValue('V5', 'Golongan IV');
			  $this->excel->getActiveSheet()->mergeCells('V6:W6');
			  $this->excel->getActiveSheet()->setCellValue('V6', 'e');
			  $this->excel->getActiveSheet()->setCellValue('V7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('W7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('X6:Y6');
			  $this->excel->getActiveSheet()->setCellValue('X6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('X7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('Y7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('Z6:AA6');
			  $this->excel->getActiveSheet()->setCellValue('Z6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('Z7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AA7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AB6:AC6');
			  $this->excel->getActiveSheet()->setCellValue('AB6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('AB7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AC7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AD6:AE6');
			  $this->excel->getActiveSheet()->setCellValue('AD6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('AD7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AE7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AF6:AG6');
			  $this->excel->getActiveSheet()->setCellValue('AF6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('AF7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AG7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('AH5:AQ5');
			  $this->excel->getActiveSheet()->setCellValue('AH5', 'Golongan III');
			  $this->excel->getActiveSheet()->mergeCells('AH6:AI6');
			  $this->excel->getActiveSheet()->setCellValue('AH6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('AH7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AI7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AJ6:AK6');
			  $this->excel->getActiveSheet()->setCellValue('AJ6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('AJ7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AK7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AL6:AM6');
			  $this->excel->getActiveSheet()->setCellValue('AL6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('AL7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AM7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AN6:AO6');
			  $this->excel->getActiveSheet()->setCellValue('AN6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('AN7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AO7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AP6:AQ6');
			  $this->excel->getActiveSheet()->setCellValue('AP6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('AP7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AQ7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('AR5:BA5');
			  $this->excel->getActiveSheet()->setCellValue('AR5', 'Golongan II');
			  $this->excel->getActiveSheet()->mergeCells('AR6:AS6');
			  $this->excel->getActiveSheet()->setCellValue('AR6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('AR7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AS7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AT6:AU6');
			  $this->excel->getActiveSheet()->setCellValue('AT6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('AT7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AU7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AV6:AW6');
			  $this->excel->getActiveSheet()->setCellValue('AV6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('AV7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AW7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AX6:AY6');
			  $this->excel->getActiveSheet()->setCellValue('AX6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('AX7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('AY7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('AZ6:BA6');
			  $this->excel->getActiveSheet()->setCellValue('AZ6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('AZ7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BA7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('BB5:BK5');
			  $this->excel->getActiveSheet()->setCellValue('BB5', 'Golongan I');
			  $this->excel->getActiveSheet()->mergeCells('BB6:BC6');
			  $this->excel->getActiveSheet()->setCellValue('BB6', 'd');
			  $this->excel->getActiveSheet()->setCellValue('BB7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BC7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BD6:BE6');
			  $this->excel->getActiveSheet()->setCellValue('BD6', 'c');
			  $this->excel->getActiveSheet()->setCellValue('BD7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BE7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BF6:BG6');
			  $this->excel->getActiveSheet()->setCellValue('BF6', 'b');
			  $this->excel->getActiveSheet()->setCellValue('BF7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BG7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BH6:BI6');
			  $this->excel->getActiveSheet()->setCellValue('BH6', 'a');
			  $this->excel->getActiveSheet()->setCellValue('BH7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BI7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BJ6:BK6');
			  $this->excel->getActiveSheet()->setCellValue('BJ6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('BJ7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BK7', 'P');

			  $this->excel->getActiveSheet()->mergeCells('BL5:CA5');
			  $this->excel->getActiveSheet()->setCellValue('BL5', 'TINGKAT PENDIDIKAN');
			  $this->excel->getActiveSheet()->mergeCells('BL6:BM6');
			  $this->excel->getActiveSheet()->setCellValue('BL6', 'S3');
			  $this->excel->getActiveSheet()->setCellValue('BL7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BM7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BN6:BO6');
			  $this->excel->getActiveSheet()->setCellValue('BN6', 'S2');
			  $this->excel->getActiveSheet()->setCellValue('BN7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BO7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BP6:BQ6');
			  $this->excel->getActiveSheet()->setCellValue('BP6', 'S1');
			  $this->excel->getActiveSheet()->setCellValue('BP7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BQ7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BR6:BS6');
			  $this->excel->getActiveSheet()->setCellValue('BR6', 'SM');
			  $this->excel->getActiveSheet()->setCellValue('BR7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BS7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BT6:BU6');
			  $this->excel->getActiveSheet()->setCellValue('BT6', 'SMA');
			  $this->excel->getActiveSheet()->setCellValue('BT7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BU7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BV6:BW6');
			  $this->excel->getActiveSheet()->setCellValue('BV6', 'SMP');
			  $this->excel->getActiveSheet()->setCellValue('BV7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BW7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BX6:BY6');
			  $this->excel->getActiveSheet()->setCellValue('BX6', 'SD');
			  $this->excel->getActiveSheet()->setCellValue('BX7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('BY7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('BZ6:CA6');
			  $this->excel->getActiveSheet()->setCellValue('BZ6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('BZ7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('CA7', 'P');


			  $this->excel->getActiveSheet()->mergeCells('CB5:CK5');
			  $this->excel->getActiveSheet()->setCellValue('CB5', 'DIKLAT');
			  $this->excel->getActiveSheet()->mergeCells('CB6:CC6');
			  $this->excel->getActiveSheet()->setCellValue('CB6', 'Tingkat I');
			  $this->excel->getActiveSheet()->setCellValue('CB7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('CC7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('CD6:CE6');
			  $this->excel->getActiveSheet()->setCellValue('CD6', 'Tingkat II');
			  $this->excel->getActiveSheet()->setCellValue('CD7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('CE7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('CF6:CG6');
			  $this->excel->getActiveSheet()->setCellValue('CF6', 'Tingkat III');
			  $this->excel->getActiveSheet()->setCellValue('CF7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('CG7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('CH6:CI6');
			  $this->excel->getActiveSheet()->setCellValue('CH6', 'Tingkat IV');
			  $this->excel->getActiveSheet()->setCellValue('CH7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('CI7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('CJ6:CK6');
			  $this->excel->getActiveSheet()->setCellValue('CJ6', 'JML');
			  $this->excel->getActiveSheet()->setCellValue('CJ7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('CK7', 'P');


			  $this->excel->getActiveSheet()->setCellValue('A8', '1');
			  $this->excel->getActiveSheet()->setCellValue('B8', '2');
			  $this->excel->getActiveSheet()->setCellValue('C8', '3');
			  $this->excel->getActiveSheet()->setCellValue('D8', '4');
			  $this->excel->getActiveSheet()->setCellValue('E8', '5');
			  $this->excel->getActiveSheet()->setCellValue('F8', '6');
			  $this->excel->getActiveSheet()->setCellValue('G8', '7');
			  $this->excel->getActiveSheet()->setCellValue('H8', '8');
			  $this->excel->getActiveSheet()->setCellValue('I8', '9');
			  $this->excel->getActiveSheet()->setCellValue('J8', '10');
			  $this->excel->getActiveSheet()->setCellValue('K8', '11');
			  $this->excel->getActiveSheet()->setCellValue('L8', '12');
			  $this->excel->getActiveSheet()->setCellValue('M8', '13');
			  $this->excel->getActiveSheet()->setCellValue('N8', '14');
			  $this->excel->getActiveSheet()->setCellValue('O8', '15');
			  $this->excel->getActiveSheet()->setCellValue('P8', '16');
			  $this->excel->getActiveSheet()->setCellValue('Q8', '17');
			  $this->excel->getActiveSheet()->setCellValue('R8', '18');
			  $this->excel->getActiveSheet()->setCellValue('S8', '19');
			  $this->excel->getActiveSheet()->setCellValue('T8', '20');
			  $this->excel->getActiveSheet()->setCellValue('U8', '21');
			  $this->excel->getActiveSheet()->setCellValue('V8', '22');
			  $this->excel->getActiveSheet()->setCellValue('W8', '23');
			  $this->excel->getActiveSheet()->setCellValue('X8', '24');
			  $this->excel->getActiveSheet()->setCellValue('Y8', '25');
			  $this->excel->getActiveSheet()->setCellValue('Z8', '26');
			  $this->excel->getActiveSheet()->setCellValue('AA8', '27');
			  $this->excel->getActiveSheet()->setCellValue('AB8', '28');
			  $this->excel->getActiveSheet()->setCellValue('AC8', '29');
			  $this->excel->getActiveSheet()->setCellValue('AD8', '30');
			  $this->excel->getActiveSheet()->setCellValue('AE8', '31');
			  $this->excel->getActiveSheet()->setCellValue('AF8', '32');
			  $this->excel->getActiveSheet()->setCellValue('AG8', '33');
			  $this->excel->getActiveSheet()->setCellValue('AH8', '34');
			  $this->excel->getActiveSheet()->setCellValue('AI8', '35');
			  $this->excel->getActiveSheet()->setCellValue('AJ8', '36');
			  $this->excel->getActiveSheet()->setCellValue('AK8', '37');
			  $this->excel->getActiveSheet()->setCellValue('AL8', '38');
			  $this->excel->getActiveSheet()->setCellValue('AM8', '39');
			  $this->excel->getActiveSheet()->setCellValue('AN8', '40');
			  $this->excel->getActiveSheet()->setCellValue('AO8', '41');
			  $this->excel->getActiveSheet()->setCellValue('AP8', '42');
			  $this->excel->getActiveSheet()->setCellValue('AQ8', '43');
			  $this->excel->getActiveSheet()->setCellValue('AR8', '44');
			  $this->excel->getActiveSheet()->setCellValue('AS8', '45');
			  $this->excel->getActiveSheet()->setCellValue('AT8', '46');
			  $this->excel->getActiveSheet()->setCellValue('AU8', '47');
			  $this->excel->getActiveSheet()->setCellValue('AV8', '48');
			  $this->excel->getActiveSheet()->setCellValue('AW8', '49');
			  $this->excel->getActiveSheet()->setCellValue('AX8', '50');
			  $this->excel->getActiveSheet()->setCellValue('AY8', '51');
			  $this->excel->getActiveSheet()->setCellValue('AZ8', '52');
			  $this->excel->getActiveSheet()->setCellValue('BA8', '53');
			  $this->excel->getActiveSheet()->setCellValue('BB8', '54');
			  $this->excel->getActiveSheet()->setCellValue('BC8', '55');
			  $this->excel->getActiveSheet()->setCellValue('BD8', '56');
			  $this->excel->getActiveSheet()->setCellValue('BE8', '57');
			  $this->excel->getActiveSheet()->setCellValue('BF8', '58');
			  $this->excel->getActiveSheet()->setCellValue('BG8', '59');
			  $this->excel->getActiveSheet()->setCellValue('BH8', '60');
			  $this->excel->getActiveSheet()->setCellValue('BI8', '61');
			  $this->excel->getActiveSheet()->setCellValue('BJ8', '62');
			  $this->excel->getActiveSheet()->setCellValue('BK8', '63');
			  $this->excel->getActiveSheet()->setCellValue('BL8', '64');
			  $this->excel->getActiveSheet()->setCellValue('BM8', '65');
			  $this->excel->getActiveSheet()->setCellValue('BN8', '66');
			  $this->excel->getActiveSheet()->setCellValue('BO8', '67');


			  $urutan = array($kd_skpd);
			  $length = count($urutan);
			  $total_jml = 0;
			  $total_pria = 0;
			  $total_wanita = 0;
			  $total_e1a = 0;
			  $total_e1b = 0;
			  $total_e2a = 0;
			  $total_e2b = 0;
			  $total_e3a = 0;
			  $total_e3b = 0;
			  $total_e4a = 0;
			  $total_e4b = 0;
			  $total_eselon = 0;
			  $total_g4e = 0;
			  $total_g4d = 0;
			  $total_g4c = 0;
			  $total_g4b = 0;
			  $total_g4a = 0;
			  $total_g4 = 0;
			  $total_g3d = 0;
			  $total_g3c = 0;
			  $total_g3b = 0;
			  $total_g3a = 0;
			  $total_g3 = 0;
			  $total_g2d = 0;
			  $total_g2c = 0;
			  $total_g2b = 0;
			  $total_g2a = 0;
			  $total_g2 = 0;
			  $total_g1d = 0;
			  $total_g1c = 0;
			  $total_g1b = 0;
			  $total_g1a = 0;
			  $total_g1 = 0;
			  $total_g = 0;
			  $total_s3 = 0;
			  $total_s2 = 0;
			  $total_s1 = 0;
			  $total_sma = 0;
			  $total_smp = 0;
			  $total_sd = 0;
			  $total_sekolah = 0;
			  $total_diklat1 = 0;
			  $total_diklat2 = 0;
			  $total_diklat3 = 0;
			  $total_diklat4 = 0;
			  $total_diklat = 0;
			  for($i=0;$i<$length;$i++) {
					//echo $urutan[$i];
					 $id = $urutan[$i];
					 if($id == 0) {
						$no = $no + 1;
					 } else {
						$data = $this->report_all_model->getValue($id);

						 foreach ($data as $datax) {
						 $no++;
							$total_jml = $total_jml + $datax->sekertariat_all;
							$total_pria = $total_pria + $datax->sekertariat_pria;
							$total_wanita = $total_wanita + $datax->sekertariat_wanita;
							$total_e1a = $total_e1a + $datax->sekertariat_e1a;
							$total_e1b = $total_e1a + $datax->sekertariat_e1b;
							$total_e2a = $total_e2a + $datax->sekertariat_e2a;
							$total_e2b = $total_e2a + $datax->sekertariat_e2b;
							$total_e3a = $total_e3a + $datax->sekertariat_e3a;
							$total_e3b = $total_e3a + $datax->sekertariat_e3b;
							$total_e4a = $total_e4a + $datax->sekertariat_e4a;
							$total_e4b = $total_e4a + $datax->sekertariat_e4b;
							$total_eselon = $total_eselon + $datax->sekertariat_eselon;
							$total_g4e = $total_g4e + $datax->sekertariat_g4e;
							$total_g4d = $total_g4d + $datax->sekertariat_g4d;
							$total_g4c = $total_g4c + $datax->sekertariat_g4c;
							$total_g4b = $total_g4b + $datax->sekertariat_g4b;
							$total_g4a = $total_g4a + $datax->sekertariat_g4a;
							$total_g4 = $total_g4 + $datax->sekertariat_g4;
							$total_g3d = $total_g3d + $datax->sekertariat_g3d;
							$total_g3c = $total_g3c + $datax->sekertariat_g3c;
							$total_g3b = $total_g3b + $datax->sekertariat_g3b;
							$total_g3a = $total_g3a + $datax->sekertariat_g3a;
							$total_g3 = $total_g3 + $datax->sekertariat_g3;
							$total_g2d = $total_g2d + $datax->sekertariat_g2d;
							$total_g2c = $total_g2c + $datax->sekertariat_g2c;
							$total_g2b = $total_g2b + $datax->sekertariat_g2b;
							$total_g2a = $total_g2a + $datax->sekertariat_g2a;
							$total_g2 = $total_g2 + $datax->sekertariat_g2;
							$total_g1d = $total_g1d + $datax->sekertariat_g1d;
							$total_g1c = $total_g1c + $datax->sekertariat_g1c;
							$total_g1b = $total_g1b + $datax->sekertariat_g1b;
							$total_g1a = $total_g1a + $datax->sekertariat_g1a;
							$total_g1 = $total_g1 + $datax->sekertariat_g1;
							$total_g = $total_g + $datax->sekertariat_g;
							$total_s3 = $total_s3 + $datax->sekertariat_s3;
							$total_s2 = $total_s2 + $datax->sekertariat_s2;
							$total_s1 = $total_s1 + $datax->sekertariat_s1;
							$total_sma = $total_sma + $datax->sekertariat_sma;
							$total_smp = $total_smp + $datax->sekertariat_smp;
							$total_sd = $total_sd + $datax->sekertariat_sd;
							$total_sekolah = $total_sekolah + $datax->sekertariat_sekolah;
							$total_diklat1 = $total_diklat1 + $datax->sekretariat_diklat1;
							$total_diklat2 = $total_diklat2 + $datax->sekretariat_diklat2;
							$total_diklat3 = $total_diklat3 + $datax->sekretariat_diklat3;
							$total_diklat4 = $total_diklat4 + $datax->sekretariat_diklat4;
							$total_diklat = $total_diklat + $datax->sekretariat_diklat;


						//sekertariat
						 //INIT NUMBERING
						  $this->excel->getActiveSheet()->setCellValue('A'.$no, 'I');

						  //INIT UNIT KERJA
						  $this->excel->getActiveSheet()->setCellValue('B'.$no, $nama_skpd);
						$this->excel->getActiveSheet()->setCellValue('C'.$no, $datax->sekertariat_all);
						$this->excel->getActiveSheet()->setCellValue('D'.$no, $datax->sekertariat_pria);
						$this->excel->getActiveSheet()->setCellValue('E'.$no, $datax->sekertariat_wanita);
						$this->excel->getActiveSheet()->setCellValue('F'.$no, $datax->sekertariat_e1a);
						$this->excel->getActiveSheet()->setCellValue('G'.$no, $datax->sekertariat_e1b);
						$this->excel->getActiveSheet()->setCellValue('H'.$no, $datax->sekertariat_e2a);
						$this->excel->getActiveSheet()->setCellValue('I'.$no, $datax->sekertariat_e2b);
						$this->excel->getActiveSheet()->setCellValue('J'.$no, $datax->sekertariat_e3a);
						$this->excel->getActiveSheet()->setCellValue('K'.$no, $datax->sekertariat_e3b);
						$this->excel->getActiveSheet()->setCellValue('L'.$no, $datax->sekertariat_e4a);
						$this->excel->getActiveSheet()->setCellValue('M'.$no, $datax->sekertariat_e4b);
						$this->excel->getActiveSheet()->setCellValue('N'.$no, $datax->sekertariat_eselon);
						$this->excel->getActiveSheet()->setCellValue('O'.$no, $datax->sekertariat_g4e);
						$this->excel->getActiveSheet()->setCellValue('P'.$no, $datax->sekertariat_g4d);
						$this->excel->getActiveSheet()->setCellValue('Q'.$no, $datax->sekertariat_g4c);
						$this->excel->getActiveSheet()->setCellValue('R'.$no, $datax->sekertariat_g4b);
						$this->excel->getActiveSheet()->setCellValue('S'.$no, $datax->sekertariat_g4a);
						$this->excel->getActiveSheet()->setCellValue('T'.$no, $datax->sekertariat_g4);
						$this->excel->getActiveSheet()->setCellValue('U'.$no, $datax->sekertariat_g3d);
						$this->excel->getActiveSheet()->setCellValue('V'.$no, $datax->sekertariat_g3c);
						$this->excel->getActiveSheet()->setCellValue('W'.$no, $datax->sekertariat_g3b);
						$this->excel->getActiveSheet()->setCellValue('X'.$no, $datax->sekertariat_g3a);
						$this->excel->getActiveSheet()->setCellValue('Y'.$no, $datax->sekertariat_g3);
						$this->excel->getActiveSheet()->setCellValue('Z'.$no, $datax->sekertariat_g2d);
						$this->excel->getActiveSheet()->setCellValue('AA'.$no, $datax->sekertariat_g2c);
						$this->excel->getActiveSheet()->setCellValue('AB'.$no, $datax->sekertariat_g2b);
						$this->excel->getActiveSheet()->setCellValue('AC'.$no, $datax->sekertariat_g2a);
						$this->excel->getActiveSheet()->setCellValue('AD'.$no, $datax->sekertariat_g2);
						$this->excel->getActiveSheet()->setCellValue('AE'.$no, $datax->sekertariat_g1d);
						$this->excel->getActiveSheet()->setCellValue('AF'.$no, $datax->sekertariat_g1c);
						$this->excel->getActiveSheet()->setCellValue('AG'.$no, $datax->sekertariat_g1b);
						$this->excel->getActiveSheet()->setCellValue('AH'.$no, $datax->sekertariat_g1a);
						$this->excel->getActiveSheet()->setCellValue('AI'.$no, $datax->sekertariat_g1);
						$this->excel->getActiveSheet()->setCellValue('AJ'.$no, $datax->sekertariat_g);
						$this->excel->getActiveSheet()->setCellValue('AK'.$no, $datax->sekertariat_s3);
						$this->excel->getActiveSheet()->setCellValue('AL'.$no, $datax->sekertariat_s2);
						$this->excel->getActiveSheet()->setCellValue('AM'.$no, $datax->sekertariat_s1);
						//$this->excel->getActiveSheet()->setCellValue('AM'.$no, $datax->sekertariat_s1);
						$this->excel->getActiveSheet()->setCellValue('AO'.$no, $datax->sekertariat_sma);
						$this->excel->getActiveSheet()->setCellValue('AP'.$no, $datax->sekertariat_smp);
						$this->excel->getActiveSheet()->setCellValue('AQ'.$no, $datax->sekertariat_sd);
						$this->excel->getActiveSheet()->setCellValue('AR'.$no, $datax->sekertariat_sekolah);
						$this->excel->getActiveSheet()->setCellValue('AS'.$no, $datax->sekretariat_diklat1);
						$this->excel->getActiveSheet()->setCellValue('AT'.$no, $datax->sekretariat_diklat2);
						$this->excel->getActiveSheet()->setCellValue('AU'.$no, $datax->sekretariat_diklat3);
						$this->excel->getActiveSheet()->setCellValue('AV'.$no, $datax->sekretariat_diklat4);
						$this->excel->getActiveSheet()->setCellValue('AW'.$no, $datax->sekretariat_diklat);


						}

					}
				}


			  $this->excel->getActiveSheet()->getStyle('A5:CK'.$no)->applyFromArray($styleArray);
			  //$this->excel->getActiveSheet()->getStyle('A5:AW'.($no-1))->applyFromArray($styleArray);
			  $this->excel->getActiveSheet()->getStyle("A5:CK8")->applyFromArray($styleCenter);
			  $this->excel->getActiveSheet()->getStyle("A8:A".$no)->applyFromArray($styleCenter);
			  $this->excel->getActiveSheet()->getStyle("C8:BZ".$no)->applyFromArray($styleCenter);

			$this->excel->getActiveSheet()->getStyle('AD10')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD10', 'Kendari,          '.$bulan.' '.date('Y'));
			$this->excel->getActiveSheet()->getStyle('AA12')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AA12', 'KEPALA BADAN KEPEGAWAIAN DAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA');
			$this->excel->getActiveSheet()->getStyle('AD13')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD13', 'KOTA KENDARI');
			$this->excel->getActiveSheet()->getStyle('AD18')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD18', 'ZAINAL ARIFIN S.Sos, M.Si');
			$this->excel->getActiveSheet()->getStyle('AD18')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('AD18', 'NIP. 195904061981031016');


			ob_end_clean();
                  $filename='Rekapitulasi.xls'; //save our workbook as this file name
                  header('Content-Type: application/vnd.ms-excel'); //mime type
                  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                  header('Cache-Control: max-age=0'); //no cache

                  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                  //if you want to save it as .XLSX Excel 2007 format
                  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');

           //redirect('report/report_all','refresh');

		} else {
			$data = array(
						'bulan'=>null,
						'nama'=>null
				);
				$this->template->admin_render('report/report_all/form',$this->inputSetting($data));
		}

	}

	public function add_pendidikan() {
		if($this->input->post('submit')){

		$this->load->library('excel');

		$bulan = date('m');
		$tahun = date('Y');

		if($bulan==1) {
			$bulan = "Januari";
		} else if($bulan==2) {
			$bulan = "Februari";
		} else if($bulan==3) {
			$bulan = "Maret";
		} else if($bulan==4) {
			$bulan = "April";
		} else if($bulan==5) {
			$bulan = "Mei";
		} else if($bulan==6) {
			$bulan = "Juni";
		} else if($bulan==7) {
			$bulan = "Juli";
		} else if($bulan==8) {
			$bulan = "Agustus";
		} else if($bulan==9) {
			$bulan = "September";
		} else if($bulan==10) {
			$bulan = "Oktober";
		} else if($bulan==11) {
			$bulan = "November";
		} else if($bulan==12) {
			$bulan = "Desember";
		}

			//load PHPExcel library
  		$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Rekap Pendidikan Total');

  				//STYLING
  				$styleArray = array(
								'borders' => array('allborders' =>
												array('style' => PHPExcel_Style_Border::BORDER_THIN,
												'color' =>	array('argb' => '0000'),
  							),
  						),

  					);
				$styleCenter = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);

			//data
			  $this->excel->getActiveSheet()->mergeCells('B3:S3');
              $this->excel->getActiveSheet()->setCellValue('B3', 'REKAPITULASI DAFTAR URUT KEPANGKATAN KOTA KENDARI');
              $this->excel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('B3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('B3')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('B4:S4');
              $this->excel->getActiveSheet()->setCellValue('B4', 'MENURUT PENDIDIKAN DAN JENIS KELAMIN PERIODE '.strtoupper($bulan).' '.$tahun);
              $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('B4')->getFont()->setSize(12);

			  //INIT WIDTH
			  $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			  $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(5);
			  $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(10);

			  //set column name
			  $this->excel->getActiveSheet()->mergeCells('B6:B7');
			  $this->excel->getActiveSheet()->setCellValue('B6', 'UNIT KERJA');
			  $this->excel->getActiveSheet()->mergeCells('C6:D6');
			  $this->excel->getActiveSheet()->setCellValue('C6', 'S3');
			  $this->excel->getActiveSheet()->setCellValue('C7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('D7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('E6:F6');
			  $this->excel->getActiveSheet()->setCellValue('E6', 'S2');
			  $this->excel->getActiveSheet()->setCellValue('E7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('F7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('G6:H6');
			  $this->excel->getActiveSheet()->setCellValue('G6', 'S1');
			  $this->excel->getActiveSheet()->setCellValue('G7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('H7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('I6:J6');
			  $this->excel->getActiveSheet()->setCellValue('I6', 'D3');
			  $this->excel->getActiveSheet()->setCellValue('I7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('J7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('K6:L6');
			  $this->excel->getActiveSheet()->setCellValue('K6', 'D1/D2');
			  $this->excel->getActiveSheet()->setCellValue('K7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('L7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('M6:N6');
			  $this->excel->getActiveSheet()->setCellValue('M6', 'SMA');
			  $this->excel->getActiveSheet()->setCellValue('M7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('N7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('O6:P6');
			  $this->excel->getActiveSheet()->setCellValue('O6', 'SMP');
			  $this->excel->getActiveSheet()->setCellValue('O7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('P7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('Q6:R6');
			  $this->excel->getActiveSheet()->setCellValue('Q6', 'SD');
			  $this->excel->getActiveSheet()->setCellValue('Q7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('R7', 'P');
			  $this->excel->getActiveSheet()->mergeCells('S6:S7');
			  $this->excel->getActiveSheet()->setCellValue('S6', 'JUMLAH');


			  $urutan = array('0', '0');
			  $length = count($urutan);
			  $no = 8;
			  $total_s3l = 0;$total_s2l = 0;$total_s1 = 0;$total_s3p = 0;$total_s2p = 0;$total_s1p = 0;
			  $total_d3l = 0;$total_d1l = 0;$total_sma1 = 0;$total_smpl = 0;$total_sdl = 0;
			  $total_d3p = 0;$total_d1p = 0;$total_smap = 0;$total_smpp = 0;$total_sdp = 0;

			  for($i=0;$i<$length;$i++) {
					//echo $urutan[$i];
					 $id = $urutan[$i];
					 if($id == 0) {
						$no = $no + 1;
					 } else {
						$data = $this->report_all_model->getValue($id);

						 foreach ($data as $datax) {
						 $no++;

						//sekertariat
						}
					}
				}
			//set row jumlah
			$this->excel->getActiveSheet()->setCellValue('B'.$no, 'TOTAL');
			$this->excel->getActiveSheet()->setCellValue('C'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('D'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('E'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('F'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('G'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('H'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('I'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('J'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('K'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('L'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('M'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('N'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('O'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('P'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('Q'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('R'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('S'.$no, $no);

			$this->excel->getActiveSheet()->getStyle('N'.($no+2))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('N'.($no+2), 'Kendari,          '.$bulan.' '.$tahun);
			$this->excel->getActiveSheet()->getStyle('M'.($no+4))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('M'.($no+4), 'KEPALA BADAN KEPEGAWAIAN DAN DIKLAT');
			$this->excel->getActiveSheet()->getStyle('N'.($no+5))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('N'.($no+5), 'DAERAH KOTA KENDARI');
			$this->excel->getActiveSheet()->getStyle('N'.($no+8))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('N'.($no+8), 'ZAINAL ARIFIN S.Sos, M.Si');
			$this->excel->getActiveSheet()->getStyle('N'.($no+9))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('N'.($no+9), 'NIP. 195904061981031016');

			$this->excel->getActiveSheet()->getStyle('B6:S'.$no)->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle('B6:S'.$no)->applyFromArray($styleCenter);

			ob_end_clean();
                  $filename='DUK PENIDIDIKAN.xls'; //save our workbook as this file name
                  header('Content-Type: application/vnd.ms-excel'); //mime type
                  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                  header('Cache-Control: max-age=0'); //no cache

                  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                  //if you want to save it as .XLSX Excel 2007 format
                  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');

           //redirect('report/report_all','refresh');

		} else {
			$data = array(
						'bulan'=>null,
						'nama'=>null
				);
				$this->template->admin_render('report/report_all/form',$this->inputSetting($data));
		}
	}

	public function add_golongan() {
		if($this->input->post('submit')){

		$this->load->library('excel');

		$bulan = date('m');
		$tahun = date('Y');

		if($bulan==1) {
			$bulan = "Januari";
		} else if($bulan==2) {
			$bulan = "Februari";
		} else if($bulan==3) {
			$bulan = "Maret";
		} else if($bulan==4) {
			$bulan = "April";
		} else if($bulan==5) {
			$bulan = "Mei";
		} else if($bulan==6) {
			$bulan = "Juni";
		} else if($bulan==7) {
			$bulan = "Juli";
		} else if($bulan==8) {
			$bulan = "Agustus";
		} else if($bulan==9) {
			$bulan = "September";
		} else if($bulan==10) {
			$bulan = "Oktober";
		} else if($bulan==11) {
			$bulan = "November";
		} else if($bulan==12) {
			$bulan = "Desember";
		}

			//load PHPExcel library
  		$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Rekap Golongan Total');

  				//STYLING
  				$styleArray = array(
								'borders' => array('allborders' =>
												array('style' => PHPExcel_Style_Border::BORDER_THIN,
												'color' =>	array('argb' => '0000'),
  							),
  						),

  					);
				$styleCenter = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);

			//data
			  $this->excel->getActiveSheet()->mergeCells('B3:S3');
              $this->excel->getActiveSheet()->setCellValue('B3', 'REKAPITULASI DAFTAR URUT KEPANGKATAN KOTA KENDARI');
              $this->excel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('B3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('B3')->getFont()->setSize(12);

			  $this->excel->getActiveSheet()->mergeCells('B4:S4');
              $this->excel->getActiveSheet()->setCellValue('B4', 'MENURUT GOLONGAN DAN JENIS KELAMIN PERIODE '.strtoupper($bulan).' '.$tahun);
              $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
              $this->excel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
              $this->excel->getActiveSheet()->getStyle('B4')->getFont()->setSize(12);

			  //INIT WIDTH
			  $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			  $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(8);
			  $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(10);

			  //set column name
			  $this->excel->getActiveSheet()->mergeCells('B6:B7');
			  $this->excel->getActiveSheet()->setCellValue('B6', 'UNIT KERJA');
			  $this->excel->getActiveSheet()->mergeCells('C6:E6');
			  $this->excel->getActiveSheet()->setCellValue('C6', 'Golongan IV');
			  $this->excel->getActiveSheet()->setCellValue('C7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('D7', 'P');
			  $this->excel->getActiveSheet()->setCellValue('E7', 'JML');
			  $this->excel->getActiveSheet()->mergeCells('F6:H6');
			  $this->excel->getActiveSheet()->setCellValue('F6', 'Golongan III');
			  $this->excel->getActiveSheet()->setCellValue('F7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('G7', 'P');
			  $this->excel->getActiveSheet()->setCellValue('H7', 'JML');
			  $this->excel->getActiveSheet()->mergeCells('I6:K6');
			  $this->excel->getActiveSheet()->setCellValue('I6', 'Golongan II');
			  $this->excel->getActiveSheet()->setCellValue('I7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('J7', 'P');
			  $this->excel->getActiveSheet()->setCellValue('K7', 'JML');
			  $this->excel->getActiveSheet()->mergeCells('L6:N6');
			  $this->excel->getActiveSheet()->setCellValue('L6', 'Golongan I');
			  $this->excel->getActiveSheet()->setCellValue('L7', 'L');
			  $this->excel->getActiveSheet()->setCellValue('M7', 'P');
			  $this->excel->getActiveSheet()->setCellValue('N7', 'JML');
			  $this->excel->getActiveSheet()->mergeCells('O6:O7');
			  $this->excel->getActiveSheet()->setCellValue('O6', 'JUMLAH');


			  $urutan = array('0', '0');
			  $length = count($urutan);
			  $no = 8;
			  $total_s3l = 0;$total_s2l = 0;$total_s1 = 0;$total_s3p = 0;$total_s2p = 0;$total_s1p = 0;
			  $total_d3l = 0;$total_d1l = 0;$total_sma1 = 0;$total_smpl = 0;$total_sdl = 0;
			  $total_d3p = 0;$total_d1p = 0;$total_smap = 0;$total_smpp = 0;$total_sdp = 0;

			  for($i=0;$i<$length;$i++) {
					//echo $urutan[$i];
					 $id = $urutan[$i];
					 if($id == 0) {
						$no = $no + 1;
					 } else {
						$data = $this->report_all_model->getValue($id);

						 foreach ($data as $datax) {
						 $no++;

						//sekertariat
						}
					}
				}
			//set row jumlah
			$this->excel->getActiveSheet()->setCellValue('B'.$no, 'TOTAL');
			$this->excel->getActiveSheet()->setCellValue('C'.$no, $no);
			$this->excel->getActiveSheet()->setCellValue('D'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('E'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('F'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('G'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('H'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('I'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('J'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('K'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('L'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('M'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('N'.$no, $no);
			  $this->excel->getActiveSheet()->setCellValue('O'.$no, $no);

			$this->excel->getActiveSheet()->getStyle('K'.($no+2))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('K'.($no+2), 'Kendari,          '.$bulan.' '.$tahun);
			$this->excel->getActiveSheet()->getStyle('J'.($no+4))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('J'.($no+4), 'KEPALA BADAN KEPEGAWAIAN DAN DIKLAT');
			$this->excel->getActiveSheet()->getStyle('K'.($no+5))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('K'.($no+5), 'DAERAH KOTA KENDARI');
			$this->excel->getActiveSheet()->getStyle('K'.($no+8))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('K'.($no+8), 'ZAINAL ARIFIN S.Sos, M.Si');
			$this->excel->getActiveSheet()->getStyle('K'.($no+9))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('k'.($no+9), 'NIP. 195904061981031016');

			$this->excel->getActiveSheet()->getStyle('B6:O'.$no)->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle('B6:O'.$no)->applyFromArray($styleCenter);

			ob_end_clean();
                  $filename='DUK GOLONGAN.xls'; //save our workbook as this file name
                  header('Content-Type: application/vnd.ms-excel'); //mime type
                  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                  header('Cache-Control: max-age=0'); //no cache

                  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                  //if you want to save it as .XLSX Excel 2007 format
                  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');

           //redirect('report/report_all','refresh');

		} else {
			$data = array(
						'bulan'=>null,
						'nama'=>null
				);
				$this->template->admin_render('report/report_all/form',$this->inputSetting($data));
		}
	}

	public function jurusan(){
		if($this->input->post('submit')){

			$this->load->library('excel');

			//load PHPExcel library
			$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
                  $this->excel->getActiveSheet()->setTitle('Laporan Rekapitulasi Pegawai');

  				//STYLING
  				$styleArray = array(
  					'borders' => array('vertical' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);

					//STYLING
  				$styleArray2 = array(
  					'borders' => array('allborders' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);

				//STYLING
  				$styleArray3 = array(
  					'borders' => array('bottom' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);

				$styleArray4 = array(
  					'borders' => array('right' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);

				$styleArray5 = array(
  					'borders' => array('left' =>
  						array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
  							array('argb' => '0000'),
  							),
  						),
  					);


			$no = 7;

			//SET DIMENSI TABEL
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(56);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(50);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(6);

			//set KOP
			$data['nama_skpd'] = $this->report_skpd_model->get_skpd($this->input->post('nama_skpd'));
			$data['nama_skpd2'] = $data['nama_skpd'][0];
			//$data['nama_unit_kerja'] = $this->report_skpd_model->get_unitkerja($this->input->post('kd_unitkerja'));
			//$data['nama_unit_kerja2'] = $data['nama_unit_kerja'][0];
			$this->excel->getActiveSheet()->mergeCells('A1:K1');
			$this->excel->getActiveSheet()->setCellValue('A1', 'DAFTAR URUT KEPANGKATAN PEGAWAI NEGERI SIPIL LINGKUP PEMERINTAH KOTA KENDARI ');
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

			$this->excel->getActiveSheet()->mergeCells('A2:K2');
			$this->excel->getActiveSheet()->setCellValue('A2', ''.strtoupper($data['nama_skpd2']->nama));
			$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);

			$this->excel->getActiveSheet()->mergeCells('A3:K3');
			$this->excel->getActiveSheet()->setCellValue('A3', 'JURUSAN '.strtoupper($this->input->post('jurusan')));
			$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);



			//SET NO
			$this->excel->getActiveSheet()->mergeCells('A5:A6');
			$this->excel->getActiveSheet()->setCellValue('A5', 'NO');
			$this->excel->getActiveSheet()->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setName('Calibri');


			//SET NIP BARU
			$this->excel->getActiveSheet()->mergeCells('B5:B6');
			$this->excel->getActiveSheet()->getStyle('B5:B6')->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->setCellValue('B5', 'NAMA / NIP                        						TEMPAT TANGGAL LAHIR');
			$this->excel->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setName('Calibri');


			//SET NIP LAMA
			$this->excel->getActiveSheet()->mergeCells('C5:C6');
			$this->excel->getActiveSheet()->setCellValue('C5', 'GOL');
			$this->excel->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setName('Calibri');

			//SET NAMA
			$this->excel->getActiveSheet()->mergeCells('D5:D6');
			$this->excel->getActiveSheet()->setCellValue('D5', 'TMT');
			$this->excel->getActiveSheet()->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setName('Calibri');

			//SET JENIS KELAMIN
			$this->excel->getActiveSheet()->mergeCells('E5:E6');
			$this->excel->getActiveSheet()->setCellValue('E5', 'JABATAN');
			$this->excel->getActiveSheet()->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setName('Calibri');

			//SET PANGKAT
			$this->excel->getActiveSheet()->mergeCells('F5:F6');
			$this->excel->getActiveSheet()->setCellValue('F5', 'TMT');
			$this->excel->getActiveSheet()->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setName('Calibri');

			//SET TMT
			$this->excel->getActiveSheet()->mergeCells('G5:H5');
			$this->excel->getActiveSheet()->setCellValue('G5', 'MASA KERJA');
			$this->excel->getActiveSheet()->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('G5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('G5')->getFont()->setName('Calibri');

			//SET TMT
			$this->excel->getActiveSheet()->setCellValue('G6', 'THN');
			$this->excel->getActiveSheet()->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('G6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('G6')->getFont()->setName('Calibri');

			//SET PENDIDIKAN
			$this->excel->getActiveSheet()->setCellValue('H6', 'BLN');
			$this->excel->getActiveSheet()->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('H6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('H6')->getFont()->setName('Calibri');

			//SET JURUSAN
			$this->excel->getActiveSheet()->mergeCells('I5:I6');
			$this->excel->getActiveSheet()->setCellValue('I5', 'DIKLAT PENJENJANGAN');
			$this->excel->getActiveSheet()->getStyle('I5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('I5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('I5')->getFont()->setName('Calibri');

			//SET TAHUN LULUS
			$this->excel->getActiveSheet()->mergeCells('J5:J6');
			$this->excel->getActiveSheet()->getStyle('J5:J6')->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->setCellValue('J5', 'IJAZAH                    TERAKHIR');
			$this->excel->getActiveSheet()->getStyle('J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('J5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('J5')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->mergeCells('K5:L5');
			$this->excel->getActiveSheet()->setCellValue('K5', 'USIA');
			$this->excel->getActiveSheet()->getStyle('K5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('K5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('K5')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->setCellValue('K6', 'THN');
			$this->excel->getActiveSheet()->getStyle('K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('K6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('K6')->getFont()->setName('Calibri');


			$this->excel->getActiveSheet()->setCellValue('L6', 'BLN');
			$this->excel->getActiveSheet()->getStyle('L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('L6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('L6')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->mergeCells('M5:N5');
			$this->excel->getActiveSheet()->setCellValue('M5', 'KELAMIN');
			$this->excel->getActiveSheet()->getStyle('M5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('M5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('M5')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->setCellValue('M6', 'L');
			$this->excel->getActiveSheet()->getStyle('M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('M6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('M6')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->setCellValue('N6', 'P');
			$this->excel->getActiveSheet()->getStyle('N6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('N6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('N6')->getFont()->setName('Calibri');

			$no =8;
			$hit =1;


				$data = array(
						'kd_skpd'=>$this->input->post('nama_skpd'),
						'jurusan'=>$this->input->post('jurusan')
				);
				$data = $this->report_all_model->cetak($data);

			foreach ($data as $datax) {
				$this->excel->getActiveSheet()->setCellValue('A'.$no, $hit );
				$this->excel->getActiveSheet()->setCellValue('C'.$no, $datax->golongan );
				if($datax->tmt_pangkat=='0000-00-00'){
					$this->excel->getActiveSheet()->setCellValue('D'.$no, '-' );
				}else{
					$tmt_pangkat = date("d-m-Y",strtotime($datax->tmt_pangkat));
					$this->excel->getActiveSheet()->setCellValue('D'.$no, $tmt_pangkat );
				}

				$this->excel->getActiveSheet()->setCellValue('E'.$no, $datax->nama_jabatan );

				if($datax->tmt=='0000-00-00'){
					$this->excel->getActiveSheet()->setCellValue('F'.$no, '-' );
				}else{
					$tmt = date("d-m-Y",strtotime($datax->tmt));
					$this->excel->getActiveSheet()->setCellValue('F'.$no, $tmt );
				}

				$this->excel->getActiveSheet()->setCellValue('G'.$no, $datax->mk_tahun );
				$this->excel->getActiveSheet()->setCellValue('H'.$no, $datax->mk_bulan);
				$this->excel->getActiveSheet()->setCellValue('J'.$no, $datax->program_studi);
				$this->excel->getActiveSheet()->setCellValue('K'.$no, $datax->umur_thn);
				$this->excel->getActiveSheet()->setCellValue('L'.$no, $datax->umur_bln);

				if($datax->kelamin=='1'){
					$this->excel->getActiveSheet()->setCellValue('M'.$no, 'L' );
				}else if($datax->kelamin=='2'){
					$this->excel->getActiveSheet()->setCellValue('N'.$no, 'P' );
				}

				$this->excel->getActiveSheet()->setCellValue('B'.$no++, $datax->gelar_depan.' '.$datax->nama.', '.$datax->gelar_belakang );
				$this->excel->getActiveSheet()->setCellValue('B'.$no++,'NIP : '.$datax->nip );

				if($datax->tgl_lahir=='0000-00-00'){
					$this->excel->getActiveSheet()->setCellValue('B'.$no,$datax->tempat_lahir,'-');
				}else{
					$tgl_lahir = date("d-m-Y",strtotime($datax->tgl_lahir));
					$this->excel->getActiveSheet()->setCellValue('B'.$no,$datax->tempat_lahir.', '.$tgl_lahir);
				}


				$this->excel->getActiveSheet()->getStyle('A'.$no++.':N'.$no++)->applyFromArray($styleArray3);

				$this->excel->getActiveSheet()->mergeCells('K5:L5');



				$this->excel->getActiveSheet()->getStyle('A'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getStyle('B'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('C'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('D'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('E'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('F'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('G'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('H'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('I'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('J'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('K'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('L'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('M'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('N'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('O'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('P'.$no)->getFont()->setSize(9);



				$this->excel->getActiveSheet()->getStyle('A7:O'.($no-1))->applyFromArray($styleArray);
				$this->excel->getActiveSheet()->getStyle('A7:N'.($no-1))->applyFromArray($styleArray4);
				$this->excel->getActiveSheet()->getStyle('A7:O'.($no-1))->applyFromArray($styleArray5);
				$no++;
				$hit++;

			}

			$this->excel->getActiveSheet()->getStyle('J'.($no))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('J'.($no), 'Kendari,          '.$bulan.' '.$tahun);
			$this->excel->getActiveSheet()->getStyle('J'.($no+2))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('J'.($no+2), 'KEPALA BADAN KEPEGAWAIAN DAN DIKLAT');
			$this->excel->getActiveSheet()->getStyle('J'.($no+3))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('J'.($no+3), 'DAERAH KOTA KENDARI');
			$this->excel->getActiveSheet()->getStyle('J'.($no+7))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('J'.($no+7), 'ZAINAL ARIFIN S.Sos, M.Si');
			$this->excel->getActiveSheet()->getStyle('J'.($no+8))->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('J'.($no+8), 'NIP. 195904061981031016');

				$this->excel->getActiveSheet()->getStyle('A5:N6')->applyFromArray($styleArray2);




			ob_end_clean();
                  $filename='DUK.xls'; //save our workbook as this file name
                  header('Content-Type: application/vnd.ms-excel'); //mime type
                  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                  header('Cache-Control: max-age=0'); //no cache

                  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                  //if you want to save it as .XLSX Excel 2007 format
                  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

  		$objWriter->save('php://output');

           //redirect('report/report_all','refresh');

		} else {
			$data = array(
						'bulan'=>null,
						'nama'=>null
				);
				$this->template->admin_render('report/report_all/form',$this->inputSetting($data));
		}
	}

	}
