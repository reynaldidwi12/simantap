<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class report_skpd extends Admin_Controller {


	public function __construct() {
		parent::__construct ();
		error_reporting(0);
		
		ini_set('max_execution_time', 10000);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_skpd'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('report/report_skpd_model' );
		$this->load->model ('setup/jabatan_model' );
		$this->load->model ('setup/sub_unit_kerja_model' );
		$this->load->model ('setup/unit_kerja_model' );
		$this->load->model ('setup/unit_organisasi_model' );
		$this->load->model ('setup/skpd_model' );
		
	}
	
	public function validationData_old(){
		
		$this->form_validation->set_rules('kd_skpd','lang:skpd_kd_skpd','max_length[50]');
		$this->form_validation->set_rules('nama', 'lang:pegawai_nama','max_length[2000]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting_old($data){
		
		$this->data['nama_skpd'] = $this->report_skpd_model->get_skpd_all();
		return $this->data;
	}

	public function validationData(){
		
		$this->form_validation->set_rules('kd_skdp','lang:kd_skdp','max_length[255]');
		$this->form_validation->set_rules('kd_unitorganisasi','lang:kd_unitorganisasi','max_length[255]');
		$this->form_validation->set_rules('kd_unitkerja','lang:kd_unitkerja','max_length[255]');
		$this->form_validation->set_rules('kd_subunitkerja','lang:kd_subunitkerja','max_length[255]');

		$this->form_validation->set_rules('kd_jabatan','lang:kd_jabatan','max_length[50]');
		$this->form_validation->set_rules('nama_jabatan', 'lang:nama_jabatan','max_length[2000]');
		$this->form_validation->set_rules('jenis_jabatan', 'lang:jenis_jabatan','required|max_length[2000]');
		
		return $this->form_validation->run();
	}


	/* Setup Property column */
	public function inputSetting($data){

		$this->data['kd_unitkerja'] = array(
			'name'  => 'kd_unitkerja',
			'id'    => 'kd_unitkerja',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder'=>'kode unit kerja',
			'value' => $data['kd_unitkerja'],
		);

		$this->data['kd_unitorganisasi'] = array(
			'name'  => 'kd_unitorganisasi',
			'id'    => 'kd_unitorganisasi',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder'=>'nama unit organisasi',
			'value' => $data['kd_unitorganisasi'],
		); 

		$this->data['kd_skpd'] = array(
			'name'  => 'kd_skpd',
			'id'    => 'kd_skpd',
			'required' => 'required',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder'=>'nama opd',
			'value' => $data['kd_skpd'],
		); 
		
		if($this->session->userdata('group_id')<>'3') { 
			$this->data['skpd'] = $this->skpd_model->get_skpd();
		}else {
			$this->data['skpd'] = $this->skpd_model->get_skpd_per_kd($this->session->userdata("ss_skpd"));
		}
		$this->data['get_skpd'] = $data['kd_skpd'];
		$this->data['unitorganisasi'] = $this->unit_organisasi_model->ambil_unit_organisasi($data['kd_skpd']);
		$this->data['get_unitorganisasi'] = $data['kd_unitorganisasi'];
		$this->data['unitkerja'] = $this->unit_kerja_model->ambil_unit_kerja($data['kd_skpd'] ,$data['kd_unitorganisasi']);
		$this->data['get_unitkerja'] = $data['kd_unitkerja'];
		$this->data['subunitkerja'] = $this->sub_unit_kerja_model->ambil_sub_unit_kerja($data['kd_unitkerja']);
		$this->data['get_subunitkerja'] = $data['kd_subunitkerja'];

		return $this->data;
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			
			$data = array(
				'filename'=>null,
				'nama'=>null
			);
			$this->template->admin_render('report/report_skpd/form_skpd',$this->inputSetting($data));
			
		}
	}

	public function result() {
		
		if($this->input->post('submit')){
			
			$value = $this->input->post('kd_skpd');
			$value2 = $this->input->post('kd_unitorganisasi');
			$value3 = $this->input->post('kd_unitkerja');

			$option = array(
				'user_column'=>$value
			);
			$this->session->set_userdata($option);

		}else{
			if(!empty($this->uri->segment ( 7 ))){	
				$value = $this->uri->segment ( 4 );
				$value2 = $this->uri->segment ( 5 );
				$value3 = $this->uri->segment ( 6 );
			} else{
				$value = $this->uri->segment ( 4 );
			}
		}

		$this->data['breadcrumb'] = $this->breadcrumbs->show();

		if (!empty($value3)) {

			$config = array ();

			$config ["base_url"] = base_url () . "report/report_skpd/result3/".$value."/".$value2."/".$value3;
			$config ["total_rows"] = count($this->report_skpd_model->record_count3($value, $value2, $value3));
			$config ["uri_segment"] = 7;
			$config ["per_page"] = 25;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;

			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';

			if ($this->uri->segment ( 7 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 7 );
			}

			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 7 )) ? $this->uri->segment ( 7 ) : 0;
			$this->data ['pegawai'] = $this->report_skpd_model->fetchAll3($value, $value2, $value3, $config ["per_page"], $page);

			$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'required' => 'required',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama opd',
				'value' => $data['kd_skpd'],
			);

			if($this->session->userdata('group_id')<>'3') { 
				$this->data['skpd'] = $this->skpd_model->get_skpd();
			}else {
				$this->data['skpd'] = $this->skpd_model->get_skpd_per_kd($this->session->userdata("ss_skpd"));
			}
			$this->data['get_skpd'] = $value;

			$this->data['kd_unitorganisasi'] = array(
				'name'  => 'kd_unitorganisasi',
				'id'    => 'kd_unitorganisasi',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama unit organisasi',
				'value' => $data['kd_unitorganisasi'],
			);
			$this->data['unitorganisasi'] = $this->report_skpd_model->ambil_unit_organisasi($value);
			$this->data['get_unitorganisasi'] = $value2;

			$this->data['kd_unitkerja'] = array(
				'name'  => 'kd_unitkerja',
				'id'    => 'kd_unitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'kode unit kerja',
				'value' => $data['kd_unitkerja'],
			);
			$this->data['unitkerja'] = $this->report_skpd_model->ambil_unit_kerja($value, $value2);
			$this->data['get_unitkerja'] = $value3;


			$this->data['input_kd_skpd'] = array(
				'name'  => 'input_kd_skpd',
				'id'    => 'input_kd_skpd',
				'type'  => 'hidden',
				'class' => 'input_form-control',
				'placeholder'=>'',
				'value' => $value,
			);
			$this->data['input_kd_unitorganisasi'] = array(
				'name'  => 'input_kd_unitorganisasi',
				'id'    => 'input_kd_unitorganisasi',
				'type'  => 'hidden',
				'class' => 'form-control',
				'placeholder'=>'',
				'value' => $value2,
			);
			$this->data['input_kd_unitkerja'] = array(
				'name'  => 'input_kd_unitkerja',
				'id'    => 'input_kd_unitkerja',
				'type'  => 'hidden',
				'class' => 'input_form-control',
				'placeholder'=>'',
				'value' => $value3,
			);

			$this->data ['links'] = $this->pagination->create_links ();


			$this->template->admin_render('report/report_skpd/form_skpd', $this->data);

		} else if (!empty($value2)) {
			
			$config = array ();

			$config ["base_url"] = base_url () . "report/report_skpd/result2/".$value."/".$value2;
			$config ["total_rows"] = count($this->report_skpd_model->record_count2($value,$value2));
			$config ["uri_segment"] = 6;
			$config ["per_page"] = 25;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;

			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';


			if ($this->uri->segment ( 6 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 6 );
			}

			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
			$this->data ['pegawai'] = $this->report_skpd_model->fetchAll2($value, $value2, $config["per_page"], $page);

			$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'required' => 'required',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama opd',
				'value' => $data['kd_skpd'],
			);

			if($this->session->userdata('group_id')<>'3') { 
				$this->data['skpd'] = $this->skpd_model->get_skpd();
			}else {
				$this->data['skpd'] = $this->skpd_model->get_skpd_per_kd($this->session->userdata("ss_skpd"));
			}
			$this->data['get_skpd'] = $value;

			$this->data['kd_unitorganisasi'] = array(
				'name'  => 'kd_unitorganisasi',
				'id'    => 'kd_unitorganisasi',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama unit organisasi',
				'value' => $data['kd_unitorganisasi'],
			);
			$this->data['unitorganisasi'] = $this->report_skpd_model->ambil_unit_organisasi($value);
			$this->data['get_unitorganisasi'] = $value2;

			$this->data['kd_unitkerja'] = array(
				'name'  => 'kd_unitkerja',
				'id'    => 'kd_unitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'kode unit kerja',
				'value' => $data['kd_unitkerja'],
			);
			$this->data['unitkerja'] = $this->report_skpd_model->ambil_unit_kerja($value, $value2);
			$this->data['get_unitkerja'] = $value3;


			$this->data['input_kd_skpd'] = array(
				'name'  => 'input_kd_skpd',
				'id'    => 'input_kd_skpd',
				'type'  => 'hidden',
				'class' => 'input_form-control',
				'placeholder'=>'',
				'value' => $value,
			);
			$this->data['input_kd_unitorganisasi'] = array(
				'name'  => 'input_kd_unitorganisasi',
				'id'    => 'input_kd_unitorganisasi',
				'type'  => 'hidden',
				'class' => 'form-control',
				'placeholder'=>'',
				'value' => $value2,
			);
			$this->data['input_kd_unitkerja'] = array(
				'name'  => 'input_kd_unitkerja',
				'id'    => 'input_kd_unitkerja',
				'type'  => 'hidden',
				'class' => 'input_form-control',
				'placeholder'=>'',
				'value' => $value3,
			);

			$this->data['links'] = $this->pagination->create_links();


			$this->template->admin_render('report/report_skpd/form_skpd', $this->data);

		} else {

			$config = array ();

			$config ["base_url"] = base_url () . "report/report_skpd/result/".$value;
			$config ["total_rows"] = count($this->report_skpd_model->record_count($value));
			$config ["uri_segment"] = 5;
			$config ["per_page"] = 20;
			$choice = $config ["total_rows"] / $config ["per_page"];
			$config ["num_links"] = 5;

			$config ['full_tag_open'] = '<ul class="pagination">';
			$config ['full_tag_close'] = '</ul>';
			$config ['first_link'] = 'First';
			$config ['last_link'] = 'Last';
			$config ['first_tag_open'] = '<li>';
			$config ['first_tag_close'] = '</li>';
			$config ['prev_link'] = 'Previous';
			$config ['prev_tag_open'] = '<li class="prev">';
			$config ['prev_tag_close'] = '</li>';
			$config ['next_link'] = 'Next';
			$config ['next_tag_open'] = '<li>';
			$config ['next_tag_close'] = '</li>';
			$config ['last_tag_open'] = '<li>';
			$config ['last_tag_close'] = '</li>';
			$config ['cur_tag_open'] = '<li class="active"><a href="#">';
			$config ['cur_tag_close'] = '</a></li>';
			$config ['num_tag_open'] = '<li>';
			$config ['num_tag_close'] = '</li>';


			if ($this->uri->segment ( 5 ) == "") {
				$this->data ['number'] = 0;
			} else {
				$this->data ['number'] = $this->uri->segment ( 5 );
			}

			$this->pagination->initialize ( $config );
			$page = ($this->uri->segment ( 5 )) ? $this->uri->segment ( 5 ) : 0;
			$this->data['pegawai'] = $this->report_skpd_model->fetchAll($value, $config["per_page"], $page);


			$this->data['kd_skpd'] = array(
				'name'  => 'kd_skpd',
				'id'    => 'kd_skpd',
				'required' => 'required',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama opd',
				'value' => $data['kd_skpd'],
			);

			if($this->session->userdata('group_id')<>'3') { 
				$this->data['skpd'] = $this->skpd_model->get_skpd();
			}else {
				$this->data['skpd'] = $this->skpd_model->get_skpd_per_kd($this->session->userdata("ss_skpd"));
			}
			$this->data['get_skpd'] = $value;

			$this->data['kd_unitorganisasi'] = array(
				'name'  => 'kd_unitorganisasi',
				'id'    => 'kd_unitorganisasi',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'nama unit organisasi',
				'value' => $data['kd_unitorganisasi'],
			);
			$this->data['unitorganisasi'] = $this->report_skpd_model->ambil_unit_organisasi($value);
			$this->data['get_unitorganisasi'] = $value2;

			$this->data['kd_unitkerja'] = array(
				'name'  => 'kd_unitkerja',
				'id'    => 'kd_unitkerja',
				'type'  => 'text',
				'class' => 'form-control',
				'placeholder'=>'kode unit kerja',
				'value' => $data['kd_unitkerja'],
			);
			$this->data['unitkerja'] = $this->report_skpd_model->ambil_unit_kerja($value, $value2);
			$this->data['get_unitkerja'] = $value3;


			$this->data['input_kd_skpd'] = array(
				'name'  => 'input_kd_skpd',
				'id'    => 'input_kd_skpd',
				'type'  => 'hidden',
				'class' => 'input_form-control',
				'placeholder'=>'',
				'value' => $value,
			);
			$this->data['input_kd_unitorganisasi'] = array(
				'name'  => 'input_kd_unitorganisasi',
				'id'    => 'input_kd_unitorganisasi',
				'type'  => 'hidden',
				'class' => 'form-control',
				'placeholder'=>'',
				'value' => $value2,
			);
			$this->data['input_kd_unitkerja'] = array(
				'name'  => 'input_kd_unitkerja',
				'id'    => 'input_kd_unitkerja',
				'type'  => 'hidden',
				'class' => 'input_form-control',
				'placeholder'=>'',
				'value' => $value3,
			);

			$this->data ['links'] = $this->pagination->create_links ();


			$this->template->admin_render('report/report_skpd/form_skpd', $this->data);
		}
	}


	public function result2() {
		
		$value = $this->uri->segment ( 4 );
		$value2 = $this->uri->segment ( 5 );

		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Get all users */

		$config = array ();

		$config ["base_url"] = base_url () . "report/report_skpd/result2/".$value."/".$value2;
		$config ["total_rows"] = count($this->report_skpd_model->record_count2($value,$value2));
		$config ["uri_segment"] = 6;
		$config ["per_page"] = 25;
		$choice = $config ["total_rows"] / $config ["per_page"];
		$config ["num_links"] = 5;

		$config ['full_tag_open'] = '<ul class="pagination">';
		$config ['full_tag_close'] = '</ul>';
		$config ['first_link'] = 'First';
		$config ['last_link'] = 'Last';
		$config ['first_tag_open'] = '<li>';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="prev">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li>';
		$config ['next_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li>';
		$config ['last_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';


		if ($this->uri->segment ( 6 ) == "") {
			$this->data ['number'] = 0;
		} else {
			$this->data ['number'] = $this->uri->segment ( 6 );
		}

		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 6 )) ? $this->uri->segment ( 6 ) : 0;
		$this->data ['pegawai'] = $this->report_skpd_model->fetchAll2($value, $value2, $config["per_page"], $page);

		$this->data['kd_skpd'] = array(
			'name'  => 'kd_skpd',
			'id'    => 'kd_skpd',
			'required' => 'required',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder'=>'nama opd',
			'value' => $data['kd_skpd'],
		);

		if($this->session->userdata('group_id')<>'3') { 
				$this->data['skpd'] = $this->skpd_model->get_skpd();
			}else {
				$this->data['skpd'] = $this->skpd_model->get_skpd_per_kd($this->session->userdata("ss_skpd"));
			}
		$this->data['get_skpd'] = $value;

		$this->data['kd_unitorganisasi'] = array(
			'name'  => 'kd_unitorganisasi',
			'id'    => 'kd_unitorganisasi',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder'=>'nama unit organisasi',
			'value' => $data['kd_unitorganisasi'],
		);
		$this->data['unitorganisasi'] = $this->report_skpd_model->ambil_unit_organisasi($value);
		$this->data['get_unitorganisasi'] = $value2;

		$this->data['kd_unitkerja'] = array(
			'name'  => 'kd_unitkerja',
			'id'    => 'kd_unitkerja',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder'=>'kode unit kerja',
			'value' => $data['kd_unitkerja'],
		);
		$this->data['unitkerja'] = $this->report_skpd_model->ambil_unit_kerja($value, $value2);
		$this->data['get_unitkerja'] = $value3;


		$this->data['input_kd_skpd'] = array(
			'name'  => 'input_kd_skpd',
			'id'    => 'input_kd_skpd',
			'type'  => 'hidden',
			'class' => 'input_form-control',
			'placeholder'=>'',
			'value' => $value,
		);
		$this->data['input_kd_unitorganisasi'] = array(
			'name'  => 'input_kd_unitorganisasi',
			'id'    => 'input_kd_unitorganisasi',
			'type'  => 'hidden',
			'class' => 'form-control',
			'placeholder'=>'',
			'value' => $value2,
		);
		$this->data['input_kd_unitkerja'] = array(
			'name'  => 'input_kd_unitkerja',
			'id'    => 'input_kd_unitkerja',
			'type'  => 'hidden',
			'class' => 'input_form-control',
			'placeholder'=>'',
			'value' => $value3,
		);

		$this->data['links'] = $this->pagination->create_links();


		$this->template->admin_render('report/report_skpd/form_skpd', $this->data);

	}


	public function result3() {
		
		$value = $this->uri->segment ( 4 );
		$value2 = $this->uri->segment ( 5 );
		$value2 = $this->uri->segment ( 6 );

		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Get all users */

		$config = array ();

		$config ["base_url"] = base_url () . "report/report_skpd/result3/".$value."/".$value2."/".$value3;
		$config ["total_rows"] = count($this->report_skpd_model->record_count3($value, $value2, $value3));
		$config ["uri_segment"] = 7;
		$config ["per_page"] = 25;
		$choice = $config ["total_rows"] / $config ["per_page"];
		$config ["num_links"] = 5;

		$config ['full_tag_open'] = '<ul class="pagination">';
		$config ['full_tag_close'] = '</ul>';
		$config ['first_link'] = 'First';
		$config ['last_link'] = 'Last';
		$config ['first_tag_open'] = '<li>';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="prev">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li>';
		$config ['next_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li>';
		$config ['last_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';

		if ($this->uri->segment ( 7 ) == "") {
			$this->data ['number'] = 0;
		} else {
			$this->data ['number'] = $this->uri->segment ( 7 );
		}

		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 7 )) ? $this->uri->segment ( 7 ) : 0;
		$this->data ['pegawai'] = $this->report_skpd_model->fetchAll3($value, $value2, $value3, $config ["per_page"], $page);

		$this->data['kd_skpd'] = array(
			'name'  => 'kd_skpd',
			'id'    => 'kd_skpd',
			'required' => 'required',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder'=>'nama opd',
			'value' => $data['kd_skpd'],
		);

		if($this->session->userdata('group_id')<>'3') { 
			$this->data['skpd'] = $this->skpd_model->get_skpd();
		}else {
			$this->data['skpd'] = $this->skpd_model->get_skpd_per_kd($this->session->userdata("ss_skpd"));
		}
		$this->data['get_skpd'] = $value;

		$this->data['kd_unitorganisasi'] = array(
			'name'  => 'kd_unitorganisasi',
			'id'    => 'kd_unitorganisasi',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder'=>'nama unit organisasi',
			'value' => $data['kd_unitorganisasi'],
		);
		$this->data['unitorganisasi'] = $this->report_skpd_model->ambil_unit_organisasi($value);
		$this->data['get_unitorganisasi'] = $value2;

		$this->data['kd_unitkerja'] = array(
			'name'  => 'kd_unitkerja',
			'id'    => 'kd_unitkerja',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder'=>'kode unit kerja',
			'value' => $data['kd_unitkerja'],
		);
		$this->data['unitkerja'] = $this->report_skpd_model->ambil_unit_kerja($value, $value2);
		$this->data['get_unitkerja'] = $value3;


		$this->data['input_kd_skpd'] = array(
			'name'  => 'input_kd_skpd',
			'id'    => 'input_kd_skpd',
			'type'  => 'hidden',
			'class' => 'input_form-control',
			'placeholder'=>'',
			'value' => $value,
		);
		$this->data['input_kd_unitorganisasi'] = array(
			'name'  => 'input_kd_unitorganisasi',
			'id'    => 'input_kd_unitorganisasi',
			'type'  => 'hidden',
			'class' => 'form-control',
			'placeholder'=>'',
			'value' => $value2,
		);
		$this->data['input_kd_unitkerja'] = array(
			'name'  => 'input_kd_unitkerja',
			'id'    => 'input_kd_unitkerja',
			'type'  => 'hidden',
			'class' => 'input_form-control',
			'placeholder'=>'',
			'value' => $value3,
		);

		$this->data ['links'] = $this->pagination->create_links ();


		$this->template->admin_render('report/report_skpd/form_skpd', $this->data);

	}



	public function add(){
		if($this->input->post('submit')){

			$this->load->library('excel');

			//load PHPExcel library
			$this->excel->setActiveSheetIndex(0);
                  //name the worksheet
			$this->excel->getActiveSheet()->setTitle('Laporan Rekapitulasi Pegawai');

  				//STYLING
			$styleArray = array(
				'borders' => array('vertical' =>
					array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
						array('argb' => '0000'),
					),
				),
			);

					//STYLING
			$styleArray2 = array(
				'borders' => array('allborders' =>
					array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
						array('argb' => '0000'),
					),
				),
			);

				//STYLING
			$styleArray3 = array(
				'borders' => array('bottom' =>
					array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
						array('argb' => '0000'),
					),
				),
			);

			$styleArray4 = array(
				'borders' => array('right' =>
					array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
						array('argb' => '0000'),
					),
				),
			);

			$styleArray5 = array(
				'borders' => array('left' =>
					array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' =>
						array('argb' => '0000'),
					),
				),
			);


			$no = 7;		

			//SET DIMENSI TABEL
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(56);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(50);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(6);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(6);

			//set KOP
			$data['nama_skpd'] = $this->report_skpd_model->get_skpd($this->input->post('input_kd_skpd'));
			$data['nama_skpd2'] = $data['nama_skpd'][0];

			$data['nama_unitorganisasi'] = $this->report_skpd_model->get_unitorganisasi($this->input->post('input_kd_unitorganisasi'));
			$data['nama_unitorganisasi2'] = $data['nama_unitorganisasi'][0];

			$data['nama_unit_kerja'] = $this->report_skpd_model->get_unitkerja($this->input->post('input_kd_unitkerja'));
			$data['nama_unit_kerja2'] = $data['nama_unit_kerja'][0];

			$data['nama_sub_unit_kerja'] = $this->report_skpd_model->get_subunitkerja($this->input->post('input_kd_subunitkerja'));
			$data['nama_sub_unit_kerja2'] = $data['nama_sub_unit_kerja'][0];


			$this->excel->getActiveSheet()->mergeCells('A1:K1');
			$this->excel->getActiveSheet()->setCellValue('A1', 'DAFTAR URUT KEPANGKATAN PEGAWAI NEGERI SIPIL LINGKUP PEMERINTAH KOTA KENDARI ');
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

			$this->excel->getActiveSheet()->mergeCells('A2:K2');
			$this->excel->getActiveSheet()->setCellValue('A2', ''.strtoupper($data['nama_skpd2']->nama));
			$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);

			$this->excel->getActiveSheet()->mergeCells('A3:K3');
			$this->excel->getActiveSheet()->setCellValue('A3', ''.strtoupper($data['nama_unitorganisasi2']->nama));
			$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);

			$this->excel->getActiveSheet()->mergeCells('A4:K4');
			$this->excel->getActiveSheet()->setCellValue('A4', ''.strtoupper($data['nama_unit_kerja2']->nama));
			$this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A4')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);



			//SET NO
			$this->excel->getActiveSheet()->mergeCells('A5:A6');
			$this->excel->getActiveSheet()->setCellValue('A5', 'NO');
			$this->excel->getActiveSheet()->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setName('Calibri');


			//SET NIP BARU
			$this->excel->getActiveSheet()->mergeCells('B5:B6');
			$this->excel->getActiveSheet()->getStyle('B5:B6')->getAlignment()->setWrapText(true); 
			$this->excel->getActiveSheet()->setCellValue('B5', 'NAMA / NIP                        						TEMPAT TANGGAL LAHIR');
			$this->excel->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setName('Calibri');


			//SET NIP LAMA
			$this->excel->getActiveSheet()->mergeCells('C5:C6');
			$this->excel->getActiveSheet()->setCellValue('C5', 'GOL');
			$this->excel->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setName('Calibri');

			//SET NAMA
			$this->excel->getActiveSheet()->mergeCells('D5:D6');
			$this->excel->getActiveSheet()->setCellValue('D5', 'TMT');
			$this->excel->getActiveSheet()->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setName('Calibri');

			//SET JENIS KELAMIN
			$this->excel->getActiveSheet()->mergeCells('E5:E6');
			$this->excel->getActiveSheet()->setCellValue('E5', 'JABATAN');
			$this->excel->getActiveSheet()->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setName('Calibri');

			//SET PANGKAT
			$this->excel->getActiveSheet()->mergeCells('F5:F6');
			$this->excel->getActiveSheet()->setCellValue('F5', 'TMT');
			$this->excel->getActiveSheet()->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setName('Calibri');

			//SET TMT
			$this->excel->getActiveSheet()->mergeCells('G5:H5');
			$this->excel->getActiveSheet()->setCellValue('G5', 'MASA KERJA');
			$this->excel->getActiveSheet()->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('G5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('G5')->getFont()->setName('Calibri');

			//SET TMT
			$this->excel->getActiveSheet()->setCellValue('G6', 'THN');
			$this->excel->getActiveSheet()->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('G6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('G6')->getFont()->setName('Calibri');

			//SET PENDIDIKAN
			$this->excel->getActiveSheet()->setCellValue('H6', 'BLN');
			$this->excel->getActiveSheet()->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('H6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('H6')->getFont()->setName('Calibri');

			//SET JURUSAN
			$this->excel->getActiveSheet()->mergeCells('I5:I6');
			$this->excel->getActiveSheet()->setCellValue('I5', 'DIKLAT PENJENJANGAN');
			$this->excel->getActiveSheet()->getStyle('I5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('I5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('I5')->getFont()->setName('Calibri');

			//SET TAHUN LULUS
			$this->excel->getActiveSheet()->mergeCells('J5:J6');
			$this->excel->getActiveSheet()->getStyle('J5:J6')->getAlignment()->setWrapText(true); 
			$this->excel->getActiveSheet()->setCellValue('J5', 'IJAZAH                    TERAKHIR');
			$this->excel->getActiveSheet()->getStyle('J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('J5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('J5')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->mergeCells('K5:L5');
			$this->excel->getActiveSheet()->setCellValue('K5', 'USIA');
			$this->excel->getActiveSheet()->getStyle('K5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('K5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('K5')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->setCellValue('K6', 'THN');
			$this->excel->getActiveSheet()->getStyle('K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('K6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('K6')->getFont()->setName('Calibri');


			$this->excel->getActiveSheet()->setCellValue('L6', 'BLN');
			$this->excel->getActiveSheet()->getStyle('L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('L6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('L6')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->mergeCells('M5:N5');
			$this->excel->getActiveSheet()->setCellValue('M5', 'KELAMIN');
			$this->excel->getActiveSheet()->getStyle('M5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M5')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('M5')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('M5')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->setCellValue('M6', 'L');
			$this->excel->getActiveSheet()->getStyle('M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('M6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('M6')->getFont()->setName('Calibri');

			//SET TEMPAT TUGAS
			$this->excel->getActiveSheet()->setCellValue('N6', 'P');
			$this->excel->getActiveSheet()->getStyle('N6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N6')->getFont()->setSize(11);
			$this->excel->getActiveSheet()->getStyle('N6')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('N6')->getFont()->setName('Calibri');

			$no =8;
			$hit =1;

			// if($this->input->post('kd_unitkerja')){
			// 	$data = array(
			// 		'kd_unitkerja'=>$this->input->post('kd_unitkerja')
			// 	);
			// 	// $data = $this->report_skpd_model->cetak2($data); //edited ------------------
			// 	$data = $this->report_skpd_model->cetak2($data, null,null);
			// }else{
			// 	$data = array(
			// 		'kd_skpd'=>$this->input->post('kd_skpd')
			// 	); 
			// 	// $data = $this->report_skpd_model->cetak($data); //edited ------------------
			// 	$data = $this->report_skpd_model->cetak($data, null, null);
			// }

			if ($this->input->post('input_kd_skpd') == NULL || $this->input->post('input_kd_skpd') == '') {
				$data = '';
			} else {
				if ($this->input->post('input_kd_unitorganisasi') == NULL || $this->input->post('input_kd_unitorganisasi') == '') {
					$valuex = $this->input->post('input_kd_skpd');

					$data = $this->report_skpd_model->cetak($valuex);
				} else {
					if ($this->input->post('input_kd_unitkerja') == NULL || $this->input->post('input_kd_unitkerja') == '') {

						$valuex = $this->input->post('input_kd_skpd');
						$valuex2 = $this->input->post('input_kd_unitorganisasi');

						$data = $this->report_skpd_model->cetak2($valuex , $valuex2);
					} else {
						if ($this->input->post('input_kd_subunitkerja') == NULL || $this->input->post('input_kd_subunitkerja') == '') {

							$valuex = $this->input->post('input_kd_skpd');
							$valuex2 = $this->input->post('input_kd_unitorganisasi');
							$valuex3 = $this->input->post('input_kd_unitkerja');

							$data = $this->report_skpd_model->cetak3($valuex , $valuex2 , $valuex3);

						} else {

							$valuex = $this->input->post('input_kd_skpd');
							$valuex2 = $this->input->post('input_kd_unitorganisasi');
							$valuex3 = $this->input->post('input_kd_unitkerja');
							$valuex4 = $this->input->post('input_kd_subunitkerja');

							$data = $this->report_skpd_model->cetak4($valuex , $valuex2 , $valuex3, $valuex4);
						}
					}
				}
			}

			foreach ($data as $datax) {
				$this->excel->getActiveSheet()->setCellValue('A'.$no, $hit );
				$this->excel->getActiveSheet()->setCellValue('C'.$no, $datax->golongan );
				if($datax->tmt_pangkat=='0000-00-00'){
					$this->excel->getActiveSheet()->setCellValue('D'.$no, '-' );
				}else{
					$tmt_pangkat = date("d-m-Y",strtotime($datax->tmt_pangkat));
					$this->excel->getActiveSheet()->setCellValue('D'.$no, $tmt_pangkat );
				}

				$this->excel->getActiveSheet()->setCellValue('E'.$no, $datax->nama_jabatan );

				if($datax->tmt=='0000-00-00'){
					$this->excel->getActiveSheet()->setCellValue('F'.$no, '-' );
				}else{
					$tmt = date("d-m-Y",strtotime($datax->tmt));
					$this->excel->getActiveSheet()->setCellValue('F'.$no, $tmt );
				}

				$this->excel->getActiveSheet()->setCellValue('G'.$no, $datax->mk_tahun );
				$this->excel->getActiveSheet()->setCellValue('H'.$no, $datax->mk_bulan);
				$this->excel->getActiveSheet()->setCellValue('I'.$no, $datax->nama_diklat.' '.$datax->tahun);
				$this->excel->getActiveSheet()->setCellValue('J'.$no, $datax->program_studi);
				$this->excel->getActiveSheet()->setCellValue('K'.$no, $datax->umur_thn);
				$this->excel->getActiveSheet()->setCellValue('L'.$no, $datax->umur_bln);

				if($datax->kelamin=='1'){
					$this->excel->getActiveSheet()->setCellValue('M'.$no, 'L' );
				}else if($datax->kelamin=='2'){
					$this->excel->getActiveSheet()->setCellValue('N'.$no, 'P' );
				}

				$this->excel->getActiveSheet()->setCellValue('B'.$no++, $datax->gelar_depan.' '.$datax->nama.', '.$datax->gelar_belakang );
				$this->excel->getActiveSheet()->setCellValue('B'.$no++,'NIP : '.$datax->nip );

				if($datax->tgl_lahir=='0000-00-00'){
					$this->excel->getActiveSheet()->setCellValue('B'.$no,$datax->tempat_lahir,'-');
				}else{
					$tgl_lahir = date("d-m-Y",strtotime($datax->tgl_lahir));
					$this->excel->getActiveSheet()->setCellValue('B'.$no,$datax->tempat_lahir.', '.$tgl_lahir);
				}


				$this->excel->getActiveSheet()->getStyle('A'.$no++.':N'.$no++)->applyFromArray($styleArray3);

				$this->excel->getActiveSheet()->mergeCells('K5:L5');



				$this->excel->getActiveSheet()->getStyle('A'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('A'.$no)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getStyle('B'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('C'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('D'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('E'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('F'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('G'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('H'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('I'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('J'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('K'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('L'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('M'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('N'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('O'.$no)->getFont()->setSize(9);
				$this->excel->getActiveSheet()->getStyle('P'.$no)->getFont()->setSize(9);


				$this->excel->getActiveSheet()->getStyle('A7:O'.($no-1))->applyFromArray($styleArray);
				$this->excel->getActiveSheet()->getStyle('A7:N'.($no-1))->applyFromArray($styleArray4);
				$this->excel->getActiveSheet()->getStyle('A7:O'.($no-1))->applyFromArray($styleArray5);
				$no++;
				$hit++;

			}

			$this->excel->getActiveSheet()->getStyle('A5:N6')->applyFromArray($styleArray2);




			ob_end_clean();
			$filename='DUK.xls';
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

			$objWriter->save('php://output');

		} else {
			$data = array(
				'bulan'=>null,
				'nama'=>null
			);
			$this->template->admin_render('report/report_all/form',$this->inputSetting($data));
		}
	}

	public function get_unitorganisasi(){
		$data ['get_unitorganisasi']=$this->report_skpd_model->ambil_unit_organisasi($this->uri->segment(4));
		$this->load->view('setup/jabatan/drop_down_unit_organisasi',$data);
	}

	public function get_unitkerja(){
		$data ['get_unitkerja']=$this->report_skpd_model->ambil_unit_kerja( $this->uri->segment(4), $this->uri->segment(5) );
		$this->load->view('setup/jabatan/drop_down_unit_kerja',$data);
	}
}