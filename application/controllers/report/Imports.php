<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class imports extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		
		ini_set('max_execution_time', 10000);
		
		/* Load Library */
		$this->load->library('form_validation');
		$this->load->library('session');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_skpd'));
		$this->data['pagetitle'] = $this->page_title->show();
		
		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_users'), 'admin/users');
		$this->load->helper(array('form', 'url'));
		$this->load->model ('setup/skpd_model' );
		
	}
	
	public function validationData(){
		
		$this->form_validation->set_rules('kd_skpd','lang:skpd_kd_skpd','max_length[50]');
		$this->form_validation->set_rules('nama', 'lang:pegawai_nama','max_length[2000]');
		
		return $this->form_validation->run();
	}
	
	/* Setup Property column */
	public function inputSetting($data){
		$this->data['filename'] = array(
				'name'  => 'filename',
				'id'    => 'filename',
				'type'  => 'file',
				'class' => 'form-control',
				'required'=> 'required',
		);
		return $this->data;
	}
	
	public function index() {
		
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			
				$data = array(
						'filename'=>null,
						'nama'=>null
				);
				$this->template->admin_render('report/imports/form',$this->inputSetting($data));
			
		}
	}
	
	public function add(){
		if($this->input->post('submit')){
			
			$seq=1;
			$handle = fopen($_FILES['filename']['tmp_name'], "r"); //Membuka file dan membacanya
			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				
				$tgl_lahir = $this->changeDate($data[6]);
				$tmt_cpns = $this->changeDate($data[8]);
				$tmt_pns = $this->changeDate($data[9]);
				$tmt_golonganruang = $this->changeDate($data[12]);
				$struktural_tmt = $this->changeDate($data[16]);
				$fungsionalt_tmt = $this->changeDate($data[18]);
				
				if($data[10]=='L') {
					$jenis_kelamin = '1';
				} else { 
					$jenis_kelamin = '2';
				}
				
				/*	
				$import="INSERT INTO table_pegawai
					(nip_baru, nip_lama, nama, gelar_depan, gelar_belakang, tempat_lahir, tanggal_lahir, gol_awal, tmt_cpns, tmt_pns, jenis_kelamin,
						gol_akhir, tmt_golonganruang, masakerja_tahun, masakerja_bulan, struktural_eselon, struktural_tmt, struktural_jabatan, 
							fungsionalt_tmt, fungsionalt_jabatan, fungsionalu_jabatan, unit_kerja, unit_kerjainduk, pendidikant, tahunlulus, 
								ked_huk, jen_peg, instansi_induk, instansi_kerja) 
						VALUES 
							('$data[0]','$data[1]','$data[2]', '$data[3]', '$data[4]', '$data[5]', '$tgl_lahir', '$data[7]', '$tmt_cpns', 
								'$tmt_golonganruang', '$data[10]', '$data[11]', '$tmt_golonganruang', '$data[13]', '$data[14]', '$data[15]', 
									'$struktural_tmt', 
									'$data[17]', '$fungsionalt_tmt', '$data[19]', '$data[20]', '$data[21]', '$data[22]', '$data[23]', '$data[24]', '$data[25]', 
										'$data[26]', '$data[27]', '$data[28]')"; 
				*/
				$import = "
					INSERT INTO pegawai_tbl (nip, nip_lama, nama, gelar_depan, gelar_belakang, tempat_lahir, tgl_lahir,
						kelamin) VALUES ('$data[0]','$data[1]','$data[2]', '$data[3]', '$data[4]', '$data[5]', '$tgl_lahir', '$jenis_kelamin') 
				";
					
				//data array sesuaikan dengan jumlah kolom pada CSV anda mulai dari �0� bukan �1�
				$this->db->query($import);
				
				$import_pendidikan = "
					INSERT INTO riwayat_pendidikan_tbl (nip, seq_no, nama_sekolah, tahun_ijazah) VALUES
					('$data[0]', '1', '$data[23]', '$data[24]')
				";
				$this->db->query($import_pendidikan);
				
				$import_pangkat = "
					INSERT INTO riwayat_kepangkatan_tbl (nip, seq_no, golongan, tmt_pangkat, mk_tahun, mk_bulan) VALUES 
						('$data[0]', '1', '$data[11]', '$tmt_golonganruang', '$data[13]', '$data[14]')
				";
				
				$this->db->query($import_pangkat);
				
				$import_jabatan_struktural = "
					INSERT INTO riwayat_jabatan_tbl (nip, seq_no, jenis_jabatan, nama_jabatan, esselon, tmt, unit_kerja) VALUES 
						('$data[0]', '1', 'Struktural', '$data[17]', '$data[15]', '$struktural_tmt', '$data[21]')
				";
				
				$this->db->query($import_jabatan_struktural);
				
				$import_jabatan_fungsional = "
					INSERT INTO riwayat_jabatan_tbl (nip, seq_no, jenis_jabatan, nama_jabatan, tmt) VALUES 
						('$data[0]', '2', 'Fungsional Tertentu', '$data[19]', '$fungsionalt_tmt')
				";
				
				$this->db->query($import_jabatan_fungsional);
				
				$import_jabatan_fungsional_umum = "
					INSERT INTO riwayat_jabatan_tbl (nip, seq_no, jenis_jabatan, nama_jabatan) VALUES 
						('$data[0]', '3', 'Fungsional Umum', '$data[20]')
				";
				
				$this->db->query($import_jabatan_fungsional_umum);
				
				
				
				//mysql_query($import) or die(mysql_error()); //Melakukan Import
			
	
				$seq++;
			}
		fclose($handle); //Menutup CSV file
           redirect('report/imports','refresh');	
			
		} else {
			$data = array(
						'filename'=>null,
						'nama'=>null
				);
				$this->template->admin_render('report/imports/form',$this->inputSetting($data));
		}
		
	}
	
	public function changeDate($data) {
		if($data!="") {
			$hasil = "";
			$tgl="";
			$bln="";
			$thn="";
			$tanggal="";
			$tanggal = explode(" - ", $data);
			$tgl = $tanggal[0];
			$bln = $tanggal[1];
			$thn = $tanggal[2];
			$hasil = $thn."-".$bln."-".$tgl;
			return $hasil;
		} else {
			return "";
		}
		
	}
	
}