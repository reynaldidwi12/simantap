<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

require APPPATH . 'libraries/RestController.php';

class data extends REST_Controller {
	public function __construct() {
		parent::__construct ();
		
		/* Load Library */
		$this->load->library('session');
		$this->load->database();
		$this->load->model ('api/data_model' );
		
	}
	
	public function index_get() {
		
		$kode = $this->get('kode');
		$nip = $this->get('nip');

		if ($kode == '' && !isset($nip)) {
			$this->db->select('kode,nama,kepala_skpd');
			$this->db->order_by('kode', 'ASC');
			$opb = $this->db->get('skpd_tbl')->result();

			$this->response($opb, REST_Controller::HTTP_OK);
		} elseif(!empty($kode) && isset($kode)) {

			$kd_skpd = $this->data_model->getId($kode);

			if(($kode <> '' && isset($kode)) && $nip == '') {
				$query = $this->db->query("
				SELECT p.nip, ifnull(p.nip_lama,'-') nip_lama, p.nama, p.gelar_depan, p.gelar_belakang, 
				p.tempat_lahir, p.tgl_lahir, 
				(case p.kelamin when '1' then 'Pria' when '2' then 'Wanita' end) jk
				FROM pegawai_tbl p 
				LEFT JOIN riwayat_jabatan_tbl b
				on b.tmt IN(
				SELECT MAX(tmt)
				FROM riwayat_jabatan_tbl 
				WHERE nip=b.nip)
				LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
				
				WHERE 
				(p.nip = b.nip AND
				p.status_pegawai != 'Pindah Tugas Keluar' AND 
				p.status_pegawai != 'Pensiun' AND 
				p.status_pegawai != 'Meninggal' AND
				c.kd_skpd = '".$kd_skpd."') OR (
				p.nip = b.nip AND
				p.status_pegawai != 'Pindah Tugas Keluar' AND 
				p.status_pegawai != 'Pensiun' AND 
				p.status_pegawai != 'Meninggal' AND
				c.kd_skpd = '".$kd_skpd."' AND
				c.kd_unitorganisasi IS NULL)

				GROUP BY p.nip , c.kd_skpd");

				$opb = $query->result();

				$this->response($opb, REST_Controller::HTTP_OK);
			} elseif(($kode <> '' && isset($kode)) && ($nip <> '') && !empty($kode)) {

				$check = $this->data_model->checkPegawaiBySkpd($nip, $kd_skpd);

				if(!empty($check)) {
					$pegawai   = array();
					$riwayat_suami_istri = array();
					$riwayat_anak = array();
					$riwayat_pendidikan = array();
					$riwayat_kepangkatan = array();
					$riwayat_jabatan = array();
					$riwayat_diklat = array();
					$riwayat_penghargaan = array();
					$riwayat_hukuman = array();

					$peg_query = $this->db->query("select nip, ifnull(nip_lama,'-') nip_lama, nama, picture, gelar_depan, gelar_belakang, 
					tempat_lahir, tgl_lahir, telp,
					(case kelamin when '1' then 'Pria' when '2' then 'Wanita' end) kelamin ,
					(case agama when '1' then 'Islam' when '2' then 'Katolik' when '3' then 'Hindu' when '4' then 'Budha' when '5' then 'Sinto' when '6' then 'Konghucu' when '7' then 'Protestan' end) agama, 
					(case gol_darah when '1' then 'A' when '2' then 'B' when '3' then 'AB' else 'O' end) gol_darah,
					(case status when '1' then 'Lajang' when '2' then 'Nikah' else 'Cerai' end) status,
					status_pegawai, no_kartu_pegawai, no_askes, no_taspen, no_kartu_keluarga, npwp, alamat, kode_pos, no_ktp, tmt_cpns, tmt_pns from pegawai_tbl where nip = '".$nip."'")->result_array();

					$pegawai[0]['nip'] = $peg_query[0]['nip'];
					$pegawai[0]['nip_lama']  = $peg_query[0]['nip_lama'];
					$pegawai[0]['nama'] = $peg_query[0]['nama'];
					$pegawai[0]['gelar_depan'] = $peg_query[0]['gelar_depan'];
					$pegawai[0]['gelar_belakang']  = $peg_query[0]['gelar_belakang'];
					$pegawai[0]['tempat_lahir'] = $peg_query[0]['tempat_lahir'];
					$pegawai[0]['tgl_lahir'] = $peg_query[0]['tgl_lahir'];
					$pegawai[0]['telp']  = $peg_query[0]['telp'];
					$pegawai[0]['jk'] = $peg_query[0]['kelamin'];
					$pegawai[0]['agama'] = $peg_query[0]['agama'];
					$pegawai[0]['gol_darah'] = $peg_query[0]['gol_darah'];
					$pegawai[0]['status']  = $peg_query[0]['status'];
					$pegawai[0]['status_pegawai'] = $peg_query[0]['status_pegawai'];
					$pegawai[0]['no_kartu_pegawai'] = $peg_query[0]['no_kartu_pegawai'];
					$pegawai[0]['no_askes'] = $peg_query[0]['no_askes'];
					$pegawai[0]['no_taspen']  = $peg_query[0]['no_taspen'];
					$pegawai[0]['no_kartu_keluarga'] = $peg_query[0]['no_kartu_keluarga'];
					$pegawai[0]['npwp'] = $peg_query[0]['npwp'];
					$pegawai[0]['alamat'] = $peg_query[0]['alamat'];
					$pegawai[0]['kode_pos']  = $peg_query[0]['kode_pos'];
					$pegawai[0]['no_ktp'] = $peg_query[0]['no_ktp'];
					$pegawai[0]['tmt_cpns']  = $peg_query[0]['tmt_cpns'];
					$pegawai[0]['tmt_pns'] = $peg_query[0]['tmt_pns'];

					$riwsuis_query = $this->db->query("select rs.nama, rs.tempat_lahir, rs.tgl_lahir, rs.tgl_menikah, rs.tgl_meninggal, rs.tgl_cerai, rs.no_akta_nikah, rs.no_akta_meninggal, rs.no_akta_cerai, rs.status, rs.bpjs from riwayat_suami_istri_tbl rs where rs.nip = '".$peg_query[0]['nip']."' order by rs.nama DESC")->result_array();

					for($h = 0; $h < count($riwsuis_query); $h++)
					{
						$riwayat_suami_istri[$h]['nama'] = $riwsuis_query[$h]['nama'];
						$riwayat_suami_istri[$h]['tempat_lahir'] = $riwsuis_query[$h]['tempat_lahir'];
						$riwayat_suami_istri[$h]['tgl_lahir'] = $riwsuis_query[$h]['tgl_lahir'];
						$riwayat_suami_istri[$h]['tgl_menikah'] = $riwsuis_query[$h]['tgl_menikah'];
						$riwayat_suami_istri[$h]['tgl_meninggal'] = $riwsuis_query[$h]['tgl_meninggal'];
						$riwayat_suami_istri[$h]['tgl_cerai'] = $riwsuis_query[$h]['tgl_cerai'];
						$riwayat_suami_istri[$h]['no_akta_nikah'] = $riwsuis_query[$h]['no_akta_nikah'];
						$riwayat_suami_istri[$h]['no_akta_meninggal'] = $riwsuis_query[$h]['no_akta_meninggal'];
						$riwayat_suami_istri[$h]['no_akta_cerai'] = $riwsuis_query[$h]['no_akta_cerai'];
						$riwayat_suami_istri[$h]['status'] = $riwsuis_query[$h]['status'];
						$riwayat_suami_istri[$h]['bpjs'] = $riwsuis_query[$h]['bpjs'];
					}

					$pegawai[0]['riwayat_suami_istri']  = $riwayat_suami_istri;

					$riwanak_query = $this->db->query("select nama, tempat_lahir, tgl_lahir, (case kelamin when '1' then 'Pria' when '2' then 'Wanita' end) kelamin ,
					(case status_anak when '1' then 'Kandung' when '2' then 'Tiri' when '3' then 'Angkat' end) status_anak from riwayat_anak_tbl where nip = '".$peg_query[0]['nip']."' order by tgl_lahir ASC")->result_array();

					for($i = 0; $i < count($riwanak_query); $i++)
					{
						$riwayat_anak[$i]['nama'] = $riwanak_query[$i]['nama'];
						$riwayat_anak[$i]['tempat_lahir'] = $riwanak_query[$i]['tempat_lahir'];
						$riwayat_anak[$i]['tgl_lahir'] = $riwanak_query[$i]['tgl_lahir'];
						$riwayat_anak[$i]['jk'] = $riwanak_query[$i]['kelamin'];
						$riwayat_anak[$i]['status_anak'] = $riwanak_query[$i]['status_anak'];
					}

					$pegawai[0]['riwayat_anak']  = $riwayat_anak;

					$riwpen_query = $this->db->query("select (CASE rp.jenis_pendidikan WHEN '1' THEN 'SD' WHEN '2' THEN 'SMP/MTs/Sederajat' WHEN '3' THEN 'SMA/SMK/Sederajat' WHEN '4' THEN 'Diploma 1' WHEN '5' THEN 'Diploma 2' WHEN '6' THEN 'Diploma 3' WHEN '7' THEN 'Diploma 4/Strata 1' WHEN '8' THEN 'Strata 2' WHEN '9' THEN 'Doktor' END) jenis_pendidikan, rp.nama_sekolah, rp.program_studi, rp.no_ijazah, rp.tgl_ijazah, rp.tahun_ijazah from riwayat_pendidikan_tbl rp where rp.nip = '".$peg_query[0]['nip']."' order by rp.jenis_pendidikan DESC")->result_array();

					for($j = 0; $j < count($riwpen_query); $j++)
					{
						$riwayat_pendidikan[$j]['jenis_pendidikan'] = $riwpen_query[$j]['jenis_pendidikan'];
						$riwayat_pendidikan[$j]['nama_sekolah'] = $riwpen_query[$j]['nama_sekolah'];
						$riwayat_pendidikan[$j]['program_studi'] = $riwpen_query[$j]['program_studi'];
						$riwayat_pendidikan[$j]['no_ijazah'] = $riwpen_query[$j]['no_ijazah'];
						$riwayat_pendidikan[$j]['tgl_ijazah'] = $riwpen_query[$j]['tgl_ijazah'];
						$riwayat_pendidikan[$j]['tahun_ijazah'] = $riwpen_query[$j]['tahun_ijazah'];
					}

					$pegawai[0]['riwayat_pendidikan']  = $riwayat_pendidikan;

					$riwpang_query = $this->db->query("select rp.golongan, rp.tmt_pangkat, rp.no_surat_kerja, rp.tgl_surat_kerja, rp.mk_bulan, rp.mk_tahun from riwayat_kepangkatan_tbl rp where rp.nip = '".$peg_query[0]['nip']."' order by rp.golongan DESC")->result_array();

					for($k = 0; $k < count($riwpang_query); $k++)
					{
						$riwayat_kepangkatan[$k]['golongan'] = $riwpang_query[$k]['golongan'];
						$riwayat_kepangkatan[$k]['tmt_pangkat'] = $riwpang_query[$k]['tmt_pangkat'];
						$riwayat_kepangkatan[$k]['no_surat_kerja'] = $riwpang_query[$k]['no_surat_kerja'];
						$riwayat_kepangkatan[$k]['tgl_surat_kerja'] = $riwpang_query[$k]['tgl_surat_kerja'];
						$riwayat_kepangkatan[$k]['mk_bulan'] = $riwpang_query[$k]['mk_bulan'];
						$riwayat_kepangkatan[$k]['mk_tahun'] = $riwpang_query[$k]['mk_tahun'];
					}

					$pegawai[0]['riwayat_kepangkatan']  = $riwayat_kepangkatan;

					$riwjab_query = $this->db->query("select rj.esselon, rj.jenis_jabatan, jb.nama_jabatan, rj.unit_kerja, rj.kd_jabatan, rj.no_sk, rj.tmt, rj.tmt_jabatan, rj.valid_jabatan, pr.nama as nama_skpd from riwayat_jabatan_tbl rj left join jabatan_tbl jb on jb.kd_jabatan = rj.kd_jabatan left join skpd_tbl pr on pr.kd_skpd = rj.kd_skpd where rj.nip = '".$peg_query[0]['nip']."' order by rj.tmt DESC")->result_array();

					for($l = 0; $l < count($riwjab_query); $l++)
					{
						$riwayat_jabatan[$l]['esselon'] = $riwjab_query[$l]['esselon'];
						$riwayat_jabatan[$l]['jenis_jabatan'] = $riwjab_query[$l]['jenis_jabatan'];
						$riwayat_jabatan[$l]['nama_jabatan'] = $riwjab_query[$l]['nama_jabatan'];
						$riwayat_jabatan[$l]['unit_kerja'] = $riwjab_query[$l]['unit_kerja'];
						$riwayat_jabatan[$l]['kd_jabatan'] = $riwjab_query[$l]['kd_jabatan'];
						$riwayat_jabatan[$l]['no_sk'] = $riwjab_query[$l]['no_sk'];
						$riwayat_jabatan[$l]['tmt'] = $riwjab_query[$l]['tmt'];
						$riwayat_jabatan[$l]['tmt_jabatan'] = $riwjab_query[$l]['tmt_jabatan'];
						$riwayat_jabatan[$l]['valid_jabatan'] = $riwjab_query[$l]['valid_jabatan'];
						$riwayat_jabatan[$l]['nama_skpd'] = $riwjab_query[$l]['nama_skpd'];
					}

					$pegawai[0]['riwayat_jabatan']  = $riwayat_jabatan;

					$riwdik_query = $this->db->query("select rp.no_diklat, rp.nama_diklat, rp.tahun from riwayat_diklat_tbl rp where rp.nip = '".$peg_query[0]['nip']."' order by rp.nama_diklat DESC")->result_array();

					for($m = 0; $m < count($riwdik_query); $m++)
					{
						$riwayat_diklat[$m]['no_diklat'] = $riwdik_query[$m]['no_diklat'];
						$riwayat_diklat[$m]['nama_diklat'] = $riwdik_query[$m]['nama_diklat'];
						$riwayat_diklat[$m]['tahun'] = $riwdik_query[$m]['tahun'];
					}

					$pegawai[0]['riwayat_diklat']  = $riwayat_diklat;

					$riwpeng_query = $this->db->query("select rp.penghargaan, rp.no_surat_kerja, rp.tgl_surat_kerja, rp.tahun from riwayat_penghargaan_tbl rp where rp.nip = '".$peg_query[0]['nip']."' order by rp.tahun DESC")->result_array();

					for($n = 0; $n < count($riwpeng_query); $n++)
					{
						$riwayat_penghargaan[$n]['penghargaan'] = $riwpeng_query[$n]['penghargaan'];
						$riwayat_penghargaan[$n]['no_surat_kerja'] = $riwpeng_query[$n]['no_surat_kerja'];
						$riwayat_penghargaan[$n]['tgl_surat_kerja'] = $riwpeng_query[$n]['tgl_surat_kerja'];
						$riwayat_penghargaan[$n]['tahun'] = $riwpeng_query[$n]['tahun'];
					}

					$pegawai[0]['riwayat_penghargaan']  = $riwayat_penghargaan;

					$riwhuk_query = $this->db->query("select rh.hukuman, rh.uraian_hukuman, rh.no_surat_kerja, rh.tgl_surat_kerja, rh.pejabat from riwayat_hukuman_tbl rh where rh.nip = '".$peg_query[0]['nip']."' order by rh.tgl_surat_kerja DESC")->result_array();

					for($o = 0; $o < count($riwhuk_query); $o++)
					{
						$riwayat_hukuman[$o]['hukuman'] = $riwhuk_query[$o]['hukuman'];
						$riwayat_hukuman[$o]['uraian_hukuman'] = $riwhuk_query[$o]['uraian_hukuman'];
						$riwayat_hukuman[$o]['no_surat_kerja'] = $riwhuk_query[$o]['no_surat_kerja'];
						$riwayat_hukuman[$o]['tgl_surat_kerja'] = $riwhuk_query[$o]['no_surat_kerja'];
						$riwayat_hukuman[$o]['pejabat'] = $riwhuk_query[$o]['pejabat'];
					}

					$pegawai[0]['riwayat_hukuman']  = $riwayat_hukuman;

					$opb = $pegawai;

					$this->response($opb, REST_Controller::HTTP_OK);
				} else {
					$this->response(array('status' => 'nip yang Anda masukkan tidak sesuai dengan kode opd'));
				}
			} else {
				$this->response(array('status' => 'kode belum terdaftar atau belum dimasukkan'));
			}
		} else {
			$this->response(array('status' => 'kode belum terdaftar atau belum dimasukkan'));
		}
	}

}