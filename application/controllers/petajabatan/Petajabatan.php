<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class petajabatan extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('setup/jabatan_model');
        $this->load->model('setup/skpd_model');
        $this->load->model('setup/unit_organisasi_model');
        $this->load->model('setup/pegawai_model');
        $this->load->model('setup/riwayat_jabatan_model');
        $this->load->model ('setup/kepala_daerah_model' );
        $this->load->library('lbrjstree');
        $this->load->library('lbr');
    }

    public function index() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            if ($this->ion_auth->is_admin()) {
                /* Title Page */
                $this->page_title->push(lang('menu_petajabatan'));
                $this->data['pagetitle'] = $this->page_title->show();

                /* Breadcrumbs */
                $this->data['breadcrumb'] = $this->breadcrumbs->show();

                $this->data['skpd'] = $this->skpd_model->get_skpd();

                /* Load Template */
                $this->template->admin_render('petajabatan/index', $this->data);
            } else {
                redirect('member', 'refresh');
            }
        }
    }

    public function get_data_default() {
        $kd_jabatan = $this->input->get('kd_jabatan', TRUE);
        $kepala_daerah = $this->kepala_daerah_model->getKepalaDaerahSingle();

        if ($kd_jabatan == '' || $kd_jabatan == '#') {

            $query = $this->jabatan_model->getListPetaJabatan(0, 2);
        } else {

            $query = $this->jabatan_model->getListPetaJabatanByKodeJabatan($kd_jabatan);
        }

        $items = array();
        if($query) {

        if (count($query) > 0) {
        // if (count($query->result()) > 0) {
            $items_tmp = array();
            foreach ($query as $item) {
            // foreach ($query->result() as $item) {
                $data = array();
                if ($item->atasan_id == NULL) {
                    $data['id'] = $item->kd_jabatan;
                    $data['kd_jabatan'] = $item->kd_jabatan;
                    $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_satu.'</span>';
                    $items_tmp[] = $data;
                }
            }

            // print_r($items_tmp);exit;

            if (count($items_tmp) > 0) {

                foreach ($items_tmp as $row) {

                    $tmps = array();

                    foreach ($query as $item) {
                    // foreach ($query->result() as $item) {
                        $data = array();

                        if ($item->atasan_id == $row['kd_jabatan']) {
                            $data['id'] = $item->kd_jabatan;
                            $data['kd_jabatan'] = $item->kd_jabatan;

                            $data['kd_skpd'] = $item->kd_skpd;
                            $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
                            $data['kd_unitkerja'] = $item->kd_unitkerja;
                            $data['kd_subunitkerja'] = $item->kd_subunitkerja;
                            $data['atasan_id'] = $item->atasan_id;
                            $data['level'] = $item->level;

                            if(strtolower($item->jenis_jabatan) == 'struktural'){
                                $data['nip'] = $item->nip;
                            }else{
                                $data['nip'] = NULL;
                            }

                            // $data['nip'] = $item->nip;
                            $data['gelar_depan'] = $item->gelar_depan;
                            $data['gelar_belakang'] = $item->gelar_belakang;
                            $data['nama'] = $item->nama;
                            $data['nama_jabatan'] = $item->nama_jabatan;
                            $data['golongan'] = $item->golongan;
                            $data['status_pegawai'] = $item->status_pegawai;

                            if ($item->nip == NULL) {
                                if($item->kd_jabatan == 2){
                                    $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
                                }else{
                                    if($item->jenis_jabatan == 'Fungsional Tertentu'){
                                        $data['text'] = $item->nama_jabatan;
                                    }else{
                                        $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                                    }

                                }
                            } else {
                                if($item->jenis_jabatan == 'Fungsional Tertentu'){
                                    $data['text'] = $item->nama_jabatan;
                                }else{
                                    $mytmt = $item->tmt;
                                    if($mytmt != NULL){
                                        $tmts = explode('-', $mytmt);
                                        $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
                                    }


                                    $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
                                }

                            }
                            $data['children'] = TRUE;
                            if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
                                $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                            }
                            $tmps[] = $data;
                        }
                    }

                    if (count($tmps) > 0) {
                        $row['state'] = array(
                            'opened' => TRUE,
                            'disabled' => FALSE,
                            'selected' => FALSE
                        );
                        $row['children'] = $tmps;
                    }

                    $items[] = $row;
                }
            } else {
                $items = array();

                $i = 0;

                foreach ($query as $item) {
                // foreach ($query->result() as $item) {

                    $data = array();
                    if($item->kd_jabatan == $kd_jabatan){
                        $data['id'] = $item->kd_jabatan. $i;
                    }else{
                        $data['id'] = $item->kd_jabatan;
                    }

                    $data['kd_jabatan'] = $item->kd_jabatan;
                    $data['kd_skpd'] = $item->kd_skpd;
                    $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
                    $data['kd_unitkerja'] = $item->kd_unitkerja;
                    $data['kd_subunitkerja'] = $item->kd_subunitkerja;
                    $data['atasan_id'] = $item->atasan_id;
                    $data['level'] = $item->level;

                    if(strtolower($item->jenis_jabatan) == 'struktural'){
                        $data['nip'] = $item->nip;
                    }else{
                        if($item->kd_jabatan == $kd_jabatan){
                            $data['nip'] = $item->nip;
                        }else{
                            $data['nip'] = NULL;
                        }
                    }

                    // $data['nip'] = $item->nip;
                    $data['gelar_depan'] = $item->gelar_depan;
                    $data['gelar_belakang'] = $item->gelar_belakang;
                    $data['nama'] = $item->nama;
                    $data['nama_jabatan'] = $item->nama_jabatan;
                    $data['golongan'] = $item->golongan;
                    $data['status_pegawai'] = $item->status_pegawai;

                    if ($item->nip == NULL) {
                        if($item->kd_jabatan == 2){
                                $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
                            }else{

                                if(strtolower($item->jenis_jabatan) == 'struktural'){
                                     $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                                }else{
                                    $data['text'] = $item->nama_jabatan;
                                }

                        }
                    } else {
                        $mytmt = $item->tmt;

                        if($mytmt != NULL){
                            $tmts = explode('-', $mytmt);
                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
                        }

                        if(strtolower($item->jenis_jabatan) == 'struktural'){

                            $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
                        }else{
                            if($item->kd_jabatan == $kd_jabatan){
                                $data['type'] = 'user';
                                $data['text'] = '<span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
                            }else{
                                $data['text'] = $item->nama_jabatan;
                            }

                        }
                    }

                    if($item->kd_jabatan == $kd_jabatan){
                        $data['children'] = FALSE;
                    }else{
                        $data['children'] = TRUE;
                    }


                    if($this->input->get('nip') == $item->nip && $kd_jabatan == $item->kd_jabatan){
                        $data['state'] = array(
                            'opened' => TRUE,
                            'disabled' => FALSE,
                            'selected' => TRUE,
                        );
                    }

                    if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
                        $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                    }

                    $items[] = $data;


                    $i++;
                }
            }
        }
    }

        header('Content-type: application/json');
        $json = json_encode($items);
        echo $json;
        exit;
    }

    public function get_data_defaultOld() {
        $kd_jabatan = $this->input->get('kd_jabatan', TRUE);
        $kepala_daerah = $this->kepala_daerah_model->getKepalaDaerahSingle();

        if ($kd_jabatan == '' || $kd_jabatan == '#') {

            $query = $this->jabatan_model->getListPetaJabatan(0, 2);
        } else {

            $query = $this->jabatan_model->getListPetaJabatanByKodeJabatan($kd_jabatan);
        }

        // print_r($query);exit;
        print_r($query->result());exit;

        $items = array();
        if($query) {

        if (count($query->result()) > 0) {
            $items_tmp = array();
            foreach ($query->result() as $item) {
                $data = array();
                if ($item->atasan_id == NULL) {
                    $data['id'] = $item->kd_jabatan;
                    $data['kd_jabatan'] = $item->kd_jabatan;
                    $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_satu.'</span>';
                    $items_tmp[] = $data;
                }
            }

            print_r($items_tmp);exit;

            if (count($items_tmp) > 0) {

                foreach ($items_tmp as $row) {

                    $tmps = array();

                    foreach ($query->result() as $item) {
                        $data = array();

                        if ($item->atasan_id == $row['kd_jabatan']) {
                            $data['id'] = $item->kd_jabatan;
                            $data['kd_jabatan'] = $item->kd_jabatan;

                            $data['kd_skpd'] = $item->kd_skpd;
                            $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
                            $data['kd_unitkerja'] = $item->kd_unitkerja;
                            $data['kd_subunitkerja'] = $item->kd_subunitkerja;
                            $data['atasan_id'] = $item->atasan_id;
                            $data['level'] = $item->level;

                            if(strtolower($item->jenis_jabatan) == 'struktural'){
                                $data['nip'] = $item->nip;
                            }else{
                                $data['nip'] = NULL;
                            }

                            // $data['nip'] = $item->nip;
                            $data['gelar_depan'] = $item->gelar_depan;
                            $data['gelar_belakang'] = $item->gelar_belakang;
                            $data['nama'] = $item->nama;
                            $data['nama_jabatan'] = $item->nama_jabatan;
                            $data['golongan'] = $item->golongan;
                            $data['status_pegawai'] = $item->status_pegawai;

                            if ($item->nip == NULL) {
                                if($item->kd_jabatan == 2){
                                    $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
                                }else{
                                    if($item->jenis_jabatan == 'Fungsional Tertentu'){
                                        $data['text'] = $item->nama_jabatan;
                                    }else{
                                        $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                                    }

                                }
                            } else {
                                if($item->jenis_jabatan == 'Fungsional Tertentu'){
                                    $data['text'] = $item->nama_jabatan;
                                }else{
                                    $mytmt = $item->tmt;
                                    if($mytmt != NULL){
                                        $tmts = explode('-', $mytmt);
                                        $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
                                    }


                                    $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
                                }

                            }
                            $data['children'] = TRUE;
                            if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
                                $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                            }
                            $tmps[] = $data;
                        }
                    }

                    if (count($tmps) > 0) {
                        $row['state'] = array(
                            'opened' => TRUE,
                            'disabled' => FALSE,
                            'selected' => FALSE
                        );
                        $row['children'] = $tmps;
                    }

                    $items[] = $row;
                }
            } else {
                $items = array();

                $i = 0;

                foreach ($query->result() as $item) {

                    $data = array();
                    if($item->kd_jabatan == $kd_jabatan){
                        $data['id'] = $item->kd_jabatan. $i;
                    }else{
                        $data['id'] = $item->kd_jabatan;
                    }

                    $data['kd_jabatan'] = $item->kd_jabatan;
                    $data['kd_skpd'] = $item->kd_skpd;
                    $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
                    $data['kd_unitkerja'] = $item->kd_unitkerja;
                    $data['kd_subunitkerja'] = $item->kd_subunitkerja;
                    $data['atasan_id'] = $item->atasan_id;
                    $data['level'] = $item->level;

                    if(strtolower($item->jenis_jabatan) == 'struktural'){
                        $data['nip'] = $item->nip;
                    }else{
                        if($item->kd_jabatan == $kd_jabatan){
                            $data['nip'] = $item->nip;
                        }else{
                            $data['nip'] = NULL;
                        }
                    }

                    // $data['nip'] = $item->nip;
                    $data['gelar_depan'] = $item->gelar_depan;
                    $data['gelar_belakang'] = $item->gelar_belakang;
                    $data['nama'] = $item->nama;
                    $data['nama_jabatan'] = $item->nama_jabatan;
                    $data['golongan'] = $item->golongan;
                    $data['status_pegawai'] = $item->status_pegawai;

                    if ($item->nip == NULL) {
                        if($item->kd_jabatan == 2){
                                $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
                            }else{

                                if(strtolower($item->jenis_jabatan) == 'struktural'){
                                     $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                                }else{
                                    $data['text'] = $item->nama_jabatan;
                                }

                        }
                    } else {
                        $mytmt = $item->tmt;

                        if($mytmt != NULL){
                            $tmts = explode('-', $mytmt);
                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
                        }

                        if(strtolower($item->jenis_jabatan) == 'struktural'){

                            $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
                        }else{
                            if($item->kd_jabatan == $kd_jabatan){
                                $data['type'] = 'user';
                                $data['text'] = '<span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
                            }else{
                                $data['text'] = $item->nama_jabatan;
                            }

                        }
                    }

                    if($item->kd_jabatan == $kd_jabatan){
                        $data['children'] = FALSE;
                    }else{
                        $data['children'] = TRUE;
                    }


                    if($this->input->get('nip') == $item->nip && $kd_jabatan == $item->kd_jabatan){
                        $data['state'] = array(
                            'opened' => TRUE,
                            'disabled' => FALSE,
                            'selected' => TRUE,
                        );
                    }

                    if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
                        $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                    }

                    $items[] = $data;


                    $i++;
                }
            }
        }
    }

        header('Content-type: application/json');
        $json = json_encode($items);
        echo $json;
        exit;
    }

    public function export() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            if ($this->ion_auth->is_admin()) {

            	$kd_skpd = $this->uri->segment(3);
            	if($kd_skpd == NULL){
            		redirect('petajabatan', 'refresh');
            	}

                /* Title Page */
                $this->page_title->push(lang('menu_petajabatan'));
                $this->data['pagetitle'] = $this->page_title->show();

                /* Breadcrumbs */
                $this->data['breadcrumb'] = $this->breadcrumbs->show();
                $this->data['kd_skpd'] = $kd_skpd;
                $this->data['skpd'] = $this->skpd_model->fetchByIdSingle($kd_skpd);
                $this->data['get_unitorganisasi'] = $this->unit_organisasi_model->ambil_unit_organisasi($kd_skpd);

                /* Load Template */
                $this->template->chart_render('petajabatan/export', $this->data);
            } else {
                redirect('member', 'refresh');
            }
        }
    }


    public function get_bagan_default() {
        $kd_skpd = $this->input->get('kd_skpd', TRUE);

        $query = $this->jabatan_model->getListBaganPetaJabatanBySKPD($kd_skpd);
        // print_r($query->result());exit;

        if (count($query) > 0) {
        // if (count($query->result()) > 0) {

            $items_tmp = array();

            $items_level2 = array();
            $items_level3 = array();
            $items_level4 = array();
            $items_level5 = array();

            foreach ($query as $item) {
            // foreach ($query->result() as $item) {
                if ($item->level == 2) {
                    $items_level2[] = $item;
                }
                if ($item->level == 3) {
                    $items_level3[] = $item;
                }
                if ($item->level == 4) {
                    $items_level4[] = $item;
                }
                if ($item->level == 5) {
                    $items_level5[] = $item;
                }
            }


            if (count($items_level2) > 0) {

                foreach ($items_level2 as $l2) {

                    if (count($items_level3) > 0) {

                        foreach ($items_level3 as $l3) {

                            if ($l3->atasan_id == $l2->kd_skpd) {

                                $items_tmp['children'][] = $l3;
                            }
                        }

                        $mytemp = array();

                        if (count($items_tmp['children']) > 0) {

                            foreach ($items_tmp['children'] as $key => $value) {

                                $items = array();

								$mytemp2 = array();

                                if (count($items_level4) > 0) {

                                    foreach ($items_level4 as $l4) {

                                        if ($l4->atasan_id == $value->kd_unitorganisasi) {

                                            $items[] = $l4;
                                        }

                                    }

                                    if(count($items) > 0) {

                                    	foreach ($items as $key2 => $value2) {

                                    		$items2 = array();

                                    		if(count($items_level5) > 0){

                                    			foreach ($items_level5 as $l5) {

                                    				if ($l5->atasan_id == $value2->kd_unitkerja) {

                                            			$items2[] = $l5;
                                        			}
                                    			}

                                    			$array2 = (array) $items[$key2];
                                    			$array2['children'] = $items2;

												$mytemp2[] = $array2;

                                    		}

                                    	}

                                    }

									$array = (array) $items_tmp['children'][$key];
									if(count($mytemp2)>0){
										$array['children'] = $mytemp2;
									}else{
										if(count($items) > 0){
											$array['children'] = $items;
										}
									}

									$mytemp[] = $array;

                                }
                            }
                        }

                    }

                    $items_tmp = (array) $l2;

                    if(count($mytemp) > 0){

						$items_tmp['children'] = $mytemp;
                    }

                }

            }

            $datas_array = $this->lbr->unsetDataItems($items_tmp);

            header('Content-type: application/json');
            $json = json_encode($datas_array);
            echo $json;
            exit;

        }
    }


    public function find_pegawai(){


		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else {
			if($this->input->post('submit',TRUE)){
				$column = $this->input->post('column',TRUE);

				if($this->input->post('column')=="nip"){
					$query = str_replace(" ","",$this->input->post('data',TRUE));
				}else{
					$query = $this->input->post('data',TRUE);
				}

                if($this->session->userdata('group_id')=='3') {
                    $pegawai = $this->jabatan_model->searchPegawai_per_skpd($this->session->userdata("ss_skpd"),$column,$query,10);
                } else {
                    $pegawai = $this->jabatan_model->searchPegawai($column,$query,10);
                }

				header('Content-type: application/json');
	            $json = json_encode($pegawai);
	            echo $json;
	            exit;

			}else{

				header('Content-type: application/json');
	            $json = json_encode(array('message'=>'Data yang di submit kosong'));
	            echo $json;
	            exit;
			}
		}
	}

    public function find() {
        $nip = $this->uri->segment(3);
        $riwayat = $this->riwayat_jabatan_model->getMaxByNip($nip);

        if($riwayat == NULL){

            $pegawai = $this->pegawai_model->getSingle($nip);
            $this->session->set_flashdata('msg_search', 'Pegawai dengan nama <span class="text-primary text-bold">' . $pegawai->gelar_depan . ' ' . $pegawai->nama . ' ' . $pegawai->gelar_belakang . '</span> (<em>' . $pegawai->nip . '</em>), tidak menempati jabatan struktural.');

            redirect('petajabatan');
        }

        $jab_top = $this->jabatan_model->getRootJabatanFromSkpd($riwayat->kd_skpd);
        $jab_user = $this->jabatan_model->fetchByIdSingle($riwayat->kd_jabatan);

        if($riwayat->kd_jabatan != NULL && $this->input->get('kd_jabatan', TRUE) != NULL){

            $kd_jabatan = $this->input->get('kd_jabatan', TRUE);
        }else if($riwayat->kd_jabatan != NULL && $this->input->get('kd_jabatan', TRUE) == NULL){

            $kd_jabatan = $riwayat->kd_jabatan;
        }else if($riwayat->kd_jabatan == NULL && $this->input->get('kd_jabatan', TRUE) != NULL){

            $kd_jabatan = $this->input->get('kd_jabatan', TRUE);
        }else{

            $kd_jabatan = '';
        }

        $kepala_daerah = $this->kepala_daerah_model->getKepalaDaerahSingle();

        $query = $this->jabatan_model->getListPetaJabatan(0, 2);

        $items = array();
        if($query){

        if (count($query) > 0) {
        // if (count($query->result()) > 0) {
            $items_tmp = array();
            foreach ($query as $item) {
            // foreach ($query->result() as $item) {
                $data = array();
                if ($item->atasan_id == NULL) {
                    $data['id'] = $item->kd_jabatan;
                    $data['kd_jabatan'] = $item->kd_jabatan;
                    $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_satu.'</span>';
                    $items_tmp[] = $data;
                }
            }

            if (count($items_tmp) > 0) {

                foreach ($items_tmp as $row) {

                    $tmps = array();

                    foreach ($query as $item) {
                    // foreach ($query->result() as $item) {
                        $data = array();

                        if ($item->atasan_id == $row['kd_jabatan']) {

                            $data['id'] = $item->kd_jabatan;
                            $data['kd_jabatan'] = $item->kd_jabatan;

                            $data['kd_skpd'] = $item->kd_skpd;
                            $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
                            $data['kd_unitkerja'] = $item->kd_unitkerja;
                            $data['kd_subunitkerja'] = $item->kd_subunitkerja;
                            $data['atasan_id'] = $item->atasan_id;
                            $data['level'] = $item->level;

                            if(strtolower($item->jenis_jabatan) == 'struktural'){
                                $data['nip'] = $item->nip;
                            }else{
                                $data['nip'] = NULL;
                            }

                            $data['gelar_depan'] = $item->gelar_depan;
                            $data['gelar_belakang'] = $item->gelar_belakang;
                            $data['nama'] = $item->nama;
                            $data['nama_jabatan'] = $item->nama_jabatan;
                            $data['golongan'] = $item->golongan;

                            if ($item->nip == NULL) {
                                if($item->kd_jabatan == 2){
                                    $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
                                }else{

                                    if(strtolower($item->jenis_jabatan) == 'struktural'){
                                         $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                                    }else{
                                        $data['text'] = $item->nama_jabatan;
                                    }

                                }

                            } else {

                                if($item->jenis_jabatan == 'Fungsional Tertentu'){
                                    $data['text'] = $item->nama_jabatan;
                                }else{
                                    $mytmt = $item->tmt;
                                    if($mytmt != NULL){
                                        $tmts = explode('-', $mytmt);
                                        $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
                                    }


                                    $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
                                }

                            }
                            $data['children'] = TRUE;

                            if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
                                $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                            }

                            if($nip == $item->nip){
                                $data['state'] = array(
                                    'opened' => TRUE,
                                    'disabled' => FALSE,
                                    'selected' => TRUE
                                );
                            }


                            if($riwayat->level > $jab_top->level){

                                if($jab_top->kd_jabatan == $item->kd_jabatan){

                                    $children_tmp = $this->jabatan_model->getJabatanByKodeSkpdAndLevel($jab_top->kd_skpd,$jab_top->level,$riwayat->level);

                                    // print_r($children_tmp);exit;

                                    $mychilds = $this->lbrjstree->parseBodyWithJabatan($children_tmp, $jab_top->kd_skpd, $nip, $jab_user);

                                    // $mychilds = $this->lbrjstree->parseBodyWithJabatan($children_tmp->result(), $jab_top->kd_skpd, $nip, $jab_user);

                                    // print_r($mychilds);exit;

                                    $data['children'] = $mychilds;

                                    $data['state'] = array(
                                        'opened' => TRUE
                                    );
                                }
                            }


                            $tmps[] = $data;
                        }
                    }

                    if (count($tmps) > 0) {
                        $row['state'] = array(
                            'opened' => TRUE,
                            'disabled' => FALSE,
                            'selected' => FALSE
                        );
                        $row['children'] = $tmps;
                    }

                    $items[] = $row;
                }
            } else {
                $items = array();

                foreach ($query as $item) {
                // foreach ($query->result() as $item) {
                    $data = array();

                    if($item->kd_jabatan == $kd_jabatan){
                        $data['id'] = $item->kd_jabatan. $i;
                    }else{
                        $data['id'] = $item->kd_jabatan;
                    }

                    $data['kd_jabatan'] = $item->kd_jabatan;
                    $data['kd_skpd'] = $item->kd_skpd;
                    $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
                    $data['kd_unitkerja'] = $item->kd_unitkerja;
                    $data['kd_subunitkerja'] = $item->kd_subunitkerja;
                    $data['atasan_id'] = $item->atasan_id;
                    $data['level'] = $item->level;

                    if(strtolower($item->jenis_jabatan) == 'struktural'){
                        $data['nip'] = $item->nip;
                    }else{
                        if($item->kd_jabatan == $kd_jabatan){
                            $data['nip'] = $item->nip;
                        }else{
                            $data['nip'] = NULL;
                        }
                    }

                    $data['gelar_depan'] = $item->gelar_depan;
                    $data['gelar_belakang'] = $item->gelar_belakang;
                    $data['nama'] = $item->nama;
                    $data['nama_jabatan'] = $item->nama_jabatan;
                    $data['golongan'] = $item->golongan;

                    if ($item->nip == NULL) {
                        if($item->kd_jabatan == 2){
                                $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
                            }else{

                                if(strtolower($item->jenis_jabatan) == 'struktural'){
                                     $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                                }else{
                                    $data['text'] = $item->nama_jabatan;
                                }

                        }
                    } else {
                        $mytmt = $item->tmt;

                        if($mytmt != NULL){
                            $tmts = explode('-', $mytmt);
                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
                        }

                        if(strtolower($item->jenis_jabatan) == 'struktural'){

                            $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
                        }else{
                            if($item->kd_jabatan == $kd_jabatan){
                                $data['type'] = 'user';
                                $data['text'] = '<span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
                            }else{
                                $data['text'] = $item->nama_jabatan;
                            }

                        }

                    }

                    if($item->kd_jabatan == $kd_jabatan){
                        $data['children'] = FALSE;
                    }else{
                        $data['children'] = TRUE;
                    }

                    if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
                        $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                    }

                    if($nip == $item->nip){
                        $data['state'] = array(
                            'opened' => TRUE,
                            'disabled' => FALSE,
                            'selected' => TRUE
                        );
                    }

                    $items[] = $data;
                }
            }
        }

        }


        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            if ($this->ion_auth->is_admin()) {
                /* Title Page */
                $this->page_title->push(lang('menu_petajabatan'));
                $this->data['pagetitle'] = $this->page_title->show();

                /* Breadcrumbs */
                $this->data['breadcrumb'] = $this->breadcrumbs->show();

                $this->data['skpd'] = $this->skpd_model->get_skpd();
                $this->data['petajabatan'] = $items;
                $this->data['query'] = $query;

                /* Load Template */
                $this->template->admin_render('petajabatan/find', $this->data);
            } else {
                redirect('member', 'refresh');
            }
        }
    }




    public function get_filtering_bagan() {
        $kd_skpd = trim($this->input->get('kd_skpd', TRUE));
        $kd_unitorganisasi = trim($this->input->get('kd_unitorganisasi', TRUE));
        $kd_unitkerja = trim($this->input->get('kd_unitkerja', TRUE));
        $blacklist = trim($this->input->get('blacklist', TRUE));

        $query = $this->jabatan_model->getListBaganPetaJabatanBySKPD($kd_skpd);
        // print_r($query->result());exit;

        if (count($query) > 0) {
        // if (count($query->result()) > 0) {
            $items_tmp = array();

            $items_level2 = array();
            $items_level3 = array();
            $items_level4 = array();
            $items_level5 = array();

            foreach ($query as $item) {
            // foreach ($query->result() as $item) {
                if ($item->level == 2) {

                    $items_level2[] = $item;

                }
                if ($item->level == 3) {
                    if(isset($blacklist) && $blacklist != NULL){
                        if (strpos(strtolower($item->nama_jabatan), strtolower($blacklist)) !== false) {

                        }else{
                            if($kd_unitorganisasi != NULL){
                                if($item->kd_unitorganisasi == $kd_unitorganisasi){
                                    $items_level3[] = $item;
                                }
                            }else{
                                $items_level3[] = $item;
                            }
                        }
                    }else{
                        if($kd_unitorganisasi != NULL){
                            if($item->kd_unitorganisasi == $kd_unitorganisasi){
                                $items_level3[] = $item;
                            }
                        }else{
                            $items_level3[] = $item;
                        }
                    }

                }
                if ($item->level == 4) {
                    if(isset($blacklist) && $blacklist != NULL){
                        if (strpos(strtolower($item->nama_jabatan), strtolower($blacklist)) !== false) {

                        }else{
                            if($kd_unitkerja != NULL){
                                if($item->kd_unitkerja == $kd_unitkerja){
                                    $items_level4[] = $item;
                                }
                            }else{
                                $items_level4[] = $item;
                            }
                        }
                    }else{
                        if($kd_unitkerja != NULL){
                            if($item->kd_unitkerja == $kd_unitkerja){

                                $items_level4[] = $item;
                            }
                        }else{

                            $items_level4[] = $item;
                        }

                    }

                }
                if ($item->level == 5) {
                    if(isset($blacklist) && $blacklist != NULL){
                        if (strpos(strtolower($item->nama_jabatan), strtolower($blacklist)) !== false) {

                        }else{
                            $items_level5[] = $item;
                        }
                    }else{
                        $items_level5[] = $item;
                    }

                }
            }


            if (count($items_level2) > 0) {

                foreach ($items_level2 as $l2) {

                    if (count($items_level3) > 0) {

                        foreach ($items_level3 as $l3) {

                            if ($l3->atasan_id == $l2->kd_skpd) {

                                $items_tmp['children'][] = $l3;
                            }
                        }

                        $mytemp = array();

                        if (count($items_tmp['children']) > 0) {

                            foreach ($items_tmp['children'] as $key => $value) {

                                $items = array();

                                $mytemp2 = array();

                                if (count($items_level4) > 0) {

                                    foreach ($items_level4 as $l4) {

                                        if ($l4->atasan_id == $value->kd_unitorganisasi) {

                                            $items[] = $l4;
                                        }

                                    }

                                    if(count($items) > 0) {

                                        foreach ($items as $key2 => $value2) {

                                            $items2 = array();

                                            if(count($items_level5) > 0){

                                                foreach ($items_level5 as $l5) {

                                                    if ($l5->atasan_id == $value2->kd_unitkerja) {

                                                        $items2[] = $l5;
                                                    }
                                                }

                                                $array2 = (array) $items[$key2];
                                                $array2['children'] = $items2;

                                                $mytemp2[] = $array2;

                                            }

                                        }

                                    }

                                    $array = (array) $items_tmp['children'][$key];
                                    if(count($mytemp2)>0){
                                        $array['children'] = $mytemp2;
                                    }else{
                                        if(count($items) > 0){
                                            $array['children'] = $items;
                                        }
                                    }

                                    $mytemp[] = $array;

                                }
                            }
                        }

                    }

                    $items_tmp = (array) $l2;

                    if(count($mytemp) > 0){

                        $items_tmp['children'] = $mytemp;
                    }

                }

            }

            // print_r($items_tmp);exit;

            $datas_array = $this->lbr->unsetDataItems($items_tmp);

            // print_r($datas_array);exit;

            header('Content-type: application/json');
            $json = json_encode($datas_array);
            echo $json;
            exit;

        }
    }



}
