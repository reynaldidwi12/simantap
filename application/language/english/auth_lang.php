<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['auth_sign_session']    = 'Sign in to start your session';
$lang['auth_sign_facebook']   = 'Sign in using Facebook';
$lang['auth_sign_google']     = 'Sign in using Google+';
$lang['auth_your_email']      = 'Your email';
$lang['auth_your_name']       = 'Your name';
$lang['auth_your_password']   = 'Your password';
$lang['auth_remember_me']     = 'Remember me';
$lang['auth_forgot_password'] = 'I forgot my password';
$lang['auth_new_member']      = 'Register a new member';
$lang['auth_login']           = 'Login';
$lang['auth_or']              = 'OR';
$lang['error_csrf']           = 'This form post did not pass our security checks.';

// Change Password
$lang['change_password_heading']                               = 'Change Password';
$lang['change_password_old_password_label']                    = 'Old Password:';
$lang['change_password_new_password_label']                    = 'New Password (at least %s characters long):';
$lang['change_password_new_password_confirm_label']            = 'Confirm New Password:';
$lang['change_password_submit_btn']                            = 'Change';
$lang['change_password_validation_old_password_label']         = 'Old Password';
$lang['change_password_validation_new_password_label']         = 'New Password';
$lang['change_password_validation_new_password_confirm_label'] = 'Confirm New Password';

