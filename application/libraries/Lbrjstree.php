<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lbrjstree {

    public function creteBody($results){
    	$items = array();

        foreach ($results as $item) {
            $data = array();
            $data['id'] = $item->kd_jabatan;
            $data['kd_jabatan'] = $item->kd_jabatan;
            if ($item->nip == NULL) {
                if($item->kd_jabatan == 2){
                            $data['text'] = $item->nama_jabatan . ' - ( <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span> )';
                    }else{
                        $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                }
            } else {
                $mytmt = $item->tmt;
                if($mytmt != NULL){
                    $tmts = explode('-', $mytmt);
                    $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
                }    
                $data['text'] = $item->nama_jabatan . ' <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>), ('.$item->golongan.'), ('.$mytmt.')';
            }

			$data['children'] = TRUE;
			
			if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
				$data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
			}

            $items[] = $data;
        }
            

        return $items;
    }


    public function parseNode($results){
    	$tmps = array();

        foreach ($results as $item) {
            $data = array();

            if ($item->atasan_id == $row['kd_jabatan']) {

                $data['id'] = $item->kd_jabatan;
                $data['kd_jabatan'] = $item->kd_jabatan;
                $data['kd_skpd'] = $item->kd_skpd;
                $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
                $data['kd_unitkerja'] = $item->kd_unitkerja;
                $data['kd_subunitkerja'] = $item->kd_subunitkerja;
                $data['atasan_id'] = $item->atasan_id;
                $data['level'] = $item->level;

				$data['nip'] = $item->nip;
                $data['gelar_depan'] = $item->gelar_depan;
                $data['gelar_belakang'] = $item->gelar_belakang;
                $data['nama'] = $item->nama;
                $data['nama_jabatan'] = $item->nama_jabatan;
                $data['golongan'] = $item->golongan;

                if ($item->nip == NULL) {
                    if($item->kd_jabatan == 2){
                        $data['text'] = $item->nama_jabatan . ' - ( <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span> )';
                    }else{
                        $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                    }
                } else {
                    $mytmt = $item->tmt;
                    if($mytmt != NULL){
                        $tmts = explode('-', $mytmt);
                        $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
                    }
                    $data['text'] = $item->nama_jabatan . ' <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>), ('.$item->golongan.'), ('.$mytmt.')';
                }
				$data['children'] = TRUE;
				
				if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
					$data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
				}

                $tmps[] = $data;
            }
        }

        if (count($tmps) > 0) {
            $row['state'] = array(
                'opened' => TRUE,
                'disabled' => FALSE,
                'selected' => FALSE
            );
            $row['children'] = $tmps;
        }

        $items[] = $row;


        return $items;
    }


    public function parseBody($results, $top_atasan_id, $nip_pegawai){

    	$items = array();
        if($results) {

	        if (count($results) > 0) {
	            $items_tmp = array();
	            foreach ($results as $item) {


	                $data = array();
	                if ($item->atasan_id == $top_atasan_id) {
	                    $data['id'] = $item->kd_jabatan;
	                    $data['kd_jabatan'] = $item->kd_jabatan;
	                    $data['kd_skpd'] = $item->kd_skpd;
	                    $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
	                    $data['kd_unitkerja'] = $item->kd_unitkerja;
	                    $data['kd_subunitkerja'] = $item->kd_subunitkerja;
	                    $data['atasan_id'] = $item->atasan_id;
	                    $data['level'] = $item->level;

						$data['nip'] = $item->nip;
	                    $data['gelar_depan'] = $item->gelar_depan;
                        $data['gelar_belakang'] = $item->gelar_belakang;
                        $data['nama'] = $item->nama;
                        $data['nama_jabatan'] = $item->nama_jabatan;
                        $data['golongan'] = $item->golongan;

	                    if ($item->nip == NULL) {
	                        
                            $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
	                        
	                    } else {
	                        $mytmt = $item->tmt;
	                        if($mytmt != NULL){
	                            $tmts = explode('-', $mytmt);
	                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
	                        }    
	                    	$data['text'] = $item->nama_jabatan . ' <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
	                    }

	                    $data['children'] = TRUE;
	                    if($nip_pegawai == $item->nip){
	                    	$data['state'] = array(
	                            'opened' => TRUE,
	                            'disabled' => FALSE,
	                            'selected' => TRUE
	                        );
	                    }

	                    $items_tmp[] = $data;
	                }
	            } 

	            if (count($items_tmp) > 0) {

	                foreach ($items_tmp as $row) {

	                    $tmps = array();

	                    foreach ($results as $item) {
	                        $data2 = array();
	                        
	                        if($item->level == 4){
	                        	
		                        if ($item->atasan_id == $row['kd_unitorganisasi']) {
		                        
		                            $data2['id'] = $item->kd_jabatan;
				                    $data2['kd_jabatan'] = $item->kd_jabatan;
				                    $data2['kd_skpd'] = $item->kd_skpd;
				                    $data2['kd_unitorganisasi'] = $item->kd_unitorganisasi;
				                    $data2['kd_unitkerja'] = $item->kd_unitkerja;
				                    $data2['kd_subunitkerja'] = $item->kd_subunitkerja;
				                    $data2['atasan_id'] = $item->atasan_id;
				                    $data2['level'] = $item->level;

									$data2['nip'] = $item->nip;
				                    $data2['gelar_depan'] = $item->gelar_depan;
		                            $data2['gelar_belakang'] = $item->gelar_belakang;
		                            $data2['nama'] = $item->nama;
		                            $data2['nama_jabatan'] = $item->nama_jabatan;
		                            $data2['golongan'] = $item->golongan;

		                            if ($item->nip == NULL) {
		                                
	                                    $data2['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
		                                
		                            } else {
		                                $mytmt = $item->tmt;
		                                if($mytmt != NULL){
		                                    $tmts = explode('-', $mytmt);
		                                    $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
		                                }
		                                $data2['text'] = $item->nama_jabatan . ' <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
		                            }
		                            $data2['children'] = TRUE;
		                            if($nip_pegawai == $item->nip){
		                            	$data2['state'] = array(
				                            'opened' => TRUE,
				                            'disabled' => FALSE,
				                            'selected' => TRUE
				                        );
		                            }
		                            $tmps[] = $data2;
		                        }
	                        }

	                        
	                    }

	                    $tmps2 = array();

	                    if(count($tmps) > 0){

	                    	// $tmps2 = array();


	                    	foreach ($tmps as $row3) {

	                    		$tmps3 = array();
	                    		
	                    		foreach ($results as $item3) {
			                        $data3 = array();
			                        
			                        if($item3->level == 5){
			                        	
				                        if ($item3->atasan_id == $row3['kd_unitkerja']) {
				                        
				                            $data3['id'] = $item3->kd_jabatan;
						                    $data3['kd_jabatan'] = $item3->kd_jabatan;
						                    $data3['kd_skpd'] = $item3->kd_skpd;
						                    $data3['kd_unitorganisasi'] = $item3->kd_unitorganisasi;
						                    $data3['kd_unitkerja'] = $item3->kd_unitkerja;
						                    $data3['kd_subunitkerja'] = $item3->kd_subunitkerja;
						                    $data3['atasan_id'] = $item3->atasan_id;
						                    $data3['level'] = $item3->level;

											$data3['nip'] = $item3->nip;
						                    $data3['gelar_depan'] = $item3->gelar_depan;
				                            $data3['gelar_belakang'] = $item3->gelar_belakang;
				                            $data3['nama'] = $item3->nama;
				                            $data3['nama_jabatan'] = $item3->nama_jabatan;
				                            $data3['golongan'] = $item3->golongan;

				                            if ($item3->nip == NULL) {
				                                
			                                    $data3['text'] = $item3->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
				                                
				                            } else {
				                                $mytmt = $item3->tmt;
				                                if($mytmt != NULL){
				                                    $tmts = explode('-', $mytmt);
				                                    $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
				                                }
				                                $data3['text'] = $item3->nama_jabatan . ' <span class="text-primary text-bold">' . $item3->gelar_depan . ' ' . $item3->nama . ' ' . $item3->gelar_belakang . '</span> (<em>' . $item3->nip . '</em>)';
				                            }
				                            $data3['children'] = TRUE;
				                            if($nip_pegawai == $item3->nip){
				                            	$data3['state'] = array(
						                            'opened' => TRUE,
						                            'disabled' => FALSE,
						                            'selected' => TRUE
						                        );
				                            }
				                            $tmps3[] = $data3;
				                        }
			                        }
	                        
	                    		}

	                    		if(count($tmps3) > 0){
		                    		$row3['state'] = array(
			                            'opened' => TRUE,
			                            'disabled' => FALSE,
			                            'selected' => FALSE
			                        );
			                        $row3['children'] = $tmps3;
	                    		}

	                    		//mulai disini
	                    		$tmps2[] = $row3;
	                    	}
	                    	
	                    }

	                    if (count($tmps) > 0) {
	                        $row['state'] = array(
	                            'opened' => TRUE,
	                            'disabled' => FALSE,
	                            'selected' => FALSE
	                        );

	                        if(count($tmps2) > 0){
	                        	$row['children'] = $tmps2;
	                        }else{
	                        	$row['children'] = $tmps;
	                    	}
	                    }

	                    $items[] = $row;
	                }

	                // print_r($items);exit;
	            } else {
	                $items = array();

	                foreach ($results as $item) {
	                    $data = array();

	                    $data['id'] = $item->kd_jabatan;
	                    $data['kd_jabatan'] = $item->kd_jabatan;
	                    $data['kd_skpd'] = $item->kd_skpd;
	                    $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
	                    $data['kd_unitkerja'] = $item->kd_unitkerja;
	                    $data['kd_subunitkerja'] = $item->kd_subunitkerja;
	                    $data['atasan_id'] = $item->atasan_id;
	                    $data['level'] = $item->level;

						$data['nip'] = $item->nip;
	                    $data['gelar_depan'] = $item->gelar_depan;
                        $data['gelar_belakang'] = $item->gelar_belakang;
                        $data['nama'] = $item->nama;
                        $data['nama_jabatan'] = $item->nama_jabatan;
                        $data['golongan'] = $item->golongan;
	                    if ($item->nip == NULL) {
	                        
                            $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
	                        
	                    } else {
	                        $mytmt = $item->tmt;
	                        if($mytmt != NULL){
	                            $tmts = explode('-', $mytmt);
	                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
	                        }    
	                    	$data['text'] = $item->nama_jabatan . ' <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
	                    }

	                    $data['children'] = TRUE;
	                    if($nip_pegawai == $item->nip){
	                    	$data['state'] = array(
	                            'opened' => TRUE,
	                            'disabled' => FALSE,
	                            'selected' => TRUE
	                        );
	                    }

	                    $items[] = $data;
	                }
	            }
	        }
    	}

    	return $items;
    }



    public function parseBodyWithJabatan($results, $top_atasan_id, $nip_pegawai, $jabatan){

    	// print_r($jabatan);exit;

    	$items = array();
        if($results) {

	        if (count($results) > 0) {
	            $items_tmp = array();
	            foreach ($results as $item) {


	                $data = array();
	                if ($item->atasan_id == $top_atasan_id) {

	                    $data['id'] = $item->kd_jabatan;
	                    $data['kd_jabatan'] = $item->kd_jabatan;
	                    $data['kd_skpd'] = $item->kd_skpd;
	                    $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
	                    $data['kd_unitkerja'] = $item->kd_unitkerja;
	                    $data['kd_subunitkerja'] = $item->kd_subunitkerja;
	                    $data['atasan_id'] = $item->atasan_id;
	                    $data['level'] = $item->level;

	                    if(strtolower($item->jenis_jabatan) == 'struktural'){
                            $data['nip'] = $item->nip;
                        }else{
                            $data['nip'] = NULL;
                        }

                        $data['nip_to_find'] = $nip_pegawai;
	                    $data['gelar_depan'] = $item->gelar_depan;
                        $data['gelar_belakang'] = $item->gelar_belakang;
                        $data['nama'] = $item->nama;
                        $data['nama_jabatan'] = $item->nama_jabatan;
                        $data['golongan'] = $item->golongan;

	                    if ($item->nip == NULL) {
	                        
                            if($item->kd_jabatan == 2){
                                $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
                            }else{

                                if(strtolower($item->jenis_jabatan) == 'struktural'){
                                     $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                                }else{
                                    $data['text'] = $item->nama_jabatan;  
                                }
                                
                            }
	                        
	                    } else {

	                    	$mytmt = $item->tmt;

	                        if($mytmt != NULL){
	                            $tmts = explode('-', $mytmt);
	                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
	                        }

	                        if(strtolower($item->jenis_jabatan) == 'struktural'){

	                            $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
	                        }else{

								$data['text'] = $item->nama_jabatan;
	                            
	                        }

	                     
	                    }

						$data['children'] = TRUE;
						
						if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
							$data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
						}

	                    if(strtolower($item->jenis_jabatan) != 'struktural'){

		                    if($jabatan->kd_jabatan == $item->kd_jabatan){

		                    	$data['state'] = array(
		                            'opened' => TRUE,
		                            'disabled' => FALSE,
		                            'selected' => FALSE
		                        );
		                    }
	                	}else{

	                		if($nip_pegawai == $item->nip){
		                    // if($nip_pegawai == $data['nip']){
		                    	$data['state'] = array(
		                            'opened' => TRUE,
		                            'disabled' => FALSE,
		                            'selected' => TRUE
		                        );
		                    }
	                	}

	                    

	                    // $items_tmp[] = $data;

	                    if($item->kd_skpd == $jabatan->kd_skpd){

	                    	$items_tmp[] = $data;

	                	}
	                }
	            }

	            // print_r($items_tmp);exit; 

	            if (count($items_tmp) > 0) {

	                foreach ($items_tmp as $row) {

	                    $tmps = array();

	                    foreach ($results as $item) {
	                        $data2 = array();
	                        
	                        if($item->level == 4){
	                        	
		                        if ($item->atasan_id == $row['kd_unitorganisasi']) {
		                        
		                            $data2['id'] = $item->kd_jabatan;
				                    $data2['kd_jabatan'] = $item->kd_jabatan;
				                    $data2['kd_skpd'] = $item->kd_skpd;
				                    $data2['kd_unitorganisasi'] = $item->kd_unitorganisasi;
				                    $data2['kd_unitkerja'] = $item->kd_unitkerja;
				                    $data2['kd_subunitkerja'] = $item->kd_subunitkerja;
				                    $data2['atasan_id'] = $item->atasan_id;
				                    $data2['level'] = $item->level;

									if(strtolower($item->jenis_jabatan) == 'struktural'){
			                            $data2['nip'] = $item->nip;
			                        }else{
			                            $data2['nip'] = NULL;
			                        }

									// $data2['nip'] = $item->nip;
									$data2['nip_to_find'] = $nip_pegawai;
				                    $data2['gelar_depan'] = $item->gelar_depan;
		                            $data2['gelar_belakang'] = $item->gelar_belakang;
		                            $data2['nama'] = $item->nama;
		                            $data2['nama_jabatan'] = $item->nama_jabatan;
		                            $data2['golongan'] = $item->golongan;

		                            if ($item->nip == NULL) {

		                            	if($item->kd_jabatan == 2){
			                                $data2['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
			                            }else{

			                                if(strtolower($item->jenis_jabatan) == 'struktural'){
			                                     $data2['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
			                                }else{
			                                    $data2['text'] = $item->nama_jabatan;  
			                                }
			                                
			                            }
		                                
		                                
		                            } else {

		                            	$mytmt = $item->tmt;

				                        if($mytmt != NULL){
				                            $tmts = explode('-', $mytmt);
				                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
				                        }

				                        if(strtolower($item->jenis_jabatan) == 'struktural'){

				                            $data2['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
				                        }else{
				                            
				                            $data2['text'] = $item->nama_jabatan;
				                        }

		                            }
									$data2['children'] = TRUE;
									
									if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
										$data2['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
									}

		                            if(strtolower($item->jenis_jabatan) != 'struktural'){

						                if($jabatan->kd_jabatan == $item->kd_jabatan){

						                    	$data2['state'] = array(
						                            'opened' => TRUE,
						                            'disabled' => FALSE,
						                            'selected' => FALSE
						                        );
						                }
				                	}else{

				                		if($nip_pegawai == $item->nip){
					                    // if($nip_pegawai == $data['nip']){
					                    	$data2['state'] = array(
					                            'opened' => TRUE,
					                            'disabled' => FALSE,
					                            'selected' => TRUE
					                        );
					                    }
				                	}

		                            // $tmps[] = $data2;

		                            if($item->kd_unitorganisasi == $jabatan->kd_unitorganisasi){

		                            	$tmps[] = $data2;
		                        	}

		                        }
	                        }

	                    }

	                    // print_r($tmps);exit;

	                    $tmps2 = array();

	                    if(count($tmps) > 0){

	                    	foreach ($tmps as $row3) {

	                    		$tmps3 = array();
	                    		
	                    		foreach ($results as $item3) {
			                        $data3 = array();
			                        
			                        if($item3->level == 5){
			                        	
				                        if ($item3->atasan_id == $row3['kd_unitkerja']) {
				                        
				                            $data3['id'] = $item3->kd_jabatan;
						                    $data3['kd_jabatan'] = $item3->kd_jabatan;
						                    $data3['kd_skpd'] = $item3->kd_skpd;
						                    $data3['kd_unitorganisasi'] = $item3->kd_unitorganisasi;
						                    $data3['kd_unitkerja'] = $item3->kd_unitkerja;
						                    $data3['kd_subunitkerja'] = $item3->kd_subunitkerja;
						                    $data3['atasan_id'] = $item3->atasan_id;
						                    $data3['level'] = $item3->level;

											
											if(strtolower($item3->jenis_jabatan) == 'struktural'){
					                            $data3['nip'] = $item3->nip;
					                        }else{
					                            $data3['nip'] = NULL;
					                        }

					                        $data3['nip_to_find'] = $nip_pegawai;
						                    $data3['gelar_depan'] = $item3->gelar_depan;
				                            $data3['gelar_belakang'] = $item3->gelar_belakang;
				                            $data3['nama'] = $item3->nama;
				                            $data3['nama_jabatan'] = $item3->nama_jabatan;
				                            $data3['golongan'] = $item3->golongan;

				                            if ($item3->nip == NULL) {

			                                    if($item3->kd_jabatan == 2){
					                                $data3['text'] = $item3->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
					                            }else{

					                                if(strtolower($item3->jenis_jabatan) == 'struktural'){
					                                     $data3['text'] = $item3->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
					                                }else{
					                                    $data3['text'] = $item3->nama_jabatan;  
					                                }
					                                
					                            }
				                                
				                            } else {
				                            	$mytmt = $item3->tmt;

						                        if($mytmt != NULL){
						                            $tmts = explode('-', $mytmt);
						                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
						                        }

						                        if(strtolower($item3->jenis_jabatan) == 'struktural'){

						                            $data3['text'] = $item3->nama_jabatan . ' - <span class="text-primary text-bold">' . $item3->gelar_depan . ' ' . $item3->nama . ' ' . $item3->gelar_belakang . '</span> (<em>' . $item3->nip . '</em>)';
						                        }else{

						                            $data3['text'] = $item3->nama_jabatan;
						                            
						                        }

				                            }

											$data3['children'] = TRUE;
											
											if($item3->status_pegawai != 'Aktif' && $item3->status_pegawai != ''){
												$data3['text'] = $item3->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
											}

				                            if(strtolower($item3->jenis_jabatan) != 'struktural'){

								                if($jabatan->kd_jabatan == $item3->kd_jabatan){

								                    	$data3['state'] = array(
								                            'opened' => TRUE,
								                            'disabled' => FALSE,
								                            'selected' => FALSE
								                        );
								                }
						                	}else{

						                		if($nip_pegawai == $item3->nip){
							                    // if($nip_pegawai == $data['nip']){
							                    	$data3['state'] = array(
							                            'opened' => TRUE,
							                            'disabled' => FALSE,
							                            'selected' => TRUE
							                        );
							                    }
						                	}

				                            // $tmps3[] = $data3;

				                            if($item3->kd_unitkerja == $jabatan->kd_unitkerja){
				                            	
				                            	$tmps3[] = $data3;
				                        	}
				                        }
			                        }









			                        /// $tmp3
	                        
	                    		}

	                    		if(count($tmps3) > 0){

	                    			$tmps4 = array();

	                    			foreach($tmps3 as $row4){

	                    				$tmps5 = array();
	                    		
			                    		foreach ($results as $item4) {

					                        $data4 = array();
					                        
					                        if($item4->level == 6){


					                        	
						                        if ($item4->atasan_id == $row4['kd_subunitkerja']) {

						                        	// print_r(array('atasan_id'=>$item4->atasan_id,'kd_subunitkerja'=>$row4['kd_subunitkerja']));exit;
						                        
						                            $data4['id'] = $item4->kd_jabatan;
								                    $data4['kd_jabatan'] = $item4->kd_jabatan;
								                    $data4['kd_skpd'] = $item4->kd_skpd;
								                    $data4['kd_unitorganisasi'] = $item4->kd_unitorganisasi;
								                    $data4['kd_unitkerja'] = $item4->kd_unitkerja;
								                    $data4['kd_subunitkerja'] = $item4->kd_subunitkerja;
								                    $data4['atasan_id'] = $item4->atasan_id;
								                    $data4['level'] = $item4->level;

													
													if(strtolower($item4->jenis_jabatan) == 'struktural'){
							                            $data4['nip'] = $item4->nip;
							                        }else{
							                            $data4['nip'] = NULL;
							                        }

							                        $data4['nip_to_find'] = $nip_pegawai;
								                    $data4['gelar_depan'] = $item4->gelar_depan;
						                            $data4['gelar_belakang'] = $item4->gelar_belakang;
						                            $data4['nama'] = $item4->nama;
						                            $data4['nama_jabatan'] = $item4->nama_jabatan;
						                            $data4['golongan'] = $item4->golongan;

						                            if ($item4->nip == NULL) {

					                                    if($item4->kd_jabatan == 2){
							                                $data4['text'] = $item4->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
							                            }else{

							                                if(strtolower($item4->jenis_jabatan) == 'struktural'){
							                                     $data4['text'] = $item4->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
							                                }else{
							                                    $data4['text'] = $item4->nama_jabatan;  
							                                }
							                                
							                            }
						                                
						                            } else {
						                            	$mytmt = $item4->tmt;

								                        if($mytmt != NULL){
								                            $tmts = explode('-', $mytmt);
								                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
								                        }

								                        if(strtolower($item4->jenis_jabatan) == 'struktural'){

								                            $data4['text'] = $item4->nama_jabatan . ' - <span class="text-primary text-bold">' . $item4->gelar_depan . ' ' . $item4->nama . ' ' . $item4->gelar_belakang . '</span> (<em>' . $item4->nip . '</em>)';
								                        }else{

								                            $data4['text'] = $item4->nama_jabatan;
								                            
								                        }

						                            }

													$data4['children'] = TRUE;
													
													if($item4->status_pegawai != 'Aktif' && $item4->status_pegawai != ''){
														$data4['text'] = $item4->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
													}

						                            if(strtolower($item4->jenis_jabatan) != 'struktural'){

										                if($jabatan->kd_jabatan == $item4->kd_jabatan){

										                    	$data4['state'] = array(
										                            'opened' => TRUE,
										                            'disabled' => FALSE,
										                            'selected' => FALSE
										                        );
										                }
								                	}else{

								                		if($nip_pegawai == $item4->nip){
									                    // if($nip_pegawai == $data['nip']){
									                    	$data4['state'] = array(
									                            'opened' => TRUE,
									                            'disabled' => FALSE,
									                            'selected' => TRUE
									                        );
									                    }
								                	}

						                            // $tmps3[] = $data3;

						                            if($item4->kd_unitkerja == $jabatan->kd_unitkerja){
						                            	
						                            	$tmps5[] = $data4;
						                        	}

						                        }

					                        }
			                        
			                    		}

			                    		if(count($tmps5)>0){
		                    				$row4['state'] = array(
					                            'opened' => TRUE,
					                            'disabled' => FALSE,
					                            'selected' => FALSE
					                        );

					                        $row4['children'] = $tmps5;

			                    		}

			                    		$tmps4[] = $row4;	

	                    			}

	                    			// print_r($tmps4); exit;

		                    	 	$row3['state'] = array(
			                            'opened' => TRUE,
			                            'disabled' => FALSE,
			                            'selected' => FALSE
			                        );
			                        $row3['children'] = $tmps4;
	                    		}

	                    		//mulai disini
	                    		$tmps2[] = $row3;
	                    	}
	                    	
	                    }

	                    if (count($tmps) > 0) {
	                        $row['state'] = array(
	                            'opened' => TRUE,
	                            'disabled' => FALSE,
	                            'selected' => FALSE
	                        );

	                        if(count($tmps2) > 0){
	                        	$row['children'] = $tmps2;
	                        }else{
	                        	$row['children'] = $tmps;
	                    	}
	                    }

	                    $items[] = $row;
	                }

	                // print_r($items);exit;
	            } else {
	                $items = array();

	                foreach ($results as $item) {
	                    $data = array();

	                    $data['id'] = $item->kd_jabatan;
	                    $data['kd_jabatan'] = $item->kd_jabatan;
	                    $data['kd_skpd'] = $item->kd_skpd;
	                    $data['kd_unitorganisasi'] = $item->kd_unitorganisasi;
	                    $data['kd_unitkerja'] = $item->kd_unitkerja;
	                    $data['kd_subunitkerja'] = $item->kd_subunitkerja;
	                    $data['atasan_id'] = $item->atasan_id;
	                    $data['level'] = $item->level;

	                    if(strtolower($item->jenis_jabatan) == 'struktural'){
                            $data['nip'] = $item->nip;
                        }else{
                            $data['nip'] = NULL;
                        }

                        $data['nip_to_find'] = $nip_pegawai;
	                    $data['gelar_depan'] = $item->gelar_depan;
                        $data['gelar_belakang'] = $item->gelar_belakang;
                        $data['nama'] = $item->nama;
                        $data['nama_jabatan'] = $item->nama_jabatan;
                        $data['golongan'] = $item->golongan;
	                    if ($item->nip == NULL) {

	                    	if($item->kd_jabatan == 2){
                                $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">'.$kepala_daerah->kosong_dua.'</span>';
                            }else{

                                if(strtolower($item->jenis_jabatan) == 'struktural'){
                                     $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
                                }else{
                                    $data['text'] = $item->nama_jabatan;  
                                }
                                
                            }
	                        
                            // $data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
	                        
	                    } else {

	                    	$mytmt = $item->tmt;

	                        if($mytmt != NULL){
	                            $tmts = explode('-', $mytmt);
	                            $mytmt = $tmts[2].'-'.$tmts[1].'-'.$tmts[0];
	                        }

	                        if(strtolower($item->jenis_jabatan) == 'struktural'){

	                            $data['text'] = $item->nama_jabatan . ' - <span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
	                        }else{
	                            if($item->kd_jabatan == $jabatan->kd_jabatan){
	                                $data['type'] = 'user';
	                                $data['text'] = '<span class="text-primary text-bold">' . $item->gelar_depan . ' ' . $item->nama . ' ' . $item->gelar_belakang . '</span> (<em>' . $item->nip . '</em>)';
	                            }else{
	                                $data['text'] = $item->nama_jabatan;
	                            }
	                            
	                        }

	                    }

						$data['children'] = TRUE;
						
						if($item->status_pegawai != 'Aktif' && $item->status_pegawai != ''){
							$data['text'] = $item->nama_jabatan . ' - ( <span class="text-danger text-bold">Data Belum Dimasukan</span> )';
						}
	                    // if($nip_pegawai == $item->nip){
	                    if($nip_pegawai == $data['nip']){	
	                    	$data['state'] = array(
	                            'opened' => TRUE,
	                            'disabled' => FALSE,
	                            'selected' => FALSE
	                        );
	                    }

	                    $items[] = $data;
	                }
	            }
	        }
    	}

    	return $items;
    }




    
}