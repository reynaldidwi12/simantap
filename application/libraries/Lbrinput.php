<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lbrinput {

    public function setNullIfEmpty($str){
    	$str = trim($str);
    	if($str != NULL){
    		return $str;
    	}else{
    		return NULL;
    	}
    }

    public function setNullIfInputLikeNull($str){
    	if($str == NULL || strtolower($str) == 'null'){
    		return NULL;
    	}else{
    		$str = trim($str);
    		return $str;
    	}
    }
    
}