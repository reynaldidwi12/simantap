<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lbr {

    function unsetDataItems($datas){

    	// print_r($datas);exit;

        unset($datas['kd_skpd']);
        unset($datas['kd_unitorganisasi']);
        unset($datas['kd_unitkerja']);
        unset($datas['kd_subunitkerja']);
        unset($datas['level']);
        unset($datas['atasan_id']);
        unset($datas['jenis_jabatan']);
        
        $items = array();
        if(count($datas['children']) > 0){

            foreach ($datas['children'] as $key => $value) {

                unset($value['kd_skpd']);
                unset($value['kd_unitorganisasi']);
                unset($value['kd_unitkerja']);
                unset($value['kd_subunitkerja']);
                unset($value['level']);
                unset($value['atasan_id']);
                unset($value['jenis_jabatan']);

                if(isset($value['children'])){

	                if(count($value['children']) > 0){

	                    $tmps = array();

	                    foreach ($value['children'] as $key2 => $value2) {
	                        
	                        $arr = (array) $value2;

	                        // print_r($arr['kd_skpd']);exit;

	                        unset($arr['kd_skpd']);
	                        unset($arr['kd_unitorganisasi']);
	                        unset($arr['kd_unitkerja']);
	                        unset($arr['kd_subunitkerja']);
	                        unset($arr['level']);
	                        unset($arr['atasan_id']);
	                        unset($arr['jenis_jabatan']);

							// print_r($arr['children']);exit;

	                        if(isset($arr['children'])){

				                if(count($arr['children']) > 0){

				                    $tmps2 = array();

				                    foreach ($arr['children'] as $key3 => $value3) {
				                        
				                        // print_r($value3->kd_skpd);exit;

				                        unset($value3->kd_skpd);
				                        unset($value3->kd_unitorganisasi);
				                        unset($value3->kd_unitkerja);
				                        unset($value3->kd_subunitkerja);
				                        unset($value3->level);
				                        unset($value3->atasan_id);
				                        unset($value3->jenis_jabatan);

				                        $tmps2[] = $value3;

				                    }

				                    unset($arr['children']);
				                    $arr['children'] = $tmps2;

				                }


			            	}
			            	// print_r($value2);exit;

	                        $tmps[] = $arr;

	                    }

	                    unset($value['children']);
	                    $value['children'] = $tmps;

	                }
            	}

                $items[] = $value;

            }
        }

        unset($datas['children']);

        $datas['children'] = $items;

        return $datas;
    }

    function getSingleSkpd($kd_skpd){
    	$CI =& get_instance();
    	$CI->load->model('skpd_model');

    	$data = $CI->skpd_model->fetchByIdSingle($kd_skpd);
    	return $data;

    }
    
}