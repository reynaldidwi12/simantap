<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('active_link_controller'))
{
    function active_link_controller($controller)
    {
        $CI    =& get_instance();
        $class = $CI->router->fetch_class();

        return ($class == $controller) ? 'active' : NULL;
    }
}


if ( ! function_exists('active_link_function'))
{
    function active_link_function($controller)
    {
        $CI    =& get_instance();
        $class = $CI->router->fetch_method();

        return ($class == $controller) ? 'active' : NULL;
    }
}

if ( ! function_exists('get_data_by_id'))
{
    function get_data_by_id($tableName='',$columnValue='',$colume='')
    {
        $CI = get_instance();
        $CI->db->select('*');
        $CI->db->from($tableName);
        $CI->db->where($colume , $columnValue);
        $query = $CI->db->get();
        return $result = $query->row();
    }
}

if ( ! function_exists('str_romawi_format'))
{
    function str_romawi_format($str)
    {
        $tr = trim($str);
        $str = str_replace(array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25'), array('I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII', 'XIV', 'XV', 'XVI', 'XVII', 'XVIII', 'XIX', 'XX', 'XXI', 'XXII', 'XXIII', 'XXIV', 'XXV'), $tr);
        return $str;
    }
}

if ( ! function_exists('tgl_indo'))
{
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}

if ( ! function_exists('convert_status_pegawai'))
{
    function convert_status_pegawai($status, $kategori, $sub_kategori) {
        if($sub_kategori == 'bup' || $sub_kategori == 'jft' || $sub_kategori == 'aps') {
            $sub_kategori = strtoupper($sub_kategori);
        }
        if($sub_kategori == 'janda_duda') {
            $sub_kategori = 'Janda / Duda';
        }
        if($sub_kategori == 'kenaikan_pangkat') {
            $sub_kategori = 'Kenaikan Pangkat';
        }
        $content = '';
        switch ($status) {
          case 'belum divalidasi':
            $content .= 'Pengiriman data pengajuan '.$kategori.' '.$sub_kategori;
            break;
          case 'validasi diterima':
            $content .= 'Pengajuan '.$kategori.' '.$sub_kategori.' telah di ACC';
            break;
          case 'berkas dikembalikan':
            $content .= 'Pengajuan berkas '.$kategori.' '.$sub_kategori.' dikembalikan';
            break;
          case 'berkas dikirim ulang':
            $content .= 'Pengiriman data pengajuan telah dikirim ulang';
            break;
          case 'dalam pengecekan':
            $content .= 'Pencocokan data oleh BKPSDM Bidang '.$kategori;
            break;
          case 'berkas fisik diterima':
            $content .= 'Berkas fisik telah diterima oleh BKPSDM';
            break;
          case 'berkas dibawa bkn':
            $content .= 'Berkas fisik telah dikirim ke BKN';
            break;
          default:
            $content .= '';
        } 

        return $content;
    }
}

if ( ! function_exists('convert_status_admin'))
{
    function convert_status_admin($status) {
        $content = '';
        switch ($status) {
          case 'belum divalidasi':
            $content .= 'Belum di Validasi';
            break;
          case 'validasi diterima':
            $content .= 'Menunggu Berkas Fisik';
            break;
          case 'berkas dikembalikan':
            $content .= 'Menunggu Perbaikan Berkas';
            break;
          case 'berkas dikirim ulang':
            $content .= 'Perbaikan berkas sudah diterima';
            break;
          case 'dalam pengecekan':
            $content .= 'Sedang di Validasi';
            break;
          case 'berkas fisik diterima':
            $content .= 'Berkas Telah diterima BKD';
            break;
          case 'berkas dibawa bkn':
            $content .= 'Berkas dibawa ke BKN';
            break;
          default:
            $content .= '';
        } 

        return $content;
    }
}
