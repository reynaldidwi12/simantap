<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

	public function get_data_pegawai($nip='') {

		if(!empty($nip)) {
			$query = $this->db->query("
				SELECT p.nip, ifnull(p.nip_lama,'-') nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, p.kelamin, p.agama, p.gol_darah,
		(case p.kelamin when '1' then 'Pria' when '2' then 'Wanita' end) jk,
		p.status, p.status_pegawai, p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos, p.no_ktp, p.tmt_cpns, p.tmt_pns, b.golongan, d.nama_jabatan as nama_jabatan, e.nama as skpd, f.nama as unitorganisasi
				FROM pegawai_tbl p 
				LEFT JOIN riwayat_kepangkatan_tbl b
				on b.tmt_pangkat IN(
				SELECT MAX(tmt_pangkat)
				FROM riwayat_kepangkatan_tbl 
				WHERE nip=b.nip)
				LEFT JOIN riwayat_jabatan_tbl c
				on c.tmt IN(
				SELECT MAX(tmt)
				FROM riwayat_jabatan_tbl 
				WHERE nip=c.nip)
				LEFT JOIN jabatan_tbl d on d.kd_jabatan = c.kd_jabatan
				LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
				LEFT JOIN unitorganisasi_tbl f on c.kd_unitorganisasi = f.kd_unitorganisasi
				
				WHERE 
				p.nip = b.nip AND
				p.nip = c.nip AND
				p.nip = '".$nip."'

				GROUP BY p.nip");

			return $query->result_array();
		} else {
			return false;
		}
	}

	public function updatenoimage($data) {
		// get data
		$this->nip_lama = $data['nip_lama'];
		$this->nama = $data['nama'];
		$this->gelar_depan = $data['gelar_depan'];
		$this->gelar_belakang = $data['gelar_belakang'];
		$this->tempat_lahir = $data['tempat_lahir'];
		$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));
		$this->tgl_lahir = $data['tgl_lahir2'];
		$this->telp = $data['telp'];
		$this->kelamin = $data['kelamin'];
		$this->agama = $data['agama'];
		$this->alamat = $data['alamat'];
		$this->kode_pos = $data['kode_pos'];
		$this->gol_darah = $data['gol_darah'];
		$this->status = $data['status'];
		$this->status_pegawai = $data['status_pegawai'];
		$this->no_kartu_pegawai = $data['no_kartu_pegawai'];
		$this->no_askes = $data['no_askes'];
		$this->no_kartu_keluarga = $data['no_kartu_keluarga'];
		$this->no_ktp = $data['no_ktp'];
		$this->npwp = $data['npwp'];
		$this->kd_skpd = $data['nama_skpd'];
		$this->kd_unitkerja = $data['nama_unit_kerja'];
		if($data['tmt_cpns']==''){
        }else{
                $data['tmt_cpns2'] = date("Y-m-d",strtotime($data['tmt_cpns']));
                $this->tmt_cpns = $data['tmt_cpns2'];
        }

        if($data['tmt_pns']==''){
        }else{
                $data['tmt_pns2'] = date("Y-m-d",strtotime($data['tmt_pns']));
                $this->tmt_pns = $data['tmt_pns2'];
        }

		
		// update data
		$this->db->update ( 'pegawai_tbl', $this, array ('nip' => $this->session->userdata('user_name')));
	}
	
	public function update($data) {
		// get data
		$this->nip_lama = $data['nip_lama'];
		$this->nama = $data['nama'];
		$this->picture = $data['picture'];
		$this->gelar_depan = $data['gelar_depan'];
		$this->gelar_belakang = $data['gelar_belakang'];
		$this->tempat_lahir = $data['tempat_lahir'];
		$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));
		$this->tgl_lahir = $data['tgl_lahir2'];
		$this->telp = $data['telp'];
		$this->kelamin = $data['kelamin'];
		$this->agama = $data['agama'];
		$this->alamat = $data['alamat'];
		$this->kode_pos = $data['kode_pos'];
		$this->gol_darah = $data['gol_darah'];
		$this->status = $data['status'];
		$this->status_pegawai = $data['status_pegawai'];
		$this->no_kartu_pegawai = $data['no_kartu_pegawai'];
		$this->no_askes = $data['no_askes'];
		$this->no_kartu_keluarga = $data['no_kartu_keluarga'];
		$this->no_ktp = $data['no_ktp'];
		$this->npwp = $data['npwp'];
		$this->kd_skpd = $data['nama_skpd'];
		$this->kd_unitkerja = $data['nama_unit_kerja'];
		if($data['tmt_cpns']==''){
        }else{
                $data['tmt_cpns2'] = date("Y-m-d",strtotime($data['tmt_cpns']));
                $this->tmt_cpns = $data['tmt_cpns2'];
        }

        if($data['tmt_pns']==''){
        }else{
                $data['tmt_pns2'] = date("Y-m-d",strtotime($data['tmt_pns']));
                $this->tmt_pns = $data['tmt_pns2'];
        }

		
		// update data
		$this->db->update ( 'pegawai_tbl', $this, array('nip' => $this->session->userdata('user_name')));
	}
	
}
