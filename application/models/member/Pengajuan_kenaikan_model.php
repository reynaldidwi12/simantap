<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_kenaikan_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

	function get_data_by($tableName='', $value='', $colum='', $order_by='') {
		if((!empty($value)) && (!empty($colum))) {
			$this->db->where($colum, $value);
			$this->db->where('kategori', 'kenaikan');
		}
		$this->db->select('*');
		$this->db->from($tableName);
		if(!empty($order_by)) {
			$this->db->order_by($order_by, 'asc');
		}
		$query = $this->db->get();
		return $query->result();
  	}

  	function get_data_by_row($tableName = '', $columnValue = '', $colume = '', $orderValue = '', $ordere = '', $limit = '') {
		$year = date('Y');
		$this->db->select('*');
		$this->db->from($tableName);
		$this->db->where($colume, $columnValue);
		$this->db->where('tahun', $year);
		$this->db->where('kategori', 'kenaikan');
		if (!empty($orderValue) && !empty($ordere)) {
			$this->db->order_by($ordere, $orderValue);
		}
		if (!empty($limit)) {
			$this->db->limit($limit);
		}
		$query = $this->db->get();
		return $query->row();
	}

  	function create($data) {

		$this->nip = $data['nip'];
		$this->kd_skpd = $data['kd_skpd'];
		$this->tahun = $data['year'];
		$this->kategori = $data['kategori'];
		$this->sub_kategori = $data['sub_kategori'];
		$this->tanggal_masuk = date('Y-m-d H:i:s');
		$this->user_masuk = $data['user_masuk'];
		$this->status = $data['status'];
		
		// insert data
		$this->db->insert('pengajuan_tbl', $this);
	}

	public function getDataById($id){
		$this->db->select ('*');
		$this->db->from ('pengajuan_tbl');
		$this->db->where('id', $id);
		$query = $this->db->get()->row_array();
		return $query;
	}

	public function updateRow($table, $col, $colVal, $data) {
  		$this->db->where($col,$colVal);
		$this->db->update($table,$data);
		return true;
	}
	
}
