<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

	public function get_data_pegawai() {

		$query = $this->db->query("
			SELECT p.nip, p.nama, p.telp, p.status_pegawai, c.kd_skpd as kdskpd
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_kepangkatan_tbl b
			on b.tmt_pangkat IN(
			SELECT MAX(tmt_pangkat)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN riwayat_jabatan_tbl c
			on c.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=c.nip)
	
			WHERE 
			p.nip = b.nip AND
			p.nip = c.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal'

			GROUP BY p.nip");

		return $query->result();
	}

	public function get_users_by_peg($nip) {
		$query = $this->db->query("
			SELECT u.username
			FROM users u
	
			WHERE 
			u.username = $nip");

		return $query->num_rows();
	}
}