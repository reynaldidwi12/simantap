<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_count_record($table)
    {
        $query = $this->db->count_all($table);

        return $query;
    }


    public function disk_totalspace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_total_space($dir);
    }


    public function disk_freespace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_free_space($dir);
    }


    public function disk_usespace($dir = DIRECTORY_SEPARATOR)
    {
        return $this->disk_totalspace($dir) - $this->disk_freespace($dir);
    }


    public function disk_freepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_freespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function disk_usepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_usespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function memory_usage()
    {
        return memory_get_usage();
    }


    public function memory_peak_usage($real = TRUE)
    {
        if ($real)
        {
            return memory_get_peak_usage(TRUE);
        }
        else
        {
            return memory_get_peak_usage(FALSE);
        }
    }


    public function memory_usepercent($real = TRUE, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->memory_usage() * 100) / $this->memory_peak_usage($real), 0).$unit;
    }
	
	public function jml_pegawai() {
		return $this->db->count_all("pegawai_tbl
						WHERE status_pegawai != 'Pindah Tugas Keluar'
						AND status_pegawai != 'Pensiun'
						AND status_pegawai != 'Meninggal'");
	}

	public function jml_pegawai_per_skpd($kd_skpd='') {

		$query = $this->db->query("
			SELECT p.nip
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			
			WHERE 
			(p.nip = b.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$kd_skpd."') OR (
			p.nip = b.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$kd_skpd."' AND
			c.kd_unitorganisasi IS NULL)

			GROUP BY p.nip , c.kd_skpd");

		return $query->num_rows();
	}

	public function jml_naik_pangkat_per_skpd($kd_skpd='') {

		$query = $this->db->query("
			SELECT p.nip
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_kepangkatan_tbl b
			on b.tmt_pangkat IN(
			SELECT MAX(tmt_pangkat)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN riwayat_jabatan_tbl c
			on c.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=c.nip)
			LEFT JOIN jabatan_tbl d on d.kd_jabatan = c.kd_jabatan
			
			WHERE 
			p.nip = b.nip AND
			p.nip = c.nip AND
			d.kd_skpd = '".$kd_skpd."' AND 
			YEAR(curdate())-YEAR(tmt_pangkat) > 4

			GROUP BY p.nip , d.kd_skpd");

		return $query->num_rows();
	}
	
	public function jml_pensiun_per_skpd($kd_skpd='') {

		$query = $this->db->query("
			SELECT p.nip
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			
			WHERE 
			p.nip = b.nip AND
			c.kd_skpd = '".$kd_skpd."' AND
			YEAR(curdate())-YEAR(p.tgl_lahir) > 55

			GROUP BY p.nip , c.kd_skpd");

		return $query->num_rows();
	}

	public function jumlah_pegawai_golongan_per_skpd($kd_skpd='') {
		$sql = "
		SELECT
		SUM( IF(d.golongan='I/a','1','0') ) as Ia,
		SUM( IF(d.golongan='I/b','1','0') ) as Ib,
		SUM( IF(d.golongan='I/c','1','0') ) as Ic,
		SUM( IF(d.golongan='I/d','1','0') ) as Id,
		SUM( IF(d.golongan='II/a','1','0') ) as IIa,
		SUM( IF(d.golongan='II/b','1','0') ) as IIb,
		SUM( IF(d.golongan='II/c','1','0') ) as IIc,
		SUM( IF(d.golongan='II/d','1','0') ) as IId,
		SUM( IF(d.golongan='III/a','1','0') ) as IIIa,
		SUM( IF(d.golongan='III/b','1','0') ) as IIIb,
		SUM( IF(d.golongan='III/c','1','0') ) as IIIc,
		SUM( IF(d.golongan='III/d','1','0') ) as IIId,
		SUM( IF(d.golongan='IV/a','1','0') ) as IVa,
		SUM( IF(d.golongan='IV/b','1','0') ) as IVb,
		SUM( IF(d.golongan='IV/c','1','0') ) as IVc,
		SUM( IF(d.golongan='IV/d','1','0') ) as IVd
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		WHERE p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_skpd = ".$kd_skpd."
		GROUP BY c.kd_skpd";
			   
		$query = $this->db->query($sql);

		return $query->result();
    }

    public function jumlah_pegawai_pendidikan_per_skpd($kd_skpd="") {
    	$sql = "SELECT
			SUM( IF(e.jenis_pendidikan='1','1','0') ) as SD,
			SUM( IF(e.jenis_pendidikan='2','1','0') ) as SMP,
			SUM( IF(e.jenis_pendidikan='3','1','0') ) as SMA,
			SUM( IF(e.jenis_pendidikan='4','1','0') ) as D1,
			SUM( IF(e.jenis_pendidikan='6','1','0') ) as D3,
			SUM( IF(e.jenis_pendidikan='7','1','0') ) as S1,
			SUM( IF(e.jenis_pendidikan='8','1','0') ) as S2,
			SUM( IF(e.jenis_pendidikan='9','1','0') ) as S3
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_pendidikan_tbl e
		on e.jenis_pendidikan IN(
		SELECT MAX(jenis_pendidikan)
		FROM riwayat_pendidikan_tbl
		WHERE nip=e.nip)
		
		WHERE p.nip = b.nip AND
		p.nip = e.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_skpd = ".$kd_skpd."
		GROUP BY c.kd_skpd";
		
		$query = $this->db->query($sql);

		return $query->result();
			
	}
	
	public function jml_skpd() {
		return $this->db->count_all("skpd_tbl where nama like 'dinas%'");
	}
	
	public function jml_skpd2() {
		return $this->db->count_all("skpd_tbl where nama like 'sekre%'");
	}
	
	public function jml_skpd3() {
		return $this->db->count_all("skpd_tbl where nama like 'badan%'");
	}
	
	public function jml_skpd4() {
		return $this->db->count_all("unitkerja_tbl where nama like 'bagian%'");
	}
	
	public function jml_skpd5() {
		return $this->db->count_all("skpd_tbl where nama like 'kecamatan%'");
	}
	
	public function jml_skpd6() {
		return $this->db->count_all("unitkerja_tbl where nama like 'kelurahan%'");
	}
	
	public function jml_naik_pangkat() {
		return $this->db->count_all("pegawai_tbl p, riwayat_kepangkatan_tbl b 
										WHERE b.tmt_pangkat IN(
										SELECT MAX(tes.tmt_pangkat)
										FROM riwayat_kepangkatan_tbl tes
										where tes.nip=b.nip)
										and p.nip = b.nip
										and p.status_pegawai != 'Pindah Tugas Keluar' 
										and p.status_pegawai != 'Pensiun'
										and p.status_pegawai != 'Meninggal'
										and YEAR(curdate())-YEAR(tmt_pangkat) > 4 ");
	}
	
	public function jml_pensiun() {
		return $this->db->count_all("pegawai_tbl WHERE YEAR(curdate())-YEAR(tgl_lahir) > 55
										AND status_pegawai != 'Pindah Tugas Keluar' 
										AND status_pegawai != 'Pensiun'
										AND status_pegawai != 'Meninggal'");
	}
	
	
	public function jml_pegawai_skpd($kd_skpd) {
		return $this->db->count_all("pegawai_tbl where kd_skpd='$kd_skpd'");
	}
	
	public function jml_naik_pangkat_skpd($kd_skpd) {
		return $this->db->count_all("pegawai_tbl p, riwayat_kepangkatan_tbl b 
										WHERE b.tmt_pangkat IN(
										SELECT MAX(tes.tmt_pangkat)
										FROM riwayat_kepangkatan_tbl tes
										where tes.nip=b.nip)
										and p.nip = b.nip
										AND p.kd_skpd = '$kd_skpd'
										and YEAR(curdate())-YEAR(tmt_pangkat) > 4 ");
	}
	
	public function jml_pensiun_skpd($kd_skpd) {
		return $this->db->count_all("pegawai_tbl WHERE kd_skpd='$kd_skpd' AND YEAR(curdate())-YEAR(tgl_lahir) > 55");
	}
	
	public function jumlah_pegawai_golongan() {
			$query = $this->db->query("
   
			select
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'I/a' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `Ia`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'I/b' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `Ib`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'I/c' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `Ic`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'I/d' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `Id`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'II/a' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIa`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'II/b' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIb`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'II/c' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIc`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'II/d' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IId`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'III/a' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIIa`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'III/b' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIIb`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'III/c' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIIc`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'III/d' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIId`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'IV/a' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IVa`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'IV/b' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IVb`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'IV/c' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IVc`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_kepangkatan_tbl a, (select nip, MAX(golongan) golongan from riwayat_kepangkatan_tbl 
					group by nip order by golongan desc) as b where b.golongan = 'IV/d' 
					and a.golongan = b.golongan 
					and a.nip = b.nip
					and p.nip = a.nip
					and p.nip = b.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IVd`


			  ");
			   
			  return $query->result();
    }
	
	public function jumlah_pegawai_golongan_skpd($kd_skpd) {
			$query = $this->db->query("
   
			select
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					WHERE d.golongan = 'I/a' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `Ia`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p,  riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d  
					where d.golongan = 'I/b' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `Ib`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'I/c' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `Ic`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'I/d' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `Id`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'II/a' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIa`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'II/b' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIb`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'II/c' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIc`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'II/d' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IId`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'III/a' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIIa`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'III/b' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIIb`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'III/c' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIIc`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'III/d' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IIId`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'IV/a' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IVa`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'IV/b' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IVb`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'IV/c' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IVc`,
			ifnull((SELECT COUNT(*) FROM pegawai_tbl p, riwayat_kepangkatan_tbl a, (SELECT b.nip, MAX(golongan) golongan
					FROM pegawai_tbl b, riwayat_kepangkatan_tbl c
					WHERE b.nip=c.nip
					AND b.kd_skpd='$kd_skpd'
					GROUP BY nip
					ORDER BY golongan DESC) AS d 
					where d.golongan = 'IV/d' 
					and a.golongan = d.golongan 
					and a.nip = d.nip
					and a.nip = p.nip
					and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'
					order by a.golongan desc),0) AS `IVd`

			  ");
			   
			  return $query->result();
    }
	
	public function jumlah_pegawai_pendidikan() {
			$query = $this->db->query("
   
			select
			ifnull((select count(*) from pegawai_tbl p, riwayat_pendidikan_tbl b where jenis_pendidikan = '1' and p.nip = b.nip and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'),0) AS `SD`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_pendidikan_tbl b where jenis_pendidikan = '2' and p.nip = b.nip and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'),0) AS `SMP`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_pendidikan_tbl b where jenis_pendidikan = '3' and p.nip = b.nip and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'),0) AS `SMA`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_pendidikan_tbl b where jenis_pendidikan >= '4' and jenis_pendidikan <= '5' and p.nip = b.nip),0) AS `D1`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_pendidikan_tbl b where jenis_pendidikan = '6' and p.nip = b.nip and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'),0) AS `D3`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_pendidikan_tbl b where jenis_pendidikan = '7' and p.nip = b.nip and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'),0) AS `S1`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_pendidikan_tbl b where jenis_pendidikan = '8' and p.nip = b.nip and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'),0) AS `S2`,
			ifnull((select count(*) from pegawai_tbl p, riwayat_pendidikan_tbl b where jenis_pendidikan = '9' and p.nip = b.nip and p.status_pegawai != 'Pindah Tugas Keluar' 
					and p.status_pegawai != 'Pensiun'
					and p.status_pegawai != 'Meninggal'),0) AS `S3`

			  ");
			   
			  return $query->result();
    }
	
	public function jumlah_pegawai_pendidikan_skpd($kd_skpd) {
			$query = $this->db->query("
   
			select
			ifnull((SELECT COUNT(*)
					FROM pegawai_tbl a, riwayat_pendidikan_tbl b
					WHERE b.jenis_pendidikan = '1' AND a.nip=b.nip AND a.kd_skpd='$kd_skpd' and a.status_pegawai != 'Pindah Tugas Keluar' 
					and a.status_pegawai != 'Pensiun'
					and a.status_pegawai != 'Meninggal'),0) AS `SD`,
			ifnull((SELECT COUNT(*)
					FROM pegawai_tbl a, riwayat_pendidikan_tbl b
					WHERE b.jenis_pendidikan = '2' AND a.nip=b.nip AND a.kd_skpd='$kd_skpd' and a.status_pegawai != 'Pindah Tugas Keluar' 
					and a.status_pegawai != 'Pensiun'
					and a.status_pegawai != 'Meninggal'),0) AS `SMP`,
			ifnull((SELECT COUNT(*)
					FROM pegawai_tbl a, riwayat_pendidikan_tbl b
					WHERE b.jenis_pendidikan = '3' AND a.nip=b.nip AND a.kd_skpd='$kd_skpd' and a.status_pegawai != 'Pindah Tugas Keluar' 
					and a.status_pegawai != 'Pensiun'
					and a.status_pegawai != 'Meninggal'),0) AS `SMA`,
			ifnull((SELECT COUNT(*)
					FROM pegawai_tbl a, riwayat_pendidikan_tbl b
					WHERE b.jenis_pendidikan >= '4' and b.jenis_pendidikan <='5' AND a.nip=b.nip AND a.kd_skpd='$kd_skpd'  and a.status_pegawai != 'Pindah Tugas Keluar' 
					and a.status_pegawai != 'Pensiun'
					and a.status_pegawai != 'Meninggal'),0) AS `D1`,
			ifnull((SELECT COUNT(*)
					FROM pegawai_tbl a, riwayat_pendidikan_tbl b
					WHERE b.jenis_pendidikan = '6' AND a.nip=b.nip AND a.kd_skpd='$kd_skpd' and a.status_pegawai != 'Pindah Tugas Keluar' 
					and a.status_pegawai != 'Pensiun'
					and a.status_pegawai != 'Meninggal'),0) AS `D3`,
			ifnull((SELECT COUNT(*)
					FROM pegawai_tbl a, riwayat_pendidikan_tbl b
					WHERE b.jenis_pendidikan = '7' AND a.nip=b.nip AND a.kd_skpd='$kd_skpd' and a.status_pegawai != 'Pindah Tugas Keluar' 
					and a.status_pegawai != 'Pensiun'
					and a.status_pegawai != 'Meninggal'),0) AS `S1`,
			ifnull((SELECT COUNT(*)
					FROM pegawai_tbl a, riwayat_pendidikan_tbl b
					WHERE b.jenis_pendidikan = '8' AND a.nip=b.nip AND a.kd_skpd='$kd_skpd' and a.status_pegawai != 'Pindah Tugas Keluar' 
					and a.status_pegawai != 'Pensiun'
					and a.status_pegawai != 'Meninggal'),0) AS `S2`,
			ifnull((SELECT COUNT(*)
					FROM pegawai_tbl a, riwayat_pendidikan_tbl b
					WHERE b.jenis_pendidikan = '9' AND a.nip=b.nip AND a.kd_skpd='$kd_skpd' and a.status_pegawai != 'Pindah Tugas Keluar' 
					and a.status_pegawai != 'Pensiun'
					and a.status_pegawai != 'Meninggal'),0) AS `S3`

			  ");
			   
			  return $query->result();
	}
	
	public function jumlah_pegawai_eselon($value=''){
		
		$sql = "
		SELECT e.kd_skpd, e.nama, 
		SUM( IF(b.esselon IN('21','22'),'1','0') ) as eselon2,
		SUM( IF(b.esselon IN('31','32'),'1','0') ) as eselon3,
		SUM( IF(b.esselon IN('41','42'),'1','0') ) as eselon4,
		SUM( IF(b.esselon NOT IN('00','11','12','21','22','31','32','41','42','51'),'1','0') ) as staff
		FROM pegawai_tbl p
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%'";
		
		if(!empty($value) || $value="") {
			$sql .= " AND d.golongan IN ('".$value."')";
		}

		$sql .= ") 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL";

		if(!empty($value) || $value="") {
			$sql .= " AND d.golongan IN ('".$array."')";
		}
		
		$sql .= ") GROUP BY c.kd_skpd
		ORDER BY e.category ASC";
		
		$query = $this->db->query($sql);

		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function jumlah_pegawai_eselon_per_skpd($value='', $kd_skpd=''){
		
		$sql = "
		SELECT e.kd_skpd, e.nama, 
		SUM( IF(b.esselon IN('21','22'),'1','0') ) as eselon2,
		SUM( IF(b.esselon IN('31','32'),'1','0') ) as eselon3,
		SUM( IF(b.esselon IN('41','42'),'1','0') ) as eselon4,
		SUM( IF(b.esselon NOT IN('00','11','12','21','22','31','32','41','42','51'),'1','0') ) as staff
		FROM pegawai_tbl p
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd = '".$kd_skpd."' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%'";
		
		if(!empty($value) || $value="") {
			$sql .= " AND d.golongan IN ('".$value."')";
		}

		$sql .= ") 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd = '".$kd_skpd."' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL";

		if(!empty($value) || $value="") {
			$sql .= " AND d.golongan IN ('".$array."')";
		}
		
		$sql .= ") GROUP BY c.kd_skpd
		ORDER BY e.category ASC";
		
		$query = $this->db->query($sql);

		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
}
