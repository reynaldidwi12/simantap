<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Pengajuan_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("pengajuan_tbl");
	}
	
	public function fetchAll($limit, $start) {
		$this->db->select ('*');
		$this->db->from ('pengajuan_tbl');
		$this->db->order_by ('id DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('*');
		$this->db->from ('pengajuan_tbl');
		$this->db->where('id', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function fetchByIdSingle($id){
		$this->db->select ('*');
		$this->db->from ('pengajuan_tbl');
		$this->db->where('id', $id);
		$query = $this->db->get()->row();
		return $query;
	}
	
	//--- untuk get skpd user ---//
	 public function fetchById2($id){
        $this->db->where('id', $id);
        $query = $this->db->get('pengajuan_tbl');
        return $query->result();
    }
	
	public function search_count($column, $data){
		return  $this->db->count_all("pengajuan_tbl where $column like '%$data%'");
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('*');
		$this->db->from ('pengajuan_tbl');
		$this->db->order_by ('id DESC');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getDataById($id){
		$this->db->select ('*');
		$this->db->from ('pengajuan_tbl');
		$this->db->where('id', $id);
		$query = $this->db->get()->row_array();
		return $query;
	}

	public function get_data_pegawai($nip='') {

		if(!empty($nip)) {
			$query = $this->db->query("
				SELECT p.nip, ifnull(p.nip_lama,'-') nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, p.kelamin, p.agama, p.gol_darah,
		(case p.kelamin when '1' then 'Pria' when '2' then 'Wanita' end) jk,
		p.status, p.status_pegawai, p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos, p.no_ktp, p.tmt_cpns, p.tmt_pns, b.golongan, d.nama_jabatan as nama_jabatan, e.nama as skpd, f.nama as unitorganisasi
				FROM pegawai_tbl p 
				LEFT JOIN riwayat_kepangkatan_tbl b
				on b.tmt_pangkat IN(
				SELECT MAX(tmt_pangkat)
				FROM riwayat_kepangkatan_tbl 
				WHERE nip=b.nip)
				LEFT JOIN riwayat_jabatan_tbl c
				on c.tmt IN(
				SELECT MAX(tmt)
				FROM riwayat_jabatan_tbl 
				WHERE nip=c.nip)
				LEFT JOIN jabatan_tbl d on d.kd_jabatan = c.kd_jabatan
				LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
				LEFT JOIN unitorganisasi_tbl f on c.kd_unitorganisasi = f.kd_unitorganisasi
				
				WHERE 
				p.nip = b.nip AND
				p.nip = c.nip AND
				p.nip = '".$nip."'

				GROUP BY p.nip");

			return $query->result_array();
		} else {
			return false;
		}
	}

	function get_data_by($tableName='', $value='', $colum='', $value2='', $colum2='', $order_by='') {
		if((!empty($value)) && (!empty($colum))) {
			$this->db->where($colum, $value);
		}
		if((!empty($value2)) && (!empty($colum2))) {
			$this->db->where($colum2, $value2);
		}
		$this->db->select('*');
		$this->db->from($tableName);
		if(!empty($order_by)) {
			$this->db->order_by($order_by, 'asc');
		}
		$query = $this->db->get();
		return $query->result();
  	}
	
  	public function delete($id) {
		$this->db->delete ('pengajuan_tbl', array ('id' => $id));
	}

	public function updateRow($table, $col, $colVal, $data) {
  		$this->db->where($col,$colVal);
		$this->db->update($table,$data);
		return true;
	}
}