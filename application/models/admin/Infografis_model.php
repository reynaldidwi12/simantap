<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infografis_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getDataEselon() {
		$sql = "
		SELECT e.kd_skpd, e.nama, SUM( IF(b.esselon IN(12,21,22,31,32,41,42),'1','0')  ) as total_pegawai, SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),'1','0'),'0')  ) as L,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),'1','0'),'0') ) as P,
		SUM( IF(p.kelamin='1',IF(b.esselon='12','1','0'),'0') ) as L1B,
		SUM( IF(p.kelamin='2',IF(b.esselon='12','1','0'),'0') ) as P1B,
		SUM( IF(p.kelamin='1',IF(b.esselon='21','1','0'),'0') ) as L2A,
		SUM( IF(p.kelamin='2',IF(b.esselon='21','1','0'),'0') ) as P2A,
		SUM( IF(p.kelamin='1',IF(b.esselon='22','1','0'),'0') ) as L2B,
		SUM( IF(p.kelamin='2',IF(b.esselon='22','1','0'),'0') ) as P2B,
		SUM( IF(p.kelamin='1',IF(b.esselon='31','1','0'),'0') ) as L3A,
		SUM( IF(p.kelamin='2',IF(b.esselon='31','1','0'),'0') ) as P3A,
		SUM( IF(p.kelamin='1',IF(b.esselon='32','1','0'),'0') ) as L3B,
		SUM( IF(p.kelamin='2',IF(b.esselon='32','1','0'),'0') ) as P3B,
		SUM( IF(p.kelamin='1',IF(b.esselon='41','1','0'),'0') ) as L4A,
		SUM( IF(p.kelamin='2',IF(b.esselon='41','1','0'),'0') ) as P4A,
		SUM( IF(p.kelamin='1',IF(b.esselon='42','1','0'),'0') ) as L4B,
		SUM( IF(p.kelamin='2',IF(b.esselon='42','1','0'),'0') ) as P4B,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='1','1','0'),'0'),'0') ) as LSD,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='1','1','0'),'0'),'0') ) as PSD,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='2','1','0'),'0'),'0') ) as LSMP,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='2','1','0'),'0'),'0') ) as PSMP,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='3','1','0'),'0'),'0') ) as LSMA,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='3','1','0'),'0'),'0') ) as PSMA,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='4','1','0'),'0'),'0') ) as LD1,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='4','1','0'),'0'),'0') ) as PD1,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='6','1','0'),'0'),'0') ) as LD3,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='6','1','0'),'0'),'0') ) as PD3,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='7','1','0'),'0'),'0') ) as LS1,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='7','1','0'),'0'),'0') ) as PS1,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='8','1','0'),'0'),'0') ) as LS2,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='8','1','0'),'0'),'0') ) as PS2,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='9','1','0'),'0'),'0') ) as LS3,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='9','1','0'),'0'),'0') ) as PS3
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN ( SELECT nip, MAX(jenis_pendidikan) as jenispendidikan FROM riwayat_pendidikan_tbl GROUP BY nip ) h ON d.nip = h.nip
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%') 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL)
		GROUP BY c.kd_skpd
		ORDER BY e.category ASC";
		
		$query = $this->db->query($sql);
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getDataEselonByCat($value, $subvalue) {

		$array=array_map('intval', explode(',', $value));
		$array=implode("','",$array);

		$sql = "
		SELECT e.kd_skpd, e.nama, SUM( IF(b.esselon IN(12,21,22,31,32,41,42),'1','0')  ) as total_pegawai, SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),'1','0'),'0')  ) as L,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),'1','0'),'0') ) as P,
		SUM( IF(p.kelamin='1',IF(b.esselon='12','1','0'),'0') ) as L1B,
		SUM( IF(p.kelamin='2',IF(b.esselon='12','1','0'),'0') ) as P1B,
		SUM( IF(p.kelamin='1',IF(b.esselon='21','1','0'),'0') ) as L2A,
		SUM( IF(p.kelamin='2',IF(b.esselon='21','1','0'),'0') ) as P2A,
		SUM( IF(p.kelamin='1',IF(b.esselon='22','1','0'),'0') ) as L2B,
		SUM( IF(p.kelamin='2',IF(b.esselon='22','1','0'),'0') ) as P2B,
		SUM( IF(p.kelamin='1',IF(b.esselon='31','1','0'),'0') ) as L3A,
		SUM( IF(p.kelamin='2',IF(b.esselon='31','1','0'),'0') ) as P3A,
		SUM( IF(p.kelamin='1',IF(b.esselon='32','1','0'),'0') ) as L3B,
		SUM( IF(p.kelamin='2',IF(b.esselon='32','1','0'),'0') ) as P3B,
		SUM( IF(p.kelamin='1',IF(b.esselon='41','1','0'),'0') ) as L4A,
		SUM( IF(p.kelamin='2',IF(b.esselon='41','1','0'),'0') ) as P4A,
		SUM( IF(p.kelamin='1',IF(b.esselon='42','1','0'),'0') ) as L4B,
		SUM( IF(p.kelamin='2',IF(b.esselon='42','1','0'),'0') ) as P4B,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='1','1','0'),'0'),'0') ) as LSD,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='1','1','0'),'0'),'0') ) as PSD,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='2','1','0'),'0'),'0') ) as LSMP,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='2','1','0'),'0'),'0') ) as PSMP,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='3','1','0'),'0'),'0') ) as LSMA,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='3','1','0'),'0'),'0') ) as PSMA,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='4','1','0'),'0'),'0') ) as LD1,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='4','1','0'),'0'),'0') ) as PD1,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='6','1','0'),'0'),'0') ) as LD3,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='6','1','0'),'0'),'0') ) as PD3,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='7','1','0'),'0'),'0') ) as LS1,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='7','1','0'),'0'),'0') ) as PS1,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='8','1','0'),'0'),'0') ) as LS2,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='8','1','0'),'0'),'0') ) as PS2,
		SUM( IF(p.kelamin='1',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='9','1','0'),'0'),'0') ) as LS3,
		SUM( IF(p.kelamin='2',IF(b.esselon IN(12,21,22,31,32,41,42),IF(h.jenispendidikan='9','1','0'),'0'),'0') ) as PS3
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN ( SELECT nip, MAX(jenis_pendidikan) as jenispendidikan FROM riwayat_pendidikan_tbl GROUP BY nip ) h ON d.nip = h.nip
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%'";
		
		if(!empty($value) || $value="") {
			$sql .= " AND e.category IN ('".$array."')";
		}

		if(!empty($subvalue) || $subvalue="") {
			$sql .= " AND e.kd_skpd = " . $subvalue;
		}

		$sql .= ")
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL";

		if(!empty($value) || $value="") {
			$sql .= " AND e.category IN ('".$array."')";
		}

		if(!empty($subvalue) || $subvalue="") {
			$sql .= " AND e.kd_skpd = " . $subvalue;
		}
		
		$sql .= ") GROUP BY c.kd_skpd
		ORDER BY e.category ASC";
		
		$query = $this->db->query($sql);
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getDataGolongan() {
		$sql = "
		SELECT e.kd_skpd, e.nama, COUNT(p.nip) as total_pegawai, SUM( CASE WHEN p.kelamin = '1' THEN 1 ELSE 0 END ) as L,
		SUM( CASE WHEN p.kelamin = '2' THEN 1 ELSE 0 END ) as P,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/a','1','0'),'0') ) as L1A,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/a','1','0'),'0') ) as P1A,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/b','1','0'),'0') ) as L1B,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/b','1','0'),'0') ) as P1B,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/c','1','0'),'0') ) as L1C,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/c','1','0'),'0') ) as P1C,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/d','1','0'),'0') ) as L1D,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/d','1','0'),'0') ) as P1D,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/a','1','0'),'0') ) as L2A,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/a','1','0'),'0') ) as P2A,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/b','1','0'),'0') ) as L2B,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/b','1','0'),'0') ) as P2B,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/c','1','0'),'0') ) as L2C,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/c','1','0'),'0') ) as P2C,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/d','1','0'),'0') ) as L2D,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/d','1','0'),'0') ) as P2D,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/a','1','0'),'0') ) as L3A,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/a','1','0'),'0') ) as P3A,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/b','1','0'),'0') ) as L3B,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/b','1','0'),'0') ) as P3B,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/c','1','0'),'0') ) as L3C,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/c','1','0'),'0') ) as P3C,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/d','1','0'),'0') ) as L3D,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/d','1','0'),'0') ) as P3D,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/a','1','0'),'0') ) as L4A,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/a','1','0'),'0') ) as P4A,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/b','1','0'),'0') ) as L4B,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/b','1','0'),'0') ) as P4B,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/c','1','0'),'0') ) as L4C,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/c','1','0'),'0') ) as P4C,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/d','1','0'),'0') ) as L4D,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/d','1','0'),'0') ) as P4D,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='1','1','0'),'0') ) as LSD,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='1','1','0'),'0') ) as PSD,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='2','1','0'),'0') ) as LSMP,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='2','1','0'),'0') ) as PSMP,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='3','1','0'),'0') ) as LSMA,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='3','1','0'),'0') ) as PSMA,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='4','1','0'),'0') ) as LD1,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='4','1','0'),'0') ) as PD1,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='6','1','0'),'0') ) as LD3,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='6','1','0'),'0') ) as PD3,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='7','1','0'),'0') ) as LS1,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='7','1','0'),'0') ) as PS1,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='8','1','0'),'0') ) as LS2,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='8','1','0'),'0') ) as PS2,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='9','1','0'),'0') ) as LS3,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='9','1','0'),'0') ) as PS3
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN ( SELECT nip, MAX(jenis_pendidikan) as jenispendidikan FROM riwayat_pendidikan_tbl GROUP BY nip ) h ON d.nip = h.nip
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%') 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL)
		GROUP BY c.kd_skpd
		ORDER BY e.category ASC";
		
		$query = $this->db->query($sql);
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getDataGolonganByCat($value, $subvalue) {

		$array=array_map('intval', explode(',', $value));
		$array=implode("','",$array);

		$sql = "
		SELECT e.kd_skpd, e.nama, COUNT(p.nip) as total_pegawai, SUM( CASE WHEN p.kelamin = '1' THEN 1 ELSE 0 END ) as L,
		SUM( CASE WHEN p.kelamin = '2' THEN 1 ELSE 0 END ) as P,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/a','1','0'),'0') ) as L1A,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/a','1','0'),'0') ) as P1A,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/b','1','0'),'0') ) as L1B,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/b','1','0'),'0') ) as P1B,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/c','1','0'),'0') ) as L1C,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/c','1','0'),'0') ) as P1C,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/d','1','0'),'0') ) as L1D,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/d','1','0'),'0') ) as P1D,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/a','1','0'),'0') ) as L2A,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/a','1','0'),'0') ) as P2A,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/b','1','0'),'0') ) as L2B,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/b','1','0'),'0') ) as P2B,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/c','1','0'),'0') ) as L2C,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/c','1','0'),'0') ) as P2C,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/d','1','0'),'0') ) as L2D,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/d','1','0'),'0') ) as P2D,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/a','1','0'),'0') ) as L3A,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/a','1','0'),'0') ) as P3A,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/b','1','0'),'0') ) as L3B,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/b','1','0'),'0') ) as P3B,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/c','1','0'),'0') ) as L3C,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/c','1','0'),'0') ) as P3C,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/d','1','0'),'0') ) as L3D,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/d','1','0'),'0') ) as P3D,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/a','1','0'),'0') ) as L4A,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/a','1','0'),'0') ) as P4A,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/b','1','0'),'0') ) as L4B,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/b','1','0'),'0') ) as P4B,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/c','1','0'),'0') ) as L4C,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/c','1','0'),'0') ) as P4C,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/d','1','0'),'0') ) as L4D,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/d','1','0'),'0') ) as P4D,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='1','1','0'),'0') ) as LSD,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='1','1','0'),'0') ) as PSD,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='2','1','0'),'0') ) as LSMP,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='2','1','0'),'0') ) as PSMP,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='3','1','0'),'0') ) as LSMA,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='3','1','0'),'0') ) as PSMA,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='4','1','0'),'0') ) as LD1,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='4','1','0'),'0') ) as PD1,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='6','1','0'),'0') ) as LD3,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='6','1','0'),'0') ) as PD3,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='7','1','0'),'0') ) as LS1,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='7','1','0'),'0') ) as PS1,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='8','1','0'),'0') ) as LS2,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='8','1','0'),'0') ) as PS2,
		SUM( IF(p.kelamin='1',IF(h.jenispendidikan='9','1','0'),'0') ) as LS3,
		SUM( IF(p.kelamin='2',IF(h.jenispendidikan='9','1','0'),'0') ) as PS3
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN ( SELECT nip, MAX(jenis_pendidikan) as jenispendidikan FROM riwayat_pendidikan_tbl GROUP BY nip ) h ON d.nip = h.nip
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%'";
		
		if(!empty($value) || $value="") {
			$sql .= " AND e.category IN ('".$array."')";
		}

		if(!empty($subvalue) || $subvalue="") {
			$sql .= " AND e.kd_skpd = " . $subvalue;
		}

		$sql .= ") 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL";

		if(!empty($value) || $value="") {
			$sql .= " AND e.category IN ('".$array."')";
		}

		if(!empty($subvalue) || $subvalue="") {
			$sql .= " AND e.kd_skpd = " . $subvalue;
		}
		
		$sql .= ") GROUP BY c.kd_skpd
		ORDER BY e.category ASC";
		
		$query = $this->db->query($sql);
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getDataPendidikan() {
		$sql = "
		SELECT f.kd_skpd, f.nama, SUM( IF(e.jenis_pendidikan IN(1,2,3,4,6,7,8,9),'1','0')  ) as total_pegawai, SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan IN(1,2,3,4,6,7,8,9),'1','0'),'0')  ) as L,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan IN(1,2,3,4,6,7,8,9),'1','0'),'0') ) as P,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='1','1','0'),'0') ) as LSD,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='1','1','0'),'0') ) as PSD,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='2','1','0'),'0') ) as LSMP,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='2','1','0'),'0') ) as PSMP,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='3','1','0'),'0') ) as LSMA,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='3','1','0'),'0') ) as PSMA,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='4','1','0'),'0') ) as LD1,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='4','1','0'),'0') ) as PD1,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='6','1','0'),'0') ) as LD3,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='6','1','0'),'0') ) as PD3,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='7','1','0'),'0') ) as LS1,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='7','1','0'),'0') ) as PS1,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='8','1','0'),'0') ) as LS2,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='8','1','0'),'0') ) as PS2,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='9','1','0'),'0') ) as LS3,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='9','1','0'),'0') ) as PS3
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN riwayat_pendidikan_tbl e on e.jenis_pendidikan IN( SELECT MAX(jenis_pendidikan) FROM riwayat_pendidikan_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl f on f.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.nip = e.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		f.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%') 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		p.nip = e.nip AND
		f.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL)
		GROUP BY c.kd_skpd
		ORDER BY f.category ASC";
		
		$query = $this->db->query($sql);
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getDataPendidikanByCat($value, $subvalue) {

		$array=array_map('intval', explode(',', $value));
		$array=implode("','",$array);
		
		$sql = "
		SELECT f.kd_skpd, f.nama, SUM( IF(e.jenis_pendidikan IN(1,2,3,4,6,7,8,9),'1','0')  ) as total_pegawai, SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan IN(1,2,3,4,6,7,8,9),'1','0'),'0')  ) as L,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan IN(1,2,3,4,6,7,8,9),'1','0'),'0') ) as P,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='1','1','0'),'0') ) as LSD,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='1','1','0'),'0') ) as PSD,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='2','1','0'),'0') ) as LSMP,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='2','1','0'),'0') ) as PSMP,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='3','1','0'),'0') ) as LSMA,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='3','1','0'),'0') ) as PSMA,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='4','1','0'),'0') ) as LD1,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='4','1','0'),'0') ) as PD1,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='6','1','0'),'0') ) as LD3,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='6','1','0'),'0') ) as PD3,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='7','1','0'),'0') ) as LS1,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='7','1','0'),'0') ) as PS1,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='8','1','0'),'0') ) as LS2,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='8','1','0'),'0') ) as PS2,
		SUM( IF(p.kelamin='1',IF(e.jenis_pendidikan='9','1','0'),'0') ) as LS3,
		SUM( IF(p.kelamin='2',IF(e.jenis_pendidikan='9','1','0'),'0') ) as PS3
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN riwayat_pendidikan_tbl e on e.jenis_pendidikan IN( SELECT MAX(jenis_pendidikan) FROM riwayat_pendidikan_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl f on f.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.nip = e.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		f.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%'";
		
		if(!empty($value) || $value="") {
			$sql .= " AND f.category IN ('".$array."')";
		}

		if(!empty($subvalue) || $subvalue="") {
			$sql .= " AND e.kd_skpd = " . $subvalue;
		}

		$sql .= ") 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		p.nip = e.nip AND
		f.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL";

		if(!empty($value) || $value="") {
			$sql .= " AND f.category IN ('".$array."')";
		}

		if(!empty($subvalue) || $subvalue="") {
			$sql .= " AND e.kd_skpd = " . $subvalue;
		}
		
		$sql .= ") GROUP BY c.kd_skpd
		ORDER BY f.category ASC";
		
		$query = $this->db->query($sql);

		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;

	}

	public function getDataDiklat() {
		$sql = "
		SELECT f.kd_skpd, f.nama, SUM( IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),'1','0')  ) as total_pegawai, SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),'1','0'),'0')  ) as L,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),'1','0'),'0') ) as P,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. I','1','0'),'0') ) as L1,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. I','1','0'),'0') ) as P1,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. II','1','0'),'0') ) as L2,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. II','1','0'),'0') ) as P2,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. III','1','0'),'0') ) as L3,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. III','1','0'),'0') ) as P3,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. IV','1','0'),'0') ) as L4,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. IV','1','0'),'0') ) as P4,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='1','1','0'),'0'),'0') ) as LSD,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='1','1','0'),'0'),'0') ) as PSD,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='2','1','0'),'0'),'0') ) as LSMP,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='2','1','0'),'0'),'0') ) as PSMP,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='3','1','0'),'0'),'0') ) as LSMA,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='3','1','0'),'0'),'0') ) as PSMA,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='4','1','0'),'0'),'0') ) as LD1,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='4','1','0'),'0'),'0') ) as PD1,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='6','1','0'),'0'),'0') ) as LD3,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='6','1','0'),'0'),'0') ) as PD3,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='7','1','0'),'0'),'0') ) as LS1,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='7','1','0'),'0'),'0') ) as PS1,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='8','1','0'),'0'),'0') ) as LS2,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='8','1','0'),'0'),'0') ) as PS2,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='9','1','0'),'0'),'0') ) as LS3,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='9','1','0'),'0'),'0') ) as PS3
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN ( SELECT nip, MAX(jenis_pendidikan) as jenispendidikan FROM riwayat_pendidikan_tbl GROUP BY nip ) h ON d.nip = h.nip
		LEFT JOIN riwayat_diklat_tbl e on e.tahun IN( SELECT MAX(tahun) FROM riwayat_diklat_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl f on f.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.nip = e.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		f.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%') 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		p.nip = e.nip AND
		f.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL)
		GROUP BY c.kd_skpd
		ORDER BY f.category ASC";
		
		$query = $this->db->query($sql);
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getDataDiklatByCat($value, $subvalue) {

		$array=array_map('intval', explode(',', $value));
		$array=implode("','",$array);

		$sql = "
		SELECT f.kd_skpd, f.nama, SUM( IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),'1','0')  ) as total_pegawai, SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),'1','0'),'0')  ) as L,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),'1','0'),'0') ) as P,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. I','1','0'),'0') ) as L1,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. I','1','0'),'0') ) as P1,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. II','1','0'),'0') ) as L2,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. II','1','0'),'0') ) as P2,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. III','1','0'),'0') ) as L3,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. III','1','0'),'0') ) as P3,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. IV','1','0'),'0') ) as L4,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat LIKE 'Diklat PIM Tingkat. IV','1','0'),'0') ) as P4,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='1','1','0'),'0'),'0') ) as LSD,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='1','1','0'),'0'),'0') ) as PSD,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='2','1','0'),'0'),'0') ) as LSMP,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='2','1','0'),'0'),'0') ) as PSMP,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='3','1','0'),'0'),'0') ) as LSMA,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='3','1','0'),'0'),'0') ) as PSMA,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='4','1','0'),'0'),'0') ) as LD1,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='4','1','0'),'0'),'0') ) as PD1,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='6','1','0'),'0'),'0') ) as LD3,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='6','1','0'),'0'),'0') ) as PD3,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='7','1','0'),'0'),'0') ) as LS1,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='7','1','0'),'0'),'0') ) as PS1,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='8','1','0'),'0'),'0') ) as LS2,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='8','1','0'),'0'),'0') ) as PS2,
		SUM( IF(p.kelamin='1',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='9','1','0'),'0'),'0') ) as LS3,
		SUM( IF(p.kelamin='2',IF(e.nama_diklat IN('Diklat PIM Tingkat. I','Diklat PIM Tingkat. II','Diklat PIM Tingkat. III','Diklat PIM Tingkat. IV'),IF(h.jenispendidikan='9','1','0'),'0'),'0') ) as PS3
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN ( SELECT nip, MAX(jenis_pendidikan) as jenispendidikan FROM riwayat_pendidikan_tbl GROUP BY nip ) h ON d.nip = h.nip
		LEFT JOIN riwayat_diklat_tbl e on e.tahun IN( SELECT MAX(tahun) FROM riwayat_diklat_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl f on f.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.nip = e.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		f.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%'";
		
		if(!empty($value) || $value="") {
			$sql .= " AND f.category IN ('".$array."')";
		}

		if(!empty($subvalue) || $subvalue="") {
			$sql .= " AND e.kd_skpd = " . $subvalue;
		}

		$sql .= ") 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		p.nip = e.nip AND
		f.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL";

		if(!empty($value) || $value="") {
			$sql .= " AND f.category IN ('".$array."')";
		}

		if(!empty($subvalue) || $subvalue="") {
			$sql .= " AND e.kd_skpd = " . $subvalue;
		}
		
		$sql .= ") GROUP BY c.kd_skpd
		ORDER BY f.category ASC";
		
		$query = $this->db->query($sql);

		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;

	}
}
