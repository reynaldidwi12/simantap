<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class report_skpd_model2 extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count($data) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, c.kd_skpd, c.kd_unitorganisasi, b.kd_jabatan, b.tmt, c.nama_jabatan, c.level, c.kd_unitorganisasi, c.position, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)
			LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi

			WHERE 
			(p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data."' AND
			g.nama NOT LIKE '%KECAMATAN%' AND
			g.nama NOT LIKE '%UPTD%' AND
			g.nama NOT LIKE '%PUSKESMAS%' AND
			g.nama NOT LIKE '%ASISTEN%' AND
			g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
			g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
			g.nama NOT LIKE '%SMP%' AND
			g.nama NOT LIKE '%SD%' AND
			g.nama NOT LIKE '%TK%') OR (
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data."' AND
			c.kd_unitorganisasi IS NULL)

			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			");
		return $query->result();
	}
	
	public function record_count2($data1, $data2) {
$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, c.kd_skpd, c.kd_unitkerja, c.kd_jabatan, b.tmt, c.nama_jabatan, c.level, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)
			LEFT JOIN unitkerja_tbl g on g.kd_unitkerja = c.kd_unitkerja

			WHERE 
			(p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data1."' AND
			c.kd_unitorganisasi = '".$data2."' AND
			g.nama NOT LIKE '%LURAH%') OR (
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data1."' AND
			c.kd_unitorganisasi = '".$data2."' AND
			c.kd_unitkerja IS NULL
			)

			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			");
		return $query->result();
	}

	public function record_count3($data1, $data2, $data3) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, b.kd_skpd, b.kd_unitkerja, b.kd_jabatan, b.tmt, c.nama_jabatan, c.level, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)

			WHERE 
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			b.kd_skpd = '".$data1."' AND
			b.kd_unitorganisasi = '".$data2."' AND
			b.kd_unitkerja = '".$data3."'
			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			");
		return $query->result();
	}

	public function record_count4($data1, $data2, $data3, $data4) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, b.kd_skpd, b.kd_unitkerja, b.kd_jabatan, b.tmt, c.nama_jabatan, c.level, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)

			WHERE 
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			b.kd_skpd = '".$data1."' AND
			b.kd_unitorganisasi = '".$data2."' AND
			b.kd_unitkerja = '".$data3."' AND
			b.kd_subunitkerja = '".$data4."' AND
			c.level <= '6' AND
			c.level >= '5'
			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			");
		return $query->result();
	}
	
	public function fetchAll($data, $limit, $start) {

		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, c.kd_skpd, c.kd_jabatan, b.tmt, c.nama_jabatan, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, (g.nama) AS unit_organisasi, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			INNER JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)
			LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi

			WHERE 
			(p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data."' AND
			g.nama NOT LIKE '%KECAMATAN%' AND
			g.nama NOT LIKE '%UPTD%' AND
			g.nama NOT LIKE '%PUSKESMAS%' AND
			g.nama NOT LIKE '%ASISTEN%' AND
			g.nama NOT LIKE '%SMP%' AND
			g.nama NOT LIKE '%SD%' AND
			g.nama NOT LIKE '%TK%') OR (
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data."' AND
			c.kd_unitorganisasi IS NULL)


			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			LIMIT ".$start.", ".$limit."
			");

		return $query->result();
	}
	
	
	public function fetchAll2($data1, $data2, $limit, $start) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, c.kd_skpd, c.kd_unitkerja, c.kd_jabatan, b.tmt, c.nama_jabatan, c.level, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)
			LEFT JOIN unitkerja_tbl g on g.kd_unitkerja = c.kd_unitkerja

			WHERE 
			(p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data1."' AND
			c.kd_unitorganisasi = '".$data2."' AND
			g.nama NOT LIKE '%LURAH%') OR (
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data1."' AND
			c.kd_unitorganisasi = '".$data2."' AND
			c.kd_unitkerja IS NULL
			)


			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			LIMIT ".$start.", ".$limit."
			");
		return $query->result();
	}

	public function fetchAll3($data1, $data2, $data3, $limit, $start) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, b.kd_skpd, b.kd_unitkerja, b.kd_jabatan, b.tmt, c.nama_jabatan, c.level, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)

			WHERE 
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			b.kd_skpd = '".$data1."' AND
			b.kd_unitorganisasi = '".$data2."' AND
			b.kd_unitkerja = '".$data3."'
			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			LIMIT ".$start.", ".$limit."
			");
		return $query->result();
	}

	public function fetchAll4($data1, $data2, $data3, $data4, $limit, $start) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, b.kd_skpd, b.kd_unitkerja, b.kd_jabatan, b.tmt, c.nama_jabatan, c.level, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)

			WHERE 
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			b.kd_skpd = '".$data1."' AND
			b.kd_unitorganisasi = '".$data2."' AND
			b.kd_unitkerja = '".$data3."' AND
			b.kd_subunitkerja = '".$data4."' AND
			c.level <= '6' AND
			c.level >= '5'
			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			LIMIT ".$start.", ".$limit."
			");
		return $query->result();
	}
	
	public function cetak($data) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, c.kd_skpd, c.kd_jabatan, b.tmt, c.nama_jabatan, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, (g.nama) AS unit_organisasi, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			INNER JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)
			LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi

			WHERE 
			(p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data."' AND
			g.nama NOT LIKE '%KECAMATAN%' AND
			g.nama NOT LIKE '%UPTD%' AND
			g.nama NOT LIKE '%PUSKESMAS%' AND
			g.nama NOT LIKE '%ASISTEN%' AND
			g.nama NOT LIKE '%SMP%' AND
			g.nama NOT LIKE '%SD%' AND
			g.nama NOT LIKE '%TK%') OR (
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data."' AND
			c.kd_unitorganisasi IS NULL)


			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			");
		return $query->result();
	}
	
	public function cetak2($data1, $data2) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, c.kd_skpd, c.kd_unitkerja, c.kd_jabatan, b.tmt, c.nama_jabatan, c.level, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)
			LEFT JOIN unitkerja_tbl g on g.kd_unitkerja = c.kd_unitkerja

			WHERE 
			(p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data1."' AND
			c.kd_unitorganisasi = '".$data2."' AND
			g.nama NOT LIKE '%LURAH%') OR (
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$data1."' AND
			c.kd_unitorganisasi = '".$data2."' AND
			c.kd_unitkerja IS NULL
			)
			
			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			");
		return $query->result();
	}

	public function cetak3($data1, $data2, $data3) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, b.kd_skpd, b.kd_unitkerja, b.kd_jabatan, b.tmt, c.nama_jabatan, c.level, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)

			WHERE 
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			b.kd_skpd = '".$data1."' AND
			b.kd_unitorganisasi = '".$data2."' AND
			b.kd_unitkerja = '".$data3."'
			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			");
		return $query->result();
	}

	public function cetak4($data1, $data2, $data3, $data4) {
		$query = $this->db->query("
			SELECT p.nip, p.nip_lama, p.gelar_depan, p.nama, p.gelar_belakang, p.tempat_lahir, p.tgl_lahir, p.kelamin, b.kd_skpd, b.kd_unitkerja, b.kd_jabatan, b.tmt, c.nama_jabatan, c.level, d.golongan, d.tmt_pangkat, d.mk_bulan, d.mk_tahun, e.jenis_pendidikan, e.nama_sekolah, e.tahun_ijazah, e.program_studi, f.tahun, f.nama_diklat, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN riwayat_kepangkatan_tbl d
			on d.golongan IN(
			SELECT MAX(golongan)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=d.nip)
			LEFT JOIN riwayat_pendidikan_tbl e
			on e.jenis_pendidikan IN(
			SELECT MAX(jenis_pendidikan)
			FROM riwayat_pendidikan_tbl
			WHERE nip=d.nip)
			LEFT JOIN riwayat_diklat_tbl f
			on f.tahun IN(
			SELECT MAX(tahun)
			FROM riwayat_diklat_tbl
			WHERE nip=d.nip)

			WHERE 
			p.nip = b.nip AND
			p.nip = d.nip AND
			p.nip = e.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			b.kd_skpd = '".$data1."' AND
			b.kd_unitorganisasi = '".$data2."' AND
			b.kd_unitkerja = '".$data3."' AND
			b.kd_subunitkerja = '".$data4."' AND
			c.level <= '6' AND
			c.level >= '5'
			GROUP BY p.nip
			ORDER BY d.golongan DESC, d.tmt_pangkat ASC, d.mk_tahun DESC, d.mk_bulan DESC, YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
			");
		return $query->result();
	}
	
	function get_skpd_all() {
		$result = $this->db->get("skpd_tbl");
		$options = array();
		$options[""] = "Pilih OPD";
		foreach($result->result_array() as $row) {
			$options[$row["kd_skpd"]] = $row["nama"];
		}
		return $options;
	}
	
	function get_skpd_skpd($kd_skpd) {
		$result = $this->db->get("skpd_tbl WHERE kd_skpd='$kd_skpd'");
		$options = array();
		foreach($result->result_array() as $row) {
			$options[$row["kd_skpd"]] = $row["nama"];
		}
		return $options;
	}
	
	public function get_skpd_all_2($id){
		$this->db->select ('*');
		$this->db->where ('kd_skpd', $id);
		$this->db->from ('skpd_tbl');
		$result = $this->db->get();
		$options = array();
		foreach($result->result_array() as $row) {
			$options = $row["kd_skpd"];
		}
		return $options;
	}
	
	public function get_skpd($id) {
		$result = $this->db->get("skpd_tbl WHERE kd_skpd='$id'");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}

	public function get_unitorganisasi($id){
		$query  = $this->db->query("SELECT * FROM unitorganisasi_tbl
			WHERE kd_unitorganisasi='".$id."'");
		return $query->result();
	}	
	
	public function get_unitkerja($id){
		$query  = $this->db->query("SELECT * FROM unitkerja_tbl
			WHERE kd_unitkerja='".$id."'");
		return $query->result();
	}

	public function get_subunitkerja($id){
		$query  = $this->db->query("SELECT * FROM subunitkerja_tbl
			WHERE kd_subunitkerja='".$id."'");
		return $query->result();
	}

	public function ambil_unit_organisasi($kd_skpd){
		$query = $this->db->query("SELECT * FROM unitorganisasi_tbl 
			WHERE
			nama LIKE '%PUSKESMAS%' OR
			nama LIKE '%KECAMATAN%' OR 
			nama LIKE '%UPTD%' OR
			nama LIKE '%ASISTEN%' OR 
			nama LIKE '%PENGAWAS SEKOLAH%' OR 
			nama LIKE '%PENYULUH PERTANIAN%' OR 
			nama LIKE '%SMP%' OR 
			nama LIKE '%SD%' OR 
			nama LIKE '%TK%'
			ORDER BY nama ASC");

		if($query->num_rows()>0){
			foreach ($query->result_array() as $row)
			{
				$result['']= '- Pilih Unit Organisasi-';
				if ($row['kd_skpd'] == $kd_skpd) {
					$result[$row['kd_unitorganisasi']] = $row['nama'];
				}
			}
		} else {
			$result['']= '';
		}
		return $result;
	}

	public function ambil_unit_kerja($kd_skpd, $kd_unitorganisasi){


		if ($kd_skpd == 18){
			$query = $this->db->query("SELECT * FROM unitkerja_tbl 
			WHERE
			nama LIKE '%LURAH%' OR
			nama LIKE '%BAGIAN%'
			ORDER BY nama ASC");
		} else {
			$query = $this->db->query("SELECT * FROM unitkerja_tbl 
			WHERE
			nama LIKE '%LURAH%' OR
			nama LIKE '%SMP%' OR
			nama LIKE '%SD%' OR
			nama LIKE '%TK%' 
			ORDER BY nama ASC");
		}

		

		if($query->num_rows()>0){
			foreach ($query->result_array() as $row)
			{
				$result['']= '- Pilih Unit Kerja-';
				if ($row['kd_unitorganisasi'] == $kd_unitorganisasi) {
					$result[$row['kd_unitkerja']] = $row['nama'];
				}
			}
		} else {
			$result['']= '';
		}
		return $result;
	}

	public function ambil_sub_unit_kerja($kd_unitkerja){
		$this->db->where('kd_unitkerja',$kd_unitkerja);
		$this->db->order_by('nama','asc');
		$unit_kerja=$this->db->get('subunitkerja_tbl');
	
		if($unit_kerja->num_rows()>0){
			foreach ($unit_kerja->result_array() as $row)
			{
				$result['']= '- Pilih Sub Unit Kerja-';
				$result[$row['kd_subunitkerja']]= $row['nama'];
			}
		} else {
			$result['']= '-Belum Ada Sub Unit Kerja-';
		}
		
		return $result;
	}
}
