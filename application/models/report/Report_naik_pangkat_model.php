<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class report_naik_pangkat_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function fetchAll($data) {
		$query  = $this->db->query("SELECT 	a.nip, nip_lama, gelar_depan, a.nama, gelar_belakang, kelamin, golongan, 
											tmt_pangkat, d.jenis_pendidikan, d.nama_sekolah, d.tahun_ijazah , e.unit_kerja
									FROM pegawai_tbl a 
									LEFT JOIN riwayat_kepangkatan_tbl c ON a.nip=c.nip
									LEFT JOIN riwayat_pendidikan_tbl d ON a.nip=d.nip
									LEFT JOIN riwayat_jabatan_tbl e ON a.nip=e.nip
									WHERE YEAR(curdate())-YEAR(c.tmt_pangkat) > 4
									GROUP BY a.nip");
        return $query->result();
	}
	
	public function fetchAll_skpd($kd_skpd, $data) {
		$query  = $this->db->query("SELECT 	a.nip, nip_lama, gelar_depan, a.nama, gelar_belakang, kelamin, golongan, 
											tmt_pangkat, d.jenis_pendidikan, d.nama_sekolah, d.tahun_ijazah , e.unit_kerja
									FROM pegawai_tbl a 
									LEFT JOIN riwayat_kepangkatan_tbl c ON a.nip=c.nip
									LEFT JOIN riwayat_pendidikan_tbl d ON a.nip=d.nip
									LEFT JOIN riwayat_jabatan_tbl e ON a.nip=e.nip
									WHERE c.tmt_pangkat IN(
									SELECT MAX(tes.tmt_pangkat)
									FROM riwayat_kepangkatan_tbl tes
									where tes.nip=c.nip)
									AND a.kd_skpd='$kd_skpd'
									AND YEAR(curdate())-YEAR(c.tmt_pangkat) > 4
									GROUP BY a.nip");
        return $query->result();
	}
}