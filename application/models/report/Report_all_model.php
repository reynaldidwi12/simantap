<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Report_all_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}

	public function record_count() {
		return $this->db->count_all("skpd_tbl");
	}

	public function fetchAll($limit, $start) {
		$this->db->select ('*');
		$this->db->from ('skpd_tbl');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function fetchById($id){
		$this->db->select ('*');
		$this->db->from ('skpd_tbl');
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function create($data) {
		$this->kd_skpd = $data['kd_skpd'];
		$this->nama = $data['nama'];

		// insert data
		$this->db->insert('skpd_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->nama = $data['nama'];;

		// update data
		$this->db->update ('skpd_tbl', $this, array ('kd_skpd' => $data['kd_skpd']));
	}

	public function delete($id) {
		$this->db->delete ('skpd_tbl', array ('kd_skpd' => $id));
	}

	public function search_count($column, $data){
		$this->db->where($column,$data);
		return  $this->db->count_all('skpd_tbl');
	}

	public function cetak($data, $limit, $start) {

		$query  = $this->db->query("SELECT p.nip, nip_lama, gelar_depan, p.nama, gelar_belakang, p.tempat_lahir, p.tgl_lahir, kelamin, c.golongan,
											c.tmt_pangkat, d.jenis_pendidikan, d.nama_sekolah, d.tahun_ijazah , b.unit_kerja, b.nama_jabatan, b.tmt, c.mk_bulan, c.mk_tahun,
											d.program_studi, YEAR(curdate())-YEAR(p.tgl_lahir) as umur_thn ,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 as umur_bln
									FROM
									pegawai_tbl p left join riwayat_jabatan_tbl b
									on b.tmt IN(
									SELECT MAX(rj.tmt)
									FROM riwayat_jabatan_tbl rj
									WHERE rj.nip=b.nip)

									left join riwayat_kepangkatan_tbl c
									on c.golongan IN(
									SELECT MAX(rk.golongan)
									FROM riwayat_kepangkatan_tbl rk
									WHERE rk.nip=c.nip)

									left join riwayat_pendidikan_tbl d
									on d.jenis_pendidikan IN(
									SELECT MAX(rp.jenis_pendidikan)
									FROM riwayat_pendidikan_tbl rp
									WHERE rp.nip=c.nip)

									where p.nip = b.nip
									AND p.nip = c.nip
									AND p.nip = d.nip
									AND p.kd_skpd='".$data['kd_skpd']."'
									AND d.program_studi = '".$data['jurusan']."'
									GROUP BY p.nip
									ORDER BY c.golongan DESC, c.tmt_pangkat ASC,
									c.mk_tahun DESC, c.mk_bulan DESC,
									YEAR(curdate())-YEAR(p.tgl_lahir) DESC,  TIMESTAMPDIFF( MONTH, p.tgl_lahir, now() ) % 12 DESC
									");
        return $query->result();
	}

	public function getName($id){
		$this->db->select('nama');
		$this->db->from('skpd_tbl');
		$this->db->where('kd_skpd',$id);
		$query= $this->db->get();
		$ret = $query->row();
		return $ret->nama;
	}

	public function search($column,$value, $limit, $start){

		$this->db->select ('*');
		$this->db->from ('skpd_tbl');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getValue($id) {

		$query  = $this->db->query("
			SELECT
				get_all_skpd('$id', '1') AS sekertariat_all,
				get_sum_kelamin_by_skpd('$id', '', '1') AS sekertariat_pria,
				get_sum_kelamin_by_skpd('$id', '', '2') AS sekertariat_wanita,
				get_sum_eselon('$id', '', '11') AS sekertariat_e1a,
				get_sum_eselon('$id', '', '12') AS sekertariat_e1b,
				get_sum_eselon('$id', '', '21') AS sekertariat_e2a,
				get_sum_eselon('$id', '', '22') AS sekertariat_e2b,
				get_sum_eselon('$id', '', '31') AS sekertariat_e3a,
				get_sum_eselon('$id', '', '32') AS sekertariat_e3b,
				get_sum_eselon('$id', ' '41') AS sekertariat_e4a,
				get_sum_eselon('$id', '', '42') AS sekertariat_e4b,
				get_sum_eselon('$id', '', '51') AS sekertariat_e5a,
				get_sum_eselon_all('$id', '') AS sekertariat_eselon,
				get_gol_by_id('$id', '', 'IV/e') AS sekertariat_g4e,
				get_gol_by_id('$id', '', 'IV/d') AS sekertariat_g4d,
				get_gol_by_id('$id', '', 'IV/c') AS sekertariat_g4c,
				get_gol_by_id('$id', '', 'IV/b') AS sekertariat_g4b,
				get_gol_by_id('$id', '', 'IV/a') AS sekertariat_g4a,
				get_gol_level4('$id', '') AS sekertariat_g4,
				get_gol_by_id('$id', '', 'III/d') AS sekertariat_g3d,
				get_gol_by_id('$id',  '','III/c') AS sekertariat_g3c,
				get_gol_by_id('$id', '1', 'III/b') AS sekertariat_g3b,
				get_gol_by_id('$id', '1', 'III/a') AS sekertariat_g3a,
				get_gol_level3('$id', '1') AS sekertariat_g3,
				get_gol_by_id('$id', '1', 'II/d') AS sekertariat_g2d,
				get_gol_by_id('$id', '1', 'II/c') AS sekertariat_g2c,
				get_gol_by_id('$id', '1', 'II/b') AS sekertariat_g2b,
				get_gol_by_id('$id', '1', 'II/a') AS sekertariat_g2a,
				get_gol_level2('$id',  '1') AS sekertariat_g2,
				get_gol_by_id('$id', '1', 'I/d') AS sekertariat_g1d,
				get_gol_by_id('$id', '1', 'I/c') AS sekertariat_g1c,
				get_gol_by_id('$id', '1', 'I/b') AS sekertariat_g1b,
				get_gol_by_id('$id',  '1','I/a') AS sekertariat_g1a,
				get_gol_level1('$id', '1') AS sekertariat_g1,
				get_gol_all('$id',  '1') AS sekertariat_g,
				get_pendidikan_level('$id', '1', 'S3') AS sekertariat_s3,
				get_pendidikan_level('$id',  '1','S2') AS sekertariat_s2,
				get_pendidikan_level('$id',  '1','S1') AS sekertariat_s1,
				get_pendidikan_level('$id',  '1','SMA') AS sekertariat_sma,
				get_pendidikan_level('$id',  '1','SMP') AS sekertariat_smp,
				get_pendidikan_level('$id',  '1','SD') AS sekertariat_sd,
				get_pendidikan_all('$id',  '1') AS sekertariat_sekolah,
				get_diklat_id('$id', '1', 'Tingkat I') AS sekretariat_diklat1,
				get_diklat_id('$id', '1', 'Tingkat II') AS sekretariat_diklat2,
				get_diklat_id('$id', '1', 'Tingkat III') AS sekretariat_diklat3,
				get_diklat_id('$id', '1', 'Tingkat IV') AS sekretariat_diklat4,
				get_diklat('$id','1') AS sekretariat_diklat

				");
        return $query->result();

	}

	public function getValues($id, $id2) {

		$query  = $this->db->query("
			SELECT
				get_all_skpd('$id', '$id2') AS sekertariat_all,
				get_sum_kelamin_by_skpd('$id', '$id2', '1') AS sekertariat_pria,
				get_sum_kelamin_by_skpd('$id', '$id2', '2') AS sekertariat_wanita,
				get_sum_eselon('$id', '$id2', '11') AS sekertariat_e1a,
				get_sum_eselon('$id', '$id2', '12') AS sekertariat_e1b,
				get_sum_eselon('$id', '$id2', '21') AS sekertariat_e2a,
				get_sum_eselon('$id', '$id2', '22') AS sekertariat_e2b,
				get_sum_eselon('$id', '$id2', '31') AS sekertariat_e3a,
				get_sum_eselon('$id', '$id2', '32') AS sekertariat_e3b,
				get_sum_eselon('$id', '$id2', '41') AS sekertariat_e4a,
				get_sum_eselon('$id', '$id2', '42') AS sekertariat_e4b,
				get_sum_eselon('$id', '$id2', '51') AS sekertariat_e5a,
				get_sum_eselon_all('$id', '$id2') AS sekertariat_eselon,
				get_gol_by_id('$id', '$id2', 'IV/e') AS sekertariat_g4e,
				get_gol_by_id('$id', '$id2', 'IV/d') AS sekertariat_g4d,
				get_gol_by_id('$id', '$id2', 'IV/c') AS sekertariat_g4c,
				get_gol_by_id('$id', '$id2', 'IV/b') AS sekertariat_g4b,
				get_gol_by_id('$id', '$id2', 'IV/a') AS sekertariat_g4a,
				get_gol_level4('$id', '$id2') AS sekertariat_g4,
				get_gol_by_id('$id', '$id2', 'III/d') AS sekertariat_g3d,
				get_gol_by_id('$id',  '$id2','III/c') AS sekertariat_g3c,
				get_gol_by_id('$id', '$id2', 'III/b') AS sekertariat_g3b,
				get_gol_by_id('$id', '$id2', 'III/a') AS sekertariat_g3a,
				get_gol_level3('$id', '$id2') AS sekertariat_g3,
				get_gol_by_id('$id', '$id2', 'II/d') AS sekertariat_g2d,
				get_gol_by_id('$id', '$id2', 'II/c') AS sekertariat_g2c,
				get_gol_by_id('$id', '$id2', 'II/b') AS sekertariat_g2b,
				get_gol_by_id('$id', '$id2', 'II/a') AS sekertariat_g2a,
				get_gol_level2('$id',  '$id2') AS sekertariat_g2,
				get_gol_by_id('$id', '$id2', 'I/d') AS sekertariat_g1d,
				get_gol_by_id('$id', '$id2', 'I/c') AS sekertariat_g1c,
				get_gol_by_id('$id', '$id2', 'I/b') AS sekertariat_g1b,
				get_gol_by_id('$id',  '$id2','I/a') AS sekertariat_g1a,
				get_gol_level1('$id', '$id2') AS sekertariat_g1,
				get_gol_all('$id',  '$id2') AS sekertariat_g,
				get_pendidikan_level('$id', '$id2', '9') AS sekertariat_s3,
				get_pendidikan_level('$id',  '$id2','8') AS sekertariat_s2,
				get_pendidikan_level('$id',  '$id2','7') AS sekertariat_s1,
				get_pendidikan_level('$id',  '$id2','3') AS sekertariat_sma,
				get_pendidikan_level('$id',  '$id2','2') AS sekertariat_smp,
				get_pendidikan_level('$id',  '$id2','1') AS sekertariat_sd,
				get_pendidikan_all('$id',  '$id2') AS sekertariat_sekolah,
				get_diklat_id('$id', '$id2', 'Diklat PIM Tingkat. I') AS sekretariat_diklat1,
				get_diklat_id('$id', '$id2', 'Diklat PIM Tingkat. II') AS sekretariat_diklat2,
				get_diklat_id('$id', '$id2', 'Diklat PIM Tingkat. III') AS sekretariat_diklat3,
				get_diklat_id('$id', '$id2', 'Diklat PIM Tingkat. IV') AS sekretariat_diklat4,
				get_diklat('$id','$id2') AS sekretariat_diklat

				");
        return $query->result();

	}

	public function getValues_jen($id, $id2) {

		$query  = $this->db->query("
			SELECT
				get_all_skpd('$id', '$id2') AS sekertariat_all,
				get_sum_kelamin_by_skpd('$id', '$id2', '1') AS sekertariat_pria,
				get_sum_kelamin_by_skpd('$id', '$id2', '2') AS sekertariat_wanita,
				get_sum_eselon_jen('$id', '$id2', '11', '1') AS sekertariat_e1a1,
				get_sum_eselon_jen('$id', '$id2', '11', '2') AS sekertariat_e1a2,
				get_sum_eselon_jen('$id', '$id2', '12', '1') AS sekertariat_e1b1,
				get_sum_eselon_jen('$id', '$id2', '12', '2') AS sekertariat_e1b2,
				get_sum_eselon_jen('$id', '$id2', '21', '1') AS sekertariat_e2a1,
				get_sum_eselon_jen('$id', '$id2', '21', '2') AS sekertariat_e2a2,
				get_sum_eselon_jen('$id', '$id2', '22', '1') AS sekertariat_e2b1,
				get_sum_eselon_jen('$id', '$id2', '22', '2') AS sekertariat_e2b2,
				get_sum_eselon_jen('$id', '$id2', '31', '1') AS sekertariat_e3a1,
				get_sum_eselon_jen('$id', '$id2', '31', '2') AS sekertariat_e3a2,
				get_sum_eselon_jen('$id', '$id2', '32', '1') AS sekertariat_e3b1,
				get_sum_eselon_jen('$id', '$id2', '32', '2') AS sekertariat_e3b2,
				get_sum_eselon_jen('$id', '$id2', '41', '1') AS sekertariat_e4a1,
				get_sum_eselon_jen('$id', '$id2', '41', '2') AS sekertariat_e4a2,
				get_sum_eselon_jen('$id', '$id2', '42', '1') AS sekertariat_e4b1,
				get_sum_eselon_jen('$id', '$id2', '42', '2') AS sekertariat_e4b2,
				get_sum_eselon_jen('$id', '$id2', '51', '1') AS sekertariat_e5a1,
				get_sum_eselon_jen('$id', '$id2', '51', '2') AS sekertariat_e5a2,
				get_sum_eselon_all_jen('$id', '$id2', '1') AS sekertariat_eselon1,
				get_sum_eselon_all_jen('$id', '$id2', '2') AS sekertariat_eselon2,
				get_gol_by_id_jen('$id', '$id2', 'IV/e', '1') AS sekertariat_g4e1,
				get_gol_by_id_jen('$id', '$id2', 'IV/e', '2') AS sekertariat_g4e2,
				get_gol_by_id_jen('$id', '$id2', 'IV/d', '1') AS sekertariat_g4d1,
				get_gol_by_id_jen('$id', '$id2', 'IV/d', '2') AS sekertariat_g4d2,
				get_gol_by_id_jen('$id', '$id2', 'IV/c', '1') AS sekertariat_g4c1,
				get_gol_by_id_jen('$id', '$id2', 'IV/c', '2') AS sekertariat_g4c2,
				get_gol_by_id_jen('$id', '$id2', 'IV/b', '1') AS sekertariat_g4b1,
				get_gol_by_id_jen('$id', '$id2', 'IV/b', '2') AS sekertariat_g4b2,
				get_gol_by_id_jen('$id', '$id2', 'IV/a', '1') AS sekertariat_g4a1,
				get_gol_by_id_jen('$id', '$id2', 'IV/a', '2') AS sekertariat_g4a2,
				get_gol_level4_jen('$id', '$id2', '1') AS sekertariat_g41,
				get_gol_level4_jen('$id', '$id2', '2') AS sekertariat_g42,
				get_gol_by_id_jen('$id', '$id2', 'III/d', '1') AS sekertariat_g3d1,
				get_gol_by_id_jen('$id', '$id2', 'III/d', '2') AS sekertariat_g3d2,
				get_gol_by_id_jen('$id',  '$id2','III/c', '1') AS sekertariat_g3c1,
				get_gol_by_id_jen('$id', '$id2', 'III/c', '2') AS sekertariat_g3c2,
				get_gol_by_id_jen('$id', '$id2', 'III/b', '1') AS sekertariat_g3b1,
				get_gol_by_id_jen('$id', '$id2', 'III/b', '2') AS sekertariat_g3b2,
				get_gol_by_id_jen('$id', '$id2', 'III/a', '1') AS sekertariat_g3a1,
				get_gol_by_id_jen('$id', '$id2', 'III/a', '1') AS sekertariat_g3a2,
				get_gol_level3_jen('$id', '$id2', '1') AS sekertariat_g31,
				get_gol_level3_jen('$id', '$id2', '2') AS sekertariat_g32,
				get_gol_by_id_jen('$id', '$id2', 'II/d', '1') AS sekertariat_g2d1,
				get_gol_by_id_jen('$id', '$id2', 'II/d', '2') AS sekertariat_g2d2,
				get_gol_by_id_jen('$id', '$id2', 'II/c', '1') AS sekertariat_g2c1,
				get_gol_by_id_jen('$id', '$id2', 'II/c', '2') AS sekertariat_g2c2,
				get_gol_by_id_jen('$id', '$id2', 'II/b', '1') AS sekertariat_g2b1,
				get_gol_by_id_jen('$id', '$id2', 'II/b', '2') AS sekertariat_g2b2,
				get_gol_by_id_jen('$id', '$id2', 'II/a', '1') AS sekertariat_g2a1,
				get_gol_by_id_jen('$id', '$id2', 'II/a', '2') AS sekertariat_g2a2,
				get_gol_level2_jen('$id',  '$id2', '1') AS sekertariat_g21,
				get_gol_level2_jen('$id',  '$id2', '2') AS sekertariat_g22,
				get_gol_by_id_jen('$id', '$id2', 'I/d', '1') AS sekertariat_g1d1,
				get_gol_by_id_jen('$id', '$id2', 'I/d', '2') AS sekertariat_g1d2,
				get_gol_by_id_jen('$id', '$id2', 'I/c', '1') AS sekertariat_g1c1,
				get_gol_by_id_jen('$id', '$id2', 'I/c', '2') AS sekertariat_g1c2,
				get_gol_by_id_jen('$id', '$id2', 'I/b', '1') AS sekertariat_g1b1,
				get_gol_by_id_jen('$id', '$id2', 'I/b', '2') AS sekertariat_g1b2,
				get_gol_by_id_jen('$id',  '$id2','I/a', '1') AS sekertariat_g1a1,
				get_gol_by_id_jen('$id',  '$id2','I/a', '2') AS sekertariat_g1a2,
				get_gol_level1_jen('$id', '$id2', '1') AS sekertariat_g11,
				get_gol_level1_jen('$id', '$id2', '2') AS sekertariat_g12,
				get_gol_all_jen('$id',  '$id2', '1') AS sekertariat_g1,
				get_gol_all_jen('$id',  '$id2', '2') AS sekertariat_g2,
				get_pendidikan_level_jen('$id', '$id2', '9', '1') AS sekertariat_s31,
				get_pendidikan_level_jen('$id', '$id2', '9', '2') AS sekertariat_s32,
				get_pendidikan_level_jen('$id',  '$id2','8', '1') AS sekertariat_s21,
				get_pendidikan_level_jen('$id',  '$id2','8', '2') AS sekertariat_s22,
				get_pendidikan_level_jen('$id',  '$id2','7', '1') AS sekertariat_s11,
				get_pendidikan_level_jen('$id',  '$id2','7', '2') AS sekertariat_s12,
				get_pendidikan_level_jen('$id',  '$id2','3', '1') AS sekertariat_sma1,
				get_pendidikan_level_jen('$id',  '$id2','3', '2') AS sekertariat_sma2,
				get_pendidikan_level_jen('$id',  '$id2','2', '1') AS sekertariat_smp1,
				get_pendidikan_level_jen('$id',  '$id2','2', '2') AS sekertariat_smp2,
				get_pendidikan_level_jen('$id',  '$id2','1', '1') AS sekertariat_sd1,
				get_pendidikan_level_jen('$id',  '$id2','1', '2') AS sekertariat_sd2,
				get_pendidikan_all_jen('$id',  '$id2', '1') AS sekertariat_sekolah1,
				get_pendidikan_all_jen('$id',  '$id2', '2') AS sekertariat_sekolah2,
				get_diklat_id_jen('$id', '$id2', 'Diklat PIM Tingkat. I', '1') AS sekretariat_diklat11,
				get_diklat_id_jen('$id', '$id2', 'Diklat PIM Tingkat. I', '2') AS sekretariat_diklat12,
				get_diklat_id_jen('$id', '$id2', 'Diklat PIM Tingkat. II', '1') AS sekretariat_diklat21,
				get_diklat_id_jen('$id', '$id2', 'Diklat PIM Tingkat. II', '2') AS sekretariat_diklat22,
				get_diklat_id_jen('$id', '$id2', 'Diklat PIM Tingkat. III', '1') AS sekretariat_diklat31,
				get_diklat_id_jen('$id', '$id2', 'Diklat PIM Tingkat. III', '2') AS sekretariat_diklat32,
				get_diklat_id_jen('$id', '$id2', 'Diklat PIM Tingkat. IV', '1') AS sekretariat_diklat41,
				get_diklat_id_jen('$id', '$id2', 'Diklat PIM Tingkat. IV', '2') AS sekretariat_diklat42,
				get_diklat_jen('$id','$id2', '1') AS sekretariat_diklat1,
				get_diklat_jen('$id','$id2', '2') AS sekretariat_diklat2
				");
        return $query->result();

	}

	public function getValuesU($id) {

		$query  = $this->db->query("
			SELECT
				get_all_skpd_u('$id') AS sekertariat_all,
				get_sum_kelamin_by_skpd_u('$id', '1') AS sekertariat_pria,
				get_sum_kelamin_by_skpd_u('$id', '2') AS sekertariat_wanita,
				get_sum_eselon_u('$id', '11') AS sekertariat_e1a,
				get_sum_eselon_u('$id', '12') AS sekertariat_e1b,
				get_sum_eselon_u('$id', '21') AS sekertariat_e2a,
				get_sum_eselon_u('$id', '22') AS sekertariat_e2b,
				get_sum_eselon_u('$id', '31') AS sekertariat_e3a,
				get_sum_eselon_u('$id', '32') AS sekertariat_e3b,
				get_sum_eselon_u('$id', '41') AS sekertariat_e4a,
				get_sum_eselon_u('$id', '42') AS sekertariat_e4b,
				get_sum_eselon_u('$id', '51') AS sekertariat_e5a,
				get_sum_eselon_all_u('$id') AS sekertariat_eselon,
				get_gol_by_id_u('$id', 'IV/e') AS sekertariat_g4e,
				get_gol_by_id_u('$id', 'IV/d') AS sekertariat_g4d,
				get_gol_by_id_u('$id', 'IV/c') AS sekertariat_g4c,
				get_gol_by_id_u('$id', 'IV/b') AS sekertariat_g4b,
				get_gol_by_id_u('$id', 'IV/a') AS sekertariat_g4a,
				get_gol_level4_u('$id') AS sekertariat_g4,
				get_gol_by_id_u('$id', 'III/d') AS sekertariat_g3d,
				get_gol_by_id_u('$id', 'III/c') AS sekertariat_g3c,
				get_gol_by_id_u('$id', 'III/b') AS sekertariat_g3b,
				get_gol_by_id_u('$id', 'III/a') AS sekertariat_g3a,
				get_gol_level3_u('$id') AS sekertariat_g3,
				get_gol_by_id_u('$id', 'II/d') AS sekertariat_g2d,
				get_gol_by_id_u('$id', 'II/c') AS sekertariat_g2c,
				get_gol_by_id_u('$id', 'II/b') AS sekertariat_g2b,
				get_gol_by_id_u('$id', 'II/a') AS sekertariat_g2a,
				get_gol_level2_u('$id') AS sekertariat_g2,
				get_gol_by_id_u('$id', 'I/d') AS sekertariat_g1d,
				get_gol_by_id_u('$id', 'I/c') AS sekertariat_g1c,
				get_gol_by_id_u('$id', 'I/b') AS sekertariat_g1b,
				get_gol_by_id_u('$id',  'I/a') AS sekertariat_g1a,
				get_gol_level1_u('$id') AS sekertariat_g1,
				get_gol_all_u('$id') AS sekertariat_g,
				get_pendidikan_level_u('$id', '9') AS sekertariat_s3,
				get_pendidikan_level_u('$id', '8') AS sekertariat_s2,
				get_pendidikan_level_u('$id', '7') AS sekertariat_s1,
				get_pendidikan_level_u('$id', '3') AS sekertariat_sma,
				get_pendidikan_level_u('$id', '2') AS sekertariat_smp,
				get_pendidikan_level_u('$id', '1') AS sekertariat_sd,
				get_pendidikan_all_u('$id') AS sekertariat_sekolah,
				get_diklat_id_u('$id', 'Diklat PIM Tingkat. I') AS sekretariat_diklat1,
				get_diklat_id_u('$id', 'Diklat PIM Tingkat. II') AS sekretariat_diklat2,
				get_diklat_id_u('$id', 'Diklat PIM Tingkat. III') AS sekretariat_diklat3,
				get_diklat_id_u('$id', 'Diklat PIM Tingkat. IV') AS sekretariat_diklat4,
				get_diklat_u('$id') AS sekretariat_diklat

				");
        return $query->result();

	}

	public function getValuesU_jen($id) {

		$query  = $this->db->query("
			SELECT
			get_all_skpd_u('$id') AS sekertariat_all,
			get_sum_kelamin_by_skpd_u('$id', '1') AS sekertariat_pria,
			get_sum_kelamin_by_skpd_u('$id', '2') AS sekertariat_wanita,
				get_sum_eselon_u_jen('$id',  '11', '1') AS sekertariat_e1a1,
				get_sum_eselon_u_jen('$id',  '11', '2') AS sekertariat_e1a2,
				get_sum_eselon_u_jen('$id',  '12', '1') AS sekertariat_e1b1,
				get_sum_eselon_u_jen('$id',  '12', '2') AS sekertariat_e1b2,
				get_sum_eselon_u_jen('$id',  '21', '1') AS sekertariat_e2a1,
				get_sum_eselon_u_jen('$id',  '21', '2') AS sekertariat_e2a2,
				get_sum_eselon_u_jen('$id',  '22', '1') AS sekertariat_e2b1,
				get_sum_eselon_u_jen('$id',  '22', '2') AS sekertariat_e2b2,
				get_sum_eselon_u_jen('$id',  '31', '1') AS sekertariat_e3a1,
				get_sum_eselon_u_jen('$id',  '31', '2') AS sekertariat_e3a2,
				get_sum_eselon_u_jen('$id',  '32', '1') AS sekertariat_e3b1,
				get_sum_eselon_u_jen('$id',  '32', '2') AS sekertariat_e3b2,
				get_sum_eselon_u_jen('$id',  '41', '1') AS sekertariat_e4a1,
				get_sum_eselon_u_jen('$id',  '41', '2') AS sekertariat_e4a2,
				get_sum_eselon_u_jen('$id',  '42', '1') AS sekertariat_e4b1,
				get_sum_eselon_u_jen('$id',  '42', '2') AS sekertariat_e4b2,
				get_sum_eselon_u_jen('$id',  '51', '1') AS sekertariat_e5a1,
				get_sum_eselon_u_jen('$id',  '51', '2') AS sekertariat_e5a2,
				get_sum_eselon_all_u_jen('$id',  '1') AS sekertariat_eselon1,
				get_sum_eselon_all_u_jen('$id',  '2') AS sekertariat_eselon2,
				get_gol_by_id_u_jen('$id',  'IV/e', '1') AS sekertariat_g4e1,
				get_gol_by_id_u_jen('$id',  'IV/e', '2') AS sekertariat_g4e2,
				get_gol_by_id_u_jen('$id',  'IV/d', '1') AS sekertariat_g4d1,
				get_gol_by_id_u_jen('$id',  'IV/d', '2') AS sekertariat_g4d2,
				get_gol_by_id_u_jen('$id',  'IV/c', '1') AS sekertariat_g4c1,
				get_gol_by_id_u_jen('$id',  'IV/c', '2') AS sekertariat_g4c2,
				get_gol_by_id_u_jen('$id',  'IV/b', '1') AS sekertariat_g4b1,
				get_gol_by_id_u_jen('$id',  'IV/b', '2') AS sekertariat_g4b2,
				get_gol_by_id_u_jen('$id',  'IV/a', '1') AS sekertariat_g4a1,
				get_gol_by_id_u_jen('$id',  'IV/a', '2') AS sekertariat_g4a2,
				get_gol_level4_u_jen('$id',  '1') AS sekertariat_g41,
				get_gol_level4_u_jen('$id',  '2') AS sekertariat_g42,
				get_gol_by_id_u_jen('$id',  'III/d', '1') AS sekertariat_g3d1,
				get_gol_by_id_u_jen('$id',  'III/d', '2') AS sekertariat_g3d2,
				get_gol_by_id_u_jen('$id',  'III/c', '1') AS sekertariat_g3c1,
				get_gol_by_id_u_jen('$id',  'III/c', '2') AS sekertariat_g3c2,
				get_gol_by_id_u_jen('$id',  'III/b', '1') AS sekertariat_g3b1,
				get_gol_by_id_u_jen('$id',  'III/b', '2') AS sekertariat_g3b2,
				get_gol_by_id_u_jen('$id',  'III/a', '1') AS sekertariat_g3a1,
				get_gol_by_id_u_jen('$id',  'III/a', '1') AS sekertariat_g3a2,
				get_gol_level3_u_jen('$id',  '1') AS sekertariat_g31,
				get_gol_level3_u_jen('$id',  '2') AS sekertariat_g32,
				get_gol_by_id_u_jen('$id',  'II/d', '1') AS sekertariat_g2d1,
				get_gol_by_id_u_jen('$id',  'II/d', '2') AS sekertariat_g2d2,
				get_gol_by_id_u_jen('$id',  'II/c', '1') AS sekertariat_g2c1,
				get_gol_by_id_u_jen('$id',  'II/c', '2') AS sekertariat_g2c2,
				get_gol_by_id_u_jen('$id',  'II/b', '1') AS sekertariat_g2b1,
				get_gol_by_id_u_jen('$id',  'II/b', '2') AS sekertariat_g2b2,
				get_gol_by_id_u_jen('$id',  'II/a', '1') AS sekertariat_g2a1,
				get_gol_by_id_u_jen('$id',  'II/a', '2') AS sekertariat_g2a2,
				get_gol_level2_u_jen('$id',   '1') AS sekertariat_g21,
				get_gol_level2_u_jen('$id',   '2') AS sekertariat_g22,
				get_gol_by_id_u_jen('$id',  'I/d', '1') AS sekertariat_g1d1,
				get_gol_by_id_u_jen('$id',  'I/d', '2') AS sekertariat_g1d2,
				get_gol_by_id_u_jen('$id',  'I/c', '1') AS sekertariat_g1c1,
				get_gol_by_id_u_jen('$id',  'I/c', '2') AS sekertariat_g1c2,
				get_gol_by_id_u_jen('$id',  'I/b', '1') AS sekertariat_g1b1,
				get_gol_by_id_u_jen('$id',  'I/b', '2') AS sekertariat_g1b2,
				get_gol_by_id_u_jen('$id',  'I/a', '1') AS sekertariat_g1a1,
				get_gol_by_id_u_jen('$id',  'I/a', '2') AS sekertariat_g1a2,
				get_gol_level1_u_jen('$id',  '1') AS sekertariat_g11,
				get_gol_level1_u_jen('$id',  '2') AS sekertariat_g12,
				get_gol_all_u_jen('$id',   '1') AS sekertariat_g1,
				get_gol_all_u_jen('$id',   '2') AS sekertariat_g2,
				get_pendidikan_level_u_jen('$id',  '9', '1') AS sekertariat_s31,
				get_pendidikan_level_u_jen('$id',  '9', '2') AS sekertariat_s32,
				get_pendidikan_level_u_jen('$id',  '8', '1') AS sekertariat_s21,
				get_pendidikan_level_u_jen('$id',  '8', '2') AS sekertariat_s22,
				get_pendidikan_level_u_jen('$id',  '7', '1') AS sekertariat_s11,
				get_pendidikan_level_u_jen('$id',  '7', '2') AS sekertariat_s12,
				get_pendidikan_level_u_jen('$id',  '3', '1') AS sekertariat_sma1,
				get_pendidikan_level_u_jen('$id',  '3', '2') AS sekertariat_sma2,
				get_pendidikan_level_u_jen('$id',  '2', '1') AS sekertariat_smp1,
				get_pendidikan_level_u_jen('$id',  '2', '2') AS sekertariat_smp2,
				get_pendidikan_level_u_jen('$id',  '1', '1') AS sekertariat_sd1,
				get_pendidikan_level_u_jen('$id',  '1', '2') AS sekertariat_sd2,
				get_pendidikan_all_u_jen('$id',   '1') AS sekertariat_sekolah1,
				get_pendidikan_all_u_jen('$id',   '2') AS sekertariat_sekolah2,
				get_diklat_id_u_jen('$id',  'Diklat PIM Tingkat. I', '1') AS sekretariat_diklat11,
				get_diklat_id_u_jen('$id',  'Diklat PIM Tingkat. I', '2') AS sekretariat_diklat12,
				get_diklat_id_u_jen('$id',  'Diklat PIM Tingkat. II', '1') AS sekretariat_diklat21,
				get_diklat_id_u_jen('$id',  'Diklat PIM Tingkat. II', '2') AS sekretariat_diklat22,
				get_diklat_id_u_jen('$id',  'Diklat PIM Tingkat. III', '1') AS sekretariat_diklat31,
				get_diklat_id_u_jen('$id',  'Diklat PIM Tingkat. III', '2') AS sekretariat_diklat32,
				get_diklat_id_u_jen('$id',  'Diklat PIM Tingkat. IV', '1') AS sekretariat_diklat41,
				get_diklat_id_u_jen('$id',  'Diklat PIM Tingkat. IV', '2') AS sekretariat_diklat42,
				get_diklat_u_jen('$id', '1') AS sekretariat_diklat1,
				get_diklat_u_jen('$id', '2') AS sekretariat_diklat2
				");
        return $query->result();

	}


}
