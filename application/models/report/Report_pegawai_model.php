<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Report_pegawai_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	
	public function cetak(){
    	$query  = $this->db->query("select a.nip,a.nama, a.`status_pegawai`,b.nama as nama_unitkerja, c.nama as nama_skpd
									from pegawai_tbl a
									left join unitkerja_tbl b on a.kd_unitkerja = b.kd_unitkerja
									left join skpd_tbl c on a.kd_skpd = c.kd_skpd 
									where a.status_pegawai = 'Aktif'
									or  a.status_pegawai = ''
									");
    	return $query->result();
    }

    public function cetak_per_skpd($kd_skpd){
    	$query = $this->db->query("
			SELECT p.nip, p.nama, p.status_pegawai, d.nama as nama_unitkerja, e.nama as nama_skpd
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			LEFT JOIN unitkerja_tbl d on d.kd_unitkerja = c.kd_unitkerja
			LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
			
			WHERE 
			p.nip = b.nip AND
			(p.status_pegawai = 'Aktif' OR 
			p.status_pegawai = '') AND
			c.kd_skpd = '".$kd_skpd."'

			GROUP BY p.nip , c.kd_skpd");

    	return $query->result();
    }


	
	
}