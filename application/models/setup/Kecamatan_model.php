<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class kecamatan_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("kecamatan_tbl");
	}
	
	public function fetchAll($limit, $start) {
		$this->db->select ('*');
		$this->db->from ('kecamatan_tbl');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('*');
		$this->db->from ('kecamatan_tbl');
		$this->db->where('kec_id', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function create($data) {
		$this->kec_id = $data['kec_id'];
		$this->kec_nama = $data['kec_nama'];
		// insert data
		$this->db->insert('kecamatan_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->kec_nama = $data['kec_nama'];
		
		// update data
		$this->db->update ('kecamatan_tbl', $this, array ('kec_id' => $data['kec_id']));
	}
	
	public function delete($id) {
		$this->db->delete ('kecamatan_tbl', array ('kec_id' => $id));
	}
	
	public function search_count($column, $data){
		$this->db->where($column,$data);
		return  $this->db->count_all("kecamatan_tbl where $column like '%$data%'" );
	}
	public function generatekd_kec(){
		return $this->db->query("SELECT kec_id FROM kecamatan_tbl order by kec_id desc limit 1")->row()->kec_id+1;	
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('*');
		$this->db->from ('kecamatan_tbl');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
}