<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class unit_kerja_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		$this->db->where('kd_unitorganisasi !=','');
		return $this->db->count_all_results("unitkerja_tbl");
	}
	
	public function record_count_all() {
		return $this->db->count_all("unitkerja_tbl");
	}

	public function fetchAll($limit, $start) {
		$this->db->select ('b.kd_unitkerja, a.nama as nama_skpd, b.nama as nama_unit_kerja, c.nama as nama_unit_organisasi');
		$this->db->from ('skpd_tbl a, unitkerja_tbl b, unitorganisasi_tbl c');
		$this->db->where ('a.kd_skpd=b.kd_skpd');
		$this->db->where ('c.kd_unitorganisasi=b.kd_unitorganisasi');
		$this->db->order_by ('b.kd_unitkerja DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('b.kd_unitkerja, a.kd_skpd as kd_skpd, b.nama as nama_unitkerja, c.kd_unitorganisasi as kd_unitorganisasi, c.nama as nama_unit_organisasi');
		$this->db->from ('skpd_tbl a, unitkerja_tbl b, unitorganisasi_tbl c');
		$this->db->where ('a.kd_skpd=b.kd_skpd');
		$this->db->where ('c.kd_unitorganisasi=b.kd_unitorganisasi');
		$this->db->where('b.kd_unitkerja', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function create($data) {
		$this->kd_unitkerja = $data['kd_unitkerja'];
		$this->nama = $data['nama_unitkerja'];
		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		
		// insert data
		$this->db->insert('unitkerja_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->nama = $data['nama_unitkerja'];
		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		
		// update data
		$this->db->update ('unitkerja_tbl', $this, array ('kd_unitkerja' => $data['kd_unitkerja']));
	}
	
	public function delete($id) {
		$this->db->delete ('unitkerja_tbl', array ('kd_unitkerja' => $id));
	}
	
	public function search_count($column, $data){
		return  $this->db->count_all("skpd_tbl a, unitkerja_tbl b, unitorganisasi_tbl c  WHERE a.kd_skpd=b.kd_skpd AND c.kd_unitorganisasi=b.kd_unitorganisasi AND $column like '%$data%'" );
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('b.kd_unitkerja, a.nama as nama_skpd, b.nama as nama_unit_kerja, c.nama as nama_unit_organisasi');
		$this->db->from ('skpd_tbl a, unitkerja_tbl b, unitorganisasi_tbl c');
		$this->db->where ('a.kd_skpd=b.kd_skpd');
		$this->db->where ('c.kd_unitorganisasi=b.kd_unitorganisasi');
		$this->db->like($column,$value);
		$this->db->order_by ('b.kd_unitkerja DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function generatekd_unitkerja(){
		return $this->db->query("SELECT kd_unitkerja FROM unitkerja_tbl order by kd_unitkerja desc limit 1")->row()->kd_unitkerja+1;	
	}

	public function ambil_unit_kerja($kd_unitorganisasi){
		$this->db->where('kd_unitorganisasi',$kd_unitorganisasi);
		$this->db->order_by('nama','asc');
		$unit_kerja=$this->db->get('unitkerja_tbl');
	
		if($unit_kerja->num_rows()>0){
			foreach ($unit_kerja->result_array() as $row)
			{
				$result['']= '- Pilih Unit Kerja-';
				$result[$row['kd_unitkerja']]= $row['nama'];
			}
		} else {
			$result['']= '-Belum Ada Unit Kerja-';
		}
		
		return $result;
	}


	public function get_auto_nama($data){
		$this->db->select('kd_unitkerja, nama');
		
		$this->db->where('kd_skpd',$data['kd_skpd']);
		$this->db->where('kd_unitorganisasi',$data['kd_unitorganisasi']);
		
		$this->db->like('nama', $data['keyword']);
		
		$this->db->order_by('nama','asc');
		$jabatan=$this->db->get('unitkerja_tbl');
		

		return $jabatan->result_array();
	}
	
	
	
}