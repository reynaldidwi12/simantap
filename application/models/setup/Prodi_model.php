<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class prodi_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("prodi_tbl");
	}
	
	public function fetchAll($limit, $start) {
		$this->db->select ('*');
		$this->db->from ('prodi_tbl');
		$this->db->order_by ('kd_prodi DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('*');
		$this->db->from ('prodi_tbl');
		$this->db->where('kd_prodi', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function create($data) {
		$this->kd_prodi = $data['kd_prodi'];
		$this->nama_prodi = $data['nama_prodi'];
		// insert data
		$this->db->insert('prodi_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->nama_prodi = $data['nama_prodi'];
		
		// update data
		$this->db->update ('prodi_tbl', $this, array ('kd_prodi' => $data['kd_prodi']));
	}
	
	public function delete($id) {
		$this->db->delete ('prodi_tbl', array ('kd_prodi' => $id));
	}
	
	public function search_count($column, $data){
		$this->db->where($column,$data);
		return  $this->db->count_all("prodi_tbl where $column like '%$data%'" );
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('*');
		$this->db->from ('prodi_tbl');
		$this->db->order_by ('kd_prodi DESC');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function generateKd_prodi(){
		return $this->db->query("SELECT kd_prodi FROM prodi_tbl order by kd_prodi desc limit 1")->row()->kd_prodi+1;
		
	}
	
	function get_prodi() {
		$result = $this->db->get("prodi_tbl");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["nama_prodi"]] = $row["nama_prodi"];
			}
		return $options;
	}
	
}