<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_jabatan_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count($nip) {
		return $this->db->count_all("riwayat_jabatan_tbl where nip = '$nip'");
	}
	
	public function fetchAll($limit, $start,$nip) {
		$this->db->select ('rj.nip, p.nama, rj.seq_no, rj.kd_skpd, rj.esselon, rj.jenis_jabatan, jb.nama_jabatan, 
			rj.unit_kerja, rj.kd_jabatan,rj.no_sk, rj.tmt, rj.tmt_jabatan, rj.valid_jabatan, pr.nama as nama_skpd');
		$this->db->from ('riwayat_jabatan_tbl rj');
		$this->db->join ('pegawai_tbl p','rj.nip = p.nip');
		$this->db->join ('skpd_tbl pr','rj.kd_skpd = pr.kd_skpd','left');
		
		$this->db->join ('jabatan_tbl jb','rj.kd_jabatan = jb.kd_jabatan','left');

		$this->db->where('rj.nip',$nip);
		$this->db->order_by('rj.tmt DESC');
		// $this->db->order_by('rj.seq_no DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($nip,$seq_no){
		$this->db->select ('rj.nip, p.nama, rj.seq_no, rj.kd_skpd, rj.jenis_jabatan, jb.nama_jabatan as nama_jabatan, rj.kd_unitorganisasi, rj.kd_unitkerja, rj.kd_subunitkerja,rj.unit_kerja, rj.kd_jabatan, rj.esselon, rj.no_sk, rj.tmt, rj.tmt_jabatan, rj.valid_jabatan, pr.nama as nama_skpd');
		$this->db->from ('riwayat_jabatan_tbl rj');
		$this->db->join ('pegawai_tbl p','rj.nip = p.nip');
		$this->db->join ('skpd_tbl pr','rj.kd_skpd = pr.kd_skpd','left');

		$this->db->join ('jabatan_tbl jb','rj.kd_jabatan = jb.kd_jabatan','left');

		$this->db->where('rj.nip',$nip);
		$this->db->where('rj.seq_no',$seq_no);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function create($data) {
		$this->nip = $data['nip'];
		$this->seq_no = $data['seq_no'];
		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		$this->kd_unitkerja = $data['kd_unitkerja'];
		$this->kd_subunitkerja = $data['kd_subunitkerja'];
		$this->jenis_jabatan = $data['jenis_jabatan'];
		$this->kd_jabatan = $data['kd_jabatan'];
		$this->esselon = $data['esselon'];
		$this->no_sk = $data['no_sk'];

		if($data['tmt']){
			$data['tmt2'] = date("Y-m-d",strtotime($data['tmt']));	
		}else{}
		$this->tmt = $data['tmt2'];

		if($data['tmt_jabatan']){
			$data['tmt_jabatan2'] = date("Y-m-d",strtotime($data['tmt_jabatan']));	
		}else{}
		$this->tmt_jabatan = $data['tmt_jabatan2'];

		$this->valid_jabatan = $data['valid_jabatan'];
		$this->qversion = 0;
		
		// print_r($this);exit;
		// insert data
		$this->db->insert('riwayat_jabatan_tbl', $this);
	}

	public function update($data) {
		// get data
		
		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		$this->kd_unitkerja = $data['kd_unitkerja'];
		$this->kd_subunitkerja = $data['kd_subunitkerja'];
		$this->jenis_jabatan = $data['jenis_jabatan'];
		$this->kd_jabatan = $data['kd_jabatan'];
		$this->esselon = $data['esselon'];
		$this->no_sk = $data['no_sk'];
		
		if($data['tmt']){
			$data['tmt2'] = date("Y-m-d",strtotime($data['tmt']));	
		}else{}
		$this->tmt = $data['tmt2'];

		if($data['tmt_jabatan']){
			$data['tmt_jabatan2'] = date("Y-m-d",strtotime($data['tmt_jabatan']));	
		}else{}
		$this->tmt_jabatan = $data['tmt_jabatan2'];

		$this->valid_jabatan = $data['valid_jabatan'];
		
		// print_r($this);exit;
		// update data
		$this->db->update ('riwayat_jabatan_tbl', $this, array ('nip' => $data['nip'],'seq_no' => $data['seq_no']));
	}
	
	public function delete($id, $seq_no) {
		$this->db->delete ('riwayat_jabatan_tbl', array ('nip' => $id,'seq_no'=> $seq_no));
	}
	
	public function search_count($column, $data,$nip){
		$this->db->where('nip',$nip);
		$this->db->where($column,$data);
		return  $this->db->count_all('riwayat_jabatan_tbl');
	}
	
	public function generateSeqNo($nip){
		return $this->db->query("SELECT ifnull(max(seq_no),0) seq_no FROM riwayat_jabatan_tbl WHERE nip='".$nip."'")->row()->seq_no+1;
	}
	
	public function search($column,$value, $nip, $limit, $start){
		
		$this->db->select ('rj.nip, p.nama, rj.seq_no, rj.kd_skpd, rj.jenis_jabatan, rj.nama_jabatan, rj.unit_kerja, 
		rj.esselon, rj.no_sk, rj.tmt, rj.tmt_jabatan, rj.valid_jabatan');
		$this->db->from ('riwayat_jabatan_tbl rj');
		$this->db->join ('pegawai_tbl p','rj.nip = p.nip');
		$this->db->where('rj.nip',$nip);
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get('riwayat_jabatan_tbl rh');
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function getValidPosition($id,$id2){
		return $this->db->query('select rj.position from riwayat_jabatan_tbl rj where rj.nip="'.$id.'" and rj.kd_skpd="'.$id2.'" and rj.valid_jabatan="Y"')->row()->position;
	}
	
	public function getListStaffByPosition($position,$skpd){
		$this->db->select('rj.kd_skpd, rj.nip, pg.nama, p.kd_position');
		$this->db->join('riwayat_jabatan_tbl rj','p.kd_position = rj.position and p.kd_skpd = rj.kd_skpd','left');
		$this->db->join('pegawai_tbl pg','pg.nip = rj.nip','left');
		$this->db->where('p.parent_position',$position);
		$this->db->where('p.skpd',$skpd);
		$query = $this->db->get('position_tbl p');
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	public function get($nip){
		$this->db->select ('rj.nip, p.nama, rj.seq_no, rj.kd_skpd, rj.jenis_jabatan, rj.nama_jabatan, 
			rj.kd_unitkerja,rj.unit_kerja, rj.esselon, rj.no_sk, rj.tmt, rj.valid_jabatan, pr.nama as nama_skpd');
		$this->db->join ('pegawai_tbl p','rj.nip = p.nip');
		$this->db->join ('skpd_tbl pr','rj.kd_skpd = pr.kd_skpd','left');
		$this->db->where('rj.nip',$nip);
		$this->db->order_by('rj.tmt DESC');
		// $this->db->order_by('rj.seq_no DESC');
		$query = $this->db->get('riwayat_jabatan_tbl rj');
        return $query->result();
    }

    public function getMaxByNip($nip){

		#Create where clause
		// $this->db->select_max('a.seq_no');
		$this->db->select_max('a.tmt');
		$this->db->where('a.nip', $nip);
		$this->db->from('riwayat_jabatan_tbl a');
		$where_clause = $this->db->get_compiled_select();

		#Create main query
		$this->db->select ('rj.nip, p.nama, rj.seq_no, rj.kd_skpd, rj.jenis_jabatan, rj.nama_jabatan, 
			rj.kd_unitkerja,rj.unit_kerja, rj.esselon, rj.no_sk, rj.tmt, rj.valid_jabatan, pr.nama as nama_skpd, rj.kd_jabatan, jb.level as level, p.status_pegawai as status_pegawai');
		$this->db->join ('pegawai_tbl p','rj.nip = p.nip');
		$this->db->join ('skpd_tbl pr','rj.kd_skpd = pr.kd_skpd','left');
		$this->db->join ('jabatan_tbl jb','rj.kd_jabatan = jb.kd_jabatan','left');
		$this->db->where('rj.nip',$nip);
		// $this->db->where('p.status_pegawai="Aktif" OR p.status_pegawai=""');

		$this->db->where("(p.status_pegawai='Aktif' OR p.status_pegawai='')", NULL, FALSE);

		/* Perubahan Sedikit */
		// $this->db->where('rj.jenis_jabatan','Struktural');

		// $this->db->where("seq_no = ($where_clause)", NULL, FALSE);
		$this->db->where("rj.tmt = ($where_clause)", NULL, FALSE);

		$this->db->order_by('rj.tmt DESC');
		$query = $this->db->get('riwayat_jabatan_tbl rj');
        return $query->row();
    }
	
	function get_jabatan() {
		$result = $this->db->get("jabatan_tbl");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["nama_jabatan"]] = $row["nama_jabatan"];
			}
		return $options;
	}
	
	function get_skpd_all() {
		$result = $this->db->get("skpd_tbl");
			$options = array();
				$options[""] = "";
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}
	
	public function ambil_unit_kerja($kd_skpd){
		$this->db->where('kd_skpd',$kd_skpd);
		$this->db->order_by('nama','asc');
		$unit_kerja=$this->db->get('unitkerja_tbl');
		if($unit_kerja->num_rows()>0){

			foreach ($unit_kerja->result_array() as $row)
			{
				$result['']= '- Pilih Unit Kerja-';
				$result[$row['kd_unitkerja']]= $row['nama'];
			}
			} else {
			   $result['']= '-Belum Ada Unit Kerja-';
			}
			return $result;
	}
	
	public function ambil_nama_jabatan($jenis_jabatan){
		$this->db->where('jenis_jabatan',$jenis_jabatan);
		$this->db->order_by('nama_jabatan','asc');
		$jabatan=$this->db->get('jabatan_tbl');
		if($jabatan->num_rows()>0){

			foreach ($jabatan->result_array() as $row)
			{
				$result['']= '- Pilih Nama Jabatan -';
				$result[$row['nama_jabatan']]= $row['nama_jabatan'];
			}
			} else {
			   $result['']= '- Belum Ada Nama Jabatan -';
			}
			return $result;
	}

	public function get_auto_nama_jabatan($jenis_jabatan,$kd_skpd,$kd_unitorganisasi,$kd_unitkerja,$kd_subunitkerja,$s){

		$this->db->select('kd_jabatan, nama_jabatan');

		$this->db->where('jenis_jabatan',$jenis_jabatan);
		
		$this->db->where('kd_skpd',$kd_skpd);
		if($kd_unitorganisasi != NULL){
			$this->db->where('kd_unitorganisasi',$kd_unitorganisasi);
		}
		if($kd_unitkerja != NULL){
			$this->db->where('kd_unitkerja',$kd_unitkerja);
		}
		if($kd_subunitkerja != NULL){
			$this->db->where('kd_subunitkerja',$kd_subunitkerja);
		}
		
		$this->db->like('nama_jabatan', $s);
		
		$this->db->order_by('nama_jabatan','asc');
		$jabatan=$this->db->get('jabatan_tbl');

		return $jabatan->result_array();
	}
	
}
