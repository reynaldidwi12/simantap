<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_anak_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count($nip) {
		return $this->db->count_all("riwayat_anak_tbl where nip = '$nip'");
	}
	
	public function fetchAll($nip) {
		$this->db->select ('nip, seq_no, nama, tempat_lahir, tgl_lahir, (case kelamin when "1" then "Pria" when "2" then "Wanita" end) kelamin ,
							(case status_anak when "1" then "Kandung" when "2" then "Tiri" when "3" then "Angkat" end) status_anak');
		$this->db->from ('riwayat_anak_tbl rp ');
		$this->db->where('rp.nip',$nip);
		$this->db->order_by('tgl_lahir ASC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	
	public function fetchById($nip,$seq_no){
		$this->db->select ('p.nama, p.nip, rp.seq_no, rp.nama as nama2, rp.tempat_lahir, rp.tgl_lahir, rp.kelamin, rp.bpjs, rp.status_anak, rp.status_pendidikan, rp.tingkat_pendidikan');
		$this->db->from ('riwayat_anak_tbl rp');
		$this->db->join ('pegawai_tbl p','p.nip = rp.nip','left');
		$this->db->where('rp.nip',$nip);
		$this->db->where('rp.seq_no',$seq_no);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function fetchById_pegawai($id, $id2, $seq_no){
		$this->db->select ('p.nip as nip2, ifnull(p.nip_lama,"-") nip_lama, p.nama as nama2,
		p.tempat_lahir, p.tgl_lahir, p.kelamin, pr.nip as nip, pr.nama as nama, "'.$seq_no.'" as seq_no');
		$this->db->from ('pegawai_tbl p, pegawai_tbl pr');
		$this->db->where('p.nip',$id);
		$this->db->where('pr.nip',$id2);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function create($data) {
		$this->nip = $data['nip'];
		$this->seq_no = $data['seq_no'];
		$this->nama = $data['nama'];
		$this->tempat_lahir = $data['tempat_lahir'];
		if($data['tgl_lahir']){
			$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));
		}else{}
		$this->tgl_lahir = $data['tgl_lahir2'];
		$this->kelamin = $data['kelamin'];
		$this->bpjs = $data['bpjs'];
		$this->status_anak = $data['status_anak'];
		$this->status_pendidikan = $data['status_pendidikan'];
		$this->tingkat_pendidikan = $data['tingkat_pendidikan'];
		
		// insert data
		$this->db->insert('riwayat_anak_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->nama = $data['nama'];
		$this->tempat_lahir = $data['tempat_lahir'];
		if($data['tgl_lahir']){
			$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));
		}else{}
		$this->tgl_lahir = $data['tgl_lahir2'];
		$this->kelamin = $data['kelamin'];
		$this->bpjs = $data['bpjs'];
		$this->status_anak = $data['status_anak'];
		$this->status_pendidikan = $data['status_pendidikan'];
		$this->tingkat_pendidikan = $data['tingkat_pendidikan'];
		
		// update data
		$this->db->update ('riwayat_anak_tbl', $this, array ('nip' => $data['nip'],'seq_no' => $data['seq_no']));
	}
	
	public function delete($id, $seq_no) {
		$this->db->delete ('riwayat_anak_tbl', array ('nip' => $id,'seq_no'=> $seq_no));
	}
	
	public function search_count($column, $data,$nip){
		$this->db->where('nip',$nip);
		$this->db->where($column,$data);
		return  $this->db->count_all('riwayat_anak_tbl');
	}
	
	public function generateSeqNo($nip){
		return $this->db->query("SELECT ifnull(max(seq_no),0) seq_no FROM riwayat_anak_tbl WHERE nip='".$nip."'")->row()->seq_no+1;
	}
	
	public function search($column,$value, $nip, $limit, $start){
		
		$this->db->select ('rp.nip, rp.seq_no, rp.jenis_pendidikan, rp.jurusan, rp.nama_sekolah, rp.kepala_sekolah, 
				rp.no_ijazah, rp.tahun_ijazah, rp.qversion, rp.qid');
		$this->db->where('nip',$nip);
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get('riwayat_anak_tbl rp');
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
		
	}
	
	public function get($nip){
        $this->db->where('nip', $nip);
        $query = $this->db->get('riwayat_anak_tbl');
        return $query->result();
    }
	
}