<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class sub_unit_kerja_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all_results("subunitkerja_tbl");
	}
	

	public function fetchAll($limit, $start) {
		$this->db->select ('d.kd_subunitkerja, a.nama as nama_skpd, b.nama as nama_unit_kerja, c.nama as nama_unit_organisasi, d.nama as nama_sub_unit_kerja');
		$this->db->from ('skpd_tbl a, unitkerja_tbl b, unitorganisasi_tbl c, subunitkerja_tbl d');
		$this->db->where ('a.kd_skpd=d.kd_skpd');
		$this->db->where ('b.kd_unitkerja=d.kd_unitkerja');
		$this->db->where ('c.kd_unitorganisasi=d.kd_unitorganisasi');
		$this->db->order_by ('d.qid DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('d.kd_subunitkerja, a.kd_skpd as kd_skpd, b.nama as nama_unitkerja, c.kd_unitorganisasi as kd_unitorganisasi, c.nama as nama_unit_organisasi, d.nama as nama_subunitkerja, d.kd_unitkerja as kd_unitkerja');
		$this->db->from ('skpd_tbl a, unitkerja_tbl b, unitorganisasi_tbl c, subunitkerja_tbl d');
		$this->db->where ('a.kd_skpd=d.kd_skpd');
		$this->db->where ('b.kd_unitkerja=d.kd_unitkerja');
		$this->db->where ('c.kd_unitorganisasi=d.kd_unitorganisasi');
		$this->db->where('d.kd_subunitkerja', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function create($data) {
		$this->kd_subunitkerja = $data['kd_subunitkerja'];
		$this->kd_unitkerja = $data['kd_unitkerja'];
		$this->nama = $data['nama_subunitkerja'];
		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		
		// print_r($this);exit;
		// insert data
		$this->db->insert('subunitkerja_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->kd_unitkerja = $data['kd_unitkerja'];
		$this->nama = $data['nama_subunitkerja'];
		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		
		// update data
		$this->db->update ('subunitkerja_tbl', $this, array ('kd_subunitkerja' => $data['kd_subunitkerja']));
	}
	
	public function delete($id) {
		$this->db->delete ('subunitkerja_tbl', array ('kd_subunitkerja' => $id));
	}
	
	public function search_count($column, $data){
		return  $this->db->count_all("skpd_tbl a, unitkerja_tbl b, unitorganisasi_tbl c, subunitkerja_tbl d  WHERE a.kd_skpd=d.kd_skpd AND b.kd_unitkerja=d.kd_unitkerja AND c.kd_unitorganisasi=d.kd_unitorganisasi AND $column like '%$data%'" );
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('d.kd_subunitkerja, a.nama as nama_skpd, b.nama as nama_unit_kerja, c.nama as nama_unit_organisasi, d.nama as nama_sub_unit_kerja');
		$this->db->from ('skpd_tbl a, unitkerja_tbl b, unitorganisasi_tbl c, subunitkerja_tbl d');
		$this->db->where ('a.kd_skpd=d.kd_skpd');
		$this->db->where ('b.kd_unitkerja=d.kd_unitkerja');
		$this->db->where ('c.kd_unitorganisasi=d.kd_unitorganisasi');
		$this->db->like($column,$value);
		$this->db->order_by ('d.qid DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();

		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
		
	}
	
	public function generatekd_subunitkerja(){
		return $this->db->query("SELECT kd_unitkerja FROM unitkerja_tbl order by kd_unitkerja desc limit 1")->row()->kd_unitkerja+1;	
	}


	public function ambil_sub_unit_kerja($kd_unitkerja){
		$this->db->where('kd_unitkerja',$kd_unitkerja);
		$this->db->order_by('nama','asc');
		$unit_kerja=$this->db->get('subunitkerja_tbl');
	
		if($unit_kerja->num_rows()>0){
			foreach ($unit_kerja->result_array() as $row)
			{
				$result['']= '- Pilih Sub Unit Kerja-';
				$result[$row['kd_subunitkerja']]= $row['nama'];
			}
		} else {
			$result['']= '-Belum Ada Sub Unit Kerja-';
		}
		
		return $result;
	}


	public function get_auto_nama($data){
		$this->db->select('kd_subunitkerja, nama');
		
		$this->db->where('kd_skpd',$data['kd_skpd']);
		$this->db->where('kd_unitorganisasi',$data['kd_unitorganisasi']);
		$this->db->where('kd_unitkerja',$data['kd_unitkerja']);
		$this->db->like('nama', $data['keyword']);
		
		$this->db->order_by('nama','asc');
		$jabatan=$this->db->get('subunitkerja_tbl');
		

		return $jabatan->result_array();
	}
	
	
	
}