<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class kelurahan_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("kelurahan_tbl");
	}
	
	public function fetchAll($limit, $start) {
		$this->db->select ('*');
		$this->db->where ('a.kec_id = b.kec_id');
		$this->db->from ('kelurahan_tbl a, kecamatan_tbl b');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('*');
		$this->db->from ('kelurahan_tbl');
		$this->db->where('kel_id', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	function get_kecamatan() {
		$result = $this->db->get("kecamatan_tbl");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["kec_id"]] = $row["kec_nama"];
			}
		return $options;
	}
	
	public function get_kecamatan_id($id){
		$this->db->select ('*');
		$this->db->where ('kec_id', $id);
		$this->db->from ('kecamatan_tbl');
		$result = $this->db->get();
			$options = array();
				foreach($result->result_array() as $row) {
				$options = $row["kec_id"];
			}
		return $options;
	}
	
	public function create($data) {
		$this->kel_id = $data['kel_id'];
		$this->kec_id = $data['kec_id'];
		$this->kel_nama = $data['kel_nama'];
		// insert data
		$this->db->insert('kelurahan_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->kec_id = $data['kec_id'];
		$this->kel_nama = $data['kel_nama'];
		
		// update data
		$this->db->update ('kelurahan_tbl', $this, array ('kel_id' => $data['kel_id']));
	}
	
	public function delete($id) {
		$this->db->delete ('kelurahan_tbl', array ('kel_id' => $id));
	}
	
	public function search_count($column, $data){
		return  $this->db->count_all("kelurahan_tbl a, kecamatan_tbl b where a.kec_id=b.kec_id AND $column like '%$data%'" );
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('*');
		$this->db->from ('kelurahan_tbl a, kecamatan_tbl b');
		$this->db->where ('a.kec_id=b.kec_id');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	public function generatekd_kel(){
		return $this->db->query("SELECT kel_id FROM kelurahan_tbl order by kel_id desc limit 1")->row()->kel_id+1;	
	}
	
}