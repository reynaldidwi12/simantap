<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class Riwayat_pendidikan_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count($nip) {
		return $this->db->count_all("riwayat_pendidikan_tbl where nip = '$nip'");
	}
	
	public function fetchAll($limit, $start,$nip) {
		$this->db->select ('rp.nip, rp.seq_no, (CASE rp.jenis_pendidikan WHEN "1" THEN "SD" WHEN "2" THEN "SMP/MTs/Sederajat" 
		WHEN "3" THEN "SMA/SMK/Sederajat" 
		WHEN "4" THEN "Diploma 1" 
		WHEN "5" THEN "Diploma 2" 
		WHEN "6" THEN "Diploma 3" 
		WHEN "7" THEN "Diploma 4/Strata 1" 
		WHEN "8" THEN "Strata 2" 
		WHEN "9" THEN "Doktor" 
		END) jenis_pendidikan, rp.nama_sekolah, rp.program_studi, 
				rp.no_ijazah, rp.tgl_ijazah, rp.tahun_ijazah, rp.qversion, rp.qid');
		$this->db->from ('riwayat_pendidikan_tbl rp ');
		$this->db->where('rp.nip',$nip);
		$this->db->order_by('rp.jenis_pendidikan DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	
	public function fetchById($nip,$seq_no){
		$this->db->select ('rp.nip,p.nama, rp.seq_no, rp.jenis_pendidikan, rp.nama_sekolah, rp.program_studi, 
				rp.no_ijazah, rp.tgl_ijazah, rp.tahun_ijazah, rp.qversion, rp.qid');
		$this->db->from ('riwayat_pendidikan_tbl rp');
		$this->db->join ('pegawai_tbl p','p.nip = rp.nip','left');
		$this->db->where('rp.nip',$nip);
		$this->db->where('rp.seq_no',$seq_no);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function create($data) {
		$this->nip = $data['nip'];
		$this->seq_no = $data['seq_no'];
		$this->jenis_pendidikan = $data['jenis_pendidikan'];
		$this->nama_sekolah = $data['nama_sekolah'];
		$this->program_studi = $data['program_studi'];
		$this->no_ijazah = $data['no_ijazah'];
		if($data['tgl_ijazah']){
			$data['tgl_ijazah2'] = date("Y-m-d",strtotime($data['tgl_ijazah']));
		}else{}
		$this->tgl_ijazah = $data['tgl_ijazah2'];
		
		// insert data
		$this->db->insert('riwayat_pendidikan_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->jenis_pendidikan = $data['jenis_pendidikan'];
		$this->nama_sekolah = $data['nama_sekolah'];
		$this->program_studi = $data['program_studi'];
		$this->no_ijazah = $data['no_ijazah'];
		if($data['tgl_ijazah']){
			$data['tgl_ijazah2'] = date("Y-m-d",strtotime($data['tgl_ijazah']));
		}else{}
		$this->tgl_ijazah = $data['tgl_ijazah2'];
		
		// update data
		$this->db->update ('riwayat_pendidikan_tbl', $this, array ('nip' => $data['nip'],'seq_no' => $data['seq_no']));
	}
	
	public function delete($id, $seq_no) {
		$this->db->delete ('riwayat_pendidikan_tbl', array ('nip' => $id,'seq_no'=> $seq_no));
	}
	
	public function search_count($column, $data,$nip){
		$this->db->where('nip',$nip);
		$this->db->where($column,$data);
		return  $this->db->count_all('riwayat_pendidikan_tbl');
	}
	
	public function generateSeqNo($nip){
		return $this->db->query("SELECT ifnull(max(seq_no),0) seq_no FROM riwayat_pendidikan_tbl WHERE nip='".$nip."'")->row()->seq_no+1;
	}
	
	public function search($column,$value, $nip, $limit, $start){
		
		$this->db->select ('rp.nip, rp.seq_no, rp.jenis_pendidikan, rp.nama_sekolah, rp.program_studi, 
				rp.no_ijazah, rp.tahun_ijazah, rp.qversion, rp.qid');
		$this->db->where('nip',$nip);
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get('riwayat_pendidikan_tbl rp');
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
		
	}
	
	public function get($nip){
        $this->db->where('nip', $nip);
        $this->db->order_by('jenis_pendidikan DESC');
        $query = $this->db->get('riwayat_pendidikan_tbl');
        return $query->result();
    }
	
	function get_pendidikan() {
		$result = $this->db->get("riwayat_pendidikan_tbl");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["program_studi"]] = $row["program_studi"];
			}
		return $options;
	}
	
}
