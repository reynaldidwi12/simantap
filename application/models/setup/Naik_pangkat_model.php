<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class naik_pangkat_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("	pegawai_tbl p, riwayat_kepangkatan_tbl b 
										WHERE b.tmt_pangkat IN(
										SELECT MAX(tes.tmt_pangkat)
										FROM riwayat_kepangkatan_tbl tes
										where tes.nip=b.nip)
										and p.nip = b.nip
										and p.status_pegawai != 'Pindah Tugas Keluar' 
										and p.status_pegawai != 'Pensiun'
										and p.status_pegawai != 'Meninggal'
										and YEAR(curdate())-YEAR(tmt_pangkat) > 4 ");
	}
	
	public function record_count_skpd($kd_skpd) {
		return $this->db->count_all("pegawai_tbl p, riwayat_kepangkatan_tbl b 
										WHERE b.tmt_pangkat IN(
										SELECT MAX(tes.tmt_pangkat)
										FROM riwayat_kepangkatan_tbl tes
										where tes.nip=b.nip)
										and p.nip = b.nip
										and p.status_pegawai != 'Pindah Tugas Keluar' 
										and p.status_pegawai != 'Pensiun'
										and p.status_pegawai != 'Meninggal'
										AND p.kd_skpd = '$kd_skpd'
										and YEAR(curdate())-YEAR(tmt_pangkat) > 4 ");
	}

	public function record_count_per_skpd($kd_skpd="") {
		$query = $this->db->query("
			SELECT p.nip
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_kepangkatan_tbl b
			on b.tmt_pangkat IN(
			SELECT MAX(tmt_pangkat)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN riwayat_jabatan_tbl c
			on c.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=c.nip)
			LEFT JOIN jabatan_tbl d on d.kd_jabatan = c.kd_jabatan
			
			WHERE 
			p.nip = b.nip AND
			p.nip = c.nip AND
			d.kd_skpd = '".$kd_skpd."' AND 
			YEAR(curdate())-YEAR(tmt_pangkat) > 4

			GROUP BY p.nip , d.kd_skpd");

		return $query->num_rows();
	}
	
	public function search_count($column, $data){
		return  $this->db->count_all("	pegawai_tbl p, riwayat_kepangkatan_tbl b 
										WHERE b.tmt_pangkat IN(
										SELECT MAX(tes.tmt_pangkat)
										FROM riwayat_kepangkatan_tbl tes
										where tes.nip=b.nip)
										and p.nip = b.nip
										and p.status_pegawai != 'Pindah Tugas Keluar' 
										and p.status_pegawai != 'Pensiun'
										and p.status_pegawai != 'Meninggal'
										and YEAR(curdate())-YEAR(tmt_pangkat) > 4 
										and $column = '$data'");
	}
	
	
	public function fetchAll($limit, $start) {
		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, 
		(case p.kelamin when "1" then "Pria" else "Wanita" end) kelamin ,
		(case p.agama when "1" then "Islam" when "2" then "Katolik" when "3" then "Protestan" when "4" then "Hindu" when "5" then "Budha" when "6" then "Sinto" when "7" then "Konghucu" else "Lain-lain" end) agama, 
		(case p.gol_darah when "1" then "A" when "2" then "B" when "3" then "AB" else "O" end) gol_darah,
		(case p.status when "1" then "Lajang" when "2" then "Nikah" else "Cerai" end) status, 
		p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos');
		$this->db->from ('pegawai_tbl p, riwayat_kepangkatan_tbl b ');
		$this->db->where ('	b.tmt_pangkat IN(
							SELECT MAX(tes.tmt_pangkat)
							FROM riwayat_kepangkatan_tbl tes
							where tes.nip=b.nip)');
		$this->db->where ('p.nip = b.nip');
		$this->db->where ('p.status_pegawai != "Pindah Tugas Keluar"');
		$this->db->where ('p.status_pegawai != "Pensiun"');
		$this->db->where ('p.status_pegawai != "Meninggal"');
		$this->db->where ('YEAR(curdate())-YEAR(b.tmt_pangkat) > 4');
		//$this->db->group_by('p.nip');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, 
		(case p.kelamin when "1" then "Pria" else "Wanita" end) kelamin ,
		(case p.agama when "1" then "Islam" when "2" then "Katolik" when "3" then "Protestan" when "4" then "Hindu" when "5" then "Budha" when "6" then "Sinto" when "7" then "Konghucu" else "Lain-lain" end) agama, 
		(case p.gol_darah when "1" then "A" when "2" then "B" when "3" then "AB" else "O" end) gol_darah,
		(case p.status when "1" then "Lajang" when "2" then "Nikah" else "Cerai" end) status, 
		p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos');
		$this->db->from ('pegawai_tbl p, riwayat_kepangkatan_tbl b ');
		$this->db->where ('	b.tmt_pangkat IN(
							SELECT MAX(tes.tmt_pangkat)
							FROM riwayat_kepangkatan_tbl tes
							where tes.nip=b.nip)');
		$this->db->where ('p.nip = b.nip');
		$this->db->where ('p.status_pegawai != "Pindah Tugas Keluar"');
		$this->db->where ('p.status_pegawai != "Pensiun"');
		$this->db->where ('p.status_pegawai != "Meninggal"');
		$this->db->where ('YEAR(curdate())-YEAR(b.tmt_pangkat) > 4');
		$this->db->where($column,$value);
		//$this->db->group_by('p.nip');
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	/*SELECT a.nama, b.tmt_pangkat
FROM pegawai_tbl a, riwayat_kepangkatan_tbl b
WHERE b.tmt_pangkat IN(
SELECT MAX(tes.tmt_pangkat)
FROM riwayat_kepangkatan_tbl tes
where tes.nip=b.nip) AND a.nip= b.nip AND kd_skpd='24'
AND YEAR(curdate())-YEAR(b.tmt_pangkat) > 4
GROUP BY a.nip*/
	
	
	public function fetchAll_skpd($kd_skpd, $limit, $start) {
		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, 
		(case p.kelamin when "1" then "Pria" else "Wanita" end) kelamin ,
		(case p.agama when "1" then "Islam" when "2" then "Katolik" when "3" then "Protestan" when "4" then "Hindu" when "5" then "Budha" when "6" then "Sinto" when "7" then "Konghucu" else "Lain-lain" end) agama, 
		(case p.gol_darah when "1" then "A" when "2" then "B" when "3" then "AB" else "O" end) gol_darah,
		(case p.status when "1" then "Lajang" when "2" then "Nikah" else "Cerai" end) status, 
		p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos');
		$this->db->from ('pegawai_tbl p, riwayat_kepangkatan_tbl b ');
		$this->db->where ('	b.tmt_pangkat IN(
							SELECT MAX(tes.tmt_pangkat)
							FROM riwayat_kepangkatan_tbl tes
							where tes.nip=b.nip)');
		$this->db->where ('p.nip = b.nip');
		$this->db->where ('p.status_pegawai != "Pindah Tugas Keluar"');
		$this->db->where ('p.status_pegawai != "Pensiun"');
		$this->db->where ('p.status_pegawai != "Meninggal"');
		$this->db->where ('p.kd_skpd', $kd_skpd);
		$this->db->where ('YEAR(curdate())-YEAR(b.tmt_pangkat) > 4');
		//$this->db->like($column,$value);
		//$this->db->group_by('p.nip');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function fetchAll_per_skpd($kd_skpd, $limit, $start) {
		$query = $this->db->query("
			SELECT p.nip, ifnull(p.nip_lama,'-') nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, 
		(case p.kelamin when '1' then 'Pria' when '2' then 'Wanita' end) kelamin ,
		(case p.agama when '1' then 'Islam' when '2' then 'Katolik' when '3' then 'Hindu' when '4' then 'Budha' when '5' then 'Sinto' when '6' then 'Konghucu' when '7' then 'Protestan' end) agama, 
		(case p.gol_darah when '1' then 'A' when '2' then 'B' when '3' then 'AB' else 'O' end) gol_darah,
		(case p.status when '1' then 'Lajang' when '2' then 'Nikah' else 'Cerai' end) status, 
		p.status_pegawai, p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_kepangkatan_tbl b
			on b.tmt_pangkat IN(
			SELECT MAX(tmt_pangkat)
			FROM riwayat_kepangkatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN riwayat_jabatan_tbl c
			on c.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=c.nip)
			LEFT JOIN jabatan_tbl d on d.kd_jabatan = c.kd_jabatan
			
			WHERE 
			p.nip = b.nip AND
			p.nip = c.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			d.kd_skpd = '".$kd_skpd."' AND 
			YEAR(curdate())-YEAR(tmt_pangkat) > 4

			GROUP BY p.nip , d.kd_skpd

			LIMIT $start, $limit");

		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	function get_skpd_all() {
		$result = $this->db->get("skpd_tbl");
			$options = array();
				$options[""] = "";
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}
	
}