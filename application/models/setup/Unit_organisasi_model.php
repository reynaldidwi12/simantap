<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class unit_organisasi_model extends CI_Model {

	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		$this->db->where('status',1);
		return $this->db->count_all("unitorganisasi_tbl");
	}
	
	public function record_count_all() {
		return $this->db->count_all("unitorganisasi_tbl");
	}

	public function fetchAll($limit, $start) {
		$this->db->select ('b.kd_unitorganisasi, a.nama as nama_skpd, b.nama as nama_unit_organisasi, b.qid as id_unor');
		$this->db->from ('skpd_tbl a, unitorganisasi_tbl b');
		$this->db->where ('a.kd_skpd=b.kd_skpd');
		$this->db->order_by ('b.qid DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('b.kd_unitorganisasi, a.kd_skpd as kd_skpd, b.nama as nama_unitorganisasi, b.qid as id_unor');
		$this->db->from ('skpd_tbl a, unitorganisasi_tbl b');
		$this->db->where ('a.kd_skpd=b.kd_skpd');
		$this->db->where('b.kd_unitorganisasi', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function create($data) {
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		$this->nama = $data['nama_unitorganisasi'];
		$this->kd_skpd = $data['kd_skpd'];

		// insert data
		$this->db->insert('unitorganisasi_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->nama = $data['nama_unitorganisasi'];
		$this->kd_skpd = $data['kd_skpd'];
		
		// update data
		$this->db->update ('unitorganisasi_tbl', $this, array ('kd_unitorganisasi' => $data['kd_unitorganisasi']));
	}
	
	public function delete($id) {
		$this->db->delete ('unitorganisasi_tbl', array ('kd_unitorganisasi' => $id));
	}
	
	public function search_count($column, $data){
		return  $this->db->count_all("skpd_tbl a, unitorganisasi_tbl b  WHERE a.kd_skpd=b.kd_skpd AND $column like '%$data%'" );
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('b.kd_unitorganisasi, a.nama as nama_skpd, b.nama as nama_unit_organisasi, b.qid as id_unor');
		$this->db->from ('skpd_tbl a, unitorganisasi_tbl b');
		$this->db->where ('a.kd_skpd=b.kd_skpd');
		$this->db->like($column,$value);
		$this->db->order_by ('b.qid DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function ambil_unit_organisasi($kd_skpd){
		$this->db->where('kd_skpd',$kd_skpd);
		$this->db->order_by('nama','asc');
		$unit_organisasi=$this->db->get('unitorganisasi_tbl');
		if($unit_organisasi->num_rows()>0){

			foreach ($unit_organisasi->result_array() as $row)
			{
				$result['']= '- Pilih Unit Organisasi-';
				$result[$row['kd_unitorganisasi']]= $row['nama'];
			}
			} else {
			   $result['']= '-Belum Ada Unit Organisasi-';
			}
			return $result;
	}
	

	public function get_auto_nama($data){
		$this->db->select('kd_unitorganisasi, nama');
		
		$this->db->where('kd_skpd',$data['kd_skpd']);
		$this->db->like('nama', $data['keyword']);
		
		$this->db->order_by('nama','asc');
		$jabatan=$this->db->get('unitorganisasi_tbl');
		

		return $jabatan->result_array();
	}
	
	
	
}