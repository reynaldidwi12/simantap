<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class riwayat_suami_istri_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count($nip) {
		return $this->db->count_all("riwayat_suami_istri_tbl where nip = '$nip'");
	}
	
	public function fetchAll($nip) {
		$this->db->select ('*');
		$this->db->from ('riwayat_suami_istri_tbl rp ');
		$this->db->where('rp.nip',$nip);
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function jml($nip) {
	
        $query = $this->db->query("SELECT * FROM riwayat_suami_istri_tbl rp WHERE  rp.nip='$nip'");
		return $query->result();
    }
	
	public function fetchById($nip,$seq_no){
		$this->db->select ('p.nama, p.nip, rp.seq_no, rp.nama as nama2, rp.tempat_lahir, rp.tgl_lahir, 
		rp.tgl_menikah, rp.tgl_meninggal, rp.tgl_cerai, rp.no_akta_nikah, rp.no_akta_meninggal, rp.no_akta_cerai, rp.status, rp.bpjs');
		$this->db->from ('riwayat_suami_istri_tbl rp');
		$this->db->join ('pegawai_tbl p','p.nip = rp.nip','left');
		$this->db->where('rp.nip',$nip);
		$this->db->where('rp.seq_no',$seq_no);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function fetchById_pegawai($id, $id2, $seq_no){
		$this->db->select ('p.nip as nip2, ifnull(p.nip_lama,"-") nip_lama, p.nama as nama2,
		p.tempat_lahir, p.tgl_lahir, p.kelamin, pr.nip as nip, pr.nama as nama, "'.$seq_no.'" as seq_no');
		$this->db->from ('pegawai_tbl p, pegawai_tbl pr');
		$this->db->where('p.nip',$id);
		$this->db->where('pr.nip',$id2);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function create($data) {
		$this->nip = $data['nip'];
		$this->seq_no = $data['seq_no'];
		$this->nama = $data['nama'];
		$this->tempat_lahir = $data['tempat_lahir'];
		
		if($data['tgl_lahir']){
			$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));	
		}else{}
		$this->tgl_lahir = $data['tgl_lahir2'];
		
		if($data['tgl_menikah']){
			$data['tgl_menikah2'] = date("Y-m-d",strtotime($data['tgl_menikah']));
		}else{}
		$this->tgl_menikah = $data['tgl_menikah2'];
		
		if($data['tgl_meninggal']){
			$data['tgl_meninggal2'] = date("Y-m-d",strtotime($data['tgl_meninggal']));
		}else{}
		$this->tgl_meninggal = $data['tgl_meninggal2'];
		
		if($data['tgl_cerai']){
			$data['tgl_cerai2'] = date("Y-m-d",strtotime($data['tgl_cerai']));
		}else{}
		$this->tgl_cerai = $data['tgl_cerai2'];
		
		$this->no_akta_nikah = $data['no_akta_nikah'];
		$this->no_akta_meninggal = $data['no_akta_meninggal'];
		$this->no_akta_cerai = $data['no_akta_cerai'];
		$this->status = $data['status'];
		$this->bpjs = $data['bpjs'];
		
		
		// insert data
		$this->db->insert('riwayat_suami_istri_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->nama = $data['nama'];
		$this->tempat_lahir = $data['tempat_lahir'];
		if($data['tgl_lahir']){
			$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));	
		}else{}
		$this->tgl_lahir = $data['tgl_lahir2'];
		
		if($data['tgl_menikah']){
			$data['tgl_menikah2'] = date("Y-m-d",strtotime($data['tgl_menikah']));
		}else{}
		$this->tgl_menikah = $data['tgl_menikah2'];
		
		if($data['tgl_meninggal']){
			$data['tgl_meninggal2'] = date("Y-m-d",strtotime($data['tgl_meninggal']));
		}else{}
		$this->tgl_meninggal = $data['tgl_meninggal2'];
		
		if($data['tgl_cerai']){
			$data['tgl_cerai2'] = date("Y-m-d",strtotime($data['tgl_cerai']));
		}else{}
		$this->tgl_cerai = $data['tgl_cerai2'];
		
		$this->no_akta_nikah = $data['no_akta_nikah'];
		$this->no_akta_meninggal = $data['no_akta_meninggal'];
		$this->no_akta_cerai = $data['no_akta_cerai'];
		$this->status = $data['status'];
		$this->bpjs = $data['bpjs'];
		
		// update data
		$this->db->update ('riwayat_suami_istri_tbl', $this, array ('nip' => $data['nip'],'seq_no' => $data['seq_no']));
	}
	
	public function delete($id, $seq_no) {
		$this->db->delete ('riwayat_suami_istri_tbl', array ('nip' => $id,'seq_no'=> $seq_no));
	}
	
	public function search_count($column, $data,$nip){
		$this->db->where('nip',$nip);
		$this->db->where($column,$data);
		return  $this->db->count_all('riwayat_suami_istri_tbl');
	}
	
	public function generateSeqNo($nip){
		return $this->db->query("SELECT ifnull(max(seq_no),0) seq_no FROM riwayat_suami_istri_tbl WHERE nip='".$nip."'")->row()->seq_no+1;
	}
	
	public function search($column,$value, $nip, $limit, $start){
		
		$this->db->select ('rp.nip, rp.seq_no, rp.jenis_pendidikan, rp.jurusan, rp.nama_sekolah, rp.kepala_sekolah, 
				rp.no_ijazah, rp.tahun_ijazah, rp.qversion, rp.qid');
		$this->db->where('nip',$nip);
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get('riwayat_suami_istri_tbl rp');
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
		
	}
	
	public function get($nip){
        $this->db->where('nip', $nip);
        $query = $this->db->get('riwayat_suami_istri_tbl');
        return $query->result();
    }
	
}