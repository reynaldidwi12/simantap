<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class skpd_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("skpd_tbl");
	}
	
	public function fetchAll($limit, $start) {
		$this->db->select ('*');
		$this->db->from ('skpd_tbl');
		$this->db->order_by ('kode DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('*');
		$this->db->from ('skpd_tbl');
		$this->db->where('kd_skpd', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function fetchByIdSingle($id){
		$this->db->select ('*');
		$this->db->from ('skpd_tbl');
		$this->db->where('kd_skpd', $id);
		$query = $this->db->get()->row();
		return $query;
	}
	
	//--- untuk get skpd user ---//
	 public function fetchById2($id){
        $this->db->where('kd_skpd', $id);
        $query = $this->db->get('skpd_tbl');
        return $query->result();
    }
	
	public function create($data) {
		$this->kd_skpd = $data['kd_skpd'];
		$this->nama = $data['nama'];
		$this->kepala_skpd = $data['kepala_skpd'];
		$this->category = $data['category'];
		$this->kode = $data['kode'];
		$this->status = $data['status'];
		
		// insert data
		$this->db->insert('skpd_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->nama = $data['nama'];
		$this->kepala_skpd = $data['kepala_skpd'];
		$this->category = $data['category'];
		$this->kode = $data['kode'];
		$this->status = $data['status'];
		
		// update data
		$this->db->update ('skpd_tbl', $this, array ('kd_skpd' => $data['kd_skpd']));
	}
	
	public function delete($id) {
		$this->db->delete ('skpd_tbl', array ('kd_skpd' => $id));
	}
	
	public function search_count($column, $data){
		return  $this->db->count_all("skpd_tbl where $column like '%$data%'");
	}
	
	public function getName($kode){
		$this->db->select('nama');
		$this->db->from('skpd_tbl');
		$this->db->where('kd_skpd', $kode);
		$query= $this->db->get();
		$ret = $query->row();
		return $ret->nama;
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('*');
		$this->db->from ('skpd_tbl');
		$this->db->order_by ('kode DESC');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	function get_skpd() {
		$result = $this->db->get("skpd_tbl");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}

	function get_skpd_per_kd($kd_skpd) {
		$this->db->where('kd_skpd',$kd_skpd);
		$result = $this->db->get("skpd_tbl");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}

	function get_skpd_by_status($status) {
		$this->db->where('status', $status);
		$result = $this->db->get("skpd_tbl");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}
	
	public function generatekd_skpd(){
		return $this->db->query("SELECT kd_skpd FROM skpd_tbl order by kode desc limit 1")->row()->kd_skpd+1;	
	}
	
}