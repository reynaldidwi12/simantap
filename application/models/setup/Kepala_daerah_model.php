<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class kepala_daerah_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("kepaladaerah_tbl");
	}
	
	public function fetchAll($limit, $start) {
		$this->db->select ('*');
		$this->db->from ('kepaladaerah_tbl');
		$this->db->order_by ('qid DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('*');
		$this->db->from ('kepaladaerah_tbl');
		$this->db->where('qid', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function fetchByIdSingle($id){
		$this->db->select ('*');
		$this->db->from ('kepaladaerah_tbl');
		$this->db->where('qid', $id);
		$query = $this->db->get()->row();
		return $query;
	}

	public function getKepalaDaerah(){
		$this->db->select ('*');
		$this->db->from ('kepaladaerah_tbl');
		$this->db->order_by ('qid ASC');
		$this->db->limit (1);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function getKepalaDaerahSingle(){
		$this->db->select ('*');
		$this->db->from ('kepaladaerah_tbl');
		$this->db->order_by ('qid ASC');
		$this->db->limit (1);
		$query = $this->db->get()->row();
		return $query;
	}
	
	
	public function create($data) {
		$this->kd_kada = $data['kd_kada'];
		$this->kosong_satu = $data['kosong_satu'];
		$this->kosong_dua = $data['kosong_dua'];
		
		// insert data
		$this->db->insert('kepaladaerah_tbl', $this);
	}

	public function update($data) {
		// get data
		$this->kosong_satu = $data['kosong_satu'];
		$this->kosong_dua = $data['kosong_dua'];
		
		// update data
		$this->db->update ('kepaladaerah_tbl', $this, array ('qid' => $data['qid']));
	}
	
	public function delete($id) {
		$this->db->delete ('kepaladaerah_tbl', array ('qid' => $id));
	}
	
	public function search_count($column, $data){
		return  $this->db->count_all("kepaladaerah_tbl where $column like '%$data%'");
	}
	
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('*');
		$this->db->from ('kepaladaerah_tbl');
		$this->db->order_by ('qid DESC');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
}