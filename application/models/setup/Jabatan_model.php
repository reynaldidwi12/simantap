<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class jabatan_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}

	public function record_count() {
		return $this->db->count_all("jabatan_tbl");
	}

	public function fetchAll($limit, $start) {
		$this->db->select ('*');
		$this->db->from ('jabatan_tbl');
		$this->db->order_by ('kd_jabatan DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function fetchAllNew($limit, $start) {
		$this->db->select ('a.*, s.nama as skpd, b.nama as unit_organisasi, c.nama as unit_kerja, d.nama as sub_unit_kerja');
		$this->db->from ('jabatan_tbl a');
		$this->db->join('skpd_tbl s','a.kd_skpd = s.kd_skpd','left');
		$this->db->join('unitorganisasi_tbl b','a.kd_unitorganisasi = b.kd_unitorganisasi','left');
		$this->db->join('unitkerja_tbl c','a.kd_unitkerja = c.kd_unitkerja','left');
		$this->db->join('subunitkerja_tbl d','a.kd_subunitkerja = d.kd_subunitkerja','left');

		$this->db->order_by ('kd_jabatan DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function fetchById($id){
		$this->db->select ('*');
		$this->db->from ('jabatan_tbl');
		$this->db->where('kd_jabatan', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function fetchByIdSingle($id){
		$this->db->select ('*');
		$this->db->from ('jabatan_tbl');
		$this->db->where('kd_jabatan', $id);
		$query = $this->db->get()->row();
		return $query;
	}

	public function create($data) {
		$this->kd_jabatan = $data['kd_jabatan'];
		$this->nama_jabatan = $data['nama_jabatan'];
		$this->jenis_jabatan = $data['jenis_jabatan'];

		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		$this->kd_unitkerja = $data['kd_unitkerja'];
		$this->kd_subunitkerja = $data['kd_subunitkerja'];

		// $this->position = $data['position'];

		if($data['position'] != NULL){
			$this->position = $data['position'];
		}


		// Fungsional Tertentu
		// Fungsional Umum

		if($this->kd_skpd != NULL && $this->kd_unitorganisasi != NULL && $this->kd_unitkerja != NULL && $this->kd_subunitkerja != NULL){
			$this->level = 5;
			$this->atasan_id = $this->kd_unitkerja;
			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 6;
				$this->atasan_id = $this->kd_subunitkerja;
			}

		}else if($this->kd_skpd != NULL && $this->kd_unitorganisasi != NULL && $this->kd_unitkerja != NULL && $this->kd_subunitkerja == NULL){
			$this->kd_subunitkerja = NULL;
			$this->level = 4;
			$this->atasan_id = $this->kd_unitorganisasi;

			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 5;
				$this->atasan_id = $this->kd_unitkerja;
			}

		}else if($this->kd_skpd != NULL && $this->kd_unitorganisasi != NULL && $this->kd_unitkerja == NULL && $this->kd_subunitkerja == NULL){
			$this->kd_unitkerja = NULL;
			$this->kd_subunitkerja = NULL;
			$this->level = 3;
			$this->atasan_id = $this->kd_skpd;

			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 4;
				$this->atasan_id = $this->kd_unitorganisasi;
			}

		}else if($this->kd_skpd != NULL && $this->kd_unitorganisasi == NULL && $this->kd_unitkerja == NULL && $this->kd_subunitkerja == NULL){
			$this->level = 2;
			$this->kd_unitorganisasi = NULL;
			$this->kd_unitkerja = NULL;
			$this->kd_subunitkerja = NULL;
			$this->atasan_id = 0;

			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 3;
				$this->atasan_id = $this->kd_skpd;
			}

			if($this->jenis_jabatan == 'Fungsional Tertentu'){
				$this->atasan_id = $this->kd_skpd;
			}
		}

		// print_r($this); exit;
		// insert data
		$this->db->insert('jabatan_tbl', $this);
	}


	public function createAndGetLastId($data) {
		$this->kd_jabatan = $data['kd_jabatan'];
		$this->nama_jabatan = $data['nama_jabatan'];
		$this->jenis_jabatan = $data['jenis_jabatan'];

		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		$this->kd_unitkerja = $data['kd_unitkerja'];
		$this->kd_subunitkerja = $data['kd_subunitkerja'];
		// $this->position = $data['position'];

		if($data['position'] != NULL){
			$this->position = $data['position'];
		}

		// Fungsional Tertentu
		// Fungsional Umum

		if($this->kd_skpd != NULL && $this->kd_unitorganisasi != NULL && $this->kd_unitkerja != NULL && $this->kd_subunitkerja != NULL){
			$this->level = 5;
			$this->atasan_id = $this->kd_unitkerja;
			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 6;
				$this->atasan_id = $this->kd_subunitkerja;
			}

		}else if($this->kd_skpd != NULL && $this->kd_unitorganisasi != NULL && $this->kd_unitkerja != NULL && $this->kd_subunitkerja == NULL){
			$this->kd_subunitkerja = NULL;
			$this->level = 4;
			$this->atasan_id = $this->kd_unitorganisasi;

			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 5;
				$this->atasan_id = $this->kd_unitkerja;
			}

		}else if($this->kd_skpd != NULL && $this->kd_unitorganisasi != NULL && $this->kd_unitkerja == NULL && $this->kd_subunitkerja == NULL){
			$this->kd_unitkerja = NULL;
			$this->kd_subunitkerja = NULL;
			$this->level = 3;
			$this->atasan_id = $this->kd_skpd;

			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 4;
				$this->atasan_id = $this->kd_unitorganisasi;
			}

		}else if($this->kd_skpd != NULL && $this->kd_unitorganisasi == NULL && $this->kd_unitkerja == NULL && $this->kd_subunitkerja == NULL){
			$this->level = 2;
			$this->kd_unitorganisasi = NULL;
			$this->kd_unitkerja = NULL;
			$this->kd_subunitkerja = NULL;
			$this->atasan_id = 0;

			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 3;
				$this->atasan_id = $this->kd_skpd;
			}
		}

		// print_r($this); exit;
		// insert data
		$this->db->insert('jabatan_tbl', $this);

		$insertId = $this->db->insert_id();


   		return  $insertId;
	}

	public function update($data) {
		// get data
		$this->nama_jabatan = $data['nama_jabatan'];
		$this->jenis_jabatan = $data['jenis_jabatan'];

		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_unitorganisasi = $data['kd_unitorganisasi'];
		$this->kd_unitkerja = $data['kd_unitkerja'];
		$this->kd_subunitkerja = $data['kd_subunitkerja'];

		// $this->position = $data['position'];

		if($data['position'] != NULL){
			$this->position = $data['position'];
		}

		// Fungsional Tertentu
		// Fungsional Umum

		if($this->kd_skpd != NULL && $this->kd_unitorganisasi != NULL && $this->kd_unitkerja != NULL && $this->kd_subunitkerja != NULL){
			$this->level = 5;
			$this->atasan_id = $this->kd_unitkerja;
			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 6;
				$this->atasan_id = $this->kd_subunitkerja;
			}

		}else if($this->kd_skpd != NULL && $this->kd_unitorganisasi != NULL && $this->kd_unitkerja != NULL && $this->kd_subunitkerja == NULL){
			$this->kd_subunitkerja = NULL;
			$this->level = 4;
			$this->atasan_id = $this->kd_unitorganisasi;

			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 5;
				$this->atasan_id = $this->kd_unitkerja;
			}

		}else if($this->kd_skpd != NULL && $this->kd_unitorganisasi != NULL && $this->kd_unitkerja == NULL && $this->kd_subunitkerja == NULL){
			$this->kd_unitkerja = NULL;
			$this->kd_subunitkerja = NULL;
			$this->level = 3;
			$this->atasan_id = $this->kd_skpd;

			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 4;
				$this->atasan_id = $this->kd_unitorganisasi;
			}

		}else if($this->kd_skpd != NULL && $this->kd_unitorganisasi == NULL && $this->kd_unitkerja == NULL && $this->kd_subunitkerja == NULL){
			$this->level = 2;
			$this->kd_unitorganisasi = NULL;
			$this->kd_unitkerja = NULL;
			$this->kd_subunitkerja = NULL;
			$this->atasan_id = 0;

			if($this->jenis_jabatan == 'Fungsional Umum'){
				$this->level = 3;
				$this->atasan_id = $this->kd_skpd;
			}

			if($this->jenis_jabatan == 'Fungsional Tertentu'){
				$this->atasan_id = $this->kd_skpd;
			}
		}

		// print_r($this);exit;

		// update data
		$this->db->update ('jabatan_tbl', $this, array ('kd_jabatan' => $data['kd_jabatan']));
	}

	public function delete($id) {
		$this->db->delete ('jabatan_tbl', array ('kd_jabatan' => $id));
	}

	public function search_count($column, $data){
		$this->db->where($column,$data);
		return  $this->db->count_all("jabatan_tbl where $column like '%$data%'" );
	}

	public function search($column,$value, $limit, $start){

		// $this->db->select ('*');
		// $this->db->from ('jabatan_tbl');
		$this->db->select ('a.*, s.nama as skpd');
		$this->db->from ('jabatan_tbl a');
		$this->db->join('skpd_tbl s','a.kd_skpd = s.kd_skpd','left');

		$this->db->order_by ('kd_jabatan DESC');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_count_new($column, $data){

		if(count($column) > 1 && count($data) > 1){

			$this->db->where($column[0],$data[0]);
			$this->db->where($column[1],$data[1]);
			$keyword = urldecode($data[1]);

			return  $this->db->count_all("jabatan_tbl where $column[0] = '$data[0]' AND $column[1] like '%$keyword%'" );
		}else{
			$keyword = urldecode($data);
			$this->db->where($column,$keyword);
			return  $this->db->count_all("jabatan_tbl where $column like '%$keyword%'" );
		}


	}

	public function search_new($column,$value, $limit, $start){

		// $this->db->select ('a.*, s.nama as skpd');
		// $this->db->from ('jabatan_tbl a');
		// $this->db->join('skpd_tbl s','a.kd_skpd = s.kd_skpd','left');

		$this->db->select ('a.*, s.nama as skpd, b.nama as unit_organisasi, c.nama as unit_kerja, d.nama as sub_unit_kerja');
		$this->db->from ('jabatan_tbl a');
		$this->db->join('skpd_tbl s','a.kd_skpd = s.kd_skpd','left');
		$this->db->join('unitorganisasi_tbl b','a.kd_unitorganisasi = b.kd_unitorganisasi','left');
		$this->db->join('unitkerja_tbl c','a.kd_unitkerja = c.kd_unitkerja','left');
		$this->db->join('subunitkerja_tbl d','a.kd_subunitkerja = d.kd_subunitkerja','left');

		$this->db->order_by ('kd_jabatan DESC');

		if(count($column) > 1 && count($value) > 1){

			$this->db->where('a.'.$column[0],$value[0]);
			$this->db->like('a.'.$column[1],urldecode($value[1]));
		}else{
			$this->db->like('a.'.$column,urldecode($value));
		}

		$this->db->limit ($limit, $start);

		// print_r($this->db);exit;

		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function generatekd_jabatan(){
		return $this->db->query("SELECT kd_jabatan FROM jabatan_tbl order by kd_jabatan desc limit 1")->row()->kd_jabatan+1;
	}

	public function ambil_jabatan_by_skpd($kd_skpd){
		if($kd_skpd == NULL){
			$kd_skpd = '-';
		}
		$this->db->where('kd_skpd',$kd_skpd);
		$this->db->where('kd_unitorganisasi',NULL);
		$this->db->order_by('nama_jabatan','asc');
		$unit_organisasi=$this->db->get('jabatan_tbl');

		if($unit_organisasi->num_rows()>0){

			foreach ($unit_organisasi->result_array() as $row)
			{
				$result['']= '- Pilih Jabatan -';
				$result[$row['kd_jabatan']]= $row['nama_jabatan'];
			}
			} else {
			   $result['']= '- Data Jabatan Belum Ada -';
			}
			return $result;
	}

	public function ambil_jabatan_by_unitorganisasi($kd_unitorganisasi){
		$this->db->where('kd_unitorganisasi',$kd_unitorganisasi);
		$this->db->where('kd_unitkerja',NULL);
		$this->db->order_by('nama_jabatan','asc');
		$unit_organisasi=$this->db->get('jabatan_tbl');
		if($unit_organisasi->num_rows()>0){

			foreach ($unit_organisasi->result_array() as $row)
			{
				$result['']= '- Pilih Jabatan -';
				$result[$row['kd_jabatan']]= $row['nama_jabatan'];
			}
			} else {
			   $result['']= '- Data Jabatan Belum Ada -';
			}
			return $result;
	}

	public function ambil_jabatan_by_unitkerja($kd_unitkerja){
		$this->db->where('kd_unitkerja',$kd_unitkerja);
		$this->db->where('kd_subunitkerja',NULL);
		$this->db->order_by('nama_jabatan','asc');
		$unit_organisasi=$this->db->get('jabatan_tbl');
		if($unit_organisasi->num_rows()>0){

			foreach ($unit_organisasi->result_array() as $row)
			{
				$result['']= '- Pilih Jabatan -';
				$result[$row['kd_jabatan']]= $row['nama_jabatan'];
			}
			} else {
			   $result['']= '- Data Jabatan Belum Ada -';
			}
			return $result;
	}

	public function ambil_jabatan_by_subunitkerja($kd_subunitkerja){
		$this->db->where('kd_subunitkerja',$kd_subunitkerja);
		$this->db->order_by('nama_jabatan','asc');
		$unit_organisasi=$this->db->get('jabatan_tbl');
		if($unit_organisasi->num_rows()>0){

			foreach ($unit_organisasi->result_array() as $row)
			{
				$result['']= '- Pilih Jabatan -';
				$result[$row['kd_jabatan']]= $row['nama_jabatan'];
			}
			} else {
			   $result['']= '- Data Jabatan Belum Ada -';
			}
			return $result;
	}

	public function getListPetaJabatanOOOOOOOOO($start_level, $end_level) {

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja, jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai, max(rk.golongan) as golongan, rj.tmt as tmt');

		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');
		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');

		//--- tambahan
		// $this->db->where('rj.seq_no = (select max(seq_no) from riwayat_jabatan_tbl AS rj2 WHERE rj2.kd_jabatan = jb.kd_jabatan)');
		//------ end
		$this->db->where('jb.level >', $start_level);
		$this->db->where('jb.level <=', $end_level);
		// $this->db->where('p.status_pegawai = "aktif" OR p.status_pegawai = ""');

		// $this->db->order_by('level', 'ASC');

		$this->db->group_by('jb.kd_jabatan');
		// $this->db->order_by('position','ASC');
		$this->db->order_by('position ASC, nama_jabatan ASC');
		$query = $this->db->get('jabatan_tbl jb');

		// print_r($query->result());exit;

		$newItems = array();

		foreach($query->result() as $hasil){

			if($hasil->nip != null){
				$this->db->select('a.kd_jabatan as kd_jabatan');
				$this->db->where('a.nip', $hasil->nip);
				$this->db->order_by('a.tmt desc');
				$result = $this->db->get('riwayat_jabatan_tbl a');



				// print_r($result->row());exit;
				if($result->row()->kd_jabatan == $hasil->kd_jabatan){
					$newItems[] = $hasil;
				}else{

					$hasil->nip = null;
					$hasil->nama = null;
					$hasil->gelar_depan = null;
					$hasil->gelar_belakang = null;
					$hasil->status_pegawai = null;
					$hasil->golongan = null;
					$hasil->tmt = null;
					$hasil->seq_no = null;

					if($jab->level < 5){
						$newItems[] = $hasil;
					}
				}
			}else{
				$newItems[] = $hasil;
			}
		}

		if(count($newItems) == 1){
			if($kd_jabatan == $newItems[0]->kd_jabatan && $newItems[0]->nip == NULL){
				return false;
			}
		}

		return $newItems;

	}


	public function getListPetaJabatan($start_level, $end_level) {

		if($this->session->userdata('group_id')=='3') {

			$query = $this->db->query("SELECT jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja, jb.atasan_id, jb.level FROM jabatan_tbl jb WHERE jb.level > 0 AND jb.level <= 2 AND (jb.kd_skpd IS NULL OR jb.kd_skpd = '".$this->session->userdata("ss_skpd")."')  GROUP BY jb.kd_jabatan ORDER BY jb.position ASC, jb.nama_jabatan ASC");

		} else {

			$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja,
			 jb.kd_subunitkerja, jb.atasan_id, jb.level');

			$this->db->where('jb.level >', $start_level);
			$this->db->where('jb.level <=', $end_level);

			$this->db->group_by('jb.kd_jabatan');
			// $this->db->order_by('position','ASC');
			$this->db->order_by('jb.position ASC, jb.nama_jabatan ASC');
			$query = $this->db->get('jabatan_tbl jb');

		}

		$newItems = array();

		foreach($query->result() as $hasil){

		 $sql="SELECT rj.seq_no , p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai,
				max(rk.golongan) as golongan, rj.tmt as tmt
				FROM riwayat_jabatan_tbl as rj
				LEFT JOIN pegawai_tbl as p ON rj.nip = p.nip
				LEFT JOIN riwayat_kepangkatan_tbl as rk ON p.nip = rk.nip";
		$sql.=" WHERE rj.kd_jabatan = ?
				AND rj.tmt = (select max(tmt) from riwayat_jabatan_tbl as rjt WHERE rjt.kd_jabatan = ?)
				";
		if($this->session->userdata('group_id')=='3') {
				$sql.=" AND rj.kd_skpd = ".$this->session->userdata("ss_skpd");
			}

   		$qq = $this->db->query($sql, array($hasil->kd_jabatan, $hasil->kd_jabatan));

			$hasil->seq_no = $qq->row()->seq_no;
			$hasil->nama = $qq->row()->nama;
			$hasil->nip = $qq->row()->nip;
			$hasil->gelar_depan = $qq->row()->gelar_depan;
			$hasil->gelar_belakang = $qq->row()->gelar_belakang;
			$hasil->status_pegawai = $qq->row()->status_pegawai;
			$hasil->golongan = $qq->row()->golongan;
			$hasil->tmt = $qq->row()->tmt;

			$newItems[] = $hasil;

		}

		if(count($newItems) == 1){
			if($hasil->kd_jabatan == $newItems[0]->kd_jabatan && $newItems[0]->nip == NULL){
				return false;
			}
		}

		return $newItems;
	}

	public function getListPetaJabatanOld($start_level, $end_level) {

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja, jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai, max(rk.golongan) as golongan, rj.tmt as tmt');

		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');
		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');
		$this->db->where('jb.level >', $start_level);
		$this->db->where('jb.level <=', $end_level);
		// $this->db->where('p.status_pegawai =', 'aktif');

		// $this->db->order_by('level', 'ASC');

		$this->db->group_by('jb.kd_jabatan');
		// $this->db->order_by('position','ASC');
		$this->db->order_by('position ASC, nama_jabatan ASC');
		$query = $this->db->get('jabatan_tbl jb');

		if ($query->num_rows()> 0) {

			return $query;
		}
		return false;
	}


	public function getListPetaJabatanByKodeJabatan($kd_jabatan) {

		$jab = $this->fetchByIdSingle($kd_jabatan);
		if($jab == NULL){
			return false;
		}

		$newItems = array();
		if($jab->jenis_jabatan == 'Struktural'){

			$newItems = $this->parsingPetajabatanStruktural($jab);
 		}else{

			$newItems = $this->parsingPetajabatanNonStruktural($jab);
 		}

		return $newItems;
	}

	public function parsingPetajabatanStruktural($jab){

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja,
		 jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai, max(rk.golongan) as golongan,
		 rj.tmt as tmt, rj.seq_no as seq_no');

		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan AND rj.tmt = (select max(aa.tmt) FROM riwayat_jabatan_tbl aa WHERE aa.kd_jabatan = jb.kd_jabatan)','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');
		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');

		if($jab->level == 2 ){
			$this->db->where('jb.atasan_id', $jab->kd_skpd);
		}else if($jab->level == 3){
			$this->db->where('jb.atasan_id', $jab->kd_unitorganisasi);
		}else if($jab->level == 4){
			$this->db->where('jb.atasan_id', $jab->kd_unitkerja);
		}else if($jab->level == 5){
			$this->db->where('jb.atasan_id', $jab->kd_subunitkerja);
		}

		if($this->session->userdata('group_id')=='3') {
			$this->db->where('jb.kd_skpd', $this->session->userdata("ss_skpd"));
		}

		$this->db->where('jb.level >', $jab->level);
		$this->db->group_by('jb.kd_jabatan');
		$this->db->order_by('position ASC, nama_jabatan ASC');

		$query = $this->db->get('jabatan_tbl jb');

		// print_r($query->result());exit;

		$newItems = array();

		foreach($query->result() as $hasil){

			if($hasil->nip != null){
				$this->db->select('a.kd_jabatan as kd_jabatan');
				$this->db->where('a.nip', $hasil->nip);
				$this->db->order_by('a.tmt desc');
				$result = $this->db->get('riwayat_jabatan_tbl a');

				// print_r($result->row());exit;
				if($result->row()->kd_jabatan == $hasil->kd_jabatan){
					$newItems[] = $hasil;
				}else{

					$hasil->nip = null;
					$hasil->nama = null;
					$hasil->gelar_depan = null;
					$hasil->gelar_belakang = null;
					$hasil->status_pegawai = null;
					$hasil->golongan = null;
					$hasil->tmt = null;
					$hasil->seq_no = null;

					if($jab->level < 5){
						$newItems[] = $hasil;
					}
				}
			}else{
				$newItems[] = $hasil;
			}
		}

	 		if(count($newItems) == 1){
	 			if($jab->kd_jabatan == $newItems[0]->kd_jabatan && $newItems[0]->nip == NULL){
	 				return false;
	 			}
	 		}

	 		return $newItems;

	}

	public function parsingPetajabatanStruktural000000000($jab){

			$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja,
			 jb.kd_subunitkerja, jb.atasan_id, jb.level');

		 //tambahan
		 // $this->db->where('rj.seq_no = (select max(seq_no) from riwayat_jabatan_tbl AS rj2 WHERE rj2.kd_jabatan = jb.kd_jabatan)');

		 if($jab->level == 2 ){
			 $this->db->where('jb.atasan_id', $jab->kd_skpd);
		 }else if($jab->level == 3){
			 $this->db->where('jb.atasan_id', $jab->kd_unitorganisasi);
		 }else if($jab->level == 4){
			 $this->db->where('jb.atasan_id', $jab->kd_unitkerja);
		 }else if($jab->level == 5){
			 $this->db->where('jb.atasan_id', $jab->kd_subunitkerja);
		 }

		 $this->db->where('jb.level >', $jab->level);
		 $this->db->group_by('jb.kd_jabatan');

		 // $this->db->order_by('position','ASC');
  		$this->db->order_by('position ASC, nama_jabatan ASC');

  		$query = $this->db->get('jabatan_tbl jb');

			// print_r($query->result());exit;

 			$newItems = array();

	 		foreach($query->result() as $hasil){

	 		 $sql="SELECT rj.seq_no , p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai,
	 				max(rk.golongan) as golongan, rj.tmt as tmt, rj.kd_jabatan as kd_jabatan
	 				FROM riwayat_jabatan_tbl as rj
	 				LEFT JOIN pegawai_tbl as p ON rj.nip = p.nip
	 				LEFT JOIN riwayat_kepangkatan_tbl as rk ON p.nip = rk.nip
	 				WHERE rj.kd_jabatan = ?
	 				AND rj.seq_no = (select max(seq_no) from riwayat_jabatan_tbl as rjt WHERE rjt.kd_jabatan = ?)";

	 			$qq = $this->db->query($sql, array($hasil->kd_jabatan, $hasil->kd_jabatan));

				// print_r($qq->row());exit;

				if($qq->row()->kd_jabatan != $hasil->kd_jabatan){
					$hasil->nip = null;
					$hasil->nama = null;
					$hasil->gelar_depan = null;
					$hasil->gelar_belakang = null;
					$hasil->status_pegawai = null;
					$hasil->golongan = null;
					$hasil->tmt = null;
					$hasil->seq_no = null;
				}else{

					$hasil->seq_no = $qq->row()->seq_no;
		 			$hasil->nama = $qq->row()->nama;
		 			$hasil->nip = $qq->row()->nip;
		 			$hasil->gelar_depan = $qq->row()->gelar_depan;
		 			$hasil->gelar_belakang = $qq->row()->gelar_belakang;
		 			$hasil->status_pegawai = $qq->row()->status_pegawai;
		 			$hasil->golongan = $qq->row()->golongan;
		 			$hasil->tmt = $qq->row()->tmt;
				}

	 			$newItems[] = $hasil;

	 		}

	 		// if(count($newItems) == 1){
	 		// 	if($kd_jabatan == $newItems[0]->kd_jabatan && $newItems[0]->nip == NULL){
	 		// 		return false;
	 		// 	}
	 		// }

	 		return $newItems;

	}

	public function parsingPetajabatanNonStruktural($jab){

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja,
		 jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai, max(rk.golongan) as golongan,
		 rj.tmt as tmt, rj.seq_no as seq_no');

		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');
		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');

		$this->db->where('jb.kd_jabatan', $jab->kd_jabatan);

		if($this->session->userdata('group_id')=='3') {
			$this->db->where('jb.kd_skpd', $this->session->userdata("ss_skpd"));
		}

		$this->db->group_by('p.nip');

		$this->db->order_by('position ASC, nama_jabatan ASC');

		$query = $this->db->get('jabatan_tbl jb');

		// print_r($query->result());exit;

		$newItems = array();

		foreach($query->result() as $hasil){

			if($hasil->nip != null){
				$this->db->select('a.kd_jabatan as kd_jabatan');
				$this->db->where('a.nip', $hasil->nip);
				$this->db->order_by('a.tmt desc');
				$result = $this->db->get('riwayat_jabatan_tbl a');

				// print_r($result->row());exit;
				if($result->row()->kd_jabatan == $hasil->kd_jabatan){
					if($hasil->status_pegawai == 'Aktif' || $hasil->status_pegawai == ''){
							$newItems[] = $hasil;
					}

				}else{

					$hasil->nip = null;
					$hasil->nama = null;
					$hasil->gelar_depan = null;
					$hasil->gelar_belakang = null;
					$hasil->status_pegawai = null;
					$hasil->golongan = null;
					$hasil->tmt = null;
					$hasil->seq_no = null;

					if($jab->level < 5){
						$newItems[] = $hasil;
					}
				}
			}else{
				$newItems[] = $hasil;
			}
		}

		if(count($newItems) == 1){
			if($jab->kd_jabatan == $newItems[0]->kd_jabatan && $newItems[0]->nip == NULL){
				return false;
			}
		}

		// print($newItems);exit;

		return $newItems;
	}

	public function getListPetaJabatanByKodeJabatan000000000000($kd_jabatan) {

		$jab = $this->fetchByIdSingle($kd_jabatan);
		if($jab == NULL){
			return false;
		}

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja,
		 jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai, max(rk.golongan) as golongan,
		 rj.tmt as tmt, rj.seq_no as seq_no');

		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');
		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');

		if($jab->jenis_jabatan == 'Struktural'){
			//tambahan
			// $this->db->where('rj.seq_no = (select max(seq_no) from riwayat_jabatan_tbl AS rj2 WHERE rj2.kd_jabatan = jb.kd_jabatan)');

			if($jab->level == 2 ){
				$this->db->where('jb.atasan_id', $jab->kd_skpd);
			}else if($jab->level == 3){
				$this->db->where('jb.atasan_id', $jab->kd_unitorganisasi);
			}else if($jab->level == 4){
				$this->db->where('jb.atasan_id', $jab->kd_unitkerja);
			}else if($jab->level == 5){
				$this->db->where('jb.atasan_id', $jab->kd_subunitkerja);
			}

			$this->db->where('jb.level >', $jab->level);
			$this->db->group_by('jb.kd_jabatan');
		}else{
			// $this->db->where('p.status_pegawai = "Aktif" OR p.status_pegawai=""');
			$this->db->where('jb.kd_jabatan', $jab->kd_jabatan); // asli 00000000

			//asli
			$this->db->group_by('p.nip');
		}

		// $this->db->order_by('position','ASC');
		$this->db->order_by('position ASC, nama_jabatan ASC');

		$query = $this->db->get('jabatan_tbl jb');

		// print_r($query->result());exit;

		$newItems = array();

		foreach($query->result() as $hasil){

			if($hasil->nip != null){
				$this->db->select('a.kd_jabatan as kd_jabatan');
				$this->db->where('a.nip', $hasil->nip);
				$this->db->order_by('a.tmt desc');
				$result = $this->db->get('riwayat_jabatan_tbl a');

				// print_r($result->row());exit;
				if($result->row()->kd_jabatan == $hasil->kd_jabatan){
					$newItems[] = $hasil;
				}else{

					$hasil->nip = null;
					$hasil->nama = null;
					$hasil->gelar_depan = null;
					$hasil->gelar_belakang = null;
					$hasil->status_pegawai = null;
					$hasil->golongan = null;
					$hasil->tmt = null;
					$hasil->seq_no = null;

					if($jab->level < 5){
						$newItems[] = $hasil;
					}
				}
			}else{
				$newItems[] = $hasil;
			}
		}

		if(count($newItems) == 1){
			if($kd_jabatan == $newItems[0]->kd_jabatan && $newItems[0]->nip == NULL){
				return false;
			}
		}

		return $newItems;

	}


	public function getListPetaJabatanByKodeJabatanOld($kd_jabatan) {

		$jab = $this->fetchByIdSingle($kd_jabatan);
		if($jab == NULL){
			return false;
		}

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja, jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai, max(rk.golongan) as golongan, rj.tmt as tmt');

		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');
		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');

		if($jab->jenis_jabatan == 'Struktural'){
			if($jab->level == 2 ){
				$this->db->where('jb.atasan_id', $jab->kd_skpd);
			}else if($jab->level == 3){
				$this->db->where('jb.atasan_id', $jab->kd_unitorganisasi);
			}else if($jab->level == 4){
				$this->db->where('jb.atasan_id', $jab->kd_unitkerja);
			}else if($jab->level == 5){
				$this->db->where('jb.atasan_id', $jab->kd_subunitkerja);
			}

			$this->db->where('jb.level >', $jab->level);
			$this->db->group_by('jb.kd_jabatan');
		}else{

			$this->db->where('jb.kd_jabatan', $jab->kd_jabatan);

			$this->db->group_by('p.nip');
		}


		// $this->db->order_by('position','ASC');
		$this->db->order_by('position ASC, nama_jabatan ASC');

		$query = $this->db->get('jabatan_tbl jb');

		if ($query->num_rows() == 0) {

			return false;
		}elseif ($query->num_rows() == 1) {
			$row = $query->row();
			if($kd_jabatan == $row->kd_jabatan && $row->nip == NULL){
				return false;
			}else{
				return $query;
			}
		}else{
			return $query;
		}
		// return false;
	}

	public function getListBaganPetaJabatanBySKPD($kd_skpd) {

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja, jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai,  max(rk.golongan) as golongan, rj.tmt as tmt');

		// $this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan','left');
		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan AND rj.tmt = (select max(aa.tmt) FROM riwayat_jabatan_tbl aa WHERE aa.kd_jabatan = jb.kd_jabatan)','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');

		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');

		// $this->db->where('rj.seq_no = (select max(seq_no) from riwayat_jabatan_tbl AS rj2 WHERE rj2.kd_jabatan = jb.kd_jabatan)'); // tambahan

		$this->db->where('jb.kd_skpd', $kd_skpd);
		$this->db->where('jb.jenis_jabatan', 'Struktural');

		// $this->db->where('p.status_pegawai', 'Aktif');

		$this->db->group_by('jb.kd_jabatan');

		$this->db->order_by('jb.position asc, nama_jabatan ASC');

		$query = $this->db->get('jabatan_tbl jb');

		// print_r($query->result());exit;

		$newItems = array();

		foreach($query->result() as $hasil){

			if($hasil->status_pegawai == 'Aktif' || $hasil->status_pegawai == '' && $hasil->nip != null){
				$newItems[] = $hasil;
			}else{
				$hasil->nama = '';
				$hasil->nip = '';
				$hasil->gelar_depan = '';
				$hasil->gelar_belakang = '';
				$hasil->status_pegawai = '';
				$hasil->golongan = '';
				$hasil->tmt = '';

				$newItems[] = $hasil;

			}
		}

		if(count($newItems) > 0){
			return $newItems;
		}else{
			return false;
		}

	}


	public function getListBaganPetaJabatanCustom($params) {

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja, jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai,  max(rk.golongan) as golongan, rj.tmt as tmt');

		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');

		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');

		if($params['kd_skpd'] != NULL){
			$this->db->where('jb.kd_skpd', $params['kd_skpd']);
		}

		if($params['kd_unitorganisasi'] != NULL){
			$this->db->where('jb.kd_unitorganisasi', $params['kd_unitorganisasi']);
		}

		if($params['kd_unitkerja'] != NULL){
			$this->db->where('jb.kd_unitkerja', $params['kd_unitkerja']);
		}


		$this->db->where('jb.jenis_jabatan', 'Struktural');

		$this->db->group_by('jb.kd_jabatan');

		$this->db->order_by('jb.position asc, nama_jabatan ASC');

		$query = $this->db->get('jabatan_tbl jb');

		if ($query->num_rows()> 0) {

			return $query;
		}
		return false;
	}


	public function searchPegawai($column,$value, $limit){

		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang,
		p.tempat_lahir, p.tgl_lahir, p.telp,
		(case p.kelamin when "1" then "Pria" when "2" then "Wanita" end) kelamin ,
		(case p.agama when "1" then "Islam" when "2" then "Katolik" when "3" then "Hindu" when "4" then "Budha" when "5" then "Sinto" when "6" then "Konghucu" when "7" then "Protestan" end) agama,
		(case p.gol_darah when "1" then "A" when "2" then "B" when "3" then "AB" else "O" end) gol_darah,
		(case p.status when "1" then "Lajang" when "2" then "Nikah" else "Cerai" end) status, p.status_pegawai,
		p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos');
		$this->db->from ('pegawai_tbl p ');
		$this->db->where ("p.status_pegawai != 'Pindah Tugas Keluar'");
		$this->db->like($column,$value);
		$this->db->limit ($limit);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function searchPegawai_per_skpd($kd_skpd, $column,$value, $limit){
		$query = $this->db->query("
			SELECT p.nip, ifnull(p.nip_lama,'-') nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang,
		p.tempat_lahir, p.tgl_lahir, p.telp,
		(case p.kelamin when '1' then 'Pria' when '2' then 'Wanita' end) kelamin ,
		(case p.agama when '1' then 'Islam' when '2' then 'Katolik' when '3' then 'Hindu' when '4' then 'Budha' when '5' then 'Sinto' when '6' then 'Konghucu' when '7' then 'Protestan' end) agama,
		(case p.gol_darah when '1' then 'A' when '2' then 'B' when '3' then 'AB' else 'O' end) gol_darah,
		(case p.status when '1' then 'Lajang' when '2' then 'Nikah' else 'Cerai' end) status, p.status_pegawai,
		p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			
			WHERE 
			p.nip = b.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$kd_skpd."' AND
			p.$column LIKE '%".$value."%'

			GROUP BY p.nip , c.kd_skpd
			LIMIT $limit");

		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getRootJabatanFromSkpd($kd_skpd){

		#Create where clause
		$this->db->select_min('a.level');
		$this->db->where('a.kd_skpd', $kd_skpd);
		$this->db->from('jabatan_tbl a');
		$where_clause = $this->db->get_compiled_select();

		#Create main query
		$this->db->select ('jb.*');
		$this->db->where('jb.kd_skpd',$kd_skpd);

		$this->db->where("jb.level = ($where_clause)", NULL, FALSE);
		$query = $this->db->get('jabatan_tbl jb');
        return $query->row();
	}


	public function getJabatanByKodeSkpdAndLevel($kd_skpd, $start_level,$end_level) {

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja, jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang, p.status_pegawai as status_pegawai,max(rk.golongan) as golongan, rj.tmt as tmt');

		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');
		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');

		//-------------------------------------------------------
		$this->db->where('rj.seq_no = (select max(seq_no) from riwayat_jabatan_tbl AS rj2 WHERE rj2.kd_jabatan = jb.kd_jabatan)'); // tambahan
		//-------------------------------------------------------

		$this->db->where('jb.level >', $start_level);
		$this->db->where('jb.level <=', $end_level);
		$this->db->where('jb.kd_skpd', $kd_skpd);
		// $this->db->order_by('level', 'ASC');

		$this->db->group_by('jb.kd_jabatan');
		// $this->db->order_by('position','ASC');
		$this->db->order_by('position ASC, nama_jabatan ASC');
		$query = $this->db->get('jabatan_tbl jb');

		// print_r($query->result());exit;


		$newItems = array();

		foreach($query->result() as $hasil){

			if($hasil->nip != null){
				$this->db->select('a.kd_jabatan as kd_jabatan');
				$this->db->where('a.nip', $hasil->nip);
				$this->db->order_by('a.tmt desc');
				$result = $this->db->get('riwayat_jabatan_tbl a');

				// print_r($result->row());exit;
				if($result->row()->kd_jabatan == $hasil->kd_jabatan){
					$newItems[] = $hasil;
				}else{

					$hasil->nip = null;
					$hasil->nama = null;
					$hasil->gelar_depan = null;
					$hasil->gelar_belakang = null;
					$hasil->status_pegawai = null;
					$hasil->golongan = null;
					$hasil->tmt = null;
					$hasil->seq_no = null;

					// print_r($hasil);exit;

					$newItems[] = $hasil;
				}
			}else{
				$newItems[] = $hasil;
			}
		}

		if(count($newItems) == 1){
			if($kd_jabatan == $newItems[0]->kd_jabatan && $newItems[0]->nip == NULL){
				return false;
			}
		}

		return $newItems;

	}


	public function getJabatanByKodeSkpdAndLevelNew($kd_skpd, $start_level,$riwayat) {

		$jab = $this->fetchByIdSingle($riwayat->kd_jabatan);
		$end_level = $jab->level;

		$this->db->select('jb.kd_jabatan, jb.nama_jabatan, jb.jenis_jabatan, jb.kd_skpd, jb.kd_unitorganisasi, jb.kd_unitkerja, jb.kd_subunitkerja, jb.atasan_id, jb.level, p.nama, p.nip, p.gelar_depan, p.gelar_belakang,p.status_pegawai as status_pegawai, max(rk.golongan) as golongan, rj.tmt as tmt');

		$this->db->join('riwayat_jabatan_tbl rj','jb.kd_jabatan = rj.kd_jabatan','left');
		$this->db->join('pegawai_tbl p','rj.nip = p.nip','left');
		$this->db->join('riwayat_kepangkatan_tbl rk','p.nip = rk.nip','left');

		if($end_level == 4){
			$this->db->where('jb.kd_unitorganisasi', $jab->kd_unitorganisasi);
		}

		if($end_level == 5){
			$this->db->where('jb.kd_unitkerja', $jab->kd_unitkerja);
		}

		$this->db->where('jb.level >', $start_level);
		$this->db->where('jb.level <=', $end_level);
		$this->db->where('jb.kd_skpd', $kd_skpd);



		// $this->db->order_by('level', 'ASC');

		$this->db->group_by('jb.kd_jabatan');
		// $this->db->order_by('position','ASC');
		$this->db->order_by('position ASC, nama_jabatan ASC');
		$query = $this->db->get('jabatan_tbl jb');

		// print_r($query->result());exit;

		if ($query->num_rows()> 0) {

			return $query;
		}
		return false;
	}


	public function get_auto_nama_jabatan($jenis_jabatan,$kd_skpd,$kd_unitorganisasi,$kd_unitkerja,$kd_subunitkerja,$s){
		$this->db->select('kd_jabatan, nama_jabatan');

		$this->db->where('jenis_jabatan',$jenis_jabatan);

		$this->db->where('kd_skpd',$kd_skpd);
		if($kd_unitorganisasi != NULL){
			$this->db->where('kd_unitorganisasi',$kd_unitorganisasi);
		}
		if($kd_unitkerja != NULL){
			$this->db->where('kd_unitkerja',$kd_unitkerja);
		}
		if($kd_subunitkerja != NULL){
			$this->db->where('kd_subunitkerja',$kd_subunitkerja);
		}

		$this->db->like('nama_jabatan', $s);

		$this->db->order_by('nama_jabatan','asc');
		$jabatan=$this->db->get('jabatan_tbl');


		return $jabatan->result_array();
	}

	public function getSingleJabatanByColumn($data){

		$this->db->select('*');

		if(isset($data['kd_skpd'])){
			$this->db->where('kd_skpd',$data['kd_skpd']);
		}
		if(isset($data['kd_unitorganisasi'])){
			$this->db->where('kd_unitorganisasi',$data['kd_unitorganisasi']);
		}
		if(isset($data['kd_unitkerja'])){
			$this->db->where('kd_unitkerja',$data['kd_unitkerja']);
		}
		if(isset($data['kd_subunitkerja'])){
			$this->db->where('kd_subunitkerja',$data['kd_subunitkerja']);
		}

		$query = $this->db->get('jabatan_tbl');

		return $query->row();
	}


	public function get_rekom_position($data){

		$jab = $this->getSingleJabatanByColumn($data);

		$this->db->select_max('position');

		if($jab->kd_skpd != NULL){
			$where = "kd_skpd='$jab->kd_skpd' AND kd_unitorganisasi IS NULL";
		}
		if($jab->kd_unitorganisasi != NULL){
			$where = "kd_skpd='$jab->kd_skpd' AND kd_unitorganisasi='$jab->kd_unitorganisasi' AND kd_unitkerja IS NULL";
		}
		if($jab->kd_unitkerja != NULL){
			$where = "kd_skpd='$jab->kd_skpd' AND kd_unitorganisasi='$jab->kd_unitorganisasi' AND kd_unitkerja='$jab->kd_unitkerja' AND kd_subunitkerja IS NULL";
		}
		if($jab->kd_subunitkerja != NULL){
			$where = "kd_skpd='$jab->kd_skpd' AND kd_unitorganisasi='$jab->kd_unitorganisasi' AND kd_unitkerja='$jab->kd_unitkerja' AND kd_subunitkerja='$jab->kd_subunitkerja'";
		}

		$this->db->where($where);

		$query = $this->db->get('jabatan_tbl');

		return $query->row();

	}

}
