<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class daftar_pengajuan_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("daftar_pengajuan");
	}
	
	public function fetchAll($limit, $start) {
		$this->db->select ('*');
		$this->db->from ('daftar_pengajuan');
		$this->db->order_by ('id_daftar DESC');
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('*');
		$this->db->from ('daftar_pengajuan');
		$this->db->where('id_daftar', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function fetchByIdSingle($id){
		$this->db->select ('*');
		$this->db->from ('daftar_pengajuan');
		$this->db->where('id_daftar', $id);
		$query = $this->db->get()->row();
		return $query;
	}
	
	//--- untuk get skpd user ---//
	 public function fetchById2($id){
        $this->db->where('id_daftar', $id);
        $query = $this->db->get('daftar_pengajuan');
        return $query->result();
    }
	
	public function create($data) {
		$this->no_urut = $data['no_urut'];
		$this->nama_berkas = $data['nama_berkas'];
		$this->nama_file = $data['nama_file'];
		$this->kategori = $data['kategori'];
		$this->sub_kategori = $data['sub_kategori'];
		$this->date_created = date('Y-m-d H:i:s');
		
		// insert data
		$this->db->insert('daftar_pengajuan', $this);
	}

	public function update($data) {
		// get data
		$this->no_urut = $data['no_urut'];
		$this->nama_berkas = $data['nama_berkas'];
		$this->nama_file = $data['nama_file'];
		$this->kategori = $data['kategori'];
		$this->sub_kategori = $data['sub_kategori'];
		$this->date_updated = date('Y-m-d H:i:s');
		
		// update data
		$this->db->update ('daftar_pengajuan', $this, array ('id_daftar' => $data['id_daftar']));
	}
	
	public function delete($id) {
		$this->db->delete ('daftar_pengajuan', array ('id_daftar' => $id));
	}
	
	public function search_count($column, $data){
		return  $this->db->count_all("daftar_pengajuan where $column like '%$data%'");
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('*');
		$this->db->from ('daftar_pengajuan');
		$this->db->order_by ('id_daftar DESC');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
}