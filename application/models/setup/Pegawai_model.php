<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class pegawai_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("pegawai_tbl");
	}
	
	public function record_count_skpd($kd_skpd) {
		return $this->db->count_all("pegawai_tbl where kd_skpd='$kd_skpd'");
	}

	public function record_count_per_skpd($kd_skpd='') {
		$query = $this->db->query("
			SELECT p.nip
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			
			WHERE 
			(p.nip = b.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$kd_skpd."') OR (
			p.nip = b.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$kd_skpd."' AND
			c.kd_unitorganisasi IS NULL)

			GROUP BY p.nip , c.kd_skpd");

		return $query->num_rows();
	}
	
	public function fetchAll($limit, $start) {
		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, 
		(case p.kelamin when "1" then "Pria" when "2" then "Wanita" end) kelamin ,
		(case p.agama when "1" then "Islam" when "2" then "Katolik" when "3" then "Hindu" when "4" then "Budha" when "5" then "Sinto" when "6" then "Konghucu" when "7" then "Protestan" end) agama, 
		(case p.gol_darah when "1" then "A" when "2" then "B" when "3" then "AB" else "O" end) gol_darah,
		p.status, p.status_pegawai,
		p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos');
		$this->db->from ('pegawai_tbl p ');
		$this->db->where ("p.status_pegawai != 'Pindah Tugas Keluar'");
		$this->db->where ("p.status_pegawai != 'Pensiun'");
		$this->db->where ("p.status_pegawai != 'Meninggal'");
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchAll_skpd($kd_skpd, $limit, $start) {
		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, 
		(case p.kelamin when "1" then "Pria" when "2" then "Wanita" end) kelamin ,
		(case p.agama when "1" then "Islam" when "2" then "Katolik" when "3" then "Hindu" when "4" then "Budha" when "5" then "Sinto" when "6" then "Konghucu" when "7" then "Protestan" end) agama, 
		(case p.gol_darah when "1" then "A" when "2" then "B" when "3" then "AB" else "O" end) gol_darah,
		(case p.status when "1" then "Lajang" when "2" then "Nikah" else "Cerai" end) status, 
		p.status_pegawai, p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos');
		$this->db->where ('kd_skpd',$kd_skpd);
		$this->db->from ('pegawai_tbl p ');
		$this->db->where ("p.status_pegawai != 'Pindah Tugas Keluar'");
		$this->db->where ("p.status_pegawai != 'Pensiun'");
		$this->db->where ("p.status_pegawai != 'Meninggal'");
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function fetchAll_per_skpd($kd_skpd, $limit, $start) {
		$query = $this->db->query("
			SELECT p.nip, ifnull(p.nip_lama,'-') nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, 
		(case p.kelamin when '1' then 'Pria' when '2' then 'Wanita' end) kelamin ,
		(case p.agama when '1' then 'Islam' when '2' then 'Katolik' when '3' then 'Hindu' when '4' then 'Budha' when '5' then 'Sinto' when '6' then 'Konghucu' when '7' then 'Protestan' end) agama, 
		(case p.gol_darah when '1' then 'A' when '2' then 'B' when '3' then 'AB' else 'O' end) gol_darah,
		(case p.status when '1' then 'Lajang' when '2' then 'Nikah' else 'Cerai' end) status, 
		p.status_pegawai, p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			
			WHERE 
			(p.nip = b.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$kd_skpd."') OR (
			p.nip = b.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$kd_skpd."' AND
			c.kd_unitorganisasi IS NULL)

			GROUP BY p.nip , c.kd_skpd
			LIMIT $start, $limit");

		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($id){
		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, p.kd_skpd, p.kd_unitkerja, p.kelamin, p.agama, p.gol_darah,
		p.status, p.status_pegawai, p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos, p.no_ktp, p.tmt_cpns, p.tmt_pns');

		$this->db->from ('pegawai_tbl p');
		
		$this->db->where('p.nip',$id);
		$query = $this->db->get()->result_array();
		return $query;
	}


	public function fetchByIdNew($id){

		#Create where clause
		$this->db->select_max('a.tmt');
		$this->db->where('a.nip', $id);
		$this->db->from('riwayat_jabatan_tbl a');
		$where_clause = $this->db->get_compiled_select();

		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, p.kd_skpd, p.kd_unitkerja, p.kelamin, p.agama, p.gol_darah,
		p.status, p.status_pegawai, p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos, p.no_ktp, p.tmt_cpns, p.tmt_pns, pr.nama as skpd, u.nama as unitorganisasi, k.nama as unitkerja, l.nama as subunitkerja');

		$this->db->from ('pegawai_tbl p');
		$this->db->join ('riwayat_jabatan_tbl rj','p.nip = rj.nip','left');
		$this->db->join ('skpd_tbl pr','rj.kd_skpd = pr.kd_skpd','left');
		$this->db->join ('unitorganisasi_tbl u','rj.kd_unitorganisasi = u.kd_unitorganisasi','left');
		$this->db->join ('unitkerja_tbl k','rj.kd_unitkerja = k.kd_unitkerja','left');
		$this->db->join ('subunitkerja_tbl l','rj.kd_subunitkerja = l.kd_subunitkerja','left');

		$this->db->where('p.nip',$id);

		$this->db->where("rj.tmt = ($where_clause)", NULL, FALSE);

		$this->db->group_by('p.nip');

		$query = $this->db->get()->result_array();
		// $query = $this->db->get()->row();
		return $query;
	}
	
	public function createnoimage($data) {
		$this->nip = $data['nip'];
		$this->nip_lama = $data['nip_lama'];
		$this->nama = $data['nama'];
		$this->gelar_depan = $data['gelar_depan'];
		$this->gelar_belakang = $data['gelar_belakang'];
		$this->tempat_lahir = $data['tempat_lahir'];
		$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));
		$this->tgl_lahir = $data['tgl_lahir2'];
		$this->telp = $data['telp'];
		$this->kelamin = $data['kelamin'];
		$this->agama = $data['agama'];
		$this->alamat = $data['alamat'];
		$this->kode_pos = $data['kode_pos'];
		$this->gol_darah = $data['gol_darah'];
		$this->status = $data['status'];
		$this->status_pegawai = $data['status_pegawai'];
		$this->no_kartu_pegawai = $data['no_kartu_pegawai'];
		$this->no_askes = $data['no_askes'];
		$this->no_kartu_keluarga = $data['no_kartu_keluarga'];
		$this->no_ktp = $data['no_ktp'];
		$this->npwp = $data['npwp'];
		$this->kd_skpd = $data['nama_skpd'];
		$this->kd_unitkerja = $data['nama_unit_kerja'];
		if($data['tmt_cpns']==''){
                        $this->tmt_cpns = '0000-00-00';
                }else{
                        $data['tmt_cpns2'] = date("Y-m-d",strtotime($data['tmt_cpns']));
                        $this->tmt_cpns = $data['tmt_cpns2'];
                }

                if($data['tmt_pns']==''){
                        $this->tmt_pns = '';
                }else{
                        $data['tmt_pns2'] = date("Y-m-d",strtotime($data['tmt_pns']));
                        $this->tmt_pns = $data['tmt_pns2'];
                }

		
		// insert data
		$this->db->insert('pegawai_tbl', $this);
	}

	public function create($data) {
		$this->nip = $data['nip'];
		$this->nip_lama = $data['nip_lama'];
		$this->nama = $data['nama'];
		$this->picture = $data['picture'];
		$this->gelar_depan = $data['gelar_depan'];
		$this->gelar_belakang = $data['gelar_belakang'];
		$this->tempat_lahir = $data['tempat_lahir'];
		$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));
		$this->tgl_lahir = $data['tgl_lahir2'];
		$this->telp = $data['telp'];
		$this->kelamin = $data['kelamin'];
		$this->agama = $data['agama'];
		$this->alamat = $data['alamat'];
		$this->kode_pos = $data['kode_pos'];
		$this->gol_darah = $data['gol_darah'];
		$this->status = $data['status'];
		$this->status_pegawai = $data['status_pegawai'];
		$this->no_kartu_pegawai = $data['no_kartu_pegawai'];
		$this->no_askes = $data['no_askes'];
		$this->no_kartu_keluarga = $data['no_kartu_keluarga'];
		$this->no_ktp = $data['no_ktp'];
		$this->npwp = $data['npwp'];
		$this->kd_skpd = $data['nama_skpd'];
		$this->kd_unitkerja = $data['nama_unit_kerja'];
		if($data['tmt_cpns']==''){
                        $this->tmt_cpns = '0000-00-00';
                }else{
                        $data['tmt_cpns2'] = date("Y-m-d",strtotime($data['tmt_cpns']));
                        $this->tmt_cpns = $data['tmt_cpns2'];
                }

                if($data['tmt_pns']==''){
                        $this->tmt_pns = '';
                }else{
                        $data['tmt_pns2'] = date("Y-m-d",strtotime($data['tmt_pns']));
                        $this->tmt_pns = $data['tmt_pns2'];
                }

		
		// insert data
		$this->db->insert('pegawai_tbl', $this);
	}

	public function updatenoimage($data) {
		// get data
		$this->nip_lama = $data['nip_lama'];
		$this->nama = $data['nama'];
		$this->gelar_depan = $data['gelar_depan'];
		$this->gelar_belakang = $data['gelar_belakang'];
		$this->tempat_lahir = $data['tempat_lahir'];
		$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));
		$this->tgl_lahir = $data['tgl_lahir2'];
		$this->telp = $data['telp'];
		$this->kelamin = $data['kelamin'];
		$this->agama = $data['agama'];
		$this->alamat = $data['alamat'];
		$this->kode_pos = $data['kode_pos'];
		$this->gol_darah = $data['gol_darah'];
		$this->status = $data['status'];
		$this->status_pegawai = $data['status_pegawai'];
		$this->no_kartu_pegawai = $data['no_kartu_pegawai'];
		$this->no_askes = $data['no_askes'];
		$this->no_kartu_keluarga = $data['no_kartu_keluarga'];
		$this->no_ktp = $data['no_ktp'];
		$this->npwp = $data['npwp'];
		$this->kd_skpd = $data['nama_skpd'];
		$this->kd_unitkerja = $data['nama_unit_kerja'];
		if($data['tmt_cpns']==''){
                        $this->tmt_cpns = '0000-00-00';
                }else{
                        $data['tmt_cpns2'] = date("Y-m-d",strtotime($data['tmt_cpns']));
                        $this->tmt_cpns = $data['tmt_cpns2'];
                }

                if($data['tmt_pns']==''){
                        $this->tmt_pns = '';
                }else{
                        $data['tmt_pns2'] = date("Y-m-d",strtotime($data['tmt_pns']));
                        $this->tmt_pns = $data['tmt_pns2'];
                }

		
		// update data
		$this->db->update ( 'pegawai_tbl', $this, array ('nip' => $data['nip']));
	}
	
	public function update($data) {
		// get data
		$this->nip_lama = $data['nip_lama'];
		$this->nama = $data['nama'];
		$this->picture = $data['picture'];
		$this->gelar_depan = $data['gelar_depan'];
		$this->gelar_belakang = $data['gelar_belakang'];
		$this->tempat_lahir = $data['tempat_lahir'];
		$data['tgl_lahir2'] = date("Y-m-d",strtotime($data['tgl_lahir']));
		$this->tgl_lahir = $data['tgl_lahir2'];
		$this->telp = $data['telp'];
		$this->kelamin = $data['kelamin'];
		$this->agama = $data['agama'];
		$this->alamat = $data['alamat'];
		$this->kode_pos = $data['kode_pos'];
		$this->gol_darah = $data['gol_darah'];
		$this->status = $data['status'];
		$this->status_pegawai = $data['status_pegawai'];
		$this->no_kartu_pegawai = $data['no_kartu_pegawai'];
		$this->no_askes = $data['no_askes'];
		$this->no_kartu_keluarga = $data['no_kartu_keluarga'];
		$this->no_ktp = $data['no_ktp'];
		$this->npwp = $data['npwp'];
		$this->kd_skpd = $data['nama_skpd'];
		$this->kd_unitkerja = $data['nama_unit_kerja'];
		if($data['tmt_cpns']==''){
                        $this->tmt_cpns = '0000-00-00';
                }else{
                        $data['tmt_cpns2'] = date("Y-m-d",strtotime($data['tmt_cpns']));
                        $this->tmt_cpns = $data['tmt_cpns2'];
                }

                if($data['tmt_pns']==''){
                        $this->tmt_pns = '';
                }else{
                        $data['tmt_pns2'] = date("Y-m-d",strtotime($data['tmt_pns']));
                        $this->tmt_pns = $data['tmt_pns2'];
                }

		
		// update data
		$this->db->update ( 'pegawai_tbl', $this, array ('nip' => $data['nip']));
	}
	
	public function delete($id) {
		$this->db->delete ('pegawai_tbl', array ('nip' => $id));
		$this->db->delete ('riwayat_pendidikan_tbl', array ('nip' => $id));
		$this->db->delete ('riwayat_kepangkatan_tbl', array ('nip' => $id));
		$this->db->delete ('riwayat_jabatan_tbl', array ('nip' => $id));
		$this->db->delete ('riwayat_diklat_tbl', array ('nip' => $id));
		$this->db->delete ('riwayat_penghargaan_tbl', array ('nip' => $id));
		$this->db->delete ('riwayat_hukuman_tbl', array ('nip' => $id));
		$this->db->delete ('riwayat_anak_tbl', array ('nip' => $id));
		$this->db->delete ('riwayat_suami_istri_tbl', array ('nip' => $id));
	}
	
	public function meninggal($id) {
		$this->status_pegawai = "Meninggal";
		// update data
		$this->db->update ('pegawai_tbl',$this, array ('nip' => $id));
	}
	
	public function pensiun($id) {
		$this->status_pegawai = "Pensiun";
		// update data
		$this->db->update ('pegawai_tbl',$this, array ('nip' => $id));
	}
	
	public function pindah_keluar($id) {
		$this->status_pegawai = "Pindah Tugas Keluar";
		// update data
		$this->db->update ('pegawai_tbl',$this, array ('nip' => $id));
	}
	
	public function kembalikan_data($id) {
		$this->status_pegawai = "Aktif";
		// update data
		$this->db->update ('pegawai_tbl',$this, array ('nip' => $id));
	}

	public function getSingle($nip){
        $this->db->where('nip', $nip);
        $query = $this->db->get('pegawai_tbl');
        return $query->row();
    }
	
	public function get($nip){
        $this->db->where('nip', $nip);
        $query = $this->db->get('pegawai_tbl', 1);
        return $query->result();
    }
	
	public function getName($nip){
		$this->db->select('nama');
		$this->db->from('pegawai_tbl');
		$this->db->where('nip',$nip);
		$query= $this->db->get();
		$ret = $query->row();
		return $ret->nama;
	}
	
	public function search_count($column, $data){
		return  $this->db->count_all("pegawai_tbl where $column like '%$data%'");
	}
	
	public function search_count_skpd($kd_skpd, $column, $data){
		return  $this->db->count_all("pegawai_tbl where kd_skpd='$kd_skpd' and $column like '%$data%'");
	}

	public function search_count_per_skpd($kd_skpd, $column, $data){
		$query = $this->db->query("
			SELECT p.nip
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			
			WHERE 
			p.nip = b.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$kd_skpd."'

			AND p.$column LIKE '%$data%'

			GROUP BY p.nip , c.kd_skpd");

		return $query->num_rows();
	}
	
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang,
		p.tempat_lahir, p.tgl_lahir, p.telp,
		(case p.kelamin when "1" then "Pria" when "2" then "Wanita" end) kelamin ,
		(case p.agama when "1" then "Islam" when "2" then "Katolik" when "3" then "Hindu" when "4" then "Budha" when "5" then "Sinto" when "6" then "Konghucu" when "7" then "Protestan" end) agama, 
		(case p.gol_darah when "1" then "A" when "2" then "B" when "3" then "AB" else "O" end) gol_darah,
		(case p.status when "1" then "Lajang" when "2" then "Nikah" else "Cerai" end) status, p.status_pegawai,
		p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos');
		$this->db->from ('pegawai_tbl p ');
		$this->db->where ("p.status_pegawai != 'Pindah Tugas Keluar'");
		$this->db->where ("p.status_pegawai != 'Pensiun'");
		$this->db->where ("p.status_pegawai != 'Meninggal'");
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_skpd($kd_skpd, $column,$value, $limit, $start){
		
		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang,
		p.tempat_lahir, p.tgl_lahir, p.telp,
		(case p.kelamin when "1" then "Pria" else "Wanita" end) kelamin ,
		(case p.agama when "1" then "Islam" when "2" then "Katolik" when "3" then "Hindu" when "4" then "Budha" when "5" then "Sinto" when "6" then "Konghucu" when "7" then "Protestan" end) agama, 
		(case p.gol_darah when "1" then "A" when "2" then "B" when "3" then "AB" else "O" end) gol_darah,
		(case p.status when "1" then "Lajang" when "2" then "Nikah" else "Cerai" end) status, p.status_pegawai,
		p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos');
		$this->db->where ('kd_skpd',$kd_skpd);
		$this->db->from ('pegawai_tbl p ');
		$this->db->where ("p.status_pegawai != 'Pindah Tugas Keluar'");
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_per_skpd($kd_skpd, $column,$value, $limit, $start){
		$query = $this->db->query("
			SELECT p.nip, ifnull(p.nip_lama,'-') nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang, 
		p.tempat_lahir, p.tgl_lahir, p.telp, 
		(case p.kelamin when '1' then 'Pria' when '2' then 'Wanita' end) kelamin ,
		(case p.agama when '1' then 'Islam' when '2' then 'Katolik' when '3' then 'Hindu' when '4' then 'Budha' when '5' then 'Sinto' when '6' then 'Konghucu' when '7' then 'Protestan' end) agama, 
		(case p.gol_darah when '1' then 'A' when '2' then 'B' when '3' then 'AB' else 'O' end) gol_darah,
		(case p.status when '1' then 'Lajang' when '2' then 'Nikah' else 'Cerai' end) status, 
		p.status_pegawai, p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos
			FROM pegawai_tbl p 
			LEFT JOIN riwayat_jabatan_tbl b
			on b.tmt IN(
			SELECT MAX(tmt)
			FROM riwayat_jabatan_tbl 
			WHERE nip=b.nip)
			LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
			
			WHERE 
			p.nip = b.nip AND
			p.status_pegawai != 'Pindah Tugas Keluar' AND 
			p.status_pegawai != 'Pensiun' AND 
			p.status_pegawai != 'Meninggal' AND
			c.kd_skpd = '".$kd_skpd."'

			AND p.$column LIKE '%$value%'

			GROUP BY p.nip , c.kd_skpd
			LIMIT $start, $limit");

		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function getSessionPegawai($username){
		$this->db->select('kd_skpd');
		$this->db->from('users');
		$this->db->where('username',$username);
		$query = $this->db->get();
		$ret = $query->row();
		$data = array(
				'ss_skpd'=>$ret->kd_skpd
		);
		return $data;
	}
	
	public function link_gambar($id)
	{
		
		$this->db->where(array ('nip' => $id));
		$query = $getData = $this->db->get('pegawai_tbl');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	public function data4($kd_skpd) {
        $query  = $this->db->query("SELECT b.kd_unitkerja, b.nama FROM skpd_tbl a, unitkerja_tbl b 
									WHERE a.kd_skpd = b.kd_skpd 
									AND a.kd_skpd='$kd_skpd' ");
        return $query->result();
    }
	
	function get_skpd_all() {
		$result = $this->db->get("skpd_tbl");
			$options = array();
				$options[""] = "Pilih SKPD";
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}
	
	public function ambil_unit_kerja($kd_skpd){
		$this->db->where('kd_skpd',$kd_skpd);
		$this->db->order_by('nama','asc');
		$unit_kerja=$this->db->get('unitkerja_tbl');
		if($unit_kerja->num_rows()>0){

			foreach ($unit_kerja->result_array() as $row)
			{
				$result['']= '- Pilih Unit Kerja-';
				$result[$row['kd_unitkerja']]= $row['nama'];
			}
			} else {
			   $result['']= '-Belum Ada Unit Kerja-';
			}
			return $result;
	}
	
	function get_skpd_skpd($kd_skpd) {
		$result = $this->db->get("skpd_tbl WHERE kd_skpd='$kd_skpd'");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}
	
	public function get_skpd_all_2($id){
		$this->db->select ('*');
		$this->db->where ('kd_skpd', $id);
		$this->db->from ('skpd_tbl');
		$result = $this->db->get();
			$options = array();
				foreach($result->result_array() as $row) {
				$options = $row["kd_skpd"];
			}
		return $options;
	}
	
	public function skpd(){
        $query = $this->db->get('skpd_tbl');
        return $query->result();
    }
	
	public function unit_kerja(){
        $query = $this->db->get('unitkerja_tbl');
        return $query->result();
    }
	
	
}