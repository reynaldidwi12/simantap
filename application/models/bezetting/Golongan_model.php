<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class golongan_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		return $this->db->count_all("skpd_tbl");
	}
	
	public function fetchAll($limit, $start) {
		$sql = "
		SELECT e.kd_skpd, e.nama, COUNT(p.nip) as total_pegawai, SUM( CASE WHEN p.kelamin = '1' THEN 1 ELSE 0 END ) as L,
		SUM( CASE WHEN p.kelamin = '2' THEN 1 ELSE 0 END ) as P,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/a','1','0'),'0') ) as L1A,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/a','1','0'),'0') ) as P1A,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/b','1','0'),'0') ) as L1B,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/b','1','0'),'0') ) as P1B,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/c','1','0'),'0') ) as L1C,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/c','1','0'),'0') ) as P1C,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/d','1','0'),'0') ) as L1D,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/d','1','0'),'0') ) as P1D,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/a','1','0'),'0') ) as L2A,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/a','1','0'),'0') ) as P2A,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/b','1','0'),'0') ) as L2B,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/b','1','0'),'0') ) as P2B,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/c','1','0'),'0') ) as L2C,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/c','1','0'),'0') ) as P2C,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/d','1','0'),'0') ) as L2D,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/d','1','0'),'0') ) as P2D,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/a','1','0'),'0') ) as L3A,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/a','1','0'),'0') ) as P3A,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/b','1','0'),'0') ) as L3B,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/b','1','0'),'0') ) as P3B,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/c','1','0'),'0') ) as L3C,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/c','1','0'),'0') ) as P3C,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/d','1','0'),'0') ) as L3D,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/d','1','0'),'0') ) as P3D,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/a','1','0'),'0') ) as L4A,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/a','1','0'),'0') ) as P4A,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/b','1','0'),'0') ) as L4B,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/b','1','0'),'0') ) as P4B,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/c','1','0'),'0') ) as L4C,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/c','1','0'),'0') ) as P4C,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/d','1','0'),'0') ) as L4D,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/d','1','0'),'0') ) as P4D
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%') 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL)
		GROUP BY c.kd_skpd
		ORDER BY e.category ASC
		LIMIT $start, $limit";
		
		$query = $this->db->query($sql);
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_count($category){
		return  $this->db->count_all("skpd_tbl where category = '$category'");
	}
	
	public function getName($kode){
		$this->db->select('nama');
		$this->db->from('skpd_tbl');
		$this->db->where('kd_skpd', $kode);
		$query= $this->db->get();
		$ret = $query->row();
		return $ret->nama;
	}
	
	public function search($value, $limit, $start){

		$array=array_map('intval', explode(',', $value));
		$array=implode("','",$array);
		
		$sql = "
		SELECT e.kd_skpd, e.nama, COUNT(p.nip) as total_pegawai, SUM( CASE WHEN p.kelamin = '1' THEN 1 ELSE 0 END ) as L,
		SUM( CASE WHEN p.kelamin = '2' THEN 1 ELSE 0 END ) as P,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/a','1','0'),'0') ) as L1A,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/a','1','0'),'0') ) as P1A,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/b','1','0'),'0') ) as L1B,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/b','1','0'),'0') ) as P1B,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/c','1','0'),'0') ) as L1C,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/c','1','0'),'0') ) as P1C,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/d','1','0'),'0') ) as L1D,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/d','1','0'),'0') ) as P1D,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/a','1','0'),'0') ) as L2A,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/a','1','0'),'0') ) as P2A,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/b','1','0'),'0') ) as L2B,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/b','1','0'),'0') ) as P2B,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/c','1','0'),'0') ) as L2C,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/c','1','0'),'0') ) as P2C,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/d','1','0'),'0') ) as L2D,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/d','1','0'),'0') ) as P2D,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/a','1','0'),'0') ) as L3A,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/a','1','0'),'0') ) as P3A,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/b','1','0'),'0') ) as L3B,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/b','1','0'),'0') ) as P3B,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/c','1','0'),'0') ) as L3C,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/c','1','0'),'0') ) as P3C,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/d','1','0'),'0') ) as L3D,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/d','1','0'),'0') ) as P3D,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/a','1','0'),'0') ) as L4A,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/a','1','0'),'0') ) as P4A,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/b','1','0'),'0') ) as L4B,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/b','1','0'),'0') ) as P4B,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/c','1','0'),'0') ) as L4C,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/c','1','0'),'0') ) as P4C,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/d','1','0'),'0') ) as L4D,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/d','1','0'),'0') ) as P4D
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%'";
		
		if(!empty($value) || $value="") {
			$sql .= " AND e.category IN ('".$array."')";
		}

		$sql .= ") 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL";

		if(!empty($value) || $value="") {
			$sql .= " AND e.category IN ('".$array."')";
		}
		
		$sql .= ") GROUP BY c.kd_skpd
		ORDER BY e.category ASC
		LIMIT $start, $limit";
		
		$query = $this->db->query($sql);

		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	function get_skpd() {
		$result = $this->db->get("skpd_tbl");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}

	function get_skpd_by_status($status) {
		$this->db->where('status', $status);
		$result = $this->db->get("skpd_tbl");
			$options = array();
				foreach($result->result_array() as $row) {
				$options[$row["kd_skpd"]] = $row["nama"];
			}
		return $options;
	}
	
	public function generatekd_skpd(){
		return $this->db->query("SELECT kd_skpd FROM skpd_tbl order by kd_skpd desc limit 1")->row()->kd_skpd+1;	
	}

	public function record_count_per_skpd() {
		$query = "
		SELECT e.kd_skpd, e.nama, e.category, COUNT(p.nip) as total_pegawai, SUM( CASE WHEN p.kelamin = '1' THEN 1 ELSE 0 END ) as L,
		SUM( CASE WHEN p.kelamin = '2' THEN 1 ELSE 0 END ) as P,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/a','1','0'),'0') ) as L1A,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/a','1','0'),'0') ) as P1A,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/b','1','0'),'0') ) as L1B,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/b','1','0'),'0') ) as P1B,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/c','1','0'),'0') ) as L1C,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/c','1','0'),'0') ) as P1C,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/d','1','0'),'0') ) as L1D,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/d','1','0'),'0') ) as P1D,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/a','1','0'),'0') ) as L2A,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/a','1','0'),'0') ) as P2A,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/b','1','0'),'0') ) as L2B,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/b','1','0'),'0') ) as P2B,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/c','1','0'),'0') ) as L2C,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/c','1','0'),'0') ) as P2C,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/d','1','0'),'0') ) as L2D,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/d','1','0'),'0') ) as P2D,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/a','1','0'),'0') ) as L3A,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/a','1','0'),'0') ) as P3A,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/b','1','0'),'0') ) as L3B,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/b','1','0'),'0') ) as P3B,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/c','1','0'),'0') ) as L3C,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/c','1','0'),'0') ) as P3C,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/d','1','0'),'0') ) as L3D,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/d','1','0'),'0') ) as P3D,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/a','1','0'),'0') ) as L4A,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/a','1','0'),'0') ) as P4A,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/b','1','0'),'0') ) as L4B,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/b','1','0'),'0') ) as P4B,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/c','1','0'),'0') ) as L4C,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/c','1','0'),'0') ) as P4C,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/d','1','0'),'0') ) as L4D,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/d','1','0'),'0') ) as P4D
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd != 'NULL' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%') 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd != 'NULL' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL)
		GROUP BY c.kd_skpd
		ORDER BY e.category ASC";

		$sql = $this->db->query($query);

		return $sql->result();
	}

	public function fetchAll_per_skpd($kd_skpd, $limit, $start) {
		$sql = "
		SELECT e.kd_skpd, e.nama, COUNT(p.nip) as total_pegawai, SUM( CASE WHEN p.kelamin = '1' THEN 1 ELSE 0 END ) as L,
		SUM( CASE WHEN p.kelamin = '2' THEN 1 ELSE 0 END ) as P,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/a','1','0'),'0') ) as L1A,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/a','1','0'),'0') ) as P1A,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/b','1','0'),'0') ) as L1B,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/b','1','0'),'0') ) as P1B,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/c','1','0'),'0') ) as L1C,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/c','1','0'),'0') ) as P1C,
		SUM( IF(p.kelamin='1',IF(d.golongan='I/d','1','0'),'0') ) as L1D,
		SUM( IF(p.kelamin='2',IF(d.golongan='I/d','1','0'),'0') ) as P1D,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/a','1','0'),'0') ) as L2A,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/a','1','0'),'0') ) as P2A,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/b','1','0'),'0') ) as L2B,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/b','1','0'),'0') ) as P2B,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/c','1','0'),'0') ) as L2C,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/c','1','0'),'0') ) as P2C,
		SUM( IF(p.kelamin='1',IF(d.golongan='II/d','1','0'),'0') ) as L2D,
		SUM( IF(p.kelamin='2',IF(d.golongan='II/d','1','0'),'0') ) as P2D,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/a','1','0'),'0') ) as L3A,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/a','1','0'),'0') ) as P3A,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/b','1','0'),'0') ) as L3B,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/b','1','0'),'0') ) as P3B,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/c','1','0'),'0') ) as L3C,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/c','1','0'),'0') ) as P3C,
		SUM( IF(p.kelamin='1',IF(d.golongan='III/d','1','0'),'0') ) as L3D,
		SUM( IF(p.kelamin='2',IF(d.golongan='III/d','1','0'),'0') ) as P3D,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/a','1','0'),'0') ) as L4A,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/a','1','0'),'0') ) as P4A,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/b','1','0'),'0') ) as L4B,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/b','1','0'),'0') ) as P4B,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/c','1','0'),'0') ) as L4C,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/c','1','0'),'0') ) as P4C,
		SUM( IF(p.kelamin='1',IF(d.golongan='IV/d','1','0'),'0') ) as L4D,
		SUM( IF(p.kelamin='2',IF(d.golongan='IV/d','1','0'),'0') ) as P4D
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b on b.tmt IN( SELECT MAX(tmt) FROM riwayat_jabatan_tbl WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		LEFT JOIN riwayat_kepangkatan_tbl d on d.golongan IN( SELECT MAX(golongan) FROM riwayat_kepangkatan_tbl WHERE nip=d.nip)
		LEFT JOIN skpd_tbl e on e.kd_skpd = c.kd_skpd
		LEFT JOIN unitorganisasi_tbl g on g.kd_unitorganisasi = c.kd_unitorganisasi
		WHERE (p.nip = b.nip AND
		p.nip = d.nip AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		e.kd_skpd = '".$kd_skpd."' AND
		g.nama NOT LIKE '%KECAMATAN%' AND
		g.nama NOT LIKE '%UPTD%' AND
		g.nama NOT LIKE '%PUSKESMAS%' AND
		g.nama NOT LIKE '%ASISTEN%' AND
		g.nama NOT LIKE '%PENGAWAS SEKOLAH%' AND
		g.nama NOT LIKE '%PENYULUH PERTANIAN%' AND
		g.nama NOT LIKE '%SMP%' AND
		g.nama NOT LIKE '%SD%' AND
		g.nama NOT LIKE '%TK%') 
		OR (
		p.nip = b.nip AND
		p.nip = d.nip AND
		e.kd_skpd = '".$kd_skpd."' AND
		p.status_pegawai != 'Pindah Tugas Keluar' AND 
		p.status_pegawai != 'Pensiun' AND 
		p.status_pegawai != 'Meninggal' AND
		c.kd_unitorganisasi IS NULL)
		GROUP BY c.kd_skpd
		ORDER BY e.category ASC
		LIMIT $start, $limit";
		
		$query = $this->db->query($sql);
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
}