<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class data_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function getId($kode){
		$this->db->select('kd_skpd');
		$this->db->from('skpd_tbl');
		$this->db->where('kode', $kode);
		$query= $this->db->get();
		$ret = $query->row();
		return $ret->kd_skpd;
	}

	public function checkPegawaiBySkpd($nip, $kd_skpd) {
		$query = $this->db->query("
		SELECT p.nip
		FROM pegawai_tbl p 
		LEFT JOIN riwayat_jabatan_tbl b
		on b.tmt IN(
		SELECT MAX(tmt)
		FROM riwayat_jabatan_tbl 
		WHERE nip=b.nip)
		LEFT JOIN jabatan_tbl c on c.kd_jabatan = b.kd_jabatan
		
		WHERE p.nip = b.nip AND b.nip = '".$nip."' AND c.kd_skpd = '".$kd_skpd."'");

		$result = $query->num_rows();
		return $result;
	}
	
}